FROM php:7.1-apache

# to build on console, run: docker build -t wisotel-kv -f ./docker/web.dockerfile .

WORKDIR /app

RUN apt-get update
RUN apt-get upgrade -y

RUN docker-php-ext-install pdo pdo_mysql mysqli sockets
RUN apt-get install -y libicu-dev
RUN docker-php-ext-configure intl
RUN docker-php-ext-install intl

RUN a2enmod rewrite