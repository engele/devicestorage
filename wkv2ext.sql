-- MySQL dump 10.14  Distrib 5.5.52-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: wkv2ext
-- ------------------------------------------------------
-- Server version	5.5.52-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Technik_KH`
--

DROP TABLE IF EXISTS `Technik_KH`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Technik_KH` (
  `index` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Kommando` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Netz` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Error` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Technik_KH`
--

LOCK TABLES `Technik_KH` WRITE;
/*!40000 ALTER TABLE `Technik_KH` DISABLE KEYS */;
/*!40000 ALTER TABLE `Technik_KH` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_users`
--

DROP TABLE IF EXISTS `app_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `groupware_username` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salt` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `include_in_history_reporting` tinyint(1) NOT NULL,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C2502824F85E0677` (`username`),
  UNIQUE KEY `UNIQ_C2502824E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_users`
--

LOCK TABLES `app_users` WRITE;
/*!40000 ALTER TABLE `app_users` DISABLE KEYS */;
INSERT INTO `app_users` VALUES (1,'This is admin',NULL,'25f48543a08ac15c41a7afd941c23500','your-email@example.de',1,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}',0,'admin','$2y$12$WnDOcq/a9WJydxyZ.zo1J.uup3IAiNwhEtcsf4y0sbsu3lHojStu.'),(2,'Sven Siebrands',NULL,'e35ac4d32e6591216159391bcbcf8b74','sven.siebrands@wisotel.com',1,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}',0,'ssi','$2y$12$XhSazF4lDPpbOwzzR//XUulUaIQdI4h.hQak6WWm1Kv.BOIYk7.uC');
/*!40000 ALTER TABLE `app_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bic_bankname`
--

DROP TABLE IF EXISTS `bic_bankname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bic_bankname` (
  `bic` varchar(20) NOT NULL,
  `bankname` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`bic`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bic_bankname`
--

LOCK TABLES `bic_bankname` WRITE;
/*!40000 ALTER TABLE `bic_bankname` DISABLE KEYS */;
/*!40000 ALTER TABLE `bic_bankname` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_types`
--

DROP TABLE IF EXISTS `card_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_types`
--

LOCK TABLES `card_types` WRITE;
/*!40000 ALTER TABLE `card_types` DISABLE KEYS */;
INSERT INTO `card_types` VALUES (1,'ADSL'),(2,'VDSL'),(3,'SDSL'),(4,'GPON'),(5,'ETHERNET'),(6,'Mobile'),(7,'VDSL-BLV'),(8,'VDSL-SLV');
/*!40000 ALTER TABLE `card_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards`
--

DROP TABLE IF EXISTS `cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `port_amount` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `card_type_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `Line` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4C258FD64D218E` (`location_id`),
  KEY `IDX_4C258FD925606E5` (`card_type_id`),
  CONSTRAINT `FK_4C258FD64D218E` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`),
  CONSTRAINT `FK_4C258FD925606E5` FOREIGN KEY (`card_type_id`) REFERENCES `card_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='type';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards`
--

LOCK TABLES `cards` WRITE;
/*!40000 ALTER TABLE `cards` DISABLE KEYS */;
INSERT INTO `cards` VALUES (1,48,'Karte 1 VDSL',2,1,'1/1/1/',NULL),(2,64,'GPON 1',4,2,'1/1/1/1/',NULL);
/*!40000 ALTER TABLE `cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `connection_city`
--

DROP TABLE IF EXISTS `connection_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `connection_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(64) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `connection_city`
--

LOCK TABLES `connection_city` WRITE;
/*!40000 ALTER TABLE `connection_city` DISABLE KEYS */;
INSERT INTO `connection_city` VALUES (1,'Ingolstadt',10);
/*!40000 ALTER TABLE `connection_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `connection_district`
--

DROP TABLE IF EXISTS `connection_district`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `connection_district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `district` varchar(64) DEFAULT NULL,
  `zipcode` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `connection_district`
--

LOCK TABLES `connection_district` WRITE;
/*!40000 ALTER TABLE `connection_district` DISABLE KEYS */;
INSERT INTO `connection_district` VALUES (1,'Ingolstadt',85049,1,110);
/*!40000 ALTER TABLE `connection_district` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_history`
--

DROP TABLE IF EXISTS `customer_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `created_by` varchar(40) COLLATE utf8_unicode_ci NOT NULL COMMENT 'UserId',
  `created_at` datetime NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_history`
--

LOCK TABLES `customer_history` WRITE;
/*!40000 ALTER TABLE `customer_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_history_events`
--

DROP TABLE IF EXISTS `customer_history_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_history_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` varchar(140) COLLATE utf8_unicode_ci NOT NULL,
  `order_number` int(11) DEFAULT NULL,
  `group_id` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_net` decimal(7,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_history_events`
--

LOCK TABLES `customer_history_events` WRITE;
/*!40000 ALTER TABLE `customer_history_events` DISABLE KEYS */;
INSERT INTO `customer_history_events` VALUES (1,'Neukundenberatung/-gespräch',NULL,NULL,NULL),(2,'Bestandskundenberatung',NULL,NULL,NULL),(3,'Mailbearbeitung',NULL,NULL,NULL),(4,'Interessent manuell anlegen',NULL,NULL,NULL),(5,'Vertragserfassung manuell',NULL,NULL,NULL),(6,'Vertragserfassung elektronisch',NULL,NULL,NULL),(7,'Anbieterwechselauftrag erstellt',NULL,NULL,NULL),(8,'Anbieterwechsel Fax',NULL,NULL,NULL),(9,'Anbieterwechsel TVS',NULL,NULL,NULL),(10,'Anbieterwechselauftrag abgebend',NULL,NULL,NULL),(11,'Anbieterwechsel Rufnummernblock (GK)',NULL,NULL,NULL),(12,'Anbieterwechselauftrag elektronisch (WBCI)',NULL,NULL,NULL),(13,'Telefonbucheintrag',NULL,NULL,NULL),(14,'ECASS-Abfrage',NULL,NULL,NULL),(15,'TAL bestellen, bestätigen',NULL,NULL,NULL),(16,'TAL TVS',NULL,NULL,NULL),(17,'Nachfragen DTAG',NULL,NULL,NULL),(18,'Eskalation',NULL,NULL,NULL),(19,'TAL StöMe',NULL,NULL,NULL),(20,'Nachweisverfahren 1+2',NULL,NULL,NULL),(21,'TAL Kündigung',NULL,NULL,NULL),(22,'TAL Storno',NULL,NULL,NULL),(24,'Absprachen mit Partner',NULL,NULL,NULL),(25,'IP-Adresse zuweisen',NULL,NULL,NULL),(26,'Zuordnung Leitungsführung (LSA KP)',NULL,NULL,NULL),(27,'Kunde eingerichtet (DSLAM, PPPOE)',NULL,NULL,NULL),(28,'Kunde gepatched',NULL,NULL,NULL),(29,'Endgerät eingerichtet (Tausch, ACS, man.)',NULL,NULL,NULL),(30,'Endgerät versenden',NULL,NULL,NULL),(31,'Aktivschaltung',NULL,NULL,NULL),(32,'Rechnungsklärung',NULL,NULL,NULL),(33,'Rufnummernaufschaltung (KT)',NULL,NULL,NULL),(34,'Umzug Kunde',NULL,NULL,NULL),(35,'Produktwechsel',NULL,NULL,NULL),(36,'StöMe intern (Ticket) erstellen / bearbeiten',NULL,NULL,NULL),(37,'Vertragsübernahme',NULL,NULL,NULL),(38,'Vertragskündigung',NULL,NULL,NULL),(39,'Dokumentation',NULL,NULL,NULL),(40,'Rufnummernbestellung',NULL,NULL,NULL),(41,'Kundenbesuch im Haus',NULL,NULL,NULL),(42,'Rufnummernimport und Storno',NULL,NULL,NULL),(43,'Inkasso',NULL,NULL,NULL),(44,'Vorabstimmung Kunde (ST)',NULL,NULL,NULL),(45,'Kundenbesuche beim Kunden (ST)',NULL,NULL,NULL),(46,'Kundenberatung telefonisch (ST)',NULL,NULL,NULL),(47,'Namensänderung',NULL,NULL,NULL),(48,'Nacherfassung bei fehlenden Kundendaten',NULL,NULL,NULL),(49,'Widerruf Kündigung',NULL,NULL,NULL),(50,'Purtel Kündigung',NULL,NULL,NULL),(51,'Rufnummernaufschaltung (ST)',NULL,NULL,NULL),(52,'Rufnummernexport',NULL,NULL,NULL),(53,'Rufumleitung für Kunden einrichten',NULL,NULL,NULL),(54,'nachträgliche Rufnummernportierung manuell',NULL,NULL,NULL),(55,'nachträgliche Rufnummernportierung elektr.',NULL,NULL,NULL),(56,'TAL Übernahme',NULL,NULL,NULL),(57,'Line in use Meldung',NULL,NULL,NULL),(58,'IP-TV einrichten',NULL,NULL,NULL),(59,'Kunde gepatched (mit Anfahrt)',NULL,NULL,NULL),(60,'Kunde sperren/freischalten',NULL,NULL,NULL),(61,'Kunde technische Hilfe Remote (telefonisch)',NULL,NULL,NULL),(62,'Endgerät-Abholung und Einweisung Kunde',NULL,NULL,NULL),(63,'Geschäftskunden SLA /IP Konfig zusenden',NULL,NULL,NULL),(64,'StöMe intern (Ticket) (ST)',NULL,NULL,NULL);
/*!40000 ALTER TABLE `customer_history_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_options`
--

DROP TABLE IF EXISTS `customer_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_id` int(11) NOT NULL,
  `opt_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `price_net` decimal(12,5) DEFAULT NULL,
  `price_gross` decimal(12,5) DEFAULT NULL,
  `activation_date` date DEFAULT NULL,
  `deactivation_date` date DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CCE85E39CCEFD70A` (`opt_id`),
  KEY `IDX_CCE85E39FDD13F95` (`tax_rate_id`),
  CONSTRAINT `FK_CCE85E39CCEFD70A` FOREIGN KEY (`opt_id`) REFERENCES `optionen` (`option`),
  CONSTRAINT `FK_CCE85E39FDD13F95` FOREIGN KEY (`tax_rate_id`) REFERENCES `tax_rates` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_options`
--

LOCK TABLES `customer_options` WRITE;
/*!40000 ALTER TABLE `customer_options` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_products`
--

DROP TABLE IF EXISTS `customer_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `price_net` decimal(12,5) DEFAULT NULL,
  `activation_date` date DEFAULT NULL,
  `deactivation_date` date DEFAULT NULL,
  `propperly_copyed_to_customer` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9F7F19754584665A` (`product_id`),
  CONSTRAINT `FK_9F7F19754584665A` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_products`
--

LOCK TABLES `customer_products` WRITE;
/*!40000 ALTER TABLE `customer_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_terminals`
--

DROP TABLE IF EXISTS `customer_terminals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_terminals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `ip_address` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mac_address` varchar(17) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serialnumber` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remote_user` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remote_password` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remote_port` int(10) DEFAULT NULL,
  `tv_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `check_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activation_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_terminals`
--

LOCK TABLES `customer_terminals` WRITE;
/*!40000 ALTER TABLE `customer_terminals` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_terminals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clientid` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `version` int(11) DEFAULT NULL,
  `customer_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `external_id_2` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_electrisity` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_electrisity_enddate` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contract_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direct_debit_allowed` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `connection_fee_paid` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_holder_firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_holder_lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_number` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_bic` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_bankname` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_iban` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_bic_new` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_debitor` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_city` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_emailaddress` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_street` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_streetno` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_zipcode` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credit_rating_ok` varchar(64) COLLATE utf8_unicode_ci DEFAULT 'outstanding',
  `special_conditions_till` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wisocontract_cancel_input_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `wisocontract_cancel_input_ack` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `wisocontract_cancel_account_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `wisocontract_cancel_account_finish` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `wisocontract_cancel_extra` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wisocontract_cancel_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `wisocontract_cancel_ack` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `wisocontract_canceled_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `wisocontract_rehire` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wisocontract_rehire_person` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wisocontract_canceled_from` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `wisocontract_move` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `cancel_comment` text COLLATE utf8_unicode_ci,
  `new_city` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_zipcode` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_street` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_streetno` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `get_hardware_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `pay_hardware_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `move_registration_ack` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wisocontract_switchoff` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `wisocontract_switchoff_finish` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `connection_street` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `connection_streetno` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `connection_city` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `connection_zipcode` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `connection_address_equal` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `connection_fee_height` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `connection_wish_date` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `connection_activation_date` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `connection_inactive_date` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recommended_product` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_phone_lines` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_phone_nos` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paper_bill` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gf_branch` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `productname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_level` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PKQ',
  `service_level_send` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tv_service` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `call_data_record` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registration_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reg_answer_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contract_sent_date` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contract_received_date` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_charge_terminal` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_product` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtelproduct_advanced` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `streetno` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `district` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clienttype` varchar(24) COLLATE utf8_unicode_ci DEFAULT 'non_commercial',
  `phoneareacode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phonenumber` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobilephone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emailaddress` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `wisotelno` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prospect_supply_status` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'supply_unknown',
  `oldcontract_exists` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldcontract_active` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldcontract_title` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldcontract_firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldcontract_lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldcontract_street` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `oldcontract_streetno` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `oldcontract_zipcode` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `oldcontract_city` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `oldcontract_phone_type` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldcontract_phone_provider` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldcontract_internet_provider` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldcontract_porting` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldcontract_phone_clientno` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldcontract_internet_clientno` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldcontract_address_equal` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cudaid` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wbci_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wbci_ekpabg` int(11) DEFAULT NULL,
  `wbci_gf` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wbci_eingestellt_am` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wbci_eingestellt_von` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wbci_ruemva_am` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wbci_ruemva_von` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wbci_bestaetigt_am` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wbci_bestaetigt_von` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wbci_akmtr` tinyint(4) DEFAULT NULL,
  `wbci_akmtr_am` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wbci_akmtr_von` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wbci_comment` text COLLATE utf8_unicode_ci,
  `exp_date_phone` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `exp_done_phone` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `exp_date_int` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `exp_done_int` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lapse_notice_date_tel` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lapse_notice_date_inet` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `special_termination_internet` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `special_termination_phone` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tal_order` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tal_order_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tal_orderd_from` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tal_product` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tal_assigned_phoneno` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tal_lended_fritzbox` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tech_free_for_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `tech_free_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `stati_port_confirm_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `portdefault_comment` text COLLATE utf8_unicode_ci,
  `stati_oldcontract_comment` text COLLATE utf8_unicode_ci,
  `ventelo_port_letter_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ventelo_port_letter_from` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ventelo_port_letter_how` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ventelo_confirmation_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ventelo_purtel_enter_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ventelo_port_wish_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tal_order_ack_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tal_order_work` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tal_dtag_assignment_no` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nb_canceled_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `application` varchar(32) COLLATE utf8_unicode_ci DEFAULT 'red',
  `contract` varchar(32) COLLATE utf8_unicode_ci DEFAULT 'red',
  `porting` varchar(32) COLLATE utf8_unicode_ci DEFAULT 'red',
  `talorder` varchar(64) COLLATE utf8_unicode_ci DEFAULT 'red',
  `implementation` varchar(64) COLLATE utf8_unicode_ci DEFAULT 'yellow',
  `application_text` text COLLATE utf8_unicode_ci,
  `contract_text` text COLLATE utf8_unicode_ci,
  `porting_text` text COLLATE utf8_unicode_ci,
  `talorder_text` text COLLATE utf8_unicode_ci,
  `implementation_text` text COLLATE utf8_unicode_ci,
  `password` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subnetmask` varchar(64) COLLATE utf8_unicode_ci DEFAULT '255.255.255.192',
  `terminal_type` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `terminal_type_exists` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `terminal_serialno` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kvz_toggle_no` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lsa_pin` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dslam_card_no` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dslam_port` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dslam_location` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kvz_name` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `network` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `techdata_status` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldcontract_internet_client_cancelled` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldcontract_phone_client_cancelled` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_login` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_passwort` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_telnr` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_use_version` tinyint(4) DEFAULT NULL,
  `purtel_login_1` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_passwort_1` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_telnr_1` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_login_2` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_passwort_2` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_telnr_2` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_login_3` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_passwort_3` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_telnr_3` varchar(64) COLLATE utf8_unicode_ci DEFAULT '',
  `techdata_comment` text COLLATE utf8_unicode_ci,
  `purtel_edit_done` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `purtel_edit_done_from` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dtagbluckage_sended` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtagbluckage_sended_date` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtagbluckage_fullfiled` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtagbluckage_fullfiled_date` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `history` longtext COLLATE utf8_unicode_ci,
  `new_or_ported_phonenumbers` text COLLATE utf8_unicode_ci,
  `patch_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `little_bar` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `connect_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reached_upstream` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reached_downstream` varchar(32) COLLATE utf8_unicode_ci DEFAULT '0',
  `terminal_ready` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `terminal_ready_from` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `purtel_customer_reg_date` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_delete_date` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tal_dtag_revisor` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_connect_info_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `customer_connect_info_from` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `customer_connect_info_how` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tal_releasing_operator` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldcontract_comment` text COLLATE utf8_unicode_ci,
  `old_provider_comment` text COLLATE utf8_unicode_ci,
  `dtag_line` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pppoe_config_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contract_version` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recruited_by` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contract_comment` text COLLATE utf8_unicode_ci,
  `telefonbuch_eintrag` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phoneentry_done_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phoneentry_done_from` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `firmware_version` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remote_password` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `terminal_sended_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `terminal_sended_from` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remote_login` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credit_rating_link` varchar(512) COLLATE utf8_unicode_ci DEFAULT 'https://iport.infoscore.de',
  `credit_rating_date` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `network_id` int(11) DEFAULT '0',
  `location_id` int(11) NOT NULL DEFAULT '0',
  `ip_range_id` int(11) NOT NULL DEFAULT '0',
  `lsa_kvz_partition_id` int(11) NOT NULL DEFAULT '0',
  `card_id` int(11) NOT NULL DEFAULT '0',
  `line_identifier` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_data_record_id` int(11) NOT NULL DEFAULT '0',
  `kvz_addition` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `notice_period_phone_int` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notice_period_phone_type` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notice_period_internet_int` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notice_period_internet_type` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tal_ordered_for_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `new_clientid_permission` tinyint(1) NOT NULL DEFAULT '0',
  `sofortdsl` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `fb_vorab` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `acs_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `routing_active` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mac_address` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dslam_arranged_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `final_purtelproduct_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tvs` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tvs_from` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tvs_to` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tvs_date` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_eze` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_number_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `new_numbers_text` text COLLATE utf8_unicode_ci,
  `nb_canceled_date_phone` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `moved` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_number_from` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number_added` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `routing_active_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `routing_wish_date` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fordering_costs` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contract_change_to` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contract_acknowledge` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contract_online_date` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `special_conditions_text` text COLLATE utf8_unicode_ci,
  `connection_installation` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_technician` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_port` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_customer_on_fb` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `special_conditions_from` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `second_tal` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gutschrift` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gutschrift_till` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sip_accounts` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hd_plus_card` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_satellite` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `higher_uplink_one` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_flat` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eu_flat` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `higher_availability` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `higher_uplink_two` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vlan_ID` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag` tinyint(4) DEFAULT '1',
  `payment_performance` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kvz_standort` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cancel_tal` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tal_cancel_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tal_canceled_from` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tal_canceled_for_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tal_cancel_ack_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ventelo_purtel_enter_from` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `house_connection` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gf_cabling` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firewall_router` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stnz_fb` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `media_converter` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address_inet` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address_phone` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_ip_address` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rDNS_installation` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rDNS_count` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number_block` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_ip_address_cost` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_phone_lines_cost` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `german_network` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `german_network_price` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `german_mobile` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `german_mobile_price` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flat_german_network_cost` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `international_calls_price` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flat_german_network` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flat_eu_network` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isdn_backup` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_adapter` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_comment` text COLLATE utf8_unicode_ci,
  `cancel_purtel_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `cancel_purtel_for_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cancel_purtel` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cancel_purtel_from` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cancel_tel_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `cancel_tel_for_date` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `cancel_tel_from` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cancel_product` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cancel_product_from` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_bluckage_fullfiled_date` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_bluckage_sended_date` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_mandantenid` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `routing_deleted` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gf_cabling_cost` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isdn_backup2` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flat_eu_network_cost` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_ping` tinyint(4) DEFAULT NULL,
  `DPBO` tinyint(4) unsigned DEFAULT '1',
  `Service` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Spectrumprofile` tinyint(4) unsigned DEFAULT '3',
  `Kommando_query` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `voucher` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vectoring` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Ja',
  `inkasso_bankgebuehren` float(10,2) DEFAULT NULL,
  `inkasso_mahngebuehren` float(10,2) DEFAULT NULL,
  `inkasso_datum_letzte_mahnung` varchar(11) CHARACTER SET utf8 DEFAULT NULL,
  `inkasso_datum_ruecklastschrift` varchar(11) CHARACTER SET utf8 DEFAULT NULL,
  `inkasso_grund_ruecklastschrift` text CHARACTER SET utf8,
  `inkasso_hauptforderung` float(10,2) DEFAULT NULL,
  `inkasso_rechnung` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `pppoe_pin` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5379 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'50100.17.0001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','Max Mustermann','0000000','77777777','KSK Muster','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'successful','','','','','',NULL,'','','',NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,'','',NULL,'','','Musterstraße','1','Ingolstadt','85057',NULL,'',NULL,'06.10.2017',NULL,NULL,'0','0','no','','PROD0010H','PK.PKQ',NULL,'','','','','01.01.1970','01.01.1970','','PROD0010H',' ','Herr','Max','Mustermann','Musterstraße','1','Ingolstadt','85049','Ingolstadt','01.01.1961','non_commercial','841','123456','',NULL,'max.mustermann@webtest.de','',NULL,'with_supply','',NULL,'Herr','Max','Mustermann','','','','',NULL,'','',NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'01.10.2017','','01.04.2017','','','','','','TAL + Port.','01.10.2017','xxx','H20 (VDSL/Vec)',NULL,NULL,'','','01.04.2017','',NULL,'01.04.2017','','','01.04.2017','01.04.2017','01.04.2017','01.04.2017','Nein','4444444667','','green','green','green','green','green','with_supply','contract_received###<i>01.01.1970</i>, contract_not_cancelled, credit_rating_ok','porting_confirmed###<i>22.09.2010</i>','tal_order_confirmed###<i>19.01.2016</i>','pppoe_done###<i>07.01.2016</i>DSLAM_done_date###<i>07.01.2016</i>terminal_ready###<i>21.12.2015</i>patch_done###<i>23.11.2015</i>connection_activated###<i>06.10.2010</i>','','192.168.1.2','255.255.255.0','7390','','','10A','16','1 (ADSL)','16',NULL,'Ingolstadt','10A','50100',NULL,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','[21.12.2015 jba] FB-Tausch\r\nDaten der alten FB7270:\r\nMAC: -\r\nSNR: A.201.354.00.005.699','','','','','','',NULL,'0841 123456','01.10.2017',NULL,NULL,'','100','01.10.2017','','01.10.2017',NULL,NULL,'06.10.2017','xxx','Email','DTAG',NULL,'Telecom','','01.10.2017','01.10.2017','',NULL,'','',' ','','','','','','https://iport.infoscore.de','erfolgt',1,1,1,1,1,'1/1/1/1',0,'1','','','','','01.10.2017',0,'','',NULL,'','','01.10.2017','','','','',NULL,'','01.10.2017','0841123456','',NULL,'',NULL,'',NULL,'',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'100',NULL,NULL,'10A',NULL,'',NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,'','',NULL,NULL,NULL,'','',NULL,'',NULL,NULL,NULL,NULL,1,'PROD0010H',3,NULL,NULL,'Nein',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'50100.17.0002',NULL,NULL,NULL,NULL,NULL,NULL,'yes',NULL,'','Moritz Mustermann','11111111','88888888','Voba Muster','','',NULL,NULL,NULL,NULL,NULL,NULL,'successful','','','','','',NULL,'','','05.10.2017',NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,'','',NULL,'','','Musterstraße','10','Ingolstadt','85057',NULL,'',NULL,'03.10.2017',NULL,NULL,'1','0','no','','PROD0010H','PK.PKQ',NULL,'empty','','','','01.01.1970','01.01.1970','','PROD0010H','','Herr','Moritz','Mustermann','Musterstraße','10','Ingolstadt','85049','Ingolstadt','01.01.1971','non_commercial','841','654321','','','moritz.mustermann@webtest.de','',NULL,'with_supply','-n-',NULL,'Herr','Moritz','Mustermann','','','','',NULL,'Telekom','Telekom',NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'05.10.2017','','05.10.2017','','','','','','TAL + Port.','05.10.2017','','H18 (VDSL/ADSL)',NULL,NULL,'','','05.10.2017',NULL,NULL,'05.10.2017','','','05.10.2017','05.10.2017','05.10.2017','05.10.2017','','','','green','red','gray','gray','gray','with_supply','contract_received###<i>01.01.1970</i>, contract_cancelled_on###<i>30.11.2014</i>, credit_rating_ok','contract_not_done','talorder_not_done','talorder_and_porting_not_done','','','255.255.255.0','','','','10A','','2 (VDSL)','',NULL,'Ingolstadt','10A','50100',NULL,'','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'','','','','','',NULL,'0841 654321','05.10.2017',NULL,NULL,'','204','01.10.2017','','01.10.2017',NULL,NULL,'01.10.2017','','','DTAG',NULL,'Deutsche Telekom','','01.10.2017','01.10.2017','','','','','','','','','','','https://iport.infoscore.de','erfolgt',1,1,1,1,0,'1/1/1/2',0,'10A','','','','','',0,'','',NULL,'','','01.10.2017','','','','',NULL,'','',NULL,'',NULL,'-n-','','',NULL,'','','',NULL,'',NULL,'','','',NULL,'','','','','','','','','','','','100',NULL,'','','Ja','01.10.2017','xxx','01.10.2017','01.10.2017',NULL,'','','','','','','',NULL,'','','','',NULL,'',NULL,'','','',NULL,'','','','','','','','',NULL,'','',NULL,NULL,NULL,'','',NULL,'','','','',NULL,1,NULL,NULL,NULL,NULL,'Nein',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers_survey_points`
--

DROP TABLE IF EXISTS `customers_survey_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers_survey_points` (
  `customer_id` int(11) NOT NULL,
  `survey_point_id` int(11) NOT NULL,
  PRIMARY KEY (`customer_id`,`survey_point_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers_survey_points`
--

LOCK TABLES `customers_survey_points` WRITE;
/*!40000 ALTER TABLE `customers_survey_points` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers_survey_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `defective_card_ports`
--

DROP TABLE IF EXISTS `defective_card_ports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `defective_card_ports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_id` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `defective_card_ports`
--

LOCK TABLES `defective_card_ports` WRITE;
/*!40000 ALTER TABLE `defective_card_ports` DISABLE KEYS */;
/*!40000 ALTER TABLE `defective_card_ports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `defective_ip_addresses`
--

DROP TABLE IF EXISTS `defective_ip_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `defective_ip_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_range_id` int(11) NOT NULL,
  `value` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `defective_ip_addresses`
--

LOCK TABLES `defective_ip_addresses` WRITE;
/*!40000 ALTER TABLE `defective_ip_addresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `defective_ip_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `defective_lsa_pins`
--

DROP TABLE IF EXISTS `defective_lsa_pins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `defective_lsa_pins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lsa_kvz_partition_id` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `defective_lsa_pins`
--

LOCK TABLES `defective_lsa_pins` WRITE;
/*!40000 ALTER TABLE `defective_lsa_pins` DISABLE KEYS */;
/*!40000 ALTER TABLE `defective_lsa_pins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `export_profiles`
--

DROP TABLE IF EXISTS `export_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `export_profiles` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `profile_name` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile` text COLLATE utf8_unicode_ci NOT NULL,
  `profile_sort` text COLLATE utf8_unicode_ci,
  `profile_where` text COLLATE utf8_unicode_ci,
  `user_id` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `export_profiles`
--

LOCK TABLES `export_profiles` WRITE;
/*!40000 ALTER TABLE `export_profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `export_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ip_addresses`
--

DROP TABLE IF EXISTS `ip_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ip_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `subnetmask` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `gateway` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vlan_id` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rdns` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_range_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip_address` (`ip_address`) USING BTREE,
  KEY `ip_range` (`ip_range_id`) USING BTREE,
  KEY `customer` (`customer_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ip_addresses`
--

LOCK TABLES `ip_addresses` WRITE;
/*!40000 ALTER TABLE `ip_addresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `ip_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ip_ranges`
--

DROP TABLE IF EXISTS `ip_ranges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ip_ranges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_address` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `end_address` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subnetmask` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gateway` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vlan_ID` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pppoe_ip` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pppoe_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pppoe_location` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ip_ranges`
--

LOCK TABLES `ip_ranges` WRITE;
/*!40000 ALTER TABLE `ip_ranges` DISABLE KEYS */;
INSERT INTO `ip_ranges` VALUES (1,'192.168.1.2','192.168.1.254','Ingolstadt Test','255.255.255.0','192.168.1.1','100',NULL,NULL,NULL);
/*!40000 ALTER TABLE `ip_ranges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ip_ranges_networks`
--

DROP TABLE IF EXISTS `ip_ranges_networks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ip_ranges_networks` (
  `network_id` int(11) NOT NULL,
  `ip_range_id` int(11) NOT NULL,
  UNIQUE KEY `ip_range_id` (`network_id`,`ip_range_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ip_ranges_networks`
--

LOCK TABLES `ip_ranges_networks` WRITE;
/*!40000 ALTER TABLE `ip_ranges_networks` DISABLE KEYS */;
INSERT INTO `ip_ranges_networks` VALUES (1,1);
/*!40000 ALTER TABLE `ip_ranges_networks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kv_group`
--

DROP TABLE IF EXISTS `kv_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kv_group` (
  `group_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `OvrList` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPorting` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingCallnum` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingConfirmedTal` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingConnectionInactive` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingContructCancel` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingContructCanceled` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingContructEmailMissing` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingContructEzeMissing` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingContructMissing` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingContructNoAnswer` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingContructNoAnswer3` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingContructNoSupply` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingContructOldMissing` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingContructPhonebook` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingContructProductMissing` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingContructPurtel` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingContructSpecial` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingCustomer` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingFritzbox5490` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingFritzbox6320` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingFritzbox6360` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingFritzbox6490` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingFritzbox7270` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingFritzbox7360` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingFritzbox7390` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingFritzbox7490` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingFritzboxGW_400` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingFritzboxIBG2426A` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingFritzboxNoType` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingNetActive` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingNetDslamCmts` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingNetPatch` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingNetPppoe` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingNewcall` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingOrderedTal` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingOrderTal` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingProspectiveCustomers` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingProspectiveCustomersOnline` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingPurtel` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingRoute` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OvrPortingTalConnectInfo` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `StatList` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `StatNetwork` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `StatActive` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `StatActivity` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CsvExport` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Accounting` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustomerSearch` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustSave` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustVersion` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustAbrogate` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustAccounting` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustConnectingRequest` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustConnection` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustContractBusiness` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustContractOptions` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustContractPrivate` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustMasterData` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustOldContract` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustProcedure` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustProgress` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustPurtel` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustPurtelAccount` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustStatus` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustTal` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustTechConf` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustTechData` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustWbci` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Network` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IpRange` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Location` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LsaKvzPartition` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TechnicsHelp` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MailTemplate` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddAccount` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddGroup` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddAccountSave` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Account` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kv_group`
--

LOCK TABLES `kv_group` WRITE;
/*!40000 ALTER TABLE `kv_group` DISABLE KEYS */;
INSERT INTO `kv_group` VALUES ('admin','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w',NULL,'w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w',NULL,NULL,NULL,'w'),('admin_all','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w',NULL,'w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w'),('default_g','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w',NULL,NULL,NULL,NULL,'w',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `kv_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kv_purtel`
--

DROP TABLE IF EXISTS `kv_purtel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kv_purtel` (
  `account` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `passwd` varbinary(64) DEFAULT NULL,
  `create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kv_purtel`
--

LOCK TABLES `kv_purtel` WRITE;
/*!40000 ALTER TABLE `kv_purtel` DISABLE KEYS */;
/*!40000 ALTER TABLE `kv_purtel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kv_user`
--

DROP TABLE IF EXISTS `kv_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kv_user` (
  `account` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `passwd` varbinary(64) DEFAULT NULL,
  `fullname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_login` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_pw` varbinary(64) DEFAULT NULL,
  `egw_login` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create` datetime DEFAULT NULL,
  PRIMARY KEY (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kv_user`
--

LOCK TABLES `kv_user` WRITE;
/*!40000 ALTER TABLE `kv_user` DISABLE KEYS */;
INSERT INTO `kv_user` VALUES ('jl','u���m0�#xzqЌn�\n�R	�(b/ǜ4:���Pf5\\諂�l; �','Jürgen Lehmann',NULL,NULL,'jl','2017-09-19 23:16:57'),('ssi','���pLxn�1KC�\r�E�3�f�V�rٺ���;z/�4�����ȼcc�','Sven Siebrands',NULL,NULL,NULL,'2017-09-19 23:17:26');
/*!40000 ALTER TABLE `kv_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kv_user_group`
--

DROP TABLE IF EXISTS `kv_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kv_user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kv_user_group`
--

LOCK TABLES `kv_user_group` WRITE;
/*!40000 ALTER TABLE `kv_user_group` DISABLE KEYS */;
INSERT INTO `kv_user_group` VALUES (1,'jl','admin_all'),(56,'ssi','admin_all');
/*!40000 ALTER TABLE `kv_user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kv_user_purtel`
--

DROP TABLE IF EXISTS `kv_user_purtel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kv_user_purtel` (
  `user` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `account` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user`,`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kv_user_purtel`
--

LOCK TABLES `kv_user_purtel` WRITE;
/*!40000 ALTER TABLE `kv_user_purtel` DISABLE KEYS */;
/*!40000 ALTER TABLE `kv_user_purtel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network_id` int(11) DEFAULT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `kvz_prefix` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `cid_suffix_start` int(11) NOT NULL,
  `cid_suffix_end` int(11) NOT NULL,
  `vlan_pppoe` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vlan_iptv` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vlan_acs` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vectoring` tinyint(1) NOT NULL,
  `logon_port` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dslam_ip_adress` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logon_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logon_pw` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dslam_conf` tinyint(1) NOT NULL,
  `dslam_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vlan_pppoe_local` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vlan_iptv_local` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vlan_acs_local` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `acs_type` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dslam_ip_real` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dslam_router` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `netz` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vlan_dcn` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prozessor` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dslam_software_release` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pppoe_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `acs_qos_profile` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_ont_firmware_release` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_17E64ABA34128B91` (`network_id`),
  CONSTRAINT `FK_17E64ABA34128B91` FOREIGN KEY (`network_id`) REFERENCES `networks` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (1,1,'Ingolstadt Test','01-K2',1,9999,NULL,NULL,NULL,1,NULL,NULL,'admin','password',1,'ALU','7','8','9','purtel',NULL,NULL,'/24',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lsa_kvz_partitions`
--

DROP TABLE IF EXISTS `lsa_kvz_partitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lsa_kvz_partitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `kvz_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `pin_amount` int(11) NOT NULL,
  `dpbo` tinyint(4) DEFAULT NULL,
  `default_VLAN` int(11) unsigned DEFAULT NULL,
  `VLAN1` int(11) unsigned DEFAULT NULL,
  `VLAN2` int(11) unsigned DEFAULT NULL,
  `VLAN3` int(11) unsigned DEFAULT NULL,
  `esel_wert` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Telecom ESEL Wert',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lsa_kvz_partitions`
--

LOCK TABLES `lsa_kvz_partitions` WRITE;
/*!40000 ALTER TABLE `lsa_kvz_partitions` DISABLE KEYS */;
INSERT INTO `lsa_kvz_partitions` VALUES (1,2,'10A',200,1,100,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `lsa_kvz_partitions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_group`
--

DROP TABLE IF EXISTS `mail_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_group`
--

LOCK TABLES `mail_group` WRITE;
/*!40000 ALTER TABLE `mail_group` DISABLE KEYS */;
INSERT INTO `mail_group` VALUES (1,'Information'),(2,'Vertragseingang'),(3,'Vertragsbearbeitung'),(4,'Portierung'),(5,'Anschaltung'),(6,'Produktänderung'),(7,'Umzug'),(8,'Kündigung'),(9,'Störung'),(10,'Aktionen');
/*!40000 ALTER TABLE `mail_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_templates`
--

DROP TABLE IF EXISTS `mail_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_templates` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `mail_id` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_templates`
--

LOCK TABLES `mail_templates` WRITE;
/*!40000 ALTER TABLE `mail_templates` DISABLE KEYS */;
INSERT INTO `mail_templates` VALUES (1,0,NULL,'leer','','Guten Tag ###ANREDE### ###NAME###,\n\n\nSie erreichen uns \r\nMontag bis Freitag von 9:00 bis 17:00 Uhr  *  Tel. 07191 / 3668 – 600,\r\nper Email unter kundenbetreuung@wisotel.com\r\noder besuchen Sie uns in Backnang, Kuchengrund 8 (Gewerbegebiet Süd).\r\n\r\nWir helfen Ihnen gerne weiter.\r\n\r\nMit freundlichen Grüßen\r\n\r\nIhre WiSoTEL Kundenbetreuung \r\n-------------------------------------------------------------- \r\nWiSoTEL GmbH \r\nGesellschaft für Telekommunikationslösungen \r\nKuchengrund 8 | 71522 Backnang | Deutschland\r\n\r\nTelefon:    +49 (0) 7191 3668-600 \r\nFax:        +49 (0) 7191 3668-999\r\n \r\nE-Mail:     kundenbetreuung@wisotel.com \r\ninfo:       https://www.wisotel.com \r\n---------------------------------------------------------------- \r\nGeschäftsführung: Thomas Berkel \r\nAmtsgericht Stuttgart | HRB 72 4829 | Ust-IdNr.: DE 258275665\r\n\r\nDiese E-Mail enthält vertrauliche und rechtlich geschützte Informationen. Das unerlaubte Kopieren sowie \r\ndie unbefugte Weitergabe dieser E-Mail und der darin enthaltenen Informationen sind nicht gestattet.\r\n----------------------------------------------------------------');
/*!40000 ALTER TABLE `mail_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `networks`
--

DROP TABLE IF EXISTS `networks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `networks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_string` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `partner` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `network_ready` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `area_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fix_cost` decimal(7,2) DEFAULT NULL,
  `var_cost` decimal(7,2) DEFAULT NULL,
  `var_percent` decimal(7,2) DEFAULT NULL,
  `no_stat` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `networks`
--

LOCK TABLES `networks` WRITE;
/*!40000 ALTER TABLE `networks` DISABLE KEYS */;
INSERT INTO `networks` VALUES (1,'30000','BK - Lerchenäcker','','Oktober 2009','07191',NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `networks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `networks_products`
--

DROP TABLE IF EXISTS `networks_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `networks_products` (
  `id` int(11) NOT NULL,
  `network_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `var_cost` float(7,2) DEFAULT NULL,
  `var_percent` float(7,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `networks_products`
--

LOCK TABLES `networks_products` WRITE;
/*!40000 ALTER TABLE `networks_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `networks_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `optionen`
--

DROP TABLE IF EXISTS `optionen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `optionen` (
  `option` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `option_bezeichnung` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Preis_Netto` decimal(7,2) DEFAULT NULL,
  `Preis_Brutto` decimal(7,2) DEFAULT NULL,
  `purtel_product_id` int(11) DEFAULT NULL,
  `contract_period` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`option`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `optionen`
--

LOCK TABLES `optionen` WRITE;
/*!40000 ALTER TABLE `optionen` DISABLE KEYS */;
INSERT INTO `optionen` VALUES ('OPOPOP','Highspeed-Internet SOFORT',50.00,59.50,NULL,NULL,0);
/*!40000 ALTER TABLE `optionen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partner`
--

DROP TABLE IF EXISTS `partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partner`
--

LOCK TABLES `partner` WRITE;
/*!40000 ALTER TABLE `partner` DISABLE KEYS */;
/*!40000 ALTER TABLE `partner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `partner` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `partner_id` int(11) DEFAULT NULL,
  `partner_identitaet` int(5) NOT NULL,
  `network_id` int(11) DEFAULT NULL,
  `network` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fix_cost` int(10) DEFAULT NULL,
  `datadsl8_cost` float(4,2) DEFAULT '5.03',
  `datadsl16_cost` float(4,2) DEFAULT '5.87',
  `datadsl25_cost` float(4,2) DEFAULT '6.71',
  `datadsl50_cost` float(4,2) DEFAULT '7.55',
  `basicdsl8_cost` float(4,2) DEFAULT '5.87',
  `basicdsl16_cost` float(4,2) DEFAULT '6.71',
  `basicdsl25_cost` float(4,2) DEFAULT '7.55',
  `basicdsl50_cost` float(4,2) DEFAULT '8.39',
  `datagf15_cost` float(4,2) DEFAULT '5.03',
  `datagf35_cost` float(4,2) DEFAULT '5.87',
  `datagf50_cost` float(4,2) DEFAULT '6.71',
  `datagf75_cost` float(4,2) DEFAULT '7.55',
  `datagf100_cost` float(4,2) DEFAULT '8.39',
  `comfgf15_cost` float(4,2) DEFAULT '5.87',
  `comfgf25_cost` float(4,2) DEFAULT '5.03',
  `comfgf35_cost` float(4,2) DEFAULT '6.71',
  `comfgf50_cost` float(4,2) DEFAULT '7.55',
  `comfgf75_cost` float(4,2) DEFAULT '8.39',
  `comfgf100_cost` float(4,2) DEFAULT '9.24',
  `datacable10_cost` float(4,2) DEFAULT '5.03',
  `datacable30_cost` float(4,2) DEFAULT '5.87',
  `datacable32_cost` float(4,2) DEFAULT '6.71',
  `datacable40_cost` float(4,2) DEFAULT '7.55',
  `datacable50_cost` float(4,2) DEFAULT '8.39',
  `basiccable10_cost` float(4,2) DEFAULT '5.87',
  `basiccable30_cost` float(4,2) DEFAULT '6.71',
  `basiccable32_cost` float(4,2) DEFAULT '7.55',
  `basiccable40_cost` float(4,2) DEFAULT '7.55',
  `basiccable50_cost` float(4,2) DEFAULT '8.39',
  `vitrodsl6_cost` float(4,2) DEFAULT '5.87',
  `vitrodsl16_cost` float(4,2) DEFAULT '5.87',
  `sdsl4_cost` float(6,2) DEFAULT '40.34',
  `sdsl10_cost` float(6,2) DEFAULT '80.19',
  `extraltg_cost` float(4,2) DEFAULT '0.64',
  `asymm25_cost` float(4,2) DEFAULT NULL,
  `asymm50_cost` float(4,2) DEFAULT NULL,
  `sharedsymm10_cost` float(4,2) DEFAULT '39.80',
  `sharedsymm15_cost` float(4,2) DEFAULT '59.80',
  `sharedsymm20_cost` float(4,2) DEFAULT NULL,
  `sharedsymm25_cost` float(4,2) DEFAULT NULL,
  `exclsymm5_cost` float(4,2) DEFAULT NULL,
  `exclsymm30_cost` float(4,2) DEFAULT NULL,
  `exclsymm10_cost` float(4,2) DEFAULT NULL,
  `exclsymm20_cost` float(4,2) DEFAULT NULL,
  `newCus_factor` float(4,2) DEFAULT '0.50',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partners`
--

LOCK TABLES `partners` WRITE;
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prod_group`
--

DROP TABLE IF EXISTS `prod_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prod_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupname` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prod_group`
--

LOCK TABLES `prod_group` WRITE;
/*!40000 ALTER TABLE `prod_group` DISABLE KEYS */;
INSERT INTO `prod_group` VALUES (100,'PK Allgemein Neu, Standardprodukte');
/*!40000 ALTER TABLE `prod_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `identifier` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `default_price_net` decimal(12,5) DEFAULT NULL,
  `default_price_gross` decimal(12,5) DEFAULT NULL,
  `item_category` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_product_id` int(11) DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B3BA5A5A772E836A` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produkte`
--

DROP TABLE IF EXISTS `produkte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produkte` (
  `produkt` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `produkt_index` int(11) DEFAULT NULL,
  `gpon_download` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gpon_upload` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ethernet_up_down` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `down` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `up` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `produkt_bezeichnung` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Preis_Netto` decimal(7,2) DEFAULT NULL,
  `Preis_Brutto` decimal(7,2) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL,
  `card_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_497DBEB31B938EA5` (`produkt`),
  KEY `IDX_497DBEB3925606E5` (`card_type_id`),
  KEY `IDX_497DBEB3FE54D947` (`group_id`),
  CONSTRAINT `FK_497DBEB3925606E5` FOREIGN KEY (`card_type_id`) REFERENCES `card_types` (`id`),
  CONSTRAINT `FK_497DBEB3FE54D947` FOREIGN KEY (`group_id`) REFERENCES `prod_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produkte`
--

LOCK TABLES `produkte` WRITE;
/*!40000 ALTER TABLE `produkte` DISABLE KEYS */;
INSERT INTO `produkte` VALUES ('PROD0010H',100,50,'D_PROD0010','U_PROD0010','PROD0010','10500','4500','Highspeed 10/4 Mbit GKQ',50.00,59.50,1,0,NULL);
/*!40000 ALTER TABLE `produkte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purtel_account`
--

DROP TABLE IF EXISTS `purtel_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purtel_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purtel_login` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purtel_password` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `master` tinyint(4) DEFAULT NULL,
  `porting` tinyint(4) DEFAULT NULL,
  `nummer` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nummernblock` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `typ` int(11) NOT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `phone_book_information` tinyint(1) DEFAULT NULL,
  `phone_book_digital_media` tinyint(1) DEFAULT NULL,
  `phone_book_inverse_search` tinyint(1) DEFAULT NULL,
  `itemized_bill` tinyint(1) DEFAULT NULL,
  `itemized_bill_anonymized` tinyint(1) DEFAULT NULL,
  `itemized_bill_shorted` tinyint(1) DEFAULT NULL,
  `area_code` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FB40EE3BBFF2A482` (`cust_id`),
  KEY `IDX_FB40EE3BCAF67037` (`purtel_login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purtel_account`
--

LOCK TABLES `purtel_account` WRITE;
/*!40000 ALTER TABLE `purtel_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `purtel_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purtel_data_records`
--

DROP TABLE IF EXISTS `purtel_data_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purtel_data_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `time` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `transaction` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `done` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purtel_data_records`
--

LOCK TABLES `purtel_data_records` WRITE;
/*!40000 ALTER TABLE `purtel_data_records` DISABLE KEYS */;
/*!40000 ALTER TABLE `purtel_data_records` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `right_area`
--

DROP TABLE IF EXISTS `right_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `right_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varbinary(128) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `right_area`
--

LOCK TABLES `right_area` WRITE;
/*!40000 ALTER TABLE `right_area` DISABLE KEYS */;
INSERT INTO `right_area` VALUES (1,'OvrList','Übersicht',1,1),(2,'OvrPorting','zu portierende Kunden',1,2),(3,'OvrPortingCallnum','Rufnummern schalten',1,3),(4,'OvrPortingConfirmedTal','Bestätigte TALs in den nächsten 2 Wochen',1,4),(5,'OvrPortingConnectionInactive','inaktive Kunden',1,5),(6,'OvrPortingContructCancel','gekündigte Verträge',1,6),(7,'OvrPortingContructCanceled','gekündigte Verträge',1,7),(8,'OvrPortingContructEmailMissing','fehlende E-Mail-Adressen',1,8),(9,'OvrPortingContructEzeMissing','fehlende Einzugsermächtigung',1,9),(10,'OvrPortingContructMissing','Interessenten, die noch nicht informiert wurden',1,10),(11,'OvrPortingContructNoAnswer','Interessenten, ohne Rückmeldung (in den letzten 3 Monaten)',1,11),(12,'OvrPortingContructNoAnswer3','Interessenten, ohne Rückmeldung (mehr als 3 Monate)',1,12),(13,'OvrPortingContructNoSupply','Kunden, nicht versorgbar',1,13),(14,'OvrPortingContructOldMissing','Altvertragsdaten fehlen',1,14),(15,'OvrPortingContructPhonebook','Telefonbucheinträge',1,15),(16,'OvrPortingContructProductMissing','fehlende Produktangaben',1,16),(17,'OvrPortingContructPurtel','Purtel Vorabprodukte',1,17),(18,'OvrPortingContructSpecial','Sonderkonditionen',1,18),(19,'OvrPortingCustomer','Portierung beim Kunde',1,19),(20,'OvrPortingFritzbox5490','5490 FRITZ!Boxen für die nächten drei Monate',1,20),(21,'OvrPortingFritzbox6320','6320 FRITZ!Boxen für die nächten drei Monate',1,21),(22,'OvrPortingFritzbox6360','6360 FRITZ!Boxen für die nächten drei Monate',1,22),(23,'OvrPortingFritzbox6490','6490 FRITZ!Boxen für die nächten drei Monate',1,23),(24,'OvrPortingFritzbox7270','7270 FRITZ!Boxen für die nächten drei Monate',1,24),(25,'OvrPortingFritzbox7360','7360 FRITZ!Boxen für die nächten drei Monate',1,25),(26,'OvrPortingFritzbox7390','7390 FRITZ!Boxen für die nächten drei Monate',1,26),(27,'OvrPortingFritzbox7490','7490 FRITZ!Boxen für die nächten drei Monate',1,27),(28,'OvrPortingFritzboxGW_400','Gateway 400 für die nächten drei Monate',1,28),(29,'OvrPortingFritzboxIBG2426A','INNBOX G2426A für die nächten drei Monate',1,29),(30,'OvrPortingFritzboxNoType','NoType FRITZ!Boxen für die nächten drei Monate',1,30),(31,'OvrPortingNetActive','Aktivschaltungen',1,31),(32,'OvrPortingNetDslamCmts','DSLAM / CMTS einrichten',1,32),(33,'OvrPortingNetPatch','Patchen',1,33),(34,'OvrPortingNetPppoe','PPPoE-Server einrichten',1,34),(35,'OvrPortingNewcall','Neue Rufnummer(n)',1,35),(36,'OvrPortingOrderedTal','TALs, die bereits bestellt wurden',1,36),(37,'OvrPortingOrderTal','TALs, die zu bestellen sind',1,37),(38,'OvrPortingProspectiveCustomers','Interessenten',1,38),(39,'OvrPortingProspectiveCustomersOnline','Eingegangene Onlineverträge',1,39),(40,'OvrPortingPurtel','Portierung in Purtel',1,40),(41,'OvrPortingRoute','Umroutungen',1,41),(42,'OvrPortingTalConnectInfo','Kunden, die noch nicht informiert wurden',1,42),(43,'StatList','Statistik gesamt',2,1),(44,'StatNetwork','Statistik Netz',2,2),(45,'StatActive','Aktivschaltungen',2,3),(46,'CsvExport','CSV Export',3,1),(47,'Accounting','Buchhaltung Purtel',4,1),(48,'CustomerSearch','Nachname, Firmenname oder Kundennummer suchen:',5,1),(49,'CustSave','Speichern',6,1),(50,'CustVersion','Version',6,2),(51,'CustAbrogate','Kündigung',6,3),(52,'CustAccounting','Kontodaten',6,4),(53,'CustConnectingRequest','Schaltauftrag',6,5),(54,'CustConnection','Anschluss',6,6),(55,'CustContractBusiness','Vertragsdaten (Geschäftskunden)',6,7),(56,'CustContractOptions','Produkt Optionen',6,8),(57,'CustContractPrivate','Vertragsdaten (Privatkunden)',6,9),(58,'CustMasterData','Kundendaten (Stammdaten)',6,10),(59,'CustOldContract','Altvertrag',6,11),(60,'CustProcedure','Ablauf',6,12),(61,'CustProgress','Verlauf',6,13),(62,'CustPurtel','Purtel / Portierung',6,14),(63,'CustPurtelAccount','Purtel Zugänge',6,15),(64,'CustStatus','Status',6,16),(65,'CustTal','TAL',6,17),(66,'CustTechConf','Technische Konfiguration',6,18),(67,'CustTechData','Technische Daten',6,19),(68,'CustWbci','WBCI',6,20),(69,'Network','IP-Blöcke',7,1),(70,'IpRange','IP-Range',8,1),(71,'Location','LSA-KVz Zuordnungen',9,1),(72,'LsaKvzPartition','LSA-KVz Zuordnung',10,1),(73,'technicsHelp','Technik Hilfe',11,1),(74,'Account','Anmeldung Verwalten',12,1),(75,'MailTemplate','Mail-Templates',13,1),(76,'AddAccount','Benutzer hinzufügen/ändern',13,1),(77,'AddGroup','Gruppen hinzufügen/ändern',13,2);
/*!40000 ALTER TABLE `right_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `right_group`
--

DROP TABLE IF EXISTS `right_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `right_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `right_group`
--

LOCK TABLES `right_group` WRITE;
/*!40000 ALTER TABLE `right_group` DISABLE KEYS */;
INSERT INTO `right_group` VALUES (1,'overview','Übersicht',1),(2,'statistics','Statistik',2),(3,'csvExport','CSV Export',3),(4,'AccountingHead','Buchhaltung',4),(5,'customerSearch','Kundensuche',5),(6,'customer','Kunden',6),(7,'network','Netzwerk',7),(8,'iprange','IP-Blöcke',8),(9,'location','Standorte',9),(10,'lsaKvzPartition','LSA KVz',10),(11,'technics','Technik Hilfe',11),(12,'AccountHead','Anmeldung',12),(13,'administration','Administration',13);
/*!40000 ALTER TABLE `right_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `survey_points`
--

DROP TABLE IF EXISTS `survey_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `survey_points` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `table_set_id` int(11) NOT NULL,
  `sort_field` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `survey_points`
--

LOCK TABLES `survey_points` WRITE;
/*!40000 ALTER TABLE `survey_points` DISABLE KEYS */;
INSERT INTO `survey_points` VALUES (1,'registrationsNoAnswer',56,NULL),(2,'routingActiveDate',55,NULL),(3,'registrations',54,NULL),(4,'newPhoneNumber',52,NULL),(5,'specialConditions',53,NULL),(6,'emailAddress',52,NULL),(7,'bankAccountData',51,NULL),(8,'productName',52,NULL),(9,'oldContractData',52,NULL),(10,'purtelProductAdvanced',38,NULL),(11,'activationNotDone',39,NULL),(12,'talsAckn',46,'tal_order_ack_date'),(13,'pppoeConfig',40,'tal_order_ack_date'),(14,'dslamConfig',40,'tal_order_ack_date'),(15,'patched',40,'tal_order_ack_date'),(16,'terminalsToSend',42,'tal_order_ack_date'),(17,'needBoxes6320',40,'tal_order_ack_date'),(18,'needBoxes6360',40,'tal_order_ack_date'),(19,'needBoxes7360',40,'tal_order_ack_date'),(20,'needBoxes7390',40,'tal_order_ack_date'),(21,'needBoxes7270',40,'tal_order_ack_date'),(22,'needBoxesUnknown',40,'tal_order_ack_date'),(23,'toBeInformed',45,'tal_order_ack_date'),(24,'talsAreOrderd',47,'tal_order_date'),(25,'addPhoneLine',43,NULL),(26,'portingInPurtel',49,'ventelo_purtel_enter_date'),(27,'talsToBeOrderd',48,'stati_port_confirm_date'),(28,'portingAway',50,'ventelo_port_letter_date'),(29,'portingNextWeek',17,'exp_date_phone'),(30,'contractCancelled',58,NULL);
/*!40000 ALTER TABLE `survey_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `table_sets`
--

DROP TABLE IF EXISTS `table_sets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `table_sets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `fields` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `table_sets`
--

LOCK TABLES `table_sets` WRITE;
/*!40000 ALTER TABLE `table_sets` DISABLE KEYS */;
INSERT INTO `table_sets` VALUES (1,'masterdata','clientid###title###firstname###lastname###company###connection_street###connection_streetno###zipcode###connection_city###district###birthday###clienttype###phoneareacode###phonenumber###fax###mobilephone###emailaddress###comment'),(2,'contractdata','clientid###emailaddress###lastname###clienttype###contract_sent_date###contract_version###contract_received_date###recruited_by###connection_activation_date###productname###add_phone_lines###add_phone_nos###purtel_product###paper_bill###call_data_record###connection_fee_height###add_charge_terminal###direct_debit_allowed###credit_rating_ok###bank_account_holder_firstname###bank_account_holder_lastname###bank_account_number###bank_account_bic###bank_account_bankname###bank_account_iban###contract_comment'),(3,'portingdata','clientid###emailaddress###lastname###firstname###city###district###connection_activation_date###tal_dtag_assignment_no###purtel_customer_reg_date###ventelo_port_letter_date###ventelo_confirmation_date###ventelo_purtel_enter_date###ventelo_port_wish_date###stati_port_confirm_date###customer_connect_info_date###purtel_login###purtel_passwort###tal_order###tal_product###dtag_line###tal_order_date###tal_releasing_operator###tal_dtag_revisor###tal_order_ack_date###stati_oldcontract_comment'),(4,'oldcontract','clientid######emailaddress###lastname###oldcontract_title###oldcontract_firstname###oldcontract_lastname###new_or_ported_phonenumbers###oldcontract_comment###old_provider_comment###oldcontract_internet_provider###oldcontract_internet_clientno###exp_date_int###special_termination_internet###oldcontract_internet_client_cancelled###oldcontract_phone_provider###oldcontract_phone_clientno###exp_date_phone###special_termination_phone###oldcontract_phone_client_cancelled'),(5,'techdata','tal_order_ack_date###clientid###productname###kvz_name###kvz_addition###line_identifier###location_id###terminal_type###dtag_line###ip_address###lsa_pin###card_id###pppoe_config_date###dslam_arranged_date###patch_date###firstname###lastname###street###streetno###city###district###remote_password###remote_login###purtel_login###purtel_passwort###connection_activation_date###terminal_serialno###tal_ordered_for_date###mac_address'),(6,'pppoelist','clientid###lastname###ip_address###subnetmask###password###purtel_product###dslam_card_no###dslam_port###lsa_pin###patch_date###pppoe_config_date'),(7,'credit_rating','clientid###credit_rating_ok###credit_rating_date###firstname###lastname###company###street###streetno###zipcode###city###birthday'),(8,'statilist','clientid###lastname###firstname###contract_sent_date###contract_received_date###network_id###ventelo_purtel_enter_date###stati_port_confirm_date###tal_order_date###tal_order_ack_date###pppoe_config_date###terminal_ready###patch_date###connection_activation_date'),(9,'portingdatalite','clientid###emailaddress###lastname###firstname###purtel_customer_reg_date###ventelo_port_letter_date###ventelo_confirmation_date###ventelo_purtel_enter_date###ventelo_port_wish_date###stati_port_confirm_date###customer_connect_info_date###purtel_login###tal_order_date###tal_order_ack_date###connection_activation_date###stati_oldcontract_comment'),(10,'masterdatalite','clientid###emailaddress###purtel_login###lastname###firstname###company###street###streetno###zipcode###city###phoneareacode###phonenumber###emailaddress'),(11,'tododata','clientid###lastname###firstname###todo'),(12,'oldaddress','clientid###title###firstname###lastname###company###street###streetno###zipcode###city###district###birthday###clienttype###phoneareacode###phonenumber###fax###mobilephone###emailaddress###comment###moved###oldstreet###oldstreetno###oldzipcode###oldcity###olddistrict'),(13,'activeclients','purtel_login###clientid###emailaddress###connection_activation_date###firstname###lastname###connection_street###connection_streetno###connection_city###district###connection_activation_date###productname###purtel_product###purtelproduct_advanced###final_purtelproduct_date###add_phone_lines###wisocontract_canceled_date###exp_date_phone###prospect_supply_status###ip_address###reached_downstream'),(14,'prospects','clientid###emailaddress###title###firstname###lastname###street###streetno###city###zipcode###emailaddress###purtel_login###purtel_passwort'),(15,'sofortdsl','clientid###title###firstname###lastname###connection_street###connection_streetno###connection_city###district###zipcode###emailaddress###sofortdsl###connection_activation_date###stati_port_confirm_date'),(17,'to_be_ported','clientid###firstname###lastname###connection_street###connection_streetno###zipcode###connection_city###district###ventelo_port_letter_date###connection_activation_date###stati_port_confirm_date###exp_date_phone###notice_period_phone_int###notice_period_phone_type###exp_date_int###notice_period_internet_int###notice_period_internet_type###wisocontract_canceled_date'),(18,'customers','clientid###clienttype###title###firstname###lastname###company###connection_street###connection_streetno###connection_city###district###zipcode###emailaddress###purtel_login###purtel_passwort###connection_activation_date###wisocontract_canceled_date###terminal_type###terminal_serialno###tal_ordered_for_date###tal_order_ack_date###ventelo_purtel_enter_date###ventelo_port_wish_date###stati_port_confirm_date###exp_date_int###exp_date_phone###terminal_ready###productname'),(19,'are_ported','clientid###title###firstname###lastname###connection_street###connection_streetno###connection_city###district###purtel_login###purtel_passwort###stati_port_confirm_date###tal_order_ack_date###connection_activation_date###purtelproduct_advanced###routing_active'),(20,'contract_missing','clientid###title###firstname###lastname###street###streetno###city###zipcode###phoneareacode###phonenumber###emailaddress'),(21,'routing_active','clientid###firstname###lastname###connection_city###district###emailaddress###phoneareacode###phonenumber###mobilephone'),(22,'recruited','clientid###title###firstname###lastname###connection_street###connection_streetno###connection_city###zipcode###emailaddress###recruited_by'),(23,'tal_order','firstname###lastname###connection_city###district###connection_street###connection_streetno###tal_order_date###tal_ordered_for_date###tal_order_ack_date###customer_connect_info_date###terminal_type###terminal_ready###id'),(24,'tal_acknowldged','firstname###lastname###connection_city###district###connection_street###connection_streetno###tal_ordered_for_date###tal_order_ack_date###connection_activation_date###terminal_type###terminal_ready###id###dslam_arranged_date###patch_date###service_technician###company###pppoe_config_date'),(25,'not_informed','clientid###firstname###lastname###connection_city###district###connection_street###connection_streetno###tal_order_ack_date###tal_order_work###connection_activation_date'),(26,'dtagbluckage_sended','clientid###firstname###lastname###connection_city###connection_activation_date###dtagbluckage_sended_date###dtagbluckage_fullfiled_date'),(27,'check_activation','clientid###firstname###lastname###connection_city###district###connection_activation_date###tal_dtag_assignment_no###tal_order_work###dtag_line'),(28,'fb_advanced','clientid###firstname###lastname###city###district###fb_vorab###connection_activation_date'),(29,'cancel','firstname###lastname###connection_city###district'),(30,'specialconditions','clientid###firstname###lastname###connection_city###district###productname###connection_activation_date###special_conditions_till###special_conditions_euro'),(31,'contract_cancelled','clientid###firstname###lastname###connection_city###district###connection_street###connection_streetno###emailaddress###phoneareacode###phonenumber###wisocontract_canceled_date'),(32,'customer_has_port','clientid###firstname###lastname###ventelo_port_letter_date###ventelo_confirmation_date###ventelo_purtel_enter_date###ventelo_port_wish_date###stati_port_confirm_date'),(33,'active-email','clientid###firstname###lastname###city###district###street###streetno###emailaddress###connection_activation_date###wisocontract_canceled_date###mac_address'),(34,'paperbill','clientid###firstname###lastname###paper_bill###bank_account_holder_firstname###bank_account_holder_lastname###bank_account_number###bank_account_bankname'),(36,'info_fb','clientid###title###firstname###lastname###connection_street###connection_streetno###zipcode###connection_city###tal_order_ack_date###phoneareacode###phonenumber###purtel_login###purtel_passwort###terminal_type###terminal_serialno###password###remote_password'),(37,'portwish','firstname###lastname###street###streetno###city###district###phoneareacode###phonenumber###ventelo_port_wish_date###stati_port_confirm_date###productname'),(38,'purtelProductAdvanced','clientid###firstname###lastname###productname###purtel_product###connection_activation_date'),(39,'activationNotDone','clientid###firstname###lastname###connection_activation_date'),(40,'needBoxesUnknown','clientid###firstname###lastname###connection_city###district###stati_port_confirm_date###tal_ordered_for_date###tal_order_ack_date###sofortdsl###terminal_type'),(41,'needBoxes2','clientid###firstname###lastname###city###stati_port_confirm_date###sofortdsl###terminal_type'),(42,'terminalsToSend','clientid###firstname###lastname###connection_street###connection_streetno###connection_zipcode###connection_city###district###tal_order_ack_date###terminal_type###terminal_ready'),(43,'addPhoneLine','clientid###firstname###lastname###sofortdsl###stati_port_confirm_date###tal_order_ack_date###phone_number_added'),(44,'needBoxes','clientid###firstname###lastname###city###tal_ordered_for_date###tal_order_ack_date###terminal_type'),(45,'toBeInformed','clientid###firstname###lastname###tal_order_date###tal_ordered_for_date###tal_order_ack_date###tvs###tal_order_work###terminal_type###terminal_ready'),(46,'talsAckn','clientid###firstname###lastname###tal_order_date###tal_ordered_for_date###tal_order_ack_date###customer_connect_info_date###terminal_type###terminal_ready'),(47,'talsAreOrderd','clientid###firstname###lastname###tal_order_date###tal_ordered_for_date'),(48,'talsToBeOrderd','clientid###firstname###lastname###city###district###sofortdsl###oldcontract_exists###ventelo_port_wish_date###stati_port_confirm_date'),(49,'portingInPurtel','clientid###firstname###lastname###ventelo_purtel_enter_date###ventelo_port_wish_date'),(50,'portingAway','clientid###firstname###lastname###phoneareacode###phonenumber###ventelo_port_letter_date###ventelo_port_wish_date###notice_period_phone_int###notice_period_phone_type'),(51,'bankAccountDate','clientid###firstname###lastname###bank_account_holder_firstname###bank_account_holder_lastname###bank_account_number###bank_account_bic###bank_account_bankname'),(52,'contractMissing','clientid###firstname###lastname###connection_city###district'),(53,'specialConditions','clientid###firstname###lastname###connection_city###district###special_conditions_till###special_conditions_euro'),(54,'registrations','clientid###firstname###lastname###connection_street###connection_streetno###connection_city###registration_date###reg_answer_date###contract_sent_date'),(55,'routingActiveDate','firstname###lastname###routing_active_date'),(56,'registrationAnswer','clientid###firstname###lastname###connection_street###connection_streetno###connection_city###registration_date###reg_answer_date###contract_sent_date'),(57,'KHLE','clientid###productname###kvz_addition###lsa_pin###line_identifier###location_id###terminal_type###dtag_line###ip_address###reached_downstream###card_id###pppoe_config_date###dslam_arranged_date###patch_date###tal_ordered_for_date###tal_order_ack_date###connection_activation_date###terminal_type###terminal_ready###remote_password###remote_login###connection_activation_date###firstname###lastname###street###streetno###district###terminal_type###mac_address###id###lsa_kvz_partition_id###connection_activation_date'),(58,'contractCancelled','clientid###firstname###lastname###wisocontract_canceled_date'),(59,'phonebook_entry','clientid###firstname###lastname###connection_city###district###connection_activation_date###telefonbuch_eintrag###phoneentry_done_date'),(60,'telefonbuch','clientid###firstname###lastname###city###district###connection_activation_date###telefonbuch_eintrag###phoneentry_done_date'),(61,'tech_nonn','clientid###lastname###firstname###connection_street###connection_streetno###zipcode###connection_city###district###mac_address###ip_address###connection_activation_date###productname');
/*!40000 ALTER TABLE `table_sets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_rates`
--

DROP TABLE IF EXISTS `tax_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` decimal(4,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_rates`
--

LOCK TABLES `tax_rates` WRITE;
/*!40000 ALTER TABLE `tax_rates` DISABLE KEYS */;
/*!40000 ALTER TABLE `tax_rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `technik__k_hs`
--

DROP TABLE IF EXISTS `technik__k_hs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `technik__k_hs` (
  `index` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Kommando` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Netz` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Error` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `technik__k_hs`
--

LOCK TABLES `technik__k_hs` WRITE;
/*!40000 ALTER TABLE `technik__k_hs` DISABLE KEYS */;
/*!40000 ALTER TABLE `technik__k_hs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `comment` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
INSERT INTO `test` VALUES (1,'umu umu','Vieleicht geht\'s'),(2,'2. Versuch','Vieleicht geht\'s immernoch'),(3,'2. Versuch','Vieleicht geht\'s immernoch'),(4,'2. Versuch','Vieleicht geht\'s immernoch'),(5,'DEPP',NULL),(6,'DEPP',NULL),(7,'DEPP',NULL);
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wbci_ekp`
--

DROP TABLE IF EXISTS `wbci_ekp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wbci_ekp` (
  `id` int(11) NOT NULL DEFAULT '0',
  `bezeichnung` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `carriercode` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wbci_ekp`
--

LOCK TABLES `wbci_ekp` WRITE;
/*!40000 ALTER TABLE `wbci_ekp` DISABLE KEYS */;
INSERT INTO `wbci_ekp` VALUES (1,'PURtel','DEU.PURTEL'),(2,'PURtel Test','DEU.PURTEL'),(3,'OR-Network','DEU.ORNET'),(4,'Deutsche Telekom','DEU.DTAG'),(5,'WiSoTEL','DEU.WISOTL'),(6,'amisol','DEU.AMISOL'),(7,'Telefónica Deutschland','DEU.TEFGER'),(8,'1 und 1 Versatel','DEU.1UND1');
/*!40000 ALTER TABLE `wbci_ekp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wbci_gf`
--

DROP TABLE IF EXISTS `wbci_gf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wbci_gf` (
  `gf` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `gf_beschreibung` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`gf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wbci_gf`
--

LOCK TABLES `wbci_gf` WRITE;
/*!40000 ALTER TABLE `wbci_gf` DISABLE KEYS */;
INSERT INTO `wbci_gf` VALUES ('STR-AEN','Storno mit Änderung des Anbieterwechsels'),('STR-AUF','Storno mit Aufhebung des Anbieterwechsels'),('TVS-VA','Terminverschiebung'),('VA-KUE-MRN','Kündigung mit Rufnummernportierung mit Auskunft über die technische Ressource'),('VA-KUE-ORN','Kündigung ohne Rufnummernportierung mit Auskunft über die technische Ressource'),('VA-RRNP','Reine Rufnummernportierung ohne Kündigung des Anschlusses');
/*!40000 ALTER TABLE `wbci_gf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wbci_message`
--

DROP TABLE IF EXISTS `wbci_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wbci_message` (
  `message` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `message_text` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wbci_message`
--

LOCK TABLES `wbci_message` WRITE;
/*!40000 ALTER TABLE `wbci_message` DISABLE KEYS */;
INSERT INTO `wbci_message` VALUES ('ABBM','Abbruchmeldung. Wird zum Abbruch des Vorgangs vor Versand/Erhalt von AKM-TR verwendet'),('ABBM-TR','Ankündigung der Übernahme der technischen Ressource. Wird vom aufnehmenden Partner an den abgebenden Partner versendet'),('AKM-TR','Ankündigung der Übernahme der technischen Ressource. Wird vom aufnehmenden Partner an den abgebenden Partner versendet'),('ERLM','Erledigt-Meldung'),('RUEM-VA','Vorabstimmungsantwort des Partners, der eine Anfrage erhalten hat');
/*!40000 ALTER TABLE `wbci_message` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-10 16:51:06
