<?php

if (!extension_loaded('intl')) {
    throw new \Exception('This software needs the intl-extension to be installed');
}
