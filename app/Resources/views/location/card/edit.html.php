<?php

$view->extend('layout.html.php');

echo '<div style="display: block; width: 100%; text-align: right;">
<a class="card-delete-link" style="font-weight: bold; color: #F00;" href="'.$view['router']->path('location-card-delete', ['id' => $card->getId(), 'locationId' => $card->getLocation()->getId()]).'">Karte löschen</a>
<br /><br />
</div>';

echo '<div style="display: block; width: 100%; text-align: left;">
<a href="'.urldecode($view['router']->path('location-view', ['id' => $card->getLocation()->getId()])).'"><button type="button"><img alt="" src="'.$view['assets']->getUrl('_img/Back.png').'" height="12px"> Zurück</button></a>
<br /><br />
</div>';

echo '<br /><p style="color: red; font-style: italic;">Vorsicht!<br />Wird die Anzahl der Kartenports oder die Nummer des ersten Ports verändert, werden die, durch die Änderung überflüssig gewordenen Ports, gelöscht.</p><br />';

echo $view['form']->start($cardForm).'<br />';
echo $view['form']->label($cardForm['name']).'<br />';
echo $view['form']->widget($cardForm['name']).'<br /><br />';
echo $view['form']->label($cardForm['firstPortNumber']).'<br />';
echo $view['form']->widget($cardForm['firstPortNumber']).'<br /><br />';
echo $view['form']->label($cardForm['portAmount']).'<br />';
echo $view['form']->widget($cardForm['portAmount']).'<br /><br />';
echo $view['form']->label($cardForm['lineIdentifierPrefix']).'<br />';
echo $view['form']->widget($cardForm['lineIdentifierPrefix']).'<br /><br />';
echo $view['form']->label($cardForm['ipAddress']).'<br />';
echo $view['form']->widget($cardForm['ipAddress']).'<br /><br />';
echo $view['form']->label($cardForm['cardType']).'<br />';
echo $view['form']->widget($cardForm['cardType']).'<br /><br />';
echo $view['form']->label($cardForm['rfOverlayEnabled']).'<br />';
echo $view['form']->widget($cardForm['rfOverlayEnabled']).'<br /><br />';
echo $view['form']->label($cardForm['portDefaultVlanProfiles']).'<br />';
echo $view['form']->widget($cardForm['portDefaultVlanProfiles']).'<br /><br />';
echo $view['form']->label($cardForm['customerDefaultVlanProfiles']).'<br />';
echo $view['form']->widget($cardForm['customerDefaultVlanProfiles']).'<br /><br />';
echo $view['form']->widget($cardForm['submit']).'<br />';

echo '<br /><br />';

if (!empty($cardPorts)) {
    if (null !== $mduConnectedCardPortId) {
        if ('dependent-MDU' === $card->getCardType()->getName()) {
            echo sprintf('<a target="_blank" class="fancy-button outline round blue" href="%s">Info für MDU-Basis auf OLT abfragen</a>',
                $view["router"]->path("show-info-about-card-for-dependent-mdu", ["id" => $card->getId()])
            )
            .'&nbsp;&nbsp;&nbsp;&nbsp;'
            .sprintf('<a target="_blank" class="fancy-button outline round red" href="%s">MDU-Basis auf OLT einrichten</a>',
                $view["router"]->path("provision-card-for-dependent-mdu", ["id" => $card->getId()])
            )
            .'&nbsp;&nbsp;&nbsp;&nbsp;'
            .sprintf('<a target="_blank" class="ask-confirmation fancy-button outline round red" href="%s">MDU-Basis von OLT löschen</a>',
                $view["router"]->path("deprovision-card-for-dependent-mdu", ["id" => $card->getId()])
            )
            .'<br /><br /><br />';
        } else {
            echo sprintf('<a target="_blank" class="fancy-button outline round blue" href="%s">Info für diese Karte von MDU auf OLT abfragen</a>',
                $view["router"]->path("show-info-about-card-for-dependent-mdu", ["id" => $card->getId()])
            )
            .'&nbsp;&nbsp;&nbsp;&nbsp;'
            .sprintf('<a target="_blank" class="fancy-button outline round red" href="%s">Diese Karte für MDU auf OLT einrichten</a>',
                $view["router"]->path("provision-card-for-dependent-mdu", ["id" => $card->getId()])
            )
            .'&nbsp;&nbsp;&nbsp;&nbsp;'
            .sprintf('<a target="_blank" class="ask-confirmation fancy-button outline round red" href="%s">Diese Karte für MDU von OLT löschen</a>',
                $view["router"]->path("deprovision-card-for-dependent-mdu", ["id" => $card->getId()])
            )
            .'<br /><br /><br />';
        }
    }

    echo '<table cellspacing="2" cellpadding="2" class="IpRange">
    <thead>
    <tr>
    <th>Portnummer</th>
    <th>Attribute</th>
    <th>Farbe</th>
    <th>Verknüpft mit</th>
    <th>Aktionen</th>
    </tr>
    </thead>';

    foreach ($cardPorts as $cardPort) {
        $relatedTo = ' - ';
        $relatedCardId = null;
        $relatedCardPortId = null;

        if (null !== $cardPort->getRelatedCardPort()) {
            $relatedCardId = $cardPort->getRelatedCardPort()->getCard()->getId();
            $relatedCardPortId = $cardPort->getRelatedCardPort()->getId();

            $relatedTo = sprintf('%s<br/>'
                .'&nbsp;&nbsp;&nbsp;&nbsp;%s<br />'
                .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%s'
                .' :: %s',
                $cardPort->getRelatedCardPort()->getCard()->getLocation()->getNetwork()->getName(),
                $cardPort->getRelatedCardPort()->getCard()->getLocation()->getName(),
                $cardPort->getRelatedCardPort()->getCard()->getName(),
                $cardPort->getRelatedCardPort()->getNumber()
            );
        }

        $selectedCardPortAttributes = array_map(function (\AppBundle\Entity\Location\CardPortAttribute $attribute) {
            return $attribute->getId();
        }, $cardPort->getAttributes()->toArray());

        $attributes = '';

        foreach ($possibleCardPortAttributes as $cardPortAttribute) {
            $checked = '';

            if (in_array($cardPortAttribute->getId(), $selectedCardPortAttributes)) {
                $checked = ' checked="checked"';
            }

            $attributes .= '<input type="checkbox" name="card_port_attribute['.$cardPort->getId().']['.$cardPortAttribute->getId().']" value="1"'.$checked.' /> '.$cardPortAttribute->getLabel().'<br />';
        }

        $attributes = substr($attributes, 0, -6); // remove last <br />
        $attributes .= '<br />'.$view['form']->label($cardForm['ports'][$cardPort->getId()]['connectionType'])
            .'<br />'.$view['form']->widget($cardForm['ports'][$cardPort->getId()]['connectionType']);
        $attributes .= '<br />'.$view['form']->label($cardForm['ports'][$cardPort->getId()]['maxUnicastMacs'])
            .'<br />'.$view['form']->widget($cardForm['ports'][$cardPort->getId()]['maxUnicastMacs']);

        echo sprintf('<tr>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td><a href="#" class="related-card-port-choice" data-card-port-id="%d" data-related-card-id="%s" data-related-card-port-id="%s">bearbeiten</a></td>
            </tr>',
            $cardPort->getNumber(),
            $attributes,
            $cardPort->getColor(),
            $relatedTo,
            $cardPort->getId(),
            $relatedCardId,
            $relatedCardPortId
        );
    }

    echo '</table>';

    ?>
    <div style="display: none;" id="block-update-related-card-port">
        <select id="select-update-related-card-port">
            <option value="">Auswählen</option>

            <?php
            $currentCardId = $card->getId();

            foreach ($networksLocationsCards as $networkName => $locations) {
                echo '<optgroup label="'.$networkName.'"></optgroup>';

                foreach ($locations as $locationName => $cards) {
                    echo '<optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$locationName.'">';

                    foreach ($cards as $cardObject) {
                        $disabled = '';

                        if ($currentCardId === $cardObject->getId()) {
                            $disabled = ' disabled'; // disable this card
                        }

                        echo '<option'.$disabled.' value="'.$cardObject->getId().'">&nbsp;&nbsp;'.$cardObject->getName().'</option>';
                    }

                    echo '</optgroup>';
                }
            }
            ?>
        </select>
        <br />
        <select id="choice-related-card-port"></select>
    </div>
    <?php
}

echo $view['form']->end($cardForm);

?>

<script type="text/javascript">
$(document).ready(function () {
    $('.ask-confirmation').on('click', function(e) {
        if (window.confirm("Wollen Sie diese Karte der MDU wirklich vom OLT löschen?")) {
            return;
        }

        e.preventDefault();
    });

    /**
     * Delete this card link aktion
     */
    $('.card-delete-link').on('click', function(e) {
        if (window.confirm("Wollen Sie diese Karte wirklich löschen?")) {
            return;
        }

        e.preventDefault();
    });

    var url = '<?php echo $view["router"]->path("card-update-port", ["id" => "CARD-PORT-ID"]); ?>';
    var urlX = '<?php echo $view["router"]->path("card-get-unassigned-ports", ["id" => "CARD-ID"]); ?>';
    var setSelectedCardPortId = null;

    /**
     * On change action for #select-update-related-card-port.
     */
    $('#select-update-related-card-port').on('change', function() {
        var cardId = $(this).val();

        if ("" == cardId) {
            $('#choice-related-card-port').val('');
            $('#choice-related-card-port').html('');

            return;
        }

        var urlX_ = urlX.replace('CARD-ID', cardId);
        
        $.ajax({
            type: "POST",
            url: urlX_,
            data: {'selected-card-port-id': setSelectedCardPortId},
            success: function(data) {
                var html = '<option value="">Auswählen</option>';

                for (var i in data) {
                    html += '<option value="'+data[i].id+'">'+data[i].number+'</option>';
                }

                $('#choice-related-card-port').html(html);

                if (setSelectedCardPortId) {
                    $('#choice-related-card-port').val(setSelectedCardPortId);

                    setSelectedCardPortId = null;
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert('Fehler: ' + thrownError+" ("+xhr.status+")");
            }
        });
    });
    
    /**
     * On click aktion for .related-card-port-choice
     */
    $('.related-card-port-choice').on('click', function(e) {
        var cardPortId = $(this).data('card-port-id');
        var relatedCardId = $(this).data('related-card-id');
        var relatedCardPortId = $(this).data('related-card-port-id');

        $('#select-update-related-card-port').val('');

        if (relatedCardId) {
            $('#select-update-related-card-port').val(relatedCardId);

            if (relatedCardPortId) {
                // set setSelectedCardPortId before triggering change on #select-update-related-card-port
                setSelectedCardPortId = relatedCardPortId
            }

            $('#select-update-related-card-port').trigger('change');
        } else {
            $('#select-update-related-card-port').trigger('change');
        }

        $('#block-update-related-card-port').dialog({
            resizable: true,
            modal: true,
            title: 'Portverknüpfungen ändern',
            position: {my: 'top', at: 'top', of: '#Container'},
            height: 300,
            width: 500,
            buttons: {
                'Übernehmen': function () {
                    $(this).dialog('close');
                    
                    var url_ = url.replace('CARD-PORT-ID', cardPortId);

                    $.ajax({
                        type: "POST",
                        url: url_,
                        data: {'related-card-port-id': $('#choice-related-card-port').val()},
                        success: function(data) {
                            window.location.reload();

                            console.log("success");
                            console.log(data);

                            //response = $.parseJSON(data)
                            //if (response.error === true) {
                            //    alert('Es ist leider ein Fehler beim Erstellen des Tickets aufgetreten! \n\nFehler-Details:\n"' + response.data + '"');
                            //} else {
                            //    alert('Ticket erfolgreich angelegt! \n\nOTRS-Ticket-Nr: ' + response.data + '');
                            //}
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert('Fehler: ' + thrownError+" ("+xhr.status+")");
                        }
                    });
                },
                'Abbrechen': function () {
                    $(this).dialog('close');
                }
            }
        });
    });
});
</script>