<?php

$view->extend('layout.html.php');

echo '<div style="display: block; width: 100%; text-align: right;">
<a class="location-delete-link" style="font-weight: bold; color: #F00;" href="'.$view['router']->path('location-delete', ['id' => $location->getId(), 'networkId' => $location->getNetwork()->getId()]).'">Location löschen</a>
<br /><br />
</div>';

echo $view['form']->start($locationForm);
echo $view['form']->widget($locationForm);
echo $view['form']->end($locationForm);

?>

<script type="text/javascript">
$(document).ready(function () {
    $('.location-delete-link').on('click', function (e) {
        if (window.confirm("Wollen Sie diesen Standort wirklich löschen?")) {
            return;
        }

        e.preventDefault();
    });
});
</script>