<?php

$view->extend('layout.html.php');

$editIsAllowed = $view['security']->isGranted('ROLE_EDIT_VLAN_PROFILES');

?>

<div class="form box">
    <h3>VLAN-Profile</h3>
</div>
<br />
<div class="form box">
    <?php
    foreach ($vlanProfiles as $vlanProfile) {
        echo sprintf('<a href="%s">%s</a><br />',
            $view['router']->path('admin-edit-vlan-profile', ['id' => $vlanProfile->getId()]),
            $vlanProfile->getName()
        );
    }
    ?>
</div>
<div class="box">
    <br />
    <?php 
    if ($view['security']->isGranted('ROLE_CREATE_VLAN_PROFILES')) {
        echo sprintf('<a class="fancy-button outline grey" href="%s">Hinzufügen</a>', 
            $view['router']->path('admin-create-vlan-profile')
        );
    }
    ?>
</div>

<br />
<br />
<br />
