<?php

$view->extend('layout.html.php');

$editIsAllowed = $view['security']->isGranted('ROLE_EDIT_VLAN_PROFILES');

?>

<div class="form box">
    <h3>VLAN-Profil</h3>
</div>
<br />
<div class="form box">
    <?php

    echo $view['form']->start($form);
    echo $view['form']->widget($form);
    echo $view['form']->end($form);

    ?>
</div>
<div class="box">
    <br />
    <?php
    echo sprintf('<a class="fancy-button outline grey" href="%s">zurück</a>', 
        $view['router']->path('admin-vlan-profiles')
    );
    ?>
</div>

<br />
<br />
<br />

<script type="text/javascript">
(function () {
    // set current vlans index
    $('#vlan_profile_vlans').data('index', <?php echo count($form['vlans']->children); ?>)

    // add remove vlan link
    for (var i = 0; i < $('#vlan_profile_vlans').data('index'); i++) {
        var elem = $('#vlan_profile_vlans_'+i);
        var removeLink = $('<a href="#" style="color: red;">VLAN Löschen</a>');
        removeLink.on('click', function (event) {
            if (window.confirm('Möchten Sie diese VLAN wirklich löschen?')) {
                $(this).parent().remove();
            }

            event.preventDefault();
        });

        elem.parent().prepend(removeLink);
    }

    // add add vlan link
    var addVlanLink = $('<a href="#" id="add-vlan-link" style="color: blue">VLAN hinzufügen</a>');
    addVlanLink.on('click', function (event) {
        var index = $('#vlan_profile_vlans').data('index');
        var prototype = $('#vlan_profile_vlans').data('prototype');
        prototype = prototype.replace(/__name__label__/g, '__name__');
        prototype = prototype.replace(/__name__/g, index);

        $('#vlan_profile_vlans').append(prototype);
        $('#vlan_profile_vlans').data('index', index + 1);

        // add remove link
        var elem = $('#vlan_profile_vlans_'+index);
        var removeLink = $('<a href="#" style="color: red;">VLAN Löschen</a>');
        removeLink.on('click', function (event) {
            if (window.confirm('Möchten Sie diese VLAN wirklich löschen?')) {
                $(this).parent().remove();
            }

            event.preventDefault();
        });

        elem.parent().prepend(removeLink);

        event.preventDefault();
    });

    $('#vlan_profile_submit').parent().before('<br />');
    $('#vlan_profile_submit').parent().before(addVlanLink);
    addVlanLink.after('<br /><br />');
})();
</script>
