<?php

$view->extend('layout.html.php');

$editIsAllowed = $view['security']->isGranted('ROLE_EDIT_PRODUCTS');

?>

<div class="form box">
    <h3>Steuersätze</h3>
</div>
<br />
<div class="form box">
    <table class="IpRange">
        <thead>
            <tr>
                <th>ID</th>
                <th>Prozentbetrag</th>
                <th>Aktionen</th>
            </tr>
        </thead>

        <tbody>
            <?php

            foreach ($taxRates as $taxRate) {
                if ($editIsAllowed) {
                    $linkToEdit = sprintf('<a href="%s">Bearbeiten</a>',
                        $view['router']->path('admin-products-edit-tax-rate', ['id' => $taxRate->getId()])
                    );
                }

                echo sprintf('<tr>
                    <td class="num">%s</td>
                    <td>%s %%</td>
                    <td>%s</td>
                    </tr>',
                    $taxRate->getId(),
                    $taxRate->getAmount(),
                    $linkToEdit
                );
            }

            ?>
        </tbody>
    </table>
</div>
<div class="box">
    <br />
    <a class="fancy-button outline grey" href="<?php echo $view['router']->path('admin-products-create-tax-rate') ?>">Hinzufügen</a>
</div>

<br />
<br />
<br />

<div class="form box">
    <h3>Haupt-Produkt-Kategorien</h3>
</div>
<br />
<div class="form box">
    <table class="IpRange">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Aktionen</th>
            </tr>
        </thead>

        <tbody>
            <?php

            foreach ($productCategories as $productCategory) {
                if ($editIsAllowed) {
                    $linkToEdit = sprintf('<a href="%s">Bearbeiten</a>',
                        $view['router']->path('admin-products-edit-category', ['id' => $productCategory->getId()])
                    );
                }

                echo sprintf('<tr>
                    <td class="num">%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    </tr>',
                    $productCategory->getId(),
                    $productCategory->getName(),
                    $linkToEdit
                );
            }

            ?>
        </tbody>
    </table>
</div>
<div class="box">
    <br />
    <a class="fancy-button outline grey" href="<?php echo $view['router']->path('admin-products-create-category') ?>">Hinzufügen</a>
</div>

<br />
<br />
<br />

<div class="form box">
    <h3>Karten-Typen</h3>
</div>
<br />
<div class="form box">
    <table class="IpRange">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Aktionen</th>
            </tr>
        </thead>

        <tbody>
            <?php

            foreach ($cardTypes as $cardType) {
                if ($editIsAllowed) {
                    $linkToEdit = sprintf('<a href="%s">Bearbeiten</a>',
                        $view['router']->path('admin-products-edit-card-type', ['id' => $cardType->getId()])
                    );
                }

                echo sprintf('<tr>
                    <td class="num">%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    </tr>',
                    $cardType->getId(),
                    $cardType->getName(),
                    $linkToEdit
                );
            }

            ?>
        </tbody>
    </table>
</div>
<div class="box">
    <br />
    <a class="fancy-button outline grey" href="<?php echo $view['router']->path('admin-products-create-card-type') ?>">Hinzufügen</a>
</div>

<br />
<br />
<br />

<div class="form box">
    <h3>Haupt-Produkte</h3>
</div>
<br />
<div class="form box">
    <table class="IpRange">
        <thead>
            <tr>
                <th>Produkt</th>
                <th>Name</th>
                <th>Kategorie</th>
                <th>xdslServiceProfile</th>
                <th>gponDownload</th>
                <th>gponUpload</th>
                <th>EthernetUpDown</th>
                <th>Anschluss</th>
                <th>Aktionen</th>
            </tr>
        </thead>

        <tbody>
            <?php

            foreach ($products as $product) {
                if ($editIsAllowed) {
                    $linkToEdit = sprintf('<a href="%s">Bearbeiten</a>',
                        $view['router']->path('admin-products-edit-product', ['id' => $product->getId()])
                    );
                }

                $categoryName = null !== $product->getCategory() ? $product->getCategory()->getName() : null;
                $cardTypeName = null !== $product->getCardType() ? $product->getCardType()->getName() : null;

                $style = '';

                if (!$product->getActive()) {
                    $style = 'color: #bc876b;';
                }

                $title = sprintf("%s\n\nId: %d\nDownload: %s\nUpload: %s\nPreis (netto): %s\nPreis (brutto): %s", 
                    $product->getActive() ? 'Aktiv' : 'Inaktiv',
                    $product->getId(),
                    $product->getDownload(),
                    $product->getUpload(),
                    $product->getPriceNet(),
                    $product->getPriceGross()
                );

                echo sprintf('<tr style="'.$style.'" title="'.$title.'">
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    </tr>',
                    $product->getIdentifier(),
                    $product->getName(),
                    $categoryName,
                    $product->getXdslServiceProfile(),
                    $product->getGponDownload(),
                    $product->getGponUpload(),
                    $product->getEthernetUpDown(),
                    $cardTypeName,
                    $linkToEdit
                );
            }

            ?>
        </tbody>
    </table>
</div>
<div class="box">
    <br />
    <a class="fancy-button outline grey" href="<?php echo $view['router']->path('admin-products-create-product') ?>">Hinzufügen</a>
</div>
