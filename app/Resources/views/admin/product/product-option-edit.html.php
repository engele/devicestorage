<?php

$view->extend('layout.html.php');

$editIsAllowed = $view['security']->isGranted('ROLE_EDIT_PRODUCTS');

?>

<div class="form box">
    <h3>Produkt-Optionen</h3>
</div>
<br />
<div class="form box">
    <?php

    echo $view['form']->start($productOptionForm);
    echo $view['form']->widget($productOptionForm);
    echo $view['form']->end($productOptionForm);

    ?>
</div>