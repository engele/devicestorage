<?php

$view->extend('layout.html.php');

?>

<div class="box">
    <h3>Haupt-Produkt anlegen/ändern</h3>
    <br />

    <?php

    echo $view['form']->start($form);
    echo $view['form']->widget($form);
    echo $view['form']->end($form);

    ?>
</div>
