<?php

$view->extend('layout.html.php');

$editIsAllowed = $view['security']->isGranted('ROLE_EDIT_PRODUCTS');

?>

<div class="form box">
    <h3>Produkt-Optionen</h3>
</div>
<br />
<div class="form box">
    <table class="IpRange">
        <thead>
        <tr>
            <th>ID</th>
            <th>Produktcode</th>
            <th>Name</th>
            <th>Preis (Netto)</th>
            <th>Preis (Brutto)</th>
            <th>Aktiv</th>
            <th>Aktionen</th>
        </tr>
        </thead>

        <tbody>
        <?php

        foreach ($productOptions as $productOption) {
            if ($editIsAllowed) { // $editIsAllowed
                $linkToEdit = sprintf('<a href="%s">Bearbeiten</a>',
                    $view['router']->path('product-option-edit', ['id' => $productOption->getId()])
                );
            }

            echo sprintf('<tr>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                </tr>',
                $productOption->getId(),
                $productOption->getIdentifier(),
                $productOption->getName(),
                $productOption->getPriceNet(),
                $productOption->getPriceGross(),
                $productOption->getActive(),
                $linkToEdit
            );
        }

        ?>
        </tbody>
    </table>
</div>
<div class="box">
    <br />
    <a class="fancy-button outline grey" href="<?php echo $view['router']->path('product-option-create') ?>">Hinzufügen</a>
</div>
