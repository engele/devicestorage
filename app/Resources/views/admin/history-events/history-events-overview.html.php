<?php

$view->extend('layout.html.php');

$editIsAllowed = $view['security']->isGranted('ROLE_MASTER');

?>

<div class="form box">
    <h3>Verlauf-Events</h3>
</div>
<br />
<div class="form box">
    <table class="IpRange">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Reihenfolge</th>
                <th>Gruppe</th>
                <th>Preis</th>
                <th>Aktionen</th>
            </tr>
        </thead>

        <tbody>
            <?php

            foreach ($historyEvents as $historyEvents) {
                if ($editIsAllowed) {
                    $linkToEdit = sprintf('<a href="%s">Bearbeiten</a>',
                        $view['router']->path('admin-history-events-edit-history-event', ['id' => $historyEvents->getId()])
                    );
                }

                echo sprintf('<tr>
                    <td class="num">%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    </tr>',
                    $historyEvents->getId(),
                    $historyEvents->getEvent(),
                    $historyEvents->getOrderNumber(),
                    $historyEvents->getGroup(),
                    $historyEvents->getPriceNet(),
                    $linkToEdit
                );
            }

            ?>
        </tbody>
    </table>
</div>
<div class="box">
    <br />
    <a class="fancy-button outline grey" href="<?php echo $view['router']->path('admin-history-events-create-history-event') ?>">Hinzufügen</a>
</div>

<br />
<br />
<br />
