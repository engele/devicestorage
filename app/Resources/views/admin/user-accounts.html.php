<?php

$view->extend('layout.html.php');

echo $view['form']->start($newAccountForm);
echo $view['form']->widget($newAccountForm);
echo $view['form']->end($newAccountForm);
echo '<br /><br />';

if (count($accounts) > 0) {
    echo '<table cellspacing="15" cellpadding="0">'
        .'<tr style="text-decoration: underline;">'
        .'<td>Active</td>'
        .'<td>Username</td>'
        .'<td>Full-Name</td>'
        .'<td>Verlaufsauswertung</td>'
        .'<td>Konto-Typ</td>'
        .'<td>Aktion</td>'
        .'</tr>';

    foreach ($accounts as $account) {
        $rowColor = $account->getIsActive() ? '#000;' : '#ccc;';

        $accountRoles = array_map(function ($role) use ($roles) {
            return array_search($role, $roles);
        }, $account->getRoles());

        echo sprintf('<tr style="color: %s;"><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td>'
            .'<td><a style="color: blue;" href="%s">Edit</a></td></tr>',

            $rowColor,
            $account->getIsActive() ? 'x' : 'o',
            $account->getUsername(),
            $account->getFullName(),
            $account->getIncludeInHistoryReporting() ? 'ja' : 'nein',
            implode(', ', $accountRoles),
            $view['router']->path('admin-user-accounts-edit', ['id' => $account->getId()])
        );
    }

    echo '</table>';

    echo '<br /><br /><hr /><br /><p align="center"><strong>Purtel-Admin-Accounts</strong></p><br />';

    echo $view['form']->start($newPurtelAdminAccountForm)
        . $view['form']->widget($newPurtelAdminAccountForm)
        . $view['form']->end($newPurtelAdminAccountForm)
        . '<br /><br />';

    echo '<table cellspacing="15" cellpadding="0">'
        .'<tr style="text-decoration: underline;">'
        .'<td>Identifier</td>'
        .'<td>Username</td>'
        .'<td>Users</td>'
        .'<td>Aktion</td>'
        .'</tr>';

    foreach ($purtelAdminAccounts as $purtelAdminAccount) {
        $authorizedUsers = array_map(function ($user) {
            return $user->getFullName();
        }, $purtelAdminAccount->getAuthorizedUsers()->toArray());

        echo sprintf('<tr><td>%s</td><td>%s</td><td>%s</td>'
            .'<td><a style="color: blue;" href="%s">Edit</a></td></tr>',

            $purtelAdminAccount->getIdentifier(),
            $purtelAdminAccount->getUsername(),
            implode(', ', $authorizedUsers),
            $view['router']->path('admin-purtel-admin-accounts-edit', ['id' => $purtelAdminAccount->getId()])
        );
    }

    echo '</table>';
}

?>