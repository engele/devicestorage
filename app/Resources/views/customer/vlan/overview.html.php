<h3 class="CustVlan inherited-vlans-content <?php echo $customer->getOverrideInheritedVlanProfiles() ? 'box-disabled' : ''; ?>">Vererbte VLANs</h3>
<div class="CustVlan inherited-vlans-content <?php echo $customer->getOverrideInheritedVlanProfiles() ? 'box-disabled' : ''; ?>">
    <?php
    $card = $customer->getCardId();

    if ($card instanceof \AppBundle\Entity\Location\Card) {
        $nameOfVlanProvider = 'Karte';
        $vlanProfiles = $card->getCustomerDefaultVlanProfiles();

        if ($vlanProfiles->isEmpty()) {
            $location = $card->getLocation();

            if ($location instanceof \AppBundle\Entity\Location\Location) {
                // use customer default vlan profiles from location - or empty ArrayCollection
                $nameOfVlanProvider = 'Standort';
                $vlanProfiles = $card->getLocation()->getCustomerDefaultVlanProfiles();
            }
        }

        if ($vlanProfiles->isEmpty()) {
            echo '<p align="center">Keine VLAN-Voreinstellungen von Karte oder Standort vorhanden.</p>';
        } else {
            echo sprintf('<p style="font-weight: bold;">Vererbt durch: <span style="color: red;">%s</span></p>', $nameOfVlanProvider);
            echo '<div class="row">';

            foreach ($vlanProfiles as $vlanProfile) {
                echo '<div class="column small-2"><div style="margin: 10px; padding: 10px;" class="callout-blue">
                <p style="font-weight: bold;">'.$vlanProfile->getName().'</p>';

                foreach ($vlanProfile->getVlans() as $vlan) {
                    echo sprintf('<div style="padding-bottom: 15px; padding-left: 20px;">
                        VLAN-ID: %s<br />
                        Tagging: %s<br />
                        QoS-Profile: %s<br />
                        Network-VLAN-ID: %s<br />
                        </div>',
                        $vlan->getVlanId(),
                        $vlan->getTagging(),
                        $vlan->getQosProfile(),
                        $vlan->getNetworkVlan()
                    );
                }

                echo '</div></div>';
            }

            echo '</div>';
        }
    } else {
        echo '<p align="center">Kein Kartenport ausgewählt.</p>';
    }
    ?>
</div>

<br />
<br />

<h3 class="CustVlan">Kundenspezifische VLANs</h3>
<div class="CustVlan form">
    <?php if ($view['security']->isGranted('ROLE_EDIT_CUSTOMER_VLAN')) { ?>
        <p style="font-weight: bold;">
            Vererbte VLANs erweitern oder überschreiben? &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <label class="switch">
                <input type="checkbox" class="customer-override-inherited-vlan-profiles-switch" <?php echo $customer->getOverrideInheritedVlanProfiles() ? 'checked="true"' : ''; ?>>
                <span class="slider"></span>
            </label>
            &nbsp;&nbsp;
            <span id="customer-override-inherited-vlan-profiles-status-text" style="color: red; vertical-align: baseline; padding: 0px; width: auto;">
                <?php echo $customer->getOverrideInheritedVlanProfiles() ? 'Überschreiben' : 'Erweitern'; ?>
            </span>
        </p>
        <br />

        <p style="font-weight: bold;">
            VLAN-Profil hinzufügen: &nbsp;&nbsp;<select style="width: auto;" id="customer-add-vlan-profile"><option>Auswählen</option>
            
            <?php
            $alreadyAssignedVlanProfiles = array_map(function (\AppBundle\Entity\Customer\CustomerVlanProfile $customerVlanProfile) {
                return $customerVlanProfile->getVlanProfile()->getId();
            }, $customer->getVlanProfiles()->toArray());

            foreach ($globalVlanProfiles as $vlanProfile) {
                echo sprintf('<option value="%s"%s>%s</option>',
                    $vlanProfile->getId(),
                    in_array($vlanProfile->getId(), $alreadyAssignedVlanProfiles) ? 'disabled="true"' : '',
                    $vlanProfile->getName()
                );
            }
            ?>

            </select>&nbsp;&nbsp;
            <button type="button" class="customer-add-vlan-profile-button">Hinzufügen</button>
        </p>
        <br />
    <?php } ?>

    <?php
    echo '<div class="row">';
    foreach ($customer->getVlanProfiles() as $customerVlanProfile) {
        $vlanProfile = $customerVlanProfile->getVlanProfile();

        echo '<div class="column small-2"><div style="margin: 10px; padding: 10px;" class="callout-blue">
        <p style="font-weight: bold; clear: right;">'.$vlanProfile->getName()
        .'&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" data-profile-id="'.$customerVlanProfile->getId().'" class="customer-remove-vlan-profile-button" style="color: red; float: right;" title="Profil löschen">X</a></p>';

        foreach ($vlanProfile->getVlans() as $vlan) {
            echo sprintf('<div style="padding-bottom: 15px; padding-left: 20px;">
                VLAN-ID: %s<br />
                Tagging: %s<br />
                QoS-Profile: %s<br />
                Network-VLAN-ID: %s<br />
                </div>',
                $vlan->getVlanId(),
                $vlan->getTagging(),
                $vlan->getQosProfile(),
                $vlan->getNetworkVlan()
            );
        }

        echo '</div></div>';
    }
    echo '</div>';
    ?>
</div>

<script type="text/javascript">
$(document).ready(function () {
    /**
     * toggle override inherited vlan profiles
     */
    $('.customer-override-inherited-vlan-profiles-switch').on('click', function (event) {
        var overrideInheritedVlanProfiles = $(this).prop('checked');

        $.ajax({
            type: "POST",
            url: '<?php echo $view['router']->path('customer/technical-data/set-override-inherited-vlan-profiles', ['id' => $customer->getId()]) ?>',
            data: "override-inherited-vlan-profiles=" + (overrideInheritedVlanProfiles ? 1 : 0),
            success: function(data) { 
                if ('success' === data.status) {
                    if (overrideInheritedVlanProfiles) {
                        $('#customer-override-inherited-vlan-profiles-status-text').html('&Uuml;berschreiben');
                        $('.inherited-vlans-content').addClass('box-disabled');
                    } else {
                        $('#customer-override-inherited-vlan-profiles-status-text').html('Erweitern');
                        $('.inherited-vlans-content').removeClass('box-disabled');
                    }

                    return;
                }

                window.alert(JSON.stringify(data));
            },
            error: function (message) { window.alert(JSON.stringify(message)); }
        });
    });

    /**
     * remove vlan profile from customer
     */
    $('.customer-remove-vlan-profile-button').on('click', function (event) {
        event.preventDefault();

        if (window.confirm('Möchten Sie dieses Profil wirklich löschen?')) {
            var customerVlanProfileId = $(this).data('profile-id');

            $.ajax({
                type: "POST",
                url: '<?php echo $view['router']->path('customer/technical-data/delete-vlan-profile', ['id' => $customer->getId()]) ?>',
                data: "customer-vlan-profile-id=" + customerVlanProfileId,
                success: function(data) { 
                    if ('success' === data.status) {
                        if (window.location.href.indexOf('&area=SwitchVlan') < 0) {
                            window.location.href = window.location.href + '&area=SwitchVlan';
                        } else {
                            window.location.reload(true);
                        }

                        return;
                    }

                    window.alert(JSON.stringify(data));
                },
                error: function (message) { window.alert(JSON.stringify(message)); }
            });
        }
    });
    
    /**
     * add vlan profile to customer
     */
    $('.customer-add-vlan-profile-button').on('click', function (event) {
        event.preventDefault();

        var globalVlanProfileId = $('#customer-add-vlan-profile').val();

        if (globalVlanProfileId.match(/^[0-9]+$/g)) {
            $.ajax({
                type: "POST",
                url: '<?php echo $view['router']->path('customer/technical-data/add-vlan-profile', ['id' => $customer->getId()]) ?>',
                data: "vlan-profile-id="+globalVlanProfileId,
                success: function(data) { 
                    if ('success' === data.status) {
                        if (window.location.href.indexOf('&area=SwitchVlan') < 0) {
                            window.location.href = window.location.href + '&area=SwitchVlan';
                        } else {
                            window.location.reload(true);
                        }

                        return;
                    }

                    window.alert(JSON.stringify(data));
                },
                error: function (message) {
                    if (message.responseJSON.message && 'VLAN-Profil wurde dem Kunden bereits hinzugefügt.' === message.responseJSON.message) {
                        window.alert(message.responseJSON.message);
                    } else {
                        window.alert(JSON.stringify(message));
                    }
                }
            });
        } else {
            window.alert('Kein VLAN-Profil ausgewählt.');
        }
    });
});
</script>
