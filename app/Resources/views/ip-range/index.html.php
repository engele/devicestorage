<?php

$view->extend('layout.html.php');

?>

<div class="IpRange box">
    <h3>IP-Blöcke</h3>
    <br />
    <a href="<?php echo $view['router']->path('ip-range-new') ?>">Hinzufügen</a>
    <br /><br />

    <table class="IpRange">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Start-Adresse</th>
                <th>End-Adresse</th>
                <th>Subnetzmaske</th>
                <th>Gateway</th>
                <th>VLAN ID</th>
                <th>Anzahl IPs</th>
                <th>Freie IPs</th>
                <th>Type</th>
                <th>PPPoE-IP</th>
                <th>PPPoE-Standort</th>
                <th>Ulink-Prov.</th>
                <th>Aktionen</th>
            </tr>
        </thead>

        <tbody>
            <?php

            $editIsAllowed = $view['security']->isGranted('ROLE_EDIT_IP_RANGE');

            foreach ($ipRanges as $ipRange) {
                $ipsTotal = $ipRange->getIpsTotal();
                $linkToEdit = null;
                
                if ($editIsAllowed) {
                    $linkToEdit = sprintf('<a href="%s">Bearbeiten</a>',
                        $view['router']->path('ip-range-edit', ['ipRange' => $ipRange->getId()])
                    );
                }

                echo sprintf('<tr>
                    <td class="num">%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td class="Num">%s</td>
                    <td class="Num">%s</td>
                    <td class="Num">%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    </tr>',
                    $ipRange->getId(),
                    $ipRange->getName(),
                    $ipRange->getStartAddress(),
                    $ipRange->getEndAddress(),
                    $ipRange->getSubnetmask(),
                    $ipRange->getGateway(),
                    $ipRange->getVlanId(),
                    $ipsTotal,
                    $ipsTotal - $ipRange->usedAddresses,
                    $ipRange->getPppoeType(),
                    $ipRange->getPppoeIp(),
                    $ipRange->getPppoeLocation(),
                    $ipRange->getUplinkProvider(),
                    $linkToEdit
                );
            }

            ?>
        </tbody>
    </table>
</div>
