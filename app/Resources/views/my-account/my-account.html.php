<?php

$view->extend('layout.html.php');

?>

<div class="account box">
    <h3>Anmeldung Verwalten</h3>

    <div class="account form">

        <a href="<?php echo $view['router']->path('my-account-change-password') ?>" class="button">
            <img alt="" src="<?php echo $view['assets']->getUrl('_img/Modify.png'); ?>" height="12px"> Passwort ändern
        </a>

        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

        <a href="<?php echo $view['router']->path('logout') ?>" class="button">
            <img alt="" src="<?php echo $view['assets']->getUrl('_img/Exit.png'); ?>" height="12px"> Abmelden
        </a>
    </div>
</div>
