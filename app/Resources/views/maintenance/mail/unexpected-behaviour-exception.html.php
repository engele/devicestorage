<?php if (null !== $app->getUser()) { ?>
    <strong>User:</strong> <?php echo $app->getUser()->getUsername(); ?><br />
<?php } ?>
<br />
<strong>Url:</strong> <?php echo $request->getUri(); ?><br />
<br />
<strong>Exception-Message:</strong> <?php echo $exception->getMessage(); ?><br />
<strong>Exception-Code:</strong> <?php echo $exception->getCode(); ?><br />
<strong>Exception-File:</strong> <?php echo $exception->getFile(); ?><br />
<strong>Exception-Line:</strong> <?php echo $exception->getLine(); ?><br />
<br />
<strong>GET:</strong><br />
<pre><?php echo var_export($request->query, true); ?></pre><br />
<strong>POST:</strong><br />
<pre><?php echo var_export($request->request, true); ?></pre><br />
<strong>SERVER:</strong><br />
<pre><?php echo var_export($request->server, true); ?></pre><br />
