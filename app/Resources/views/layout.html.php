<?php

require_once __DIR__.'/../../../web/vendor/wisotel/configuration/Configuration.php';
include __DIR__.'/../../../web/config.inc';
require_once $StartPath.'/_conf/menu.inc';

$ForceLoginDiv  = '';

# getmenu select
$SelMenuName = '';
if (isset($_GET['menu'])) {
    $SelMenu = $_GET['menu'];
    if (isset($menu[$SelMenu])) $SelMenuName = $menu[$SelMenu];
}
if (!$SelMenuName) $SelMenu = $menu_start;
list($SelPath) = explode (':', $SelMenu);

$_SESSION['kv_config']['system']['startPath'] = $StartPath;
$_SESSION['kv_config']['system']['startURL']  = $StartURL;
//$_SESSION['kv_config']['system']['selMenu']   = $SelMenu;
$_SESSION['kv_config']['system']['selPath']   = $SelPath;

#get side
$SelSide = 'index.php';
if (isset($_GET['side'])) $SelSide = $_GET['side'].'.php';
if (!file_exists($StartPath.'/'.$SelPath.'/'.$SelSide)) $SelSide = 'index.php';

$loginFull = $app->getUser()->getFullName();
$loginUser = $app->getUser()->getUsername();

$Start  = "<input name='StartPath' id='StartPath' type='hidden' value='".urlencode($StartPath)."'>";
$Start .= "<input name='SelPath' id='SelPath' type='hidden' value='".urlencode($SelPath)."'>";
$Start .= "<input name='StartURL' id='StartURL' type='hidden' value='".urlencode($StartURL)."'>";



include $StartPath.'/header.php';

echo <<<HTML
<div id="Container" class="clearfix">
    <div id="Navigation">
        <div id="Logo"><img src="$StartURL/_img/logo.png"></div>
HTML;
        //require_once $StartPath.'/menu.php';
        echo $view->render('mainMenu.html.php', ['menus' => $menus]);

        if (file_exists($StartPath.'/'.$SelPath.'/button.php')) {
            require_once $StartPath.'/'.$SelPath.'/button.php';
        }

        echo <<<HTML
        Version: $KVVesion<br />
        Login: {$loginFull} ({$loginUser})<br />
    </div>
    <div id="Content">
HTML;
        
        $errors = $view['session']->getFlash('error');

        if (!empty($errors)) {
            echo '<div style="background-color: red; padding: 10px;">';

            foreach ($errors as $message) {
                echo $message;
            }

            echo '</div><br /><br />';
        }

        $success = $view['session']->getFlash('success');

        if (!empty($success)) {
            echo '<div style="background-color: green; padding: 10px; color: white;">';

            foreach ($success as $message) {
                echo $message;
            }

            echo '</div><br /><br />';
        }

        $view['slots']->output('_content');

    echo <<<HTML
    </div>
    $Start
</div>
<div id="LoginBack"></div>
$ForceLoginDiv
HTML;

require_once $StartPath.'/footer.php';

?>
