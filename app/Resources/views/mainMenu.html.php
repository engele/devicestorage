<?php

use Symfony\Bundle\FrameworkBundle\Templating\PhpEngine;
use Symfony\Menu\MenuItem;

/**
 * @param MenuItem $item
 * @param integer $loopIndex
 * @param PhpEngine $view
 * 
 * @return string
 */
function sidebarMenu(MenuItem $item, $loopIndex, PhpEngine $view)
{
    $isVisible = $item->visible;

    if (isset($item->requiredRole)) {
        if (!$view['security']->isGranted($item->requiredRole)) {
            $isVisible = false;
        }
    }

    if ($isVisible) {
        $string = '<li><a%s href="%s">%s</a></li>';

        $args = [
            isset($item->target) ? $item->target : '',
            '',
            (isset($item->icon) ? $item->icon : '') . $item->name,
        ];

        if (isset($item->path)) {
            $args[1] = urldecode($item->path);
        } else if (isset($item->link)) {
            $args[1] = $item->link;
        }

        return vsprintf($string, $args);
    }

    return '';
}

?>

<ul id="Menu">
    <?php

    foreach ($menus->MainMenu() as $key => $item) {
        echo sidebarMenu($item, $key, $view);
    }

    ?>
</ul>
