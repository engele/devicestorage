<?php
$view->extend('layout.html.php');
?>

<div class="serving-addresses IpRange box">
    <h3>Versorgungs-Adressen</h3>


    <br/>

    <form id="serving-addresses-form-search" name="serving-addresses-form-search">
        <div class="typeahead__container">
            <div class="typeahead__field">
                <div class="typeahead__query">
                    <input class="serving-addresses-form-input" name="search_phrase" type="search"
                           placeholder="Suchen (ID, PLZ, Stadt, Strasse)" autocomplete="off">
                </div>
            </div>
        </div>
    </form>

    <br/>
    <table class="IpRange">
        <thead>
        <tr>
            <th>PLZ</th>
            <th>Ort</th>
            <th>Ortsteil</th>
            <th>Strasse</th>
            <th>Nr.</th>
            <th>Anz. Einh.</th><!-- Amount of Business Units -->
            <th>Anz. Whng.</th><!-- Amount of Dwelling Units -->
            <th class="sa-center">Rollout gepl.</th><!-- Rollout geplant -->
            <th class="sa-center">Rollout beendet</th><!-- Rollout beendet -->
            <th class="sa-center">Conn. ja</th><!-- Gebäude ist connected -->
            <th class="sa-center">Ne4 beend.</th><!-- is Ne4 finished -->
            <th class="sa-center">hat GnV</th><!-- has Gnv -->
            <th class="sa-center">Internet Vertrag</th><!-- hatte jemals einen Internetvertrag -->
            <th class="sa-center">Klassik Vertrag</th><!-- hatte jemals einen Basis Fernsehvertrag  -->
            <th class="sa-center"></th><!-- Bedienelemente  -->
        </tr>
        </thead>
        <tbody>
        <?php

        $linkToCreate = '';
        if ($view['security']->isGranted('ROLE_CREATE_SERVING_ADDRESSES')) {
            $linkToCreate = sprintf('<a class="fancy-button outline grey" href="%s">Hinzufügen</a>', $view['router']->path('servingaddress_new'));
        }

        /** @var AppBundle\Entity\ServingAddress[] $servingAddresses */
        foreach ($servingAddresses as $servingAddress) {
            $linkToEdit = '';
            if ($view['security']->isGranted('ROLE_EDIT_SERVING_ADDRESSES')) {
                $linkToEdit = sprintf('<a href="%s">Edit</a>', $view['router']->path('servingaddress_edit', ['id' => $servingAddress->getId()]));
            }
            $linkToShow = '';
            if ($view['security']->isGranted('ROLE_VIEW_SERVING_ADDRESSES')) {
                $linkToShow = sprintf('<a href="%s">Detail</a>', $view['router']->path('servingaddress_show', ['id' => $servingAddress->getId()]));
            }

            $values['zip'] = $servingAddress->getZipCode();
            $values['city'] = $servingAddress->getCity();
            $values['dist'] = $servingAddress->getDistrict();
            $values['street'] = $servingAddress->getStreet();
            $values['num'] = $servingAddress->getHouseNumber();
            $values['amtBU'] = $servingAddress->getAmountOfBusinessUnits();
            $values['amtDU'] = $servingAddress->getAmountOfDwellingUnits();

            $values['ropi'] = null !== $servingAddress->getRolloutIsPlannedIn() ? $servingAddress->getRolloutIsPlannedIn()->format("d.m.Y") : null;

            $rof = $servingAddress->getIsRolloutFinished();
            $values['rof'] = $rof === null ? " sa-null" : ( $rof ? " sa-true" : " sa-false");

            $bc = $servingAddress->getIsBuildingConnected();
            $values['bc'] = $bc === null ? " sa-null" : ( $bc ? " sa-true" : " sa-false");

            $n4f = $servingAddress->getIsNe4Finished();
            $values['n4f'] = $n4f === null ? " sa-null" : ( $n4f ? " sa-true" : " sa-false");

            $gnv = $servingAddress->getHasGnv();
            $values['gnv'] = $gnv === null ? " sa-null" : ( $gnv ? " sa-true" : " sa-false");

            $isc = $servingAddress->getHasEverHadAnInternetSupplyContract();
            $values['isc'] = $isc === null ? " sa-null" : ( $isc ? " sa-true" : " sa-false");

            $bsc = $servingAddress->getHasEverHadABasicTvSupplyContract();
            $values['bsc'] = $bsc === null ? " sa-null" : ( $bsc ? " sa-true" : " sa-false");

            ?>

            <tr>
                <td><?php echo $values['zip']; ?></td>
                <td><?php echo $values['city']; ?></td>
                <td><?php echo $values['dist']; ?></td>
                <td><?php echo $values['street']; ?></td>
                <td><?php echo $values['num']; ?></td>
                <td><?php echo $values['amtBU']; ?></td>
                <td><?php echo $values['amtDU']; ?></td>
                <td><?php echo $values['ropi']; ?></td>
                <td class="sa-bool<?php echo $values['rof']; ?>"><span></span></td>
                <td class="sa-bool<?php echo $values['bc']; ?>"><span></span></td>
                <td class="sa-bool<?php echo $values['n4f']; ?>"><span></span></td>
                <td class="sa-bool<?php echo $values['gnv']; ?>"><span></span></td>
                <td class="sa-bool<?php echo $values['isc']; ?>"><span></span></td>
                <td class="sa-bool<?php echo $values['bsc']; ?>"><span></span></td>





                <!--
                <td><?php /*echo($servingAddress->getDamping()); */?></td>
                <td><?php /*echo($servingAddress->getDiameter1()); */?></td>
                <td><?php /*echo($servingAddress->getLength1()); */?></td>
                <td><?php /*echo($servingAddress->getDiameter2()); */?></td>
                <td><?php /*echo($servingAddress->getLength2()); */?></td>
                <td><?php /*echo($servingAddress->getDiameter3()); */?></td>
                <td><?php /*echo($servingAddress->getLength3()); */?></td>
                <td><?php /*echo($servingAddress->getDiameter4()); */?></td>
                <td><?php /*echo($servingAddress->getLength4()); */?></td>
                <td><?php /*echo($servingAddress->getDiameter5()); */?></td>
                <td><?php /*echo($servingAddress->getLength5()); */?></td>-->
                <td>
                    <?php echo($linkToShow); ?>
                    <br/>
                    <?php echo($linkToEdit); ?>
                </td>
            </tr>
        <?php } ?>

        </tbody>
    </table>

    <style>
        td.sa-bool {
            text-align: center;
        }
        td.sa-bool span{
            font-weight: bold;
            font-size: 20px;
        }
        td.sa-bool.sa-true span::before {
            content: "\2713";
            color: green;
        }
        td.sa-bool.sa-false span::before {
            content: "\2014";
            color: red;
        }
        td.sa-bool.sa-null span::before {
            content: "";
        }
        th.sa-center {
            text-align: center !important;
        }
    </style>


    <br/>
    <?php echo($linkToCreate); ?>


</div>


<script type="text/javascript">

    $(function () {
        $.typeahead({
            input: '.serving-addresses-form-input',
            minLength: 1,
            order: "asc",
            dynamic: true,
            delay: 300,
            backdrop: {
                "background-color": "#fff"
            },
            template: function (query, item) {

                return '<span class="row">' +
                    '<span class="id">(ID {{id}}) -  </span>' +
                    'Adresse: <span class="zip_code">{{zip_code}} </span>' +
                    '<span class="city">{{city}}, </span>' +
                    '<span class="street">{{street}} </span>' +
                    '<span class="house_number">{{house_number}}</span>' +
                    '</span>'
            },
            emptyTemplate: "Keine Ergebnisse für: {{query}}",
            source: {
                user: {
                    display: ["id", "zip_code", "city", "street", "house_number"],
                    href: "/servingaddress/{{id}}/show",
                    ajax: function (query) {
                        return {
                            type: "POST",
                            url: "<?php echo $view['router']->path('servingaddress_searchproposal') ?>",
                            path: "data.address",
                            data: {
                                search_phrase: "{{query}}"
                            },
                            callback: {
                                done: function (data) {
                                    return data;
                                }
                            }
                        }
                    }
                }
            },
            callback: {
                onClick: function (node, a, item, event) {
                    window.location.href = item.href;
                },
                onSendRequest: function (node, query) {
                    console.log('request is sent')
                },
                onReceiveRequest: function (node, query) {
                    console.log('request is received')
                }
            },
            debug: true
        });
    });

</script>