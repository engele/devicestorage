<?php
$view->extend('layout.html.php');
$linkToBack = '';
if ($view['security']->isGranted('ROLE_EDIT_SERVING_ADDRESSES')) {
    $linkToBack = sprintf('<a class="fancy-button outline grey" href="%s">Zurück</a>', $view['router']->path('servingaddress_index', ['id' => $servingAddress->getId()]));
}
?>

<div class="serving-addresses-edit IpRange box">
    <h3>Versorgungs-Adresse Ändern</h3>

    <?php if ($view['security']->isGranted('ROLE_EDIT_SERVING_ADDRESSES')) { ?>
        <div class="box">
            <?php echo $view['form']->start($edit_form); ?>
            <?php echo $view['form']->widget($edit_form); ?>
            <br/>
            <input class="fancy-button outline grey" type="submit" value="Speichern">
            <?php echo $linkToBack; ?>
            <?php echo $view['form']->end($edit_form); ?>
            <br/><br/>
        </div>
    <?php } ?>
</div>
<style>
    input {
        max-width: 240px !important;
    }
    select {
        width: 240px !important;
    }
    textarea {
        width: 240px !important;
    }
    div#appbundle_servingaddress_rollOutIsPlannedIn select {
        width: 80px !important;
    }
</style>