<?php
$view->extend('layout.html.php');
?>

<div class="serving-addresses-show IpRange box">
    <h3>Versorgungs-Adresse</h3>
    <br/>

    <?php
    /** @var AppBundle\Entity\ServingAddress $servingAddress */
    $linkToEdit = '';
    if ($view['security']->isGranted('ROLE_EDIT_SERVING_ADDRESSES')) {
        $linkToEdit = sprintf('<a class="fancy-button outline grey" href="%s">Edit</a>', $view['router']->path('servingaddress_edit', ['id' => $servingAddress->getId()]));
    }
    $linkToBack = '';
    if ($view['security']->isGranted('ROLE_EDIT_SERVING_ADDRESSES')) {
        $linkToBack = sprintf('<a class="fancy-button outline grey" href="%s">Back</a>', $view['router']->path('servingaddress_index', ['id' => $servingAddress->getId()]));
    }
    ?>

    <?php if ($view['security']->isGranted('ROLE_CREATE_SERVING_ADDRESSES')) { ?>
        <table class="IpRange">
            <tbody>
            <tr>
                <th>ID</th>
                <td><?php echo $servingAddress->getId(); ?></td>
            </tr>
            <tr>
                <th>PLZ</th>
                <td><?php echo $servingAddress->getZipCode(); ?></td>
            </tr>
            <tr>
                <th>Ort</th>
                <td><?php echo $servingAddress->getCity(); ?></td>
            </tr>
            <tr>
                <th>Ortsteil</th>
                <td><?php echo $servingAddress->getDistrict(); ?></td>
            </tr>
            <tr>
                <th>Strasse</th>
                <td><?php echo $servingAddress->getStreet(); ?></td>
            </tr>
            <tr>
                <th>Hausnr.</th>
                <td><?php echo $servingAddress->getHouseNumber(); ?></td>
            </tr>
            <tr>
                <th>Anz. der Geschäftseinheiten</th>
                <td><?php echo $servingAddress->getAmountOfBusinessUnits(); ?></td>
            </tr>
            <tr>
                <th>Anz. der Wohnungseinheiten</th>
                <td><?php echo $servingAddress->getAmountOfDwellingUnits(); ?></td>
            </tr>
            <tr>
                <th>Datum des Ausbaus</th>
                <td><?php echo $servingAddress->getRollOutIsPlannedIn() instanceof \DateTime ? $servingAddress->getRollOutIsPlannedIn()->format("d.m.Y") : null; ?></td>
            </tr>
            <tr>
                <th>Rollout beendet</th>
                <td><?php echo $servingAddress->getIsRollOutFinished() ? "ja" : "nein"; ?></td>
            </tr>
            <tr>
                <th>Hausanschluss gelegt</th>
                <td><?php echo $servingAddress->getIsBuildingConnected() ? "ja" : "nein"; ?></td>
            </tr>
            <tr>
                <th>Ne4 gelegt</th>
                <td><?php echo $servingAddress->getIsNe4Finished() ? "ja" : "nein"; ?></td>
            </tr>
            <tr>
                <th>Gnv vorhanden</th>
                <td><?php echo $servingAddress->getHasGnv() ? "ja" : "nein"; ?></td>
            </tr>
            <tr>
                <th>Gnv Anmerkung</th>
                <td><?php echo $servingAddress->getGnvNote(); ?></td>
            </tr>
            <tr>
                <th>Hatte zuvor Internetvertrag</th>
                <td><?php echo $servingAddress->getHasEverHadAnInternetSupplyContract() ? "ja" : "nein"; ?></td>
            </tr>
            <tr>
                <th>Hatte zuvor Basis TV-Vertrag</th>
                <td><?php echo $servingAddress->getHasEverHadABasicTvSupplyContract() ? "ja" : "nein"; ?></td>
            </tr>
            <tr>
                <th>Kvz Nr.</th>
                <td><?php echo $servingAddress->getConnectedToKvz(); ?></td>
            </tr>
            <tr>
                <th>Techn. Hinweis</th>
                <td><?php echo $servingAddress->getTechnicalNote(); ?></td>
            </tr>
            <tr>
                <th>Flur / Kataster</th>
                <td><?php echo $servingAddress->getFieldAndCadastre(); ?></td>
            </tr>
            <tr>
                <th>Bemerkung</th>
                <td><?php echo nl2br($servingAddress->getNote()); ?></td>
            </tr>
            <tr>
                <th>Dämpfung</th>
                <td><?php echo($servingAddress->getDamping()); ?></td>
            </tr>
            <tr>
                <th>Durchmesser 1</th>
                <td><?php echo($servingAddress->getDiameter1()); ?></td>
            </tr>
            <tr>
                <th>Länge 1</th>
                <td><?php echo($servingAddress->getLength1()); ?></td>
            </tr>
            <tr>
                <th>Durchmesser 2</th>
                <td><?php echo($servingAddress->getDiameter2()); ?></td>
            </tr>
            <tr>
                <th>Länge 2</th>
                <td><?php echo($servingAddress->getLength2()); ?></td>
            </tr>
            <tr>
                <th>Durchmesser 3</th>
                <td><?php echo($servingAddress->getDiameter3()); ?></td>
            </tr>
            <tr>
                <th>Länge 3</th>
                <td><?php echo($servingAddress->getLength3()); ?></td>
            </tr>
            <tr>
                <th>Durchmesser 4</th>
                <td><?php echo($servingAddress->getDiameter4()); ?></td>
            </tr>
            <tr>
                <th>Länge 4</th>
                <td><?php echo($servingAddress->getLength4()); ?></td>
            </tr>
            <tr>
                <th>Durchmesser 5</th>
                <td><?php echo($servingAddress->getDiameter5()); ?></td>
            </tr>
            <tr>
                <th>Länge 5</th>
                <td><?php echo($servingAddress->getLength5()); ?></td>
            </tr>
            </tbody>
        </table>

        <br/>
        <?php echo $view['form']->start($delete_form); ?>
        <?php echo $linkToEdit; ?> <input class="fancy-button outline grey" type="submit" value="Delete">
        <?php echo $linkToBack; ?>
        <?php echo $view['form']->end($delete_form); ?>

    <?php } ?>

</div>