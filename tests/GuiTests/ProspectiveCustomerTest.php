<?php

namespace Tests\GuiTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DomCrawler\Form;

/**
 * Test covers the creating of a new prospective customer
 * as well as the necessary stages before prospective customer
 * can be converted to an actual customer and the convertig process.
 */
class ProspectiveCustomerTest extends WebTestCase
{
    /**
     * Instance of client
     */
    protected static $client;

    /**
     * Internal id of created prospective customer
     * 
     * @var integer
     */
    protected static $prospectiveCustomerId;

    /**
     * Constructor
     * 
     * Create a loged in client
     */
    public static function setUpBeforeClass()
    {
        if (null === static::$client) {
            static::$client = static::createClient();
            
            self::logClientIn();
        }
    }

    /**
     * Loged client in
     */
    public static function logClientIn()
    {
        $crawler = static::$client->request('GET', '/login');
        $form = $crawler->selectButton('Login')->first()->form();

        $form['_username']->setValue('admin');
        $form['_password']->setValue('admin');
        
        static::$client->insulate();

        $crawler = static::$client->submit($form);
        $crawler = static::$client->followRedirect();

        return static::$client;
    }

    /**
     * Set form values
     */
    protected function setFormValues($form, $fieldsAndValues)
    {
        $changedFields = '|';

        foreach ($fieldsAndValues as $key => $value) {
            $this->assertTrue(isset($form[$key]));

            $changedFields .= $key.'|';

            $field = $form[$key];

            if ($field instanceof \Symfony\Component\DomCrawler\Field\ChoiceFormField) {
                $field->select($value);

                continue;
            }

            $field->setValue($value);
        }
        
        $this->assertTrue(isset($form['changed_fields']));

        $form['changed_fields']->setValue($changedFields);
    }

    /**
     * Set some dummy test data to prospective customer form
     * 
     * @param \Symfony\Component\DomCrawler\Form
     * 
     * @return \Symfony\Component\DomCrawler\Form
     */
    public function setProspectiveCustomerTestData(Form $form) : Form
    {
        $connectionCity = $form['CustomerConnectionCity']->availableOptionValues();
        $connectionCity = $connectionCity[1]; // first not empty value

        $district = $form['CustomerDistrict']->availableOptionValues();
        $district = $district[1]; // first not empty value

        $networkId = $form['CustomerNetworkId']->availableOptionValues();
        $networkId = $networkId[1]; // first not empty value

        $fieldsAndValues = [
            'CustomerCustomerId' => 'test external Customer-Id',
            'CustomerVoucher' => 'test voucher',
            'CustomerPhoneareacode' => '0123',
            'CustomerPhonenumber' => '4567',
            'CustomerFax' => '890',
            'CustomerMobilephone' => '987654',
            'CustomerEmailaddress' => 'test@wisotel.com',
            'CustomerBirthday' => '07.11.2018',
            'CustomerTitle' => 'Herr und Frau',
            'CustomerFirstname' => 'test Firstname',
            'CustomerLastname' => 'test Lastname',
            'CustomerCompany' => 'test Company',
            'CustomerStreet' => 'test Street',
            'CustomerStreetno' => 'Str.no',
            'CustomerZipcode' => '159753',
            'CustomerCity' => 'Test-City',
            'CustomerComment' => "Test\nMultiline\nComment",
            'CustomerConnectionStreet' => 'test ConnectStreet',
            'CustomerConnectionStreetno' => 'ConStrno',
            'CustomerConnectionZipcode' => '357951',
            'CustomerConnectionCity' => $connectionCity,
            'CustomerDistrict' => $district,
            'CustomerNetworkId' => $networkId,
            'CustomerProspectSupplyStatus' => 'with_supply',
        ];

        $this->setFormValues($form, $fieldsAndValues);

        return $form;
    }

    /**
     * Test that each prospective customer gets a unique clientid, even if same form is submitted twice.
     */
    public function testProspectiveCustomerClientIdWontBeUsedMultipleTimes()
    {
        $crawler1 = static::$client->getCrawler();
        $link1 = $crawler1->selectLink('Neuer Interessent');
        $crawler1 = static::$client->click($link1->link());
        $button1 = $crawler1->selectButton('save');
        $form1 = $button1->form();

        $crawler2 = static::$client->getCrawler();
        $link2 = $crawler2->selectLink('Neuer Interessent');
        $crawler2 = static::$client->click($link2->link());
        $button2 = $crawler2->selectButton('save');
        $form2 = $button2->form();

        $this->setProspectiveCustomerTestData($form1);
        $this->setProspectiveCustomerTestData($form2);

        $crawler1 = static::$client->submit($form1);
        $crawler1 = static::$client->followRedirect();
        $headlineText1 = $crawler1->filter('h1')->first()->text();

        $crawler2 = static::$client->submit($form2);
        $crawler2 = static::$client->followRedirect();
        $headlineText2 = $crawler2->filter('h1')->first()->text();
        
        $this->assertEquals(1, preg_match('/ID (00000\.\d{2}.\d{4,})/', $headlineText1, $match1));
        $this->assertEquals(1, preg_match('/ID (00000\.\d{2}.\d{4,})/', $headlineText2, $match2));

        $this->assertNotEquals($match1[1], $match2[1], 'Two prospective customers got the same clientid');
    }

    /**
     * Test create new prospective customer
     */
    public function testCreateProspectiveCustomer()
    {
        $crawler = static::$client->getCrawler();

        $link = $crawler->selectLink('Neuer Interessent');

        $this->assertEquals(1, $link->count());

        $crawler = static::$client->click($link->link());

        $button = $crawler->selectButton('save');

        $this->assertEquals(1, $button->count());

        $form = $button->form();

        $this->setProspectiveCustomerTestData($form);

        $crawler = static::$client->submit($form);
        $crawler = static::$client->followRedirect();

        $headlineText = $crawler->filter('h1')->first()->text();
        
        $this->assertContains('Kundendaten wurden gesichert', $headlineText);
        $this->assertRegExp('/ID 00000\.\d{2}.\d{4,}/', $headlineText);

        $match = null;

        $this->assertEquals(1, preg_match('/id=(\d+)/', $crawler->getUri(), $match));

        static::$prospectiveCustomerId = $match[1];
    }

    /**
     * Test complete porting stage
     */
    public function testCompletePortingStage()
    {
        $this->assertTrue(null !== static::$prospectiveCustomerId);

        $crawler = static::$client->request('GET', '/index.php?menu=customer&func=vertrag&id='.static::$prospectiveCustomerId);

        $portingStatusNode = $crawler->filter('div.CustStatus')->first()->filter('p')->reduce(function ($node) {
            return false !== strpos($node->text(), 'Portierung:');
        });

        $this->assertEquals(1, $portingStatusNode->count());
        $this->assertContains('Vertragsstufe nicht abgeschlossen', $portingStatusNode->text());

        $form = $crawler->selectButton('save')->first()->form();

        $fieldsAndValues = [
            'CustomerContractSentDate' => '05.11.2018',
            'CustomerContractReceivedDate' => '07.11.2018',
        ];

        $this->setFormValues($form, $fieldsAndValues);

        $crawler = static::$client->submit($form);
        $crawler = static::$client->followRedirect();
        
        $headlineText = $crawler->filter('h1')->first()->text();
        
        $this->assertContains('Kundendaten wurden gesichert', $headlineText);

        $button = $crawler->selectButton('Interessent in Kunde umwandeln');
        
        $this->assertEquals(1, $button->count());
    }

    /**
     * Test convert to actual customer
     */
    public function testConvertToCustomer()
    {
        $this->assertTrue(null !== static::$prospectiveCustomerId);

        $crawler = static::$client->request('POST', '/customer/ajaxChange2Customer.php', [
            'CustomerId' => static::$prospectiveCustomerId,
        ]);

        $response = static::$client->getResponse();

        $this->assertTrue($response->isSuccessful());

        $responseData = json_decode($response->getContent(), true);

        $this->assertTrue(isset($responseData['status']));
        $this->assertTrue($responseData['status'], sprintf('status was not true for customer-id %s', 
            static::$prospectiveCustomerId
        ));

        $crawler = static::$client->request('GET', '/index.php?menu=customer&func=vertrag&id='.static::$prospectiveCustomerId);

        $headlineText = $crawler->filter('h1')->first()->text();
        
        $match = null;
        
        $this->assertEquals(1, preg_match('/ID (\d{5,})\.\d{2}.\d{4,}/', $headlineText, $match));

        $this->assertTrue((int) $match[1] > 0);
    }
}
