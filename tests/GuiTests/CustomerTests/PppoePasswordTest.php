<?php

namespace Tests\GuiTests\CustomerTest;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Test tries to change a customers PPPoE password which must not be possible
 */
class PppoePasswordTest extends WebTestCase
{
    /**
     * Instance of client
     */
    protected static $client;

    /**
     * Instance of doctrine
     */
    protected static $doctrine;

    /**
     * customer.id
     */
    protected static $customerId;

    /**
     * Customers password at beginning of testing
     */
    protected static $initialPppoePassword;

    /**
     * Constructor
     * 
     * Create a loged in client
     */
    public static function setUpBeforeClass()
    {
        if (null === static::$client) {
            static::$client = static::createClient();
            
            self::logClientIn();

            static::$doctrine = static::$client->getContainer()->get('doctrine');

            $db = static::$doctrine->getManager()->getConnection();

            $result = $db->query("SELECT `id`, `password` FROM `customers` ORDER BY `id` DESC LIMIT 1");
            
            $customer = $result->fetch();

            static::$customerId = $customer['id'];
            static::$initialPppoePassword = $customer['password'];
        }
    }

    /**
     * Loged client in
     */
    public static function logClientIn()
    {
        $crawler = static::$client->request('GET', '/login');
        $form = $crawler->selectButton('Login')->first()->form();

        $form['_username']->setValue('admin');
        $form['_password']->setValue('admin');
        
        static::$client->insulate();

        $crawler = static::$client->submit($form);
        $crawler = static::$client->followRedirect();

        return static::$client;
    }

    /**
     * Test try to change PPPoE password by just sending a new one
     */
    public function testPppoePasswordMustNotBeChangableBySendingNew()
    {
        $db = static::$doctrine->getManager()->getConnection();

        $crawler = static::$client->request('GET', '/index.php?menu=customer&id='.static::$customerId);

        $form = $crawler->selectButton('save')->first()->form();

        $form['CustomerPassword']->setValue('1234');

        $crawler = static::$client->submit($form);
        $crawler = static::$client->followRedirect();

        $result = $db->query("SELECT `password` FROM `customers` WHERE `id` = ".static::$customerId);

        $pppoePassword = $result->fetch();

        $this->assertEquals(static::$initialPppoePassword, $pppoePassword['password']);
    }

    /**
     * Test try to change PPPoE password by sending an empty value
     */
    public function testPppoePasswordMustNotBeChangableBySendingEmpty()
    {
        $db = static::$doctrine->getManager()->getConnection();

        $crawler = static::$client->request('GET', '/index.php?menu=customer&id='.static::$customerId);

        $form = $crawler->selectButton('save')->first()->form();

        $form['CustomerPassword']->setValue('');

        $crawler = static::$client->submit($form);
        $crawler = static::$client->followRedirect();

        $result = $db->query("SELECT `password` FROM `customers` WHERE `id` = ".static::$customerId);

        $pppoePassword = $result->fetch();

        $this->assertEquals(static::$initialPppoePassword, $pppoePassword['password']);
    }

    /**
     * Test try to change PPPoE password by not sending the field at all
     */
    public function testPppoePasswordMustNotBeChangableByNotSendingAtAll()
    {
        $db = static::$doctrine->getManager()->getConnection();

        $crawler = static::$client->request('GET', '/index.php?menu=customer&id='.static::$customerId);

        $form = $crawler->selectButton('save')->first()->form();

        $form['CustomerPassword'] = null;
        unset($form['CustomerPassword']);

        $crawler = static::$client->submit($form);
        $crawler = static::$client->followRedirect();

        $result = $db->query("SELECT `password` FROM `customers` WHERE `id` = ".static::$customerId);

        $pppoePassword = $result->fetch();

        $this->assertEquals(static::$initialPppoePassword, $pppoePassword['password']);
    }
}
