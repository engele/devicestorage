<?php

namespace Tests\GuiTests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Goutte\Client;

class LoginTest extends WebTestCase
{
    public function testLogin()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('Login')->first()->form();

        $this->assertTrue(isset($form['_username']));
        $this->assertTrue(isset($form['_password']));

        $form['_username']->setValue('admin');
        $form['_password']->setValue('admin');
        
        $client->insulate();

        $crawler = $client->submit($form);
        $crawler = $client->followRedirect();

        $this->assertContains('Übersicht', $crawler->filter('h1.OvrList')->first()->text());
    }
}
