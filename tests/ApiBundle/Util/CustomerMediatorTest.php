<?php

/**
 *
 */

namespace Tests\ApiBundle\Util;

use PHPUnit\Framework\TestCase;
use ApiBundle\Util\CustomerMediator;
use ApiBundle\Util\DataMediator;
use AppBundle\Entity\Customer;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\Mapping as ORM;

/**
 * Dummy class to simulate an instance of \AppBundle\Entity\Customer
 */
class CustomerX extends Customer
{
    /**
     * @ORM\Column(type="date")
     */
    public $oneDate;

    /**
     * @ORM\Column(type="datetime")
     */
    public $secondDate;

    /**
     * @ORM\Column(type="date")
     */
    public $lastDate;

    public function setOneDate(\DateTime $dateTime)
    {
        $this->oneDate = $dateTime;
    }

    public function setSecondDate(\DateTime $dateTime)
    {
        $this->secondDate = $dateTime;
    }

    public function setLastDate(\DateTime $dateTime)
    {
        $this->lastDate = $dateTime;
    }
}

/**
 * Tests for CustomerMediator
 */
class CustomerMediatorTest extends TestCase
{
    /**
     * Test setDateMediators functionality
     */
    public function testSetDateMediators()
    {
        $customer = new CustomerX();
        $doctrine = $this->createMock(Registry::class);

        $customerMediator = new CustomerMediator($customer, $doctrine);
        $customerMediator->setOneDate('2018-09-21');
        $customerMediator->setSecondDate('2018-09-21');
        $customerMediator->setLastDate('now');

        $this->assertInstanceOf(\DateTime::class, $customer->oneDate);
        $this->assertInstanceOf(\DateTime::class, $customer->secondDate);
        $this->assertInstanceOf(\DateTime::class, $customer->lastDate);
    }

    /**
     * Test convertPriceToFloat with propper values
     */
    public function testConvertPriceToFloatWithPropperValues()
    {
        $customerMediator = $this->createCustomerMediatorObject();

        $this->assertTrue(is_float($customerMediator->convertPriceToFloat(1)));
        $this->assertTrue(is_float($customerMediator->convertPriceToFloat('1.0')));
        $this->assertTrue(is_float($customerMediator->convertPriceToFloat(1.0)));

        $this->assertEquals($customerMediator->convertPriceToFloat(1.0), 1.0);
        $this->assertEquals($customerMediator->convertPriceToFloat('1.0'), 1.0);
        $this->assertEquals($customerMediator->convertPriceToFloat('1.1'), 1.1);
        $this->assertEquals($customerMediator->convertPriceToFloat(1), 1.0);
    }

    /**
     * Test convertPriceToFloat with incorrect values (boolean)
     */
    public function testConvertPriceToFloatWithBoolean()
    {
        $this->expectException(\InvalidArgumentException::class);

        $customerMediator = $this->createCustomerMediatorObject();

        $customerMediator->convertPriceToFloat(true);
    }

    /**
     * Test convertPriceToFloat with incorrect values (null)
     */
    public function testConvertPriceToFloatWithNull()
    {
        $this->expectException(\InvalidArgumentException::class);

        $customerMediator = $this->createCustomerMediatorObject();

        $customerMediator->convertPriceToFloat(null);
    }

    /**
     * Test convertPriceToFloat with incorrect values (empty string)
     */
    public function testConvertPriceToFloatWithEmptySting()
    {
        $this->expectException(\InvalidArgumentException::class);

        $customerMediator = $this->createCustomerMediatorObject();

        $customerMediator->convertPriceToFloat('');
    }

    /**
     * @todo
     */
    /*public function test__construct()
    {
        //$customer = $this->createMock(Customer::class);
        //$doctrine = $this->createMock(Registry::class);
    }*/

    /**
     * Test getCustomer
     */
    public function testGetCustomer()
    {
        $customer = $this->createMock(Customer::class);
        $doctrine = $this->createMock(Registry::class);

        $customerMediator = new CustomerMediator($customer, $doctrine);

        $this->assertEquals($customer, $customerMediator->getCustomer());
    }

    /**
     * Create CustomerMediator object with mock object
     * 
     * @return CustomerMediator
     */
    public function createCustomerMediatorObject() : CustomerMediator
    {
        $customer = $this->createMock(Customer::class);
        $doctrine = $this->createMock(Registry::class);

        return new CustomerMediator($customer, $doctrine);
    }
}
