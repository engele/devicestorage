<?php

/**
 *
 */

namespace Tests\ApiBundle\Util;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use ApiBundle\Util\ApiRequest;

/**
 * 
 */
class ApiRequestTest extends TestCase
{
    /**
     * Tests __construct methods default values
     */
    public function test__constructDefaults()
    {
        $this->assertInstanceOf(ApiRequest::class, new ApiRequest());
    }

    /**
     * Tests __construct method
     */
    public function test__construct()
    {
        $requestMock = $this->getMockBuilder(Request::class)->getMock();
        $requestMock->method('getContent')->willReturn('yes');

        $apiRequest = new ApiRequest($requestMock, $originalRequestContent = ['yyy' => 'yes2'], $values = ['zzz' => 'yes3']);

        $this->assertEquals($apiRequest->getRequest()->getContent(), 'yes');
        $this->assertEquals($apiRequest->values['zzz'], 'yes3');

        $returnedOriginalRequestContent = $apiRequest->getOriginalRequestContent();

        $this->assertEquals($returnedOriginalRequestContent['yyy'], 'yes2');
    }

    /**
     * Tests initialize method
     */
    public function testInitialize()
    {
        $requestMock = $this->getMockBuilder(Request::class)->getMock();
        $apiRequest = new ApiRequest($requestMock);

        $requestMock2 = $this->getMockBuilder(Request::class)->getMock();
        $requestMock2->method('getContent')->willReturn('yes');

        $apiRequest->initialize($requestMock2, $originalRequestContent = ['yyy' => 'yes2'], $values = ['zzz' => 'yes3']);

        $this->assertEquals($apiRequest->getRequest()->getContent(), 'yes');
        $this->assertEquals($apiRequest->values['zzz'], 'yes3');

        $returnedOriginalRequestContent = $apiRequest->getOriginalRequestContent();

        $this->assertEquals($returnedOriginalRequestContent['yyy'], 'yes2');
    }

    /**
     * Tests createFromRequest method
     */
    public function testCreateFromRequest()
    {
        $requestMock = $this->getMockBuilder(Request::class)->getMock();
        $requestMock->expects($this->once())->method('getContentType')->willReturn('json');
        $requestMock->expects($this->once())->method('getContent')->willReturn(json_encode(
            [
                'values' => [
                    'id' => 345,
                ],
            ]
        ));

        $this->assertInstanceOf(ApiRequest::class, ApiRequest::createFromRequest($requestMock));
    }

    /**
     * Tests getCustomerIdFromValuesOrException method
     */
    public function testGetCustomerIdFromValuesOrException()
    {
        $requestMock = $this->getMockBuilder(Request::class)->getMock();
        $requestMock->expects($this->once())->method('getContentType')->willReturn('json');
        $requestMock->expects($this->once())->method('getContent')->willReturn(json_encode(
            [
                'values' => [
                    'id' => 123,
                ],
            ]
        ));

        $apiRequest = ApiRequest::createFromRequest($requestMock);

        $this->assertEquals($apiRequest->getCustomerIdFromValuesOrException(), 123);
    }

    /**
     * Tests getValueOrNull method
     */
    public function testGetValueOrNull()
    {
        $requestMock = $this->getMockBuilder(Request::class)->getMock();
        $requestMock->expects($this->once())->method('getContentType')->willReturn('json');
        $requestMock->expects($this->once())->method('getContent')->willReturn(json_encode(
            [
                'values' => [
                    'xxx' => 'ppp',
                ],
            ]
        ));

        $apiRequest = ApiRequest::createFromRequest($requestMock);

        $this->assertEquals($apiRequest->getValueOrNull('xxx'), 'ppp');
        $this->assertEquals($apiRequest->getValueOrNull('zzz'), null);
    }

    /**
     * Already tested in testInitialize and test__construct
     */
    /*public function testGetOriginalRequestContent()
    {
    }*/

    /**
     * Already tested in testInitialize and test__construct
     */
    /*public function testGetRequest()
    {
    }*/
}
