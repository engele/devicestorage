<?php

/**
 *
 */

namespace Tests\ApiBundle\Util;

use PHPUnit\Framework\TestCase;
use ApiBundle\Util\DataMediator;

/**
 * 
 */
class DataMediatorTest extends TestCase
{
    /**
     * Test return value og setMediationParty
     */
    public function testSetMediationParty()
    {
        $dataMediator = new DataMediator();

        $return = $dataMediator->setMediationParty(new \stdClass());

        $this->assertTrue($return instanceof DataMediator);
    }

    /**
     * Test return value of getMediationParty
     */
    public function testGetMediationParty()
    {
        $mediationParty = new \stdClass();
        $mediationParty->xyz = uniqid();

        $dataMediator = new DataMediator();
        $dataMediator->setMediationParty($mediationParty);

        $this->assertEquals($mediationParty, $dataMediator->getMediationParty());
    }

    /**
     * Test setMediationCallback expects callable as $callback parameter
     */
    public function testSetMediationCallbackExpectsCallable()
    {
        $this->expectException(\TypeError::class);

        $dataMediator = new DataMediator();

        $dataMediator->setMediationCallback('string', null);
    }

    /**
     * Test return value of setMediationCallback
     */
    public function testSetMediationCallback()
    {
        $dataMediator = new DataMediator();

        $return = $dataMediator->setMediationCallback('string', function() {});

        $this->assertTrue($return instanceof DataMediator);
    }

    /**
     * Test the __class method using mediation callback
     */
    public function test__callUsingMediationCallback()
    {
        $dataMediator = new DataMediator();
        $dataMediator->setMediationCallback('someMethodName', function() {
            return [
                'yxz',
                'abc',
            ];
        });

        $mediationParty = new class extends TestCase {
            public $wasCalled = false;

            public function someMethodName($param1, $param2)
            {
                $this->assertEquals('yxz', $param1);
                $this->assertEquals('abc', $param2);

                $this->wasCalled = true;
            }
        };

        $dataMediator->setMediationParty($mediationParty);
        $dataMediator->someMethodName();

        $this->assertTrue($mediationParty->wasCalled);
    }

    /**
     * Test the __class method using mediation callback with none array return value
     */
    public function test__callUsingMediationCallbackWithNoneArrayReturnValue()
    {
        $this->expectException(\InvalidArgumentException::class);

        $dataMediator = new DataMediator();
        $dataMediator->setMediationCallback('someMethodName', function() {
            return null;
        });

        $mediationParty = new class extends TestCase {
            public function someMethodName()
            {
            }
        };

        $dataMediator->setMediationParty($mediationParty);
        $dataMediator->someMethodName();
    }

    /**
     * Test the __class method without mediation callback
     */
    public function test__callWithoutMediationCallback()
    {
        $dataMediator = new DataMediator();

        $mediationParty = new class extends TestCase {
            public $wasCalled = false;

            public function someMethodName($param1)
            {
                $this->assertEquals('yxz', $param1);

                $this->wasCalled = true;
            }
        };

        $dataMediator->setMediationParty($mediationParty);
        $dataMediator->someMethodName('yxz');

        $this->assertTrue($mediationParty->wasCalled);
    }
}
