<?php

namespace Tests\GuiTests\CustomerTest;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use AppBundle\Util\LineIdentifierGenerator;

/**
 * Test api version 1.0.0
 */
class ApiDefaultControllerTest extends WebTestCase
{
    /**
     * Instance of client
     */
    protected static $client;

    /**
     * Instance of doctrine
     */
    protected static $doctrine;

    /**
     * customer.id
     */
    protected static $customerId;

    /**
     * Constructor
     * 
     * Create a loged in client
     */
    public static function setUpBeforeClass()
    {
        if (null === static::$client) {
            static::$client = static::createClient();
            static::$doctrine = static::$client->getContainer()->get('doctrine');
        }
    }

    /**
     * Create an api post request
     * 
     * @param string $uri
     * @param array $values
     * 
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function createApiPostRequest($uri, array $values) : \Symfony\Component\DomCrawler\Crawler
    {
        return static::$client->request(
            'POST',
            $uri,
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode($values)
        );
    }

    /**
     * Verifies the default api response parameters
     * 
     * @param string $content
     * 
     * @return array - json decoded content
     */
    public function verifyApiResponse($content) : array
    {
        $content_ = $content;

        $content = json_decode($content, true);

        $this->assertEquals(json_last_error(), JSON_ERROR_NONE, sprintf('response was not json-formated: %s', 
            $content_
        ));

        $this->assertEquals($content['status'], 'success', sprintf('a non-success status was returned: %s',
            $content_
        ));

        $this->assertTrue(isset($content['id'])); // response includes internal id
        $this->assertTrue(isset($content['clientId'])); // response includes public client id

        return $content;
    }

    /**
     * Tests createProspectiveCustomerAction method
     */
    public function testCreateProspectiveCustomerAction()
    {
        $db = static::$doctrine->getManager()->getConnection();
        $customClientIdPrefix = '00001';

        $crawler = $this->createApiPostRequest('/api/1.0.0/create-prospective-customer', [
            'values' => [
                'customClientIdPrefix' => $customClientIdPrefix,
                'externalId2' => 'some-test-name',
                'company' => 'Dummy Company',
                'title' => 'Herr und Frau',
                'firstname' => 'Häns',
                'lastname' => 'Müller',
                'clientType' => 'non_commercial',
                'emailaddress' => 'hans@mueller.de',
            ],
        ]);

        $content = $this->verifyApiResponse(static::$client->getResponse()->getContent());

        $this->assertTrue(0 === strpos($content['clientId'], $customClientIdPrefix)); // client id starts with $customerClientIdPrefix

        static::$customerId = $content['id']; // remember this customer id for further tests
    }

    /**
     * Tests update-customer with networkId, locationId, cardId and dslamPort
     */
    public function testUpdateCustomerAction()
    {
        $this->assertFalse(empty(static::$customerId));

        $db = static::$doctrine->getManager()->getConnection();

        // get some technical data to use for testing
        $result = $db->query("SELECT n.`id` as networkId, l.`id` as locationId, c.`id` as cardId, c.`card_type_id`, c.`Line`, cp.`number` as portNumber 
            FROM `networks` n 
            INNER JOIN `locations` l ON l.`network_id` = n.`id` 
            INNER JOIN `cards` c ON c.`location_id` = l.`id` 
            INNER JOIN `card_ports` cp ON cp.`card_id` = c.`id` 
            LIMIT 1
        ");

        $networkData = $result->fetch();

        $dates = [
            'patchDate' => null,
            'talOrderAckDate' => null,
            'creditRatingDate' => null,
            'contractSentDate' => null,
            'contractReceivedDate' => null,
        ];

        $counter = 0;

        foreach ($dates as $key => $null) {
            $dates[$key] = new \DateTime('now + '.$counter++.' days');
        }

        // get a product to use for testing
        $result = $db->query("SELECT `produkt` 
            FROM `produkte` 
            WHERE `card_type_id` = '".$networkData['card_type_id']."' OR `card_type_id` IS NULL 
            LIMIT 1
        ");

        $prooduct = $result->fetch();
        $prooduct = $prooduct['produkt'];

        // get some vlan-profiles to use for testing
        $result = $db->query("SELECT `id` 
            FROM `vlan_profiles` 
            LIMIT 2
        ");

        $vlanProfiles = $result->fetchAll();

        // set values array
        $values = [
            'id' => static::$customerId,
            'networkId' => $networkData['networkId'],
            'locationId' => $networkData['locationId'],
            'cardId' => $networkData['cardId'],
            'dslamPort' => $networkData['portNumber'],
            'talOrderDate' => 'N/E da GF',
            'talDtagAssignmentNo' => 'N/E da GF',
            'customerConnectInfoDate' => 'N/E da GF',
            'patchDate' => $dates['patchDate']->format('d.m.Y'),
            'talOrderAckDate' => $dates['talOrderAckDate']->format('d.m.Y'),
            'creditRatingDate' => $dates['creditRatingDate']->format('d.m.Y'),
            'contractSentDate' => $dates['contractSentDate']->format('d.m.Y'),
            'contractReceivedDate' => $dates['contractReceivedDate']->format('d.m.Y'),
            'creditRatingOk' => 'successful',
            'firmwareVersion' => 'auto',
            'dtagLine' => '1-2-3',
            'prospectSupplyStatus' => 'with_supply',
            'productname' => $prooduct,
            'Service' => $prooduct,
            'overrideInheritedVlanProfiles' => 1,
            'vlanProfiles' => $vlanProfiles,
        ];

        // map api keys to database column names for all simple values
        $valueKeysToDatabaseColumnNames = [
            'id' => 'id',
            'networkId' => 'network_id',
            'locationId' => 'location_id',
            'cardId' => 'card_id',
            'dslamPort' => 'dslam_port',
            'talOrderDate' => 'tal_order_date',
            'talDtagAssignmentNo' => 'tal_dtag_assignment_no',
            'customerConnectInfoDate' => 'customer_connect_info_date',
            'patchDate' => 'patch_date',
            'talOrderAckDate' => 'tal_order_ack_date',
            'creditRatingDate' => 'credit_rating_date',
            'contractSentDate' => 'contract_sent_date',
            'contractReceivedDate' => 'contract_received_date',
            'creditRatingOk' => 'credit_rating_ok',
            'firmwareVersion' => 'firmware_version',
            'dtagLine' => 'dtag_line',
            'prospectSupplyStatus' => 'prospect_supply_status',
            'productname' => 'productname',
            'Service' => 'Service',
            'overrideInheritedVlanProfiles' => 'override_inherited_vlan_profiles',
        ];

        // update customer via api
        $crawler = $this->createApiPostRequest('/api/1.0.0/update-customer', [
            'values' => $values,
        ]);

        $this->verifyApiResponse(static::$client->getResponse()->getContent());

        // get the actual customer data from database
        $result = $db->query("SELECT * FROM `customers` WHERE `id` = ".static::$customerId);
        $customer = $result->fetch();

        // compare all simple values
        foreach ($valueKeysToDatabaseColumnNames as $key => $columnName) {
            $this->assertEquals($values[$key], $customer[$columnName]);            
        }

        // compare creation of line identifier
        $this->assertEquals(
            $customer['line_identifier'],
            LineIdentifierGenerator::generate($networkData['Line'], $networkData['portNumber']),
            sprintf('line-identifiert wasn\'t propperly created : customerId: %s', static::$customerId)
        );

        // compare setting of vlan-profiles
        $result = $db->query("SELECT `vlan_profile_id` FROM `customer_vlan_profiles` WHERE `customer_id` = ".static::$customerId);

        $this->assertEquals(count($vlanProfiles), $result->rowCount());
        $this->assertEquals(
            array_column($vlanProfiles, 'id'),
            array_column($result->fetchAll(), 'vlan_profile_id')
        );
    }

    /**
     * Tests availabilityQueryAddressAction with querist = epilot
     */
    public function testavailabilityQueryAddressAction()
    {
        $db = static::$doctrine->getManager()->getConnection();

        $result = $db->query("SELECT * 
            FROM `serving_address` 
            LIMIT 1
        ");

        $this->assertEquals(1, $result->rowCount());

        $address = $result->fetch();

        $crawler = static::$client->request(
            'GET',
            '/api/1.0.0/availability/query-address',
            [
                'postal_code' => $address['zip_code'],
                'street_name' => $address['street'],
                'street_no' => $address['house_number'],
                'querist' => 'epilot',
            ],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ]
        );

        $content_ = static::$client->getResponse()->getContent();

        $content = json_decode($content_, true);

        $this->assertEquals(json_last_error(), JSON_ERROR_NONE, sprintf('response was not json-formated: %s', 
            $content_
        ));
        
        $this->assertEquals($content['items'][0]['has_gnv'], (bool) $address['has_gnv']);
        $this->assertEquals($content['items'][0]['has_house_connection'], (bool)  $address['is_building_connected']);
        // $this->assertEquals($content['available']); todo
    }
}
