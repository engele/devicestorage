<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/* moved to GuiTests 
class DefaultControllerTest extends WebTestCase
{

    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $response = $client->getResponse();
        $router = $client->getContainer()->get('router');

        // expected redirect to /login
        $this->assertTrue($response->isRedirect(
            $router->generate('login', array(), UrlGeneratorInterface::ABSOLUTE_URL)
        ));

        $this->assertEquals(302, $response->getStatusCode());

        $crawler = $client->followRedirect();

        $this->assertEquals(1, $crawler->selectButton('Login')->count());

        $form = $crawler->selectButton('Login')->first()->form();

        $this->assertTrue(isset($form['_username']));
        $this->assertTrue(isset($form['_password']));

        //$this->assertContains('Welcome to Symfony', $crawler->filter('#container h1')->text());
    }
}
*/
