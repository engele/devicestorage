<?php

/**
 *
 */

namespace Tests\ApiBundle\Util;

use PHPUnit\Framework\TestCase;
use AppBundle\Util\LineIdentifierGenerator;
use AppBundle\Entity\Location\Card;
use AppBundle\Entity\Location\CardPort;

/**
 * 
 */
class LineIdentifierGeneratorTest extends TestCase
{
    /**
     * Tests generateByStrings method
     */
    public function testGenerateByStrings()
    {
        $this->assertEquals(
            LineIdentifierGenerator::generateByStrings('abc', 'zyx', '&'),
            'abc&zyx'
        );
    }

    /**
     * Tests generateByStrings method with empty string
     */
    public function testGenerateByStrings_WithEmptyStrings()
    {
        $this->assertEquals(
            LineIdentifierGenerator::generateByStrings('', ''),
            '',
            'expected empty string as result but wasn\'t returned'
        );
    }

    /**
     * Tests generateByStrings method with default value for separator
     */
    public function testGenerateByStrings_WithDefaultSeparator()
    {
        $this->assertEquals(
            LineIdentifierGenerator::generateByStrings('abc', 'zyx'),
            'abc/zyx'
        );
    }

    /**
     * Tests generate method with strings
     */
    public function testGenerate_WithStrings()
    {
        $this->assertEquals(
            LineIdentifierGenerator::generate('abc', 'zyx', '&'),
            'abc&zyx'
        );
    }

    /**
     * Tests generate method with card
     */
    public function testGenerate_WithCard()
    {
        $cardMock = $this->getMockBuilder(Card::class)->getMock();
        $cardMock->expects($this->once())->method('getLineIdentifierPrefix')->willReturn('abc');

        $this->assertEquals(
            LineIdentifierGenerator::generate($cardMock, 'zyx', '&'),
            'abc&zyx'
        );
    }

    /**
     * Tests generate method with cardPort
     */
    public function testGenerate_WithCardPort()
    {
        $cardMock = $this->getMockBuilder(Card::class)->getMock();
        $cardMock->expects($this->once())->method('getLineIdentifierPrefix')->willReturn('abc');

        $cardPortMock = $this->getMockBuilder(CardPort::class)->getMock();
        $cardPortMock->expects($this->once())->method('getCard')->willReturn($cardMock);
        $cardPortMock->expects($this->once())->method('getNumber')->willReturn('zyx');

        $this->assertEquals(
            LineIdentifierGenerator::generate('something', $cardPortMock, '&'),
            'abc&zyx'
        );
    }

    /**
     * Tests generateByCardAndPortNumber method
     */
    public function testGenerateByCardAndPortNumber()
    {
        $cardMock = $this->getMockBuilder(Card::class)->getMock();
        $cardMock->expects($this->once())->method('getLineIdentifierPrefix')->willReturn('abc');

        $this->assertEquals(
            LineIdentifierGenerator::generateByCardAndPortNumber($cardMock, 'zyx', '&'),
            'abc&zyx'
        );
    }

    /**
     * Tests generateByCardPort method
     */
    public function testGenerateByCardPort()
    {
        $cardMock = $this->getMockBuilder(Card::class)->getMock();
        $cardMock->expects($this->once())->method('getLineIdentifierPrefix')->willReturn('abc');

        $cardPortMock = $this->getMockBuilder(CardPort::class)->getMock();
        $cardPortMock->expects($this->once())->method('getCard')->willReturn($cardMock);
        $cardPortMock->expects($this->once())->method('getNumber')->willReturn('zyx');

        $this->assertEquals(
            LineIdentifierGenerator::generateByCardPort($cardPortMock, '&'),
            'abc&zyx'
        );
    }
}
