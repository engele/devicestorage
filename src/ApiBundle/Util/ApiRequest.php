<?php

/**
 *
 */

namespace ApiBundle\Util;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 *
 */
class ApiRequest
{
    /**
     * @var Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * @var array
     */
    public $values;

    /**
     * @var array
     */
    protected $originalRequestContent;

    /**
     * Constructor
     * 
     * @param Symfony\Component\HttpFoundation\Request $request
     * @param array $originalRequestContent
     * @param array $values
     */
    public function __construct(Request $request = null, array $originalRequestContent = [], array $values = [])
    {
        $this->initialize($request, $originalRequestContent, $values);
    }

    /**
     * Initialize variables
     * 
     * @param Symfony\Component\HttpFoundation\Request $request
     * @param array $originalRequestContent
     * @param array $values
     * 
     * @return void
     */
    public function initialize(Request $request = null, array $originalRequestContent = [], array $values = [])
    {
        $this->request = $request;
        $this->originalRequestContent = $originalRequestContent;
        $this->values = $values;
    }

    /**
     * Create instance of ApiRequest from Symfony\Component\HttpFoundation\Request
     * 
     * @param Symfony\Component\HttpFoundation\Request $request
     * 
     * @throws Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * 
     * @return ApiRequest
     */
    public static function createFromRequest(Request $request)
    {
        if ('json' !== $request->getContentType()) {
            throw new BadRequestHttpException('Only application/json supported');
        }

        $content = $request->getContent();

        if (empty($content)) {
            throw new BadRequestHttpException('No content was send');
        }

        $content = json_decode($content, true);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new BadRequestHttpException(sprintf('Invalid json was send. Error-Code: %s',
                json_last_error()
            ));
        }

        return new static($request, $content, $content['values']);
    }

    /**
     * Get customer id from values
     * 
     * @throws Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * 
     * @return integer
     */
    public function getCustomerIdFromValuesOrException() : int
    {
        if (empty($this->values)) {
            throw new BadRequestHttpException('No values where send');
        }

        if (!isset($this->values['id']) || empty($this->values['id'])) {
            throw new BadRequestHttpException('No customer id was send');
        }

        return (int) $this->values['id'];
    }

    /**
     * Get a value from $this->values if it exits or null otherwise
     * 
     * @param mixed
     * 
     * @return mixed
     */
    public function getValueOrNull($identifier)
    {
        return $this->values[$identifier] ?? null;
    }

    /**
     * Get original request content
     * 
     * @return array
     */
    public function getOriginalRequestContent() : array
    {
        return $this->originalRequestContent;
    }

    /**
     * Get request
     * 
     * @return Symfony\Component\HttpFoundation\Request
     */
    public function getRequest() : Request
    {
        return $this->request;
    }
}
