<?php

/**
 * 
 */

namespace ApiBundle\Util;

use AppBundle\Entity\Customer;
use AppBundle\Entity\CustomerCrossSellingProduct;
use AppBundle\Entity\Product\CrossSellingProduct;
use Doctrine\Bundle\DoctrineBundle\Registry;
use AppBundle\Util\Configuration;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Annotations\AnnotationReader;
use AppBundle\Entity\Customer\CustomerVlanProfile;
use AppBundle\Entity\VlanProfile;
use AppBundle\Entity\Customer\TechnicalData;

/**
 * A data mediator for customer entity
 */
class CustomerMediator extends DataMediator
{
    /**
     * @var Doctrine\Bundle\DoctrineBundle\Registry
     */
    protected $doctrine;

    /**
     * 
     */
    protected function setDateMediators()
    {
        $customer = $this->getCustomer();

        $annotationReader = new AnnotationReader();
        
        $reflectionObject = new \ReflectionObject($customer);

        foreach ($reflectionObject->getProperties() as $reflectionProperty) {
            $propertyAnnotations = $annotationReader->getPropertyAnnotations($reflectionProperty);

            if (!isset($propertyAnnotations[0])) {
                continue;
            }

            switch ($propertyAnnotations[0]->type) {
                case 'date':
                case 'datetime':
                    $this->setMediationCallback('set'.ucfirst($reflectionProperty->getName()), function ($string) {
                        if (empty($string)) {
                            return [ $string ];
                        }

                        return [new \DateTime($string)];
                    });

                    break;
            }
        }
    }

    /**
     * Convert price to float.
     * Check if an valid value for a number is given.
     * 
     * @param mixed $price
     * 
     * @throws \InvalidArgumentException
     * 
     * @return float
     */
    public function convertPriceToFloat($price)
    {
        $newPrice = (float) $price;

        switch (gettype($price)) {
            case 'integer':
                $reConvertedPriceNet = (int) $newPrice;
                break;

            case 'double':
                $reConvertedPriceNet = (float) $newPrice;
                break;

            case 'string':
                $reConvertedPriceNet = (string) $newPrice;

                $price = preg_replace('/0+$/', '', $price); // strip 0 from end

                if ('.' === substr($price, -1)) {
                    $price = substr($price, 0, -1); // stip . from end
                }

                break;
        }

        if (!isset($reConvertedPriceNet)) {
            throw new \InvalidArgumentException('Invalid value for priceNet');
        }

        if ($reConvertedPriceNet !== $price) {
            throw new \InvalidArgumentException('Invalid value for priceNet');
        }

        return $newPrice;
    }

    /**
     * Constructor
     * 
     * @param Customer $customer
     */
    public function __construct(Customer $customer, Registry $doctrine)
    {
        $this->doctrine = $doctrine;

        $this->setMediationParty($customer);

        $this->setDateMediators();

        $this->setMediationCallback('setCrossSellingProducts', function (array $crossSellingProducts) {
            $arrayCollection = new ArrayCollection();

            foreach ($crossSellingProducts as $values) {
                $crossSellingProduct = null;
                $usedIdentifier = null;

                if (isset($values['productIdentifier'])) {
                    $crossSellingProduct = $this->doctrine->getRepository('AppBundle\Entity\Product\CrossSellingProduct')
                        ->findOneByIdentifier($values['productIdentifier']);

                    $usedIdentifier = $values['productIdentifier'];
                } elseif (isset($values['productId'])) {
                    $crossSellingProduct = $this->doctrine->getRepository('AppBundle\Entity\Product\CrossSellingProduct')
                        ->findOneById($values['productId']);

                    $usedIdentifier = $values['productId'];
                }

                if (null === $crossSellingProduct) {
                    if (null !== $usedIdentifier) {
                        throw new \InvalidArgumentException(sprintf('CrossSellingProduct (%s) does not exist',
                            $usedIdentifier
                        ));
                    }

                    throw new \InvalidArgumentException('Missing product. Either "productIdentifier" or "productId" must be specified');
                }

                $customerCrossSellingProduct = new CustomerCrossSellingProduct();
                $customerCrossSellingProduct->setCrossSellingProduct($crossSellingProduct);

                $taxRate = null;

                if (isset($values['taxId'])) {
                    $taxRate = $this->doctrine->getRepository('AppBundle\Entity\Product\TaxRate')
                        ->findOneById($values['taxId']);
                } else {
                    $defaultTaxRate = Configuration::get('defaultTaxRate');

                    if (null !== $defaultTaxRate) {
                        $taxRate = $this->doctrine->getRepository('AppBundle\Entity\Product\TaxRate')
                            ->findOneById($defaultTaxRate);
                    }
                }

                if (null === $taxRate) {
                    throw new \InvalidArgumentException('Invalid value for "taxId"');
                }

                $customerCrossSellingProduct->setTaxRate($taxRate);

                if (isset($values['priceNet'])) {
                    $priceNet = $this->convertPriceToFloat($values['priceNet']);

                    $customerCrossSellingProduct->setPriceNet($priceNet);

                    if (isset($values['priceGross'])) {
                        // A gross price was provided. Just use it and trust the sender.
                        $customerCrossSellingProduct->setPriceGross(
                            $this->convertPriceToFloat($values['priceGross'])
                        );
                    } else {
                        // No gross price was provided. Calculte it from net price
                        $customerCrossSellingProduct->setPriceGross(
                            $customerCrossSellingProduct->calculateGross($priceNet, $taxRate->getAmount(), 5)
                        );
                    }
                } elseif (isset($values['priceGross'])) {
                    $priceGross = $this->convertPriceToFloat($values['priceGross']);

                    $customerCrossSellingProduct->setPriceGross($priceGross);

                    $customerCrossSellingProduct->setPriceNet(
                        $customerCrossSellingProduct->calculateNet($priceGross, $taxRate->getAmount(), 5)
                    );
                } else {
                    $customerCrossSellingProduct->setPriceNet(
                        $this->convertPriceToFloat($crossSellingProduct->getPriceNet())
                    );

                    $customerCrossSellingProduct->setPriceGross(
                        $this->convertPriceToFloat($crossSellingProduct->getPriceGross())
                    );
                }

                if (isset($values['activationDate'])) {
                    $customerCrossSellingProduct->setActivationDate(new \DateTime($values['activationDate']));
                }

                if (isset($values['deactivationDate'])) {
                    $customerCrossSellingProduct->setDeactivationDate(new \DateTime($values['deactivationDate']));
                }

                $arrayCollection->add($customerCrossSellingProduct);
            }

            return [$arrayCollection];
        });

        $this->setMediationCallback('setVlanProfiles', function (array $vlanProfileIds) {
            $customer = $this->getCustomer();
            $vlanProfiles = [];

            foreach ($vlanProfileIds as $vlanProfile_) {
                $vlanProfileId = $vlanProfile_;

                if ($vlanProfile_ instanceof CustomerVlanProfile) {
                    $vlanProfile_->setCustomerId($customer->getId());

                    $vlanProfiles[] = $vlanProfile_;

                    continue;
                }

                $vlanProfile = $this->doctrine->getRepository(VlanProfile::class)->findOneById($vlanProfileId);

                if (!($vlanProfile instanceof VlanProfile)) {
                    throw new \UnexpectedValueException(sprintf('Unable to find vlan profile (%d)', $vlanProfileId));
                }

                $customerVlanProfile = $this->doctrine->getRepository(CustomerVlanProfile::class)->findOneBy([
                    'vlanProfile' => $vlanProfile,
                    'customerId' => $customer->getId(),
                ]);

                if (null !== $customerVlanProfile) {
                    $vlanProfiles[] = $customerVlanProfile;

                    continue;
                }

                $customerVlanProfile = new CustomerVlanProfile();
                $customerVlanProfile
                    ->setCustomerId($customer->getId())
                    ->setVlanProfile($vlanProfile);

                $vlanProfiles[] = $customerVlanProfile;
            }

            return [$vlanProfiles];
        });

        $this->setMediationCallback('setOverrideInheritedVlanProfiles', function ($value) {
            return [(int) $value];
        });

        $this->setMediationCallback('setTechnicalData', function ($value) {
            if ($value instanceof TechnicalData) {
                return [$value];
            }

            return [TechnicalData::createFromArray($value, $this->doctrine)];
        });
    }

    /**
     * Get customer
     * 
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->getMediationParty();
    }
}
