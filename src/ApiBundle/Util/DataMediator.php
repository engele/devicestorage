<?php

/**
 * 
 */

namespace ApiBundle\Util;

/**
 * This class enables you to simply run a "mapping"-method before actually calling 
 * the real method of the mapped object and use the return values of the 
 * callback method as input for the real method
 */
class DataMediator
{
    /**
     * Mediate for this object
     * 
     * @var object
     */
    public $mediationParty;

    /**
     * Mediation callbacks
     * - to be called befor actually method of mediation party object
     * 
     * @var array
     */
    protected $mediationCallbacks = [];

    /**
     * Set object to be mediated for
     * 
     * @param object $mappedObject
     * 
     * @return DataMediator
     */
    public function setMediationParty($mediationParty)
    {
        $this->mediationParty = $mediationParty;

        return $this;
    }

    /**
     * Get mediationParty
     * 
     * @return object
     */
    public function getMediationParty()
    {
        return $this->mediationParty;
    }

    /**
     * Set a mediation callback for a method of mappedObject
     * 
     * @param string $mediateForm
     * @param callable $callback
     * 
     * @return DataMediator
     */
    public function setMediationCallback($mediateFor, callable $callback)
    {
        $this->mediationCallbacks[$mediateFor] = $callback;

        return $this;
    }

    /**
     * @throws InvalidArgumentException|BadMethodCallException - and execptions comming from object while setting data
     * 
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        if (isset($this->mediationCallbacks[$method])) {
            $arguments = call_user_func_array($this->mediationCallbacks[$method], $arguments);

            if (!is_array($arguments)) {
                throw new \InvalidArgumentException(sprintf('the mediation method for %s must return an array',
                    $method
                ));
            }
        }

        return call_user_func_array([$this->mediationParty, $method], $arguments);
    }
}
