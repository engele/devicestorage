<?php

/**
 * 
 */

namespace ApiBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Common\Annotations\AnnotationReader;
use AppBundle\Entity\Customer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Debug\Exception\UndefinedMethodException;
use ApiBundle\Util\CustomerMediator;
use PropertyReader\PropertyReader;
use AppBundle\Entity\Network;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use AppBundle\Entity\Customer\CustomerVlanProfile;
use AppBundle\Entity\VlanProfile;
use ApiBundle\Util\ApiRequest;
use AppBundle\Util\LineIdentifierGenerator;
use AppBundle\Util\KvzSchaltnummerGenerator;
use AppBundle\Entity\Location\Card;
use AppBundle\Entity\Location\CardPort;
use AppBundle\Entity\ServingAddress;

/**
 * @Route("/api/1.0.0")
 * 
 * @todo
 *      Crosssellingproducts can not be removed from customers yet
 */
class ApiDefaultController extends Controller
{
    /**
     * 
     */
    public function verifyApiToken(Request $request)
    {
        //throw new AccessDeniedHttpException('api-token passt nicht - '.$request->headers->get('API-TOKEN'));
    }

    /**
     * @Route("/availability/{zip}/streetlist")
     * @Method("GET")
     * 
     * ladet darüber alle verfügbaren Straßennamen zu dieser Postleitzahl
     */
    public function availabilityGetStreetlistByZipAction(Request $request, $zip)
    {
        $this->verifyApiToken($request);

        $zip = filter_var($zip, FILTER_SANITIZE_NUMBER_INT);

        return new JsonResponse([
            'status' => 'success',
            'streetList' => array_column(
                $this->getDoctrine()->getRepository(ServingAddress::class)->findDistinctStreetsByZip($zip),
                'street'
            ),
        ]);
    }

    /**
     * @Route("/availability/{zip}/{street}/housenumbers")
     * @Method("GET")
     * 
     * 3. Nach Auswahl der Straße, macht ihr einen weiteren Aufruf zu unserem API und ladet
     * darüber alle verfügbaren Hausnummern und deren Verfügbarkeitsstatus zu dieser Straße und Postleitzahl
     */
    public function availabilityGetHousenumbersByZipAndStreetAction(Request $request, $zip, $street)
    {
        $this->verifyApiToken($request);

        $zip = filter_var($zip, FILTER_SANITIZE_NUMBER_INT);
        $street = addslashes($street);

        return new JsonResponse([
            'status' => 'success',
            'hausenumbers' => array_column(
                $this->getDoctrine()->getRepository(ServingAddress::class)->findDistinctHousenumbersByZipAndStreet($zip, $street),
                'houseNumber'
            ),
        ]);
    }

    /**
     * @Route("/availability/query-address")
     * @Method("GET")
     * 
     * aufruf von szilard
     * curl -X GET "https://product-availability.staging.sls.epilot.io/v1/product-availability?postal_code=asdas&organization_id=asd"
     * 
     * @todo
     *      define normal (not epilot) return
     */
    public function availabilityQueryAddressAction(Request $request)
    {
        $requiredParameters = [
            'postal_code',
            'street_name',
            'street_no',
        ];

        foreach ($requiredParameters as $parameter) {
            if (empty($request->get($parameter))) {
                return new JsonResponse(['status' => 'error', 'message' => sprintf('missing parameter or value for (%s)', $parameter)], 404);
            }
        }

        $address = $this->getDoctrine()->getRepository(ServingAddress::class)->findOneBy([
            'zipCode' => $request->get('postal_code'),
            'street' => $request->get('street_name'),
            'houseNumber' => $request->get('street_no'),
        ]);

        if (!($address instanceof ServingAddress)) {
            return new JsonResponse(['status' => 'error', 'message' => 'address not found'], 404);
        }

        if ('epilot' === $request->get('querist')) {
            $available = false;
            $availableFrom = null;

            if (false === $address->getIsRollOutFinished()) {
                if ($address->getRollOutIsPlannedIn() instanceof \DateTime) {
                    $now = new \DateTime();
                    $availableFrom = $address->getRollOutIsPlannedIn()->format(\DateTime::ISO8601);

                    if ($now->format('Y') === $address->getRollOutIsPlannedIn()->format('Y')) {
                        // this year
                        $available = true;
                    } else {
                        $now->add(new \DateInterval('P1Y'));

                        if ($now->format('Y') === $address->getRollOutIsPlannedIn()->format('Y')) {
                            // plus 1 year
                            $available = true;
                        }
                    }
                }
            } else {
                $available = true;
            }
            
            return new JsonResponse([
                'items' => [
                    [
                        'product_ids' => [],
                        'has_gnv' => $address->getHasGnv(),
                        'has_house_connection' => $address->getIsBuildingConnected(),
                        'available' => $available,
                        'available_from' => $availableFrom,
                        'type' => 'BROADBAND',
                    ],
                ],
            ]);
        }

        return new JsonResponse(['status' => 'error', 'message' => sprintf('missing parameter or value for (%s)', 'querist')], 404);
        // return new JsonResponse(['status' => 'success',]); // todo
    }

    /**
     * @Route("/delete-customer")
     * @Method("DELETE")
     */
    public function deleteCustomerAction(Request $request)
    {
        try {
            $apiRequest = $this->createApiRequestFromRequest($request);

            $customerId = $apiRequest->getCustomerIdFromValuesOrException();
            $doctrine = $this->getDoctrine();
            $db = $doctrine->getManager()->getConnection();

            $db->query("DELETE FROM `customer_history` WHERE `customer_id` = ".$customerId);
            $db->query("DELETE FROM `customer_options` WHERE `cust_id` = ".$customerId);
            $db->query("DELETE FROM `customer_terminals` WHERE `customer_id` = ".$customerId);
            $db->query("DELETE FROM `ip_addresses` WHERE `customer_id` = ".$customerId);
            $db->query("DELETE FROM `purtel_account` WHERE `cust_id` = ".$customerId);
            $db->query("DELETE FROM `customers` WHERE `id` = ".$customerId);

            return new JsonResponse(['status' => 'success', 'id' => $customerId]);
        } catch (\Exception $exception) {
            return new JsonResponse(['status' => 'error', 'message' => $exception->getMessage()]);
        }
    }

    /**
     * @Route("/delete-customer-vlan-profiles")
     * @Method("DELETE")
     */
    public function removeVlanProfilesFromCustomer(Request $request)
    {
        try {
            $apiRequest = $this->createApiRequestFromRequest($request);

            $customerId = $apiRequest->getCustomerIdFromValuesOrException();

            if (!isset($apiRequest->values['vlanProfiles']) 
                || empty($apiRequest->values['vlanProfiles']) 
                || !is_array($apiRequest->values['vlanProfiles'])) {
                throw new BadRequestHttpException('No vlan profiles where send');
            }

            $doctrine = $this->getDoctrine();

            foreach ($apiRequest->values['vlanProfiles'] as $vlanProfileId) {
                $vlanProfile = $doctrine->getRepository(VlanProfile::class)->findOneById($vlanProfileId);

                if (!($vlanProfile instanceof VlanProfile)) {
                    throw new \UnexpectedValueException(sprintf('Unable to find vlan profile (%d)', $vlanProfileId));
                }

                $customerVlanProfile = $doctrine->getRepository(CustomerVlanProfile::class)->findOneBy([
                    'vlanProfile' => $vlanProfile,
                    'customerId' => $customerId,
                ]);

                if ($customerVlanProfile instanceof CustomerVlanProfile) {
                    $doctrine->getManager()->remove($customerVlanProfile);
                }
            }

            $doctrine->getManager()->flush();

            return new JsonResponse(['status' => 'success', 'id' => $customerId]);
        } catch (\Exception $exception) {
            return new JsonResponse(['status' => 'error', 'message' => $exception->getMessage()]);
        }
    }

    /**
     * @Route("/convert-prospective-customer-to-customer")
     * @Method("POST")
     */
    public function convertProspectiveCustomerToCustomerAction(Request $request)
    {
        try {
            $apiRequest = $this->createApiRequestFromRequest($request);

            $customerId = $apiRequest->getCustomerIdFromValuesOrException();
            $doctrine = $this->getDoctrine();
            $customerQuery = $doctrine->getManager()->getConnection()->query("SELECT * FROM `customers` WHERE `id` = ".$customerId);

            if (1 !== $customerQuery->rowCount()) {
                throw new BadRequestHttpException('Invalid customer id was send');
            }

            $customerArray = $customerQuery->fetch(\PDO::FETCH_ASSOC);
            $customer = Customer::createFromArray($customerArray, $doctrine);

            if (1 !== preg_match('/^00000|1$/', $customer->getClientId())) {
                throw new BadRequestHttpException('Customer can only be converted from prospective customer to customer once');
            }

            if (empty($customer->getNetworkId())) {
                throw new BadRequestHttpException('Prospective customer must be assigned to a network');
            }

            $network = $this->getDoctrine()->getRepository('AppBundle\Entity\Network')->findOneById($customer->getNetworkId());

            if (!($network instanceof Network)) {
                throw new BadRequestHttpException('Prospective customer must be assigned to an existing network');
            }

            $clientId = $this->getNextClientId($network->getIdentifier());

            $customer->setClientId($clientId);

            if ($this->updateCustomer($customer)) {
                return new JsonResponse(['status' => 'success', 'id' => $customer->getId(), 'clientId' => $customer->getClientId()]);
            }

            throw new \RuntimeException('Unable to update customer');
        } catch (\Exception $exception) {
            return new JsonResponse(['status' => 'error', 'message' => $exception->getMessage()]);
        }
    }

    /**
     * @Route("/update-customer")
     * @Method("POST")
     */
    public function updateCustomerAction(Request $request)
    {
        try {
            $apiRequest = $this->createApiRequestFromRequest($request);

            $customerId = $apiRequest->getCustomerIdFromValuesOrException();
            $doctrine = $this->getDoctrine();
            $customerQuery = $doctrine->getManager()->getConnection()->query("SELECT * FROM `customers` WHERE `id` = ".$customerId);

            if (1 !== $customerQuery->rowCount()) {
                throw new BadRequestHttpException('Invalid customer id was send');
            }

            $customerArray = $customerQuery->fetch(\PDO::FETCH_ASSOC);
            $customer = Customer::createFromArray($customerArray, $doctrine);
            $customerBeforUpdating = clone $customer;
            $customerMediator = new CustomerMediator($customer, $this->getDoctrine());
            $customerMethods = get_class_methods(Customer::class);

            foreach ($apiRequest->values as $property => $value) {
                // to avoid "Unexpected property id in values" Exception
                if ($property === 'id') {
                    continue;
                }

                $method = 'set'.ucfirst($property);

                if (!in_array($method, $customerMethods)) {
                    throw new \BadMethodCallException(sprintf('Unexpected property %s in values (1)', $property));
                }

                $customerMediator->$method($value);
            }

            $this->handleChangesOfRelatedValues($apiRequest, $customerBeforUpdating, $customerMediator);

            $customer = $customerMediator->getCustomer();

            if ($this->updateCustomer($customer)) {
                return new JsonResponse(['status' => 'success', 'id' => $customer->getId(), 'clientId' => $customer->getClientId()]);
            }

            throw new \RuntimeException('Unable to update customer');
        } catch (\Exception $exception) {
            return new JsonResponse(['status' => 'error', 'message' => $exception->getMessage()]);
        }
    }

    /**
     * @Route("/create-prospective-customer")
     * @Method("POST")
     */
    public function createProspectiveCustomerAction(Request $request)
    {
        try {
            $apiRequest = $this->createApiRequestFromRequest($request);

            if (empty($apiRequest->values)) {
                throw new BadRequestHttpException('No values where send');
            }

            $prospectiveCustomerMediator = new CustomerMediator(new Customer(), $this->getDoctrine());

            $clientIdPrefix = '00000';

            if (isset($apiRequest->values['customClientIdPrefix'])) {
                $clientIdPrefix = $apiRequest->values['customClientIdPrefix'];

                if (!is_numeric($clientIdPrefix)) {
                    throw new \Exception('Only numeric values are allowed for customClientIdPrefix');
                }

                unset($apiRequest->values['customClientIdPrefix']);
            }

            $clientId = $this->getNextClientId($clientIdPrefix);

            $prospectiveCustomerMediator->setClientId($clientId);

            $customerMethods = get_class_methods(Customer::class);

            foreach ($apiRequest->values as $property => $value) {
                $method = 'set'.ucfirst($property);

                if (!in_array($method, $customerMethods)) {
                    throw new \BadMethodCallException(sprintf('Unexpected property %s in values (2)', $property));
                }

                $prospectiveCustomerMediator->$method($value);
            }

            $prospectiveCustomer = $prospectiveCustomerMediator->getCustomer();

            if ($this->insertCustomer($prospectiveCustomer)) {
                return new JsonResponse(['status' => 'success', 'id' => $prospectiveCustomer->getId(), 'clientId' => $clientId]);
            }

            throw new \RuntimeException('Unable to create prospective customer');
        } catch (\Exception $exception) {
            return new JsonResponse(['status' => 'error', 'message' => $exception->getMessage()]);
        }
    }

    /**
     * Handle changes of related values.
     * E.g. networkId -> locationId -> cardId -> dslamPort -> lineIdentifier
     * 
     * IMPORTANT NOTE:
     * Call this funktion AFTER updating the customers data - not before!
     * 
     * @param ApiBundle\Util\ApiRequest $apiRequest
     * @param Customer $customerBeforUpdating
     * @param ApiBundle\Util\CustomerMediator $customerMediator
     * 
     * @return void
     * 
     * @todo
     *      lsa_kvz_partition
     */
    public function handleChangesOfRelatedValues(ApiRequest $apiRequest, Customer $customerBeforUpdating, CustomerMediator $customerMediator)
    {
        if (array_key_exists('networkId', $apiRequest->values)) {
            if (0 !== strcmp($customerBeforUpdating->getNetworkId(), $customerMediator->getNetworkId())) {
                $networkIdWasChanged = true;

                $customerMediator
                    ->setLocationId($apiRequest->values['locationId'] ?? null)
                    ->setCardId($apiRequest->values['cardId'] ?? null)
                    ->setDslamPort($apiRequest->values['dslamPort'] ?? null)
                    ->setLineIdentifier(null)
                    ->setKvzToggleNo(null)
                ;
            }
        }

        if (!isset($networkIdWasChanged) && array_key_exists('locationId', $apiRequest->values)) {
            if (0 !== strcmp($customerBeforUpdating->getLocationId(), $customerMediator->getLocationId())) {
                $locationIdWasChanged = true;

                $customerMediator
                    ->setCardId($apiRequest->values['cardId'] ?? null)
                    ->setDslamPort($apiRequest->values['dslamPort'] ?? null)
                    ->setLineIdentifier(null)
                ;
            }
        }

        if (!isset($locationIdWasChanged) && array_key_exists('cardId', $apiRequest->values)) {
            if (0 !== strcmp($customerBeforUpdating->getCardId(), $customerMediator->getCardId())) {
                $cardIdWasChanged = true;

                $customerMediator
                    ->setDslamPort($apiRequest->values['dslamPort'] ?? null)
                    ->setLineIdentifier(null)
                ;
            }
        }

        if (array_key_exists('dslamPort', $apiRequest->values)) {
            $customersCardPort = $apiRequest->values['dslamPort'];

            if (!empty($customersCardPort)) {
                $doctrine = $this->getDoctrine();
                $customersCard = $doctrine->getRepository(Card::class)->findOneById($customerMediator->getCardId());

                if (!($customersCard instanceof Card)) {
                    throw new \BadMethodCallException('customer must not have a card-port without a card');
                }

                $customersCardPort = $doctrine->getRepository(CardPort::class)
                    ->findOneByCardAndNumber($customersCard, $customersCardPort);

                if (!($customersCardPort instanceof CardPort)) {
                    throw new \BadMethodCallException('invalid card port for customer');
                }

                $customerMediator->setLineIdentifier(
                    LineIdentifierGenerator::generateByCardPort($customersCardPort)
                );
            } else {
                $customerMediator->setLineIdentifier(null);
            }
        }

        if (array_key_exists('kvzToggleNo', $apiRequest->values)) {
            $customersKvzToggleNo = $apiRequest->values['kvzToggleNo'];

            if (!empty($customersKvzToggleNo)) {
                $location = $this->getDoctrine()->getRepository(Location::class)->findOneById($customerMediator->getLocationId());

                if (!($location instanceof Location)) {
                    throw new \BadMethodCallException('customer must not have a kvz schaltnummer without a location');
                }

                $customerMediator->setKvzToggleNo(
                    KvzSchaltnummerGenerator::generate($customersKvzToggleNo, $location->getKvzPrefix())
                );
            } else {
                $customerMediator->setKvzToggleNo(null);
            }
        }
    }

    /**
     * @return string
     */
    public function getNextClientId($network)
    {
        $timeZone = new \DateTimeZone('Europe/Berlin');
        $now = new \DateTime('now', $timeZone);

        $clientIdStartingString = sprintf('%s.%s.',
            $network,
            $now->format('y')
        );

        $lastClientId = 0;

        $db = $this->getDoctrine()->getManager()->getConnection();

        $lastClientIdQuery = $db->query("SELECT `clientid` FROM `customers` 
            WHERE `clientid` LIKE '".$clientIdStartingString."%' ORDER BY `clientid` DESC LIMIT 1"
        );

        if (1 === $lastClientIdQuery->rowCount()) {
            $lastClientId = $lastClientIdQuery->fetch(\PDO::FETCH_NUM);
            $lastClientId = $lastClientId[0];
            $lastClientId = explode('.', $lastClientId);
            $lastClientId = (int) end($lastClientId);
        }

        return $clientIdStartingString.str_pad(++$lastClientId, 4, "0", STR_PAD_LEFT);
    }

    /**
     * 
     */
    public function getMappingForEntity($entity)
    {
        $annotationReader = new AnnotationReader();
        
        $reflectionObject = new \ReflectionObject($entity);

        $mapping = [];

        foreach ($reflectionObject->getProperties() as $reflectionProperty) {
            $propertyAnnotations = $annotationReader->getPropertyAnnotations($reflectionProperty);

            if (empty($propertyAnnotations[0]->name)) {
                continue;
            }

            $mapping[$propertyAnnotations[0]->name] = 'get'.ucfirst($reflectionProperty->getName());
        }

        return $mapping;
    }

    /**
     * 
     */
    public function mapCustomerValuesToDatabaseColumns(Customer $customer) : array
    {
        $mapping = $this->getMappingForEntity($customer);

        foreach ($mapping as $key => $method) {
            $mapping[$key] = $customer->$method();
        }

        $canNotBeNullColumns = [
            'card_id' => 0,
            'cancel_purtel_for_date' => '',
            'stati_port_confirm_date' => '',
            'wisocontract_cancel_date' => '',
            'wisocontract_switchoff_finish' => '',
            'wisocontract_canceled_date' => '',
            'telefonbuch_eintrag' => '',
            'purtel_edit_done' => '',
            'credit_rating_date' => '',
            'dslam_arranged_date' => '',
            'customer_connect_info_date' => '',
            'customer_connect_info_from' => '',
            'customer_connect_info_how' => '',
            'exp_date_int' => '',
            'exp_date_phone' => '',
            'final_purtelproduct_date' => '',
            'gf_branch' => '',
            'ip_range_id' => 0,
            'kvz_addition' => '',
            'location_id' => 0,
            'lsa_kvz_partition_id' => 0,
            'nb_canceled_date' => '',
            'new_clientid_permission' => 0,
            'new_number_date' => '',
            'oldcontract_city' => '',
            'oldcontract_street' => '',
            'oldcontract_streetno' => '',
            'oldcontract_zipcode' => '',
            'patch_date' => '',
            'phoneentry_done_date' => '',
            'phoneentry_done_from' => '',
            'pppoe_config_date' => '',
            'prospect_supply_status' => '',
            'purtel_data_record_id' => 0,
            'purtel_edit_done_from' => '',
            'purtelproduct_advanced' => '',
            'reg_answer_date' => '',
            'registration_date' => '',
            'routing_active_date' => '',
            'service_level' => '',
            'sofortdsl' => '',
            'tal_cancel_ack_date' => '',
            'tal_cancel_date' => '',
            'tal_canceled_for_date' => '',
            'tal_order_ack_date' => '',
            'tal_order_date' => '',
            'tal_order_work' => '',
            'tal_orderd_from' => '',
            'tal_ordered_for_date' => '',
            'terminal_ready_from' => '',
            'terminal_sended_date' => '',
            'tv_service' => '',
            'ventelo_confirmation_date' => '',
            'ventelo_port_letter_date' => '',
            'ventelo_port_letter_from' => '',
            'ventelo_port_letter_how' => '',
            'ventelo_port_wish_date' => '',
            'ventelo_purtel_enter_date' => '',
            'wisocontract_cancel_input_date' => '',
            'wisocontract_cancel_input_ack' => '',
            'wisocontract_cancel_account_date' => '',
            'wisocontract_cancel_account_finish' => '',
            'wisocontract_cancel_ack' => '',
            'wisocontract_canceled_from' => '',
            'wisocontract_move' => '',
            'get_hardware_date' => '',
            'pay_hardware_date' => '',
            'wisocontract_switchoff' => '',
            'exp_done_phone' => '',
            'exp_done_int' => '',
            'tech_free_for_date' => '',
            'tech_free_date' => '',
            'cancel_purtel_date' => '',
            'cancel_tel_date' => '',
            'cancel_tel_for_date' => '',
            'vectoring' => '',
        ];

        // can not be null
        foreach ($canNotBeNullColumns as $key => $value) {
            if (null === $mapping[$key]) {
                $mapping[$key] = $value;
            }
        }

        return $mapping;
    }

    /**
     * 
     */
    public function updateCustomer(Customer $customer)
    {
        $mapping = $this->mapCustomerValuesToDatabaseColumns($customer);

        unset($mapping['id']); // remove from mapping to avoid overwriting (although it shouldn't be a problem)

        $query = sprintf('UPDATE `customers` SET %s WHERE `id` = %d',
            implode(',', array_map(function ($column, $value) {
                if (null === $value) {
                    $value = 'NULL';
                } elseif ($value === '') {
                    $value = "''";
                } elseif ($value instanceOf \DateTime) {
                    $value = sprintf("'%s'", $value->format('d.m.Y'));
                } else {
                    $value = sprintf("'%s'", $value);
                }

                return sprintf("`%s` = %s",
                    $column,
                    $value
                );
            }, array_keys($mapping), $mapping)),
            $customer->getId()
        );

        $em = $this->getDoctrine()->getManager();
        $db = $em->getConnection();

        try {
            $result = $db->query($query);

            if (false == $result || '00000' != $result->errorCode()) {
                return false;
            }

            // crossSellingProducts
            // @todo
            // at the moment this only adds more crossselling products to a customer but never removes them
            foreach ($customer->getCrossSellingProducts() as $customerCrossSellingProduct) {
                $customerCrossSellingProduct->setCustomerId($customerId);

                $em->persist($customerCrossSellingProduct);
            }

            // vlanProfiles
            // @todo
            // at the moment this only adds more vlan profiles to a customer but never removes them
            foreach ($customer->getVlanProfiles() as $customerVlanProfile) {
                $em->persist($customerVlanProfile);
            }

            $em->flush();
            
            return true;
        } catch (\Exception $exception) {
            if (isset($customerCrossSellingProduct)) {
                var_dump($customerCrossSellingProduct->getCrossSellingProduct()->getIdentifier());
            }
            die(var_dump( $exception->getMessage() ));
            return false;
        }
    }

    /**
     * 
     */
    public function insertCustomer(Customer $customer)
    {
        $mapping = $this->mapCustomerValuesToDatabaseColumns($customer);

        $query = sprintf('INSERT INTO `customers` (%s) VALUES (%s)',
            // colum names
            implode(',', array_map(function ($value) {
                return sprintf('`%s`', $value);
            }, array_keys($mapping))),

            // values
            implode(',', array_map(function ($value) {
                if (null === $value) {
                    return 'NULL';
                }

                if (empty($value) && is_string($value)) {
                    return "''";
                }

                if ($value instanceOf \DateTime) {
                    return sprintf("'%s'", $value->format('d.m.Y'));
                }

                if (is_bool($value)) {
                    return $value ? 1 : 0;
                }

                return sprintf("'%s'", $value);
            }, $mapping))
        );

        $em = $this->getDoctrine()->getManager();
        $db = $em->getConnection();

        try {
            $result = $db->query($query);

            if (false == $result || '00000' != $result->errorCode()) {
                return false;
            }

            //$customerId = & PropertyReader::newInstance()->read($customer, 'id');
            //$customerId = $db->lastInsertId();
            $customerId = & \Closure::bind(function & () {
                return $this->id;
            }, $customer, $customer)->__invoke();
            $customerId = $db->lastInsertId();

            // crossSellingProducts
            foreach ($customer->getCrossSellingProducts() as $customerCrossSellingProduct) {
                $customerCrossSellingProduct->setCustomerId($customerId);

                $em->persist($customerCrossSellingProduct);
            }

            $em->flush();
            
            return true;
        } catch (\Exception $exception) {
            if (isset($customerCrossSellingProduct)) {
                var_dump($customerCrossSellingProduct->getCrossSellingProduct()->getIdentifier());
            }
            die(var_dump( $exception->getMessage() ));
            return false;
        }
    }

    /**
     * Get (json-)decoded content from request
     * 
     * @throws Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * 
     * @return ApiBundle\Util\ApiRequest
     */
    public function createApiRequestFromRequest(Request $request) : ApiRequest
    {
        return ApiRequest::createFromRequest($request);
    }

    /**
     * Get spectrum profiles
     * 
     * @Route("/spectrum-profiles")
     * @Method("GET")
     */
    public function getSpectrumProfilesAction()
    {
        $spectrumProfiles = $doctrine = $this->getDoctrine()->getRepository(\AppBundle\Entity\SpectrumProfile::class)->findAll();

        $spectrumProfilesArray = [];

        foreach ($spectrumProfiles as $spectrumProfile) {
            $spectrumProfilesArray[] = [
                'id' => $spectrumProfile->getId(),
                'identifier' => $spectrumProfile->getIdentifier(),
                'profile' => $spectrumProfile->getProfile(),
                'connectionType' => $spectrumProfile->getConnectionType()->getName(),
            ];
        }

        return new JsonResponse($spectrumProfilesArray);
    }

    /**
     * @Route("/customer-remove-spectrum-profiles")
     * @Method("POST")
     */
    public function removeSpectrumProfilesFromCustomerAction(Request $request)
    {
        try {
            $apiRequest = $this->createApiRequestFromRequest($request);

            $customerId = $apiRequest->getCustomerIdFromValuesOrException();
            $doctrine = $this->getDoctrine();
            $customerQuery = $doctrine->getManager()->getConnection()->query("SELECT * FROM `customers` WHERE `id` = ".$customerId);

            if (1 !== $customerQuery->rowCount()) {
                throw new BadRequestHttpException('Invalid customer id was send');
            }

            $customerArray = $customerQuery->fetch(\PDO::FETCH_ASSOC);
            $customer = Customer::createFromArray($customerArray, $doctrine);

            $customerTechnicalData = $doctrine->getRepository(\AppBundle\Entity\Customer\TechnicalData::class)->findOneByCustomerId($customerId);

            if (null !== $customerTechnicalData) {
                foreach ($customerTechnicalData->getSpectrumProfiles() as $spectrumProfile) {
                    if (in_array($spectrumProfile->getId(), $apiRequest->values['technicalData']['spectrumProfiles'])) {
                        $customerTechnicalData->removeSpectrumProfile($spectrumProfile);
                    }
                }

                $em = $doctrine->getManager();
                $em->persist($customerTechnicalData);
                $em->flush();

                return new JsonResponse(['status' => 'success', 'id' => $customer->getId(), 'clientId' => $customer->getClientId()]);
            }

            throw new \RuntimeException('Unable to update customer');
        } catch (\Exception $exception) {
            return new JsonResponse(['status' => 'error', 'message' => $exception->getMessage()]);
        }
    }

    /**
     * @Route("/customer-add-spectrum-profiles")
     * @Method("POST")
     */
    public function addSpectrumProfilesToCustomerAction(Request $request)
    {
        try {
            $apiRequest = $this->createApiRequestFromRequest($request);

            $customerId = $apiRequest->getCustomerIdFromValuesOrException();
            $doctrine = $this->getDoctrine();
            $customerQuery = $doctrine->getManager()->getConnection()->query("SELECT * FROM `customers` WHERE `id` = ".$customerId);

            if (1 !== $customerQuery->rowCount()) {
                throw new BadRequestHttpException('Invalid customer id was send');
            }

            $customerArray = $customerQuery->fetch(\PDO::FETCH_ASSOC);
            $customer = Customer::createFromArray($customerArray, $doctrine);

            $customerTechnicalData = $doctrine->getRepository(\AppBundle\Entity\Customer\TechnicalData::class)
                ->findOneByCustomerId($customerId);

            if (null === $customerTechnicalData) {
                $customerTechnicalData = new \AppBundle\Entity\Customer\TechnicalData();
                $customerTechnicalData->setCustomerId($customerId);
            }

            $connectionType = null;
            $customersCardPort = $customer->getDslamPort();

            $customersCard = $doctrine->getRepository(\AppBundle\Entity\Location\Card::class)->findOneById($customer->getCardId());

            if (!($customersCard instanceof \AppBundle\Entity\Location\Card)) {
                throw new \BadMethodCallException('customer has no card yet');
            }

            if (!empty($customersCardPort)) {
                $customersCardPort = $doctrine->getRepository('AppBundle\Entity\Location\CardPort')->
                    findOneByCardAndNumber($customersCard, $customersCardPort);

                if ($customersCardPort instanceof \AppBundle\Entity\Location\CardPort) {
                    $connectionType = $customersCardPort->getConnectionType();
                }
            }

            if (null === $connectionType) {
                if ($customersCard instanceof \AppBundle\Entity\Location\Card) {
                    $connectionType = $customersCard->getCardType();
                }
            }

            if (null !== $connectionType) {
                if (1 === preg_match('/\+ ?vdsl/i', $connectionType->getName())) {
                    // we use technicalData.spectrumProfiles currently only with cardType == g.fast + vdsl
                    // therefore only allow this when 'vdsl' exists in cardType.name

                    if (count($apiRequest->values['technicalData']['spectrumProfiles']) > 1) {
                        throw new \BadMethodCallException('only one spectrum profile allowed');
                    }

                    foreach ($apiRequest->values['technicalData']['spectrumProfiles'] as $spectrumProfileId) {
                        $spectrumProfile = $doctrine->getRepository(\AppBundle\Entity\SpectrumProfile::class)->findOneById($spectrumProfileId);

                        if ($spectrumProfile instanceof \AppBundle\Entity\SpectrumProfile) {
                            if (!$customerTechnicalData->hasSpectrumProfile($spectrumProfile)) {
                                $customerTechnicalData->addSpectrumProfile($spectrumProfile);
                            }
                        }
                    }

                    $em = $doctrine->getManager();
                    $em->persist($customerTechnicalData);
                    $em->flush();

                    return new JsonResponse(['status' => 'success', 'id' => $customer->getId(), 'clientId' => $customer->getClientId()]);
                }
            }

            throw new \RuntimeException('Unable to update customer');
        } catch (\Exception $exception) {
            return new JsonResponse(['status' => 'error', 'message' => $exception->getMessage()]);
        }
    }
}
