<?php

/**
 * 
 */

namespace AppBundle\src\DslamCommunication;

use Symfony\Component\Cache\Adapter\AdapterInterface;
use AppBundle\src\DslamCommunication\Communicator\CommunicatorInterface;
use Symfony\Component\Finder\Finder;
use Doctrine\Common\Annotations\AnnotationReader;
use AppBundle\src\DslamCommunication\SupportedDevice;
use AppBundle\src\DslamCommunication\Communicator\DslamCommunicator;

/**
 * Attention
 *      With ALU Dslams it is important to choose by processor-card over model
 */
final class DslamChooser
{
    /**
     * Symfony cache adapter
     * 
     * @var Symfony\Component\Cache\Adapter\AdapterInterface
     */
    protected $cache;

    /**
     * Current kernel environment
     * 
     * @var string
     */
    protected $environment;

    /**
     * Constructor
     * 
     * @param Symfony\Component\Cache\Adapter\AdapterInterface $cache
     * @param string $environment - current kernel environment
     */
    public function __construct(AdapterInterface $cache, $environment = 'prod')
    {
        $this->cache = $cache;
        $this->environment = $environment;
    }

    /**
     * Find communicator class according to vendor, model an optionaly to software-release version
     * 
     * @param sting $vendor
     * @param sting $model
     * @param sting $version - software-release version
     * 
     * @return strAppBundle\src\DslamCommunication\Communicator\DslamCommunicatoring|null
     */
    public function findCommunicationClass($vendor, $model, $version) : ?DslamCommunicator
    {
        $cacheItem = $this->cache->getItem('dslam_communicator_classes');

        if (!$cacheItem->isHit() || 'prod' !== $this->environment) {
            $this->createCacheValues();

            $cacheItem = $this->cache->getItem('dslam_communicator_classes');
        }

        $communicationClass = null;
        $classMap = $cacheItem->get();

        if (isset($classMap[$vendor], $classMap[$vendor][$model])) {
            if (!empty($version) && isset($classMap[$vendor][$model][$version])) {
                $communicationClass = $classMap[$vendor][$model][$version];
            } else {
                $communicationClass = $classMap[$vendor][$model];
            }
        }

        return null === $communicationClass ? null : new DslamCommunicator(new $communicationClass);
    }

    /**
     * Finds available communicator classes and adds them to cache
     * 
     * @return void
     */
    public function createCacheValues()
    {
        $devicesToClassMapping = [];

        $annotationReader = new AnnotationReader();

        $finder = new Finder();
        $finder->files()->name('*Communicator.php')->in(__DIR__.'/Communicator/*/');

        foreach ($finder as $file) {
            $array = explode('/Communicator/', $file->getRealPath());
            $array = end($array);

            $class = '\\AppBundle\\src\\DslamCommunication\\Communicator\\'.str_replace('/', '\\', substr($array, 0, -4));

            $reflectionClass = new \ReflectionClass($class);
            $classAnnotations = $annotationReader->getClassAnnotations($reflectionClass);

            if (!is_array($classAnnotations)) {
                continue;
            }

            foreach ($classAnnotations as $object) {
                if (!($object instanceof SupportedDevice)) {
                    continue;
                }

                if (empty($object->vendor) || empty($object->model)) {
                    throw new \UnexpectedValueException(sprintf(
                        'Neither the property "vendor" nor "model" can be empty in SupportedDevice annoation at %s', $file->getRealPath()
                    ));
                }

                $mapping = $class;

                if (null !== $object->softwareRelease) {
                    $mapping = [$object->softwareRelease => $class];
                }

                $devicesToClassMapping[$object->vendor][$object->model] = $mapping;
            }
        }

        $cacheItem = $this->cache->getItem('dslam_communicator_classes');

        $cacheItem->set($devicesToClassMapping);

        $this->cache->save($cacheItem);
    }
}
