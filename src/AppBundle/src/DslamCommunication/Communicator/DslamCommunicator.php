<?php

/** 
 *
 */

namespace AppBundle\src\DslamCommunication\Communicator;

use AppBundle\src\DslamCommunication\Communicator\CommunicatorInterface;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use AppBundle\src\DslamCommunication\ArgumentResolver\ServiceValueResolver;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver\SessionValueResolver;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver\DefaultValueResolver;
use AppBundle\src\DslamCommunication\Communicator\GatewayInterace;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 */
final class DslamCommunicator
{
    /**
     * @var AppBundle\src\DslamCommunication\Communicator\CommunicatorInterface
     */
    protected $communicator;

    /**
     * @var Symfony\Component\HttpKernel\Controller\ArgumentResolver
     */
    public $argumentResolver;

    /**
     * @var Symfony\Component\DependencyInjection\ContainerBuilder
     */
    public $container;

    /**
     * @var AppBundle\src\DslamCommunication\Communicator\GatewayInterace
     */
    public $gateway;

    /**
     * @var Symfony\Component\HttpFoundation\Request
     */
    public $request;

    /**
     * @var Symfony\Component\DependencyInjection\ContainerBuilder
     */
    protected $callArgumentsContainer;
    
    /**
     * Constructor
     * 
     * @param AppBundle\src\DslamCommunication\Communicator\CommunicatorInterface $communicator
     */
    public function __construct(CommunicatorInterface $communicator)
    {
        $this->communicator = new $communicator;
        $this->callArgumentsContainer = new ContainerBuilder();
        $this->request = new Request();
    }

    /**
     * Set a container for dependecy injection
     * 
     * @param Symfony\Component\DependencyInjection\ContainerBuilder $container
     * 
     * @return DslamCommunicator
     */
    public function setContainer(ContainerBuilder $container) : DslamCommunicator
    {
        $this->container = $container;

        return $this;
    }

    /**
     * Set request object (Overwrites existing one). Used for dependecy injection
     * 
     * @param Symfony\Component\HttpFoundation\Request $request
     * 
     * @return DslamCommunicator
     */
    public function setRequest(Request $request) : DslamCommunicator
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Set a gateway for connection.
     * 
     * @param AppBundle\src\DslamCommunication\Communicator\GatewayInterace $gateway
     * 
     * @return DslamCommunicator
     */
    public function setGateway(GatewayInterace $gateway) : DslamCommunicator
    {
        $this->gateway = $gateway;
        $this->communicator->setGateway($gateway);

        return $this;
    }

    /**
     * Instatiate argumentResolver object
     * 
     * @return void
     */
    public function instantiateArgumentResolver()
    {
        $this->argumentResolver = new ArgumentResolver(null, [
            new ServiceValueResolver($this->callArgumentsContainer),
            new ServiceValueResolver($this->container),
            new SessionValueResolver(),
            new DefaultValueResolver(),
        ]);
    }

    /**
     * __call
     * 
     * @param string $name
     * @param array $arguments_
     * 
     * @return mixed
     */
    public function __call($name, $arguments_)
    {
        if (null === $this->argumentResolver) {
            $this->instantiateArgumentResolver();
        }

        $servicesAdded = [];

        // add arguments as service to callArgumentsContainer
        if (!empty($arguments_)) {
            $reflectionMethod = new \ReflectionMethod($this->communicator, $name);
            $parameters = $reflectionMethod->getParameters();

            foreach ($arguments_ as $key => $value) {
                $key = isset($parameters[$key]) ? $parameters[$key]->getName() : $key;

                $this->callArgumentsContainer->set($key, $value);
                $servicesAdded[] = $key;
            }
        }

        $arguments = $this->argumentResolver->getArguments($this->request, [$this->communicator, $name]);

        foreach ($servicesAdded as $id) {
            // remove service to keep container clear for other calls
            $this->callArgumentsContainer->removeDefinition($id);
        }

        return call_user_func_array([$this->communicator, $name], $arguments);
    }
}
