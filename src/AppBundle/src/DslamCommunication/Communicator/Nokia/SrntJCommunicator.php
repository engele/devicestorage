<?php

/**

*/

namespace AppBundle\src\DslamCommunication\Communicator\Nokia;

use AppBundle\src\DslamCommunication\Communicator\AbstractCommunicator;
use AppBundle\src\DslamCommunication\Communicator\CommunicatorInterface;
use AppBundle\src\DslamCommunication\Communicator\GatewayInterace;
use AppBundle\src\DslamCommunication\Communicator\SshGateway;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Location\Card;
use AppBundle\Entity\Location\Location;
use AppBundle\src\DslamCommunication\SupportedDevice;
use AppBundle\Entity\Product\MainProduct;
use AppBundle\src\DslamCommunication\Exception\LoginFailedException;

/**
 * @SupportedDevice(vendor="ALU", model="SRNT-J")
 */
class SrntJCommunicator extends AbstractCommunicator implements CommunicatorInterface
{

        public $location_id1;
        public $lsa_id;
        public $location_id;
        public $line_identifier;
        public $card_id;
        public $clientid;
        public $version;
        //if ($version != "") $clientid = $clientid."_".$version;
        public $vlan_id;
        public $vlan_ID_u;
        public $tag;
        public $ip_address;
        public $terminal_type;
        public $dslam_port;
        public $tv_service;
        public $DPBO;
        public $Service;
        public $Spectrumprofile;
        public $firmware_version;
        public $dtag_line;
        public $vectoring;
        public $pppoePassword;
        public $detail;
        public $DSLAM_Status;
        public $prozessor;
        public $produkt_index;
        public $gpon_download;
        public $gpon_upload;
        public $ethernet_up_down;
        public $dslam_name;
        public $dslam_name_ext;
        public $vlan_name1;
        public $kvz_name1;
        public $kvz_name2;
        public $kvz_name3;
        public $Default_DPBO1;
        public $esel_wert1;
        public $Default_DPBO2;
        public $esel_wert2;
        public $Default_DPBO3;
        public $esel_wert3;
        public $dns1; //= \Wisotel\Configuration\Configuration::get('dnsServer');
        //public $dns1 = $dns1['1'];
        public $tftpserver;// = \Wisotel\Configuration\Configuration::get('tftpServer');
        public $admin_save = "";
        public $Backup;
        public $Backup_id;
        public $debug;

        /**
         * Mappes card-types to planned-card-type (used to provision/deprovision MDUs at OLT)
         */
        protected $plannedCardTypeMapping = [
            'ADSL' => 'todo',
            'VDSL' => 'vdsl2',
            'SDSL' => 'todo',
            'GPON' => 'todo',
            'ETHERNET' => 'ethernet',
            'Mobile' => 'todo',
            'VDSL-BLV' => 'todo',
            'VDSL-SLV' => 'todo',
            'MDU-VDSL' => 'vdsl2',
            'MDU-ETHERNET' => 'ethernet',
            'MDU-G.fast+VDSL' => 'MDU-G.fast+VDSL',
            'dependent-MDU' => 'GPON-MDU',
        ];

        //############################################
        //# DebugInit
        //############################################

        public function DebugInit ()
        {
            $this->$debug = true;
        }
        
        //############################################
        // print_color
        //############################################
        Public function print_color ($buffer,$color) {
            if ($color == 'true')
            {
                
                //var_dump ($buffer);
                //exit;
                $buffer = str_replace("Error", "<span style=\"color: red;\"> Error </span>", $buffer);
                $buffer = str_replace("invalid token", "<span style=\"color: red;\"> invalid token</span> ", $buffer);
                $buffer = str_replace("1D", "", $buffer); # -[\[ 
                        $buffer = str_replace('[\[ [', "", $buffer); //-\/- 
                        
                        $buffer = str_replace('[', "", $buffer);
                        $buffer = str_replace(']', "", $buffer);
                        #$buffer = str_replace('/', "", $buffer);
                        $buffer = str_replace('|', "", $buffer);
                        $buffer = str_replace('-\/-', "", $buffer); //-\/- 

                        $buffer = str_replace(" up ", "<span style=\"color: green;\"> up </span>",$buffer);
                        $buffer = str_replace(" down ", "<span style=\"color: red;\"> down </span>",$buffer); //admin-state down
                        $buffer = str_replace("admin-state down", "<span style=\"color: red;\">admin-state down</span>",$buffer); //admin-state down
                        $buffer = str_replace("ont no slot", "<span style=\"color: red;\">ont no slot</span>",$buffer); //admin-state down

                        echo " <pre>$buffer</pre>";
                return;
            }
            echo " <pre>$buffer</pre>";
        }
         //############################################
         // WaitErrorsNew
         //############################################
         public function WaitErrorsNewHelp ($findFirst)
         {
             try {
                 $this->gateway->waitUntilStringIsFound("$findFirst", null, null, false,[ "invalid token" => 'Error',"Error : instance does not exist" => 'instance']);
                 $Buffer = $this->gateway->getLastReadData();
                 if ($this->$debug == true) $this->print_color ($Buffer,'true');
                 $this->gateway->clearLastReadData();
                 $Buffer = '';
                 return 'true';
             } catch (\Exception $e) {
                 $Buffer = $this->gateway->getLastReadData();
                 if ($this->$debug == true) $this->print_color ($Buffer,'true');
                 $this->gateway->clearLastReadData();
                 if (false !== strpos($e->getMessage(),'instance')) {
                     
                     return "Error : instance does not exist";
                     
                 }
                 return 'false';
             }
         }
        public function WaitErrorsNew ($findFirst, $findLast, $exeption1, $exeption2, $exeption3, $print,$color)
        {
            if ($findLast == '') {
                $findLast = "#exitphp";
                
            }
            $this->gateway->write("$findLast\r\n");
            $findok = 'true';
            if ($findFirst != '')
            {
                
                $findok = $this->WaitErrorsNewHelp ($findFirst);
            }
            //echo "findok $findok";
            if ($findok == 'true' and $findok !=  "Error : instance does not exist") {
                try {
                    $this->gateway->waitUntilStringIsFound("$findLast", null, null, false, [ "$exeption1" => 'Error', "$exeption2" => 'Failure', "$exeption3" => 'dontknow']); //invalid token
                    $Buffer = $this->gateway->getLastReadData();

                    if ($this->$debug == true) $this->print_color ($Buffer,$color);
                    if ($print == 'y' and $this->$debug == false )$this->print_color ($Buffer,$color);

                    $this->gateway->clearLastReadData(); // reset buffer, so it only returns the output of

                    return $findLast;
                    
                } catch (\Exception $e) {
                    $Buffer = $this->gateway->getLastReadData();
                    $this->gateway->clearLastReadData();
                    if (false !== strpos($e->getMessage(),'Error')) {

                        if ($this->$debug == true or $print == 'y') $this->print_color ($Buffer,$color);

                        return $exeption1;
                    }

                    $this->print_color ($Buffer,$color);
                    if (false !== strpos($e->getMessage(),'Error')) return $exeption1;
                    if (false !== strpos($e->getMessage(),'Failure')) return $exeption2;
                    if (false !== strpos($e->getMessage(),'dontknow')) return $exeption3;
                    
                    return 'xx';
                }
            }
            return $findok;
        }


         //############################################
         // WaitErrors
         //############################################

        public function WaitErrors ($such1, $such2, $such3, $such4, $such5, $print)
        {
            try {
                $this->gateway->waitUntilStringIsFound("$such1", null, null, false, [ "$such2" => 'Error', "$such3" => 'Failure', "$such4" => 'invalid', "$such5" => '#php']); //invalid token
                $Buffer = $this->gateway->getLastReadData();

                if ($this->$debug == true) echo " <pre>$Buffer</pre>";
                if ($print == 'y' and $this->$debug == false ) echo " <pre>$Buffer</pre>";

                $this->gateway->clearLastReadData(); // reset buffer, so it only returns the output of

                return $such1;
            } catch (\Exception $e) {
                if (false !== strpos($e->getMessage(),'Error')) {
                    $Buffer = $this->gateway->getLastReadData();

                    if ($this->$debug == true or $print == 'y') echo " <pre>$Buffer</pre>";

                    $this->gateway->clearLastReadData();

                    return $such2;
                }

                if (false !== strpos( $e->getMessage(),'Failure') or false !== strpos( $e->getMessage(),'invalid') or false !== strpos( $e->getMessage(),'#php')) {
                    $Buffer = $Buffer.$this->gateway->getLastReadData();

                    echo " <pre>$Buffer</pre>";

                    $this->gateway->clearLastReadData();

                    return $such4;
                } else {
                    $Buffer = $Buffer.$this->gateway->getLastReadData();
                    echo " <pre>$Buffer</pre>";

                    return 'xx';
                }

                throw $e;
            }
        }

        /**
         * Show info about dependent MDU
         *
         * @param Card $card
         *
         * @return string
         */
         public function showInfoAboutDependentMdu(Card $card)
         {
             $lineIdentifier = $card->getLineIdentifierPrefix();

             $this->DebugInit ();

             if (substr($lineIdentifier,-1,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-1);

             $this->gateway->write('#');
             $this->gateway->waitUntilStringIsFound('#');
             $Buffer = $this->gateway->getLastReadData();

             if ($this->$debug == true) echo " <pre>$Buffer</pre>";

             $this->gateway->clearLastReadData();
             $this->gateway->write("info configure equipment ont slot $lineIdentifier \r\n ");
             $return = $this->WaitErrorsNew ("echo","#end","Error : instance does not exist","Failure","invalid token",'y','true');
             //echo "<br>Return Wait Value = $return <br><br>";

             switch ($return) {
                 case "Error : instance does not exist":
                         echo "<span style=\"color: red;\">keine Card vorhanden $lineIdentifier </span><br>";
                         

                     break;

                 case "#end":
                 /*
                     $this->gateway->write("show vlan bridge-port-fdb $lineIdentifier/1\r\n");
                     $this->gateway->write("#end \r\n");
                     $return1 = $this->WaitErrorsNew ("#end","#end","Error : instance does not exist","Failure","invalid token",'y','true');
                     switch ($return1) {
                         
                                          case "#end":
                                          
                                          break;
                                          
                                          case "Failure":
                                          case "invalid token":
                                          
                                          echo "Error = $return1";
                                          
                                          break;
                                      }
                 
                 
                    */              
                        
                     echo (sprintf("<span style=\"color: green;\">vorhanden Card ont slot %s type %s dataports %d voiceports 0 </span><br>",
                         $lineIdentifier,
                         $this->plannedCardTypeMapping[$card->getCardType()->getName()],
                         $card->getPortAmount()
                     ));

                     break;

                 case "invalid token":
                 case "xx":
                     echo (sprintf("<span style=\"color: red;\">Command Error Card ont slot %s type %s dataports %d voiceports 0 </span><br>",
                         $lineIdentifier,
                         $this->plannedCardTypeMapping[$card->getCardType()->getName()],
                         $card->getPortAmount()
                     ));

                     break;
             }

             if ($this->$debug == false) {
                 $this->gateway->getLastReadData();
                 $this->gateway->clearLastReadData();
             }

             return $this->gateway->getLastReadData();
         }

        /**
         * Deprovision dependent MDU
         *
         * @param Card $card
         *
         * @return string
         */
        public function deprovisionDependentMdu(Card $card)
        {
            $lineIdentifier = $card->getLineIdentifierPrefix();

            $this->DebugInit ();

            if (substr($lineIdentifier,-1,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-1);

            $this->gateway->write('#');
            $this->gateway->waitUntilStringIsFound('#');
            $Buffer = $this->gateway->getLastReadData();

            if ($this->$debug == true) echo " <pre>$Buffer</pre>";

            $this->gateway->clearLastReadData();

            $this->gateway->write("info configure equipment ont slot $lineIdentifier \r\n ");
            $return = $this->WaitErrorsNew ("echo","#end","Error : instance does not exist","Failure","invalid token",'y','true');

            if ($this->$debug == true) echo "<br>Return Wait Value = $return <br><br>";

            switch ($return) {
                case "Error : instance does not exist":
                    echo "<span style=\"color: red;\">keine Card vorhanden $lineIdentifier </span><br>";

                    break;

                case "#end":
                    

                    $this->gateway->write("configure equipment ont slot $lineIdentifier admin-state down \r\n exit all \r\n");
                    $this->gateway->write("configure equipment ont no slot $lineIdentifier\r\n exit all \r\n");
                    $this->gateway->write("#end\r\n");
                    $return = $this->WaitErrorsNew ("#end","#end","Error : instance does not exist","Failure","invalid token",'y','true');
                    echo "<span style=\"color: green;\">Card gelöscht $lineIdentifier</span> <br>";
                    
                    break;

                case "invalid token":
                case "xx":
                    echo (sprintf("Command Error Card ont slot %s type %s dataports %d voiceports 0 <br>",
                        $lineIdentifier,
                        $this->plannedCardTypeMapping[$card->getCardType()->getName()],
                        $card->getPortAmount()
                    ));

                    break;
            }

            return $this->gateway->getLastReadData();
        }

        //############################################
        // provisionDependentMdu
        //############################################
        /**
         * Provision dependent MDU
         *
         * @param Card $card
         *
         * @return string
         */
        public function provisionDependentMdu(Card $card)
        {
            $lineIdentifier = $card->getLineIdentifierPrefix();

            $this->DebugInit ();

            if (substr($lineIdentifier,-1,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-1);

            //echo "$lineIdentifier";
            //$this->gateway->waitUntilStringIsFound("last");
            //$this->gateway->clearLastReadData();
            //$this->gateway->waitUntilStringIsFound("#");
            $this->gateway->write('#');
            $this->gateway->waitUntilStringIsFound('#');
            $Buffer = $this->gateway->getLastReadData();

            if ($this->$debug == true) echo " <pre>$Buffer</pre>";

            $this->gateway->clearLastReadData();
            $this->gateway->write("info configure equipment ont slot $lineIdentifier \r\n ");
                            //->write("#\r\n#php\r\n");
            $return = $this->WaitErrorsNew ("info configure","#end","Error : instance does not exist","Failure","invalid token",'n','true');
            if ($this->$debug == true) echo "<br>Return Wait Value = $return <br><br>";

            switch ($return) {
                case "Error : instance does not exist":
                    //$this->gateway->write("configure equipment ont slot $lineIdentifier admin-state down \r\n")
                    //->write("exit all\r\n #\r\n")
                    //->write("configure equipment ont no slot $lineIdentifier\r\n")
                    //->write("exit all\r\n #\r\n")
                    $this->gateway->write(sprintf("configure equipment ont slot %s planned-card-type %s plndnumdataports %d plndnumvoiceports 0\r\n",
                        $lineIdentifier,
                        $this->plannedCardTypeMapping[$card->getCardType()->getName()],
                        $card->getPortAmount()
                    ));
                    $this->gateway->write('#end');
                    $return = $this->WaitErrorsNew ("#end","#end","Error : instance does not exist","Failure","invalid token",'n','true');
                    echo (sprintf("<span style=\"color: green;\">Card ont slot %s type %s dataports %d voiceports 0 eingerichtet </span> <br>",
                        $lineIdentifier,
                        $this->plannedCardTypeMapping[$card->getCardType()->getName()],
                        $card->getPortAmount()
                    ));
                    
                    break;

                case "#end":
                    echo (sprintf("<span style=\"color: red;\">Eirichten nicht möglich da vorhanden Card ont slot %s type %s dataports %d voiceports 0 </span> <br>",
                        $lineIdentifier,
                        $this->plannedCardTypeMapping[$card->getCardType()->getName()],
                        $card->getPortAmount()
                    ));

                    break;

                case "invalid token":
                case "xx":
                    echo (sprintf("<span style=\"color: red;\">Command Error Card ont slot %s type %s dataports %d voiceports 0 </span><br>",
                        $lineIdentifier,
                        $this->plannedCardTypeMapping[$card->getCardType()->getName()],
                        $card->getPortAmount()
                    ));

                    break;
            }

            $this->gateway->write('#end1');
            $this->wait_exit('','#end1','');

            if ($this->$debug == false) {
                $this->gateway->getLastReadData();
                $this->gateway->clearLastReadData();
            }

            return $this->gateway->getLastReadData();
        }

        //############################################
        //# VarInit  Variable Init beinahe nicht mehr benutzt
        //############################################

        public function showInfoAboutDependentBaseMdu(Card $mduBaseCard)
        {
            $lineIdentifier = $mduBaseCard->getLineIdentifierPrefix();

            $this->DebugInit ();

            if (substr($lineIdentifier,-1,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-1);

            $this->gateway->write('#');
            $this->gateway->waitUntilStringIsFound('#');
            $Buffer = $this->gateway->getLastReadData();

            if ($this->$debug == true) echo " <pre>$Buffer</pre>";

            $this->gateway->clearLastReadData();
            $this->gateway->write("info configure equipment ont interface $lineIdentifier \r\n ");
            $return = $this->WaitErrorsNew ("echo","#end","Error : instance does not exist","Failure","invalid token",'y','true');
            if ($this->$debug == true) echo "<br>Return Wait Value = $return <br><br>";

            switch ($return) {
                case "Error : instance does not exist":
                        echo "<span style=\"color: red;\">kein ont interface vorhanden $lineIdentifier </span><br>";
                        

                    break;

                case "#end":
                    $this->gateway->write("show interface port ont:$lineIdentifier \r\n ");
                    $this->gateway->write("show equipment ont interface  $lineIdentifier \r\n");
                    $this->gateway->write("show equipment ont optics $lineIdentifier \r\n");
                    //$this->gateway->write("info configure bridge port  $lineIdentifier/1/1 \r\n");
                    //$this->gateway->write("info configure qos interface  $lineIdentifier/1/1 queue 0\r\n");
                    //$this->gateway->write("info configure qos interface  $lineIdentifier/1/1 upstream-queue 0\r\n");
                    //$this->gateway->write("show vlan bridge-port-fdb  $lineIdentifier/1/1 \r\n");port table
                    $this->gateway->write("show equipment ont sw-download  $lineIdentifier\r\n");
                    //$this->gateway->write("show vlan bridge-port-fdb $lineIdentifier\r\n");
                    $this->gateway->write("#end\r\n");
                    $return = $this->WaitErrorsNew ("port table","#end","Error : instance does not exist","Failure","invalid token",'y','true');
                    if ($this->$debug == true) echo "<br>Return Wait Value = $return <br><br>";
                    echo (sprintf("<span style=\"color: green;\">ont interface vorhanden %s type %s </span><br>",
                        $lineIdentifier,
                        $this->plannedCardTypeMapping[$mduBaseCard->getCardType()->getName()]
                    ));

                    break;

                case "invalid token":
                case "xx":
                    echo (sprintf("<span style=\"color: red;\">Command Error Card ont interface %s type %s  </span><br>",
                        $lineIdentifier,
                        $this->plannedCardTypeMapping[$mduBaseCard->getCardType()->getName()]
                        
                    ));

                    break;
            }

            
            if ($this->$debug == false) {
                $this->gateway->getLastReadData();
                $this->gateway->clearLastReadData();
            }

            return $this->gateway->getLastReadData();
        }

        /**
         * Deprovision dependent MDU
         *
         * @param Card $mduBaseCard
         *
         * @return string
         */
        public function deprovisionDependentBaseMdu(Card $mduBaseCard)
        {
            $lineIdentifier = $mduBaseCard->getLineIdentifierPrefix();

            $this->DebugInit ();

            if (substr($lineIdentifier,-1,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-1);

            $this->gateway->write('#');
            $this->gateway->waitUntilStringIsFound('#');
            $Buffer = $this->gateway->getLastReadData();

            if ($this->$debug == true) echo " <pre>$Buffer</pre>";

            $this->gateway->clearLastReadData();

            $this->gateway->write("info configure equipment ont interface $lineIdentifier \r\n ");
            $return = $this->WaitErrorsNew ("echo","#end","Error : instance does not exist","Failure","invalid token",'n','true');
            if ($this->$debug == true) echo "<br>Return Wait Value = $return <br><br>";
            switch ($return) {
                case "port table":
                case "#end":
                    $this->gateway->write("configure equipment ont interface $lineIdentifier  admin-state down\r\n ");
                    $this->gateway->write("configure equipment ont no interface $lineIdentifier \r\n ");
                    $this->gateway->write("info configure equipment ont interface $lineIdentifier \r\n ");
                    $return1 = $this->WaitErrorsNew ("Error : instance does not exist","#end","Error : instance does not exist","Failure","invalid token",'y','true');
                    if ($this->$debug == true) echo "<br>Return Wait Value = $return1 <br><br>";
                    switch ($return1) {
                        case "Error : instance does not exist":
                        case "#end";
                            echo (sprintf("<span style=\"color: green;\">ont interface wurde gelöscht %s type %s mit allen darunterliegenden Karten</span><br>",
                                $lineIdentifier,
                                $this->plannedCardTypeMapping[$mduBaseCard->getCardType()->getName()]
                                
                            ));
                        
                        break;
                        echo (sprintf("<span style=\"color: red;\">Error ont interface löschen nicht durchgeführt %s type %s  <br>",
                            $lineIdentifier,
                            $this->plannedCardTypeMapping[$mduBaseCard->getCardType()->getName()]
                        ));
                        
                    }
                    
                    break;
                    
                case "Error : instance does not exist":
                    echo (sprintf("<span style=\"color: red;\">ont interface existiert nicht, löschen nicht möglich %s type %s  <br>",
                        $lineIdentifier,
                        $this->plannedCardTypeMapping[$mduBaseCard->getCardType()->getName()]
                    ));
                    break;
                }
                return $this->gateway->getLastReadData();
            }


            
        

        //############################################
        // provisionDependentMdu
        //############################################
        /**
         * Provision dependent MDU
         *
         * @param Card $mduBaseCard
         *
         * @return string
         */
        public function provisionDependentBaseMdu(Card $mduBaseCard)
        {
            $lineIdentifier = $mduBaseCard->getLineIdentifierPrefix();
            $ontFirmwareVersion = $mduBaseCard->getLocation()->getOntFirmwareVersion();
            //echo "ontFirmwareVersion = $ontFirmwareVersion   ";
            $ontSerialNumber = $mduBaseCard->getLocation()->getOntSerialNumber();
            //echo "BaseMdu ontSerialNumber = $ontSerialNumber <br>";
            if (false === strpos($ontSerialNumber, ':')) {
                $ontSerialNumber = "ALCL:".$ontSerialNumber;
            }


            $this->DebugInit ();

            if (substr($lineIdentifier,-1,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-1);

            //echo "$lineIdentifier";
            //$this->gateway->waitUntilStringIsFound("last");
            //$this->gateway->clearLastReadData();
            //$this->gateway->waitUntilStringIsFound("#");
            //$this->gateway->write('#');
            //$this->gateway->waitUntilStringIsFound('#');
            //$Buffer = $this->gateway->getLastReadData();

            //if ($this->$debug == true) echo " <pre>$Buffer</pre>";

            //$this->gateway->clearLastReadData();
            $this->gateway->write("info configure equipment ont interface $lineIdentifier \r\n ");
            //->write("#\r\n#php\r\n");
            $return = $this->WaitErrorsNew ("echo","#end","Error : instance does not exist","Failure","invalid token",'n','true');
            if ($this->$debug == true) echo "<br>Return Wait Value = $return <br><br>";

            switch ($return) {
                case "Error : instance does not exist":
                        echo "<span style=\"color: blue;\">keine ont interface vorhanden $lineIdentifier Einrichtung startet<br></span>";
                        $this->gateway->write("configure equipment ont interface $lineIdentifier sw-ver-pland $ontFirmwareVersion \r\n ");
                        //$this->gateway->write('exit all');
                        $this->gateway->write("configure equipment ont interface $lineIdentifier sernum $ontSerialNumber sw-dnload-version disabled desc1 ".$this->plannedCardTypeMapping[$mduBaseCard->getCardType()->getName()]." \r\n ");
                        //$this->gateway->write('exit all');
                        $this->gateway->write("configure equipment ont interface $lineIdentifier admin-state up");
                        $return = $this->WaitErrorsNew ("admin-state up","#end","Error : instance does not exist","Failure","invalid token",'n','true');
                        if ($this->$debug == true) echo "<br>Return Wait Value = $return <br><br>";
                        switch ($return) {
                        
                            case "admin-state up":
                            case "#end":
                                
                                echo (sprintf("<span style=\"color: green;\">Ont interface %s type %s   eingerichtet </span><br>",
                                $lineIdentifier,
                                $this->plannedCardTypeMapping[$mduBaseCard->getCardType()->getName()]

                            ));
                            break;
                        
                            default;
                            
                                echo "<span style=\"color: red;\">Error einrichten nicht möglich ont interface schon vorhanden</span>";
                        
                        }
                            
                        

                    break;

                case "#end":
                    echo (sprintf("<span style=\"color: red;\">ont interface vorhanden wurde nicht neu eingerichtet %s type %s </span><br>",
                        $lineIdentifier,
                        $this->plannedCardTypeMapping[$mduBaseCard->getCardType()->getName()]
                        
                    ));

                    break;

                case "invalid token":
                case "xx":
                    echo (sprintf("Command Error ont interface  %s type %s  <br>",
                        $lineIdentifier,
                        $this->plannedCardTypeMapping[$mduBaseCard->getCardType()->getName()]
                    ));

                    break;
            }
            

            

            return $this->gateway->getLastReadData();
        }

        //############################################
        //# VarInit  Variable Init beinahe nicht mehr benutzt
        //############################################



        public function VarInit (Customer $customer = null, Location $location = null, Card $card = null , MainProduct $customerService = null)
        {

            if ($customerService != null)
            {
                $this->produkt_index = $customerService->getXdslServiceProfile();
                $this->gpon_download = $customerService->getGponDownload();
                $this->gpon_upload = $customerService->getGponUpload();
                $this->ethernet_up_down = $customerService->getEthernetUpDown();
            }

            if ($customer != null)
            {
                $this->location_id = $customer->getLocationId();
                $this->version = $customer->getVersion();
                //if ($version != "") $clientid = $clientid."_".$version= $customer->get
                $this->vlan_id = $customer->getVlanId();
                $this->vlan_ID_u = $customer->getReachedDownstream();
                $this->tag = $customer->getTag();
                $this->ip_address = $customer->getIpAddress();
                $this->terminal_type = $customer->getTerminalType();
                $this->dslam_port = $customer->getDslamPort();
                $this->tv_service = $customer->getTvService();
                $this->DPBO = $customer->getDPBO();
                $this->Service = $customer->getService();
                $this->Spectrumprofile = $customer->getSpectrumprofile();
                $this->firmware_version = $customer->getFirmwareVersion();
                $this->dtag_line = $customer->getDtagLine();
                $this->vectoring = $customer->getVectoring();
                $this->pppoePassword = $customer->getPassword();
                $this->lsaId = $customer->getLsaKvzPartitionId();
                $this->customerId = $customer->getId();
                $this->clientid = $this->customerId;
                $this->line_identifier =  $customer->getLineIdentifier();
                if ($this->version != "") $this->clientid = $this->clientid."_".$this->version;
                $this->lsa_id = $this->lsaId;
            }


            if ($location != null)
            {




                $this->locationId = $location->getId();
                $this->dslam_name = $location->getName();
                $dslamNameParted = null;

                if (1 === preg_match('/^((([^_]+)_.*)_R\d{2})\/.*/', $dslam_name, $dslamNameParted))
                {
                    $this->dslam_name = $dslamNameParted[2];
                    $this->dslam_name_ext = $dslamNameParted[1];
                    $this->vlan_name1 = $dslamNameParted[3];
                }
                else
                {
                    $this->dslam_name_ext     = substr($dslam_name, 0, 14);
                    $this->dslam_name_ext     = preg_replace('/[^0-9a-z_]/i', '_', $dslam_name_ext);
                    $this->dslam_name         = substr($dslam_name, 0, 10);
                    $this->vlan_name1         = substr($dslam_name, 0, 3);
                }
                $this->prozessor = $location->getProcessor();
                $this->location_id1 = $this->locationId;
            }

            if ($card != null)
            {
                $this->cardType = $card->getCardType()->getId();
            }


        }
        /**
            Maps some aliases to methods to be used with getMethodByAlias()

            @var array
        */
        protected $aliasMap = [
                                  'DSLAM_Kunden_Status' => 'DSLAM_Kunden_Status',
                              ];

        /**
            Holds current login stage

            @var boolean
        */
        protected $isLoggedIn = false;

        public function Experte(Customer $customer, Location $location, Card $card, MainProduct $customerService)
        {
            return 'done ';
        }
        /**
            Get customer status from dslam

            @param AppBundle\Entity\Customer $customer

            @return string
        */

        //############################################
        //# Testx Login only
        //############################################

        public function Testx ()
        {
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();
        }

        //############################################
        //# Test Login von den globalen Kommandos
        //############################################

        public function Test( Location $location )
        {
            //$this->VarInit ( null, $location ,null, null);
            $name = $location->getName();
            $this->dslam_name_extract ($location->getName(),$location);

            //(Customer $customer = null, Location $location, Card $card = null , MainProduct $customerService = null)
            echo 'Test <br>';
            echo " &nbsp;&nbsp;&nbsp;&nbsp; DSLAM_NAME      =   $this->dslam_name <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; Dslam Type      =   ".$location->getType()."   <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; logon_ip        =   ".$location->getIpAddress()."   <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; logon_port      =   ".$location->getLogonPort()."     <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; DSLAMLOGON      =   ".$location->getLogonUsername()."<br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; DSLAMPW         =   *******     <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; Real _ip        =   ".$location->getRealIp()."   <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; Current SW      =   ".$location->getCurrentSoftwareRelease()."   <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; Backup          =   $this->Backup <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; Vlan IPTV       =   ".$location->getVlanIptv()."  <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; vlan_ACS        =   ".$location->getVlanAcs()."   <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; vlan_pppoe      =   ".$location->getVlanPppoe()." <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; Vlan IPTV local =   ".$location->getVlanIptvLocal()."    <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; vlan_ACS local  =   ".$location->getVlanAcsLocal()." <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; vlan_pppoe_local=   ".$location->getVlanPppoeLocal()."   <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; vlan DCN        =   ".$location->getVlanDcn()."   <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; Vector_location =   ".$location->getVectoring()."    <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; Dslam_conf      =   ".$location->getConfigurable()." <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; location_id     =   ".$location->getId()."     <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; ACS Type        =   ".$location->getAcsType()."   <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; Prozessor = ".$location->getProcessor()."<br><br>";
            /*
                if ($kvz_name1 != '') echo " &nbsp;&nbsp;&nbsp;&nbsp;KVZ = $this->kvz_name1 DPBO = $this->Default_DPBO1 ESEL = $this->esel_wert1 <br>";
                if ($kvz_name2 != '')echo " &nbsp;&nbsp;&nbsp;&nbsp;KVZ = $this->kvz_name2 DPBO = $this->Default_DPBO2 ESEL = $this->esel_wert2 <br>";
                if ($kvz_name3 != '')echo " &nbsp;&nbsp;&nbsp;&nbsp;KVZ = $this->kvz_name3 DPBO = $this->Default_DPBO3 ESEL = $this->esel_wert3 <br>";

                echo " &nbsp;&nbsp;&nbsp;&nbsp;Cardtyp = $this->cardtype<br>";
                echo " <hr>&nbsp;&nbsp;&nbsp;&nbsp; Kunde        = $this->clientid    <br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp; lsa_id       = $this->lsa_id  <br>";

                echo " &nbsp;&nbsp;&nbsp;&nbsp; line_identifier          = $this->line_identifier     <br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp; card_id      = $this->card_id <br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp; vlan_ID      = $this->vlan_id <br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp;  VLAN_ID-old  = $this->vlan_ID_u<br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp; tag          = $this->tag     <br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp; ip_address       = $this->ip_address  <br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp; dslam_port       = $this->dslam_port  <br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp; tv_service       = $this->tv_service  <br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp; DPBO         = $this->DPBO    <br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp; Service      = $this->Service <br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp; Spectrumprofile      = $this->Spectrumprofile <br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp; vectoring           = $this->vectoring    <br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp; produkt_index       = $this->produkt_index<br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp; gpon_download       = $this->gpon_download<br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp; gpon_upload         = $this->gpon_upload<br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp; ethernet_up_down    = $this->ethernet_up_down<br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp; firmware_version    = $this->firmware_version<br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp; dtag_line           = $this->dtag_line<br>";
                echo " &nbsp;&nbsp;&nbsp;&nbsp; IPTV local VLAN = $this->vlan_iptv_local";

                //#firmware_version dtag_line
            */
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();


        }



        //#### begin new khl
        //############################################
        //# reboot1  confirm to reboot no test
        //############################################

        public function reboot1 ( Location $location)
        {
            $this->logout();
            $this->dslam_name_extract ($location->getName(),$location);
            $Dslam_comando = '';
            ?>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            <a  class = "fancy-button outline round  red large18" href = "<?php echo "?location_id=".$location->getId()."&kommando=reboot"  ?>" > &nbsp; reboot <?php echo "$this->dslam_name" ?>  Bestaetigen&nbsp; </a> <br> <br>
            <?php



        }

        //############################################
        //# reboot2 with test
        //############################################

        public function reboot2 ( Location $location)
        {
          $this->logout();
          $this->dslam_name_extract ($location->getName(),$location);
          $Dslam_comando = '';
          ?>
          &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
          <a  class = "fancy-button outline round  red large18" href = "<?php echo "?location_id=".$location->getId()."&kommando=reboot3"  ?>" > &nbsp; reboot <?php echo "$this->dslam_name" ?>  Bestaetigen&nbsp; </a> <br> <br>
          <?php

        }

        //############################################
        //# DSLAM_Kunden_Loeschen
        //############################################

        public function DSLAM_Kunden_Loeschen (Customer $customer, Location $location)
        {
            $this->logout();
            $this->dslam_name_extract ($location->getName(),$location);
            $Dslam_comando = '';
            ?>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            <a  class = "fancy-button outline round  red large18" href = "<?php echo " index.php?menu=customer&id=".$customer->getId()."&area=SwitchTechConf&kommando=DSLAM_Kunden_Loeschen1"  ?>" > &nbsp; Kunde  <?php echo "".$customer->getClientId()." Loeschen im DSLAM $this->dslam_name"?>  Bestaetigen&nbsp; </a > <br> <br>
            <?php

        }


        //############################################
        //# reboot now
        //############################################


        public function reboot ( Location $location)
        {
            $this->adminsave($location);
            echo "reboot gestartet"; #admin equipment reboot-isam without-self-test
            $this->gateway->write(" admin equipment reboot-isam without-self-test\r\n");
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# reboot3 without-self-test
        //############################################

        public function reboot3 ( Location $location)
        {
            $this->adminsave($location);
            echo "reboot gestartet"; #admin equipment reboot-isam without-self-test
            $this->gateway->write(" admin equipment reboot-isam with-self-test\r\n");

            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# EQ_Slot with all details
        //############################################


        public function EQ_Slot ( Location $location)
        {
            $this->gateway->write("show equipment slot detail \r\n");
            $this->gateway->write("info configure xdsl board flat\r\n");
            $this->gateway->write("show xdsl vp-board\r\n");

            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();
        }

        //############################################
        //# EQ_Slots short display
        //############################################

        public function EQ_Slots (Location $location)
        {
            $this->gateway->write("show equipment slot\r\n");

            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_Equipmentslot
        //############################################

        public function DSLAM_Equipmentslot ()
        {

            $this->gateway->write("show equipment slot\r\n");

            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //Software Management
        //############################################
        //# Software with all possible commands
        //############################################

        public function Software ($Dslam_comando2, $Dslam_comando3, $Dslam_comando4)
        {
            echo "Dslam_comando1 = $Dslam_comando1 / Dslam_comando2 = $Dslam_comando2 / Dslam_comando3 = $Dslam_comando3 / Dslam_comando4 = $Dslam_comando4 / Dslam_comando5 = $Dslam_comando5";
            switch ($Dslam_comando2)
            {
                case 'show software-mngt oswp':
                    $this->gateway->write("$Dslam_comando2 \r\n");
                    $tftpserver = \Wisotel\Configuration\Configuration::get('tftpServer');
                    
                    $tftpserverip = $tftpserver;
                    $search = ":";
                    $count = strpos ($tftpserver , $search  );
                    if ($count != 0) $tftpserverip = substr ($tftpserver,0,$count);
                    echo "<br><span style=\"color: green;\">TFTP Server ist = $tftpserver  TFTP IP = $tftpserverip count = $count</span><br>";

                    break;

                case 'show software-mngt swp-disk-file':
                    $this->gateway->write("$Dslam_comando2 \r\n");
                    break;

                case 'show equipment ont sw-version':
                    $this->gateway->write("$Dslam_comando2 \r\n");
                    $this->gateway->write("show pon sfp-inventory \r\n");
                    $this->gateway->write("show pon optics \r\n");
                    $this->gateway->write("info configure pon flat\r\n");
                    break;
                case 'admin software-mngt oswp 1 download L6GPAA58.474':
                case 'admin software-mngt oswp 2 download L6GPAA58.474':
                case 'admin software-mngt oswp 1 download L6GPAA56.452':
                case 'admin software-mngt oswp 2 download L6GPAA56.452':
                case 'admin software-mngt oswp 1 download L6GPAA54.543':
                case 'admin software-mngt oswp 2 download L6GPAA54.543':
                    $this->gateway->write("$Dslam_comando2 \r\n");
                    $this->gateway->write("show software-mngt oswp \r\n");
                    break;
                case 'admin software-mngt database download 195.178.0.114':
                    $this->gateway->write("$Dslam_comando2:$Dslam_comando3 \r\n");
                    $this->gateway->write("show software-mngt upload-download \r\n");
                    break;
                case 'admin software-mngt oswp':
                    $this->gateway->write("$Dslam_comando2 $Dslam_comando2 $Dslam_comando4 \r\n");
                    break;
                    break;
            }


            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# IP_setzen im DSLAM
        //############################################

        public function IP_setzen (Location $location)
        {
            //setzen der richtigen DSLAM IP nach erstbetrieb
            $newIP = substr($dslam_ip_real, 0, 9).$Dslam_comando1;
            echo "das waere die IP nachdem der Service von ZEAG nach WiSoTEL geaendert worden ist !! IP = $newIP";
            if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName'))
            {
                switch ($location->getVlanDcn())
                {
                    case '530':
                        $iesInterface = "interface MGNT-VLAN-BIB-530";
                        break;
                    case '531':
                        $iesInterface = "interface MGNT-VLAN-BIB-531";
                        break;
                    case '532':
                        $iesInterface = "interface MGNT-VLAN-GEM-532";
                        break;
                    case '530':
                        $iesInterface = "interface MGNT-VLAN-HN-533";
                        break;
                    case '530':
                        $iesInterface = "interface MGNT-VLAN-HN-540";
                        break;
                }
                $this->gateway->write("configure service ies 2 customer 1\r\n");
                $this->gateway->write("$iesInterface\r\n");
                $this->gateway->write("address $newIP/24\r\n");
                //#echo "Test new IP = $newIP";
                $this->gateway->write("info\r\n");
            }
            if ('WiSoTEL' === \Wisotel\Configuration\Configuration::get('companyName'))
            {
                $this->gateway->write("configure service ies 1 customer 1\r\n");
                $this->gateway->write("interface WiSoTEL_DCN\r\n");
                $this->gateway->write("address $newIP/21\r\n");
                //#echo "Test new IP = $newIP";
                $this->gateway->write("info\r\n");
            }

            $this->adminsave($location);
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# IPTV information
        //############################################

        public function IPTV ()
        {
            $this->gateway->write(" info configure igmp\r\n");

            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# IGMP information
        //############################################

        public function IGMP (Customer $customer, Location $location)
        {
            $this->gateway->write(" show igmp channel protocol detail\r\n");
            $this->gateway->write(" show igmp channel counter detail\r\n");

            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# IPTV_setup
        //############################################

        public function IPTV_setup ( Location $location)
        {
            $this->gateway->write("configure igmp system src-ip-address ".$location->getRealIp()." \r\n");
            $this->gateway->write("configure igmp system start\r\n");
            $this->gateway->write("configure igmp vlan ".$location->getVlanIptv()." netw-igmp-version 3\r\n");
            $this->gateway->write("configure igmp vlan ".$location->getVlanIptvLocal()." netw-igmp-version 3\r\n");
            $this->gateway->write("configure igmp package 1\r\n");
            $this->gateway->write("name IPTV\n");
            $this->gateway->write("template-version 1\r\n");
            $this->gateway->write("configure igmp mcast-svc-context Default\r\n");

            $this->adminsave($location);
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# aging_time setup
        //############################################

        public function aging_time ( Location $location)
        {

            $this->gateway->write( " configure vlan id ".$location->getVlanAcs()." aging-time 6000 \r\n");
            $this->gateway->write( " configure vlan id ".$location->getVlanAcs()." local aging-time 6000 \r\n");
            $this->gateway->write( " configure vlan id ".$location->getVlanIptv()." aging-time 6000 \r\n");
            $this->gateway->write( " configure vlan id ".$location->getVlanIptvLocal()." aging-time 6000 \r\n");

            $this->adminsave($location);
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# LAG_setup Zeag Style
        //############################################

        public function LAG_setup ( Location $location)
        {
            switch ($location->getProcessor())
            {
                case 'NANT-E':
                case 'FANT-F':
                    $this->gateway->write(" configure port nt-a:xfp:3 ethernet use-vlan-dot1q-etype \r\n");
                    $this->gateway->write(" configure port nt-a:xfp:3 no shutdown \r\n");
                    $this->gateway->write(" configure port nt-a:xfp:4 ethernet use-vlan-dot1q-etype \r\n");
                    $this->gateway->write(" configure port nt-a:xfp:4 no shutdown \r\n");
                    $this->gateway->write(" configure lag 1  \r\n");
                    $this->gateway->write(" port nt-a:xfp:3  \r\n");
                    $this->gateway->write(" port nt-a:xfp:4  \r\n");
                    $this->gateway->write(" local_nt_ports_only \r\n");
                    $this->gateway->write(" no shutdown \r\n");
                    if ($location->getVlanPppoe() != '')     $this->gateway->write(" configure service vpls ".$location->getVlanPppoe()." sap lag-1:".$location->getVlanPppoe()." create\r\n");
                    if ($location->getVlanAcs() != '')    $this->gateway->write(" configure service vpls ".$location->getVlanAcs()." sap lag-1:".$location->getVlanAcs()." create\r\n");
                    if ($location->getVlanDcn() != '')    $this->gateway->write(" configure service vpls ".$location->getVlanDcn()." sap lag-1:".$location->getVlanDcn()." create\r\n");
                    if ($location->getVlanIptv() != '')
                    {
                        $this->gateway->write(" configure service vpls ".$location->getVlanIptv()." sap lag-1:".$location->getVlanIptv()." create\r\n");
                        $this->gateway->write(" configure service vpls ".$location->getVlanIptv()." sap lag-1:".$location->getVlanIptv()." igmp-snooping\r\n");
                        $this->gateway->write(" configure service vpls ".$location->getVlanIptv()." sap lag-1:".$location->getVlanIptv()." igmp-snooping mrouter-port\r\n");
                    }
                    break;
                case 'NRNT-A':
                    ////#echo " LAG nicht implementiert fuer $prozessor";
                    $this->gateway->write(" configure interface shub port nt:sfp:3 port-type network admin-status up \r\n");
                    $this->gateway->write(" configure la aggregator-port nt:sfp:3 name lag1 selection-policy mac-src-dst actor-key 1234 active-lacp short-timeout aggregatable lacp-mode disable-lacp \r\n");
                    $this->gateway->write(" configure interface shub port nt:sfp:4 port-type network admin-status up \r\n");
                    $this->gateway->write(" configure la aggregator-port nt:sfp:4 name lag1 selection-policy mac-src-dst actor-key 1234 active-lacp short-timeout aggregatable lacp-mode disable-lacp \r\n");

                    break;
                case 'RANT-A':
                    //#echo " LAG nicht implementiert fuer $prozessor";
                    $this->gateway->write(" configure link-agg group nt-a:xfp:1 \r\n");
                    $this->gateway->write(" load-sharing-policy ip-src-dst mode static \r\n");
                    $this->gateway->write(" port nt-a:xfp:1 \r\n");
                    $this->gateway->write(" exit \r\n");
                    $this->gateway->write(" port nt-b:xfp:1 \r\n");
                    $this->gateway->write(" exit \r\n");
                    break;
            }

            $this->adminsave($location);
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# LAG_show
        //############################################

        public function LAG_show (Location $location)
        {
            switch ($location->getProcessor())
            {
                case 'NANT-E':
                case 'FANT-F':
                    $this->gateway->write(" show lag 1 detail\r\n");

                    break;
                case 'NRNT-A':
                    //#echo " LAG nicht implementiert fuer $prozessor";
                    $this->gateway->write(" show la aggregator-info\r\n");
                    $this->gateway->write(" show la network-port-info \r\n");

                    break;
                case 'RANT-A':
                    //#echo " LAG nicht implementiert fuer $prozessor";
                    $this->gateway->write(" info configure link-agg group \r\n");
                    $this->gateway->write(" show link-agg member-port\r\n");
                    break;
            }
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# Vectoring
        //############################################

        public function Vectoring ()
        {

            echo " &nbsp;&nbsp;&nbsp;&nbsp; Vectoring am DSLAM eingeschalten = ".$location->getVectoring()." <br />";
            $this->gateway->write("info configure xdsl board flat\r\n");
            $this->gateway->write("show xdsl vp-board\r\n");
            $this->gateway->write(" exit all\r\n");
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DPBO_setup
        //############################################

        public function DPBO_setup (Location $location,$Dslam_comando1,$Dslam_comando2,$Dslam_comando3,$Dslam_comando4,$Dslam_comando5)
        {
            echo "DPBO Setup  $Dslam_comando4 default Profile werden eingerichtet<br />";
            $iii = intval ($Dslam_comando4);

            $ii = 1;

            while ($ii <= $iii )
            {
                $items = strval($ii);
                $this->default_dpbo ( $items);
                //echo "$ii input $items<br />";
                $ii++;
            }

            $this->adminsave($location);
            $this->wait_exit ('true','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DPBO setup in location
        //############################################

        public function DPBO (  Location $location)
        {
            if ($this->kvz_name1 != '' && $this->Default_DPBO1 > 0) $this->gateway->write( " info configure xdsl dpbo-profile $this->Default_DPBO1 flat \r\n");
            if ($this->kvz_name2 != '' && $this->Default_DPBO2 > 0) $this->gateway->write( " info configure xdsl dpbo-profile $this->Default_DPBO2 flat \r\n");
            if ($this->kvz_name3 != '' && $this->Default_DPBO3 > 0) $this->gateway->write( " info configure xdsl dpbo-profile $this->Default_DPBO3 flat \r\n");
            if ($this->kvz_name4 != '' && $this->Default_DPBO3 > 0) $this->gateway->write( " info configure xdsl dpbo-profile $this->Default_DPBO4 flat \r\n");
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();
        }

        //############################################
        //# DPBO_set für 4 KVZ's
        //############################################

        public function DPBO_set (Customer $customer, Location $location)
        {

            if ($esel_wert1 != '' )
            {
                $this->gateway->write( " configure xdsl dpbo-profile $this->Default_DPBO1  \r\n");
                $this->gateway->write( " modification start \r\n");
                $this->gateway->write( " es-elect-length  $this->esel_wert1 \r\n");
                $this->gateway->write( "name  $this->kvz_name1 \r\n");
                $this->gateway->write( " modification complete \r\n");
            }
            else
            {
                if ($this->kvz_name1 != '')echo " &nbsp;&nbsp;&nbsp;&nbsp; kein gueltiger ESEL Wert f?r KVZ  = $this->kvz_name1 <br />" ;
            }
            if ($this->esel_wert2 != '' )
            {
                $this->gateway->write( " configure xdsl dpbo-profile $this->Default_DPBO2  \r\n");
                $this->gateway->write( " modification start \r\n");
                $this->gateway->write( " es-elect-length  $this->esel_wert2 \r\n");
                $this->gateway->write( "name  $this->kvz_name2 \r\n");
                $this->gateway->write( " modification complete \r\n");
            }
            else
            {
                if ($this->kvz_name2 != '') echo " &nbsp;&nbsp;&nbsp;&nbsp; kein gueltiger ESEL Wert f?r KVZ  = $this->kvz_name2 <br />" ;
            }
            if ($this->esel_wert3 != '' )
            {
                $this->gateway->write( " configure xdsl dpbo-profile $this->Default_DPBO3  \r\n");
                $this->gateway->write( " modification start \r\n");
                $this->gateway->write( " es-elect-length  $this->esel_wert3 \r\n");
                $this->gateway->write( "name  $this->kvz_name3 \r\n");
                $this->gateway->write( " modification complete \r\n");
            }
            else
            {
                if ($this->kvz_name3 != '' ) echo " &nbsp;&nbsp;&nbsp;&nbsp; kein gueltiger ESEL Wert f?r KVZ  = $this->kvz_name3 <br />" ;
            }
            if ($this->esel_wert4 != '' )
            {
                $this->gateway->write( " configure xdsl dpbo-profile $this->Default_DPBO4  \r\n");
                $this->gateway->write( " modification start \r\n");
                $this->gateway->write( " es-elect-length  $this->esel_wert4 \r\n");
                $this->gateway->write( "name  $this->kvz_name4 \r\n");
                $this->gateway->write( " modification complete \r\n");
            }
            else
            {
                if ($this->kvz_name4 != '' ) echo " &nbsp;&nbsp;&nbsp;&nbsp; kein gueltiger ESEL Wert f?r KVZ  = $this->kvz_name4 <br />" ;
            }


            $this->adminsave($location);
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //#$this->gateway->write( " info configure xdsl dpbo-profile flat \r\n");
        //############################################
        //#  DPBO_setx
        //############################################


        public function DPBO_setx (  Location $location)
        {
            $this->gateway->write( " configure xdsl dpbo-profile 1  \r\n");
            $this->gateway->write( " modification start \r\n");
            $this->gateway->write( " es-elect-length  $Dslam_comando1 \r\n");
            $this->gateway->write( " modification complete \r\n");
            $this->adminsave($location);
            $this->wait_exit ('','>#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_Service show config
        //############################################

        public function DSLAM_Service ()
        {
            $this->gateway->write("info configure service \r\n");
            $this->gateway->write(" exit all\r\n");
            $this->wait_exit ('','>#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_VLAN zeige alle vlans im dslam
        //############################################

        public function DSLAM_VLAN ()
        {

            $this->gateway->write("info configure vlan \r\n");
            $this->gateway->write(" exit all\r\n");

            $this->wait_exit ('','>#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# ShowSoftware zeige Software version und status
        //############################################

        public function ShowSoftware ()
        {

            $this->gateway->write("show software-mngt oswp\r\n");
            $this->gateway->write("show software-mngt swp-disk-file\r\n");
            $this->gateway->write(" exit all\r\n");

            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_Diagnose_SFP
        //############################################

        public function DSLAM_Diagnose_SFP ()
        {

            $this->gateway->write("show equipment diagnostics sfp\r\n");
            $this->gateway->write(" exit all\r\n");


            $this->wait_exit ('','>#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_show_Port
        //############################################

        public function DSLAM_show_Port ()
        {

            $this->gateway->write("show port \r\n");
            $this->gateway->write("info configure equipment external-link-assign flat\r\n");
            $this->gateway->write(" exit all\r\n");


            $this->wait_exit ('','>#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_Line_up
        //############################################


        public function DSLAM_Line_up ()
        {

            $this->gateway->write("show xdsl operational-data line | match exact:up | match exact:up | match exact::  | count lines \r\n");
            $this->gateway->write(" exit all\r\n");

            $this->wait_exit ('','>#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_Line_upsum Summe aller XDSL Lines up
        //############################################

        public function DSLAM_Line_upsum ()
        {

            $this->gateway->write("show xdsl operational-data line | match exact:up | match exact:up | match exact::  | count summary \r\n");
            $this->gateway->write(" exit all\r\n");


            $this->wait_exit ('','>#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_Line_opup zeige alle aktiven xdsl Lines down
        //############################################

        public function DSLAM_Line_opup ( Location $location)
        {

            $this->gateway->write("show xdsl operational-data line | match exact:up | match exact:down  | count lines \r\n");
            $this->gateway->write(" exit all\r\n");


            $this->wait_exit ('','>#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_Line zeige alle aktiven Lines
        //############################################

        public function DSLAM_Line ( Location $location)
        {

            $this->gateway->write("show xdsl operational-data line | match exact:up | count lines \r\n");
            $this->gateway->write(" exit all\r\n");


            $this->wait_exit ('','>#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# SNTP zeige SNTP Einstellungen
        //############################################

        public function SNTP ()
        {
            $this->gateway->write( " show sntp \r\n");
            $this->gateway->write( " info configure system sntp \r\n");
            $this->gateway->write(" exit all\r\n");


            $this->wait_exit ('','>#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# MAXspeed ändere Backplane speed to 10G
        //############################################

        public function MAXspeed (  Location $location)
        {
            $this->dslam_name_extract ($location->getName(),$location);
            $this->gateway->write( " configure system max-lt-link-speed  link-speed ten-gb\r\n");
            $this->adminsave ($location);

            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# SNTPset setup SNTP
        //############################################

        public function SNTPset (Location $location)
        {
            $this->gateway->write( " configure system sntp server-ip-addr ".\Wisotel\Configuration\Configuration::get('sntpServer')." enable timezone-offset 60 \r\n");
            $this->gateway->write(" exit all\r\n");

            $this->adminsave ($location);
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DPBOshow  zeige alle DPBO Profile
        //############################################

        public function DPBOshow ()
        {
            $this->gateway->write( " info configure xdsl dpbo-profile  flat \r\n");
            $this->gateway->write(" exit all\r\n");

            $this->wait_exit ('','>#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# SNMP zeige SNMP
        //############################################

        public function SNMP ()
        {
            $this->gateway->write( "info configure system security snmp\r\n");
            $this->gateway->write(" exit all\r\n");


            $this->wait_exit ('','>#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# SNMPset SNMP setup für Kunden
        //############################################

        public function SNMPset (Location $location)
        {
            $this->dslam_name_extract ($location->getName(),$location);
            switch (\Wisotel\Configuration\Configuration::get('companyName'))
            {
                case 'ZEAG':
                    //#$this->gateway->write( "info configure system security snmp\r\n");
                    $this->gateway->write( "configure system security snmp user zeagams authentication sha1:plain:Wienaesaithee9ja privacy aes-128:plain:eBi3paPoo4ohSh3e\r\n");
                    $this->gateway->write( "configure system security snmp group zeaggroup security-level privacy-and-auth context all\r\n");
                    $this->gateway->write( "configure system security snmp map-user-group zeagams group-name zeaggroup\r\n");

                    break;

                case 'WiSoTEL':
                    //#$this->gateway->write( "info configure system security snmp\r\n");
                    $this->gateway->write( "configure system security snmp user wisoamsdes authentication sha1:plain:wisotel@2018. privacy des:plain:wisotel@2018.\r\n");
                    $this->gateway->write( "configure system security snmp group wisogroup security-level privacy-and-auth context all\r\n");
                    $this->gateway->write( "configure system security snmp map-user-group wisoamsdes group-name wisogroup\r\n");

                    break;

                case 'Stadtwerke Bühl':
                    //$this->gateway-->write( "info configure system security snmp\r\n");
                    $this->gateway->write( "configure system security snmp user buehlamsdes authentication sha1:plain:buehl@2018. privacy des:plain:buehl@2018.\r\n");
                    $this->gateway->write( "configure system security snmp group buehlgroup security-level privacy-and-auth context all \r\n");
                    $this->gateway->write( "configure system security snmp map-user-group buehlamsdes group-name buehlgroup \r\n");

                    break;
            }

            $this->adminsave ($location);

            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();
        }

        //############################################
        //# DSLAM_Vectorinit // muss noch überarbeitet werden
        //############################################

        public function DSLAM_Vectorinit (  Location $location)
        {
            $this->gateway->write( "configure xdsl vect-profile 1 name VECT_US_DS\r\n");
            $this->gateway->write( "version 1\r\n");
            $this->gateway->write( "leg-can-dn-m1\r\n");
            $this->gateway->write( "band-control-up 1:6\r\n");
            $this->gateway->write( "active\r\n");
            $this->gateway->write( "exit\r\n");

            $this->gateway->write( "configure xdsl vect-profile 11 name ZTV_VECT_US_DS_m1\r\n");
            $this->gateway->write( "version 1\r\n");
            $this->gateway->write( "leg-can-dn-m1\r\n");
            $this->gateway->write( "legacy-cpe\r\n");
            $this->gateway->write( "active\r\n");
            $this->gateway->write( "exit\r\n");
            $this->gateway->write( "configure xdsl vce-profile 1 name vce_default version 1 active\r\n");

            if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName'))
            {
                if ($location->getId() != "43" and $location->getId() != "21" and $location->getId() != "34" and $location->getId() != "32")
                {
                    $this->gateway->write( "configure xdsl board 1/1/9\r\n");
                    $this->gateway->write( "vce-profile 1\r\n");
                    $this->gateway->write( "exit\r\n");
                    $this->gateway->write("configure xdsl board 1/1/9 vplt-autodiscover enable\r\n");
                }
            }

            $this->gateway->write("configure xdsl board 1/1/1 vplt-autodiscover enable\r\n");
            $this->gateway->write( "configure xdsl board 1/1/1 vce-profile 1\r\n");
            $this->gateway->write("configure xdsl board 1/1/2 vplt-autodiscover enable\r\n");
            $this->gateway->write( "configure xdsl board 1/1/2 vce-profile 1\r\n");
            $this->gateway->write("configure xdsl board 1/1/3 vplt-autodiscover enable\r\n");
            $this->gateway->write( "configure xdsl board 1/1/3 vce-profile 1\r\n");
            $this->gateway->write("configure xdsl board 1/1/4 vplt-autodiscover enable\r\n");
            $this->gateway->write( "configure xdsl board 1/1/4 vce-profile 1\r\n");
            $this->gateway->write("configure xdsl board 1/1/5 vplt-autodiscover enable\r\n");
            $this->gateway->write( "configure xdsl board 1/1/5 vce-profile 1\r\n");
            $this->gateway->write("configure xdsl board 1/1/6 vplt-autodiscover enable\r\n");
            $this->gateway->write( "configure xdsl board 1/1/6 vce-profile 1\r\n");
            if ($location->getProcessor() != 'RANT-A' and $location->getProcessor() != 'NRNT-A' )
            {
                $this->gateway->write("configure xdsl board 1/1/7 vplt-autodiscover enable\r\n");
                $this->gateway->write( "configure xdsl board 1/1/7 vce-profile 1\r\n");
                $this->gateway->write("configure xdsl board 1/1/8 vplt-autodiscover enable\r\n");
                $this->gateway->write( "configure xdsl board 1/1/8 vce-profile 1\r\n");
            }

            //jetzt alle möglichen REM's
            $this->gateway->write("configure xdsl board ctrl:1/2 vce-profile 1\r\n");
            $this->gateway->write("configure xdsl board ctrl:1/2 vplt-autodiscover enable\r\n");
            $this->gateway->write("configure xdsl board 1/2/1 vplt-autodiscover enable\r\n");
            $this->gateway->write( "configure xdsl board 1/2/2 vce-profile 1\r\n");
            $this->gateway->write("configure xdsl board ctrl:1/3 vce-profile 1\r\n");
            $this->gateway->write("configure xdsl board ctrl:1/3 vplt-autodiscover enable\r\n");
            $this->gateway->write("configure xdsl board 1/3/1 vplt-autodiscover enable\r\n");
            $this->gateway->write( "configure xdsl board 1/3/2 vce-profile 1\r\n");
            $this->gateway->write("configure xdsl board ctrl:1/4 vce-profile 1\r\n");
            $this->gateway->write("configure xdsl board ctrl:1/4 vplt-autodiscover enable\r\n");
            $this->gateway->write("configure xdsl board 1/4/1 vplt-autodiscover enable\r\n");
            $this->gateway->write( "configure xdsl board 1/4/2 vce-profile 1\r\n");
            $this->gateway->write("configure xdsl board ctrl:2/1 vce-profile 1\r\n");
            $this->gateway->write("configure xdsl board ctrl:2/1 vplt-autodiscover enable\r\n");
            $this->gateway->write("configure xdsl board 2/1/1 vplt-autodiscover enable\r\n");
            $this->gateway->write( "configure xdsl board 2/1/2 vce-profile 1\r\n");

            $this->gateway->write(" exit all\r\n");
            switch ($location->getProcessor())
            {
                case 'RANT - A':
                    $this->gateway->write( "configure xdsl board nt \r\n");
                    $this->gateway->write( "vce-capacity-mode extended\r\n");
                    $this->gateway->write( "vce-profile 1\r\n");
                    $this->gateway->write( "vplt-autodiscover enable \r\n");
                    $this->gateway->write(" exit all\r\n");
                    break;

            }
            $this->gateway->write("show xdsl vp-board\r\n");
            $this->gateway->write(" exit all\r\n");
            $this->adminsave ($location);


            $this->wait_exit ('','','');

            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_Bandwith_setW
        //############################################

        public function DSLAM_Bandwith_setW ()
        {
            wiso_produkte ($usenet);
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_Bandwith_set alle Profile in den DSLAM schreiben
        //############################################


        public function DSLAM_Bandwith_set ()
        {

            $this->products();
            $this->gateway->write ("#Job has finished KHL\r\n");
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_Bandwith_set alle Profile in den DSLAM schreiben
        //############################################

        public function products ()
        {
            //require_once __DIR__.'/buehl_produkte1.php';

            //buehl_produkte1($this->gateway);
            switch (\Wisotel\Configuration\Configuration::get('companyName'))
            {
                case 'ZEAG':
                    require_once __DIR__.'/zeag_produkte1.php';

                    zeag_produkte1($this->gateway);

                    break;

                case 'WiSoTEL':
                    require_once __DIR__.'/wiso_produkte.php';

                    wiso_produkte1($this->gateway);

                    break;

                case 'Stadtwerke Bühl':
                    require_once __DIR__.'/buehl_produkte1.php';

                    buehl_produkte1($this->gateway);

                    break;
            }
        }

        //############################################
        //# DSLAM_QOS zeige alle qos und xdsl Profile
        //############################################

        public function DSLAM_QOS ()
        {
            $this->gateway->write( "info configure qos profiles flat\r\n");
            $this->gateway->write( "info configure xdsl service-profile flat\r\n");
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# Vlan_set richte neuies VLAN ein : globale _Kommandos
        //############################################

        public function Vlan_set (Location $location,$Dslam_comando2,$Dslam_comando3)
        {
            $this->vlan_setup_new ( $Dslam_comando2, $Dslam_comando3);
            $this->adminsave ($location);

            $this->wait_exit ('true','last','');

            return $this->gateway->getLastReadData();

        }

        //############################################
        //# Vlan_delete
        //############################################

        public function Vlan_delete (Location $location,$Dslam_comando2)

        {
            $this->gateway->write( " configure vlan no id $Dslam_comando2  \r\n");
            $this->gateway->write( "exit\r\n");

            $this->wait_exit ('true','last','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_Vlan_set
        //############################################

        public function DSLAM_Vlan_set (Customer $customer, Location $location)
        {
            $vlansWritten = false;

            try
            {
                // try to get vlans from ipRanges

                if (empty($location['network_id']))
                {
                    throw new Excption('Failure: network_id in location not set');
                }

                $statement = $db->prepare($sql['ipRangesVlansByNetworkId']);

                if (!empty($db->error))
                {
                    throw new \Exception($db->error." - ".$sql['ipRangesVlansByNetworkId']);
                }

                $statement->bind_param('i', $location['network_id']);

                if (!$statement->execute())
                {
                    throw new Excption(sprintf('Failure: query "ipRangesVlansByNetworkId" failed. Db - error: % s',
                                               $db->error
                                              ));
                }

                $ipRangesVlansResult = $statement->get_result();
                $statement->close();

                if ($ipRangesVlansResult->num_rows > 0)
                {
                    while ($ipRangesVlans = $ipRangesVlansResult->fetch_assoc())
                    {
                        $vlanName = $vlan_name1.'_IpRange_'.$ipRangesVlans['vlan_ID'];



                        echo sprintf('<pre>%s</pre>', $Antwort);
                        $this->vlan_setup_new( $ipRangesVlans['vlan_ID'], $vlanName);

                        $this->lan_setup_new( $ipRangesVlans['vlan_ID'], $vlanName);


                        $vlansWritten = true;
                    }
                }

                $ipRangesVlansResult->close();
            }
            catch (Excption $exception)
            {
                echo $exception->getMessage();

                return; // an error occurred, stop here
            }

            // set vlans from location

            $vlans = [
                         'vlan_iptv',
                         'vlan_acs',
                         'vlan_dcn',
                         'vlan_pppoe',
                         'vlan_iptv_local',
                         'vlan_pppoe_local',
                         'vlan_acs_local',
                     ];

            foreach ($vlans as $vlan)
            {
                if (!empty($location[$vlan]))
                {
                    $vlanId = $location[$vlan];
                    $vlanName = $vlan_name1.$vlan;

                    $this->vlan_setup_new( $vlanId, $vlanName);
                    $this->vlan_setup_new( $vlanId, $vlanName);

                    $vlansWritten = true;
                }
            }

            if ($vlansWritten)
            {
                $this->gateway->write("$admin_save. \r\n");
            }
            else
            {
                echo "No Vlans configured!";
            }

            /*  khl original
                $vlan_id     = '7';
                $vlan_name = 'ZEAG_F_BOX_Internet';
                vlan_setup_new($vlan_id,$vlan_name);
                vlan_setup_new($vlan_id,$vlan_name);
                $vlan_id     = '8';
                $vlan_name = 'ZEAG_F_BOX_iptv';
                vlan_setup_new($vlan_id,$vlan_name);
                vlan_setup_new($vlan_id,$vlan_name);
                $vlan_id     = '9';
                $vlan_name = 'ZEAG_F_BOX_acs';
                vlan_setup_new($vlan_id,$vlan_name);
                vlan_setup_new($vlan_id,$vlan_name);
                $vlan_id     = "$vlan_pppoe";
                $vlan_name = "$vlan_name1"."_pppoe_network";
                vlan_setup_new($vlan_id,$vlan_name);
                vlan_setup_new($vlan_id,$vlan_name);
                $vlan_id     = "$vlan_iptv";
                $vlan_name = "$vlan_name1"function vlan_setup."_iptv_network";
                vlan_setup_new($vlan_id,$vlan_name);
                vlan_setup_new($vlan_id,$vlan_name);
                $vlan_id     = "$location->getVlanAcs()";
                $vlan_name = "$vlan_name1"."_acs_network";
                vlan_setup_new($vlan_id,$vlan_name);
                vlan_setup_new($vlan_id,$vlan_name);
                $vlan_id     = "$vlan_dcn";
                $vlan_name = "$vlan_name1"."_DCN";
                vlan_setup_new($vlan_id,$vlan_name);
                vlan_setup_new($vlan_id,$vlan_name);
                $this->gateway->write( "$admin_save \r\n");
            */
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# CardCommit
        //############################################


        public function CardCommit ( Location $location,$Dslam_comando2,$Dslam_comando3,$Dslam_comando4,$Dslam_comando5)
        {
            $Dslam_comando3 = $Dslam_comando3.$Dslam_comando2;
            $this->gateway->write( "configure equipment slot $Dslam_comando3 \r\n");
            $this->gateway->write( "no planned-type  \r\n");
            $this->gateway->write( "planned-type $Dslam_comando4 \r\n");
            $this->gateway->write( "exit\r\n");
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# Service_create
        //############################################


        public function Service_create ( Location $location,$Dslam_comando2,$Dslam_comando3,$Dslam_comando4,$Dslam_comando5)
        {
            $sfp_port = "sfp";
            switch ($location->getProcessor())
            {
                case 'NANT - E':
                case 'FANT - F':
                    $sfp_port = "xfp";
            }
            $this->gateway->write( "configure service vpls $Dslam_comando2 customer $Dslam_comando4 v-vpls vlan $Dslam_comando2 create\r\n");
            if ($Dslam_comando3 != 'No') {
                $this->gateway->write( "sap $Dslam_comando3:$Dslam_comando2 create\r\n");
                $this->gateway->write( "exit\r\n");
            }

            $this->gateway->write( "sap lt:1/1/$Dslam_comando5:$Dslam_comando2 create \r\n");
            $this->gateway->write( "exit\r\n");
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }


        //############################################
        // Service_delete
        //############################################
        public function Service_delete ( Location $location,$Dslam_comando2,$Dslam_comando3,$Dslam_comando4,$Dslam_comando5)
        {
            $sfp_port = "sfp";
            switch ($location->getProcessor())
            {
                case 'NANT - E':
                case 'FANT - F':
                    $sfp_port = "xfp";
            }
            $this->gateway->write( "configure service vpls $Dslam_comando2 customer $Dslam_comando4 v-vpls vlan $Dslam_comando2 \r\n");
            if ($Dslam_comando3 != 'No') {
                $this->gateway->write( "sap $Dslam_comando3:$Dslam_comando2 \r\n");
                $this->gateway->write( "shutdown \r\n");
                $this->gateway->write( "exit\r\n");
                $this->gateway->write( "no sap $Dslam_comando3:$Dslam_comando2 \r\n");
            }

            $this->gateway->write( "sap lt:1/1/$Dslam_comando5:$Dslam_comando2 \r\n");
            $this->gateway->write( "shutdown \r\n");
            $this->gateway->write( "exit\r\n");
            $this->gateway->write( "no sap lt:1/1/$Dslam_comando5:$Dslam_comando2 \r\n");

            $this->gateway->write( "exit\r\n");
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        // VPLS_delete
        //############################################
        public function VPLS_delete ( Location $location,$Dslam_comando2,$Dslam_comando3)
        {

            $this->gateway->write( "configure service vpls $Dslam_comando2 customer $Dslam_comando3 v-vpls vlan $Dslam_comando2 \r\n");
            $this->gateway->write( "shutdown \r\n");
            $this->gateway->write( "exit\r\n");
            $this->gateway->write( "no vpls $Dslam_comando2 \r\n");

            $this->gateway->write( "exit\r\n");
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        // VPLS_create
        //############################################
        public function VPLS_create ( Location $location,$Dslam_comando2,$Dslam_comando3)
        {

            $this->gateway->write( "configure service vpls $Dslam_comando2 customer $Dslam_comando3 v-vpls vlan $Dslam_comando2 create\r\n");
            $this->gateway->write( "no shutdown \r\n");
            $this->gateway->write( "stp\r\n");
            $this->gateway->write( "shutdown\r\n");
            $this->gateway->write( "exit\r\n");
            $this->gateway->write( "exit\r\n");
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_Service_modify
        //############################################


        public function DSLAM_Service_modify (  Location $location)
        {
            switch ($location->getProcessor())
            {
                case 'NANT - E':
                case 'FANT - F':
                    if ($location->getVlanAcs() != '')
                    {
                        $this->gateway->write( "configure service vpls ".$location->getVlanAcs()." customer 1 v-vpls vlan ".$location->getVlanAcs()." \r\n");
                        $this->gateway->write( "stp\r\n");
                        $this->gateway->write( "shutdown\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap nt-a:xfp:1:".$location->getVlanAcs()." \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap nt-a:xfp:2:".$location->getVlanAcs()." \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/1:".$location->getVlanAcs()." \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/2:".$location->getVlanAcs()." \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/3:".$location->getVlanAcs()." \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/4:".$location->getVlanAcs()." \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/5:".$location->getVlanAcs()." \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/6:".$location->getVlanAcs()." \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/7:".$location->getVlanAcs()." \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/8:".$location->getVlanAcs()." \r\n");
                        $this->gateway->write( "exit\r\n");

                        if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName'))
                        {
                            if ($location->getId() != "43" and $location->getId() != "12" and $location->getId() != "32" and $location->getId() != "31" and $location->getId() != "34" and $location->getId() != "59")
                            {
                                $this->gateway->write( "sap lt:1/1/10:".$location->getVlanAcs()." \r\n");
                                $this->gateway->write( "exit\r\n");
                            }
                            if ($location->getId() != "32" and $location->getId() != "12"  and $location->getId() != "59")
                            {
                                $this->gateway->write( "sap lag-1:".$location->getVlanAcs()." \r\n");
                                $this->gateway->write( "exit\r\n");
                            }
                        }
                    }
                    $this->gateway->write( "no shutdown\r\n");

                    $this->gateway->write( "exit\r\n");
                    if ($location->getVlanPppoe() != '')
                    {
                        $this->gateway->write( "vpls ".$location->getVlanPppoe()." customer 1 v-vpls vlan ".$location->getVlanPppoe()." \r\n");
                        $this->gateway->write( "stp\r\n");
                        $this->gateway->write( "shutdown\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap nt-a:xfp:1:".$location->getVlanPppoe()." \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap nt-a:xfp:2:".$location->getVlanPppoe()." \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/1:".$location->getVlanPppoe()." \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/2:".$location->getVlanPppoe()." \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/3:".$location->getVlanPppoe()." \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/4:".$location->getVlanPppoe()." \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/5:".$location->getVlanPppoe()." \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/6:".$location->getVlanPppoe()." \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/7:".$location->getVlanPppoe()." \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/8:".$location->getVlanPppoe()." \r\n");
                        $this->gateway->write( "exit\r\n");

                        if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName'))
                        {
                            if ($location->getId() != "43" and $location->getId() != "12" and $location->getId() != "32" and $location->getId() != "31"  and $location->getId() != "34" and $location->getId() != "59")
                            {
                                $this->gateway->write( "sap lt:1/1/10:".$location->getVlanPppoe()." \r\n");
                                $this->gateway->write( "exit\r\n");
                            }
                            if ($location->getId() != "32" and $location->getId() != "12" and $location->getId() != "59")
                            {
                                $this->gateway->write( "sap lag-1:".$location->getVlanPppoe()." \r\n");
                                $this->gateway->write( "exit\r\n");
                            }
                        }

                        $this->gateway->write( "no shutdown\r\n");

                        $this->gateway->write( "exit\r\n");
                    }
                    if ( $location->getVlanIptv() != '')
                    {
                        $this->gateway->write( "vpls ".$location->getVlanIptv()." customer 1 v-vpls vlan ".$location->getVlanIptv()." \r\n");
                        $this->gateway->write( "stp\r\n");
                        $this->gateway->write( "shutdown\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "no shutdown\r\n");
                        $this->gateway->write( "exit\r\n");

                        $this->gateway->write( "sap nt-a:xfp:1:".$location->getVlanIptv()." \r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "mrouter-port\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "exit\r\n");

                        $this->gateway->write( "sap nt-a:xfp:2:".$location->getVlanIptv()." \r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "mrouter-port\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "exit\r\n");

                        $this->gateway->write( "sap lt:1/1/1:".$location->getVlanIptv()." \r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "send-queries\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "exit\r\n");

                        $this->gateway->write( "sap lt:1/1/2:".$location->getVlanIptv()." \r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "send-queries\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "exit\r\n");

                        $this->gateway->write( "sap lt:1/1/3:".$location->getVlanIptv()." \r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "send-queries\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "exit\r\n");

                        $this->gateway->write( "sap lt:1/1/4:".$location->getVlanIptv()." \r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "send-queries\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "exit\r\n");

                        $this->gateway->write( "sap lt:1/1/5:".$location->getVlanIptv()." \r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "send-queries\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "exit\r\n");

                        $this->gateway->write( "sap lt:1/1/6:".$location->getVlanIptv()." \r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "send-queries\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "exit\r\n");

                        $this->gateway->write( "sap lt:1/1/7:".$location->getVlanIptv()." \r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "send-queries\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "exit\r\n");

                        $this->gateway->write( "sap lt:1/1/8:".$location->getVlanIptv()." \r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "send-queries\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "exit\r\n");

                        if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName'))
                        {
                            if ($location->getId() != "32" and $location->getId() != "12"  )
                            {
                                $this->gateway->write( "sap lag-1:".$location->getVlanIptv()." \r\n");
                                $this->gateway->write( "igmp-snooping\r\n");
                                $this->gateway->write( "mrouter-port\r\n");
                                $this->gateway->write( "exit\r\n");
                                $this->gateway->write( "exit\r\n");
                            }
                            if ($location->getId() != "32" and $location->getId() != "12" and $location->getId() != "43" and $location->getId() != "31" and $location->getId() != "34"  and $location->getId() != "59" )
                            {
                                $this->gateway->write( "sap lt:1/1/10:".$location->getVlanIptv()." \r\n");
                                $this->gateway->write( "igmp-snooping\r\n");
                                $this->gateway->write( "send-queries\r\n");
                                $this->gateway->write( "exit\r\n");
                                $this->gateway->write( "exit\r\n");
                            }
                        }

                        $this->gateway->write( "no shutdown\r\n");

                        $this->gateway->write( "exit\r\n");
                    }

                    break;
                case 'NRNT - A':
                    //#echo " Service noch nicht implementiert fuer $prozessor";
                    $this->gateway->write("  igmp shub igs-system enable-snooping self-ip-addr-mode \r\n");
                    $this->gateway->write("  self-ip-addr ".$location->getRealIp()." start-snooping \r\n");
                    $this->gateway->write("  igmp system start \r\n");
                    $this->gateway->write("  igmp package 1 name IPTV template-version 1 \r\n");
                    $this->gateway->write("  mcast general package-member 1 \r\n");
                    break;
                case 'RANT - A':
                    $this->gateway->write( "configure bridge port nt-a:xfp:1\r\n");
                    $this->gateway->write( "  max-unicast-mac 2496\r\n");
                    $this->gateway->write( "  vlan-id ".$location->getVlanPppoe()."\r\n");
                    $this->gateway->write( "  exit\r\n");
                    $this->gateway->write( "  vlan-id ".$location->getVlanAcs()."\r\n");
                    $this->gateway->write( "  exit\r\n");
                    $this->gateway->write( "  vlan-id ".$location->getVlanIptv()."\r\n");
                    $this->gateway->write( "  exit all\r\n");
                    break;

            }
            $this->adminsave ( $location);
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_Service_set
        //############################################


        public function DSLAM_Service_set (  Location $location)
        {
            switch ($location->getProcessor())
            {
                case 'NANT - E':
                case 'FANT - F':
                    if ($location->getVlanAcs() != '')
                    {
                        $this->gateway->write( "configure service vpls ".$location->getVlanAcs()." customer 1 v-vpls vlan ".$location->getVlanAcs()." create\r\n");
                        $this->gateway->write( "stp\r\n");
                        $this->gateway->write( "shutdown\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap nt-a:xfp:1:".$location->getVlanAcs()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap nt-a:xfp:2:".$location->getVlanAcs()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/1:".$location->getVlanAcs()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/2:".$location->getVlanAcs()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/3:".$location->getVlanAcs()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/4:".$location->getVlanAcs()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/5:".$location->getVlanAcs()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/6:".$location->getVlanAcs()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/7:".$location->getVlanAcs()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/8:".$location->getVlanAcs()." create \r\n");
                        $this->gateway->write( "exit\r\n");

                        if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName'))
                        {
                            if ($location->getId() != "32" and $location->getId() != "12"  and $location->getId() != "59")
                            {
                                $this->gateway->write( "sap lag-1:".$location->getVlanAcs()." create \r\n");
                                $this->gateway->write( "exit\r\n");
                            }
                            if ($location->getId() != "43" and $location->getId() != "12" and $location->getId() != "32" and $location->getId() != "31" and $location->getId() != "34"  and $location->getId() != "59")
                            {
                                $this->gateway->write( "sap lt:1/1/10:".$location->getVlanAcs()." create \r\n");
                                $this->gateway->write( "exit\r\n");
                            }
                        }

                        $this->gateway->write( "no shutdown\r\n");

                        $this->gateway->write( "exit\r\n");
                    }
                    if ($location->getVlanPppoe() != '')
                    {
                        $this->gateway->write( "vpls ".$location->getVlanPppoe()." customer 1 v-vpls vlan ".$location->getVlanPppoe()." create\r\n");
                        $this->gateway->write( "stp\r\n");
                        $this->gateway->write( "shutdown\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap nt-a:xfp:1:".$location->getVlanPppoe()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap nt-a:xfp:2:".$location->getVlanPppoe()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/1:".$location->getVlanPppoe()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/2:".$location->getVlanPppoe()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/3:".$location->getVlanPppoe()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/4:".$location->getVlanPppoe()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/5:".$location->getVlanPppoe()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/6:".$location->getVlanPppoe()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/7:".$location->getVlanPppoe()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/8:".$location->getVlanPppoe()." create \r\n");
                        $this->gateway->write( "exit\r\n");

                        if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName'))
                        {
                            if ($location->getId() != "32" and $location->getId() != "12" and $location->getId() != "59")
                            {
                                $this->gateway->write( "sap lag-1:".$location->getVlanPppoe()." create \r\n");
                                $this->gateway->write( "exit\r\n");
                            }
                            if ($location->getId() != "43" and $location->getId() != "12" and $location->getId() != "32" and $location->getId() != "31" and $location->getId() != "34"  and $location->getId() != "59")
                            {
                                $this->gateway->write( "sap lt:1/1/10:".$location->getVlanPppoe()." create \r\n");
                                $this->gateway->write( "exit\r\n");
                            }
                        }

                        $this->gateway->write( "no shutdown\r\n");

                        $this->gateway->write( "exit\r\n");
                    }
                    if ($location->getVlanIptv() != '')
                    {
                        $this->gateway->write( "vpls ".$location->getVlanIptv()." customer 1 v-vpls vlan ".$location->getVlanIptv()." create\r\n");
                        $this->gateway->write( "stp\r\n");
                        $this->gateway->write( "shutdown\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap nt-a:xfp:1:".$location->getVlanIptv()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap nt-a:xfp:2:".$location->getVlanIptv()." create \r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/1:".$location->getVlanIptv()." create \r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "send-queries\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/2:".$location->getVlanIptv()." create \r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "send-queries\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/3:".$location->getVlanIptv()." create \r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "send-queries\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/4:".$location->getVlanIptv()." create \r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "send-queries\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/5:".$location->getVlanIptv()." create \r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "send-queries\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/6:".$location->getVlanIptv()." create \r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "send-queries\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/7:".$location->getVlanIptv()." create \r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "send-queries\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "sap lt:1/1/8:".$location->getVlanIptv()." create \r\n");
                        $this->gateway->write( "igmp-snooping\r\n");
                        $this->gateway->write( "send-queries\r\n");
                        $this->gateway->write( "exit\r\n");
                        $this->gateway->write( "exit\r\n");

                        if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName'))
                        {
                            if ($location->getId() != "32" and $location->getId() != "12" and $location->getId() != "59")
                            {
                                $this->gateway->write( "sap lag-1:".$location->getVlanIptv()." create \r\n");
                                $this->gateway->write( "igmp-snooping\r\n");
                                $this->gateway->write( "mrouter-port\r\n");
                                $this->gateway->write( "exit\r\n");
                                $this->gateway->write( "exit\r\n");
                            }
                            if ($location->getId() != "43" and $location->getId() != "12" and $location->getId() != "34" and $location->getId() != "31" and $location->getId() != "32"  and $location->getId() != "59")
                            {
                                $this->gateway->write( "sap lt:1/1/10:".$location->getVlanIptv()." create \r\n");
                                $this->gateway->write( "igmp-snooping\r\n");
                                $this->gateway->write( "exit\r\n");
                                $this->gateway->write( "exit\r\n");
                            }
                        }
                        $this->gateway->write( "no shutdown\r\n");
                    }

                    $this->gateway->write( "exit\r\n");

                    break;
                case 'NRNT - A':
                    //#echo " Service noch nicht implementiert fuer $prozessor";
                    $this->gateway->write("  igmp shub igs-system enable-snooping self-ip-addr-mode \r\n");
                    $this->gateway->write("  self-ip-addr ".$location->getRealIp()."  start-snooping \r\n");
                    $this->gateway->write("  igmp system start \r\n");
                    $this->gateway->write("  igmp package 1 name IPTV template-version 1 \r\n");
                    $this->gateway->write("  mcast general package-member 1 \r\n");

                    break;
                case 'RANT - A':
                    $this->gateway->write( "configure bridge port nt-a:xfp:1\r\n");
                    $this->gateway->write( "  max-unicast-mac 2496\r\n");
                    $this->gateway->write( "  vlan-id ".$location->getVlanPppoe()."\r\n");
                    $this->gateway->write( "  exit\r\n");
                    $this->gateway->write( "  vlan-id ".$location->getVlanAcs()."\r\n");
                    $this->gateway->write( "  exit\r\n");
                    $this->gateway->write( "  vlan-id ".$location->getVlanIptv()."\r\n");
                    $this->gateway->write( "  exit all\r\n");
                    //#echo " Service noch nicht implementiert fuer $prozessor";
                    break;

            }
            $this->adminsave ( $location);
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# Backupshow
        //############################################

        public function Backupshow ()
        {
            $down_red = 'so nicht';
            $this->gateway->write( " show software-mngt upload-download \r\n");
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_sSecurity_show
        //############################################

        public function DSLAM_sSecurity_show ( Location $location)
        {

            $this->gateway->write("info configure system security \r\n");
            $this->gateway->write(" exit all\r\n");
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_sSecurity_set
        //############################################

        public function DSLAM_sSecurity_set (Location $location)
        {

            switch (\Wisotel\Configuration\Configuration::get('companyName'))
            {
                case 'ZEAG':
                    $login_banner = "\"//#####################################\\r\\n#//                                  ##\\r\\n#//        <<< $this->dslam_name >>>        ##\\r\\n#//                                  ##\\r\\n#//     Authorized access only!      ##\\r\\n#//                                  ##\\r\\n#//           Z  E  A  G             ##\\r\\n#//                                  ##\\r\\n//#####################################\\r\\n//#####################################\"";

                    break;

                case 'WiSoTEL':
                    $login_banner = "\"//#####################################\\r\\n#//                                  ##\\r\\n#//    <<< $this->dslam_name_ext >>>        ##\\r\\n#//                                  ##\\r\\n#//     Authorized access only!      ##\\r\\n#//                                  ##\\r\\n#//          W i S o T E L           ##\\r\\n#//                                  ##\\r\\n//#####################################\\r\\n//#####################################\"";

                    break;
            }

            $welcome_banner = "\"//#################################\\r\\n#//         $dslam_name           ##\\r\\n#//MNGT IP      |$logon_ip     ##\\r\\n##-------------------------------##\\r\\n#//MGMT-VLAN    |".$location->getVlanDcn() ."            ##\\r\\n##-------------------------------##\\r\\n#//S-VLAN-PPPOE |".$location->getVlanPppoe() ."           ##\\r\\n##-------------------------------##\\r\\n#//S-VLAN-IPTV  |".$location->getVlanIptv() ."           ##\\r\\n##-------------------------------##\\r\\n#//S-VLAN-ACS   |".$location->getVlanAcs() ."           ##\\r\\n//#################################\"";

            //#echo " &nbsp;&nbsp;&nbsp;&nbsp;$login_banner";

            if ($dslam_name != '')
            {
                $this->gateway->write("configure system security \r\n");
                $this->gateway->write("login-banner $login_banner\r\n" );
                $this->gateway->write("welcome-banner $welcome_banner\r\n" );
                $this->gateway->write(" operator isadmin prompt $dslam_name%d%c \r\n");
                $this->gateway->write(" exit all\r\n");
                $this->gateway->write( "$admin_save \r\n");
            }
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_extendet_slot
        //############################################

        public function DSLAM_extendet_slot ( Location $location)
        {
            switch ($location->getProcessor())
            {
                case 'NANT - E':
                case 'FANT - F':
                case 'NANT - A':
                case 'NANT - C':
                case 'NANT - D':
                    $this->gateway->write("configure system port-num-in-proto type-based \r\n");
                    $this->gateway->write("configure system security profile admin slot-numbering type-based\r\n");
                    $this->gateway->write("configure  equipment shelf 1/1 mode extended-lt-slots \r\n");
                    $this->gateway->write(" exit all\r\n");

                    break;
                case 'NRNT - A':
                    echo " extendet slot nicht vorhanden fuer ".$location->getProcessor()."<br>";
                    break;
                case 'RANT - A':
                    $this->gateway->write("configure system port-num-in-proto type-based \r\n");
                    $this->gateway->write("configure system security profile admin slot-numbering type-based\r\n");
                    //#configure system security profile admin slot-numbering type-based
                    echo " extendet slot nicht vorhanden fuer ".$location->getProcessor()."<br>";
                    break;

            }
            $this->adminsave ( $location);
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_slot_numm
        //############################################

        public function DSLAM_slot_numm ()
        {
            $this->gateway->write("configure system port-num-in-proto type-based \r\n");
            $this->gateway->write("configure system security profile admin slot-numbering type-based\r\n");
            $this->gateway->write(" exit all\r\n");
            $this->gateway->write( "$admin_save \r\n");
            $this->gateway->write( "admin equipment reboot-isam without-self-test\r\n");
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_bridge_port
        //############################################

        public function DSLAM_bridge_port ()
        {

            $this->gateway->write("show vlan bridge-port-fdb \r\n");
            $this->gateway->write( "show vlan fdb-board \r\n");
            $this->gateway->write(" exit all\r\n");
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();
        }

        //############################################
        //# Backup
        //############################################

        public function Backup (Location $location)
        {

            $this->dslam_name_extract(($location->getName()),$location);
            $this->gateway->write( "$this->admin_save \r\n");
            $this->gateway->write("$this->Backup_id \r\n");
            $this->gateway->write( "show software-mngt upload-download  \r\n");
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_Backup_show
        //############################################

        public function DSLAM_Backup_show ()
        {

            $this->gateway->write( "show software-mngt upload-download  \r\n");
            $this->gateway->write("show software-mngt oswp\r\n");
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# Backupladen
        //############################################

        public function Backupladen ()
        {
            $this->dslam_name_extract(($location->getName()),$location);
            $this->gateway->write( "$this->Backup_restore  \r\n");
            $this->gateway->write( "show software-mngt upload-download  \r\n");
            $this->gateway->write("show software-mngt oswp\r\n");
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# Backupaktivieren oswp 1 activieren mit linked-db
        //############################################

        public function Backupaktivieren1 ()
        {
            $this->gateway->write( "admin software-mngt oswp 1 activate with-linked-db  \r\n");
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# Backupaktivieren oswp 1 activieren mit linked-db
        //############################################

        public function Backupaktivieren2 ()
        {
            $this->gateway->write( "admin software-mngt oswp 2 activate with-linked-db  \r\n");
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# DSLAM_oswp_set der TFTP Server wird gesetzt
        //############################################

        public function DSLAM_oswp_set ()
        {
            $tftpserver = \Wisotel\Configuration\Configuration::get('tftpServer');
            $search = ":";
            $count = strpos ($tftpserver , $search  );
            $tftpserverip = $tftpserver;
            if ($count != 0) $tftpserverip = substr ($tftpserver,0,$count);
            $this->gateway->write( "configure software-mngt oswp 1 primary-file-server-id ".$tftpserverip."\r\n");
            $this->gateway->write( "configure software-mngt oswp 2 primary-file-server-id ".$tftpserverip."\r\n");
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# ShowOntOptics
        //############################################

        public function ShowOntOptics ()
        {

            $this->gateway->write("show equipment ont sw-version \r\n");
            $this->gateway->write("show pon sw-download \r\n");

            $this->gateway->write(" exit all\r\n");
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# ShowPonOptics
        //############################################

        public function ShowPonOptics ()
        {

            $this->gateway->write("show pon sfp-inventory \r\n");
            $this->gateway->write("show pon optics \r\n");
            $this->gateway->write("info configure pon flat \r\n");
            $this->gateway->write(" exit all\r\n");
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //#######################//#################################
        //TFTP write ONT Firmware
        //tftp DSLAMIP
        //( put FE56065AFIA53 /pub/OntSw/Download/FE56065AFIA53 ) ?
        //( put FE56065AFEB65 /pub/OntSw/Download/FE56065AFEB65 ) ?
        //#######################//##################################

        //############################################
        //# ShowOntStatus
        //############################################

        public function ShowOntStatus ()
        {

            $this->gateway->write("show interface port | match exact:ont: \r\n");

            $this->gateway->write(" exit all\r\n");
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# ShowOntupS
        //############################################

        public function ShowOntupS ()
        {

            $this->gateway->write("show interface port | match exact:ont:  | match exact:up | match exact:up   | match exact::  | count summary \r\n");

            $this->gateway->write(" exit all\r\n");
            $this->wait_exit ('true','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# ShowOntdownS
        //############################################

        public function ShowOntdownS ()
        {

            $this->gateway->write("show interface port | match exact:ont:  | match exact:down  | match exact::  | count summary \r\n");

            $this->gateway->write(" exit all\r\n");
            $this->wait_exit ('','#','');

            return $this->gateway->getLastReadData();

        }

        //############################################
        //# ShowOntup
        //############################################

        public function ShowOntup ()
        {


            $this->gateway->write("show interface port | match exact:ont:  | match exact:up   | match exact:down skip \r\n");

            $this->gateway->write(" exit all\r\n");
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# ShowOntdown
        //############################################

        public function ShowOntdown ()
        {

            $this->gateway->write("show interface port | match exact:ont:  | match exact:up  | match exact:down   \r\n");

            $this->gateway->write(" exit all\r\n");
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }

        //############################################
        //# Spectrum_setzen
        //############################################

        public function Spectrum_setzen ()
        {
            $this->gateway->write( "configure xdsl spectrum-profile 1 name VDSL2_17a_excladsl2plus version 1 dis-ansi-t1413 dis-etsi-dts dis-g992-1-a dis-g992-1-b dis-g992-2-a dis-g992-3-a dis-g992-3-b dis-etsi-ts g993-2-17a rf-band-list 07:12:07:d0:19:0d:ac:0e:d8:19:1b:58:1b:bc:19:27:74:27:a6:19:36:b0:38:0e:19\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 1 vdsl vdsl-band-plan annex-b-998ade optional-band up optional-endfreq 276 adsl-band allow-adsl max-agpowlev-up 145 opt-startfreq 138\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 1 vdsl pbo 1 param-a 4000\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 1 vdsl pbo 2 param-a 4450 param-b 2930\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 1 vdsl pbo 3 param-a 4550 param-b 1660\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 1 vdsl pbo 4 param-a 4000 param-b 1077\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 1 vdsl pbo 5 param-a 4000 param-b 943\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 1 vdsl2 max-agpowlev-down 145 max-agpowlev-up 145 psd-shape-down regionbm2-psd-down cs-psd-shape-dn 00:01:14:be:08:a0:be:08:a0:60 psd-shape-up regionbm2-psd-up rx-psd-shape-up equal-fext psd-pbo-par-a-up 0f:a0:11:62:11:c6:0f:a0:0f:a0 psd-pbo-par-b-up 00:00:0b:72:06:7c:04:35:03:af\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 1 vdsl2 cust-psd-pt-down 1 frequency 276 psd 190\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 1 vdsl2 cust-psd-pt-down 2 frequency 2208 psd 190\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 1 vdsl2 cust-psd-pt-down 3 frequency 2208 psd 96\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 1 vdsl2 pbo 1 equal-fext 18 param-a 4000\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 1 vdsl2 pbo 2 equal-fext 237 param-a 4450 param-b 2930\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 1 vdsl2 pbo 3 equal-fext 118 param-a 4550 param-b 1660\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 1 vdsl2 pbo 4 equal-fext 18 param-a 4000 param-b 1077\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 1 vdsl2 pbo 5 equal-fext 18 param-a 4000 param-b 943\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 1 active\r\n");

            $this->gateway->write( "configure xdsl spectrum-profile 2 name ADSL2_Plus version 1 dis-ansi-t1413 dis-etsi-dts dis-g992-1-a dis-g992-1-b dis-g992-2-a dis-g992-3-a dis-g992-3-b g992-5-b dis-etsi-ts\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 2 vdsl vdsl-band-plan annex-b-998ade adsl-band allow-adsl\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 2 vdsl pbo 1 param-a 4000\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 2 vdsl pbo 2 param-a 4730 param-b 1977\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 2 vdsl pbo 3 param-a 5400 param-b 1577\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 2 vdsl pbo 4 param-a 5400 param-b 1077\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 2 vdsl pbo 5 param-a 5400 param-b 943\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 2 vdsl2 pbo 1 param-a 4000\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 2 vdsl2 pbo 2 param-a 4730 param-b 1977\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 2 vdsl2 pbo 3 param-a 5400 param-b 1577\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 2 vdsl2 pbo 4 param-a 5400 param-b 1077\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 2 vdsl2 pbo 5 param-a 5400 param-b 943\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 2 active\r\n");

            $this->gateway->write( "configure xdsl spectrum-profile 3 name VDSL_17a_VDSL_allow version 1 dis-ansi-t1413 dis-etsi-dts dis-g992-1-a dis-g992-1-b dis-g992-2-a dis-g992-3-a dis-g992-3-b dis-etsi-ts g993-2-17a\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 3 vdsl vdsl-band-plan annex-b-998ade optional-band up optional-endfreq 276 max-agpowlev-up 145\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 3 vdsl pbo 1 param-a 4000\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 3 vdsl pbo 2 param-a 4450 param-b 2930\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 3 vdsl pbo 3 param-a 4550 param-b 1660\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 3 vdsl pbo 4 param-a 4000\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 3 vdsl pbo 5 param-a 4000\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 3 vdsl2 propr-feat-value 0 max-agpowlev-down 145 max-agpowlev-up 145 psd-shape-down regionbm2-psd-down psd-shape-up regionbm2-psd-up rx-psd-shape-up equal-fext psd-pbo-par-a-up 0f:a0:11:62:11:c6:0f:a0:0f:a0 psd-pbo-par-b-up 00:00:0b:72:06:7c:00:00:00:00\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 3 vdsl2 pbo 1 equal-fext 18 param-a 4000\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 3 vdsl2 pbo 2 equal-fext 237 param-a 4450 param-b 2930\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 3 vdsl2 pbo 3 equal-fext 118 param-a 4550 param-b 1660\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 3 vdsl2 pbo 4 equal-fext 18 param-a 4000\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 3 vdsl2 pbo 5 equal-fext 18 param-a 4000\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 3 active\r\n");

            $this->gateway->write( "configure xdsl spectrum-profile 4 name VDSL_17a_incl_adsl version 1 dis-ansi-t1413 dis-etsi-dts dis-g992-1-a dis-g992-1-b dis-g992-2-a dis-g992-3-a dis-g992-3-b dis-etsi-ts g993-2-17a\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 4 vdsl vdsl-band-plan annex-b-998ade optional-band up optional-endfreq 276 adsl-band allow-adsl max-agpowlev-up 145\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 4 vdsl pbo 1 param-a 4000\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 4 vdsl pbo 2 param-a 4450 param-b 2930\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 4 vdsl pbo 3 param-a 4550 param-b 1660\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 4 vdsl pbo 4 param-a 4000\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 4 vdsl pbo 5 param-a 4000\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 4 vdsl2 propr-feat-value 0 max-agpowlev-down 145 max-agpowlev-up 145 psd-shape-down regionbm2-psd-down psd-shape-up regionbm2-psd-up rx-psd-shape-up equal-fext psd-pbo-par-a-up 0f:a0:11:62:11:c6:0f:a0:0f:a0 psd-pbo-par-b-up 00:00:0b:72:06:7c:00:00:00:00\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 4 vdsl2 pbo 1 equal-fext 18 param-a 4000\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 4 vdsl2 pbo 2 equal-fext 237 param-a 4450 param-b 2930\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 4 vdsl2 pbo 3 equal-fext 118 param-a 4550 param-b 1660\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 4 vdsl2 pbo 4 equal-fext 18 param-a 4000\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 4 vdsl2 pbo 5 equal-fext 18 param-a 4000\r\n");
            $this->gateway->write( "configure xdsl spectrum-profile 4 active\r\n");
            $this->wait_exit ('','','');
            return $this->gateway->getLastReadData();
        }

        //############################################
        //# spectrum_anzeigen
        //############################################

        public function spectrum_anzeigen ()
        {
            $this->gateway->write( "info configure xdsl spectrum-profile flat\r\n");
            $this->wait_exit ('','#','');
            return $this->gateway->getLastReadData();

        }


        //#### end new khl


        /**
            {@inheritDoc}

            @todo
            implement usage of ssh key
        */
        public function login(array $loginOptions) : bool
        {
            if ($this->isLoggedIn)
            {
                return true;
            }

            // hier noch prüfen, ob vorhanden oder ggf. key
            $this->isLoggedIn = $this->gateway->login($loginOptions['username'], $loginOptions['password']);

            if (!$this->isLoggedIn)
            {
                throw new LoginFailedException('login failed');
            }

            return $this->isLoggedIn;
        }

        /**
            Get method (as string) by an alias. Mapping defined in $this->aliasMap

            @param string $alias

            @return string
        */
        public function getMethodByAlias(string $alias) : ? string
        {
            return isset($this->aliasMap[$alias]) ? $this->aliasMap[$alias] : null;
        }

        /**
            {@inheritDoc}

            @todo
            Implement logout
        */
        public function logout()
        {
            if (!$this->isLoggedIn)
            {
                return true;
            }

            // todo - Implement logout

            $this->gateway->disconnect();
        }

        /**
            {@inheritDoc}
        */
        public function __destruct()
        {
            $this->logout();
        }

        //############################
        //DSLAM Bridge Port setup
        //############################

        public function DSLAM_Bridge ( $line_identifier, $vlan_id, $vlan_pppoe_local, $vlan_pppoe, $vlan_acs_local, $vlan_acs, $vlan_iptv_local, $vlan_iptv, $tag, $tv_service, $atm, $acs_qos_profile,$pppoe_quos_profile)
        {
            $temp=substr($Line, -1);
            $temp1=substr($Line, -3);

            if ($temp == "/" or $temp1 == "/00")
            {

                $this->gateway->write("// error Line_identifier $line_identifier \r\n");
            }
            else
            {

                if ($vlan_id == '' )
                {
                    $vlan_id = $vlan_pppoe;
                }
                else
                {
                    $vlan_pppoe = $vlan_id;
                }
                if ($vlan_id != '')
                {
                    $this->gateway->write( "configure bridge no port $line_identifier$atm \r\n");
                    $this->gateway->write( "configure bridge port $line_identifier$atm \r\n");
                    $this->gateway->write( "max-unicast-mac 16 \r\n");
                    $this->gateway->write("configure bridge port $line_identifier$atm no qos-profile \r\n");
                    $this->gateway->write("configure bridge port $line_identifier$atm vlan-id $vlan_id vlan-scope local qos-profile name:$pppoe_quos_profile \r\n");

                    // $this->gateway->write( "no pvid\r\n");
                    // $this->gateway->write( "no vlan-id $vlan_id \r\n");


                    $this->gateway->write( "configure bridge port $line_identifier$atm vlan-id $vlan_pppoe_local \r\n");
                    //$this->gateway->write( "vlan-scope local \r\n");
                    $this->gateway->write("network-vlan $vlan_id \r\n");  #bei ZEAG ab R.5.4 l2fwder-vlan

                    if ($tag == true )
                    {
                        $this->gateway->write("tag single-tagged\r\n");
                        if ('WiSoTEL' === \Wisotel\Configuration\Configuration::get('companyName'))
                        {
                            $this->gateway->write("exit \r\n");
                            $this->gateway->write("vlan-id 805 vlan-scope loca \r\n");
                            $this->gateway->write("tag single-tagged \r\n");

                            $this->gateway->write("network-vlan 805 \r\n");
                        }

                    }
                    else
                    {
                        $this->gateway->write("tag untagged\r\n");
                        $this->gateway->write("configure bridge port $line_identifier$atm pvid $vlan_pppoe_local \r\n");
                    }

                    if ( $vlan_pppoe_local != $vlan_id)
                    {
                        $this->gateway->write("configure bridge port $line_identifier$atm vlan-id $vlan_pppoe_local vlan-scope local \r\n");
                        //$this->gateway->write( "vlan-scope local \r\n");
                        $this->gateway->write( "network-vlan $vlan_id \r\n");
                    }
                    $this->gateway->write( "exit \r\n");

                    if ( $vlan_acs_local != '')
                    {
                        $this->gateway->write( "configure bridge port $line_identifier$atm vlan-id $vlan_acs_local vlan-scope local \r\n");
                        //$this->gateway->write( "vlan-scope local \r\n");
                        $this->gateway->write( "network-vlan $vlan_acs \r\n");
                        $this->gateway->write("tag single-tagged\r\n");
                        $this->gateway->write("qos-profile name:$acs_qos_profile\r\n");
                        $this->gateway->write( "exit \r\n");
                    }

                    if ($tv_service == "yes" )
                    {
                        if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName'))
                        {
                            if (!empty($vlan_iptv_local))
                            {
                                $this->gateway->write("configure bridge port $line_identifier$atm vlan-id $vlan_iptv_local vlan-scope local \r\n");
                                if ($vlan_iptv != $vlan_iptv_local)
                                {
                                    //$this->gateway->write( "vlan-scope local \r\n");
                                    $this->gateway->write( "network-vlan $vlan_iptv \r\n");
                                }
                                $this->gateway->write("tag single-tagged\r\n");
                                $this->gateway->write("configure igmp channel vlan:$line_identifier$atm:$vlan_iptv_local \r\n");
                                $this->gateway->write("max-num-group 10 mcast-vlan-id $vlan_iptv_local \r\n");
                                $this->gateway->write( "exit \r\n");
                            }
                            else
                            {
                                $this->gateway->write("configure bridge port $line_identifier$atm no vlan-id $vlan_iptv_local\r\n");
                            }
                        }
                        else
                        {
                            echo "vlan_iptv_local nicht gesetzt.<br />\n";
                        }
                    }
                }
                else
                {
                    $this->gateway->write(" // error no VLAN !! \r\n");
                }
            }
            // $this->wait_exit ('','','');
            // return $this->gateway->getLastReadData();
        }

        //############################
        //DSLAM Bridge Port setup
        //############################
        public function DSLAM_Bridge_all($customer,$card, $customerService,$location)
        {
            if ($customer->getCardId()->getLocation()->getId() === $location->getId()) {
                // keine parent location
            } else {
                // ist parent location
                // $location -> parent location
                // $customer->getLocation() -> sub location
                $subLocation = $customer->getCardId()->getLocation();
                $subLocation->getVlanPppoeLocal();#
                $location = $subLocation;
            }
            
            $bridgeremove = true;
            $unicastMac = 16;

            // check, if max unicast macs was set at card port
            if (!empty($customer->getDslamPort()->getMaxUnicastMacs())) {
                $unicastMac = $customer->getDslamPort()->getMaxUnicastMacs();
            }

            //$this->DSLAM_Bridge ($customer->getLineIdentifier(),$customer->getVlanId(),$location->getVlanPppoeLocal(),$location->getVlanPppoe(),$location->getVlanAcsLocal(),$location->getVlanAcs(),$location->getVlanIptvLocal(),$location->getVlanIptv(),$customer->getTag(),$customer->getTvService(),$atm,$location->getAcsQosProfile(),$customerService->getEthernetUpDown());
            $VlanPppoeLocal = $location->getVlanPppoeLocal();
            $VlanPppoe = $location->getVlanPppoe();
            
            if (!$customer->getOverrideInheritedVlanProfiles()) {
            
                if ($customer->getVlanId() != '') 
                {
                    $VlanPppoe = $customer->getVlanId();
                    if ($VlanPppoeLocal == '') $VlanPppoeLocal = $VlanPppoe;
                }
                $pvid = $VlanPppoeLocal;
                if ($VlanPppoeLocal == $VlanPppoe) $VlanPppoe = '';
                if ($VlanPppoeLocal != '')
                {
                    $this->DSLAM_Bridge_OneVlan ( $customer->getLineIdentifier(),$VlanPppoeLocal , $VlanPppoe, $customer->getTag(),$customerService,$unicastMac,$card->getCardType()->getName(),$pvid,$bridgeremove);
                    $this->gateway->write("exit all\r\n");
                    $this->gateway->write("#begBLAU\r\n");
                    $this->gateway->write("#customers vlan wurde geschrieben\r\n");
                    $this->gateway->write("#endBLAU\r\n");
                    $bridgeremove = false;
                    $unicastMac = 0;
                    
                }
                $ACSvlan = $location->getVlanAcsLocal();
                //echo "ACS vlan = $ACSvlan";
                if ( $location->getVlanAcsLocal() != '')
                {
                    $pvid = '';
                    $this->DSLAM_Bridge_OneVlan ( $customer->getLineIdentifier(),$location->getVlanAcsLocal() , $location->getVlanAcs(), $customer->getTag(),$location->getAcsQosProfile(),$unicastMac,$card->getCardType()->getName(),$pvid,$bridgeremove);
                    $this->gateway->write("exit all\r\n");
                    $this->gateway->write("#begBLAU\r\n");
                    $this->gateway->write("#location ACS vlan's wurden geschrieben\r\n");                
                    $this->gateway->write("#endBLAU\r\n");
                    $bridgeremove = false;
                    $unicastMac = 0;

                }
                
                if ( $location->getVlanIptv() != '' and $customer->getTvService() == 'yes' )
                {
                    $pvid = '';
                    $this->DSLAM_Bridge_OneVlan ( $customer->getLineIdentifier(),$location->getVlanIptvLocal() , $location->getVlanIptv(), $customer->getTag(),$customerService,$unicastMac,$card->getCardType()->getName(),$pvid,$bridgeremove);
                    $this->gateway->write("exit all\r\n");
                    $this->gateway->write("#begBLAU\r\n");
                    $this->gateway->write("#location Iptv vlan's wurden geschrieben\r\n"); 
                    $this->gateway->write("#endBLAU\r\n");
                   
                    $bridgeremove = false;
                    $unicastMac = 0;
                    
                }
                if ( $location->getVlanDcn() != '')
                {
                    $pvid = '';
                    $this->DSLAM_Bridge_OneVlan ( $customer->getLineIdentifier(),$location->getVlanDcn() , $location->getVlanDcn(), $customer->getTag(),$customerService,$unicastMac,$card->getCardType()->getName(),$pvid,$bridgeremove);
                    $this->gateway->write("exit all\r\n");
                    $this->gateway->write("#begBLAU\r\n");
                    $this->gateway->write("#location DCN vlan wurden geschrieben\r\n");                
                    $this->gateway->write("#endBLAU\r\n");
                    $bridgeremove = false;
                    $unicastMac = 0;
                }
            }
            
            // $customer->getEffectiveVlanProfiles();
            foreach ($customer->getEffectiveVlanProfiles() as $vlanProfile) {
                $pvid=$vlanProfile->getPvid();
                $profilename = $vlanProfile->getName();
                foreach ($vlanProfile->getVlans() as $vlan) {
                    $vlan_local=$vlan->getVlanId();
                    $vlan_network=$vlan->getNetworkVlan();
                    $Tag=$vlan->getTagging();
                    //$unicastMac->getUnicastMac();
                    $customerServiceL = $vlan->getQosProfile();
                    
                    //if ($customerServiceL == '') $customerServiceL = $customerService;
                    
                    $this->DSLAM_Bridge_OneVlan ( $customer->getLineIdentifier(), $vlan_local, $vlan_network, $Tag,$customerServiceL,$unicastMac,$card->getCardType()->getName(),$pvid,$bridgeremove);
                    
                    $bridgeremove = false;
                    $unicastMac = 0;
                }
                $this->gateway->write("#begBLAU\r\n");
                $this->gateway->write("#vlan's vom $profilename an DSLAM gesendet\r\n");
                $this->gateway->write("#endBLAU\r\n");

            }
            
        }
        public function DSLAM_Bridge_OneVlan ( $line_identifier, $vlan_local, $vlan_network, $tag,$qos_profile,$unicastMac,$cardName,$pvid,$bridgeremove)
        {
            //echo "tag = $tag <br>";
            $temp=substr($Line, -1);
            $temp1=substr($Line, -3);
            if ($tag == '1') $tag = "single-tagged";
            $qos_profile_L = "qos-profile name:$qos_profile";
            if ($qos_profile == '') $qos_profile_L = '';
            $vlanscope = "vlan-scope local";
            if ($customerServiceL == '')
            if ($vlan_network == '' or $vlan_network == $vlan_local) 
            
            {
                $vlanscope = '';
                $vlan_network = '';
            }
            if ($temp == "/" or $temp1 == "/00")
            {

                $this->gateway->write("#// error Line_identifier $line_identifier \r\n");
            }
            else
            {

                if ($vlan_local != '')
                {
                    if ($cardName == "GPON") $line_identifier = $line_identifier."/1/1" ; // only one port on the ont
                    if ($bridgeremove == true) $this->gateway->write( "configure bridge no port $line_identifier \r\n"); // später zum überdenken beim ersten VLAN bridge löschen
                    $this->gateway->write( "configure bridge port $line_identifier \r\n");
                    if ($unicastMac != 0) $this->gateway->write( "max-unicast-mac $unicastMac \r\n");
                    if ($bridgeremove == true) $this->gateway->write("configure bridge port $line_identifier no qos-profile \r\n");
                    $this->gateway->write("configure bridge port $line_identifier vlan-id $vlan_local $vlanscope $qos_profile_L \r\n");

                    // $this->gateway->write( "no pvid\r\n");
                    // $this->gateway->write( "no vlan-id $vlan_id \r\n");


                    $this->gateway->write( "configure bridge port $line_identifier vlan-id $vlan_local \r\n");
                    //$this->gateway->write( "vlan-scope local \r\n");
                    if ($vlan_network != '') $this->gateway->write("network-vlan $vlan_network \r\n");  #bei ZEAG ab R.5.4 l2fwder-vlan

                    if ($tag != false and $tag !=  'untagged')
                    {
                        $this->gateway->write("tag $tag\r\n");
                        
                    }
                    else
                    {
                        $this->gateway->write("tag untagged\r\n");
                        if ($pvid != '')$this->gateway->write("configure bridge port $line_identifier$atm pvid $pvid  \r\n");
                    }
                    $this->gateway->write("exit all\r\n");
                }
                else
                {
                    $this->gateway->write(" #// error no VLAN !! \r\n");
                }
            }
            // $this->wait_exit ('','','');
            // return $this->gateway->getLastReadData();
        }


        //############################
        //DSLAM Bridge Port show
        //############################
        public function DSLAM_Bridge_show( $line_identifier, $vlan_id, $vlan_pppoe_local, $vlan_pppoe, $vlan_acs_local, $vlan_acs, $vlan_iptv_local, $vlan_iptv, $tag, $tv_service, $atm, $acs_qos_profile,$pppoe_quos_profile)
        {
            echo  "configure bridge no port $line_identifier <br>";
            echo  "configure bridge port $line_identifier <br>";
            echo  "max-unicast-mac 14 <br>";
            // echo  "no pvid<br>";
            // echo  "no vlan-id $vlan_id <br>";
            echo  "vlan-id $vlan_pppoe_local <br>";
            if ($tag == true )
            {
                echo " &nbsp; &nbsp; &nbsp; &nbsp; tag single-tagged<br>";
            }
            else
            {
                echo " &nbsp; &nbsp; &nbsp; &nbsp; tag untagged<br>";
                echo " &nbsp; &nbsp; &nbsp; &nbsp; configure bridge port $line_identifier pvid $vlan_pppoe_local <br>";
            }

            if ( $vlan_pppoe_local != $vlan_id)
            {
                echo " &nbsp; &nbsp; &nbsp; &nbsp; configure bridge port $line_identifier vlan-id $vlan_pppoe_local <br>";
                echo  "vlan-scope local <br>";
                echo  "network-vlan $vlan_id Qos Profile $pppoe_quos_profile<br>";

            }
            echo  "exit <br>";

            if ( $vlan_acs_local != '')
            {
                echo  "configure bridge port $line_identifier vlan-id $vlan_acs_local <br>";
                echo  "vlan-scope local <br>";
                echo  "network-vlan $vlan_acs <br>";
                echo " &nbsp; &nbsp; &nbsp; &nbsp; tag single-tagged<br>";
                echo " qos-profile name:$acs_qos_profile<br>";
                echo  "exit <br>";
            }

            if ($tv_service == "yes" )
            {
                echo " &nbsp; &nbsp; &nbsp; &nbsp; configure bridge port $line_identifier vlan-id $vlan_iptv_local<br>";
                if ($vlan_iptv != $vlan_iptv_local)
                {
                    echo  "vlan-scope local <br>";
                    echo  "network-vlan $vlan_iptv <br>";
                }
                echo " &nbsp; &nbsp; &nbsp; &nbsp; tag single-tagged<br>";
                echo " &nbsp; &nbsp; &nbsp; &nbsp; configure igmp channel vlan:$line_identifier:$vlan_iptv_local <br>";
                echo " &nbsp; &nbsp; &nbsp; &nbsp; max-num-group 10 mcast-vlan-id $vlan_iptv_local <br>";
                echo  "exit <br>";
            }
            else
            {
                echo " &nbsp; &nbsp; &nbsp; &nbsp; configure bridge port $line_identifier no vlan-id $vlan_iptv_local<br>";
            }
            $this->wait_exit ('','','');

        }
        //############################################
        //# VLAN set UP
        //############################################
        public  function vlan_setup_new ( $vlan_id, $vlan_name)
        {

            $this->gateway->write( " configure vlan id $vlan_id mode residential-bridge \r\n");
            $this->gateway->write( "name $vlan_name\r\n");
            $this->gateway->write( "no new-secure-fwd \r\n");
            $this->gateway->write( "circuit-id-dhcp physical-id\r\n");
            $this->gateway->write( "remote-id-dhcp customer-id\r\n");
            $this->gateway->write( "dhcp-opt-82 \r\n");
            $this->gateway->write( "pppoe-linerate addactuallinerate\r\n");
            $this->gateway->write( "circuit-id-pppoe physical-id\r\n");
            $this->gateway->write( "remote-id-pppoe customer-id\r\n");
            $this->gateway->write( "pppoe-linerate addactuallinerate\r\n");
            $this->gateway->write( "pppoe-relay-tag configurable\r\n");
            $this->gateway->write( "dhcp-linerate addactuallinerate\r\n");
            $this->gateway->write( "in-qos-prof-name name:2Q_HSI-VOIP\r\n"); #GPON  braucht das
            $this->gateway->write( "info \r\n");
            $this->gateway->write( "exit\r\n");


        } # end VLAN setup

        //########################################################################################################################
        //default_dpbo
        //########################################################################################################################


        public function default_dpbo ( $Nr)
        {

            $this->gateway->write( "configure xdsl dpbo-profile $Nr name DPBO".$Nr."_Defaul80_MUS_1120\r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr es-elect-length 800\r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr es-cable-model-a 14 es-cable-model-b 234 \r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr es-cable-model-c 8 \r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr min-usable-signal -1120 \r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr min-frequency 256 max-frequency 2208 rs-elect-length 665\r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 1 psd 900\r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 2 frequency 93 psd 900\r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 3 frequency 209 psd 620\r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 4 frequency 254 psd 485\r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 5 frequency 254 psd 365\r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 6 frequency 1104 psd 365\r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 7 frequency 1622 psd 465\r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 8 frequency 2208 psd 478\r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 9 frequency 2500 psd 594\r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 10 frequency 3002 psd 800\r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 11 frequency 3132 psd 950\r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 12 frequency 30000 psd 950\r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 13 no frequency no psd\r\n");
            $this->gateway->write( "configure xdsl dpbo-profile $Nr active\r\n");


        }

        //########################################################################################################################
        //wait_exit and print if collor to be made
        //########################################################################################################################

        public function wait_exit (string $color,string $first,string $last)
        {

            //if ($first == '') $first = ">#";
            if ($last == '') $last = "#exitphp";
            //echo " first = $first last = $last <br>";

            $this->gateway->write("$last\r\n");
            if ($first != '')
            {
                $this->gateway->waitUntilStringIsFound($first);
                $this->gateway->clearLastReadData(); // reset buffer, so it only returns the output of
            }
            $this->gateway->waitUntilStringIsFound($last);

            if ($color == 'true')
            {
                $buffer = $this->gateway->getLastReadData();
                //var_dump ($buffer);
                //exit;
                $buffer = str_replace("Error", "<span style=\"color: red;\"> Error </span>", $buffer);
                $buffer = str_replace("#begBLAU", "<span style=\"color: blue;\">", $buffer);
                $buffer = str_replace("#endBLAU", "</span>", $buffer);
                
                $buffer = str_replace("invalid token", "<span style=\"color: red;\"> invalid token</span> ", $buffer);
                $buffer = str_replace("1D", "", $buffer); # -[\[ 
                        $buffer = str_replace('[\[ [', "", $buffer);
                        $buffer = str_replace('[', "", $buffer);
                        $buffer = str_replace(']', "", $buffer);
                        #$buffer = str_replace('/', "", $buffer);
                        $buffer = str_replace('|', "", $buffer);

                        $buffer = str_replace(" up ", "<span style=\"color: green;\"> up </span>",$buffer);
                        $buffer = str_replace(" down ", "<span style=\"color: red;\"> down </span>",$buffer);
                        echo " <pre>$buffer</pre>";

            }

        }

        //########################################################################################################################
        //dslam_name_extract
        //########################################################################################################################

        public function dslam_name_extract (string $name,Location $location)
        {
            $dslamNameParted = null;

            if (1 === preg_match('/^((([^_]+)_.*)_R\d{2})\/.*/', $name, $dslamNameParted))
            {
                $this->dslam_name = $dslamNameParted[2];
                $this->dslam_name_ext = $dslamNameParted[1];
                $this->vlan_name1 = $dslamNameParted[3];
            }
            else
            {
                $this->dslam_name_ext     = substr($name, 0, 14);
                $this->dslam_name_ext     = preg_replace('/[^0-9a-z_]/i', '_', $this->dslam_name_ext);
                $this->dslam_name         = substr($name, 0, 10);
                $this->vlan_name1         = substr($name, 0, 3);
            }
            $this->Backup = $this->dslam_name_ext.".tar";
            $this->admin_save = "admin save";
            if ($location->getProcessor() == "NRNT-A" or $location->getProcessor() == "NATN-A") $this->admin_save = "admin software-mngt shub database save";
            if ($location->getProcessor() == "RANT-A" )  $this->admin_save = "";
            if ($location->getProcessor() == "SRNT-J" )  $this->admin_save = "";

            $this->tftpserver = \Wisotel\Configuration\Configuration::get('tftpServer');
            $this->Backup_id    = "admin software-mngt database upload actual-active:".$this->tftpserver."dm_complete_$this->dslam_name_ext".".tar";
        }

        //########################################################################################################################
        //adminsave
        //########################################################################################################################

        public function adminsave (Location $location)
        {
            $this->dslam_name_extract(($location->getName()),$location);
                                      $this->gateway->write(" exit all\r\n");
                                      $this->gateway->write("$this->admin_save \r\n");
        }

        //########################################################################################################################
        //backup_dslam
        //########################################################################################################################

        public function backup_dslam ( $location)
        {
            $this->dslam_name_extract(($location->getName()),$location);
                                      $this->gateway->write(" exit all\r\n");
                                      $this->gateway->write("$this->admin_save \r\n");

        }


        //########################################################################################################################
        //DSLAM_Kunden_einrichten
        //########################################################################################################################

        public function DSLAM_Kunden_einrichten (Customer $customer, Location $location, Card $card, MainProduct $customerService,$backup_JN)
        {
            // $customer->getEffectiveVlanProfiles();
            /*foreach ($customer->getEffectiveVlanProfiles() as $vlanProfile) {
                $pvid=$vlanProfile->getPvid();
                
                foreach ($vlanProfile->getVlans() as $vlan) {
                    $vlan_local=$vlan->getVlanId();
                    $vlan_network=$vlan->getNetworkVlan();
                    $Tag=$vlan->getTag();
                    //$unicastMac->getUnicastMac();
                    $unicastMac = 16;
                    DSLAM_Bridge_OneVlan ( $customer->getLineIdentifier(), $vlan_local, $vlan_network, $Tag,$customerService->getGponUpload(),$unicastMac,$card->getCardType()->getName(),$pvid);
                }
            }
            */
            if (substr($customer->getLineIdentifier(), -1) == "/" or substr($customer->getLineIdentifier(), -3) == "/00") {
                $this->gateway->write("## Error Line_identifier ".$customer->getLineIdentifier()." \r\n");
            } else {
                $Transfermode   = "ptm";
                if ($card->getCardType()->getName() == "ADSL") $Transfermode   = "atm";
                $Vectoring      = "";
                $NoVectoring    =   "";
                //echo ("Vectoring customer = ".$customer->getVectoring()."  Location ".$location->getVectoring()." \r\n" );
                if ($customer->getVectoring() == "Ja"  and $location->getVectoring() == true)
                {
                    $Vectoring      = "vect-profile 1";
                    $NoVectoring    = "no vect-profile";
                }
                if ($customer->getVectoring() == "Nein" and $location->getVectoring() == true)
                {
                    $Vectoring      = "no vect-profile";
                    $NoVectoring    = "no vect-profile";
                }
                $atm            ="";
                /*#echo "spectrum = $Spectrumprofile";
                if ($customer->getSpectrumprofile() == 2) {
                    $Transfermode     = "atm";
                    $atm = ":1:32";
                }
                */
                $wait = 'last';
                switch ($card->getCardType()->getName()) {
                    case 'ADSL':
                    case 'VDSL':
                    case 'SDSL':
                    case 'VDSL-BLV':
                    case 'VDSL-SLV':
                        $this->gateway->write("configure xdsl line ".$customer->getLineIdentifier()." service-profile ".$customerService->getXdslServiceProfile()."   spectrum-profile ".$customer->getSpectrumprofile()." $Vectoring transfer-mode $Transfermode dpbo-profile ".$customer->getDPBO()." admin-up \r\n");
                        $this->gateway->write( "exit all \r\n");

                        if ( $Transfermode == "atm" )
                        {
                            $this->gateway->write( "configure atm pvc ".$customer->getLineIdentifier()."$atm \r\n");
                        }

                        $this->gateway->write( "configure interface port xdsl-line:".$customer->getLineIdentifier()." user ".$customer->getClientId()." admin-up \r\n");
                        //$this->DSLAM_Bridge ($customer->getLineIdentifier(),$customer->getVlanId(),$location->getVlanPppoeLocal(),$location->getVlanPppoe(),$location->getVlanAcsLocal(),$location->getVlanAcs(),$location->getVlanIptvLocal(),$location->getVlanIptv(),$customer->getTag(),$customer->getTvService(),$atm,$location->getAcsQosProfile(),$customerService->getEthernetUpDown());
                        $this->DSLAM_Bridge_all($customer,$card, $customerService,$location);
                        break;

                    case 'ETHERNET':
                        $Transfermode   = ""; // only need in xdsl
                        $this->gateway->write("configure ethernet line ".$customer->getLineIdentifier()." mau 1 type 1000basebx10d power up \r\n");
                        $this->gateway->write("exit \r\n");
                        $this->gateway->write("admin-up \r\n");
                        //$this->DSLAM_Bridge ($customer->getLineIdentifier(),$customer->getVlanId(),$location->getVlanPppoeLocal(),$vlan_pppoe,$location->getVlanAcsLocal(),$location->getVlanAcs(),$location->getVlanIptvLocal(),$location->getVlanIptv(),$customer->getTag(),$customer->getTvService(),$atm,$location->getAcsQosProfile(),$customerService->getEthernetUpDown());
                        $this->DSLAM_Bridge_all($customer,$card, $customerService->getEthernetUpDown(),$location);

                        break;

                    case 'GPON':
                        $Transfermode   = "/1/1"; // only need in xdsl
                        $firmware_version = $customer->getFirmwareVersion();
                        if ($firmware_version == '') $firmware_version = $location->getDefaultOntFirmwareRelease();
                        if ($firmware_version  != '')
                        {
                            //echo " Line = ".substr($customer->getDtagLine(), 0, 8);
                            if (substr($customer->getDtagLine(), 0, 8) == "5A594B4C") {
                                $serialNumber = "ZYKL:".substr($customer->getDtagLine(), 8);
                                
                            } else {
                                $serialNumber = 'ALCL:'.$customer->getDtagLine();

                                if (false !== strpos($customer->getDtagLine(), ':')) {
                                    $serialNumber = $customer->getDtagLine();
                                }
                            }
                            

                            $this->gateway->write("configure equipment ont interface ".$customer->getLineIdentifier()." admin-state down\r\n");
                            $this->gateway->write("configure equipment ont slot ".$customer->getLineIdentifier()."/1 admin-state down\r\n");
                            $this->gateway->write("configure equipment ont no interface ".$customer->getLineIdentifier()."\r\n");
                            $this->gateway->write("######\r\n");
                            $this->gateway->write("configure equipment ont interface ".$customer->getLineIdentifier()." sw-ver-pland $firmware_version\r\n");
                            $this->gateway->write("configure equipment ont interface ".$customer->getLineIdentifier()." sernum ".$serialNumber." sw-dnload-version disabled desc1 ".$customer->getClientId()."\r\n");
                            $this->gateway->write("configure equipment ont interface ".$customer->getLineIdentifier()." admin-state up\r\n");
                            $this->gateway->write("configure equipment ont slot ".$customer->getLineIdentifier()."/1 planned-card-type 10_100base plndnumdataports 1 plndnumvoiceports 0\r\n");
                            $this->gateway->write("configure interface port uni:".$customer->getLineIdentifier()."/1/1 user ".$customer->getClientId()." admin-up \r\n");
                            $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()."/1/1 queue 0 priority 6 weight 34 queue-profile name:NGLT_Default shaper-profile name:".$customerService->getGponDownload()."\r\n");
                            $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()."/1/1 upstream-queue 0 weight 1 bandwidth-profile name:".$customerService->getGponUpload()."\r\n");
                            $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()."/1/1 upstream-queue 6 weight 1 bandwidth-profile name:CIR_1M\r\n");
                            $atm = "";
                            //$this->DSLAM_Bridge ("".$customer->getLineIdentifier()."/1/1",$customer->getVlanId(),$location->getVlanPppoeLocal(),$vlan_pppoe,$location->getVlanAcsLocal(),$location->getVlanAcs(),$location->getVlanIptvLocal(),$location->getVlanIptv(),$customer->getTag(),$customer->getTvService(),$atm,$location->getAcsQosProfile(),$customerService->getEthernetUpDown());
                            $this->DSLAM_Bridge_all($customer,$card, $customerService->getEthernetUpDown(),$location);

                        }

                        break;

                    case 'MDU-VDSL':
                        $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // vdsl2
                        //$this->gateway->write("configure xdsl ont line ".$customer->getLineIdentifier()." service-profile ".$customerService->getXdslServiceProfile()."   spectrum-profile 1 admin-state up \r\n");
                        $this->gateway->write("configure xdsl ont line ".$customer->getLineIdentifier()." service-profile ".$customerService->getXdslServiceProfile()."   spectrum-profile 0 admin-state up \r\n");

                        $this->gateway->write( "exit all \r\n");
                        $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." queue 0 shaper-profile name:".$customerService->getGponDownload()."\r\n");

                        $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." upstream-queue 0 weight 1 bandwidth-profile name:".$customerService->getGponUpload()."\r\n");

                        $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." upstream-queue 6 weight 1 bandwidth-profile name:CIR_1M\r\n");


                        $this->gateway->write( "configure interface port uni:".$customer->getLineIdentifier()." user ".$customer->getClientId()." admin-up \r\n");
                        //$this->DSLAM_Bridge ($customer->getLineIdentifier(),$customer->getVlanId(),$location->getVlanPppoeLocal(),$location->getVlanPppoe(),$location->getVlanAcsLocal(),$location->getVlanAcs(),$location->getVlanIptvLocal(),$location->getVlanIptv(),$customer->getTag(),$customer->getTvService(),$atm,$location->getAcsQosProfile(),$customerService->getEthernetUpDown());
                        $this->DSLAM_Bridge_all($customer,$card, $customerService->getEthernetUpDown(),$location);
                        break;


                    case 'MDU-ETHERNET':
                        $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // ethernet
                        $Transfermode   = ""; // only need in xdsl
                        $this->gateway->write("configure ethernet ont ".$customer->getLineIdentifier()." admin-state up \r\n");

                        //$this->gateway->write("admin-up \r\n");
                        //$this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." queue 0 priority 6 weight 34 queue-profile name:NGLT_Default shaper-profile name:".$customerService->getGponDownload()."\r\n");
                        //$this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." upstream-queue 0 weight 1 bandwidth-profile name:".$customerService->getGponUpload()."\r\n");
                        $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." queue 0 shaper-profile name:".$customerService->getGponDownload()."\r\n");
                        $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." upstream-queue 0 weight 1 bandwidth-profile name:".$customerService->getGponUpload()."\r\n");
                        $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." upstream-queue 6 weight 1 bandwidth-profile name:CIR_1M\r\n");

                        //$this->DSLAM_Bridge ($customer->getLineIdentifier(),$customer->getVlanId(),$location->getVlanPppoeLocal(),$vlan_pppoe,$location->getVlanAcsLocal(),$location->getVlanAcs(),$location->getVlanIptvLocal(),$location->getVlanIptv(),$customer->getTag(),$customer->getTvService(),$atm,$location->getAcsQosProfile(),$customerService->getEthernetUpDown());
                        $this->DSLAM_Bridge_all($customer,$card, $customerService->getEthernetUpDown(),$location);
                        break;

                    case 'MDU-G.fast+VDSL':
                        $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // todo
                        $gfastSpectrumProfile = $customer->getSpectrumprofile();
                        $vdslSpectrumProfile = '';
                        if ($customer->getTechnicalData()->getSpectrumProfileByCardTypeName('VDSL') != '') $vdslSpectrumProfile = $customer->getTechnicalData()->getSpectrumProfileByCardTypeName('VDSL')->getProfile();
                        if ($vdslSpectrumProfile == '' and $gfastSpectrumProfile == '') {
                            $this->gateway->write("#Error kein VDSL und kein Gfast spectrum Profile \r\n");
                        } else {
                            if ($vdslSpectrumProfile != '' and $gfastSpectrumProfile != '') {
                                $this->gateway->write("configure xdsl line ".$customer->getLineIdentifier()." service-profile ".$customerService->getXdslServiceProfile()."   spectrum-profile $vdslSpectrumProfile $Vectoring transfer-mode $Transfermode  gf-dsrate-profile name:".$customerService->getGfastDownstreamRate()."   gf-usrate-profile name:".$customerService->getGfastUpstreamRate()." gf-spec-profile  $gfastSpectrumProfile  gf-rfi-profile 1 gf-tdd-profile 1 gf-upbo-profile 1 gf-snrm-profile 1 gf-fra-profile 1 gf-rtx-profile 1 gf-reinit-profile 1  gf-vect-profile 1 admin-up \r\n");

                            }                        
                            if ($vdslSpectrumProfile != '' and $gfastSpectrumProfile == '') {
                                $this->gateway->write("configure xdsl line ".$customer->getLineIdentifier()." service-profile ".$customerService->getXdslServiceProfile()."   spectrum-profile $vdslSpectrumProfile $Vectoring transfer-mode $Transfermode   admin-up \r\n");

                            }                        
                            if ($vdslSpectrumProfile == '' and $gfastSpectrumProfile != '') {
                                $this->gateway->write("configure xdsl line ".$customer->getLineIdentifier()." gf-dsrate-profile name:".$customerService->getGfastDownstreamRate()."   gf-usrate-profile name:".$customerService->getGfastUpstreamRate()." gf-spec-profile  $gfastSpectrumProfile  gf-rfi-profile 1 gf-tdd-profile 1 gf-upbo-profile 1 gf-snrm-profile 1 gf-fra-profile 1 gf-rtx-profile 1 gf-reinit-profile 1  gf-vect-profile 1 admin-up \r\n");

                            }                        
                            
                            $this->gateway->write( "configure interface port xdsl-line:".$customer->getLineIdentifier()." user ".$customer->getClientId()." admin-up \r\n");
                            $this->DSLAM_Bridge_all($customer,$card, $customerService,$location);
                            
                        }

                        /*
                        echo ("configure xdsl line ".$customer->getLineIdentifier()." service-profile ".$customerService->getXdslServiceProfile()."   spectrum-profile $vdslSpectrumProfile $Vectoring transfer-mode $Transfermode  admin-up <br>");

                        echo "gf-dsrate-profile = ".$customerService->getGponDownload()."   gf-usrate-profile: = ".$customerService->getGponUpload()."   plannedCardType = $plannedCardType   gfastSpectrumProfile = $gfastSpectrumProfile   vdslSpectrumProfile = $vdslSpectrumProfile <br><br>";
                        echo "Zeile 3683 im NantECommunicator <br><br>";
                        var_dump ($vdslSpectrumProfile);
                        exit;
                        */


                        break;
                }
            }

            if ($backup_JN = 'yes') {
                $this->dslam_name_extract(($location->getName()),$location);
                $this->gateway->write( "$this->admin_save \r\n");
                //var_dump ($this->Backup_id);
                //exit;
                $this->gateway->write("$this->Backup_id \r\n");
                //$this->gateway->write( "show software-mngt upload-download  \r\n");
            }

            $this->wait_exit ('true',"$wait",'');

            return $this->gateway->getLastReadData();
        }

        //########################################################################################################################
        //DSLAM_Kunden_Loeschen1  wird nach bestätigung ausgeführt
        //########################################################################################################################

        public function DSLAM_Kunden_Loeschen1 (Customer $customer, Location $location, Card $card, MainProduct $customerService)
        {

            if (substr($customer->getLineIdentifier(), -1) == "/" or substr($customer->getLineIdentifier(), -3) == "/00")
            {

                $this->gateway->write("## Error Line_identifier ".$customer->getLineIdentifier()." \r\n");
            }
            else
            {

                if (substr($customer->getLineIdentifier(), -1) == "/" or substr($customer->getLineIdentifier(), -3) == "/00")
                {

                    $this->gateway->write("// error Line_identifier ".$customer->getLineIdentifier()." \r\n");
                }
                else
                {
                    $Transfermode   = "ptm";
                    if ($card->getCardType()->getName() == "ADSL") $Transfermode   = "atm";
                    $Vectoring      = "";
                    $NoVectoring    =   "";
                    if ($customer->getVectoring() == true  and $location->getVectoring() == true)
                    {
                        $Vectoring      = "vect-profile 1";
                        $NoVectoring    = "no vect-profile";
                    }
                    if ($customer->getVectoring() == false and $location->getVectoring() == true)
                    {
                        $Vectoring      = "no vect-profile";
                        $NoVectoring    = "no vect-profile";
                    }
                    /*
                    if ($customer->getSpectrumprofile() == 2) {
                        $Transfermode     = "atm";
                        $atm = ":1:32";
                    }
                    */
                    switch ($card->getCardType()->getName())
                    {

                        case 'ADSL':
                        case 'VDSL':
                        case 'SDSL':
                        case 'VDSL-BLV':
                        case 'VDSL-SLV':
                            $this->gateway->write("configure bridge no port ".$customer->getLineIdentifier()."$atm\r\n exit all \r\n");
                            $this->gateway->write("configure xdsl line ".$customer->getLineIdentifier()." no admin-up  no service-profile no dpbo-profile $NoVectoring\r\n exit all \r\n");
                            $this->gateway->write("configure interface port xdsl-line:".$customer->getLineIdentifier()." no admin-up  no user\r\n exit all \r\n");
                            echo '<hr> Kunde geloescht<hr>';
                            break;

                        case 'ETHERNET':
                            $this->gateway->write("configure ethernet line ".$customer->getLineIdentifier()." no admin-up \r\n");
                            $this->gateway->write("configure bridge no port ".$customer->getLineIdentifier()."\r\n");
                            $this->gateway->write("exit \r\n");
                            echo '<hr> Kunde geloescht<hr>';
                            break;

                        case 'GPON':

                            $this->gateway->write("configure equipment ont interface ".$customer->getLineIdentifier()." admin-state down\r\n");
                            $this->gateway->write("configure equipment ont slot ".$customer->getLineIdentifier()."/1 admin-state down\r\n");
                            $this->gateway->write("configure equipment ont no interface ".$customer->getLineIdentifier()."\r\n");
                            /*$this->gateway->write("configure bridge no port ".$customer->getLineIdentifier()."/1/1\r\n");
                            $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()."/1/1 upstream-queue 0 no bandwidth-profile\r\n");
                            $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()."/1/1 queue 0 shaper-profile none\r\n");
                            $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()."/1/1 upstream-queue 6  no bandwidth-profile\r\n"); //shaper-profile
                            $this->gateway->write("configure bridge no port ".$customer->getLineIdentifier()."/1/1\r\n");
                            */
                            echo '<hr> Kunde geloescht<hr>';
                            break;

                        case 'MDU-VDSL':
                            $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // vdsl2
                            $this->gateway->write("configure bridge no port ".$customer->getLineIdentifier()."$atm\r\n exit all \r\n");
                            $this->gateway->write("configure xdsl ont line ".$customer->getLineIdentifier()." admin-state down no spectrum-profile no service-profile no dpbo-profile $NoVectoring\r\n exit all \r\n");
                            $this->gateway->write("configure interface port uni:".$customer->getLineIdentifier()." no admin-up  no user\r\n exit all \r\n");
                            $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." queue 0 shaper-profile none \r\n");
                            $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." upstream-queue 0 no bandwidth-profile \r\n");
                            $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." upstream-queue 6 no bandwidth-profile \r\n");

                            echo '<hr> Kunde geloescht<hr>';


                            break;

                        case 'MDU-ETHERNET':
                            $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // ethernet
                            $this->gateway->write("configure ethernet ont ".$customer->getLineIdentifier()." admin-state down \r\n");
                            $this->gateway->write("configure bridge no port ".$customer->getLineIdentifier()."\r\n");
                            $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." queue 0 shaper-profile none \r\n");
                            $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." upstream-queue 0 no bandwidth-profile \r\n");
                            $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." upstream-queue 6 no bandwidth-profile \r\n");

                            

                            $this->gateway->write("exit \r\n");
                            echo '<hr> Kunde geloescht<hr>';
                            break;

                        case 'MDU-G.fast+VDSL':
                            $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // todo
                            $this->gateway->write("configure bridge no port ".$customer->getLineIdentifier()."$atm\r\n exit all \r\n");
                            $this->gateway->write("configure xdsl line ".$customer->getLineIdentifier()." no admin-up  no service-profile no dpbo-profile $NoVectoring no gf-dsrate-profile no gf-usrate-profile no gf-spec-profile  no gf-rfi-profile no gf-tdd-profile no gf-upbo-profile no gf-snrm-profile no gf-fra-profile no gf-rtx-profile no gf-reinit-profile no  gf-vect-profile \r\n exit all \r\n");
                            $this->gateway->write("configure interface port xdsl-line:".$customer->getLineIdentifier()." no admin-up  no user\r\n exit all \r\n");
                            echo '<hr> Kunde geloescht<hr>';

                            echo '<hr> Kunde geloescht<hr>';
                            break;


                    }
                }
            }
            $this->wait_exit ('true','last','');
            return $this->gateway->getLastReadData();


        }

        //########################################################################################################################
        //DSLAM_Kunden_Bandbreite
        //########################################################################################################################

        public function DSLAM_Kunden_Bandbreite (Customer $customer, Location $location, Card $card, MainProduct $customerService)
        {

            if (substr($customer->getLineIdentifier(), -1) == "/" or substr($customer->getLineIdentifier(), -3) == "/00")
            {

                $this->gateway->write("## Error Line_identifier ".$customer->getLineIdentifier()." \r\n");
            }
            else
            {

                switch ($card->getCardType()->getName())
                {
                    case 'ADSL':
                    case 'VDSL':
                    case 'SDSL':
                    case 'VDSL-BLV':
                    case 'VDSL-SLV':
                    $this->gateway->write("configure xdsl line ".$customer->getLineIdentifier()." service-profile ".$customerService->getXdslServiceProfile()."  \r\n");
                    $this->gateway->write( "exit all \r\n");

                        break;

                    case 'ETHERNET':
                        $this->gateway->write("configure bridge port ".$customer->getLineIdentifier()." no qos-profile \r\n");
                        $this->gateway->write("configure bridge port ".$customer->getLineIdentifier()." vlan-id ".$location->getVlanPppoeLocal()." no qos-profile \r\n");
                        $this->gateway->write("configure bridge port ".$customer->getLineIdentifier()." vlan-id ".$location->getVlanPppoeLocal()." qos-profile name:".$customerService->getEthernetUpDown()." \r\n");

                        break;

                    case 'GPON':
                        $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()."/1/1 queue 0 priority 6 weight 34 queue-profile name:NGLT_Default shaper-profile name:".$customerService->getGponDownload()."\r\n");
                        $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()."/1/1 upstream-queue 0 weight 1 bandwidth-profile name:".$customerService->getGponUpload()."\r\n");


                        break;
                    case 'MDU-VDSL':
                        $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // vdsl2
                        $this->gateway->write("configure xdsl ont line ".$customer->getLineIdentifier()." service-profile ".$customerService->getXdslServiceProfile()."  \r\n");
                        $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." queue 0 shaper-profile name:".$customerService->getGponDownload()."\r\n");

                        $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." upstream-queue 0 weight 1 bandwidth-profile name:".$customerService->getGponUpload()."\r\n");

                        $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." upstream-queue 6 weight 1 bandwidth-profile name:CIR_1M\r\n");

                        $this->gateway->write( "exit all \r\n");


                        break;

                    case 'MDU-ETHERNET':
                        $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // ethernet
                        
                        $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." queue 0 priority 6 weight 34 queue-profile name:NGLT_Default shaper-profile name:".$customerService->getGponDownload()."\r\n");
                        $this->gateway->write("configure qos interface ".$customer->getLineIdentifier()." upstream-queue 0 weight 1 bandwidth-profile name:".$customerService->getGponUpload()."\r\n");

                        $this->gateway->write("configure bridge port ".$customer->getLineIdentifier()." vlan-id ".$location->getVlanPppoeLocal()." no qos-profile \r\n");
                        $this->gateway->write("configure bridge port ".$customer->getLineIdentifier()." vlan-id ".$location->getVlanPppoeLocal()." qos-profile name:".$customerService->getEthernetUpDown()." \r\n");

                        break;

                    case 'MDU-G.fast+VDSL':

                        $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // todo
                        $gfastSpectrumProfile = $customer->getSpectrumprofile();
                        $vdslSpectrumProfile = '';
                        if ($customer->getTechnicalData()->getSpectrumProfileByCardTypeName('VDSL') != '') $vdslSpectrumProfile = $customer->getTechnicalData()->getSpectrumProfileByCardTypeName('VDSL')->getProfile();
                        if ($vdslSpectrumProfile == '' and $gfastSpectrumProfile == '') {
                            $this->gateway->write("#Error kein VDSL und kein Gfast spectrum Profile \r\n");
                        } else {
                            if ($vdslSpectrumProfile != '' and $gfastSpectrumProfile != '') {
                                $this->gateway->write("configure xdsl line ".$customer->getLineIdentifier()." service-profile ".$customerService->getXdslServiceProfile()." gf-dsrate-profile name:".$customerService->getGponDownload()."   gf-usrate-profile name:".$customerService->getGponUpload()."  \r\n");
                                
                            }                        
                            if ($vdslSpectrumProfile != '' and $gfastSpectrumProfile == '') {
                                $this->gateway->write("configure xdsl line ".$customer->getLineIdentifier()." service-profile ".$customerService->getXdslServiceProfile()."   \r\n");
                                
                            }                        
                            if ($vdslSpectrumProfile == '' and $gfastSpectrumProfile != '') {
                                $this->gateway->write("configure xdsl line ".$customer->getLineIdentifier()." gf-dsrate-profile name:".$customerService->getGponDownload()."   gf-usrate-profile name:".$customerService->getGponUpload()."   \r\n");
                                
                            }                        
                            
                            
                        }


                        break;

                }
            }
            $this->wait_exit ('true','last','');
            return $this->gateway->getLastReadData();

        }

        //########################################################################################################################
        //DSLAM_Kunden_sperren
        //########################################################################################################################

        public function DSLAM_Kunden_sperren (Customer $customer, Location $location, Card $card, MainProduct $customerService)
        {

            if (substr($customer->getLineIdentifier(), -1) == "/" or substr($customer->getLineIdentifier(), -3) == "/00")
            {

                $this->gateway->write("## Error Line_identifier ".$customer->getLineIdentifier()." \r\n");
            }
            else
            {

                switch ($card->getCardType()->getName())
                {
                    case 'ADSL':
                    case 'VDSL':
                    case 'SDSL':
                    case 'VDSL-BLV':
                    case 'VDSL-SLV':
                    $this->gateway->write("configure xdsl line ".$customer->getLineIdentifier()." no admin-up \r\n");
                    $this->gateway->write( "exit all \r\n");

                        break;

                    case 'ETHERNET':




                        $this->gateway->write("configure ethernet line ".$customer->getLineIdentifier()." no admin-up \r\n");



                        break;

                    case 'GPON':

                    $this->gateway->write("configure equipment ont interface ".$customer->getLineIdentifier()." admin-state down\r\n");
                    $this->gateway->write("configure equipment ont slot ".$customer->getLineIdentifier()."/1  admin-state down\r\n");

                        break;
                    case 'MDU-VDSL':
                        $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // vdsl2
                        $this->gateway->write("configure xdsl ont line ".$customer->getLineIdentifier()." admin-state down \r\n");


                        break;

                    case 'MDU-ETHERNET':
                        $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // ethernet
                        $this->gateway->write("configure ethernet ont ".$customer->getLineIdentifier()." admin-state down \r\n");

                        break;

                    case 'MDU-G.fast+VDSL':
                        $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // todo
                        $this->gateway->write("configure xdsl line ".$customer->getLineIdentifier()." no admin-up \r\n");
                        $this->gateway->write( "exit all \r\n");

                        break;

                }
            }
            $this->wait_exit ('true','last','');
            return $this->gateway->getLastReadData();

        }

        //########################################################################################################################
        //DSLAM_Kunden_freischalten
        //########################################################################################################################

        public function DSLAM_Kunden_freischalten (Customer $customer, Location $location, Card $card, MainProduct $customerService)
        {

            if (substr($customer->getLineIdentifier(), -1) == "/" or substr($customer->getLineIdentifier(), -3) == "/00")
            {

                $this->gateway->write("## Error Line_identifier ".$customer->getLineIdentifier()." \r\n");
            }
            else
            {

                switch ($card->getCardType()->getName())
                {
                    case 'ADSL':
                    case 'VDSL':
                    case 'SDSL':
                    case 'VDSL-BLV':
                    case 'VDSL-SLV':
                    $this->gateway->write("configure xdsl line ".$customer->getLineIdentifier()." admin-up \r\n");
                    $this->gateway->write( "exit all \r\n");

                        break;

                    case 'ETHERNET':
                        $this->gateway->write("configure ethernet line ".$customer->getLineIdentifier()." admin-up \r\n");
                        break;

                    case 'GPON':
                    $this->gateway->write("configure equipment ont interface ".$customer->getLineIdentifier()." admin-state up\r\n");
                    $this->gateway->write("configure equipment ont slot ".$customer->getLineIdentifier()."/1  admin-state up\r\n");


                        break;
                    case 'MDU-VDSL':
                        $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // vdsl2
                        $this->gateway->write("configure xdsl ont line ".$customer->getLineIdentifier()." admin-state up \r\n");


                        break;

                    case 'MDU-ETHERNET':
                        $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // ethernet
                        $this->gateway->write("configure ethernet ont ".$customer->getLineIdentifier()." admin-state up \r\n");

                        break;

                    case 'MDU-G.fast+VDSL':
                        $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // todo
                        $this->gateway->write("configure xdsl line ".$customer->getLineIdentifier()." admin-up \r\n");
                        $this->gateway->write( "exit all \r\n");

                        break;

                }
            }
            $this->wait_exit ('true','last','');
            return $this->gateway->getLastReadData();


        }

        //########################################################################################################################
        //DSLAM_Port_Reset
        //########################################################################################################################

        public function DSLAM_Port_Reset (Customer $customer, Location $location, Card $card, MainProduct $customerService)
        {
            if (substr($customer->getLineIdentifier(), -1) == "/" or substr($customer->getLineIdentifier(), -3) == "/00")
            {

                $this->gateway->write("## Error Line_identifier ".$customer->getLineIdentifier()." \r\n");
            }
            else
            {

                switch ($card->getCardType()->getName())
                {
                    case 'ADSL':
                    case 'VDSL':
                    case 'SDSL':
                    case 'VDSL-BLV':
                    case 'VDSL-SLV':
                    $this->gateway->write("configure xdsl line ".$customer->getLineIdentifier()." no admin-up \r\n");
                    $this->gateway->write("configure xdsl line ".$customer->getLineIdentifier()." admin-up \r\n");
                    $this->gateway->write( "exit all \r\n");

                        break;

                    case 'ETHERNET':
                        $this->gateway->write("configure ethernet line ".$customer->getLineIdentifier()." mau 1 power down \r\n");
                        $this->gateway->write("power up \r\n");
                        break;

                    case 'GPON':
                    $this->gateway->write("configure equipment ont interface ".$customer->getLineIdentifier()." admin-state down\r\n");
                    $this->gateway->write("configure equipment ont slot ".$customer->getLineIdentifier()."/1  admin-state down\r\n");
                    $this->gateway->write("configure equipment ont interface ".$customer->getLineIdentifier()." admin-state up\r\n");
                    $this->gateway->write("configure equipment ont slot ".$customer->getLineIdentifier()."/1  admin-state up\r\n");


                        break;
                    case 'MDU-VDSL':
                        $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // vdsl2
                        $this->gateway->write("configure xdsl ont line ".$customer->getLineIdentifier()." admin-state down \r\n");
                        $this->gateway->write("configure xdsl ont line ".$customer->getLineIdentifier()." admin-state up \r\n");


                        break;

                    case 'MDU-ETHERNET':
                        $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // ethernet
                        $this->gateway->write("configure ethernet ont ".$customer->getLineIdentifier()." admin-state down \r\n");
                        $this->gateway->write("configure ethernet ont ".$customer->getLineIdentifier()." admin-state up \r\n");
                        break;

                    case 'MDU-G.fast+VDSL':
                        $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // todo
                        $this->gateway->write("configure xdsl line ".$customer->getLineIdentifier()." no admin-up \r\n");
                        $this->gateway->write("configure xdsl line ".$customer->getLineIdentifier()." admin-up \r\n");

                        $this->gateway->write( "exit all \r\n");

                        break;

                }
            }

            $this->wait_exit ('true','last','');
            return $this->gateway->getLastReadData();

        }

        //########################################################################################################################
        //DSLAM_Kunden_Status_maximal
        //########################################################################################################################

        public function DSLAM_Kunden_Status_maximal(Customer $customer, Location $location, Card $card, MainProduct $customerService)
        {
            //$this->detail = " detail";
            $Antwort = $this->DSLAM_Kunden_Status_gen($customer, $location, $card, $customerService,true,"detail");
            echo sprintf('<pre>%s</pre>', $Antwort);
            $this->wait_exit ('true','login','');

            return $this->gateway->getLastReadData();
        }

        //########################################################################################################################
        //DSLAM_Kunden_Config
        //########################################################################################################################

        public function DSLAM_Kunden_Config(Customer $customer, Location $location, Card $card, MainProduct $customerService)
        {
            //echo " Prozessor = ".$location->getProcessor()."<br>";
            //$this->DSLAM_Status = false;
            $Antwort = $this->DSLAM_Kunden_Status_gen($customer, $location, $card, $customerService,false," detail");
            //echo sprintf('<pre>%s</pre>', $Antwort);
            $this->wait_exit ('true','login','');

            return $this->gateway->getLastReadData();
        }

        //########################################################################################################################
        //DSLAM_Kunden_Status
        //########################################################################################################################

        public function DSLAM_Kunden_Status(Customer $customer, Location $location, Card $card, MainProduct $customerService)
        {
            //$this->DSLAM_Status = true;
            $Antwort = $this->DSLAM_Kunden_Status_gen($customer, $location, $card, $customerService,true,"");
            //echo sprintf('<pre>%s</pre>', $Antwort);
            $this->wait_exit ('true','login','');

            return $this->gateway->getLastReadData();
        }

        //########################################################################################################################
        //DSLAM_Kunden_Status_generic
        //########################################################################################################################

        public function DSLAM_Kunden_Status_gen($customer, $location,  $card,  $customerService,bool $DSLAM_Status,string $detail)
        {
            /*  $lsaId = $customer->getLsaKvzPartitionId();
                $customerId = $customer->getId();
                $locationId = $location->getId();
                $lineIdentifier = $customer->getLineIdentifier();
                $cardType = $card->getCardType()->getId();
            */
            //var_dump($cardType);
            // exit;
            //$this->VarInit ($customer,  $location,  $card, $customerService);
            // echo "Card Name $card->getCardType()->getName() <br>";

            $atm            ="";
            #echo "spectrum = $Spectrumprofile";
            /*if ($customer->getSpectrumprofile() == "2") {
                $Transfermode     = "atm";
                $atm = ":1:32";
            }
            */
            // Beispiel
            //$this->DSLAM_Bridge($this->gateway, $customer->getLineIdentifier(), $vlan_id, $vlan_pppoe_local, $vlan_pppoe, $vlan_acs_local, $vlan_acs, $vlan_iptv_local, $vlan_iptv, $tag, $tv_service, $atm, $acs_qos_profile);
            if (substr($customer->getLineIdentifier(), -1) == "/" or substr($customer->getLineIdentifier(), -3) == "/00")
            {

                $this->gateway->write("## Error Line_identifier ".$customer->getLineIdentifier()." \r\n");
            }
            else
            {
                switch ($card->getCardType()->getName())
                {
                    case 'ADSL':
                    case 'VDSL':
                    case 'SDSL':
                    case 'VDSL-BLV':
                    case 'VDSL-SLV':

                        $this->gateway->write("show xdsl operational-data line  ".$customer->getLineIdentifier()." $detail \r\n");
                        if ( $DSLAM_Status  == false)
                        {
                            $this->gateway->write("info configure xdsl service-profile ".$customerService->getXdslServiceProfile()." \r\n");
                            $this->gateway->write(" info configure xdsl line  ".$customer->getLineIdentifier()." \r\n");
                            $this->gateway->write( " info configure bridge port  ".$customer->getLineIdentifier()."$atm \r\n");
                            $this->gateway->write( "show vlan bridge-port-fdb  ".$customer->getLineIdentifier()."$atm \r\n");
                            if ($this->tv_service == "yes" ) $this->gateway->write("info configure igmp channel vlan: ".$customer->getLineIdentifier().":$customer->getVlanIptvLocal \r\n");
                        }

                        break;
                    case 'ETHERNET':

                        $sfp = '/';

                        if ($location->getProcessor() == 'NANT-E' or $location->getProcessor() == 'FANT-F')
                        {
                            $sfp = ':sfp:';
                        }

                        $PolicerD = "D_".$customerService->getEthernetUpDown();
                        $PolicerU = "U_".$customerService->getEthernetUpDown();
                        $PolicerD = substr($PolicerD, 0, 10);
                        $PolicerU = substr($PolicerU, 0, 10);
                        //echo " line ",$customer->getLineIdentifier()," <br>";
                        $this->gateway->write("show interface port ethernet-line:".$customer->getLineIdentifier()." $detail \r\n");

                        $Line = substr($customer->getLineIdentifier(), 0, 6);

                        if (($tmp = substr($Line, -1)) == "/")
                        {
                            $Line = substr($customer->getLineIdentifier(), 0, 5);
                        }

                        if (($tmp = substr($customer->getLineIdentifier(), -2, 1)) != "/")
                        {
                            $Line1 = substr($customer->getLineIdentifier(), -2);
                        }
                        else
                        {
                            $Line1 = substr($customer->getLineIdentifier(), -1);
                        }

                        if ($DSLAM_Status == false)
                        {
                            $down_red = 'so nicht';
                            $showsfp = "show equipment diagnostics sfp lt:$Line$sfp$Line1 detail\r\n";

                            $this->gateway->write($showsfp);
                            $this->gateway->write("info configure ethernet line  ".$customer->getLineIdentifier()." \r\n");
                            $this->gateway->write("info configure interface port ethernet-line:".$customer->getLineIdentifier()."\r\n");
                            $this->gateway->write("info configure bridge port  ".$customer->getLineIdentifier()." \r\n");

                            if ($this->tv_service == "yes")
                            {
                                $this->gateway->write("info configure igmp channel vlan:".$customer->getLineIdentifier().":".$lacation->getVlanIptvLocal." \r\n");
                            }

                            $this->gateway->write("info configure qos profiles session ".$customerService->getEthernetUpDown()." \r\n");

                            if ('WiSoTEL' === \Wisotel\Configuration\Configuration::get('companyName'))
                            {
                                $this->gateway->write ("info configure qos profiles policer $PolicerD flat \r\n");
                                $this->gateway->write ("info configure qos profiles policer $PolicerU flat \r\n");
                            }

                            $this->gateway->write( "show vlan bridge-port-fdb  ".$customer->getLineIdentifier()." \r\n");
                            
                            $this->gateway->write( "show trap \r\n");
                        }



                        break;

                    case 'GPON':
                        $line_identifierG = $customer->getLineIdentifier()."/1/1";
                        $display_ende = true;
                        $this->gateway->write("show interface port ont:".$customer->getLineIdentifier()." \r\n");

                        if ($DSLAM_Status == false)
                        {
                            $this->gateway->write("show equipment ont interface   ".$customer->getLineIdentifier()." \r\n");
                            $this->gateway->write("show equipment ont optics  ".$customer->getLineIdentifier()."\r\n");
                            $this->gateway->write("info configure bridge port  ".$customer->getLineIdentifier()."/1/1 \r\n");
                            $this->gateway->write("info configure qos interface  ".$customer->getLineIdentifier()."/1/1 queue 0\r\n");
                            $this->gateway->write("info configure qos interface  ".$customer->getLineIdentifier()."/1/1 upstream-queue 0\r\n");
                            $this->gateway->write("show vlan bridge-port-fdb  ".$customer->getLineIdentifier()."/1/1 \r\n");
                            $this->gateway->write("show equipment ont sw-download  ".$customer->getLineIdentifier()."\r\n");

                            $anzahl = 7;
                            $Test = substr($customer->getLineIdentifier(), 6, 1);

                            if ($Test == "/")
                            {
                                $anzahl = 8;
                            }

                            $line_identifierE = substr($customer->getLineIdentifier(), 0, $anzahl);

                            $this->gateway->write("show pon sw-download $line_identifierE\r\n");
                        }


                        break;
                    case 'MDU-VDSL':
                        $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // vdsl2
                        $this->gateway->write("show interface port uni:".$customer->getLineIdentifier()." $detail \r\n");
                        $this->gateway->write("show xdsl ont operational-data near-end channel ".$customer->getLineIdentifier()." $detail \r\n");
                        $this->gateway->write("show xdsl ont operational-data far-end channel  ".$customer->getLineIdentifier()." $detail \r\n");

                        if ( $DSLAM_Status  == false)
                        {
                            $this->gateway->write("info configure xdsl ont service-profile ".$customerService->getXdslServiceProfile()." \r\n");
                            $this->gateway->write(" info configure xdsl ont line  ".$customer->getLineIdentifier()." \r\n");
                            $this->gateway->write( " info configure bridge port  ".$customer->getLineIdentifier()."$atm \r\n");
                            $this->gateway->write( "show vlan bridge-port-fdb  ".$customer->getLineIdentifier()."$atm \r\n");
                            if ($this->tv_service == "yes" ) $this->gateway->write("info configure igmp channel vlan: ".$customer->getLineIdentifier().":$customer->getVlanIptvLocal \r\n");
                        }


                        break;

                    case 'MDU-ETHERNET':
                        $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // ethernet
                        $this->gateway->write("show interface port uni:".$customer->getLineIdentifier()." $detail \r\n");
                        $this->gateway->write( " info configure bridge port  ".$customer->getLineIdentifier()."$atm \r\n");
                        $this->gateway->write( "show vlan bridge-port-fdb  ".$customer->getLineIdentifier()."$atm \r\n");
                        break;

                    case 'MDU-G.fast+VDSL':
                        $plannedCardType = $this->plannedCardTypeMapping[$card->getCardType()->getName()]; // todo
                        $this->gateway->write("show xdsl operational-data line  ".$customer->getLineIdentifier()." $detail \r\n");
                        if ( $DSLAM_Status  == false)
                        {
                            $this->gateway->write("info configure xdsl service-profile ".$customerService->getXdslServiceProfile()." \r\n");
                            $this->gateway->write(" info configure xdsl line  ".$customer->getLineIdentifier()." \r\n");
                            $this->gateway->write( " info configure bridge port  ".$customer->getLineIdentifier()."$atm \r\n");
                            $this->gateway->write( "show vlan bridge-port-fdb  ".$customer->getLineIdentifier()."$atm \r\n");
                            if ($this->tv_service == "yes" ) $this->gateway->write("info configure igmp channel vlan: ".$customer->getLineIdentifier().":$customer->getVlanIptvLocal \r\n");
                        }

                        break;

                }

            }
            //return var_dump ($this);
            //return 'done ';
            //#echo 'done ';
        }
        public function DSLAM_Vector ( Location $location, Card  $card)
        {



		    //echo " &nbsp;&nbsp;&nbsp;&nbsp; Vectoring am DSLAM eingeschalten = $Vectoring <br />";
			$this->gateway->write("info configure xdsl board flat\r\n");
			$this->gateway->write("show xdsl vp-board\r\n");
			$this->gateway->write(" exit all\r\n");
            $this->wait_exit ('true','login','');

            return $this->gateway->getLastReadData();

        }

        /**
         * Status RF-Overlay
         */
        public function statusRfOverlay(Customer $customer, Card $card)
        {
            $rfOverlaySignalStrength = $customer->getTechnicalData()->getRfOverlaySignalStrength();
            if ($rfOverlaySignalStrength == '') $rfOverlaySignalStrength = '+2.0';
            $lineIdentifier = $customer->getLineIdentifier();
            $card->getCardType()->getName();
            $do_work = true;
            switch ($card->getCardType()->getName()) {
                
                
                case 'ONT':
                    if (substr($lineIdentifier,-1,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-1);
                    if (substr($lineIdentifier,-2,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-2);

                    break;
        
                case 'GPON':
                    if (substr($lineIdentifier,-1,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-1);
                    if (substr($lineIdentifier,-2,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-2);
            
                    break;
                
                case 'MDU-VDSL':
                case 'MDU-ETHERNET':
                case 'MDU-Video':
                    if (substr($lineIdentifier,-4,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-4);
                    if (substr($lineIdentifier,-5,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-5);
                
                    break;
                
                default:
                    $do_work = false;
                    echo "<span style=\"color: red;\">RF-overlay nicht vorhanden bei Karte ->  ".$card->getCardType()->getName()." </span><br>";

                    break;
            }
            if ($do_work == true) {
                $this->DebugInit ();


                $this->gateway->write('#');
                $this->gateway->waitUntilStringIsFound('#');
                $Buffer = $this->gateway->getLastReadData();

                if ($this->$debug == true) echo " <pre>$Buffer</pre>";

                $this->gateway->clearLastReadData();

                $this->gateway->write("info configure equipment ont slot $lineIdentifier/3 \r\n ");
                $return = $this->WaitErrorsNew ("echo","#end","Error : instance does not exist","Failure","invalid token",'y','true');
                //echo "<br>Return Wait Value = $return <br><br>";

                switch ($return) {
                    case "Error : instance does not exist":
                            echo "<span style=\"color: red;\">RF-overlay nicht aktive am ont slot $lineIdentifier/3 </span><br>";


                        break;

                    case "#end":
                    /*
                        
                       */              
                           
                       $this->gateway->write("info configure uni ont video $lineIdentifier/3/1 \r\n");
                       $this->gateway->write("info configure ani ont video $lineIdentifier \r\n");


                        break;

                    case "invalid token":
                    case "xx":
                        echo (sprintf("<span style=\"color: red;\">Command Error Card ont slot %s type %s dataports %d voiceports 0 </span><br>",
                            $lineIdentifier,
                            $this->plannedCardTypeMapping[$card->getCardType()->getName()],
                            $card->getPortAmount()
                        ));

                        break;
                }

                /*
                $this->gateway->write("configure equipment ont slot $lineIdentifier/3 planned-card-type video plndnumdataports 1 plndnumvoiceports 0 \r\n");
                $this->gateway->write("configure uni ont video $lineIdentifier/3/1 admin-state unlocked \r\n");
                $this->gateway->write("configure ani ont video $lineIdentifier agc-offset $rfOverlaySignalStrength  \r\n");
                */
            
            } 
            $this->wait_exit ('true','','');
            return $this->gateway->getLastReadData();

            //echo 'done - activate rf-overlay';
        }
        /**
         * Activate RF-Overlay
         */
        public function activateRfOverlay(Customer $customer, Card $card)
        {
            $rfOverlaySignalStrength = $customer->getTechnicalData()->getRfOverlaySignalStrength();
            if ($rfOverlaySignalStrength == '') $rfOverlaySignalStrength = '+2.0';
            $lineIdentifier = $customer->getLineIdentifier();
            $card->getCardType()->getName();
            $do_work = true;
            switch ($card->getCardType()->getName()) {
                
                
                case 'ONT':
                    if (substr($lineIdentifier,-1,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-1);
                    if (substr($lineIdentifier,-2,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-2);

                    break;
        
                case 'GPON':
                    if (substr($lineIdentifier,-1,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-1);
                    if (substr($lineIdentifier,-2,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-2);
            
                    break;
                
                case 'MDU-VDSL':
                case 'MDU-ETHERNET':
                case 'MDU-Video':
                    if (substr($lineIdentifier,-4,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-4);
                    if (substr($lineIdentifier,-5,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-5);
                
                    break;
                
                default:
                    $do_work = false;
                    echo "<span style=\"color: red;\">RF-overlay nicht vorhanden bei Karte ->  ".$card->getCardType()->getName()." </span><br>";

                    break;
            }
            if ($do_work == true) {
            
                    $this->DebugInit ();


                    $this->gateway->write('#');
                    $this->gateway->waitUntilStringIsFound('#');
                    $Buffer = $this->gateway->getLastReadData();

                    if ($this->$debug == true) echo " <pre>$Buffer</pre>";

                    $this->gateway->clearLastReadData();

                    $this->gateway->write("info configure equipment ont slot $lineIdentifier/3 \r\n ");
                    $return = $this->WaitErrorsNew ("echo","#end","Error : instance does not exist","Failure","invalid token",'y','true');
                    //echo "<br>Return Wait Value = $return <br><br>";

                    switch ($return) {
                        case "Error : instance does not exist":
                                echo "<span style=\"color: red;\">Card wird eingerichtet om ont slot $lineIdentifier/3 </span><br>";
                                $this->gateway->write("configure equipment ont slot $lineIdentifier/3 planned-card-type video plndnumdataports 1 plndnumvoiceports 0 \r\n");
                                $this->gateway->write("configure uni ont video $lineIdentifier/3/1 admin-state unlocked \r\n");
                                $this->gateway->write("configure ani ont video $lineIdentifier agc-offset $rfOverlaySignalStrength  \r\n");


                            break;

                        case "#end":
                        /*
                            
                           */              
                               
                           $this->gateway->write("configure uni ont video $lineIdentifier/3/1 admin-state unlocked \r\n");
                           $this->gateway->write("configure ani ont video $lineIdentifier agc-offset $rfOverlaySignalStrength  \r\n");


                            break;

                        case "invalid token":
                        case "xx":
                            echo (sprintf("<span style=\"color: red;\">Command Error Card ont slot %s type %s dataports %d voiceports 0 </span><br>",
                                $lineIdentifier,
                                $this->plannedCardTypeMapping[$card->getCardType()->getName()],
                                $card->getPortAmount()
                            ));

                            break;
                    }

                    /*
                    $this->gateway->write("configure equipment ont slot $lineIdentifier/3 planned-card-type video plndnumdataports 1 plndnumvoiceports 0 \r\n");
                    $this->gateway->write("configure uni ont video $lineIdentifier/3/1 admin-state unlocked \r\n");
                    $this->gateway->write("configure ani ont video $lineIdentifier agc-offset $rfOverlaySignalStrength  \r\n");
                    */
                }
            $this->wait_exit ('true','','');
            return $this->gateway->getLastReadData();

            //echo 'done - activate rf-overlay';
        }

        /**
         * Deactivate RF-Overlay
         */
        public function deactivateRfOverlay(Customer $customer, Card $card)
        {
            $lineIdentifier = $customer->getLineIdentifier();
            $card->getCardType()->getName();
            $do_work = true;
            switch ($card->getCardType()->getName()) {
                
                
                case 'ONT':
                    if (substr($lineIdentifier,-1,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-1);
                    if (substr($lineIdentifier,-2,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-2);

                    break;
        
                case 'GPON':
                    if (substr($lineIdentifier,-1,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-1);
                    if (substr($lineIdentifier,-2,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-2);
            
                    break;
                
                case 'MDU-VDSL':
                case 'MDU-ETHERNET':
                case 'MDU-Video':
                    if (substr($lineIdentifier,-4,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-4);
                    if (substr($lineIdentifier,-5,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-5);
                
                    break;
                
                default:
                    $do_work = false;
                    echo "<span style=\"color: red;\">RF-overlay nicht vorhanden bei Karte ->  ".$card->getCardType()->getName()." </span><br>";

                    break;
            }
            if ($do_work == true) {

                $rfOverlaySignalStrength = $customer->getTechnicalData()->getRfOverlaySignalStrength();
                $this->gateway->write("configure equipment ont slot $lineIdentifier/3 admin-state down \r\n");
                $this->gateway->write("configure equipment ont no slot $lineIdentifier/3 \r\n");
                $this->wait_exit ('true','last','');
                return $this->gateway->getLastReadData();

                //echo 'done - deactivate rf-overlay';
            }
        }

        /**
         * Ajust Signal RF-Overlay
         */
        public function signalRfOverlay(Customer $customer, Card $card)
        {
            
            $lineIdentifier = $customer->getLineIdentifier();
            $rfOverlaySignalStrength = $customer->getTechnicalData()->getRfOverlaySignalStrength();
            if ($rfOverlaySignalStrength == '') $rfOverlaySignalStrength = '+2.0';
            $do_work = true;
            switch ($card->getCardType()->getName()) {
                
                
                case 'ONT':
                    if (substr($lineIdentifier,-1,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-1);
                    if (substr($lineIdentifier,-2,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-2);

                    break;
        
                case 'GPON':
                    if (substr($lineIdentifier,-1,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-1);
                    if (substr($lineIdentifier,-2,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-2);
            
                    break;
                
                case 'MDU-VDSL':
                case 'MDU-ETHERNET':
                case 'MDU-Video':
                    if (substr($lineIdentifier,-4,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-4);
                    if (substr($lineIdentifier,-5,1) == "/" ) $lineIdentifier = substr($lineIdentifier,0,strlen($lineIdentifier)-5);
                
                    break;
                
                default:
                    $do_work = false;
                    echo "<span style=\"color: red;\">RF-overlay nicht vorhanden bei Karte ->  ".$card->getCardType()->getName()." </span><br>";

                    break;
            }
            if ($do_work == true) {

                $this->gateway->write("configure ani ont video $lineIdentifier agc-offset $rfOverlaySignalStrength  \r\n");
            }
            $this->wait_exit ('true','last','');
            return $this->gateway->getLastReadData();

            //echo 'done - deactivate rf-overlay';
        }
}
