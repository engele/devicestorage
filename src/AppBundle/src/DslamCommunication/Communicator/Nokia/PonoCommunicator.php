<?php

/**
 * 
 */

namespace AppBundle\src\DslamCommunication\Communicator\Nokia;

use AppBundle\src\DslamCommunication\Communicator\AbstractCommunicator;
use AppBundle\src\DslamCommunication\Communicator\CommunicatorInterface;
use AppBundle\src\DslamCommunication\Communicator\GatewayInterace;
use AppBundle\src\DslamCommunication\Communicator\SshGateway;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Location\Card;
use AppBundle\Entity\Location\Location;
use AppBundle\src\DslamCommunication\SupportedDevice;
use AppBundle\Entity\Product\MainProduct;
use AppBundle\src\DslamCommunication\Exception\LoginFailedException;

/**
 * @SupportedDevice(vendor="ISKRATEL-under-constr", model="PONO")
 */
class PonoCommunicator extends AbstractCommunicator implements CommunicatorInterface
{

    public $location_id1;
    public $lsa_id;
    public $location_id;
    public $line_identifier;
    public $card_id;
    public $clientid;
    public $version;
    //if ($version != "") $clientid = $clientid."_".$version;
    public $vlan_id;
    public $vlan_ID_u;
    public $tag;
    public $ip_address;
    public $terminal_type;
    public $dslam_port;
    public $tv_service;
    public $DPBO;
    public $Service;
    public $Spectrumprofile;
    public $firmware_version;
    public $dtag_line;
    public $vectoring;
    public $pppoePassword;
    public $detail;
    public $DSLAM_Status;
    public $prozessor;
    public $produkt_index;
    public $gpon_download;
    public $gpon_upload;
    public $ethernet_up_down;
    public $dslam_name;
    public $dslam_name_ext;
    public $vlan_name1;
    public $kvz_name1;
    public $kvz_name2;
    public $kvz_name3;
    public $Default_DPBO1;
    public $esel_wert1;
    public $Default_DPBO2;
    public $esel_wert2;
    public $Default_DPBO3;
    public $esel_wert3;
    public $dns1; //= \Wisotel\Configuration\Configuration::get('dnsServer');
    //public $dns1 = $dns1['1'];
    public $tftpserver;// = \Wisotel\Configuration\Configuration::get('tftpServer');
    public $admin_save = "";
    public $Backup;
    public $Backup_id;
    public $debug;

    /**
     * Maps some aliases to methods to be used with getMethodByAlias()
     *
     * @var array
     */
    protected $aliasMap = [
        'DSLAM_Kunden_Status' => 'DSLAM_Kunden_Status',
    ];

    /**
     * Holds current login stage
     * 
     * @var boolean
     */
    protected $isLoggedIn = false;

    /**
     * Mappes card-types to planned-card-type (used to provision/deprovision MDUs at OLT)
     */
    protected $plannedCardTypeMapping = [
        'ADSL' => 'todo',
        'VDSL' => 'vdsl2',
        'SDSL' => 'todo',
        'GPON' => 'todo',
        'ETHERNET' => 'ethernet',
        'Mobile' => 'todo',
        'VDSL-BLV' => 'todo',
        'VDSL-SLV' => 'todo',
        'MDU-VDSL' => 'vdsl2',
        'MDU-ETHERNET' => 'ethernet',
        'MDU-G.fast+VDSL' => 'MDU-G.fast+VDSL',
        'dependent-MDU' => 'GPON-MDU',
    ];

    //############################################
    //# DebugInit
    //############################################
    public function DebugInit()
    {
        $this->$debug = true;
    }
    
    //############################################
    // print_color
    //############################################
    public function print_color($buffer,$color) {
        if ($color == 'true')
        {
            
            //var_dump ($buffer);
            //exit;
            $buffer = str_replace("Error", "<span style=\"color: red;\"> Error </span>", $buffer);
            $buffer = str_replace("invalid token", "<span style=\"color: red;\"> invalid token</span> ", $buffer);
            $buffer = str_replace("1D", "", $buffer); # -[\[ 
                    $buffer = str_replace('[\[ [', "", $buffer); //-\/- 
                    
                    $buffer = str_replace('[', "", $buffer);
                    $buffer = str_replace(']', "", $buffer);
                    #$buffer = str_replace('/', "", $buffer);
                    $buffer = str_replace('|', "", $buffer);
                    $buffer = str_replace('-\/-', "", $buffer); //-\/- 

                    $buffer = str_replace(" up ", "<span style=\"color: green;\"> up </span>",$buffer);
                    $buffer = str_replace(" down ", "<span style=\"color: red;\"> down </span>",$buffer); //admin-state down
                    $buffer = str_replace("admin-state down", "<span style=\"color: red;\">admin-state down</span>",$buffer); //admin-state down
                    $buffer = str_replace("ont no slot", "<span style=\"color: red;\">ont no slot</span>",$buffer); //admin-state down

                    echo " <pre>$buffer</pre>";
            return;
        }
        echo " <pre>$buffer</pre>";
    }

    public function WaitErrorsNewHelp($findFirst)
    {
        try {
            $this->gateway->waitUntilStringIsFound("$findFirst", null, null, false,[ "invalid token" => 'Error',"Error : instance does not exist" => 'instance']);
            $Buffer = $this->gateway->getLastReadData();
            if ($this->$debug == true) $this->print_color ($Buffer,'true');
            $this->gateway->clearLastReadData();
            $Buffer = '';
            return 'true';
        } catch (\Exception $e) {
            $Buffer = $this->gateway->getLastReadData();
            if ($this->$debug == true) $this->print_color ($Buffer,'true');
            $this->gateway->clearLastReadData();
            if (false !== strpos($e->getMessage(),'instance')) {
                
                return "Error : instance does not exist";
                
            }
            return 'false';
        }
    }

    //############################################
    // WaitErrorsNew
    //############################################
    public function WaitErrorsNew($findFirst, $findLast, $exeption1, $exeption2, $exeption3, $print,$color)
    {
        if ($findLast == '') {
            $findLast = "#exitphp";
            
        }
        $this->gateway->write("$findLast\r\n");
        $findok = 'true';
        if ($findFirst != '')
        {
            
            $findok = $this->WaitErrorsNewHelp ($findFirst);
        }
        //echo "findok $findok";
        if ($findok == 'true' and $findok !=  "Error : instance does not exist") {
            try {
                $this->gateway->waitUntilStringIsFound("$findLast", null, null, false, [ "$exeption1" => 'Error', "$exeption2" => 'Failure', "$exeption3" => 'dontknow']); //invalid token
                $Buffer = $this->gateway->getLastReadData();

                if ($this->$debug == true) $this->print_color ($Buffer,$color);
                if ($print == 'y' and $this->$debug == false )$this->print_color ($Buffer,$color);

                $this->gateway->clearLastReadData(); // reset buffer, so it only returns the output of

                return $findLast;
                
            } catch (\Exception $e) {
                $Buffer = $this->gateway->getLastReadData();
                $this->gateway->clearLastReadData();
                if (false !== strpos($e->getMessage(),'Error')) {

                    if ($this->$debug == true or $print == 'y') $this->print_color ($Buffer,$color);

                    return $exeption1;
                }

                $this->print_color ($Buffer,$color);
                if (false !== strpos($e->getMessage(),'Error')) return $exeption1;
                if (false !== strpos($e->getMessage(),'Failure')) return $exeption2;
                if (false !== strpos($e->getMessage(),'dontknow')) return $exeption3;
                
                return 'xx';
            }
        }
        return $findok;
    }

    //############################################
    // WaitErrors
    //############################################
    public function WaitErrors($such1, $such2, $such3, $such4, $such5, $print)
    {
        try {
            $this->gateway->waitUntilStringIsFound("$such1", null, null, false, [ "$such2" => 'Error', "$such3" => 'Failure', "$such4" => 'invalid', "$such5" => '#php']); //invalid token
            $Buffer = $this->gateway->getLastReadData();

            if ($this->$debug == true) echo " <pre>$Buffer</pre>";
            if ($print == 'y' and $this->$debug == false ) echo " <pre>$Buffer</pre>";

            $this->gateway->clearLastReadData(); // reset buffer, so it only returns the output of

            return $such1;
        } catch (\Exception $e) {
            if (false !== strpos($e->getMessage(),'Error')) {
                $Buffer = $this->gateway->getLastReadData();

                if ($this->$debug == true or $print == 'y') echo " <pre>$Buffer</pre>";

                $this->gateway->clearLastReadData();

                return $such2;
            }

            if (false !== strpos( $e->getMessage(),'Failure') or false !== strpos( $e->getMessage(),'invalid') or false !== strpos( $e->getMessage(),'#php')) {
                $Buffer = $Buffer.$this->gateway->getLastReadData();

                echo " <pre>$Buffer</pre>";

                $this->gateway->clearLastReadData();

                return $such4;
            } else {
                $Buffer = $Buffer.$this->gateway->getLastReadData();
                echo " <pre>$Buffer</pre>";

                return 'xx';
            }

            throw $e;
        }
    }

    public function Experte(Customer $customer, Location $location, Card $card, MainProduct $customerService)
    {
        return 'done ';
    }

    //############################################
    //# Testx Login only
    //############################################
    public function Testx()
    {
        $this->wait_exit ('','#','');
        return $this->gateway->getLastReadData();
    }

    //############################################
    //# Test Login von den globalen Kommandos
    //############################################
    public function Test( Location $location )
    {
        //$this->VarInit ( null, $location ,null, null);
        $name = $location->getName();
        $this->dslam_name_extract ($location->getName(),$location);

        //(Customer $customer = null, Location $location, Card $card = null , MainProduct $customerService = null)
        echo 'Test <br>';
        echo " &nbsp;&nbsp;&nbsp;&nbsp; DSLAM_NAME      =   $this->dslam_name <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; Dslam Type      =   ".$location->getType()."   <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; logon_ip        =   ".$location->getIpAddress()."   <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; logon_port      =   ".$location->getLogonPort()."     <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; DSLAMLOGON      =   ".$location->getLogonUsername()."<br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; DSLAMPW         =   *******     <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; Real _ip        =   ".$location->getRealIp()."   <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; Current SW      =   ".$location->getCurrentSoftwareRelease()."   <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; Backup          =   $this->Backup <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; Vlan IPTV       =   ".$location->getVlanIptv()."  <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; vlan_ACS        =   ".$location->getVlanAcs()."   <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; vlan_pppoe      =   ".$location->getVlanPppoe()." <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; Vlan IPTV local =   ".$location->getVlanIptvLocal()."    <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; vlan_ACS local  =   ".$location->getVlanAcsLocal()." <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; vlan_pppoe_local=   ".$location->getVlanPppoeLocal()."   <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; vlan DCN        =   ".$location->getVlanDcn()."   <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; Vector_location =   ".$location->getVectoring()."    <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; Dslam_conf      =   ".$location->getConfigurable()." <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; location_id     =   ".$location->getId()."     <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; ACS Type        =   ".$location->getAcsType()."   <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp; Prozessor = ".$location->getProcessor()."<br><br>";
        /*
            if ($kvz_name1 != '') echo " &nbsp;&nbsp;&nbsp;&nbsp;KVZ = $this->kvz_name1 DPBO = $this->Default_DPBO1 ESEL = $this->esel_wert1 <br>";
            if ($kvz_name2 != '')echo " &nbsp;&nbsp;&nbsp;&nbsp;KVZ = $this->kvz_name2 DPBO = $this->Default_DPBO2 ESEL = $this->esel_wert2 <br>";
            if ($kvz_name3 != '')echo " &nbsp;&nbsp;&nbsp;&nbsp;KVZ = $this->kvz_name3 DPBO = $this->Default_DPBO3 ESEL = $this->esel_wert3 <br>";

            echo " &nbsp;&nbsp;&nbsp;&nbsp;Cardtyp = $this->cardtype<br>";
            echo " <hr>&nbsp;&nbsp;&nbsp;&nbsp; Kunde        = $this->clientid    <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; lsa_id       = $this->lsa_id  <br>";

            echo " &nbsp;&nbsp;&nbsp;&nbsp; line_identifier          = $this->line_identifier     <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; card_id      = $this->card_id <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; vlan_ID      = $this->vlan_id <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp;  VLAN_ID-old  = $this->vlan_ID_u<br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; tag          = $this->tag     <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; ip_address       = $this->ip_address  <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; dslam_port       = $this->dslam_port  <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; tv_service       = $this->tv_service  <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; DPBO         = $this->DPBO    <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; Service      = $this->Service <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; Spectrumprofile      = $this->Spectrumprofile <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; vectoring           = $this->vectoring    <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; produkt_index       = $this->produkt_index<br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; gpon_download       = $this->gpon_download<br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; gpon_upload         = $this->gpon_upload<br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; ethernet_up_down    = $this->ethernet_up_down<br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; firmware_version    = $this->firmware_version<br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; dtag_line           = $this->dtag_line<br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp; IPTV local VLAN = $this->vlan_iptv_local";

            //#firmware_version dtag_line
        */
        $this->wait_exit ('','#','');
        return $this->gateway->getLastReadData();


    }

    //#### begin new khl
    //############################################
    //# reboot1  confirm to reboot no test
    //############################################
    public function reboot1( Location $location)
    {
        $this->logout();
        $this->dslam_name_extract ($location->getName(),$location);
        $Dslam_comando = '';
        ?>
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        <a  class = "fancy-button outline round  red large18" href = "<?php echo "?location_id=".$location->getId()."&kommando=reboot"  ?>" > &nbsp; reboot <?php echo "$this->dslam_name" ?>  Bestaetigen&nbsp; </a> <br> <br>
        <?php



    }

    //############################################
    //# reboot2 with test
    //############################################
    public function reboot2( Location $location)
    {
      $this->logout();
      $this->dslam_name_extract ($location->getName(),$location);
      $Dslam_comando = '';
      ?>
      &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
      <a  class = "fancy-button outline round  red large18" href = "<?php echo "?location_id=".$location->getId()."&kommando=reboot3"  ?>" > &nbsp; reboot <?php echo "$this->dslam_name" ?>  Bestaetigen&nbsp; </a> <br> <br>
      <?php

    }

    //############################################
    //# DSLAM_Kunden_Loeschen
    //############################################
    public function DSLAM_Kunden_Loeschen(Customer $customer, Location $location)
    {
        $this->logout();
        $this->dslam_name_extract ($location->getName(),$location);
        $Dslam_comando = '';
        ?>
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        <a  class = "fancy-button outline round  red large18" href = "<?php echo " index.php?menu=customer&id=".$customer->getId()."&area=SwitchTechConf&kommando=DSLAM_Kunden_Loeschen1"  ?>" > &nbsp; Kunde  <?php echo "".$customer->getClientId()." Loeschen im DSLAM $this->dslam_name"?>  Bestaetigen&nbsp; </a > <br> <br>
        <?php

    }

    //############################################
    //# reboot now
    //############################################
    public function reboot( Location $location)
    {

    }

    //#### end new khl

    /**
     * {@inheritDoc}
     * 
     * @todo
     *      implement usage of ssh key
     */
    public function login(array $loginOptions) : bool
    {
        if ($this->isLoggedIn)
        {
            return true;
        }

        // hier noch prüfen, ob vorhanden oder ggf. key
        $this->isLoggedIn = $this->gateway->login($loginOptions['username'], $loginOptions['password']);

        if (!$this->isLoggedIn)
        {
            throw new LoginFailedException('login failed');
        }
        $this->gateway->waitUntilStringIsFound('>');
        //    ->clearLastReadData(); // reset buffer, so it only returns the output of following commands
        
        return $this->isLoggedIn;
    }

    /**
     * Get method (as string) by an alias. Mapping defined in $this->aliasMap
     * 
     * @param string $alias
     * 
     * @return string
     */
    public function getMethodByAlias(string $alias) : ? string
    {
        return isset($this->aliasMap[$alias]) ? $this->aliasMap[$alias] : null;
    }

    /**
     * {@inheritDoc}
     * 
     * @todo
     *      Implement logout
     */
    public function logout()
    {
        if (!$this->isLoggedIn)
        {
            return true;
        }

        // todo - Implement logout

        $this->gateway->disconnect();
    }

    /**
     * {@inheritDoc}
     */
    public function __destruct()
    {
        $this->logout();
    }

    //########################################################################################################################
    //wait_exit and print if collor to be made
    //########################################################################################################################
    public function wait_exit(string $color,string $first,string $last)
    {
        //if ($first == '') $first = ">#";
        if ($last == '') $last = "whoami";
        //echo " first = $first last = $lt <br>";

        $this->gateway->write("$last\r\n");
        if ($first != '')
        {
            $this->gateway->waitUntilStringIsFound($first);
            //$this->gateway->clearLastReadData(); // reset buffer, so it only returns the output of
        }
        $this->gateway->waitUntilStringIsFound($last);

        if ($color == 'true')
        {
            $buffer = $this->gateway->getLastReadData();
            //var_dump ($buffer);
            //exit;
            $buffer = str_replace("Error", "<span style=\"color: red;\"> Error </span>", $buffer);
            $buffer = str_replace("#begBLAU", "<span style=\"color: blue;\">", $buffer);
            $buffer = str_replace("#endBLAU", "</span>", $buffer);
            
            $buffer = str_replace("invalid token", "<span style=\"color: red;\"> invalid token</span> ", $buffer);
            $buffer = str_replace("1D", "", $buffer); # -[\[ 
                    $buffer = str_replace('[\[ [', "", $buffer);
                    $buffer = str_replace('[', "", $buffer);
                    $buffer = str_replace(']', "", $buffer);
                    #$buffer = str_replace('/', "", $buffer);
                    $buffer = str_replace('|', "", $buffer);

                    $buffer = str_replace(" up ", "<span style=\"color: green;\"> up </span>",$buffer);
                    $buffer = str_replace(" down ", "<span style=\"color: red;\"> down </span>",$buffer);
                    echo " <pre>$buffer</pre>";
        }
    }

    //########################################################################################################################
    //dslam_name_extract
    //########################################################################################################################
    public function dslam_name_extract(string $name,Location $location)
    {
        $dslamNameParted = null;

        if (1 === preg_match('/^((([^_]+)_.*)_R\d{2})\/.*/', $name, $dslamNameParted))
        {
            $this->dslam_name = $dslamNameParted[2];
            $this->dslam_name_ext = $dslamNameParted[1];
            $this->vlan_name1 = $dslamNameParted[3];
        }
        else
        {
            $this->dslam_name_ext     = substr($name, 0, 14);
            $this->dslam_name_ext     = preg_replace('/[^0-9a-z_]/i', '_', $this->dslam_name_ext);
            $this->dslam_name         = substr($name, 0, 10);
            $this->vlan_name1         = substr($name, 0, 3);
        }
        $this->Backup = $this->dslam_name_ext.".tar";
        $this->admin_save = "admin save";
        if ($location->getProcessor() == "NRNT-A" or $location->getProcessor() == "NATN-A") $this->admin_save = "admin software-mngt shub database save";
        if ($location->getProcessor() == "RANT-A" )  $this->admin_save = "";
        if ($location->getProcessor() == "SRNT-J" )  $this->admin_save = "";

        $this->tftpserver = \Wisotel\Configuration\Configuration::get('tftpServer');
        $this->Backup_id    = "admin software-mngt database upload actual-active:".$this->tftpserver."dm_complete_$this->dslam_name_ext".".tar";
    }

    //########################################################################################################################
    //adminsave
    //########################################################################################################################
    public function adminsave(Location $location)
    {
    }

    //########################################################################################################################
    //backup_dslam
    //########################################################################################################################
    public function backup_dslam( $location)
    {
        $this->dslam_name_extract(($location->getName()),$location);

    }

    //########################################################################################################################
    //DSLAM_Kunden_Anzeige CLI
    //########################################################################################################################
    public function Anzeigen(Customer $customer, Location $location, Card $card, MainProduct $customerService, $backup_JN)
    {
        require_once ('Ponosharedcode.php'); // init all needed Variables
        
        if ($firmware_version <> "") {
            $Einrichten4 ="onu image download-activate-commit 1/$PON/$sublineident $firmware_version\n";
        }

        switch ($FSAN) {
            case '2426':
                $Einrichten = "bridge add 1-1-$PON-$sublineident/gpononu gem 7$sublineident gtp  $produkt_index downlink-pppoe vlan $vlan_id epktrule $produkt_index tagged sip eth all wlan 1 rg-bpppoe\n";
                $Einrichten1 = "cpe rg wan modify 1/$PON/$sublineident-gponport vlan $vlan_id pppoe-usr-id $clientidv pppoe-password $pppoePassword\n";
                $Einrichten2 = "cpe-mgr add local 1-1-$PON-$sublineident/gpononu gem 5$sublineident gtp 1\n";
                $Einrichten3 = "cpe voip add 1/$PON/$sublineident/1 dial-number $PurtelLogin username $PurtelLogin password $PurtelPassword voip-server-profile WisoTel\n";
                
                break;

            case '2301':
            case '2311':
                $Einrichten = "bridge add 1-1-$PON-$sublineident/gpononu gem 7$sublineident gtp $produkt_index downlink vlan $vlan_id epktrule $produkt_index tagged eth 1\n";
                $Einrichten1 = "";
                $Einrichten2 = "";
                $Einrichten3 = "";
                
                break;
        }
        
        $this->dslam_name_extract(($location->getName()),$location);
        echo "<pre>";
        echo "Lineident = $line_identifier Line = $sublineident -- PON = $PON <BR>";  
        echo  "<span class='text green'>Einrichten mit CLI</span><BR><BR>";
        echo  "onu set 1/$PON/$sublineident vendorid ZNTS serno fsan $dtag_line meprof zhone-$FSAN\n<BR>";
        echo  $Einrichten."<BR>";
        echo  "bridge stats enable 1-1-$PON-7$sublineident-gponport-$vlan_id/bridge\n<BR>";
        
        if ($Einrichten1 <> "") echo  $Einrichten1."<BR>";
        if ($Einrichten2 <> "") echo  $Einrichten2."<BR>";
        if (isset($purtelMasterAccount) && !empty($purtelMasterAccount['nummer'])) {
            if ($Einrichten3 <> "") echo  $Einrichten3."<BR>";
        }
        if ($Einrichten4 <> "") echo  $Einrichten4."<BR>";
        echo "</pre>";
        
        
        $this->gateway->write("whoami \r\n");
        $this->wait_exit ('true','','>');

        return $this->gateway->getLastReadData();
    }

    //########################################################################################################################
    //DSLAM_show Bridge
    //########################################################################################################################
    public function Bridge(Customer $customer, Location $location, Card $card, MainProduct $customerService,$backup_JN) {
        //echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>";

        require_once ('Ponosharedcode.php'); // init all needed Variables
        $this->gateway->write("bridge show $line_identifier/gpononu \r\n");
        $this->gateway->write("whoami \r\n");

        $this->wait_exit ('true','','ssh');

        return $this->gateway->getLastReadData();
    }

    //########################################################################################################################
    //DSLAM_Kunden_einrichten
    //########################################################################################################################
}
