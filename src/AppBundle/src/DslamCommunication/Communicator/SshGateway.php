<?php

/**
 *
 */

namespace AppBundle\src\DslamCommunication\Communicator;

use AppBundle\src\DslamCommunication\Communicator\GatewayInterace;
use phpseclib\Net\SSH2;

/**
 * 
 */
class SshGateway implements GatewayInterace
{
    /**
     * @var phpseclib\Net\SSH2
     */
    protected $connection;

    /**
     * Holds last read data from waitUntilStringIsFound()
     * 
     * @var string
     */
    protected $lastReadData = '';

    /**
     * Constructor
     * 
     * @param string $server
     * @param integer $port
     * @param integer $connectionTimeout
     */
    public function __construct(string $server, int $port, int $connectionTimeout)
    {
        $this->connection = new SSH2($server, $port, $connectionTimeout);
    }

    /**
     * @param string $username
     * @param string $password
     * 
     * @return boolean
     */
    public function login(string $username, string $password)
    {
        return $this->connection->login($username, $password);
    }

    /**
     * {@inheritDoc}
     */
    public function disconnect() : void
    {
        $this->connection->disconnect();
    }

    /**
     * {@inheritDoc}
     */
    public function __destruct()
    {
        $this->disconnect();
    }

    /**
     * {@inheritDoc}
     */
    public function write(string $command) : ?GatewayInterace
    {
        if ("\n" !== substr($command, -1)) {
            $command .= "\n";
        }

        $this->connection->write($command);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function read() : string
    {
        $data = $this->connection->read();

        return is_string($data) ? $data : '';
    }

    /**
     * Most code was copied from phpseclib\Net\SSH2::read()
     * 
     * {@inheritDoc}
     */
    public function waitUntilStringIsFound(string $string, int $maxWaitTime = null, string $errorMessage = null, bool $useRegularExpression = false, $stringsThatAlsoBreak = null) : GatewayInterace
    {
        $timeout = $this->connection->timeout;

        if (null !== $maxWaitTime) {
            $timeout = $maxWaitTime;
        }

        $this->connection->curTimeout = $timeout;
        $this->connection->is_timeout = false;

        if (null === $errorMessage) {
            $errorMessage = 'unable to find string';
        }

        if (!$this->connection->isAuthenticated()) {
            user_error('Operation disallowed prior to login()');
            return false;
        }

        if (!($this->connection->bitmap & SSH2::MASK_SHELL) && !$this->connection->_initShell()) {
            user_error('Unable to initiate an interactive shell session');
            return false;
        }

        $channel = $this->connection->_get_interactive_channel();

        while (true) {
            $response = $this->connection->_get_channel_packet($channel);
            
            // reached end or timeout
            if (is_bool($response)) {
                $this->connection->in_request_pty_exec = false;
                break;
            }

            $this->lastReadData .= $response;

            if ($useRegularExpression) {
                if (1 === preg_match($string, $response)) {
                    $found = true;

                    break;
                }
            } else {
                if (false !== strpos($response, $string)) {
                    $found = true;

                    break;
                }
            }

            if (is_array($stringsThatAlsoBreak)) {
                foreach ($stringsThatAlsoBreak as $str => $error) {
                    if (false !== strpos($response, $str)) {
                        $errorMessage .= ' ('.$error.')';

                        break 2;
                    }
                }
            }
        }

        $this->connection->curTimeout = $this->connection->timeout; // reset timeout

        if ($found) {
            return $this;
        }

        throw new \Exception($errorMessage);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastReadData() : string
    {
        $data = $this->lastReadData;

        $this->lastReadData = '';

        return $data;
    }

    /**
     * {@inheritDoc}
     */
    public function clearLastReadData() : GatewayInterace
    {
        $this->lastReadData = '';
        
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getConnection()
    {
        return $this->connection;
    }
}
