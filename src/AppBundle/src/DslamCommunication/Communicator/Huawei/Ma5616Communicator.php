<?php

/**
 *
 */

namespace AppBundle\src\DslamCommunication\Communicator\Huawei;

use AppBundle\src\DslamCommunication\Communicator\AbstractCommunicator;
use AppBundle\src\DslamCommunication\Communicator\CommunicatorInterface;
use AppBundle\src\DslamCommunication\Communicator\GatewayInterace;
use AppBundle\src\DslamCommunication\Communicator\SshGateway;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Location\Card;
use AppBundle\Entity\Location\Location;
use AppBundle\Entity\Product\MainProduct;
use AppBundle\src\DslamCommunication\SupportedDevice;
use AppBundle\src\DslamCommunication\Exception\LoginFailedException;

/**
 * @SupportedDevice(vendor="huawei", model="ma5616", softwareRelease="1.0")
 * @SupportedDevice(vendor="huawei", model="ccc")
 */
class Ma5616Communicator extends AbstractCommunicator implements CommunicatorInterface
{
    /**
     * Maps some aliases to methods to be used with getMethodByAlias()
     *
     * @var array
     */
    protected $aliasMap = [
        'DSLAM Kunden Status' => 'getCustomerStatus',
    ];

    /**
     * Holds current login stage
     *
     * @var boolean
     */
    protected $isLoggedIn = false;

    /**
     * Get customer status from dslam
     *
     * @param AppBundle\Entity\Customer $customer
     *
     * @return string
     */
    public function getCustomerStatus(Customer $customer)
    {
        return 'todo';
    }

    /**
     * Find mac addresses for customers line identifier
     *
     * @param AppBundle\Entity\Customer $customer
     *
     * @throws RuntimeException
     *
     * @return string
     */


     //######################################################
     //
     //#####################################################

    public function findMac(Customer $customer)
    {
//die(var_dump( "Dslam-Port:", $customer->getDslamPort()->getNumber() ));

        $this->gateway
            ->write(sprintf("display mac-address port %s/%u",substr($customer->getLineIdentifier(),0,3),$customer->getDslamPort()->getNumber()))
            ->waitUntilStringIsFound('#');

        return $this->gateway->getLastReadData();
    }

    //######################################################
    ////display vlanAll
    //#####################################################

   public function vlanAll(Customer $customer)
   {

       $this->gateway
           ->write(sprintf("display vlan all"))
           ->write(sprintf(""))

           ->waitUntilStringIsFound('#');

       return $this->gateway->getLastReadData();
   }
   //######################################################
   ////display board 0/0 0/1
   //#####################################################

  public function displayBoards(Customer $customer)
  {


    $this->gateway->write("display board 0/0");
    $this->printWaitMore (false);
    $this->gateway->write("display board 0/1");
    $this->printWaitMore (false);

    return $this->gateway->getLastReadData();
  }

  //######################################################
  ////display board 0/0 0/1
  //#####################################################

 public function displayBoardsTest(Customer $customer)
 {

   $this->gateway->write("display board 0/0");
   $this->printWaitMore (false);
   $this->gateway->write("display board 0/1");

   $this->printWaitMore (false);



     return $this->gateway->getLastReadData();
 }
    //######################################################
    //
    //#####################################################

    public function findMacAll(Customer $customer)
    {

        $this->gateway
            ->write(sprintf("display mac-address port 0/0/0"))
            ->waitUntilStringIsFound('#');

        return $this->gateway->getLastReadData();
    }

    //######################################################
    //
    //#####################################################

    public function backup(Customer $customer)
    {

        $this->gateway
            ->write('config')
            ->waitUntilStringIsFound('#')
            ->write('save')
            ->write('')
            ->waitUntilStringIsFound('completely',120)

            ->write('quit')
            ->waitUntilStringIsFound('#');
            //->waitUntilStringIsFound('successfully')
            //->waitUntilStringIsFound('#')
            //->waitUntilStringIsFound('#');
            //var_dump ($this->gateway->read());
            //exit



        return $this->gateway->getLastReadData();
    }

    //######################################################
    //#ntp-service unicast-server 5.189.146.13
    //#####################################################

    public function timeDisplay(Customer $customer)
    {

        $this->gateway
            ->write('display time')
            ->write('')
            ->waitUntilStringIsFound('display time')
            ->clearLastReadData()
            ->waitUntilStringIsFound('#');
            echo 'Datum und Zeit';
            echo ' ';

        return $this->gateway->getLastReadData();
    }
    //######################################################
    //#ntp-service unicast-server 5.189.146.13
    //#####################################################

    public function timeSet(Customer $customer)
    {

        $this->gateway
            ->write('config')
            ->waitUntilStringIsFound('#')
            ->write('ntp-service unicast-server 5.189.146.13')
            ->write('')
            ->write ('timezone GMT+ 01:00')
            ->write ('dns server 8.8.8.8')
            ->write('save')
            ->write('')
            ->waitUntilStringIsFound('completely',120)

            ->write('quit')
            ->write('display time')
            ->write('')
            ->waitUntilStringIsFound('display time')
            ->waitUntilStringIsFound('#');

        return $this->gateway->getLastReadData();
    }


    public function Experte()
    {
        return 'done ';
    }

    //######################################################
    //
    //#####################################################

    public function cpeInfo (Customer $customer)
    {
        $testkh = $customer->getDslamPort()->getNumber();

        //$testkh = str_replace ("string","",$testkh);
        //$testkh1 = $testkh->dslam_port;
        //var_dump ($customer);
        //exit;
        //echo ("display interface vdsl 0/$testkh \r\n");
        //echo sprintf(" interface vdsl 0/%u", $customer->getDslamPort()->getNumber());
        //echo ("");
        //die (var_dump ($testkh));
        //die(var_dump( "Dslam-Port:", $customer->getDslamPort()->getNumber() ));
        //$line_s = substr($customer->getLineIdentifier(),0,3);
        //$line_s = substr($line_s,0,3);
      $this->gateway

          ->write(sprintf("config"))
          ->waitUntilStringIsFound('#')
          ->write(sprintf(" interface vdsl %s",substr($customer->getLineIdentifier(),0,3)));
      if ($this->WaitErrors ())
      {


              //->waitUntilStringIsFound('#')
          $this->gateway->write(sprintf("display inventory cpe %u",$customer->getDslamPort()->getNumber()));
          $this->WaitErrors ();
              //->waitUntilStringIsFound('#')
              //->waitUntilStringIsFound(' More ')
         $this->gateway->write("quit")
              //->waitUntilStringIsFound(' More ')
              ->write("quit");
         $this->WaitErrors ();
         $this->WaitErrors ();
     }      //->waitUntilStringIsFound('#')
          //->waitUntilStringIsFound('#');
          return $this->gateway->getLastReadData();
    }
    //######################################################
    //
    //#####################################################

    public function portInfo (Customer $customer)
    {
      $this->gateway

          ->write(sprintf("config"))
          ->waitUntilStringIsFound('#')
          ->write(sprintf("display interface vdsl %s", $customer->getLineIdentifier()));
      $this->printWaitMore (false);
      $this->gateway->write("quit");
          return $this->gateway->getLastReadData();
    }


    //######################################################
    //
    //#####################################################
    public function displayLineOperation (Customer $customer)
    {
        $this->gateway
        ->write(sprintf("config"))
        ->waitUntilStringIsFound('#')
        ->write(sprintf(" interface vdsl %s",substr($customer->getLineIdentifier(),0,3)))
        ->waitUntilStringIsFound('#')
        ->write(sprintf("display line operation %u", $customer->getDslamPort()->getNumber()));
        $this->printWaitMore (true);
        //->waitUntilStringIsFound(' More ')
        $this->gateway->write("quit")
        //->waitUntilStringIsFound(' More ')


        ->waitUntilStringIsFound('#');
        return $this->gateway->getLastReadData();

    }

    //######################################################
    //
    //#####################################################
    public function lastRetrain (Customer $customer)
    {
        $this->gateway
        ->write(sprintf("diagnose"))
        ->waitUntilStringIsFound('%%')
        ->write(sprintf(" display line last-retrain-info %s/%u",substr($customer->getLineIdentifier(),0,3),$customer->getDslamPort()->getNumber()))
        ->waitUntilStringIsFound('%%')
        ->write("quit")

        ->waitUntilStringIsFound('#');
        return $this->gateway->getLastReadData();

    }

    //######################################################
    //
    //#####################################################
    public function einrichten (Customer $customer, Location $location, MainProduct $customerService)
    {
        $this->gateway
        ->write(sprintf("config"))
        ->waitUntilStringIsFound('#')
        ->write(sprintf(" interface vdsl %s",substr($customer->getLineIdentifier(),0,3)))
        ->waitUntilStringIsFound('#')
        ->write(sprintf("deactivate %u", $customer->getDslamPort()->getNumber()))
        ->waitUntilStringIsFound('#')
        ->write(sprintf("activate %u prof-idx ds-rate %u us-rate %u ", $customer->getDslamPort()->getNumber(),$customerService->getXdslServiceProfile(),$customerService->getXdslServiceProfile()))
        ->write("\r\n")
        ->waitUntilStringIsFound('activate')
        ->waitUntilStringIsFound('#')
        //->waitUntilStringIsFound(' More ')
        ->write("quit")
        //->write("save")
        //->waitUntilStringIsFound('save')
        ->write("quit \r\n")
        ->waitUntilStringIsFound('#')
        ->waitUntilStringIsFound('#');
        //echo sprintf('<pre>%s</pre>', $this->gateway->getLastReadData());
        //exit;
        return $this->gateway->getLastReadData();

    }
    //######################################################
    //
    //#####################################################
    public function SELTein (Customer $customer)
    {
        $this->gateway
        ->write(sprintf("config"))
        ->waitUntilStringIsFound('#')
        ->write(sprintf(" interface vdsl %s",substr($customer->getLineIdentifier(),0,3)))
        ->waitUntilStringIsFound('#')
        ->write(sprintf("deactivate %u", $customer->getDslamPort()->getNumber()))
        ->waitUntilStringIsFound('#')
        ->write(sprintf("vdsl selt %u", $customer->getDslamPort()->getNumber()))
        ->write("")
        ->write(sprintf("activate %u ", $customer->getDslamPort()->getNumber()))
        ->write("")
        ->waitUntilStringIsFound('activate')
        ->waitUntilStringIsFound('#')
        //->waitUntilStringIsFound(' More ')
        ->write("quit")
        //->write("save \r\n")
        //->waitUntilStringIsFound('save')
        ->write("quit")
        ->waitUntilStringIsFound('#')
        ->waitUntilStringIsFound('#');
        //echo sprintf('<pre>%s</pre>', $this->gateway->getLastReadData());
        //exit;
        return $this->gateway->getLastReadData();

    }

    //######################################################
    //
    //#####################################################
    public function displayChannelOperationCo (Customer $customer)
    {
        $this->gateway
        ->write(sprintf("config"))
        ->waitUntilStringIsFound('#')
        ->write(sprintf(" interface vdsl %s",substr($customer->getLineIdentifier(),0,3)))
        ->waitUntilStringIsFound('#')
        ->write(sprintf("display channel operation co %u channel 1", $customer->getDslamPort()->getNumber()))
        ->waitUntilStringIsFound('#')
        //->waitUntilStringIsFound(' More ')
        ->write("quit")
        //->waitUntilStringIsFound(' More ')
        ->write("quit")
        ->waitUntilStringIsFound('#')
        ->waitUntilStringIsFound('#');
        return $this->gateway->getLastReadData();

    }
    //######################################################
    //
    //#####################################################
    public function displayChannelOperationCpe (Customer $customer)
    {
        $this->gateway
        ->write(sprintf("config"))
        ->waitUntilStringIsFound('#')
        ->write(sprintf(" interface vdsl %s",substr($customer->getLineIdentifier(),0,3)))
        ->waitUntilStringIsFound('#')
        ->write(sprintf("display channel operation cpe %u channel 1", $customer->getDslamPort()->getNumber()))
        ->waitUntilStringIsFound('#')
        //->waitUntilStringIsFound(' More ')
        ->write("quit")
        //->waitUntilStringIsFound(' More ')
        ->write("quit")
        ->waitUntilStringIsFound('#')
        ->waitUntilStringIsFound('#');
        return $this->gateway->getLastReadData();

    }

    //######################################################
    //
    //#####################################################
    public function displayRouteId ()
    {
        $this->gateway
         ->write(sprintf("display router id"))
         ->waitUntilStringIsFound('#');
         return $this->gateway->getLastReadData();

    }

    //######################################################
    //
    //#####################################################
    public function pingGoogle ()
    {
        $this->gateway
         ->write("ping 8.8.8.8")
         ->waitUntilStringIsFound('#');
         return $this->gateway->getLastReadData();

    }

    //######################################################
    //
    //#####################################################
    public function displayArp ()
    {
        $this->gateway
         ->write("display arp all")
         ->write(" ")
         ->waitUntilStringIsFound('Command:')
         ->clearLastReadData()
         ->waitUntilStringIsFound('#');
         return $this->gateway->getLastReadData();

    }

    //######################################################
    //xdslInfo
    //#####################################################
    public function xdslInfo ()
    {
        $this->gateway
        ->write("config")
        ->waitUntilStringIsFound('#')
         ->write("display xdsl dpbo-profile")
         ->write(" ")
         ->waitUntilStringIsFound('Command:')
         ->clearLastReadData()
         ->waitUntilStringIsFound('#');
         echo 'dpbo-profiles';
         echo "<pre>".$this->gateway->getLastReadData()."</pre>";
         $this->gateway
         ->clearLastReadData()
         ->write("display xdsl vectoring-profile")
         ->write(" ")
         ->waitUntilStringIsFound('Command:')
         ->clearLastReadData()
         ->write("quit")
         ->waitUntilStringIsFound('#')
         ->waitUntilStringIsFound('#');
         echo 'vectoring-profiles';
         return $this->gateway->getLastReadData();

    }

    //######################################################
    //
    //#####################################################
    public function displayInventoryCpe (Customer $customer)
    {

        $this->gateway
         ->write(sprintf("display inventory cpe %i",$customer->getDslamPort()->getNumber()))
         ->waitUntilStringIsFound('#');
         return $this->gateway->getLastReadData();
    }
    /**
     * {@inheritDoc}
     *
     * @todo
     *      implement usage of ssh key
     */
    public function login(array $loginOptions) : bool
    {
        if ($this->isLoggedIn) {
            return true;
        }

        // hier noch prüfen, ob vorhanden oder ggf. key
        $this->isLoggedIn = $this->gateway->login($loginOptions['username'], $loginOptions['password']);

        if (!$this->isLoggedIn) {
            throw new LoginFailedException('login failed');
        }
        $this->gateway
            ->write("enable")
            ->waitUntilStringIsFound('#')
            ->clearLastReadData(); // reset buffer, so it only returns the output of following commands
        return $this->isLoggedIn;
    }

    /**
     * Get method (as string) by an alias. Mapping defined in $this->aliasMap
     *
     * @param string $alias
     *
     * @return string
     */
    public function getMethodByAlias(string $alias) : ?string
    {
        return isset($this->aliasMap[$alias]) ? $this->aliasMap[$alias] : null;
    }

    /**
     * {@inheritDoc}
     */
    public function logout()
    {
        if (!$this->isLoggedIn) {
            return true;
        }

        $this->gateway
            ->write("quit")
            ->waitUntilStringIsFound('Are you sure to log out? (y/n)[n]:')
            ->write("y");

        $this->gateway->disconnect();
    }

    /**
     * {@inheritDoc}
     */
    public function __destruct()
    {
        $this->logout();
    }

    //############################################
    //# reboot1  confirm to reboot no test
    //############################################

    public function reboot1 ( Customer $customer,Location $location)
    {
        //$this->logout();
        //$this->dslam_name_extract ($location->getName(),$location);
        $Dslam_comando = '';
        ?>
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        <a  class = "fancy-button outline round  red large18" href = "<?php echo "?menu=customer&id=".$customer->getId()."&area=SwitchTechConf&kommando=reboot"  ?>" > &nbsp; Reboot <?php echo "$this->dslam_name" ?>  Bestaetigen&nbsp; </a> <br> <br>
        <?php



    }

    //############################################
    //# reboot now
    //############################################


    public function reboot ( Customer $customer,Location $location)
    {

        echo "reboot gestartet";
        $this->gateway
        ->write("reboot system")
        ->waitUntilStringIsFound("are you sure to reboot system? (y/n)[n]:")
        ->write("y")
        ->waitUntilStringIsFound("#");
        return $this->gateway->getLastReadData();

    }

    //############################################
    //
    //############################################

    public function printWaitMore ($quit)
    {
        while (true) {
            try {
                $this->gateway->waitUntilStringIsFound('#', null, null, false,["---- More ( Press 'Q' to break ) ----"=>'more found'] );
                $Buffer = $this->gateway->getLastReadData();
                for ($i=1;$i < 30;$i++)
                {
                    $Buffer = str_replace("---- More ( Press 'Q' to break ) ----", "", $Buffer);
                    $Buffer = str_replace("[37D                                     [37D", "", $Buffer);
                }

                echo " <pre>$Buffer</pre>";
                if ($quit == true) {
                    $this->gateway->write("quit");
                }
                $this->gateway->write("display timezone");
                $this->gateway->waitUntilStringIsFound('current time zone');
                $this->gateway->waitUntilStringIsFound('#');
                $this->gateway->clearLastReadData(); // reset buffer, so it only returns the output of

                break;
            } catch (\Exception $e) {
                if (false !== strpos( $e->getMessage(),'more found')) {

                    $this->gateway->write(" ");
                } else {
                    throw $e;
                }
            }
        }

    }

    //############################################
    //
    //############################################

    public function WaitErrors ()
    {
        while (true) {
            try {
                $this->gateway->waitUntilStringIsFound('#', null, null, false,["Unknown command"=>'Unknown command'], ["Parameter error"=>'Parameter error'],["Failure:"=>'Failure:']);
                $Buffer = $this->gateway->getLastReadData();

                echo " <pre>$Buffer</pre>";
                $this->gateway->clearLastReadData(); // reset buffer, so it only returns the output of
                return true;
                break;
            } catch (\Exception $e) {
                if (false !== strpos( $e->getMessage(),'Unknown command') or false !== strpos( $e->getMessage(),'Parameter error') or false !== strpos( $e->getMessage(),'Failure:')) {

                    echo " <pre>$Buffer</pre>";
                    $this->gateway->clearLastReadData();
                    return false;
                } else {
                    throw $e;
                }
            }
        }

    }

}
