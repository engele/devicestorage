<?php

/**
 *
 */

namespace AppBundle\src\DslamCommunication\Communicator;

/**
 * 
 */
interface GatewayInterace
{
    /**
     * @return void
     */
    public function disconnect() : void;

    /**
     * Destructor
     */
    public function __destruct();
    
    /**
     * Get the raw connection object
     * 
     * @return object
     * 
     * @todo
     *      add typehinting for return value (php 7.2 required)
     */
    public function getConnection();

    /**
     * Write command
     * 
     * @param string $command
     * 
     * @return AppBundle\src\DslamCommunication\Communicator\GatewayInterace|null
     */
    public function write(string $command) : ?GatewayInterace;

    /**
     * Read until end or timeout
     * 
     * @return string
     */
    public function read() : string;

    /**
     * Reads to connection until the given string comes up or until maxWaitTime is reached
     * 
     * @param string $string - String to wait for
     * @param int $maxWaitTime - Overwrite default timeout
     * @param string $errorMessage - If string is nout found, this message will be returned
     * @param boolean $useRegularExpression - Whether to treat string as regular expression
     * @param array $stringsThatAlsoBreak - This array contains strings (key) and error-messages (values) that are also supposed to break the waiting
     * 
     * @return AppBundle\src\DslamCommunication\Communicator\GatewayInterace
     */
    public function waitUntilStringIsFound(string $string, int $maxWaitTime = null, string $errorMessage = null, bool $useRegularExpression = false, $stringsThatAlsoBreak = null) : GatewayInterace;

    /**
     * Get last read data from waitUntilStringIsFound(). Truncates the buffer with each call.
     * 
     * @return string
     */
    public function getLastReadData() : string;

    /**
     * Truncate last read data buffer
     * 
     * @return GatewayInterace
     */
    public function clearLastReadData() : GatewayInterace;
}
