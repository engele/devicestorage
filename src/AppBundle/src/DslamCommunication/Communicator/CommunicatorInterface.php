<?php

/**
 * 
 */

namespace AppBundle\src\DslamCommunication\Communicator;

use AppBundle\src\DslamCommunication\Communicator\GatewayInterace;
use AppBundle\src\DslamCommunication\Communicator\CommunicatorInterface;

/**
 * 
 */
interface CommunicatorInterface
{
    /**
     * Perform a login on gateway
     * 
     * @param array $loginOptions
     * 
     * @throws AppBundle\src\DslamCommunication\Exception\LoginFailedException
     * 
     * @return boolean
     */
    public function login(array $loginOptions) : bool;

    /**
     * Perform a logout on gateway
     * 
     * @todo
     *      Think about return value
     */
    public function logout();

    /**
     * Set a gateway for connection.
     * 
     * @param AppBundle\src\DslamCommunication\Communicator\GatewayInterace $gateway
     * 
     * @return AppBundle\src\DslamCommunication\Communicator\CommunicatorInterface
     */
    public function setGateway(GatewayInterace $gateway) : CommunicatorInterface;

    /**
     * Ensure connection is disconnected
     */
    public function __destruct();
}
