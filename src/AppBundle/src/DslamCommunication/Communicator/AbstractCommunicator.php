<?php

/**
 * 
 */

namespace AppBundle\src\DslamCommunication\Communicator;

use AppBundle\src\DslamCommunication\Communicator\CommunicatorInterface;
use AppBundle\src\DslamCommunication\Communicator\GatewayInterace;

/**
 * 
 */
abstract class AbstractCommunicator
{
    /**
     * The connection gateway
     * 
     * @var AppBundle\src\DslamCommunication\Communicator\GatewayInterace
     */
    protected $gateway;

    /**
     * {@inheritDoc}
     */
    public function setGateway(GatewayInterace $gateway) : CommunicatorInterface
    {
        $this->gateway = $gateway;

        return $this;
    }

    /**
     * Get connection gateway
     * 
     * @return AppBundle\src\DslamCommunication\Communicator\GatewayInterace
     */
    public function getGateway() : GatewayInterace
    {
        return $this->gateway;
    }
}
