<?php

/**
 * 
 */

namespace AppBundle\src\DslamCommunication\Exception;

/**
 * 
 */
class LoginFailedException extends \Exception
{
    /**
     * @param string  $message
     * @param integer $number - default null
     */
    public function __construct($message, $number = null)
    {
        parent::__construct($message, $number);
    }
}
