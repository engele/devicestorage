<?php

/**
 * 
 */

namespace AppBundle\src\DslamCommunication;

/**
 * @Annotation
 */
class SupportedDevice
{
    /**
     * Name of vendor in lowecase
     */
    public $vendor;

    /**
     * Name of model in lowecase
     */
    public $model;

    /**
     * String of software-release version
     */
    public $softwareRelease;
}
