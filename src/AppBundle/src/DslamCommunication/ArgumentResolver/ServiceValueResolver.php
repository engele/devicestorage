<?php

/**
 *
 */

namespace AppBundle\src\DslamCommunication\ArgumentResolver;

use Psr\Container\ContainerInterface;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * 
 */
final class ServiceValueResolver implements ArgumentValueResolverInterface
{
    /**
     * @var Psr\Container\ContainerInterface
     */
    private $container;

    /**
     * 
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return $this->container->has($argument->getType()) || $this->container->has($argument->getName());
    }

        /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $service = $argument->getType();

        if (!$this->container->has($service)) {
            $service = $argument->getName();
        }

        yield $this->container->get($service);
    }
}
