<?php

/**
 *
 */

namespace AppBundle\src\AxirosAcs\Exception;

/**
 *
 */
class AxirosAcsUnexpectedValueException extends \UnexpectedValueException implements FatalExceptionInterface
{
    /**
     * Constructor
     * 
     * @param string $message
     * @param integer $code
     */
    public function __construct($message, $code = null)
    {
        parent::__construct($message, $code);
    }
}
