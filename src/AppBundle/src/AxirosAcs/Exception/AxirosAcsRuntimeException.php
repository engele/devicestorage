<?php

/**
 *
 */

namespace AppBundle\src\AxirosAcs\Exception;

/**
 *
 */
class AxirosAcsRuntimeException extends \RuntimeException implements FatalExceptionInterface
{
    /**
     * Constructor
     * 
     * @param string $message
     * @param integer $code
     */
    public function __construct($message, $code = null)
    {
        parent::__construct($message, $code);
    }
}
