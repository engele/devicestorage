<?php

/**
 *
 */

namespace AppBundle\src\AxirosAcs\Exception;

/**
 *
 */
class ErrorResponseException extends \Exception
{
    /**
     * Create exception from curlResponse
     * 
     * @param \stdClass $curlResponse
     */
    public function __construct(\stdClass $curlResponse)
    {
        parent::__construct($curlResponse->message, $curlResponse->code);
    }
}
