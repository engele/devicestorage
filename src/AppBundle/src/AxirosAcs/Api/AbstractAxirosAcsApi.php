<?php

/**
 *
 */

namespace AppBundle\src\AxirosAcs\Api;

/**
 * Class to use Axiros ACS API
 */
abstract class AbstractAxirosAcsApi
{
    /**
     * API username
     * 
     * @var string
     */
    protected $username;

    /**
     * API password
     * 
     * @var string
     */
    protected $password;

    /**
     * Url of server (must start with protocol)
     * 
     * @var string
     */
    protected $url;

    /**
     * Constructor
     * 
     * @param string $username
     * @param string $password
     * @param string $url
     */
    public function __construct($username, $password, $url)
    {
        $this->url = $url;
        $this->username = $username;
        $this->password = $password;
    }
}
