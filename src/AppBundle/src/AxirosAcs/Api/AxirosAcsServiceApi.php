<?php

/**
 *
 */

namespace AppBundle\src\AxirosAcs\Api;

use AppBundle\src\AxirosAcs\Service\AbstractService;
use Curl\Curl;
use AppBundle\src\AxirosAcs\Exception\ErrorResponseException;
use AppBundle\src\AxirosAcs\Exception\AxirosAcsRuntimeException;
use AppBundle\src\AxirosAcs\Exception\AxirosAcsUnexpectedValueException;
use AppBundle\src\AxirosAcs\Exception\FatalExceptionInterface;
use PropertyReader\PropertyReader;
use AppBundle\src\AxirosAcs\Service\ServiceTransmissionStatus;

/**
 * Class to use Axiros ACS API for services
 */
class AxirosAcsServiceApi extends AbstractAxirosAcsApi
{
    /**
     * Services to send to acs
     * 
     * @var array
     */
    protected $services = [];

    /**
     * API paths (url)
     * 
     * Use $this->getPath($identifier) over $this->paths[$identifier]
     * 
     * @var array
     */
    protected $paths = [
        'activate.pppoe' => '/live/CPEManager/AXServiceStorage/Interfaces/rest/v1/services/%s/cpeid/%s/action/%s',
        'modify.pppoe' => 'activate.pppoe',
        'deactivate.pppoe' => 'activate.pppoe',
        'reactivate.pppoe' => 'activate.pppoe',
        'delete.pppoe' => 'activate.pppoe',
        'activate.voice' => 'activate.pppoe',
        'modify.voice' => 'activate.voice',
        'deactivate.voice' => 'activate.voice',
        'reactivate.voice' => 'activate.voice',
        'delete.voice' => 'activate.voice',
    ];

    /**
     * Get path for serviceIdentifier
     * Use this function over $this->paths[$serviceIdentifier]
     * 
     * @param string $serviceIdentifier
     * 
     * @throws AxirosAcsUnexpectedValueException
     * 
     * @return string
     */
    protected function getPath($serviceIdentifier)
    {
        if (!isset($this->paths[$serviceIdentifier])) {
            throw new AxirosAcsUnexpectedValueException(sprintf('no path for service (%s) found', $serviceIdentifier));
        }

        $path = $this->paths[$serviceIdentifier];

        while (isset($this->paths[$path])) {
            $path = $this->paths[$path];
        }

        return $path;
    }

    /**
     * Get full path (including url of acs) for serviceIdentifier
     * 
     * @param string $serviceIdentifier
     * 
     * @return string
     */
    protected function getFullPath($serviceIdentifier)
    {
        return $this->url.$this->getPath($serviceIdentifier);
    }

    /**
     * Get api url for given service
     * 
     * @param AbstractService $service
     * 
     * @return string
     */
    protected function getServiceApiUrl(AbstractService $service)
    {
        return sprintf($this->getFullPath(sprintf('%s.%s', $service->getAction(), $service->getType())),
            $service->getType(),
            $service->getCpeId(),
            $service->getAction()
        );
    }

    /**
     * Write all services to acs
     * 
     * @return AxirosAcsServiceApi
     */
    public function writeServices()
    {
        foreach ($this->services as $service) {
            $curl = $this->createCurlInstance();

            try {
                if (null === $service->getCpeId()) {
                    throw new AxirosAcsUnexpectedValueException('CpeId can not be empty');
                }

                $serviceApiUrl = $this->getServiceApiUrl($service);

                // axiros expectes json-encoded always as object ({}) not as array ([])
                $curl->post($serviceApiUrl, json_encode($service->getData(), JSON_FORCE_OBJECT));

                if ($curl->error) {
                    throw new AxirosAcsRuntimeException(
                        sprintf('%s - Url: %s - Response: %s',
                            $curl->errorMessage, $serviceApiUrl, $curl->response->error
                        ),
                        $curl->errorCode
                    );
                }

                if (200 === $curl->response->code) {
                    $this->setTransmissionStatus($service, 'success', $curl->response);

                    $successCallback = $service->getSuccessCallback();

                    if (is_callable($successCallback)) {
                        call_user_func($successCallback, $curl->response);
                    }

                    // else - do nothing, swallow success quietly
                } else {
                    throw new ErrorResponseException($curl->response);
                }
            } catch (\Exception $exception) {
                $this->handleServiceError($service, $exception);
            }

            $curl->close();
        }

        return $this;
    }

    /**
     * Set transmission status to service
     */
    private function setTransmissionStatus(AbstractService $service, $type, $value)
    {
        if ('success' !== $type && 'error' !== $type) {
            throw new \InvalidArgumentException(sprintf('Unexpected type %s - Expected "success" or "error"', (string) $type));
        }

        $typeProperty = 'success' === $type ? 'successResponse' : 'errorException';

        $transmissionStatus = new ServiceTransmissionStatus();

        $status = & PropertyReader::newInstance()->read($transmissionStatus, 'status');
        $status = $type;
        
        $successResponse = & PropertyReader::newInstance()->read($transmissionStatus, $typeProperty);
        $successResponse = $exception;

        $serviceTransmissionStatus = & PropertyReader::newInstance()->read($service, 'transmissionStatus');
        $serviceTransmissionStatus = $transmissionStatus;
    }

    /**
     * An error occured while writing the service.
     * Use onError callback if set or throw the exception again.
     * 
     * @param AbstractService $service
     * @param \Exception $exception
     * 
     * @throws \Exception
     * 
     * @return void
     */
    private function handleServiceError(AbstractService $service, \Exception $exception)
    {
        $this->setTransmissionStatus($service, 'error', $exception);

        $errorCallback = $service->getErrorCallback();

        if (is_callable($errorCallback)) {
            call_user_func($errorCallback, $exception);
        } elseif ($exception instanceOf FatalExceptionInterface) {
            throw $exception;
        }

        // else - do nothing, swallow error quietly
    }

    /**
     * Create Curl instance with default settings
     * 
     * @return \Curl\Curl
     */
    protected function createCurlInstance()
    {
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setOpt(CURLOPT_FRESH_CONNECT, true);
        $curl->setBasicAuthentication($this->username, $this->password);

        return $curl;
    }

    /**
     * Clear services
     * 
     * @return AxirosAcsServiceApi
     */
    public function clearServices()
    {
        $this->services = [];

        return $this;
    }

    /**
     * Get services
     * 
     * @return array
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Add a service
     * 
     * @param AbstractService $service
     * 
     * @return AxirosAcsServiceApi
     */
    public function addService(AbstractService $service)
    {
        $this->services[$service->getUniqueName()] = $service;

        return $this;
    }

    /**
     * Remove a service
     * 
     * @param AbstractService $service
     * 
     * @return AxirosAcsServiceApi
     */
    public function removeService(AbstractService $service)
    {
        $type = $service->getUniqueName();

        if (isset($this->services[$type])) {
            unset($this->services[$type]);
        }

        return $this;
    }
}
