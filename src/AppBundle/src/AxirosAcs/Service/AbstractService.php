<?php

/**
 *
 */

namespace AppBundle\src\AxirosAcs\Service;

/**
 * Abstract class for services
 */
abstract class AbstractService
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * CpeId
     * 
     * @var string
     */
    protected $cpeId;

    /**
     * Success callback handler
     * 
     * @var \callable
     */
    protected $onSuccessCallback;

    /**
     * Error callback handler
     * 
     * @var \callable
     */
    protected $onErrorCallback;

    /**
     * Status of transmission
     * 
     * @var ServiceTransmissionStatus
     */
    protected $transmissionStatus;

    /**
     * Get type of this service
     * 
     * @return string
     */
    abstract public function getType();

    /**
     * Get action of this service
     * 
     * @return string
     */
    abstract public function getAction();

    /**
     * Get transmission status
     * 
     * @return ServiceTransmissionStatus
     */
    public function getTransmissionStatus()
    {
        return $this->transmissionStatus;
    }

    /**
     * Unique name for this service
     * 
     * @return string
     */
    public function getUniqueName()
    {
        return bin2hex(random_bytes(6)).'-'.__CLASS__;
    }

    /**
     * Set a success callback handler
     * 
     * @param \callable $callback
     * 
     * @return AbstractService
     */
    public function onSuccess(callable $callback)
    {
        $this->onSuccessCallback = $callback;

        return $this;
    }

    /**
     * Get on-success callback handler
     * 
     * @return \callable|null
     */
    public function getSuccessCallback()
    {
        return $this->onSuccessCallback;
    }

    /**
     * Set a error callback handler
     * 
     * @param \callable $callback
     * 
     * @return AbstractService
     */
    public function onError(callable $callback)
    {
        $this->onErrorCallback = $callback;

        return $this;
    }

    /**
     * Get on-error callback handler
     * 
     * @return \callable|null
     */
    public function getErrorCallback()
    {
        return $this->onErrorCallback;
    }

    /**
     * Get cpeId of this service
     * 
     * @return string
     */
    public function getCpeId()
    {
        return $this->cpeId;
    }

    /**
     * Set cpeId
     * 
     * @param string $cpeId
     * 
     * @return AbstractService
     */
    public function setCpeId($cpeId)
    {
        $this->cpeId = $cpeId;

        return $this;
    }

    /**
     * Get data
     * 
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}
