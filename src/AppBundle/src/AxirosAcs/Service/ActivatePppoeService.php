<?php

/**
 *
 */

namespace AppBundle\src\AxirosAcs\Service;

/**
 * Data class for Axiros-ACS activate PPPoE Service
 */
class ActivatePppoeService extends AbstractService
{
    use CommonServiceMethodsTrait;

    /**
     * Set default data
     */
    public function __construct()
    {
        $this->data = [
            'ServiceIdentifiers' => [],
            'CommandOptions' => [],
            'ServiceParameters' => [],
        ];
    }

    /**
     * Get type (name of the service as it is in acs)
     * 
     * @return string
     */
    public function getType()
    {
        return 'pppoe';
    }

    /**
     * Get action
     * 
     * @return string
     */
    public function getAction()
    {
        return 'activate';
    }

    /**
     * Convert this service into ModifyPppoeService
     * 
     * @return ModifyPppoeService
     */
    public function convertToModifyPppoeService()
    {
        $service = new ModifyPppoeService();
        $service
            ->setCpeId($this->cpeId)
            ->setServiceIdentifiers($this->data['ServiceIdentifiers'])
            ->setCommandOptions($this->data['CommandOptions'])
            ->setServiceParameters($this->data['ServiceParameters']);

        return $service;
    }
}
