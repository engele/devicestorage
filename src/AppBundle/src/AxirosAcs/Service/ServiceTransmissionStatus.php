<?php

/**
 *
 */

namespace AppBundle\src\AxirosAcs\Service;

/**
 * Class for services transmission status
 */
class ServiceTransmissionStatus
{
    /**
     * Status of transmission.
     * "null" until after transmission - "success" or "error" afterwards
     * 
     * @var string|null
     */
    protected $status;

    /**
     * If there was an error, this will hold the exception
     * 
     * @var \Exception|null
     */
    protected $errorException;

    /**
     * If it was successful, this will hold the curl response class
     * 
     * @var \stdClass|null
     */
    protected $successResponse;

    /**
     * Get status
     * 
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get error exception
     * 
     * @return \Exception|null
     */
    public function getErrorException()
    {
        return $this->errorException;
    }

    /**
     * Get success response
     * 
     * @return \stdClass|null
     */
    public function getSuccessResponse()
    {
        return $this->successResponse;
    }
}
