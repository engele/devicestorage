<?php

/**
 *
 */

namespace AppBundle\src\AxirosAcs\Service;

/**
 * Trait for Axiros-ACS services
 */
trait CommonServiceMethodsTrait
{
    /**
     * Remove ServiceParameter
     * 
     * @param string $serviceParameter
     * 
     * @return ActivatePppoeService
     */
    public function removeServiceParameter($serviceParameter)
    {
        unset($this->data['ServiceParameters'][$serviceParameter]);

        return $this;
    }

    /**
     * Remove CommandOption
     * 
     * @param string $commandOption
     * 
     * @return ActivatePppoeService
     */
    public function removeCommandOption($commandOption)
    {
        unset($this->data['CommandOptions'][$commandOption]);

        return $this;
    }

    /**
     * Remove ServiceIdentifier
     * 
     * @param string $serviceIdentifier
     * 
     * @return ActivatePppoeService
     */
    public function removeServiceIdentifier($serviceIdentifier)
    {
        unset($this->data['ServiceIdentifiers'][$serviceIdentifier]);

        return $this;
    }

    /**
     * Set ServiceParameter
     * 
     * @param string $serviceParameter
     * @param mixed $value
     * 
     * @return ActivatePppoeService
     */
    public function setServiceParameter($serviceParameter, $value)
    {
        $this->data['ServiceParameters'][$serviceParameter] = $value;

        return $this;
    }

    /**
     * Set CommandOption
     * 
     * @param string $commandOption
     * @param mixed $value
     * 
     * @return ActivatePppoeService
     */
    public function setCommandOption($commandOption, $value)
    {
        $this->data['CommandOptions'][$commandOption] = $value;

        return $this;
    }

    /**
     * Set ServiceIdentifier
     * 
     * @param string $serviceIdentifier
     * @param mixed $value
     * 
     * @return ActivatePppoeService
     */
    public function setServiceIdentifier($serviceIdentifier, $value)
    {
        $this->data['ServiceIdentifiers'][$serviceIdentifier] = $value;

        return $this;
    }

    /**
     * Set ServiceParameters
     * 
     * @param array $serviceParameters
     * 
     * @return ActivatePppoeService
     */
    public function setServiceParameters(array $serviceParameters)
    {
        $this->data['ServiceParameters'] = $serviceParameters;

        return $this;
    }

    /**
     * Set CommandOptions
     * 
     * @param array $commandOptions
     * 
     * @return ActivatePppoeService
     */
    public function setCommandOptions(array $commandOptions)
    {
        $this->data['CommandOptions'] = $commandOptions;

        return $this;
    }

    /**
     * Set ServiceIdentifiers
     * 
     * @param array $serviceIdentifiers
     * 
     * @return ActivatePppoeService
     */
    public function setServiceIdentifiers(array $serviceIdentifiers)
    {
        $this->data['ServiceIdentifiers'] = $serviceIdentifiers;

        return $this;
    }

    /**
     * Get ServiceParameter
     * 
     * @param string $serviceParameter
     * 
     * @return mixed
     */
    public function getServiceParameter($serviceParameter)
    {
        if (isset($this->data['ServiceParameters'][$serviceParameter])) {
            return $this->data['ServiceParameters'][$serviceParameter];
        }

        return null;
    }

    /**
     * Get CommandOption
     * 
     * @param string $commandOption
     * 
     * @return mixed
     */
    public function getCommandOption($commandOption)
    {
        if (isset($this->data['CommandOptions'][$commandOption])) {
            return $this->data['CommandOptions'][$commandOption];
        }

        return null;
    }

    /**
     * Get ServiceIdentifier
     * 
     * @param string $serviceIdentifier
     * 
     * @return mixed
     */
    public function getServiceIdentifier($serviceIdentifier)
    {
        if (isset($this->data['ServiceIdentifiers'][$serviceIdentifier])) {
            return $this->data['ServiceIdentifiers'][$serviceIdentifier];
        }

        return null;
    }

    /**
     * Get ServiceParameters
     * 
     * @return array
     */
    public function getServiceParameters()
    {
        return $this->data['ServiceParameters'];
    }

    /**
     * Get CommandOptions
     * 
     * @return array
     */
    public function getCommandOptions()
    {
        return $this->data['CommandOptions'];
    }

    /**
     * Get ServiceIdentifiers
     * 
     * @return array
     */
    public function getServiceIdentifiers()
    {
        return $this->data['ServiceIdentifiers'];
    }
}
