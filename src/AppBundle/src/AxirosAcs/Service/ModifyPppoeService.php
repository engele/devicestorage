<?php

/**
 *
 */

namespace AppBundle\src\AxirosAcs\Service;

/**
 * Data class for Axiros-ACS modify PPPoE Service
 */
class ModifyPppoeService extends AbstractService
{
    use CommonServiceMethodsTrait;

    /**
     * Set default data
     */
    public function __construct()
    {
        $this->data = [
            'ServiceIdentifiers' => [],
            'CommandOptions' => [],
            'ServiceParameters' => [],
        ];
    }

    /**
     * Get type (name of the service as it is in acs)
     * 
     * @return string
     */
    public function getType()
    {
        return 'pppoe';
    }

    /**
     * Get action
     * 
     * @return string
     */
    public function getAction()
    {
        return 'modify';
    }
}
