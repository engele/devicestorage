<?php

namespace AppBundle\src\OtrsAdapter\Entity;


class RequestParamsTicketCreate
{
    /**
     * @var string
     */
    public $subject;

    /**
     * @var string
     */
    public $body;

    /**
     * @var int
     */
    public $queueId;

    /**
     * @var string
     */
    public $customerUser;

    /**
     * @var string
     */
    public $customerId;

    /**
     * @var int
     */
    public $ownerId;

    /**
     * @var int
     */
    public $responsibleId;

    /**
     * RequestParamsTicketCreate constructor.
     * @param string $subject
     * @param string $body
     * @param int $queueId
     * @param string $customerUser
     * @param string $customerId
     * @param int $ownerId
     * @param int $responsibleId
     */
    public function __construct( $subject = null,  $body = null,  $queueId = null,  $customerUser = null,  $customerId = null,  $ownerId = null,  $responsibleId = null)
    {
        $this->subject = $subject;
        $this->body = $body;
        $this->queueId = $queueId;
        $this->customerUser = $customerUser;
        $this->customerId = $customerId;
        $this->ownerId = $ownerId;
        $this->responsibleId = $responsibleId;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return mixed
     */
    public function getQueueId()
    {
        return $this->queueId;
    }

    /**
     * @return mixed
     */
    public function getCustomerUser()
    {
        return $this->customerUser;
    }

    /**
     * @return string
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @return mixed
     */
    public function getOwnerId()
    {
        return $this->ownerId;
    }

    /**
     * @return int
     */
    public function getResponsibleId()
    {
        return $this->responsibleId;
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @param string $body
     */
    public function setBody(string $body): void
    {
        $this->body = $body;
    }

    /**
     * @param int $queueId
     */
    public function setQueueId(int $queueId): void
    {
        $this->queueId = $queueId;
    }

    /**
     * @param string $customerUser
     */
    public function setCustomerUser(string $customerUser): void
    {
        $this->customerUser = $customerUser;
    }

    /**
     * @param string $customerId
     */
    public function setCustomerId(string $customerId): void
    {
        $this->customerId = $customerId;
    }

    /**
     * @param int $ownerId
     */
    public function setOwnerId(int $ownerId): void
    {
        $this->ownerId = $ownerId;
    }

    /**
     * @param int $responsibleId
     */
    public function setResponsibleId(int $responsibleId): void
    {
        $this->responsibleId = $responsibleId;
    }
}