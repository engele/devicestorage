<?php

namespace AppBundle\src\OtrsAdapter;


use AppBundle\src\OtrsAdapter\Entity\RequestParamsTicketCreate;
use OtrsRestClient\Entity\OtrsResult;

interface OtrsAdapterInterface
{
    /**
     * @param RequestParamsTicketCreate $requestParamsTicketCreate
     * @return OtrsResult
     */
    public function createTicket(RequestParamsTicketCreate $requestParamsTicketCreate);

    /**
     * @return array
     */
    public function getOtrsAdapterConfig(): array;

    /**
     * @return string
     */
    public function getTicketViewUrl();
}