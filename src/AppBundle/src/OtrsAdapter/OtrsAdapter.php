<?php

namespace AppBundle\src\OtrsAdapter;


use AppBundle\src\OtrsAdapter\Entity\RequestParamsTicketCreate;
use OtrsRestClient\Entity\OtrsRequiredConfigs;
use OtrsRestClient\Entity\OtrsTicket;
use OtrsRestClient\Entity\OtrsTicketArticle;
use OtrsRestClient\OtrsRestClient;


class OtrsAdapter implements OtrsAdapterInterface
{
    /**
     * @var OtrsRestClient
     */
    private $otrsClient;
    /**
     * @var array
     */
    private $otrsAdapterConfig;

    public function __construct($otrsAdapterConfig)
    {
        $this->otrsAdapterConfig = $otrsAdapterConfig;
        $this->otrsClient = $this->getOtrsClient();
    }

    public function createTicket(RequestParamsTicketCreate $requestParamsTicketCreate)
    {
        //todo check meaning/use of default-values

        $ticketArticleDefaultValues = $this->otrsAdapterConfig['ticketArticleDefaultValues'];
        $otrsTicketArticle = new OtrsTicketArticle(
            $requestParamsTicketCreate->getSubject(),
            $requestParamsTicketCreate->getBody(),
            $ticketArticleDefaultValues['contentType']
        );

        $ticketDefaultValues = $this->otrsAdapterConfig['ticketDefaultValues'];
        $otrsTicket = new OtrsTicket(
            $ticketDefaultValues['title'],
            $requestParamsTicketCreate->getQueueId(),
            $requestParamsTicketCreate->getCustomerUser(),
            $requestParamsTicketCreate->getCustomerId(),
            $ticketDefaultValues['state'],
            $ticketDefaultValues['lock'],
            $ticketDefaultValues['priority'],
            $requestParamsTicketCreate->getOwnerId(),
            $requestParamsTicketCreate->getResponsibleId(),
            $ticketDefaultValues['typeId'],
            $otrsTicketArticle,
            null
        );

        return $this->otrsClient->ticketCreate($otrsTicket);
    }

    /**
     * @return OtrsRestClient
     */
    private function getOtrsClient()
    {
        $otrsRequiredConfigs = new OtrsRequiredConfigs(
            $otrsHostName = $this->otrsAdapterConfig['otrsHostName'],
            $otrsUserLogin = $this->otrsAdapterConfig['otrsUserLogin'],
            $otrsPassword = $this->otrsAdapterConfig['otrsPassword'],
            $transferProtocolType = $this->otrsAdapterConfig['transferProtocolType'],
            $otrsServiceUrlSessionCreate = $this->otrsAdapterConfig['otrsServiceUrlSessionCreate'],
            $otrsServiceUrlTicketCreate = $this->otrsAdapterConfig['otrsServiceUrlTicketCreate'],
            $otrsServiceUrlTicketUpdate = $this->otrsAdapterConfig['otrsServiceUrlTicketUpdate'],
            $otrsServiceUrlTicketSearch = $this->otrsAdapterConfig['otrsServiceUrlTicketSearch'],
            $otrsServiceUrlTicketGet = $this->otrsAdapterConfig['otrsServiceUrlTicketGet'],
            $otrsServiceUrlTicketHistoryGet = $this->otrsAdapterConfig['otrsServiceUrlTicketHistoryGet']
        );

        return new OtrsRestClient($otrsRequiredConfigs, $this->otrsAdapterConfig['debugMode']);
    }

    public function getOtrsAdapterConfig(): array
    {
        return $this->otrsAdapterConfig;
    }

    public function getTicketViewUrl()
    {
        return $this->otrsAdapterConfig['transferProtocolType'].'://'.$this->otrsAdapterConfig['otrsHostName'].'/'.$this->otrsAdapterConfig['otrsTicketViewUrl'];
    }

    public function getOtrsAjaxControllerUrl()
    {
        return "/otrs/ticket-create";
    }
}