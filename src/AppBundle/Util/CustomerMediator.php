<?php

/**
 * 
 */

namespace AppBundle\Util;

use AppBundle\Entity\Customer;
use AppBundle\Entity\CustomerCrossSellingProduct;
use AppBundle\Entity\Product\CrossSellingProduct;
use Doctrine\Bundle\DoctrineBundle\Registry;
use AppBundle\Util\Configuration;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Annotations\AnnotationReader;
use ApiBundle\Util\CustomerMediator as ApiCustomerMediator;

/**
 * A data mediator for customer entity
 */
class CustomerMediator extends ApiCustomerMediator
{
    /**
     * Overwritten this method to avoid exceptions (DateTime::__construct(): Failed to parse time string (Kein Vertrag))
     */
    protected function setDateMediators()
    {
        $customer = $this->getCustomer();

        $annotationReader = new AnnotationReader();
        
        $reflectionObject = new \ReflectionObject($customer);

        foreach ($reflectionObject->getProperties() as $reflectionProperty) {
            $propertyAnnotations = $annotationReader->getPropertyAnnotations($reflectionProperty);

            if (!isset($propertyAnnotations[0])) {
                continue;
            }

            switch ($propertyAnnotations[0]->type) {
                case 'date':
                case 'datetime':
                    $this->setMediationCallback('set'.ucfirst($reflectionProperty->getName()), function ($string) {
                        if (empty($string)) {
                            return [ $string ];
                        }

                        try {
                            return [ new \DateTime($string) ];
                        } catch (\Exception $exception) {
                            // workaround to avoid exception
                            // instead of throwing the exception, just return the plain value
                            return [ $string ];
                        }
                    });

                    break;
            }
        }
    }
}
