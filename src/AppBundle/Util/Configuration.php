<?php

/**
 * This file is part of the wkv project.
 */

namespace AppBundle\Util;

require_once __DIR__.'/../../../web/vendor/wisotel/configuration/Configuration.php';

/**
 * To autoload Wisotel\Configuration\Configuration
 */
class Configuration extends \Wisotel\Configuration\Configuration
{
}
