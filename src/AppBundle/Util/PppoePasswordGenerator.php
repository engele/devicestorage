<?php

/**
 *
 */

namespace AppBundle\Util;

/**
 *
 */
class PppoePasswordGenerator
{
    /**
     * Generate pppoe password
     * 
     * @return string
     */
    public static function generatePassword() : string
    {
        $length = 10;
        
        // a "+" can be part of an url and we have to send the password via url-api to purtel (radius)
        // this leads to a " " (space) instead of a "+" in the password
        // therefore we simply don't use "+" anymore
        //$conso = array("b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "r", "s", "t", "v", "w", "x", "y", "z", "+", "$");
        $conso = array("b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "r", "s", "t", "v", "w", "x", "y", "z", "$");
        $vocal = array("a", "e", "i", "o", "u", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
        $password = "";

        srand((double) microtime() * 1000000);

        $max = $length / 2;

        for ($i = 1; $i <= $max; $i++) {
            $password .= $conso[rand(0, count($conso) - 1)];
            $password .= $vocal[rand(0, count($vocal) - 1)];
        }

        return $password;
    }
}
