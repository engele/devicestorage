<?php

/**
 *
 */

namespace AppBundle\Util;

/**
 *
 */
class UplinkProviderType
{
    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            'COLT' => 'colt',
            'Telia' => 'telia',
            'Unitymedia' => 'unitymeda',
            'Wisotel (intern)' => 'wisotel-intern',
            'Sparkassen-IT' => 'sparkassen-it',
        ];
    }
}
