<?php

/**
 *
 */

namespace AppBundle\Util;

/**
 *
 */
class AcsType
{
    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            'Purtel' => 'purtel',
            'Axiros' => 'zeag',
        ];
    }
}
