<?php

/**
 *
 */

namespace AppBundle\Util;

/**
 *
 */
class KvzSchaltnummerGenerator
{
    /**
     * 
     */
    public static function generate($lsaPin, $kvzPrefix) : string
    {
        $lsaPinString = str_pad($lsaPin, 4, "0", STR_PAD_LEFT);

        return $kvzPrefix . '-' . $lsaPinString[0] . $lsaPinString[1] . '-' . $lsaPinString[2] . $lsaPinString[3];
    }
}
