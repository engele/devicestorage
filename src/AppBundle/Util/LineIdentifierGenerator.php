<?php

/**
 *
 */

namespace AppBundle\Util;

use AppBundle\Entity\Location\Card;
use AppBundle\Entity\Location\CardPort;

/**
 *
 */
class LineIdentifierGenerator
{
    /**
     * Generate line identifier by strings
     * 
     * @param string $lineIdentifierPrefix
     * @param string $portNumber
     * @param string $separator
     * 
     * @return string
     */
    public static function generateByStrings(string $lineIdentifierPrefix, string $portNumber, string $separator = '/') : string
    {
        if ($separator !== substr($lineIdentifierPrefix, -1)) {
            $lineIdentifierPrefix .= $separator;
        }

        // $lineIdentifier = $lineIdentifierPrefix . str_pad($portNumber, 2, "0", STR_PAD_LEFT);
        $lineIdentifier = $lineIdentifierPrefix.$portNumber;

        if ($lineIdentifier === $separator) {
            return '';
        }

        return $lineIdentifier;
    }

    /**
     * Generate line identifier
     * 
     * @param AppBundle\Entity\Location\Card|string $lineIdentifierPrefix
     * @param AppBundle\Entity\Location\CardPort|string $portNumber
     * @param string $separator
     * 
     * @return string
     */
    public static function generate($lineIdentifierPrefix, $portNumber, string $separator = '/') : string
    {
        if ($portNumber instanceof CardPort) {
            return static::generateByCardPort($portNumber, $separator);
        }

        if ($lineIdentifierPrefix instanceof Card) {
            return static::generateByCardAndPortNumber($lineIdentifierPrefix, $portNumber, $separator);
        }

        return static::generateByStrings($lineIdentifierPrefix, $portNumber, $separator);
    }

    /**
     * Generate line identifier by card and port number
     * 
     * @param AppBundle\Entity\Location\Card $card
     * @param int|string $portNumber
     * @param string $separator
     * 
     * @return string
     */
    public static function generateByCardAndPortNumber(Card $card, $portNumber, string $separator = '/') : string
    {
        return static::generateByStrings($card->getLineIdentifierPrefix(), $portNumber, $separator);
    }

    /**
     * Generate line identifier by card port
     * 
     * @param AppBundle\Entity\Location\CardPort $cardPort
     * @param string $separator
     * 
     * @return string
     */
    public static function generateByCardPort(CardPort $cardPort, string $separator = '/') : string
    {
        return static::generateByStrings(
            $cardPort->getCard()->getLineIdentifierPrefix(),
            $cardPort->getNumber(),
            $separator
        );
    }
}
