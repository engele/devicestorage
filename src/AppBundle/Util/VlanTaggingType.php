<?php

/**
 *
 */

namespace AppBundle\Util;

/**
 *
 */
class VlanTaggingType
{
    /**
     * @return array
     */
    public static function getTypes()
    {
        // Values = values
        return [
            'single-tagged' => 'single-tagged',
            'double-tagged' => 'double-tagged',
            'priority-tagged' => 'priority-tagged',
            'untagged' => 'untagged',
        ];
    }
}
