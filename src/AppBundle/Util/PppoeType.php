<?php

/**
 *
 */

namespace AppBundle\Util;

/**
 *
 */
class PppoeType
{
    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            'MikroTik' => 'mikrotik',
            'Bartels' => 'bartels',
            'Purtel-Radius' => 'purtel-radius',
            'Provisioning-Server' => 'provisioning-server',
            'Static IP (kein PPPoE)' => 'static-ip-no-pppoe',
        ];
    }
}
