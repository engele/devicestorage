<?php

/**
 *
 */

namespace AppBundle\Util;

/**
 *
 */
class DslamType
{
    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            'ALU' => 'ALU',
            'KEYMILE' => 'KEYMILE',
            'ISKRATEL' => 'ISKRATEL',
            'Huawei' => 'huawei',
            'Unselbstst&auml;ndige MDU' => 'dependent-mdu',
        ];
    }
}
