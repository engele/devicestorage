<?php

/*
 *
 */

namespace AppBundle\Twig;

/**
 * 
 */
class AppExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('url_decode', 'urldecode'),
        );
    }
}
