<?php

/**
 * 
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use customer\Lib\IpAddress\IpRangeCollection;
//require_once __DIR__.'/../../../web/customer/Lib/IpAddress/IpRangeCollection.php';

/**
 * @Route("/customer/{customerId}/letter")
 */
class CustomerLetterController extends Controller
{    
    /**
     * Get customer-data from database by id
     * 
     * @param integer $customerId
     * 
     * @return array|null
     */
    protected function getCustomerById($customerId)
    {
        $customerId = (int) $customerId;

        $db = $this->getDoctrine()->getManager()->getConnection();

        $stmt = $db->prepare("SELECT * FROM `customers` WHERE id = ?");
        $stmt->bindValue(1, $customerId);
        $stmt->execute();

        if ($stmt->rowCount() == 1) {
            return $stmt->fetch();
        }

        return null;
    }

    /**
     * Get network-data from database by id
     * 
     * @param integer $networkId
     * 
     * @throws \UnexpectedValueException
     * 
     * @return array
     */
    protected function getNetworkData($networkId)
    {
        $networkId = (int) $networkId;

        $db = $this->getDoctrine()->getManager()->getConnection();

        $stmt = $db->prepare("SELECT * FROM `networks` WHERE id = ?");
        $stmt->bindValue(1, $networkId);
        $stmt->execute();

        if ($stmt->rowCount() == 1) {
            return $stmt->fetch();
        }

        throw new \UnexpectedValueException(sprintf('No network with id %d exists', $networkId));
    }

    /**
     * Make fileName a save fileName (only alphanumerics)
     * 
     * @param string $fileName
     * 
     * @return string
     */
    public function makeSaveFileName($fileName)
    {
        return preg_replace('/[^0-9a-z]/i', '-', $fileName);
    }

    /**
     * Add relevant data to customer (like purtelAccount, ..)
     * 
     * @param array $customer
     * 
     * @return array
     */
    private function addRelevantDataToCustomer(& $customer)
    {
        require_once $this->get('kernel')->getRootDir().'/../web/customer/Lib/PppoeUsername/PppoeUsername.php';

        $db = $this->getDoctrine()->getManager()->getConnection();

        // fetch purtel accounts
        $stmt = $db->prepare("SELECT * FROM `purtel_account` WHERE `cust_id` = ?");
        $stmt->bindValue(1, $customer['id']);
        $stmt->execute();

        if ($stmt->rowCount() < 1) {
            throw new \UnexpectedValueException('customer has no purtel accounts');
        }

        $customer['purtelAccounts'] = $stmt->fetchAll();

        foreach ($customer['purtelAccounts'] as $purtelAccount) {
            if ('1' === $purtelAccount['master']) {
                $customer['purtelMasterAccount'] = $purtelAccount;

                break;
            }
        }

        if (!isset($customer['purtelMasterAccount'])) {
            throw new \UnexpectedValueException('customer has no master purtel account');
        }

        // set name for purtel_product
        if (!empty($customer['purtel_product'])) {
            $stmt = $db->prepare("SELECT `produkt_bezeichnung` FROM `produkte` WHERE `produkt` = ?");
            $stmt->bindValue(1, $customer['purtel_product']);
            $stmt->execute();

            if ($stmt->rowCount() < 1) {
                throw new \UnexpectedValueException('customer has invalid purtel product');
            }

            $customer['purtel_product'] = $stmt->fetch();
            $customer['purtel_product'] = $customer['purtel_product']['produkt_bezeichnung'];
        }

        // set name for productname
        if (!empty($customer['productname'])) {
            $stmt = $db->prepare("SELECT `produkt_bezeichnung` FROM `produkte` WHERE `produkt` = ?");
            $stmt->bindValue(1, $customer['productname']);
            $stmt->execute();

            if ($stmt->rowCount() < 1) {
                throw new \UnexpectedValueException('customer has invalid productname');
            }

            $customer['productname'] = $stmt->fetch();
            $customer['productname'] = $customer['productname']['produkt_bezeichnung'];
        }

        // fetch multiple ip addresses
        $customer['multipleIpAdresses'] = [];
        
        $stmt = $db->prepare("SELECT * FROM `ip_addresses` WHERE `customer_id` = ?");
        $stmt->bindValue(1, $customer['id']);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $customer['multipleIpAdresses'] = $stmt->fetchAll();
        }        

        $stmt = $db->prepare("SELECT * FROM `ip_ranges` WHERE id = ?");
        $stmt->bindValue(1, $customer['ip_range_id']);
        $stmt->execute();

        $customer['primaryIpGateway'] = null; // primary ip gateway
        $customer['primaryIpSubnetmask'] = null; // primary ip subnetmask
        $customer['primaryIpDns1'] = null; // primary ip dns1
        $customer['primaryIpDns2'] = null; // primary ip dns2
        $customer['primaryIpRangePppoeType'] = null; // primary ip-range pppoeType

        if ($stmt->rowCount() === 1) {
            $ipRange = $stmt->fetch();

            $customer['primaryIpGateway'] = $ipRange['gateway']; // primary ip gateway
            $customer['primaryIpSubnetmask'] = $ipRange['subnetmask']; // primary ip subnetmask
            $customer['primaryIpDns1'] = $ipRange['dns1']; // primary ip dns1
            $customer['primaryIpDns2'] = $ipRange['dns2']; // primary ip dns2
            $customer['primaryIpRangePppoeType'] = $ipRange['pppoe_type']; // primary ip-range pppoeType
        }

        // PPPoE username
        $customer['pppoeUsername'] = \PppoeUsername::create($customer['pppoe_pin'], $customer['clientid']);

        // cross-selling products
        $customer['crossSellingProducts'] = null;

        $stmt = $db->prepare("SELECT co.*, o.`option_bezeichnung` as name FROM `customer_options` co INNER JOIN `optionen` o ON o.`option` = co.`opt_id` WHERE co.`cust_id` = ?");
        $stmt->bindValue(1, $customer['id']);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $customer['crossSellingProducts'] = $stmt->fetchAll();
        }

        return $customer;
    }

    /**
     * @param array $pages
     * @param array $data
     * 
     * @return 
     */
    public function outputAsPdf($pages, $data)
    {
        $pdf = new \AppBundle\src\TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator($data['companyName']);
        $pdf->SetAuthor($data['companyName']);
        $pdf->SetTitle($data['letterTemplate']['documentTitle']);
        $pdf->SetSubject($data['letterTemplate']['documentTitle']);
        
        // set header and footer fonts
        $pdf->setHeaderFont([
            //PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN
            'helvetica', '', 12
        ]);
        $pdf->setFooterFont([
            //PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA
            'helvetica', '', 12
        ]);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(22, 10, 15);
        $pdf->SetHeaderMargin(0);
        $pdf->SetFooterMargin(0);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 0);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set font
        //$pdf->SetFont('helvetica', '', 10);
        $pdf->SetFont('helvetica', '', 12);
        $pdf->AddFont('delicious', '', 12);

        foreach ($pages as $file) {
            // add a page
            $pdf->AddPage();

            $data['documentTitle'] = $data['letterTemplate']['documentTitle'];

            $html = $this->renderView($file, $data);

            // output the HTML content
            $pdf->writeHTML($html, true, false, true, false, '');

            $pdf->SetY(-40);
        }

        $pdf->lastPage();

        $now = new \DateTime();
        
        // datum-documentTitle-vorname-nachname-clientid.pdf
        $outputFileName = sprintf('%s-%s-%s-%s-%s.pdf',
            $now->format('d.m.Y'),
            $this->makeSaveFileName($data['letterTemplate']['documentTitle']),
            $this->makeSaveFileName($data['customer']['firstname']),
            $this->makeSaveFileName($data['customer']['lastname']),
            $data['customer']['cientid']
        );

        return new Response(
            $pdf->Output($outputFileName, 'S'),
            200,
            [
                'Content-Type' => 'application/pdf',
                'Cache-Control' => 'private, must-revalidate, post-check=0, pre-check=0, max-age=1',
                'Pragma' => 'public, must-revalidate, max-age=0',
                'Expires' => 'Sat, 26 Jul 1997 05:00:00 GMT',
                'Last-Modified' => gmdate('D, d M Y H:i:s').' GMT',
                'Content-Disposition' => 'inline; filename="'.basename($outputFileName).'"',
            ]
        );
    }

    /**
     * 
     */
    public function outputAsHtml($pages, $data)
    {
        $html = '';

        $data['documentTitle'] = $data['letterTemplate']['documentTitle'];

        foreach ($pages as $file) {
            $html .= $this->renderView($file, $data);
        }

        return new Response($html);
    }

    /**
     * 
     */
    public function outputAsRtf($pages, $data)
    {
        $now = new \DateTime();

        // for compatibility reasons
        // someday, the letter templates should be converted into twig
        $placeholderReplacements = [
            '#address#' => $data['customer']['title'] == 'Herr' ? "Sehr geehrter" : "Sehr geehrte",
            '#title#' => $data['customer']['title'],
            '#firstname#' => $data['customer']['firstname'],
            '#lastname#' => $data['customer']['lastname'],
            '#company#' => $data['customer']['company'],
            '#street#' => $data['customer']['street'],
            '#streetno#' => $data['customer']['streetno'],
            '#zipcode#' => $data['customer']['zipcode'],
            '#city#' => $data['customer']['city'],
            '#Date#' => $now->format('d.m.Y'),
            '#tal_order_ack_date#' => $data['customer']['tal_order_ack_date'],
            '#clientid#' => $data['customer']['clientid'],
            '#password#' => $data['customer']['password'],
            '#phoneareacode#' => $data['customer']['phoneareacode'],
            '#phonenumber#' => $data['customer']['phonenumber'],
            '#purtel_login#' => $data['customer']['purtelMasterAccount']['purtel_login'],
            '#purtel_passwort#' => $data['customer']['purtelMasterAccount']['purtel_password'],
            '#purtel_phone#' => $data['customer']['purtelMasterAccount']['nummer'],
            '#terminal_type#' => $data['customer']['terminal_type'],
            '#terminal_serialno#' => $data['customer']['terminal_serialno'],
            '#remote_login#' => $data['customer']['remote_login'],
            '#remote_password#' => $data['customer']['remote_password'],
            '#mac_address#' => $data['customer']['mac_address'],
            '#ip_address#' => $data['customer']['ip_address'],
            '#subnetmask#' => $data['customer']['subnetmask'],
            '#mobilephone#' => $data['customer']['mobilephone'],
            '#patch_date#' => $data['customer']['patch_date'],
            '#kvz_name#' => $data['customer']['kvz_name'],
            '#add_phone_lines#' => $data['customer']['add_phone_lines'],
            '#kvz_toggle_no#' => $data['customer']['kvz_toggle_no'],
            '#new_or_ported_phonenumbers#' => $data['customer']['new_or_ported_phonenumbers'],
            '#lsa_pin#' => $data['customer']['lsa_pin'],
            '#dslam_card_no#' => $data['customer']['dslam_card_no'],
            '#dslam_port#' => $data['customer']['dslam_port'],
            '#new_numbers_text#' => $data['customer']['new_numbers_text'],
            '#reached_downstream#' => $data['customer']['reached_downstream'],
            '#tal_product#' => $data['customer']['tal_product'],
            '#reached_upstream#' => $data['customer']['reached_upstream'],
            '#zusatzaufwand_ja#' => '',
            '#hoursofwork#' => '',
            '#productname#' => $data['customer']['productname'],
        ];

        foreach ($placeholderReplacements as $key => $value) {
            $placeholderReplacements[$key] = mb_convert_encoding($value, 'ISO-8859-1');
        }

        // might change it to:  return new Response(string);  ?
        $stream = fopen('php://temp', 'r+');

        foreach ($pages as $file) {
            fwrite($stream, str_replace(
                array_keys($placeholderReplacements),
                $placeholderReplacements,
                file_get_contents($file)
            ));
        }

        rewind($stream); // reset pointer to beginning of the stream

        return new \Symfony\Component\HttpFoundation\StreamedResponse(
            function () use ($stream) {
                echo stream_get_contents($stream);
            },
            200,
            [
                'Content-Type' => mime_content_type($file),
                'Content-Disposition' => 'attachment; filename="'.$data['letterTemplate']['file'].'"',
            ]
        );
    }

    /**
     * @Route("/create/{slug}", name="customer-letter-create")
     */
    public function indexAction(Request $request, $customerId, $slug)
    {
        $letterTemplates = $this->container->getParameter('customer_letters');

        if (!isset($letterTemplates[$slug])) {
            throw new NotFoundHttpException('Customer letter not found');
        }

        $customer = $this->getCustomerById($customerId);

        if (null === $customer) {
            throw new NotFoundHttpException('Page not found');
        }

        try {
            $this->addRelevantDataToCustomer($customer);

            $network = $this->getNetworkData($customer['network_id']);
            $card = $this->getDoctrine()->getRepository('AppBundle\Entity\Location\Card')->findOneById($customer['card_id']);
        } catch (\UnexpectedValueException $exception) {
            // end with error message

            return $this->render('letter-templates/add-relevant-data-exception.html.twig', [
                'exception' => $exception
            ]);
        }

        require_once $this->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';

        $companyName = \Wisotel\Configuration\Configuration::get('companyName');

        $letterTemplate = $letterTemplates[$slug];

        $data = [
            'companyName' => $companyName,
            'letterTemplate' => $letterTemplate,
            'customer' => $customer,
            'network' => $network,
            'card' => $card,
        ];

        $pages = [];

        if (isset($letterTemplate['file'])) {
            $fileEnding = explode('.', $letterTemplate['file']);
            $fileEnding = end($fileEnding);
            
            if ('twig' == $fileEnding) {
                $pages[] = 'letter-templates/'.$letterTemplate['file'];
            } else {
                $file = $this->get('kernel')->getRootDir().'/../web/customer/_files/'.$letterTemplate['file'];

                if (!file_exists($file) || !is_readable($file)) {
                    throw new \InvalidArgumentException(sprintf('Can not open file %s for reading',
                        $file
                    ));
                }

                return $this->outputAsRtf([$file], $data);
            }
        } elseif (isset($letterTemplate['dir'])) {
            $dir = $this->get('kernel')->getRootDir().'/../app/Resources/views/letter-templates/'.$letterTemplate['dir'];

            if (!is_dir($dir)) {
                throw new NotFoundHttpException('Customer letter directory not found');
            }

            $files = scandir($dir, SCANDIR_SORT_ASCENDING);
            unset($files[0], $files[1]);

            natsort($files);

            if ('/' !== substr($letterTemplate['dir'], -1)) {
                $letterTemplate['dir'] .= '/';
            }

            foreach ($files as $filename) {
                $pages[] = 'letter-templates/'.$letterTemplate['dir'].$filename;
            }
        } else {
            throw new NotFoundHttpException('Customer letter templates not defined - (file or dir)');
        }

        return $this->outputAsPdf($pages, $data);
    }
}
