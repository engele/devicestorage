<?php

/**
 *
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * BaseController to overwrite render function to add some global variables to php templates
 */
abstract class BaseController extends Controller
{
    /**
     * Returns a rendered view.
     *
     * @param string $view       The view name
     * @param array  $parameters An array of parameters to pass to the view
     *
     * @return string The rendered view
     */
    protected function renderView($view, array $parameters = array())
    {
        $parameters = $this->addAdditionalVariablesToParameters($parameters);

        return parent::renderView($view, $parameters);
    }

    /**
     * Renders a view.
     *
     * @param string   $view       The view name
     * @param array    $parameters An array of parameters to pass to the view
     * @param Response $response   A response instance
     *
     * @return Response A Response instance
     */
    protected function render($view, array $parameters = array(), Response $response = null)
    {
        $parameters = $this->addAdditionalVariablesToParameters($parameters);

        return parent::render($view, $parameters, $response);
    }

    /**
     * This add array-elements to be inserted as global variables to all templates
     * 
     * @param array $parameters An array of parameters to pass to the view
     * 
     * @return array
     */
    private function addAdditionalVariablesToParameters(array $parameters = array())
    {
        if (!array_key_exists('menus', $parameters)) {
            $parameters['menus'] = $this->get('menu_collection');
        }

        return $parameters;
    }
}
