<?php

/**
 * 
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Exception\RedirectException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * 
 */
class AccountingController extends BaseController
{
    /**
     * Accounting history
     * 
     * @Route("/accounting/accounting-history.php", name="accounting-history")
     */
    public function accountingHistoryAction()
    {
        $this->denyAccessUnlessGranted('ROLE_VIEW_ACCOUNTANCY');

        $db = $this->getDoctrine()->getManager()->getConnection();

        require $this->get('kernel')->getRootDir().'/../web/accounting/_conf/database.inc';

        $filterReporting = [];

        if (isset($_GET['filter-history-reporting']) && !empty($_GET['filter-history-reporting'])) {
            $filterReporting = json_decode($_GET['filter-history-reporting'], true);
        }

        $dateFormatter = new \IntlDateFormatter(
            null, // should be defined in php.ini
            \IntlDateFormatter::FULL,
            \IntlDateFormatter::FULL,
            null, // should be defined in php.ini
            \IntlDateFormatter::GREGORIAN,
            'MMMM yyyy'
        );

        $period = \DateTime::createFromFormat('U', $dateFormatter->parse($_GET['accountingDate']));
        $period->add(new \DateInterval('P1D')); // add plus 1 day to avoid problems with daylight saving

        $toDate = $period->format('Y-m-t 23:59:59');
        $fromDate = $period->format('Y-m-01');
        $periode = $period->format('Ym');

        $headline = array();

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=History_'.$periode.'.csv');  

        $historyEventPrices = [];
        
        $db_event = $db->query($sql['event']);

        while ($temp = $db_event->fetch(\PDO::FETCH_ASSOC)){
            $hist[$temp['event']] = NULL;
            $historyEventPrices[$temp['event']] = $temp['price_net'];
        }

        ksort($hist);

        $db_net = $db->query($sql['networks']);
        
        while ($temp = $db_net->fetch(\PDO::FETCH_ASSOC)){
            if ($temp['id_string'] != 'XXXXXX' && $temp['id_string'] != 'YYYYYY') {
                array_push($headline, $temp['id_string']);  
                
                foreach ($hist as $key => $value) {
                    $hist[$key][$temp['id_string']] = 0;
                }

                $hist['Summe'][$temp['id_string']] = 0;
            }
        }

        asort($headline);
        array_unshift($headline, 'Aktion');
        array_unshift($headline, 'Aktions-Preis');
        array_push($headline, 'Summe');

        $query = "SELECT
            ch.*, 
            n.`id_string`,
            che.`event`
        FROM `customer_history` ch
        INNER JOIN `customers` c ON c.`id` = ch.`customer_id`
        INNER JOIN `networks` n ON n.`id` = c.`network_id`
        INNER JOIN `customer_history_events` che ON che.`id` = ch.`event_id`
        WHERE
            ch.`created_at` BETWEEN ? AND ?";

        $db_hist = $db->prepare($query);
        $db_hist->execute([$fromDate, $toDate]);
        $db_hist_result = $db_hist;

        while ($temp = $db_hist_result->fetch(\PDO::FETCH_ASSOC)) {
            if (!in_array($temp['created_by'], $filterReporting)) {
                continue;
            }

            if (!isset($hist[$temp['event']][$temp['id_string']])) {
                $hist[$temp['event']][$temp['id_string']] = 0;
            }

            $hist[$temp['event']][$temp['id_string']]++;

            if ($temp['event'] != 'Dokumentation') {
                $hist['Summe'][$temp['id_string']]++;
            }
        }

        $csv_handle = fopen('php://output', 'w');

        fputcsv($csv_handle, $headline, ';');

        foreach ($hist as $key => $value) {
            $csv = [
                str_replace('.', ',', $historyEventPrices[$key]),
                $key
            ];

            ksort($value);
            
            $sum = 0;
            
            foreach ($value as $count) {
                array_push($csv, $count);
                
                $sum += $count;
            }

            array_push($csv, $sum);

            fputcsv($csv_handle, array_map('utf8_decode', $csv), ';');
        }

        fclose ($csv_handle);

        exit;
    }

    /**
     * Accounting Bill (Rechnungsliste)
     * 
     * @Route("/accounting/accounting-bill.php", name="accounting-bill")
     */
    public function accountingBillAction()
    {
        require $this->get('kernel')->getRootDir().'/../web/accounting/accountingBill.php';

        exit;
    }

    /**
     * Accounting Bill SAP coded (Rechnungsliste)
     * 
     * @Route("/accounting/accounting-bill-sap.php", name="accounting-bill-sap")
     */
    public function accountingBillSapAction()
    {
         require $this->get('kernel')->getRootDir().'/../web/accounting/accountingBillSap.php';

        exit;
       
    }

    /**
     * Accounting Bill Endica coded (Rechnungsliste)
     * 
     * @Route("/accounting/accounting-bill-endica.php", name="accounting-bill-endica")
     */
    public function accountingBillEndicaAction()
    {
        require $this->get('kernel')->getRootDir().'/../web/accounting/accountingBillEndica.php';

        exit;
    }

    /**
     * Accounting Account (Buchungsliste)
     * 
     * @Route("/accounting/accounting-account.php", name="accounting-account")
     */
    public function accountingAccountAction()
    {
        require $this->get('kernel')->getRootDir().'/../web/accounting/accountingAccount.php';

        exit;
    }

    /**
     * Accounting CheckSepa (Sepa Check)
     * 
     * @Route("/accounting/accounting-check-sepa.php", name="accounting-check-sepa")
     */
    public function accountingCheckSepaAction()
    {
        require $this->get('kernel')->getRootDir().'/../web/accounting/accountingCheckSepa.php';

        exit;
    }

    /**
     * Accounting Check (Check Sepa und Rechung)
     * 
     * @Route("/accounting/accounting-check.php", name="accounting-check")
     */
    public function accountingCheckAction()
    {
        require $this->get('kernel')->getRootDir().'/../web/accounting/accountingCheck.php';

        exit;
    }
}
