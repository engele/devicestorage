<?php

/**
 * 
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\BulkMailerType;
use AppBundle\Service\BackgroundProcessConnector\Command;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * @Route("/bulkmailer")
 * 
 * @todo
 *  create directory below var/ to store temporary files - don't use /tmp/ !
 * 
 * %kernel.project_dir%
 */
class BulkMailerController extends BaseController
{
    /**
     * @var boolean
     */
    private $commandCreated = false;

    /**
     * @var string
     */
    private $textMessageFile = null;

    /**
     * @var string
     */
    private $addressFile = null;

    /**
     * Files get uploaded to this folder.
     * Value gets set in __construct
     * 
     * @var string
     */
    protected $filesDir = '';

    /**
     * Constructor
     */
    public function __construct($bulkmailerFilesDir)
    {
        $this->filesDir = $bulkmailerFilesDir;

        $fileSystem = new Filesystem();

        $fileSystem->mkdir($this->filesDir);
    }

    /**
     * Destructor
     */
    public function __destruct()
    {
        if (!$this->commandCreated) {
            unlink($this->textMessageFile);
            unlink($this->addressFile);
        }
    }

    /**
     * @Route("/", name="bulkmailer")
     * 
     * @todo
     *      Verify uploaded file before use
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(BulkMailerType::class, []);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->textMessageFile = $this->filesDir.'/'.uniqid().'.html';

                $formData = $form->getData();
                $formData['messageFile'] = $this->textMessageFile;

                $formData['emails'] = array_map('trim', explode(',', $formData['emails']));

                $invalidEmail = false;

                foreach ($formData['emails'] as $emailAddress) {
                    if (false == filter_var($emailAddress, FILTER_VALIDATE_EMAIL)) {
                        $invalidEmail = true;

                        $form->get('emails')->addError(
                            new FormError('Invalid email-address')
                        );

                        $request->getSession()->getFlashBag()->add('error', 'Invalid email-address');

                        break;
                    }
                }

                if (!$invalidEmail) {
                    $htmlWrap = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>{SUBJECT}</title>
    </head>
    <body>{BODY}</body>
</html>
';

                    $formData['messageText'] = str_replace(
                        ['{BODY}', '{SUBJECT}'],
                        [$formData['messageText'],
                        $formData['subject']],
                        $htmlWrap
                    );

                    file_put_contents($formData['messageFile'], $formData['messageText']);

                    try {
                        /* Doesn't work. Windows does not deliver text/csv for expected files
                           Need to think of a better way
                        if ('text/csv' !== $formData['addressesFile']->getClientMimeType()) {
                            throw new FileException('Invalid filetype');
                        }*/

                        $formData['addressesFile']->move(
                            $this->filesDir.'/',
                            $formData['addressesFile']->getClientOriginalName() // append something unique
                        );

                        $formData['addressesFile'] = $this->filesDir.'/'.$formData['addressesFile']->getClientOriginalName(); // append something unique

                        $this->addressFile = $formData['addressesFile'];

                        if ($form->get('sendTestMail')->isClicked()) {
                            $pid = $this->triggerBulkMailerTestMessageProcess($formData);

                            $this->commandCreated = true;
                        } else {
                            $pid = $this->triggerBulkMailerProcess($formData);

                            $this->commandCreated = true;

                            return $this->redirectToRoute('bulkmailer');
                        }
                    } catch (FileException $e) {
                        $form->get('addressesFile')->addError(
                            new FormError('Invalid file')
                        );

                        $request->getSession()->getFlashBag()->add('error', sprintf($e->getMessage()));
                    }
                }
            } else {
                $errorString = '';

                foreach ($form->getErrors(true) as $error) {
                    $errorString .= '('.$error->getOrigin()->getName().') '.$error->getMessage();
                }

                $request->getSession()->getFlashBag()->add('error', sprintf($errorString));
            }
        }

        return $this->render('bulkmailer/index.html.twig', [
            'form' => $form->createView(),
            'isProcessRunnerReachable' => $this->isProcessRunnerReachable(),
            'pid' => isset($pid) ? $pid : null,
            'runningBulkMailerProcesses' => $this->getRunningBulkMailerProcesses(),
        ]);
    }

    /**
     * @Route("/show-process-log/{pid}", name="show-process-log")
     */
    public function showProcessLogAction($pid)
    {
        $log = $this->get('background_process_connector')->getProcessLog($pid);
        $log = str_replace(["\0x04", "\n"], ["", "<br />\n"], $log);

        echo $log;

        exit;
    }

    /**
     * @return boolean
     */
    protected function isProcessRunnerReachable() : bool
    {
        return $this->get('background_process_connector')->isProcessRunnerReachable();
    }

    /**
     * @return array
     */
    protected function getRunningBulkMailerProcesses() : array
    {
        try {
            return $this->get('background_process_connector')->getRunningProcesses();
        } catch (\Exception $exception) {
            return [];
        }
    }

    /**
     * @return int - pid (or -1)?
     */
    protected function triggerBulkMailerTestMessageProcess($formData) : int
    {
        
        return $this->get('background_process_connector')->triggerProcess(
            Command::newInstance()
                ->setCommand($this->get('kernel')->getRootDir().'/../bin/console')
                ->addOption('-vv')
                ->addOption('BulkMailerCommandForBackgroundProcessRunner')
                ->addOption('--message-file='.$formData['messageFile'])
                ->addOption('--addresses-file='.$formData['addressesFile'])
                ->addOption('--subject='.$formData['subject'])
                ->addOption('--testmode='.serialize($formData['emails']))
                //->addMailAddress($formData['emails'])
                ->setMailAddresses($formData['emails'])
        );
    }

    /**
     * @return int - pid (or -1)?
     */
    protected function triggerBulkMailerProcess($formData) : int
    {
        return $this->get('background_process_connector')->triggerProcess(
            Command::newInstance()
                ->setCommand($this->get('kernel')->getRootDir().'/../bin/console')
                //->setCwd(null)
                ->addOption('-vv')
                ->addOption('BulkMailerCommandForBackgroundProcessRunner')
                ->addOption('--message-file='.$formData['messageFile'])
                ->addOption('--addresses-file='.$formData['addressesFile'])
                ->addOption('--subject='.$formData['subject'])
                //->setOptions([..])
                //->addMailAddress($formData['emails'])
                ->setMailAddresses($formData['emails'])
        );
    }
}
