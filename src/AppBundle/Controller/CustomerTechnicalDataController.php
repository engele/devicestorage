<?php

/**
 * 
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Customer\CustomerVlanProfile;
use AppBundle\Entity\VlanProfile;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/customer/{id}/technical-data")
 */
class CustomerTechnicalDataController extends BaseController
{
    /**
     * Set value for  override inherited vlan profiles for customer
     * 
     * @Route("/set-override-inherited-vlan-profiles/", name="customer/technical-data/set-override-inherited-vlan-profiles", methods={"POST"})
     */
    public function ajaxSetOverrideInheritedVlanProfilesAction(Request $request, $id)
    {
        if (1 !== preg_match('/^[01]$/', $request->request->get('override-inherited-vlan-profiles'))) {
            return new JsonResponse(['status' => 'error', 'message' => 'Invalid value for override-inherited-vlan-profiles'], 403);
        }

        $db = $this->getDoctrine()->getManager()->getConnection();

        // check if customer exists
        $stmt = $db->prepare("SELECT `override_inherited_vlan_profiles` FROM `customers` WHERE `id` = ?");
        $stmt->bindValue(1, (int) $id);
        $stmt->execute();

        $customer = $stmt->fetch();

        // update customer
        $stmt = $db->prepare("UPDATE `customers` SET `override_inherited_vlan_profiles` = ? WHERE `id` = ?");
        $stmt->execute([
            (int) $request->request->get('override-inherited-vlan-profiles'),
            (int) $id
        ]);

        if (empty($stmt->errorCode())) {
            return new JsonResponse(['status' => 'error', 'message' => 'Can not update customers override_inherited_vlan_profiles.'], 403);
        }

        return new JsonResponse(['status' => 'success']);
    }

    /**
     * Add one vlan profile to customer
     * 
     * @Route("/add-vlan-profile/", name="customer/technical-data/add-vlan-profile", methods={"POST"})
     */
    public function ajaxAddVlanProfileAction(Request $request, $id)
    {
        $vlanProfile = $this->getDoctrine()->getRepository(VlanProfile::class)->findOneById($request->request->get('vlan-profile-id'));

        if (!($vlanProfile instanceof VlanProfile)) {
            return new JsonResponse(['status' => 'error', 'message' => 'Missing vlan profile id'], 403);
        }

        $customerVlanProfile = $this->getDoctrine()->getRepository(CustomerVlanProfile::class)->findOneBy([
            'vlanProfile' => $vlanProfile,
            'customerId' => $id
        ]);

        if (null !== $customerVlanProfile) {
            return new JsonResponse(['status' => 'error', 'message' => 'VLAN-Profil wurde dem Kunden bereits hinzugefügt.'], 403);
        }

        $customerVlanProfile = new CustomerVlanProfile();
        $customerVlanProfile
            ->setCustomerId($id)
            ->setVlanProfile($vlanProfile);

        $em = $this->getDoctrine()->getManager();
        $em->persist($customerVlanProfile);
        $em->flush();

        return new JsonResponse(['status' => 'success']);
    }

    /**
     * Delete one customer vlan profile
     * 
     * @Route("/delete-vlan-profile/", name="customer/technical-data/delete-vlan-profile", methods={"POST"})
     */
    public function ajaxDeleteCustomerVlanProfileAction(Request $request, $id)
    {
        $customerVlanProfile = $this->getDoctrine()->getRepository(CustomerVlanProfile::class)->findOneById($request->request->get('customer-vlan-profile-id'));

        if (!($customerVlanProfile instanceof CustomerVlanProfile)) {
            return new JsonResponse(['status' => 'error', 'message' => 'Missing customer vlan profile id'], 403);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($customerVlanProfile);
        $em->flush();

        $request->getSession()->getFlashBag()->add('success', 'VLAN-Profil entfernt.');

        return new JsonResponse(['status' => 'success']);
    }

    /**
     * Set customers internet password from pppoe-pin
     * 
     * @Route("/set-internet-password-from-pppoe-pin/", name="customer/technical-data/set-internet-password-from-pppoe-pin")
     */
    public function setInternetPasswordFromPppoePinAction(Request $request, $id)
    {
        $db = $this->getDoctrine()->getManager()->getConnection();

        $stmt = $db->prepare("SELECT `pppoe_pin`, `password`, `techdata_comment` FROM `customers` WHERE `id` = ?");
        $stmt->bindValue(1, (int) $id);
        $stmt->execute();

        $customer = $stmt->fetch();

        try {
            if (empty($customer['pppoe_pin']) || strlen($customer['pppoe_pin']) < 15) {
                throw new \UnexpectedValueException('PPPoE-PIN must be an integer with a total length of 15.');
            }

            $now = new \DateTime();

            $comment = $customer['techdata_comment'];

            if (!empty($comment)) {
                $lastTwoCharacters = substr($comment, -2);

                if (false !== strpos($lastTwoCharacters, "\n")) {
                    $comment .= "\n";
                } else {
                    $comment .= "\n\n";
                }
            }

            $comment .= sprintf("Altes inet-pw (%s): %s\n",
                $now->format('d.m.Y'),
                $customer['password']
            );

            $password = substr($customer['pppoe_pin'], -6);

            $stmt = $db->prepare("UPDATE `customers` SET `password` = ?, `techdata_comment` = ? WHERE `id` = ?");
            $stmt->execute([
                $password,
                $comment,
                (int) $id
            ]);

            if (empty($stmt->errorCode())) {
                throw new \RuntimeException(sprintf('Can not update customers password - %s', $stmt->errorInfo()));
            }
            
            $request->getSession()->getFlashBag()->add('success', 'Neues Internet-Passwort gespeichert');
        } catch (\Exception $exception) {
            $request->getSession()->getFlashBag()->add('error', $exception->getMessage());
        }

        if (null !== $request->headers->get('referer')) {
            return $this->redirect($request->headers->get('referer'));
        }

        return $this->redirectToRoute('homepage'); // fallback
    }
}
