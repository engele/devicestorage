<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Customer\History\History;
use AppBundle\Entity\User;
use AppBundle\Form\Type\OtrsAdapter\OtrsTicketCreateType;
use AppBundle\src\OtrsAdapter\Entity\RequestParamsTicketCreate;
use AppBundle\src\OtrsAdapter\OtrsAdapter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class OtrsAjaxController extends BaseController
{
    /**
     * @var OtrsAdapter
     */
    public $otrsAdapter;

    /**
     * @var array
     */
    public $otrsAdapterConfig;

    /**
     * @var Request
     */
    public $request;

    /**
     * @param Request $request
     * @param OtrsAdapter $otrsAdapter
     * @return JsonResponse
     */
    public function ticketCreateAction(Request $request, OtrsAdapter $otrsAdapter)
    {
        $this->request = $request;
        $this->otrsAdapter = $otrsAdapter;
        $this->otrsAdapterConfig = $this->otrsAdapter->getOtrsAdapterConfig();

        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse('Error: only json-format allowed!');
        }

//        $ticketCreateForm = $this->createForm(OtrsTicketCreateType::class, new RequestParamsTicketCreate()); // todo: check

        $ticketCreateForm = $this->createForm(OtrsTicketCreateType::class);
        $ticketCreateForm->handleRequest($request);
        $ticketCreateForm->submit($request->request->get($ticketCreateForm->getName()));

        // form-validation-error?
        if (!($ticketCreateForm->isSubmitted() && $ticketCreateForm->isValid())) {
            $errorString = '';
            foreach ($ticketCreateForm->getErrors(true) as $error) {
                $errorString .= '(' . $error->getOrigin()->getName() . ') ' . $error->getMessage();
            }
            $response = [
                'error' => true,
                'data' => 'Fehler: Die Formular-Eingaben sind ungültig! Fehler:' . $errorString
            ];
            return new JsonResponse(json_encode($response));
        }


//        $this->denyAccessUnlessGranted('ROLE_EDIT_HISTORY_EVENTS'); //todo rechtecheck: ticket-create

        // customerId-mapping-error?
        $otrsCustomerId = '';
        if (isset($this->otrsAdapterConfig['mappingWisotelCustomerIdToOtrsCustomerId'][$this->request->get('internal_customerId')])) {
            $otrsCustomerId = $this->otrsAdapterConfig['mappingWisotelCustomerIdToOtrsCustomerId'][$this->request->get('internal_customerId')];
        }

        if ($otrsCustomerId == '') {
            $errorMessage = 'Zu Wisotel-CustomerID wurde keine OTRS-CustomerID gefunden: ' . $this->request->get('internal_customerId');
            $response = [
                'error' => true,
                'data' => $errorMessage
            ];
            return new JsonResponse(json_encode($response));
        }

        // ticket-create-error?
        $result = $this->createTicket($otrsCustomerId);
        if (!$result->isOperationSuccess()) {
            $errorMessage = $result->getResultMessage();
            $debugData = $result->getDebugData(); //todo: log somewhere
            $otrsErrorMessage = '';
            if (($debugData != '') && isset($debugData['responseData'])) {
                $otrsErrorMessage = $debugData['responseData']->body->Error->ErrorMessage;
            }
            $response = [
                'error' => true,
                'data' => $errorMessage . '; OTRS-Message: ' . $otrsErrorMessage . ''
            ];
            return new JsonResponse(json_encode($response));
        }

        // success!
        $data = $result->getResultData();
        $this->createCustomerHistoryEntry($data->Ticket, $otrsCustomerId);
        $response = [
            'error' => false,
            'data' => $data->TicketID
        ];
        return new JsonResponse(json_encode($response));
    }

    private function createCustomerHistoryEntry($ticketResultData, string $otrsCustomerId): void
    {
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $url = $this->otrsAdapter->getTicketViewUrl() . $ticketResultData->TicketID;
        $note = 'Es wurde ein OTRS-Ticket erstellt: CustomerId in OTRS: ' . $otrsCustomerId . ' ; TicketID: ' . $ticketResultData->TicketID . '.<br/>Link: <a href="' . $url . '" target="_blank">' . $url . '</a>';

        $history = new History();
        $history->setEventId($this->otrsAdapterConfig['customerHistoryEventId']);
        $history->setCustomerId($this->request->get('internal_customerId'));
        $history->setNote($note);
        $history->setCreatedBy($user->getUsername());
        $history->setCreatedAt(new \DateTime('now'));

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($history);
        $entityManager->flush();
    }

    /**
     * @param string $otrsCustomerId
     * @return \OtrsRestClient\Entity\OtrsResult
     */
    private function createTicket($otrsCustomerId): \OtrsRestClient\Entity\OtrsResult
    {
        $requestParamsTicketCreate = new RequestParamsTicketCreate(
            $this->request->get('subject'),
            $this->request->get('body'),
            $this->request->get('queueId'),
            $this->otrsAdapterConfig['otrsCustomerUser'],
            $otrsCustomerId,
            $this->request->get('ownerId'),
            $this->request->get('responsibleId')
        );

        return $this->otrsAdapter->createTicket($requestParamsTicketCreate);
    }
}