<?php

/**
 *
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Customer\History\HistoryEvent;
use AppBundle\Form\Type\Customer\History\HistoryEventType;

/**
 * @Route("/admin/history-events")
 */
class HistoryEventsController extends BaseController
{
    /**
     * @Route("/", name="admin-history-events")
     */
    public function indexAction(Request $request)
    {
        $doctrine = $this->getDoctrine();

        return $this->render('admin/history-events/history-events-overview.html.php', [
            'historyEvents' => $doctrine->getRepository(HistoryEvent::class)->findAll(),
        ]);




        /*$productCategories = $doctrine->getRepository('AppBundle\Entity\Product\MainProductCategory')->findAll();
        $cardTypes = $doctrine->getRepository('AppBundle\Entity\Location\CardType')->findAll();
        $products = $doctrine->getRepository('AppBundle\Entity\Product\MainProduct')->findAll();
        $taxRates = $doctrine->getRepository('AppBundle\Entity\Product\TaxRate')->findAll();

        return $this->render('admin/product/products-overview.html.php', [
            'productCategories' => $productCategories,
            'cardTypes' => $cardTypes,
            'products' => $products,
            'taxRates' => $taxRates,
        ]);*/
    }

    /**
     * @Route("/create-history-event", name="admin-history-events-create-history-event")
     * @Route("/edit-history-event/{id}", name="admin-history-events-edit-history-event")
     */
    public function createHistoryEventAction(Request $request, HistoryEvent $historyEvent = null)
    {
        if (null === $historyEvent) {
            $historyEvent = new HistoryEvent();
        }

        $form = $this->createForm(HistoryEventType::class, $historyEvent);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($form->getData());
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'Verlauf-Event gespeichert.');

                return $this->redirectToRoute('admin-history-events');
            } else {
                $errorString = '';

                foreach ($form->getErrors(true) as $error) {
                    $errorString .= '('.$error->getOrigin()->getName().') '.$error->getMessage();
                }

                $request->getSession()->getFlashBag()->add('error', sprintf(
                    'Fehler<br />%s', $errorString
                ));
            }
        }

        return $this->render('admin/history-events/create-history-event.html.php', [
            'form' => $form->createView(),
        ]);
    }
}