<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Product\CrossSellingProduct;
use AppBundle\Form\Type\Admin\Product\ProductOptionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
* @Route("/admin/product-options")
* @Security("has_role('ROLE_EDIT_PRODUCTS')")
*/
class ProductOptionManagementController extends BaseController
{
    /**
     * @Route("/", name="admin-product-options-overview")
     */
    public function indexAction(Request $request)
    {
        $doctrine = $this->getDoctrine();

        $productOptions = $doctrine->getRepository('AppBundle\Entity\Product\CrossSellingProduct')->findAll();

        return $this->render('admin/product/product-options.html.php', [
            'productOptions' => $productOptions,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="product-option-edit")
     */
    public function editAction(CrossSellingProduct $crossSellingProduct, Request $request)
    {
        $identifier = $crossSellingProduct->getIdentifier();

        $form = $this->createForm(ProductOptionType::class, $crossSellingProduct);
        $form->handleRequest($request);

        $crossSellingProduct->setIdentifier($identifier);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($crossSellingProduct);
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'Option gespeichert.');

                return $this->redirectToRoute('admin-product-options-overview');
            }
        }

        return $this->render('admin/product/product-option-edit.html.php', [
            'productOptionForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/create/", name="product-option-create")
     *
     * @param Request $request
     * @param CrossSellingProduct|null $crossSellingProduct
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request, CrossSellingProduct $crossSellingProduct = null)
    {
        if ($crossSellingProduct === null) {
            $crossSellingProduct = new CrossSellingProduct();
        }

        $form = $this->createForm(ProductOptionType::class, $crossSellingProduct, ['new' => true]);
        // $form->get('identifier')->
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($crossSellingProduct);
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'Option gespeichert.');

                return $this->redirectToRoute('admin-product-options-overview');
            }
        }

        return $this->render('admin/product/product-option-edit.html.php', [
            'productOptionForm' => $form->createView(),
        ]);
    }
}
