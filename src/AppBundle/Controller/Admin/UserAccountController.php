<?php

/**
 *
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\ProductType;
use AppBundle\Form\Type\Admin\UserAccountType;
use AppBundle\Form\Type\Admin\PurtelAdminAccountType;
use AppBundle\Form\Type\Admin\PurtelAdminAccountEditType;
use AppBundle\Entity\User;
use AppBundle\Entity\PurtelAdminAccount;

/**
 * @Route("/admin/user-accounts")
 * @Security("has_role('ROLE_ADMIN')")
 */
class UserAccountController extends BaseController
{
    /**
     * @Route("/{id}", name="admin-user-accounts-edit")
     */
    public function userAccountsEditAction(User $user, Request $request)
    {
        $accountForm = $this->createForm(UserAccountType::class, $user, [
            'roles' => $this->container->getParameter('user_account_roles'),
        ]);

        $accountForm->handleRequest($request);

        if ($accountForm->isValid()) {
            try {
                $password = $user->getNewPassword();
                $passwordRepeated = $user->getNewPasswordRepeated();

                $passwordEmpty = empty($password);
                $passwordRepeatedEmpty = empty($passwordRepeated);
                
                // if on is empty but not both
                if (($passwordEmpty || $passwordRepeatedEmpty) && (!$passwordEmpty || !$passwordRepeatedEmpty)) {
                    throw new \InvalidArgumentException('Passwort und Passwort wiederholen stimmen nicht überein');
                }

                if (!$passwordEmpty && !$passwordRepeatedEmpty) {
                    if ($password !== $passwordRepeated) {
                        throw new \InvalidArgumentException('Passwort und Passwort wiederholen stimmen nicht überein');
                    }

                    $encodedPassword = $this->get('security.password_encoder')->encodePassword($user, $password);

                    $user->setPassword($encodedPassword);
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'User Account erfolgreich aktualisiert');

                return $this->redirectToRoute('admin-user-accounts');
            } catch (\InvalidArgumentException $e) {
                $request->getSession()->getFlashBag()->add('error', $e->getMessage());
            }
        }

        return $this->render('admin/user-accounts-edit.html.php', [
            'accountForm' => $accountForm->createView(),
        ]);
    }

    /**
     * @Route("/purtel-admin-account/{id}", name="admin-purtel-admin-accounts-edit")
     */
    public function purtelAdminAccountsEditAction(PurtelAdminAccount $purtelAdminAccount, Request $request)
    {
        $purtelAdminAccountForm = $this->createForm(PurtelAdminAccountEditType::class, $purtelAdminAccount, [
            'users' => $this->getDoctrine()->getRepository('AppBundle:User')->findAll(),
        ]);

        $purtelAdminAccountForm->handleRequest($request);

        if ($purtelAdminAccountForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($purtelAdminAccount);

            try {
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'Purtel-Admin-Account erfolgreich gespeichert');

                return $this->redirectToRoute('admin-user-accounts');
            } catch (\Exception $exception) {
                $request->getSession()->getFlashBag()->add('error', $e->getMessage());
            }
        }

        return $this->render('admin/user-accounts-edit.html.php', [
            'accountForm' => $purtelAdminAccountForm->createView(),
        ]);
    }

    /**
     * @Route("/", name="admin-user-accounts")
     */
    public function userAccountsAction(Request $request)
    {
        $roles = $this->container->getParameter('user_account_roles');
        $doctrine = $this->getDoctrine();

        $accounts = $doctrine->getRepository('AppBundle:User')->findAll();

        $newAccountForm = $this->createForm(UserAccountType::class, [], [
            'roles' => $roles,
        ]);

        $newAccountForm->handleRequest($request);

        if ($newAccountForm->isValid()) {
            $data = $newAccountForm->getData();

            $password = $data['newPassword'];
            $passwordRepeated = $data['newPasswordRepeated'];

            try {
                if (empty($password) || empty($passwordRepeated)) {
                    throw new \InvalidArgumentException('Passwort und Passwort wiederholen dürfen nicht leer sein');
                }

                if ($password !== $passwordRepeated) {
                    throw new \InvalidArgumentException('Passwort und Passwort wiederholen stimmen nicht überein');
                }

                $user = new User();

                $encodedPassword = $this->get('security.password_encoder')->encodePassword($user, $password);

                $user->setPassword($encodedPassword)
                    ->setUsername($data['username'])
                    ->setFullName($data['fullName'])
                    ->setEmail($data['email'])
                    ->setGroupwareUsername($data['groupwareUsername'])
                    ->setIncludeInHistoryReporting($data['includeInHistoryReporting'])
                    ->setRoles($data['roles'])
                    ->setIsActive($data['isActive']);

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'User Account erfolgreich angelegt');

                return $this->redirectToRoute('admin-user-accounts');
            } catch (\InvalidArgumentException $e) {
                $request->getSession()->getFlashBag()->add('error', $e->getMessage());
            }
        }

        $newPurtelAdminAccountForm = $this->createForm(PurtelAdminAccountType::class, new PurtelAdminAccount(), [
            'users' => $accounts,
        ]);
        $newPurtelAdminAccountForm->handleRequest($request);

        if ($newPurtelAdminAccountForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($newPurtelAdminAccountForm->getData());
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'Purtel-Admin-Account erfolgreich angelegt');

            return $this->redirectToRoute('admin-user-accounts');
        }

        return $this->render('admin/user-accounts.html.php', [
            'roles' => $roles,
            'accounts' => $accounts,
            'newAccountForm' => $newAccountForm->createView(),
            'newPurtelAdminAccountForm' => $newPurtelAdminAccountForm->createView(),
            'purtelAdminAccounts' => $doctrine->getRepository('AppBundle:PurtelAdminAccount')->findAll(),
        ]);
    }
}
