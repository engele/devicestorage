<?php

/**
 *
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Product\MainProduct;
use AppBundle\Entity\Product\MainProductCategory;
use AppBundle\Entity\Location\CardType;
use AppBundle\Entity\Product\TaxRate;
use AppBundle\Form\Type\Admin\Product\MainProductCategoryType;
use AppBundle\Form\Type\Admin\Product\MainProductType;
use AppBundle\Form\Type\Location\CardTypeType;
use AppBundle\Form\Type\Admin\Product\TaxRateType;

/**
 * @Route("/admin/products")
 */
class ProductManagementController extends BaseController
{
    /**
     * @Route("/", name="admin-products-overview")
     */
    public function indexAction(Request $request)
    {
        $doctrine = $this->getDoctrine();

        $productCategories = $doctrine->getRepository('AppBundle\Entity\Product\MainProductCategory')->findAll();
        $cardTypes = $doctrine->getRepository('AppBundle\Entity\Location\CardType')->findAll();
        $products = $doctrine->getRepository('AppBundle\Entity\Product\MainProduct')->findAll();
        $taxRates = $doctrine->getRepository('AppBundle\Entity\Product\TaxRate')->findAll();

        return $this->render('admin/product/products-overview.html.php', [
            'productCategories' => $productCategories,
            'cardTypes' => $cardTypes,
            'products' => $products,
            'taxRates' => $taxRates,
        ]);
    }

    /**
     * @Route("/create-tax-rate", name="admin-products-create-tax-rate")
     * @Route("/edit-tax-rate/{id}", name="admin-products-edit-tax-rate")
     */
    public function createTaxRateAction(Request $request, TaxRate $taxRate = null)
    {
        if (null === $taxRate) {
            $taxRate = new TaxRate();
        }

        $form = $this->createForm(TaxRateType::class, $taxRate);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($form->getData());
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'Steuersatz gespeichert.');

                return $this->redirectToRoute('admin-products-overview');
            } else {
                $errorString = '';

                foreach ($form->getErrors(true) as $error) {
                    $errorString .= '('.$error->getOrigin()->getName().') '.$error->getMessage();
                }

                $request->getSession()->getFlashBag()->add('error', sprintf(
                    'Fehler<br />%s', $errorString
                ));
            }
        }

        return $this->render('admin/product/create-tax-rate.html.php', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/create-card-type", name="admin-products-create-card-type")
     * @Route("/edit-card-type/{id}", name="admin-products-edit-card-type")
     */
    public function createCardTypeAction(Request $request, CardType $cardType = null)
    {
        if (null === $cardType) {
            $cardType = new CardType();
        }

        $form = $this->createForm(CardTypeType::class, $cardType);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($form->getData());
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'Katen-Typ gespeichert.');

                return $this->redirectToRoute('admin-products-overview');
            } else {
                $errorString = '';

                foreach ($form->getErrors(true) as $error) {
                    $errorString .= '('.$error->getOrigin()->getName().') '.$error->getMessage();
                }

                $request->getSession()->getFlashBag()->add('error', sprintf(
                    'Fehler<br />%s', $errorString
                ));
            }
        }

        return $this->render('create-card-type.html.php', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/create-category", name="admin-products-create-category")
     * @Route("/edit-category/{id}", name="admin-products-edit-category")
     */
    public function createCategoryAction(Request $request, MainProductCategory $category = null)
    {
        if (null === $category) {
            $category = new MainProductCategory();
        }

        $form = $this->createForm(MainProductCategoryType::class, $category);
        
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($form->getData());
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'Kategorie gespeichert.');

                return $this->redirectToRoute('admin-products-overview');
            } else {
                $errorString = '';

                foreach ($form->getErrors(true) as $error) {
                    $errorString .= '('.$error->getOrigin()->getName().') '.$error->getMessage();
                }

                $request->getSession()->getFlashBag()->add('error', sprintf(
                    'Fehler<br />%s', $errorString
                ));
            }
        }

        return $this->render('admin/product/create-category.html.php', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/create-product", name="admin-products-create-product")
     * @Route("/edit-product/{id}", name="admin-products-edit-product")
     */
    public function createProductAction(Request $request, MainProduct $product = null)
    {
        $doctrine = $this->getDoctrine();

        if (null === $product) {
            $product = new MainProduct();
        }

        $form = $this->createForm(MainProductType::class, $product, [
            'categories' => $doctrine->getRepository('AppBundle\Entity\Product\MainProductCategory')->findAll(),
            'cardTypes' => $doctrine->getRepository('AppBundle\Entity\Location\CardType')->findAll(),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $doctrine->getManager();
                $em->persist($form->getData());
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'Produkt gespeichert.');

                return $this->redirectToRoute('admin-products-overview');
            } else {
                $errorString = '';

                foreach ($form->getErrors(true) as $error) {
                    $errorString .= '('.$error->getOrigin()->getName().') '.$error->getMessage();
                }

                $request->getSession()->getFlashBag()->add('error', sprintf(
                    'Fehler<br />%s', $errorString
                ));
            }
        }

        return $this->render('admin/product/create-product.html.php', [
            'form' => $form->createView(),
        ]);
    }
}