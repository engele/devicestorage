<?php

/**
 *
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\SpectrumProfile;
use AppBundle\Form\Type\Admin\SpectrumProfileType;

/**
 * @Route("/admin/spectrum-profiles")
 */
class SpectrumProfileController extends BaseController
{
    /**
     * @Route("/", name="admin-spectrum-profiles")
     */
    public function indexAction(Request $request)
    {
        $doctrine = $this->getDoctrine();

        $groupedSpectrumProfiles = [];

        foreach ($doctrine->getRepository(SpectrumProfile::class)->findAll() as $spectrumProfile) {
            $connectionType = $spectrumProfile->getConnectionType()->getName();

            if (!isset($groupedSpectrumProfiles[$connectionType])) {
                $groupedSpectrumProfiles[$connectionType] = [];
            }

            $groupedSpectrumProfiles[$connectionType][] = $spectrumProfile;
        }

        return $this->render('admin/spectrum-profile/overview.html.twig', [
            'groupedSpectrumProfiles' => $groupedSpectrumProfiles,
        ]);
    }

    /**
     * @Route("/create-spectrum-profile", name="admin-create-spectrum-profile")
     * @Route("/edit-spectrum-profile/{id}", name="admin-edit-spectrum-profile")
     */
    public function createAndEditAction(Request $request, SpectrumProfile $spectrumProfile = null)
    {
        if (null === $spectrumProfile) {
            $spectrumProfile = new SpectrumProfile();
        }

        $form = $this->createForm(SpectrumProfileType::class, $spectrumProfile);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($form->getData());
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'Spectrum-Profil gespeichert.');

                return $this->redirectToRoute('admin-spectrum-profiles');
            } else {
                $errorString = '';

                foreach ($form->getErrors(true) as $error) {
                    $errorString .= '('.$error->getOrigin()->getName().') '.$error->getMessage();
                }

                $request->getSession()->getFlashBag()->add('error', sprintf(
                    'Fehler<br />%s', $errorString
                ));
            }
        }

        return $this->render('admin/spectrum-profile/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}