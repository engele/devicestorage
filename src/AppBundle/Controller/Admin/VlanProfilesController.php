<?php

/**
 *
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\VlanProfile;
use AppBundle\Entity\Vlan;
use AppBundle\Form\Type\Admin\VlanProfileType;

/**
 * @Route("/admin/vlan-profiles")
 */
class VlanProfilesController extends BaseController
{
    /**
     * @Route("/", name="admin-vlan-profiles")
     */
    public function indexAction(Request $request)
    {
        $doctrine = $this->getDoctrine();

        return $this->render('admin/vlan-profiles/overview.html.php', [
            'vlanProfiles' => $doctrine->getRepository(VlanProfile::class)->findAll(),
        ]);
    }

    /**
     * @Route("/create-vlan-profile", name="admin-create-vlan-profile")
     * @Route("/edit-vlan-profile/{id}", name="admin-edit-vlan-profile")
     */
    public function createVlanProfileAction(Request $request, VlanProfile $vlanProfile = null)
    {
        $vlansBeforeSubmitting = [];

        if (null === $vlanProfile) {
            $vlanProfile = new VlanProfile();
        } else {
            $vlansBeforeSubmitting = $vlanProfile->getVlans()->toArray();
        }

        //$vlanProfile->addVlan(new Vlan());

        $form = $this->createForm(VlanProfileType::class, $vlanProfile);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $vlansIdsAfterSubmitting = [];

                foreach ($vlanProfile->getVlans() as $vlan) {
                    if (null === $vlan->getVlanId()) {
                        $vlanProfile->removeVlan($vlan);

                        continue;
                    }

                    $vlan->setVlanProfile($vlanProfile);
                    $em->persist($vlan);

                    $vlansIdsAfterSubmitting[$vlan->getId()] = true;
                }

                foreach ($vlansBeforeSubmitting as $vlan) {
                    if (!isset($vlansIdsAfterSubmitting[$vlan->getId()])) {
                        $em->remove($vlan);
                    }
                }
                
                $em->persist($vlanProfile);
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'VLAN-Profil gespeichert.');

                return $this->redirectToRoute('admin-vlan-profiles');
            } else {
                $errorString = '';

                foreach ($form->getErrors(true) as $error) {
                    $errorString .= '('.$error->getOrigin()->getName().') '.$error->getMessage();
                }

                $request->getSession()->getFlashBag()->add('error', sprintf(
                    'Fehler<br />%s', $errorString
                ));
            }
        }

        return $this->render('admin/vlan-profiles/create-and-edit.html.php', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/create-history-event", name="admin-history-events-create-history-event")
     * @Route("/edit-history-event/{id}", name="admin-history-events-edit-history-event")
     */
    /*public function createHistoryEventAction(Request $request, HistoryEvent $historyEvent = null)
    {
        if (null === $historyEvent) {
            $historyEvent = new HistoryEvent();
        }

        $form = $this->createForm(HistoryEventType::class, $historyEvent);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($form->getData());
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'Verlauf-Event gespeichert.');

                return $this->redirectToRoute('admin-history-events');
            } else {
                $errorString = '';

                foreach ($form->getErrors(true) as $error) {
                    $errorString .= '('.$error->getOrigin()->getName().') '.$error->getMessage();
                }

                $request->getSession()->getFlashBag()->add('error', sprintf(
                    'Fehler<br />%s', $errorString
                ));
            }
        }

        return $this->render('admin/history-events/create-history-event.html.php', [
            'form' => $form->createView(),
        ]);
    }*/
}