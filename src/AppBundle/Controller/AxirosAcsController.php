<?php

/**
 * 
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AxirosAcsApi\Api\AxirosAcsServiceApi;
use AxirosAcsApi\Api\AxirosAcsActionApi;
use AxirosAcsApi\Service\ActivatePppoeService;
use AxirosAcsApi\Service\ModifyPppoeService;
use AxirosAcsApi\Service\DeactivatePppoeService;
use AxirosAcsApi\Service\ReactivatePppoeService;
use AxirosAcsApi\Service\DeletePppoeService;
use AxirosAcsApi\Service\ActivateVoiceService;
use AxirosAcsApi\Service\ModifyVoiceService;
use AxirosAcsApi\Service\DeactivateVoiceService;
use AxirosAcsApi\Service\ReactivateVoiceService;
use AxirosAcsApi\Service\DeleteVoiceService;
use AxirosAcsApi\Service\AbstractService;
use AxirosAcsApi\Service\ActivateVoiceByCid2Service;
use AxirosAcsApi\Service\DeleteVoiceByCid2Service;
use AxirosAcsApi\Service\GetCwmpByCid2Service;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * 
 */
class AxirosAcsController extends Controller
{
    /**
     * Log for responses from written services
     * 
     * @var array
     */
    protected $log;

    /**
     * Log something
     * 
     * @param string $value
     * @param string|null $event
     * 
     * @return AxirosAcsController
     */
    protected function log($value, $event = null)
    {
        if (null !== $event) {
            $this->log[$event] = $value;
        } else {
            $this->log[] = $value;
        }

        return $this;
    }

    /**
     * Get customer-data from database by id
     * 
     * @param integer $customerId
     * 
     * @return array|null
     */
    protected function getCustomerById($customerId)
    {
        $customerId = (int) $customerId;

        $db = $this->getDoctrine()->getManager()->getConnection();

        $stmt = $db->prepare("SELECT * FROM `customers` WHERE id = ?");
        $stmt->bindValue(1, $customerId);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return $stmt->fetch();
        }

        return null;
    }

    /**
     * @Route("/customer/{id}/axiros-acs/fetch-cpeid-by-cid2", name="axiros-acs/fetch-cpeid-by-cid2")
     */
    public function fetchCpeIdByCid2Action(Request $request, $id)
    {
        $customer = $this->getCustomerById($id);

        if (null === $customer) {
            throw new NotFoundHttpException('Page not found');
        }

        if (empty($customer['pppoe_pin'])) {
            $customer['pppoe_pin'] = null;
        }

        require_once $this->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';
        require_once $this->get('kernel')->getRootDir().'/../web/customer/Lib/PppoeUsername/PppoeUsername.php';

        $axirosAcsApiParameters = \Wisotel\Configuration\Configuration::get('axirosAcs');

        $axirosAcsApi = new AxirosAcsActionApi(
            $axirosAcsApiParameters['username'],
            $axirosAcsApiParameters['password'],
            $axirosAcsApiParameters['url']
        );

        $pppoeUsername = \PppoeUsername::create(
            $customer['pppoe_pin'],
            $customer['clientid']
        );

        $pppoeUsername = str_replace('/', '-', $pppoeUsername); // ACS will translate - back to / (/ is part of an url)

        $voiceByCid2Service = new GetCwmpByCid2Service();
        $voiceByCid2Service->setData([
            'getDeviceIDbyCID2Input' => $pppoeUsername,
        ]);

        $axirosAcsApi->addService($voiceByCid2Service);

        $axirosAcsApi->writeServices();

        $response = '';

        foreach ($axirosAcsApi->getServices() as $service) {
            $transmissionStatus = $service->getTransmissionStatus();

            $data = var_export($service->getData(), true);
            $data = str_replace(" ", '&nbsp;', $data);
            $data = str_replace("\n", '<br />', $data);

            $response .= sprintf('<strong>Url:</strong> %s<br /><strong>Data:</strong><br />%s<br /><br />',
                null !== $transmissionStatus ? $transmissionStatus->getUrl() : 'service not sent',
                $data
            );

            if (null !== $transmissionStatus) {
                if ('success' === $transmissionStatus->getStatus()) {
                    $successResponse = $transmissionStatus->getSuccessResponse();

                    $response .= sprintf('<strong>Response:</strong><br />
                        <span style="color: green;">Success</span> - %s - cwmp (<strong>%s</strong>) - (%s)',
                        $successResponse->getDeviceIDbyCID2Output->msg,
                        $successResponse->getDeviceIDbyCID2Output->cpeid,
                        $successResponse->getDeviceIDbyCID2Output->code
                    );
                } else {
                    $errorResponse = $transmissionStatus->getErrorException();

                    $response .= sprintf('<strong>Response:</strong><br /><span style="color: red;">Error</span> - %s (%s)',
                        $errorResponse->getMessage(),
                        $errorResponse->getCode()
                    );
                }
            }

            $response .= '<br /><br /><hr /><br />';
        }

        return new Response($response);
    }

    /**
     * @Route("/customer/{id}/axiros-acs/voice-by-cid2/{action}", name="axiros-acs/voice-by-cid2")
     */
    public function voiceByCid2Action(Request $request, $id, $action)
    {
        if (!in_array($action, ['activate', 'delete'])) {
            throw new NotFoundHttpException('Action not known');
        }

        $customer = $this->getCustomerById($id);

        if (null === $customer) {
            throw new NotFoundHttpException('Page not found');
        }

        $db = $this->getDoctrine()->getManager()->getConnection();

        $stmt = $db->prepare("SELECT `area_code` FROM `networks` WHERE id = ?");
        $stmt->bindValue(1, $customer['network_id']);
        $stmt->execute();

        if ($stmt->rowCount() < 1) {
            throw new NotFoundHttpException(sprintf('Customers network (id: %s) not found', $customer['network_id']));
        }

        $defaultAreaCode = $stmt->fetch();
        $defaultAreaCode = $defaultAreaCode['area_code'];

        if (empty($defaultAreaCode)) {
            throw new NotFoundHttpException(sprintf('Customers network (id: %s) has no area_code',
                $customer['network_id']
            ));
        }

        require_once $this->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';
        require_once $this->get('kernel')->getRootDir().'/../web/customer/Lib/PppoeUsername/PppoeUsername.php';

        $axirosAcsApi = $this->createAxirosAcsApiInstance();

        $axirosAcsApiParameters = \Wisotel\Configuration\Configuration::get('axirosAcs');

        $purtelAccounts = $this->getDoctrine()->getRepository('AppBundle\Entity\PurtelAccount')
            ->findByCustomerId($customer['id']);

        foreach ($purtelAccounts as $key => $account) {
            $areaCode = $account->getAreaCode();

            if (empty($areaCode)) {
                $areaCode = $defaultAreaCode;
            }

            $areaCode = preg_replace('/^0/', '', $areaCode); // strip leading 0 from area-code

            switch ($action) {
                case 'activate':
                    $voiceByCid2Service = new ActivateVoiceByCid2Service();
                    $voiceByCid2Service
                        // set data
                        ->setServiceIdentifier('directory_number', $account->getNummer())
                        ->setServiceParameter('area_code', $areaCode)
                        ->setServiceParameter('registrar', $axirosAcsApiParameters['registrar'])
                        ->setServiceParameter('username', $account->getPurtelLogin())
                        ->setServiceParameter('password', $account->getPurtelPassword())
                        ->setServiceParameter('directory_number', $account->getNummer());
                    break;

                case 'delete':
                    $voiceByCid2Service = new DeleteVoiceByCid2Service();
                    $voiceByCid2Service
                        // set data
                        ->setServiceIdentifier('directory_number', $account->getNummer());
                    break;
            }

            $pppoeUsername = \PppoeUsername::create(
                $customer['pppoe_pin'],
                $customer['clientid']
            );

            $pppoeUsername = str_replace('/', '-', $pppoeUsername); // ACS will translate - back to / (/ is part of an url)
            
            // set api essential parameters
            $voiceByCid2Service->setCpeId($pppoeUsername);

            $axirosAcsApi->addService($voiceByCid2Service);
        }

        $axirosAcsApi->writeServices();

        $response = '';

        foreach ($axirosAcsApi->getServices() as $service) {
            $transmissionStatus = $service->getTransmissionStatus();

            $data = var_export($service->getData(), true);
            $data = str_replace(" ", '&nbsp;', $data);
            $data = str_replace("\n", '<br />', $data);

            $response .= sprintf('<strong>Url:</strong> %s<br /><strong>Data:</strong><br />%s<br /><br />',
                null !== $transmissionStatus ? $transmissionStatus->getUrl() : 'service not sent',
                $data
            );

            if (null !== $transmissionStatus) {
                if ('success' === $transmissionStatus->getStatus()) {
                    $successResponse = $transmissionStatus->getSuccessResponse();

                    $response .= sprintf('<strong>Response:</strong><br /><span style="color: green;">Success</span> - %s (%s)',
                        $successResponse->message,
                        $successResponse->code
                    );
                } else {
                    $errorResponse = $transmissionStatus->getErrorException();

                    $response .= sprintf('<strong>Response:</strong><br /><span style="color: red;">Error</span> - %s (%s)',
                        $errorResponse->getMessage(),
                        $errorResponse->getCode()
                    );
                }
            }

            $response .= '<br /><br /><hr /><br />';
        }

        return new Response($response);
    }

    /**
     * @Route("/customer/ajaxAcs2.php", name="ajax-acs")
     */
    public function ajaxAcsAction(Request $request)
    {
        //$typeParam = urldecode($_POST['type']); # pppoe, voice, both
        $typeParam = 'both'; # pppoe, voice, both
        //$action = urldecode($_POST['action']); # activate, modify, delete, deactivate, reactivate 
        //$action = 'activate'; # activate, modify, delete, deactivate, reactivate 
        $action = 'deactivate'; # activate, modify, delete, deactivate, reactivate 
        //$acs = json_decode($_POST['acs'], true);
        $acs = [
            //'area_code' => '08459',
            'area_code' => '0841',
            'macAddress' => '1234567890AB',
            //'custId' => '5384',
            'custId' => '8972',
            'password' => '000000',
            'clientId' => 'abcdefgh',
        ];

        if (isset($acs['area_code']) && !empty($acs['area_code'])) {
            $acs['area_code'] = preg_replace('/^0/', '', $acs['area_code']); // strip leading 0 from area-code
        }

        require_once $this->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';
        require_once $this->get('kernel')->getRootDir().'/../web/customer/Lib/PppoeUsername/PppoeUsername.php';

        

        $info       = '';

        //$cpe_url = $axirosAcs['url']."/live/CPEManager/DMInterfaces/rest/v1/action/%s";
        //$cpe_action = 'DeleteCPEs';
        //$acs_url = $axirosAcs['url']."/live/CPEManager/AXServiceStorage/Interfaces/rest/v1/services/%s/cpeid/%s/action/%s";

        //$acs['registrar'] = $axirosAcs['registrar'];

        // extract mac-address from "macAddress"
        $cpeId = '';

        if (!empty($acs['macAddress'])) {
            $matchMac = null;

            preg_match(
                '/([0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2})|([0-9a-f]{12})/i',
                $acs['macAddress'],
                $matchMac
            );

            if (isset($matchMac[0])) {
                $cpeId = preg_replace('/[^0-9a-f]/i', '', $matchMac[0]);

                // extract fritzbox-gui-password from "macAddress"
                $matchPw = explode($matchMac[0], $acs['macAddress']);

                if (2 === count($matchPw) && !empty($matchPw[1])) {
                    $acs['acsPw'] = $matchPw[1];
                }
            }
        }





        
    
        /*foreach ($purtelAccounts as $key => $account) {
            $temp = $account->toArray();
        }*/
        



        /*
        $sql        = "SELECT * FROM purtel_account WHERE cust_id = ".$acs['custId'];

        $db_purtel_result = $db->query($sql);
        while ($temp = $db_purtel_result->fetch_assoc()) {
            array_push ($pur_voice, $temp);

            $acs_param['ServiceIdentifiers'] = array (
            'cpeid'             => $acs['acsId'],
            'directory_number'  => $temp['nummer']
            );
            $acs_param['CommandOptions'] = array();
            
            array_push ($acs_voice, $acs_param);
        }
        */

        

        $axirosAcsApi = $this->createAxirosAcsApiInstance();



        if ('pppoe' === $typeParam || 'both' === $typeParam) {
            $pppoeUsername = \PppoeUsername::create(
                $this->getPppoePinFromCustomer($acs['custId']),
                $acs['clientId']
            );

            $axirosAcsApi->addService(
                $this->createPppoeService($action, [
                    'cpeId' => $cpeId,
                    'username' => $pppoeUsername,
                    'password' => $acs['password'],
                ])
            );
        }

        if ('voice' === $typeParam || 'both' === $typeParam) {
            $axirosAcsApiParameters = \Wisotel\Configuration\Configuration::get('axirosAcs');

            $purtelAccounts = $this->getDoctrine()->getRepository('AppBundle\Entity\PurtelAccount')
                ->findByCustomerId($acs['custId']);

            foreach ($purtelAccounts as $key => $account) {
                $areaCode = $account->getAreaCode();

                if (null === $areaCode) {
                    $areaCode = $acs['area_code'];
                }

                $axirosAcsApi->addService(
                    $this->createVoiceService($action, [
                        'cpeId' => $cpeId,
                        'directory_number' => $account->getNummer(),
                        'area_code' => $areaCode,
                        'registrar' => $axirosAcsApiParameters['registrar'],
                        'username' => $account->getPurtelLogin(),
                        'password' => $account->getPurtelPassword(),
                    ])
                );
            }
        }









        switch ($action) {
            case 'activate':
            case 'modify':

            case 'deactivate':
            case 'reactivate':
            case 'delete':
        }





        $axirosAcsApi->writeServices();
        var_dump("log output", $this->log);

        foreach ($axirosAcsApi->getServices() as $service) {
            $transmissionStatus = $service->getTransmissionStatus();

            if (null !== $transmissionStatus) {
                var_dump($transmissionStatus->getStatus());
            }
        }

        die("asd");
    }

    /**
     * Set data to pppoe service object
     * 
     * @param AbstractService $pppoeService
     * @param array $data
     * 
     * @return AbstractService
     */
    private function setDataToPppoeService(AbstractService $pppoeService, array $data)
    {
        $pppoeService
            // set api essential parameters
            ->setCpeId($data['cpeId'])

            // set data
            ->setServiceIdentifier('cpeid', $data['cpeId'])
            ->setServiceParameter('username', $data['username'])
            ->setServiceParameter('password', $data['password']);

        $this->setDefaultEventHandlerToPppoeService($pppoeService);

        return $pppoeService;
    }

    /**
     * Set onError- and onSuccess-handler for pppoe service
     * 
     * @param AbstractService $pppoeService
     * 
     * @return AbstractService
     */
    public function setDefaultEventHandlerToPppoeService(AbstractService $pppoeService)
    {
        $pppoeService
            // set error handler
            ->onError(function (\Exception $exception) use ($pppoeService) {
                $this->log(sprintf('%s - %s', $exception->getCode(), $exception->getMessage()), 
                    sprintf('error.%s.%s.%s',
                        $pppoeService->getAction(),
                        $pppoeService->getType(),
                        $pppoeService->getServiceParameter('username')
                    )
                );
            })

            // set success handler
            ->onSuccess(function (\stdClass $curlResponse) use ($pppoeService) {
                $this->log(sprintf('%s - %s', $curlResponse->code, $curlResponse->message), 
                    sprintf('success.%s.%s.%s',
                        $pppoeService->getAction(),
                        $pppoeService->getType(),
                        $pppoeService->getServiceParameter('username')
                    )
                );
            });

        return $pppoeService;
    }

    /**
     * Set data to voice service object
     * 
     * @param AbstractService $voiceService
     * @param array $data
     * 
     * @return AbstractService
     */
    private function setDataToVoiceService(AbstractService $voiceService, array $data)
    {
        $voiceService
            // set api essential parameters
            ->setCpeId($data['cpeId'])

            // set data
            ->setServiceIdentifier('cpeid', $data['cpeId'])
            ->setServiceIdentifier('directory_number', $data['directory_number'])
            ->setServiceParameter('area_code', $data['area_code'])
            ->setServiceParameter('registrar', $data['registrar'])
            ->setServiceParameter('username', $data['username'])
            ->setServiceParameter('password', $data['password'])
            ->setServiceParameter('directory_number', $data['directory_number']);

        $this->setDefaultEventHandlerToVoiceService($voiceService);

        return $voiceService;
    }

    /**
     * Set onError- and onSuccess-handler for voice service
     * 
     * @param AbstractService $voiceService
     * 
     * @return AbstractService
     */
    public function setDefaultEventHandlerToVoiceService(AbstractService $voiceService)
    {
        $voiceService
            // set error handler
            ->onError(function (\Exception $exception) use ($voiceService) {
                $this->log(sprintf('%s - %s', $exception->getCode(), $exception->getMessage()), 
                    sprintf('error.%s.%s.%s',
                        $voiceService->getAction(),
                        $voiceService->getType(),
                        $voiceService->getServiceParameter('directory_number')
                    )
                );
            })

            // set success handler
            ->onSuccess(function (\stdClass $curlResponse) use ($voiceService) {
                $this->log(sprintf('%s - %s', $curlResponse->code, $curlResponse->message), 
                    sprintf('success.%s.%s.%s',
                        $voiceService->getAction(),
                        $voiceService->getType(),
                        $voiceService->getServiceParameter('directory_number')
                    )
                );
            });

        return $voiceService;
    }

    /**
     * Create a voice service object
     * 
     * @param string $type
     * @param array $data
     * 
     * @return AbstractService
     */
    private function createVoiceService($type, array $data)
    {
        switch ($type) {
            case 'activate':
                return $this->createActiveVoiceService($data);

            case 'modify':
                return $this->setDataToVoiceService(new ModifyVoiceService(), $data);

            case 'deactivate':
                return $this->setDataToVoiceService(new DeactivateVoiceService(), $data);

            case 'reactivate':
                return $this->setDataToVoiceService(new ReactivateVoiceService(), $data);

            case 'delete':
                return $this->setDataToVoiceService(new DeleteVoiceService(), $data);
        }

        return null;
    }

    /**
     * Create a ActivateVoiceService object
     * 
     * @param array $data
     * 
     * @return ActivateVoiceService
     */
    private function createActiveVoiceService(array $data)
    {
        $voiceService = $this->setDataToVoiceService(new ActivateVoiceService(), $data);

        $voiceService
            // set an error handler
            ->onError(function (\Exception $exception) use ($voiceService) {
                if (false !== strpos($exception->getMessage(), 'already activated')) {
                    // looks like this service already existed
                    // try to modify service

                    $this->log(sprintf('%s - %s', $exception->getCode(), $exception->getMessage()), 
                        'error.activate.voice.'.$voiceService->getServiceParameter('directory_number')
                    );

                    $axirosAcsApi = $this->createAxirosAcsApiInstance();

                    $modifyVoiceService = $this->createModifyVoiceServiceFromActivateVoiceService($voiceService);

                    $axirosAcsApi->addService($modifyVoiceService);
                    $axirosAcsApi->writeServices();
                }
            })
            
            // set a success handler
            ->onSuccess(function (\stdClass $curlResponse) use ($voiceService) {
                $this->log(sprintf('%s - %s', $curlResponse->code, $curlResponse->message), 
                    'success.activate.voice.'.$voiceService->getServiceParameter('directory_number')
                );
            });

        return $voiceService;
    }

    /**
     * Create a pppoe service object
     * 
     * @param string $type
     * @param array $data
     * 
     * @return AbstractService
     */
    private function createPppoeService($type, array $data)
    {
        switch ($type) {
            case 'activate':
                return $this->createActivePppoeService($data);

            case 'modify':
                return $this->setDataToPppoeService(new ModifyPppoeService(), $data);

            case 'deactivate':
                return $this->setDataToPppoeService(new DeactivatePppoeService(), $data);

            case 'reactivate':
                return $this->setDataToPppoeService(new ReactivatePppoeService(), $data);

            case 'delete':
                return $this->setDataToPppoeService(new DeletePppoeService(), $data);
        }

        return null;
    }

    /**
     * Create a ActivatePppoeService object
     * 
     * @param array $data
     * 
     * @return ActivatePppoeService
     */
    private function createActivatePppoeService(array $data)
    {
        $pppoeService = $this->setDataToPppoeService(new ActivatePppoeService(), $data);

        $pppoeService
            // set an error handler
            ->onError(function (\Exception $exception) use ($pppoeService) {
                if (false !== strpos($exception->getMessage(), 'already activated')) {
                    // looks like this service already existed
                    // try to modify service

                    $this->log(sprintf('%s - %s', $exception->getCode(), $exception->getMessage()), 
                        'error.activate.pppoe.'.$pppoeService->getServiceParameter('username')
                    );

                    $axirosAcsApi = $this->createAxirosAcsApiInstance();

                    $modifyPppoeService = $this->createModifyPppoeServiceFromActivatePppoeService($pppoeService);

                    $axirosAcsApi->addService($modifyPppoeService);
                    $axirosAcsApi->writeServices();
                }
            })

            // set a success handler
            ->onSuccess(function (\stdClass $curlResponse) use ($pppoeService) {
                $this->log(sprintf('%s - %s', $curlResponse->code, $curlResponse->message), 
                    'success.activate.pppoe.'.$pppoeService->getServiceParameter('username')
                );
            });

        return $pppoeService;
    }

    /**
     * Create a ModifyPppoeService object from ActivatePppoeService object
     * 
     * @param ActivatePppoeService $activatePppoeService
     * 
     * @return ModifyPppoeService
     */
    private function createModifyPppoeServiceFromActivatePppoeService(ActivatePppoeService $activatePppoeService)
    {
        $modifyPppoeService = $activatePppoeService->convertToModifyPppoeService(); // create ModifyPppoeService

        // set error handler
        $modifyPppoeService->onError(function (\Exception $exception) use ($modifyPppoeService) {
            $this->log(sprintf('%s - %s', $exception->getCode(), $exception->getMessage()), 
                'error.modify.pppoe.'.$modifyPppoeService->getServiceParameter('username')
            );
        })

        // set success handler
        ->onSuccess(function (\stdClass $curlResponse) use ($modifyPppoeService) {
            $this->log(sprintf('%s - %s', $curlResponse->code, $curlResponse->message), 
                'success.modify.pppoe.'.$modifyPppoeService->getServiceParameter('username')
            );
        });

        return $modifyPppoeService;
    }

    /**
     * Create a ModifyVoiceService object from ActivateVoiceService object
     * 
     * @param ActivateVoiceService $activateVoiceService
     * 
     * @return ModifyVoiceService
     */
    private function createModifyVoiceServiceFromActivateVoiceService(ActivateVoiceService $activateVoiceService)
    {
        $modifyVoiceService = $activateVoiceService->convertToModifyVoiceService(); // create ModifyVoiceService

        // set error handler
        $modifyVoiceService->onError(function (\Exception $exception) use ($modifyVoiceService) {
            $this->log(sprintf('%s - %s', $exception->getCode(), $exception->getMessage()), 
                'error.modify.voice.'.$modifyVoiceService->getServiceParameter('directory_number')
            );
        })

        // set success handler
        ->onSuccess(function (\stdClass $curlResponse) use ($modifyVoiceService) {
            $this->log(sprintf('%s - %s', $curlResponse->code, $curlResponse->message), 
                'success.modify.voice.'.$modifyVoiceService->getServiceParameter('directory_number')
            );
        });

        return $modifyVoiceService;
    }

    /**
     * Get PPPoE-PIN from customer
     * 
     * @param integer $customerId
     * 
     * @return string|null
     */
    protected function getPppoePinFromCustomer($customerId)
    {
        $db = $this->getDoctrine()->getManager()->getConnection();

        $stmt = $db->prepare("SELECT `pppoe_pin` FROM `customers` WHERE `id` = ?");
        $stmt->bindValue(1, $customerId);
        $stmt->execute();

        $customer = $stmt->fetchAll();

        return $customer[0]['pppoe_pin'];
    }

    /**
     * Create an instance of ActivatePppoeService
     * 
     * @return AxirosAcsServiceApi
     */
    private function createAxirosAcsApiInstance()
    {
        $axirosAcsApiParameters = \Wisotel\Configuration\Configuration::get('axirosAcs');

        return new AxirosAcsServiceApi(
            $axirosAcsApiParameters['username'],
            $axirosAcsApiParameters['password'],
            $axirosAcsApiParameters['url']
        );
    }
}
