<?php

/**
 * 
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Exception\RedirectException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Psr\Log\LoggerInterface;

/**
 * 
 */
class DefaultController extends BaseController
{
    /**
     * @Route("/login", name="login")
     * @Route("/check_login", name="check_login")
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/changelog", name="changelog")
     */
    public function changelogAction(Request $request)
    {
        return $this->render('maintenance/changelog.html.twig', []);
    }

    /**
     * @Route("/customer/{customerId}/delete-purtel-account/{id}", name="delete-purtel-account")
     * @Security("has_role('ROLE_EDIT_CUSTOMER_PURTEL_ACCOUNT')")
     */
    public function deletePurtelAccount(Request $request, \AppBundle\Entity\PurtelAccount $purtelAccount, $customerId)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($purtelAccount);
        $em->flush();

        $request->getSession()->getFlashBag()->add('success', sprintf('Purtel-Account %s wurde gelöscht.<br /><br />Achtung!<br />Account manuell bei Purtel löschen!<br />', 
            $purtelAccount->getPurtelLogin()
        ));

        if (null !== $request->headers->get('referer')) {
            return $this->redirect($request->headers->get('referer'));
        }

        return $this->redirectToRoute('customer-overview', ['id' => $customerId]);
    }

    /**
     * @Route("/redirect-to-customer/{redirectBy}/{value}", name="redirect-to-customer")
     */
    public function redirectToCustomerByClientIdAction(Request $request, $redirectBy, $value)
    {
        $slugToColumn = [
            'client-id' => 'clientid',
        ];

        if (!isset($slugToColumn[$redirectBy])) {
            throw new NotFoundHttpException('Invalid route');
        }

        $column = $slugToColumn[$redirectBy];

        $db = $this->getDoctrine()->getManager()->getConnection();

        $stmt = $db->prepare("SELECT `id` FROM `customers` WHERE `".$column."` = ?");
        $stmt->bindValue(1, $value);
        $stmt->execute();

        if ($stmt->rowCount() < 1) {
            throw new NotFoundHttpException('Customer not found');
        }

        $clientId = $stmt->fetch();

        if ($stmt->rowCount() > 1) {
            // redirect to customer search
            $request->getSession()->getFlashBag()->add('customerSearch.query', $value);

            $url = $this->generateUrl('customer-search');
            $url = urldecode($url); // need to decode because of wkv's ?& in urls - symfony encodes them by default
            
            return $this->redirect($url);
        }

        $url = $this->generateUrl('customer-overview', ['id' => $clientId['id']]);
        $url = urldecode($url); // need to decode because of wkv's ?& in urls - symfony encodes them by default

        return $this->redirect($url);
    }

    /**
     * Globale DSLAM Kommandos
     * 
     * @Route("/dslam/", name="global-dslam-commands")
     */
    public function globalDslamCommandsAction()
    {
        require $this->get('kernel')->getRootDir().'/../web/dslam/index.php';

        exit;
    }

    /**
     * Update changes at purtel accounts
     * 
     * @Route("/customer/ajaxPurtelChange.php", name="update-purtel-accounts")
     */
    public function updatePurtelAccountsAction(Request $request)
    {
        try {
            $content = json_decode($request->get('Purtel'), true);

            if (JSON_ERROR_NONE !== json_last_error()) {
                throw new BadRequestHttpException(sprintf('Invalid json was send. Error-Code: %s',
                    json_last_error()
                ));
            }
        } catch (BadRequestHttpException $exception) {
            return new JsonResponse(['status' => '0', 'sql' => $exception->getMessage()]);
        }

        require_once $this->get('kernel')->getRootDir().'/../web/_conf/database.inc';
        require_once $this->get('kernel')->getRootDir().'/../web/customer/_conf/database.inc';
        require_once $this->get('kernel')->getRootDir().'/../web/customer/_conf/form.inc';

        $match = array();
        $status = 1;
        $statusSql = '';

        foreach ($content as $key => $value) {
            preg_match("/(\D+)_(\d+)/", $key, $match);

            $field = $save_purtel_fields[$match[1]];
            $id = $match[2];

            $sql_update = "UPDATE purtel_account SET \n";
            $sql_update .= $field." = '".$value."' \n";
            $sql_update .= "WHERE id = '".$id."'\n";

            $db_update_result = $db->query($sql_update);

            if (!$db_update_result) {
                $status = 0;
                $statusSql = $sql_update;

                break;
            }
        }

        return new Response(json_encode([
            'status' => $status,
            'sql' => $statusSql,
        ]));
    }

    /**
     * Convert prospective customer to actual customer
     * 
     * @Route("/customer/ajaxChange2Customer.php", name="convert-prospective-customer-to-customer")
     */
    public function convertProspectiveCustomerToCustomerAction(Request $request)
    {
        $CustomerId = (int) $request->get('CustomerId');

        require_once $this->get('kernel')->getRootDir().'/../web/_conf/database.inc';
        require_once $this->get('kernel')->getRootDir().'/../web/customer/_conf/database.inc';

        $db_networks_id = $db->prepare($sql['networks_id']);
        $db_networks_id->bind_param('d', $CustomerId);
        $db_networks_id->execute();

        $db_networks_id_result = $db_networks_id->get_result();

        if ($db_networks_id_result->num_rows < 1) {
            return new JsonResponse([
                'status' => false,
                'returnStr' => '<br><span class="red">Bitte wählen Sie zuerst ein Netz aus.</span>',
                'clientId' => null,
            ]);
        }

        $networks_id = $db_networks_id_result->fetch_assoc();
        $db_networks_id_result->close();

        $interest_clientid  = $networks_id['id_string'].'.'.date('y').'.%';
        $db_max_clientid = $db->prepare($sql['max_clientid']);
        
        $db_max_clientid->bind_param('s', $interest_clientid);
        $db_max_clientid->execute();
        
        $db_max_clientid_result = $db_max_clientid->get_result();

        $max_clientid = $db_max_clientid_result->fetch_row();

        $db_max_clientid_result->free_result();
        $db_max_clientid->close();

        $new_clientid = 1;

        if (null !== $max_clientid[0]) {
            $max_clientid_arr = explode('.', $max_clientid[0]);
            $index = end($max_clientid_arr);
            $new_clientid = $index + 1;
        }
        
        $new_clientid = $networks_id['id_string'].".".date('y').".".sprintf ("%04d", $new_clientid);
        $sql_save = "UPDATE customers SET clientid = '".$new_clientid."' WHERE id ='".$CustomerId."'";
        $db_save_result = $db->query($sql_save);
        
        if ($db_save_result) {
            $customer_saved = "<br><span class='green'>Interessent wurde in Kunde umgewandelt</span>";
        } else {
            $customer_saved = "<br><span class='red'>Interessent konnte nicht umgewandelt werden</span>";
        }

        $db->close();

        return new Response(json_encode([
            'status' => $db_save_result,
            'returnStr' => $customer_saved,
            'clientId' => $new_clientid,
        ]));
    }
    
    /**
     * @Route("/", name="homepage")
     * @Route("/index{whatever}", name="_omepage")
     */
    public function indexAction(Request $request, LoggerInterface $logger)
    {
        // ugly hack to be able to use webtestcase
        // because get and post variables will be set by webserver but not by webtestcase
        if ('cli' === PHP_SAPI) {
            foreach ($request->query->all() as $key => $value) {
                $_GET[$key] = $value;
            }

            foreach ($request->request->all() as $key => $value) {
                $_POST[$key] = $value;
            }
        }

        $session = $request->getSession();

        if (!$session->isStarted()) {
            $session->start();
        }
//die(var_dump( $this->getParameter('enabled_modules', null) )); // Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
        
        $doctrine = $this->getDoctrine();

        require_once __DIR__.'/../../../web/config.inc';
        require_once $StartPath.'/_conf/database.inc';
        //require_once $StartPath.'/_conf/menu.inc';
        require_once $StartPath.'/_inc/Mikrotik_api.php';
        //require_once $StartPath.'/vendor/wisotel/session/FlashBag.php';
        require_once $StartPath.'/vendor/wisotel/configuration/Configuration.php';

        //session_start();

        /*if (!isset($_SESSION['flashBag'])) {
            $_SESSION['flashBag'] = new \Wisotel\Session\FlashBag();
        }*/

        # Login
        $ForceLoginDiv  = '';
        /*if (!key_exists('login', $_SESSION) || $_SESSION['login'] == '') {
            $ForceLoginDiv = '<div id="ForceLogin"></div>';
        }*/

        // needed for compatibily reasons
        $menu_start = 'overview';
        $menu = array(
            'overview'        => 'Übersicht:i:',
            'statistics'      => 'Statistiken:i:',
            'csvExport'       => 'CSV Export:i:',
            'accounting'      => 'Buchhaltung:i:',
            #'partner'         => 'Partner:e:',
            'customerSearch'  => 'Kundensuche:e:',
            'customer:1'      => 'Neuer Interessent:e:interessent',
            'customer:2'      => 'Neuer Vertrag:e:vertrag',
            'customer'        => 'Ändern:h:',
            'network'         => 'Netzübersicht:w:',
            'iprange'         => 'IP-Block Übersicht:w:',
            'location'        => 'Standorte:h:',
            'card'            => 'DSLAM-Karten:h:',
            'lsaKvzPartition' => 'LSA-KVz Zuordnungen:h:',
            'technics'        => 'Technik Hilfe:w:',
            'administration'  => 'Administration:w:',
            'account'         => 'Anmeldung:e:',
        );

        # getmenu select
        $SelMenuName = '';
        if (isset($_GET['menu'])) {
            $SelMenu = $_GET['menu'];
            if (isset($menu[$SelMenu])) $SelMenuName = $menu[$SelMenu];
        }
        if (!$SelMenuName) $SelMenu = $menu_start;
        list($SelPath) = explode (':', $SelMenu);

        $GLOBALS['SelPath'] = & $SelPath;

        $_SESSION['kv_config']['system']['startPath'] = $StartPath;
        $_SESSION['kv_config']['system']['startURL']  = $StartURL;
        //$_SESSION['kv_config']['system']['selMenu']   = $SelMenu;
        $_SESSION['kv_config']['system']['selPath']   = $SelPath;

        #get side
        $SelSide = 'index.php';
        if (isset($_GET['side'])) $SelSide = $_GET['side'].'.php';
        if (!file_exists($StartPath.'/'.$SelPath.'/'.$SelSide)) $SelSide = 'index.php';

        ob_start();

        # get header
        require_once $StartPath.'/header.php';
        $IncHeader = ob_get_contents();
        ob_clean();

        # get footer 
        require_once $StartPath.'/footer.php';
        $IncFooter = ob_get_contents();
        ob_clean();

        # get menu 
        //require_once $StartPath.'/menu.php';
        //$IncMenu = ob_get_contents();
        $IncMenu = $this->renderView('mainMenu.html.twig');
        ob_clean();

        try {
            # get link
            require_once $StartPath.'/'.$SelPath.'/'.$SelSide;
            $SelContent = ob_get_contents();
            ob_clean();
        } catch (RedirectException $exception) {
            // ugly workarount for "header('Location:..') from within the required file"
            // because session->flashbag won't get saved with the "header('Location:..')"-way
            return $this->redirect($exception->getMessage());
        }

        # get button 
        if (file_exists($StartPath.'/'.$SelPath.'/button.php')) {
            require_once $StartPath.'/'.$SelPath.'/button.php';
            $IncButton = ob_get_contents();
            ob_clean();
        } else {
            $IncButton = '';
        }

        ob_end_clean();

        $Start  = "<input name='StartPath' id='StartPath' type='hidden' value='".urlencode($StartPath)."'>";
        $Start .= "<input name='SelPath' id='SelPath' type='hidden' value='".urlencode($SelPath)."'>";
        $Start .= "<input name='StartURL' id='StartURL' type='hidden' value='".urlencode($StartURL)."'>";

        // to avoid Undefined index: login
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $loginFull = $user->getFullName();
        $loginUser = $user->getUsername();
        $dbHost = $doctrine->getConnection()->getHost();
        $dbDatabase = $doctrine->getConnection()->getDatabase();

        $databaseGwHost = isset($databaseegw['host']) ? $databaseegw['host'] : '';
        $databaseGwDatabase = isset($databaseegw['database']) ? $databaseegw['database'] : '';

        $userNotificationsColorization = [
            'error' => 'red',
            'warning' => 'orange',
            'success' => 'green',
        ];

        $userNotificationString = '';

        foreach ($userNotificationsColorization as $notificationType => $notificationColorCode) {
            $notificationMessages = $request->getSession()->getFlashBag()->get($notificationType);

            if (!empty($notificationMessages)) {
                foreach ($notificationMessages as $notificationMessage) {
                    $userNotificationString .= sprintf('<span class="text" style="font-size: 20px; font-weight: bold; color: %s">%s</span><br />',
                        $notificationColorCode,
                        $notificationMessage
                    );
                }
            }
        }

        # output content
        $response = <<<HTML
        $IncHeader
        <div id="Container" class="clearfix">
          <div id="Navigation">
            <div id="Logo"><img src="$StartURL/_img/logo.png"></div>
            $IncMenu
            $IncButton
            Version: $KVVesion<br />
            Login: {$loginFull}<br />
            {$database['host']} ({$database['database']})<br />
            {$databaseGwHost} ({$databaseGwDatabase})<br />
            $db_error
            $dbegw_error
          </div>
          <div id="Content">
            $userNotificationString
            $SelContent
          </div>
        $Start  
        </div>
        <div id="LoginBack">
        </div>
        $ForceLoginDiv
        $IncFooter
HTML;
        return new Response($response);
    }

    /**
     * @Route("/customer/ajaxGroupware.php", name="ajaxGroupware.php")
     */
    public function ajaxGroupwareAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if (!($user instanceOf \AppBundle\Entity\User)) {
            return new JsonResponse(['url' => '']);
        }

        $groupwareUsername = $user->getGroupwareUsername();

        if (empty($groupwareUsername)) {
            return new JsonResponse(['url' => '']);
        }

        $track = $this->container->getParameter('groupware');

        $trackCust = json_decode($_POST['Cust'], true);
        $trackCustHead = json_decode($_POST['CustHead'], true);
        $url = '';
        $tr_id = 0;
        $var_set = '';
        
        require_once $this->get('kernel')->getRootDir().'/../web/_conf/database.inc';

        $sql = "SELECT * FROM networks WHERE id = ".$trackCust['Netze'];
        $db_save_result = $db->query($sql);
        if ($db_save_result) {
            $netdb = $db_save_result->fetch_assoc();
            $trackCustHead['net'] = $netdb['id_string'];
        } else {
            $trackCustHead['net'] = '';
        }

        $sql = "SELECT account_id FROM egw_accounts WHERE account_lid = '".$groupwareUsername."'";
        $dbegw_user = $dbegw->query($sql);

        if (!empty($dbegw->errno) || $dbegw_user->num_rows !== 1) {
            return new JsonResponse(['url' => '']);
        }

        $groupwareUserId = $dbegw_user->fetch_row();
        $groupwareUserId = $groupwareUserId[0];

        $track['tr_creator'] = $groupwareUserId;
        $track['tr_created'] = time();
        $track['tr_startdate'] = time();

        foreach ($track as $key => $value) {
            if ($key == 'tr_summary') {
                $value = str_replace('#clientid#', $trackCustHead['clientid'], $value);
                $value = str_replace('#net#', $trackCustHead['net'], $value);
                $value = str_replace('#name#', $trackCustHead['name'], $value);
                $track['tr_summary'] = $value;
            }

            $var_set .= "$key = '$value',\n";
        }

        $var_set = substr($var_set, 0, -2);

        $sql = "INSERT INTO egw_tracker SET $var_set";
        $db_save_result = $dbegw->query($sql);
        $tr_id = $dbegw->insert_id;

        $track['tr_summary'] = '#'.$tr_id.' '.$track['tr_summary'];
        $sql = "UPDATE egw_tracker SET tr_summary = '".$track['tr_summary']."' WHERE tr_id = ".$tr_id;
        $db_save_result = $dbegw->query($sql);

        $sql = '';
        foreach ($trackCust as $key => $value) {
            $sql .= "INSERT INTO egw_tracker_extra SET tr_id = '$tr_id', tr_extra_name = '$key', tr_extra_value = '$value';\n"; 
        } 

        $db_save_result = $dbegw->multi_query($sql);

        $url = sprintf("%s://%s:%u%s/index.php?menuaction=tracker.tracker_ui.edit&tr_id=%s&no_popup=1",
            \Wisotel\Configuration\Configuration::get('groupwareProtocol'),
            \Wisotel\Configuration\Configuration::get('groupwareIpAddress'),
            \Wisotel\Configuration\Configuration::get('groupwarePort'),
            \Wisotel\Configuration\Configuration::get('groupwareSubPath'),
            $tr_id
        );

        $dbegw->close();

        $db = $this->getDoctrine()->getManager()->getConnection();

        $note = "Es wurde ein Ticket mit der ID = #".$tr_id." erstellt<br>Link: <a href='".$url."' target='_blank'>".$url."</a>";

        $stmt = $db->prepare("INSERT INTO `customer_history` SET `customer_id` = ?, event_id = ?, 
            created_by = ?, created_at = NOW(), note = ?");

        $stmt->execute([
            $trackCustHead['id'],
            39,
            $user->getUsername(),
            $note
        ]);

        return new JsonResponse(['url' => $url]);
    }

    /**
     * @Route("/axiros-acs/{customerId}", name="axiros-acs");
     */
    public function axirosAcsAction(Request $request, $customerId)
    {
        $db = $this->getDoctrine()->getManager()->getConnection();

        $stmt = $db->prepare("SELECT * FROM `customers` WHERE id = ?");
        $stmt->bindValue(1, $customerId);
        $stmt->execute();

        $customer = $stmt->fetchAll();

        if (!is_array($customer) || 1 !== count($customer)) {
            die('can not be here');
        }

        $customer = $customer[0];

        $stmt = $db->prepare("SELECT `area_code` FROM `networks` WHERE id = ?");
        $stmt->bindValue(1, $customer['network_id']);
        $stmt->execute();

        $areaCode = $stmt->fetchAll();

        if (!is_array($areaCode) || 1 !== count($areaCode)) {
            die('can not be here');
        }

        $areaCode = preg_replace('/^0/', '', $areaCode[0]['area_code']);

        /*
        acs['custId']     = $("#CustomerId").val();
        acs['clientId']   = $("#CustomerClientid10").val();
        acs['macAddress'] = $("#CustomerMacAddress10").val();
        acs['password']   = encodeURIComponent($("#CustomerPassword10").val());
        acs['version']    = $("#CustomerVersion").val();
        acs['area_code']  = $("#area_code").val();

        $_POST['type'] = both
        $_POST['action'] = activate
        */

        $typeParam = 'both';
        $action = 'activate';
        $voiceServiceName = 'voice';

        $acs = [
            'custId' => $customer['id'],
            'clientId' => $customer['clientid'],
            'macAddress' => $customer['mac_address'],
            'password' => $customer['password'],
            'version' => $customer['version'],
            'area_code' => $areaCode,
        ];


        require_once $this->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';
        require_once $this->get('kernel')->getRootDir().'/../web/customer/Lib/PppoeUsername/PppoeUsername.php';

        $axirosAcs = \Wisotel\Configuration\Configuration::get('axirosAcs');




        $info       = '';
        $status     = 0;
        $username   = $axirosAcs['username'];
        $password   = $axirosAcs['password'];
        $cpe_url    = $axirosAcs['url']."/live/CPEManager/DMInterfaces/rest/v1/action/%s";
        $cpe_action = 'DeleteCPEs';
        $cpe_param  = array();
        $acs_url    = $axirosAcs['url']."/live/CPEManager/AXServiceStorage/Interfaces/rest/v1/services/%s/cpeid/%s/action/%s";
        $acs_param  = array();
        $acs_pppoe  = array();
        $acs_voice  = array();
        $pur_voice  = array();

        $sql = "SELECT * FROM purtel_account WHERE cust_id = ".$acs['custId'];

        $acs['registrar'] = $axirosAcs['registrar'];
        // doesn't always work this way
        //$acs['acsId']     = substr($acs['macAddress'],7,12);
        //$acs['acsPw']     = substr($acs['macAddress'],-12,12);

        // extract mac-address from "macAddress"
        $acs['acsId'] = '';

        if (!empty($acs['macAddress'])) {
        $matchMac = null;

        preg_match('/([0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2})|([0-9a-f]{12})/i', $acs['macAddress'], $matchMac);

        if (isset($matchMac[0])) {
            $acs['acsId'] = preg_replace('/[^0-9a-f]/i', '', $matchMac[0]);

            // extract fritzbox-gui-password from "macAddress"
            $matchPw = explode($matchMac[0], $acs['macAddress']);

            if (2 === count($matchPw) && !empty($matchPw[1])) {
                $acs['acsPw'] = $matchPw[1];
            }
        }
        }

        if ($acs['acsId']) {
            $acs_pppoe['ServiceIdentifiers'] = array (
                'cpeid' => $acs['acsId']
            );
            $acs_pppoe['CommandOptions'] = array();

            if ($action == 'activate' && $action == 'modify') $acs_pppoe['ServiceParameters'] = array();

            $cpe_param['CPESearchOptions'] = array (
                'cpeid' => $acs['acsId']
            );

            $cpe_param['CommandOptions'] = array();
        }

        $db_purtel_result = $db->query($sql);

        while ($temp = $db_purtel_result->fetch(\PDO::FETCH_ASSOC)) {
            //$temp['nummer'] = preg_replace('/^0/', '49', $temp['nummer']);

            array_push ($pur_voice, $temp);

            $acs_param['ServiceIdentifiers'] = array (
                'cpeid'             => $acs['acsId'],
                'directory_number'  => $temp['nummer']
            );

            $acs_param['CommandOptions'] = array();

            if ($action == 'activate' && $action == 'modify') $acs_param['ServiceParameters'] = array();

            array_push ($acs_voice, $acs_param);
        }




        //////////////////////
        switch ($action) {
            case 'activate':
            case 'modify':
                if ($acs['acsId']) {
                    //$acs_pppoe['ServiceParameters']['username'] = $acs['clientId'];

                    $pppoePin = $customer['pppoe_pin'];
                    $acs_pppoe['ServiceParameters']['username'] = \PppoeUsername::create($pppoePin, $acs['clientId']);
                    $acs_pppoe['ServiceParameters']['password'] = $acs['password'];

                    foreach ($acs_voice as $key => $value) {
                        $acs_voice[$key]['ServiceParameters']['area_code']        = $acs['area_code'];
                        $acs_voice[$key]['ServiceParameters']['registrar']        = $acs['registrar'];
                        $acs_voice[$key]['ServiceParameters']['username']         = $pur_voice[$key]['purtel_login'];
                        $acs_voice[$key]['ServiceParameters']['password']         = $pur_voice[$key]['purtel_password'];
                        $acs_voice[$key]['ServiceParameters']['directory_number'] = $pur_voice[$key]['nummer'];
                        //$acs_voice[$key]['ServiceParameters']['proxy_server']     = '';
                    }
                }
            case 'deactivate':
            case 'reactivate':
            case 'delete':
                if ($acs['version']) $acs['clientId'] .= '_'.$acs['version'];

                if ($acs['acsId']) {
                    $acs_post = json_encode($acs_pppoe);
                    $acs_post = str_replace('[]', '{}', $acs_post);

                    if ($typeParam == 'pppoe' || $typeParam == 'both') {
                        $type = 'pppoe';
                        $url  = sprintf ($acs_url, $type, $acs['acsId'], $action);

                        // send 'pppoe' service
                        $curlHandler = curl_init();
                        curl_setopt($curlHandler, CURLOPT_URL, $url);
                        curl_setopt($curlHandler, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                        curl_setopt($curlHandler, CURLOPT_FRESH_CONNECT, TRUE);
                        curl_setopt($curlHandler, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                        curl_setopt($curlHandler, CURLOPT_USERPWD, $username.':'.$password);
                        curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curlHandler, CURLOPT_POST, true);
                        curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $acs_post);
                        $execstr  = curl_exec($curlHandler);
                        curl_close($curlHandler);
                        $exec     = json_decode($execstr, true);
                        
                        $info     .= '<hr>Code: '.$exec['code'].'<br />';
                        $info     .= 'Message: '.$exec['message'].'<br />';
                        $info     .= 'cpeid: '.$acs['acsId'].'<br />';
                        $info     .= 'username: '.$acs['clientId'].'<br /><br />';

                        // is an error occured while 'activate' 'pppoe' service try a 'modify' again
                        if ($action == 'activate' && $exec['code'] == '500') {
                            $action1 = 'modify';
                            $url  = sprintf ($acs_url, $type, $acs['acsId'], $action1);

                            $curlHandler = curl_init();
                            curl_setopt($curlHandler, CURLOPT_URL, $url);
                            curl_setopt($curlHandler, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                            curl_setopt($curlHandler, CURLOPT_FRESH_CONNECT, TRUE);
                            curl_setopt($curlHandler, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                            curl_setopt($curlHandler, CURLOPT_USERPWD, $username.':'.$password);
                            curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($curlHandler, CURLOPT_POST, true);
                            curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $acs_post);
                            $execstr  = curl_exec($curlHandler);
                            curl_close($curlHandler);
                            $exec     = json_decode($execstr, true);
                            $info     .= '<hr>Code: '.$exec['code'].'<br />';
                            $info     .= 'Message: '.$exec['message'].'<br />';
                            $info     .= 'cpeid: '.$acs['acsId'].'<br />';
                            $info     .= 'username: '.$acs['clientId'].'<br /><br />';
                        }
                    }

                    // send 'voice' services
                    if ($typeParam == 'voice' || $typeParam == 'both') {
                        $type = $voiceServiceName;
                        $url  = sprintf ($acs_url, $type, $acs['acsId'], $action);

                        foreach ($acs_voice as $key => $acs_param) {
                            if (isset($acs_param['ServiceParameters']['directory_number']) && empty($acs_param['ServiceParameters']['directory_number'])) {
                                // don't send sip without a phonenumber
                                $info .= 'Skipped SIP without phonenumber<br />';

                                continue;
                            }

                            

                            // send delete first
                            $deleteParams = $acs_param;

                            $deleteParams['ServiceIdentifiers']['directory_number'] = preg_replace('/^0/', '49', 
                                $deleteParams['ServiceIdentifiers']['directory_number']
                            );

                            unset($deleteParams['ServiceParameters']);

                            var_dump('delete voice (49)', $deleteParams, sprintf($acs_url, $type, $acs['acsId'], 'delete'));

                            $acs_post = json_encode($deleteParams);
                            $acs_post = str_replace('[]', '{}', $acs_post);

                            $curlHandler = curl_init();
                            curl_setopt($curlHandler, CURLOPT_URL, sprintf($acs_url, $type, $acs['acsId'], 'delete'));
                            curl_setopt($curlHandler, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                            curl_setopt($curlHandler, CURLOPT_FRESH_CONNECT, TRUE);
                            curl_setopt($curlHandler, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                            curl_setopt($curlHandler, CURLOPT_USERPWD, $username.':'.$password);
                            curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($curlHandler, CURLOPT_POST, true);
                            curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $acs_post);
                            $execstr  = curl_exec($curlHandler);
                            curl_close($curlHandler);

                            var_dump($execstr);
                            

                            // now send create
                            $acs_post = json_encode($acs_param);
                            $acs_post = str_replace('[]', '{}', $acs_post);

                            var_dump('create voice', $acs_param, $url);

                            $curlHandler = curl_init();
                            curl_setopt($curlHandler, CURLOPT_URL, $url);
                            curl_setopt($curlHandler, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                            curl_setopt($curlHandler, CURLOPT_FRESH_CONNECT, TRUE);
                            curl_setopt($curlHandler, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                            curl_setopt($curlHandler, CURLOPT_USERPWD, $username.':'.$password);
                            curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($curlHandler, CURLOPT_POST, true);
                            curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $acs_post);
                            $execstr  = curl_exec($curlHandler);

                            var_dump($execstr);

                            curl_close($curlHandler);

                            $exec     = json_decode($execstr, true);
                            $info     .= '<hr>Code: '.$exec['code'].'<br />';
                            $info     .= 'Message: '.$exec['message'].'<br />';
                            $info     .= 'cpeid: '.$acs['acsId'].'<br />';
                            $info     .= 'directory_number: '.$pur_voice[$key]['nummer'].'<br />';
                            $info     .= 'area_code: '.$acs['area_code'].'<br />';
                            $info     .= 'registrar: '.$acs['registrar'].'<br />';
                            $info     .= 'username: '.$pur_voice[$key]['purtel_login'].'<br />';
                            $info     .= 'directory_number: '.$pur_voice[$key]['nummer'].'<br /><br />';

                            if ($action == 'activate' && $exec['code'] == '500') {
                                $action1 = 'modify';
                                $url  = sprintf ($acs_url, $type, $acs['acsId'], $action1);

                                $curlHandler = curl_init();
                                curl_setopt($curlHandler, CURLOPT_URL, $url);
                                curl_setopt($curlHandler, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                                curl_setopt($curlHandler, CURLOPT_FRESH_CONNECT, TRUE);
                                curl_setopt($curlHandler, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                                curl_setopt($curlHandler, CURLOPT_USERPWD, $username.':'.$password);
                                curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($curlHandler, CURLOPT_POST, true);
                                curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $acs_post);
                                $execstr  = curl_exec($curlHandler);
                                curl_close($curlHandler);
                                $exec     = json_decode($execstr, true);
                                $info     .= '<hr>Code: '.$exec['code'].'<br />';
                                $info     .= 'Message: '.$exec['message'].'<br />';
                                $info     .= 'cpeid: '.$acs['acsId'].'<br />';
                                $info     .= 'directory_number: '.$pur_voice[$key]['nummer'].'<br />';
                                $info     .= 'area_code: '.$acs['area_code'].'<br />';
                                $info     .= 'registrar: '.$acs['registrar'].'<br />';
                                $info     .= 'username: '.$pur_voice[$key]['purtel_login'].'<br />';
                                $info     .= 'directory_number: '.$pur_voice[$key]['nummer'].'<br /><br />';
                            }
                        }
                    }
                } else {
                    $status = 1;  
                    $info = 'Keine Fritzbox angegeben!';
                }
                break;
            default:
                    $status = 1;
                    $info = "'$action' ist keine unterstützte Funktion!";
                break;
        }


        $response = array (
        'status'  => $status,
        'info'    => $info,
        );

        print_r($response);

        exit;
        //////////////////////

    }

    /**
     * Create, update or delete a purtel radius account for customer
     * 
     * @Route("/customer/ajaxPurtelRadius.php", name="create-and-update-purtel-radius")
     */
    public function createAndUpdatePurtelRadiusAction(Request $request)
    {
        $paramKeys = [
            'StartPath',
            'CustId',
            'PurtelMaster',
            'RadiusCommand',
            'RadiusUser',
            'RadiusPassword',
        ];

        $accessUp = null;
        $accessDown = null;

        foreach ($paramKeys as $key) {
            if (null !== $value = $request->request->get($key)) {
                $param[$key] = urldecode($value); 
            } elseif (null !== $value = $request->query->get($key)) {
                $param[$key] = urldecode($value);
            }
        }

        $status = 1;
        $mes = [];

        if (!isset($param['StartPath'], $param['RadiusCommand'], $param['PurtelMaster'], $param['CustId'])) {
            return new Response(0);
        }

        $db = $this->getDoctrine()->getManager()->getConnection();

        $stmt = $db->prepare("SELECT c.`clientid`, p.`down`, p.`up` FROM `customers` c 
            LEFT JOIN `produkte` p ON c.`service` = p.`produkt` 
            WHERE c.`id` = ?");
        $stmt->bindValue(1, $param['CustId']);
        $stmt->execute();

        if ($stmt->rowCount() !== 1) {
            return new Response(json_encode([
                'status' => 0,
                'mes' => 'Customer not found',
            ]));
        }

        $result = $stmt->fetch();
        $clientId = $result['clientid'];
        $accessUp = $result['up'];
        $accessDown = $result['down'];
        
        require_once $this->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';
        require_once $this->get('kernel')->getRootDir().'/../web/customer/Lib/PppoeUsername/PppoeUsername.php';

        $action = $param['RadiusCommand'];
        $username = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
        $password = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

        /*if ($param['RadiusUser'] === $clientId) {
            $purtelSipRealm = \Wisotel\Configuration\Configuration::get('purtelSipRealm');
        } else {
            $purtelSipRealm = \Wisotel\Configuration\Configuration::get('purtelSipRealmOld');
        }*/
        $pppoeUsername = explode('@', $param['RadiusUser']);
        $pppoeUsername = $pppoeUsername[0];

        $purtelRadiusVia = \Wisotel\Configuration\Configuration::get('purtelRadiusVia');
        $purtelRadiusAcs = \Wisotel\Configuration\Configuration::get('purtelRadiusAcs');

        $Purtel = [
            'anschluss' => $param['PurtelMaster'],
            'via' => $purtelRadiusVia,
            //'realm' => $purtelSipRealm,
            'realm' => \PppoeUsername::getRealm($pppoeUsername, $clientId),
        ];

        switch ($action){                         
            case 'create_radius':
                /*if (key_exists('RadiusUser', $param)) {
                    $Purtel['radius_benutzername'] = $param['RadiusUser'];    
                }*/
                $Purtel['radius_benutzername'] = $pppoeUsername;

                $Purtel['acs'] = $purtelRadiusAcs;
                $Purtel['radius_passwort'] = $param['RadiusPassword'];
                $PurtelUrl = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
                    urlencode($username),
                    urlencode($password),
                    urlencode($action)
                );

                foreach ($Purtel as $key => $value) {
                    $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
                }
                $curl = curl_init($PurtelUrl);

                curl_setopt_array($curl, array(
                    CURLOPT_URL             => $PurtelUrl,
                    CURLOPT_HEADER          => false,
                    CURLOPT_RETURNTRANSFER  => true,
                    CURLOPT_SSL_VERIFYPEER  => false,
                ));

                $result = curl_exec($curl);

                curl_close($curl);

                $resultArray = str_getcsv($result, ';');

                if(strpos ($resultArray[0], '-ERR') === 0) {
                    $status = 0;
                    $mes[] = $result;
                    break;
                }

                $mes[] = $result;
                $action = 'update_radius';
            case 'update_radius':
                if (isset($param['RadiusPassword'])) {
                    $Purtel['radius_passwort'] = $param['RadiusPassword'];
                }

                $Purtel['access_upstream'] = empty($accessUp) ? 'loeschen' : $accessUp;
                $Purtel['access_downstream'] = empty($accessUp) ? 'loeschen' : $accessDown;
            case 'close_radius':
            case 'open_radius':
            case 'cancel_radius':
            case 'status_radius':
                $PurtelUrl = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
                    urlencode($username),
                    urlencode($password),
                    urlencode($action)
                );

                foreach ($Purtel as $key => $value) {
                    $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
                }

                $curl = curl_init($PurtelUrl);

                curl_setopt_array($curl, array(
                    CURLOPT_URL             => $PurtelUrl,
                    CURLOPT_HEADER          => false,
                    CURLOPT_RETURNTRANSFER  => true,
                    CURLOPT_SSL_VERIFYPEER  => false,
                ));

                $result = curl_exec($curl);

                curl_close($curl);

                $resultArray = str_getcsv($result, ';');

                if(strpos($resultArray[0], '-ERR') === 0) {
                    $status = 0;
                }
                $mes[] = $result;
                break;
            default:
                $status = 0;
                $mes[] = 'Falsches Radiuskommando';
        }

        $response = array(
            'status' => $status,
            'mes' => utf8_encode(implode("\n", $mes)),
        );

        return new Response(json_encode($response));
    }
}
