<?php

/**
 *
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\MyAccount\ChangePasswordType;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use AppBundle\Exception\IncorrectPasswordException;

/**
 * @Route("/my-account")
 */
class MyAccountController extends BaseController
{
    /**
     * @Route("/", name="my-account")
     */
    public function myAccountAction(Request $request)
    {
        return $this->render('my-account/my-account.html.php');
    }

    /**
     * @Route("/change-password", name="my-account-change-password")
     */
    public function changePasswordAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $changePasswordForm = $this->createForm(ChangePasswordType::class, $user);
        $changePasswordForm->handleRequest($request);

        if ($changePasswordForm->isSubmitted()) {
            if ($changePasswordForm->isValid()) {
                try {
                    if (!empty($user->getNewPassword())) {
                        $newPassword = $user->getNewPassword();
                        $newPasswordRepeated = $user->getNewPasswordRepeated();

                        if (!empty($newPassword) || !empty($newPasswordRepeated)) {
                            if (empty($newPassword) || empty($newPasswordRepeated)) {
                                throw new IncorrectPasswordException('Passwort und Passwort wiederholen dürfen nicht leer sein.');
                            }

                            if (!($newPassword === $newPasswordRepeated)) {
                                throw new IncorrectPasswordException('Passwort und Passwort wiederholen stimmen nicht überein.');
                            }

                            $passwordEncoder = $this->get('security.password_encoder');

                            if (!$passwordEncoder->isPasswordValid($user, $user->getCurrentPassword())) {
                                throw new IncorrectPasswordException('Das alte Password ist falsch.');
                            }

                            // update pw
                            $encodedPassword = $passwordEncoder->encodePassword($user, $newPassword);

                            $user->setPassword($encodedPassword);
                        }
                    }

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();

                    $request->getSession()->getFlashBag()->add('success', 'Konto aktualisiert.');

                    return $this->redirectToRoute('my-account-change-password');
                } catch (IncorrectPasswordException $e) {
                    $request->getSession()->getFlashBag()->add('error', $e->getMessage());
                } catch (UniqueConstraintViolationException $e) {
                    $request->getSession()->getFlashBag()->add('error', $e->getMessage());
                }
            } else {
                $errorString = '';

                foreach ($form->getErrors(true) as $error) {
                    $errorString .= '('.$error->getOrigin()->getName().') '.$error->getMessage();
                }

                $request->getSession()->getFlashBag()->add('error', sprintf(
                    'Fehler<br />%s', $errorString
                ));
            }
        }

        return $this->render('my-account/change-password.html.php', [
            'changePasswordForm' => $changePasswordForm->createView(),
        ]);
    }
}
