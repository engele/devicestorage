<?php

/**
 * 
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\IpRange;
use AppBundle\Form\Type\IpRangeType;

/**
 * @Route("/ip-range")
 */
class IpRangeController extends BaseController
{    
    /**
     * @Route("/", name="ip-range")
     */
    public function indexAction(Request $request)
    {
        $ipRanges = $this->getDoctrine()->getRepository('AppBundle\Entity\IpRange')->findAll();

        $database = $this->getDoctrine()->getManager()->getConnection();

        foreach ($ipRanges as $ipRange) {
            $statement = $database->prepare('SELECT COUNT(DISTINCT `ip_address`) as used 
                FROM `customers` 
                WHERE INET_ATON(`ip_address`) BETWEEN INET_ATON(:start) AND INET_ATON(:end)');
            
            $statement->execute(array(
                'start' => $ipRange->getStartAddress(),
                'end' => $ipRange->getEndAddress()
            ));

            $ipRange->usedAddresses = $statement->fetchAll();
            $ipRange->usedAddresses = $ipRange->usedAddresses[0]['used'];
        }

        return $this->render('ip-range/index.html.php', [
            'ipRanges' => $ipRanges,
        ]);
    }

    /**
     * @Route("/new", name="ip-range-new")
     * @Route("/edit/{ipRange}", name="ip-range-edit")
     */
    public function editAction(IpRange $ipRange = null, Request $request)
    {
        $ipRangeForm = $this->createForm(IpRangeType::class, $ipRange);
        $ipRangeForm->handleRequest($request);

        if ($ipRangeForm->isSubmitted()) {
            if ($ipRangeForm->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($ipRangeForm->getData());
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'Ip-Block gespeichert.');

                return $this->redirectToRoute('ip-range');
            } else {
                $errorString = '';

                foreach ($ipRangeForm->getErrors(true) as $error) {
                    $errorString .= '('.$error->getOrigin()->getName().') '.$error->getMessage();
                }

                $request->getSession()->getFlashBag()->add('error', sprintf(
                    'Fehler<br />%s', $errorString
                ));
            }
        }

        return $this->render('ip-range/edit.html.php', [
            'ipRangeForm' => $ipRangeForm->createView(),
        ]);
    }
}
