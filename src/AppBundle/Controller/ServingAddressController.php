<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ServingAddress;
use AppBundle\Entity\ServingAddressRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Serving-Address controller.
 *
 * @Route("/servingaddress")
 */
class ServingAddressController extends BaseController
{
    /**
     * Lists all servingAddress entities.
     *
     * @Route("/", name="servingaddress_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $query = (!empty($_GET['search_phrase'])) ? strtolower($_GET['search_phrase']) : null;
        if (isset($query)) {
            $servingAddresses = $this->searchServingAddresses($query);
        } else {
            $servingAddresses = $em->getRepository('AppBundle:ServingAddress')->findAll();
        }

        return $this->render('servingaddress/index.html.php', array(
            'servingAddresses' => $servingAddresses,
        ));
    }

    /**
     * Creates a new servingAddress entity.
     * 
     * @Route("/new", name="servingaddress_new")
     * 
     * @param Request $request
     * 
     * @return bool|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_CREATE_SERVING_ADDRESSES');

        $servingAddress = new ServingAddress();
        $form = $this->createForm('AppBundle\Form\ServingAddressType', $servingAddress);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($servingAddress);
            $em->flush();

            return $this->redirectToRoute('servingaddress_show', array('id' => $servingAddress->getId()));
        }

        return $this->render('servingaddress/new.html.php', array(
            'servingAddress' => $servingAddress,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a servingAddress entity.
     * 
     * @Route("/{id}/show", name="servingaddress_show")
     * 
     * @param ServingAddress $servingAddress
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(ServingAddress $servingAddress)
    {
        $deleteForm = $this->createDeleteForm($servingAddress);

        return $this->render('servingaddress/show.html.php', array(
            'servingAddress' => $servingAddress,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing servingAddress entity.
     * 
     * @Route("/{id}/edit", name="servingaddress_edit")
     * 
     * @param Request $request
     * @param ServingAddress $servingAddress
     * 
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, ServingAddress $servingAddress)
    {
        $this->denyAccessUnlessGranted('ROLE_EDIT_SERVING_ADDRESSES');

        $deleteForm = $this->createDeleteForm($servingAddress);
        $editForm = $this->createForm('AppBundle\Form\ServingAddressType', $servingAddress);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('servingaddress_index', array('id' => $servingAddress->getId()));
        }

        return $this->render('servingaddress/edit.html.php', array(
            'servingAddress' => $servingAddress,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a servingAddress entity.
     * 
     * @Route("/{id}/delete", name="servingaddress_delete")
     * 
     * @param Request $request
     * @param ServingAddress $servingAddress
     * 
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, ServingAddress $servingAddress)
    {
        $this->denyAccessUnlessGranted('ROLE_DELETE_SERVING_ADDRESSES');

        $form = $this->createDeleteForm($servingAddress);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($servingAddress);
            $em->flush();
        }

        return $this->redirectToRoute('servingaddress_index');
    }


    /**
     * @Route("/searchproposal", name="servingaddress_searchproposal")
     * 
     * @param Request $request
     * 
     * @return JsonResponse
     */
    public function searchproposalAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse('Error: only json-format allowed!');
        }

        $query = (!empty($_POST['search_phrase'])) ? strtolower($_POST['search_phrase']) : null;

        if (!isset($query)) {
            die('Invalid query.');
        }

        $servingAddresses = $this->searchServingAddresses($query);
        $servingAddressesArray = [];

        foreach ($servingAddresses as $servingAddress) {
            $servingAddressesArray[] = [
                "id" => $servingAddress->getId(),
                "zip_code" => $servingAddress->getZipCode(),
                "city" => $servingAddress->getCity(),
                "street" => $servingAddress->getStreet(),
                "house_number" => $servingAddress->getHouseNumber()
            ];
        }

        $status = true;

        if (empty($servingAddressesArray)) {
            $status = false;
        }

        $response = [
            'status' => $status,
            'error' => false,
            'data' => array(
                "address" => $servingAddressesArray
            )
        ];

        return new JsonResponse($response);
    }

    /**
     * Creates a form to delete a servingAddress entity.
     *
     * @param ServingAddress $servingAddress The servingAddress entity
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(ServingAddress $servingAddress)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('servingaddress_delete', array('id' => $servingAddress->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @param string $query
     * 
     * @return object[]
     */
    private function searchServingAddresses(string $query): array
    {
        /** @var ServingAddressRepository $servingAddressRepository */
        $servingAddressRepository = $this->getDoctrine()->getManager()->getRepository(ServingAddress::class);
        /** @var ServingAddress $servingAddress */
        $servingAddresses = $servingAddressRepository->findByCustom(
            ['searchPhrase' => $query],
            ['sa.id'],
            10
        );

        return $servingAddresses;
    }
}
