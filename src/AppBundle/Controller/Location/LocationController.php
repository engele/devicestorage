<?php

/**
 * 
 */

namespace AppBundle\Controller\Location;

use AppBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Location\Location;
use AppBundle\Entity\Network;
use AppBundle\Form\Type\Location\LocationType;
use AppBundle\Entity\Location\Card;
use AppBundle\Entity\Location\CardPort;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\src\DslamCommunication\Communicator\SshGateway;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * 
 */
class LocationController extends BaseController
{
    /**
     * @Route("/location/{id}/show-info-about-dependent-mdu", name="location-show-info-about-dependent-mdu");
     */
    public function showInfoAboutDependentMduAction(Request $request, Location $location)
    {
        // has this location at least one card with type 'dependent-MDU' and at least one card port with attribute 'olt-connected'
        // and is that port connected to other exsiting port
        $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare("SELECT cp.`id` FROM `card_ports` cp 
            INNER JOIN `card_ports_to_card_port_attributes` cpa ON cpa.`card_port_id` = cp.`id` 
            INNER JOIN `card_port_attributes` cp_a ON cp_a.`id` = cpa.`card_port_attribute_id` 
            INNER JOIN `cards` c ON c.`id` = cp.`card_id` 
            INNER JOIN `card_types` ct ON ct.`id` = c.`card_type_id` 
            WHERE ct.`name` = 'dependent-MDU' 
            AND cp_a.`identifier` = 'olt-connected' 
            AND cp.`card_port_id` IS NOT NULL 
            AND c.`location_id` = ?");

        $stmt->bindValue(1, $location->getId());
        $stmt->execute();

        if ($stmt->rowCount() < 1) {
            // todo: use access denied exception
            throw new \Exception('Dieser Standort erfüllt nicht alle Bedingungen um als unselbstständige MDU auf einem OLT abgefragt zu werden.');
        }

        $mduBaseCardPortId = $stmt->fetch();
        $mduBaseCardPortId = $mduBaseCardPortId['id'];
        $mduBaseCardPort = $this->getDoctrine()->getRepository('\AppBundle\Entity\Location\CardPort')->findOneById($mduBaseCardPortId);

        $parentLocation = $mduBaseCardPort->getRelatedCardPort()->getCard()->getLocation();

        $dslamCommunicator = $this->get('dslam_chooser')->findCommunicationClass(
            $parentLocation->getType(), $parentLocation->getProcessor(), $parentLocation->getCurrentSoftwareRelease()
        );

        if (null === $dslamCommunicator) {
            // todo: use access denied exception
            throw new \Exception("error - no dslam communicator found");
        }

        $dslamCommunicator
            ->setContainer(new ContainerBuilder()) // set service container to dslam communicator for dependency injection
            ->setGateway(new SshGateway($parentLocation->getIpAddress(), $parentLocation->getLogonPort(), 10)); // set connection gateway to dslam communicator

        // perform the login
        $dslamCommunicator->login(['username' => $parentLocation->getLogonUsername(), 'password' => $parentLocation->getLogonPassword()]);

        $cards = $this->getDoctrine()->getRepository('\AppBundle\Entity\Location\Card')->findByLocation($location);

        $response = '';

        foreach ($cards as $card) {
            if ('dependent-MDU' === $card->getCardType()->getName()) { // skip this card-types
                $response .= sprintf('<pre>%s</pre>', $dslamCommunicator->showInfoAboutDependentBaseMdu(
                    $card
                ));
            } else {
                $response .= sprintf('<pre>%s</pre>', $dslamCommunicator->showInfoAboutDependentMdu(
                    $card
                ));
            }
        }

        return new Response($response);
    }

    /**
     * @Route("/location/{id}/deprovision-dependent-mdu", name="location-deprovision-dependent-mdu");
     */
    public function deprovisionDependentMduAction(Request $request, Location $location)
    {
        // has this location at least one card with type 'dependent-MDU' and at least one card port with attribute 'olt-connected'
        // and is that port connected to other exsiting port
        $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare("SELECT cp.`id` FROM `card_ports` cp 
            INNER JOIN `card_ports_to_card_port_attributes` cpa ON cpa.`card_port_id` = cp.`id` 
            INNER JOIN `card_port_attributes` cp_a ON cp_a.`id` = cpa.`card_port_attribute_id` 
            INNER JOIN `cards` c ON c.`id` = cp.`card_id` 
            INNER JOIN `card_types` ct ON ct.`id` = c.`card_type_id` 
            WHERE ct.`name` = 'dependent-MDU' 
            AND cp_a.`identifier` = 'olt-connected' 
            AND cp.`card_port_id` IS NOT NULL 
            AND c.`location_id` = ?");

        $stmt->bindValue(1, $location->getId());
        $stmt->execute();

        if ($stmt->rowCount() < 1) {
            // todo: use access denied exception
            throw new \Exception('Dieser Standort erfüllt nicht alle Bedingungen um als unselbstständige MDU von einem OLT gelöscht zu werden.');
        }

        $mduBaseCardPortId = $stmt->fetch();
        $mduBaseCardPortId = $mduBaseCardPortId['id'];
        $mduBaseCardPort = $this->getDoctrine()->getRepository('\AppBundle\Entity\Location\CardPort')->findOneById($mduBaseCardPortId);

        $parentLocation = $mduBaseCardPort->getRelatedCardPort()->getCard()->getLocation();

        $dslamCommunicator = $this->get('dslam_chooser')->findCommunicationClass(
            $parentLocation->getType(), $parentLocation->getProcessor(), $parentLocation->getCurrentSoftwareRelease()
        );

        if (null === $dslamCommunicator) {
            // todo: use access denied exception
            throw new \Exception("error - no dslam communicator found");
        }

        $dslamCommunicator
            ->setContainer(new ContainerBuilder()) // set service container to dslam communicator for dependency injection
            ->setGateway(new SshGateway($parentLocation->getIpAddress(), $parentLocation->getLogonPort(), 10)); // set connection gateway to dslam communicator

        // perform the login
        $dslamCommunicator->login(['username' => $parentLocation->getLogonUsername(), 'password' => $parentLocation->getLogonPassword()]);

        /* uncomment to deprovision each single card for mdu

        $cards = $this->getDoctrine()->getRepository('\AppBundle\Entity\Location\Card')->findByLocation($location);

        $response = '';

        foreach ($cards as $card) {
            if ('dependent-MDU' === $card->getCardType()->getName()) { // skip this card-types
                $response .= sprintf('<pre>%s</pre>', $dslamCommunicator->deprovisionDependentBaseMdu(
                    $card
                ));
            } else {
                $response .= sprintf('<pre>%s</pre>', $dslamCommunicator->deprovisionDependentMdu(
                    $card
                ));
            }
        }*/

        // deprovision whole (base) mdu from olt - this removes all cards too
        $response = sprintf('<pre>%s</pre>', $dslamCommunicator->deprovisionDependentBaseMdu(
            $mduBaseCardPort->getCard()
        ));

        return new Response($response);
    }

    /**
     * @Route("/location/{id}/provision-dependent-mdu", name="location-provision-dependent-mdu");
     */
    public function provisionDependentMduAction(Request $request, Location $location)
    {
        // has this location at least one card with type 'dependent-MDU' and at least one card port with attribute 'olt-connected'
        // and is that port connected to other exsiting port
        $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare("SELECT cp.`id` FROM `card_ports` cp 
            INNER JOIN `card_ports_to_card_port_attributes` cpa ON cpa.`card_port_id` = cp.`id` 
            INNER JOIN `card_port_attributes` cp_a ON cp_a.`id` = cpa.`card_port_attribute_id` 
            INNER JOIN `cards` c ON c.`id` = cp.`card_id` 
            INNER JOIN `card_types` ct ON ct.`id` = c.`card_type_id` 
            WHERE ct.`name` = 'dependent-MDU' 
            AND cp_a.`identifier` = 'olt-connected' 
            AND cp.`card_port_id` IS NOT NULL 
            AND c.`location_id` = ?");

        $stmt->bindValue(1, $location->getId());
        $stmt->execute();

        if ($stmt->rowCount() < 1) {
            // todo: use access denied exception
            throw new \Exception('Dieser Standort erfüllt nicht alle Bedingungen um als unselbstständige MDU auf einem OLT eingerichtet zu werden.');
        }

        $mduBaseCardPortId = $stmt->fetch();
        $mduBaseCardPortId = $mduBaseCardPortId['id'];
        $mduBaseCardPort = $this->getDoctrine()->getRepository('\AppBundle\Entity\Location\CardPort')->findOneById($mduBaseCardPortId);

        $parentLocation = $mduBaseCardPort->getRelatedCardPort()->getCard()->getLocation();

        $dslamCommunicator = $this->get('dslam_chooser')->findCommunicationClass(
            $parentLocation->getType(), $parentLocation->getProcessor(), $parentLocation->getCurrentSoftwareRelease()
        );

        if (null === $dslamCommunicator) {
            // todo: use access denied exception
            throw new \Exception("error - no dslam communicator found");
        }

        $dslamCommunicator
            ->setContainer(new ContainerBuilder()) // set service container to dslam communicator for dependency injection
            ->setGateway(new SshGateway($parentLocation->getIpAddress(), $parentLocation->getLogonPort(), 10)); // set connection gateway to dslam communicator

        // perform the login
        $dslamCommunicator->login(['username' => $parentLocation->getLogonUsername(), 'password' => $parentLocation->getLogonPassword()]);

        $cards = $this->getDoctrine()->getRepository('\AppBundle\Entity\Location\Card')->findByLocation($location);

        $response = '';

        foreach ($cards as $card) {
            if ('dependent-MDU' === $card->getCardType()->getName()) { // skip this card-types
                $response .= sprintf('<pre>%s</pre>', $dslamCommunicator->provisionDependentBaseMdu(
                    $card
                ));
            } else {
                $response .= sprintf('<pre>%s</pre>', $dslamCommunicator->provisionDependentMdu(
                    $card
                ));
            }
        }

        return new Response($response);
    }

    /**
     * @Route("/network/{id}/location/new", name="location-create-new")
     */
    public function createNewAction(Request $request, Network $network)
    {
        $this->denyAccessUnlessGranted('ROLE_EDIT_LOCATION');

        $doctrine = $this->getDoctrine();

        $locationForm = $this->createForm(LocationType::class, new Location(), []);

        $locationForm->handleRequest($request);

        if ($locationForm->isSubmitted()) {
            if ($locationForm->isValid()) {
                $location = $locationForm->getData();
                $location->setNetwork($network);

                $em = $doctrine->getManager();
                $em->persist($locationForm->getData());
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'Location gespeichert.');

                $url = $this->generateUrl('network-view', [
                    'id' => $network->getId(),
                ]);
                
                $url = urldecode($url); // need to decode because of wkv's ?& in urls - symfony encodes them by default
                
                return $this->redirect($url);
            } else {
                $errorString = '';

                foreach ($locationForm->getErrors(true) as $error) {
                    $errorString .= '('.$error->getOrigin()->getName().') '.$error->getMessage();
                }

                $request->getSession()->getFlashBag()->add('error', sprintf(
                    'Fehler<br />%s', $errorString
                ));
            }
        }

        return $this->render('location/create.html.php', [
            'locationForm' => $locationForm->createView(),
        ]);
    }

    /**
     * @Route("/network/{networkId}/location/edit/{id}", name="location-edit")
     */
    public function editAction(Request $request, $networkId, Location $location)
    {
        $this->denyAccessUnlessGranted('ROLE_EDIT_LOCATION');

        $doctrine = $this->getDoctrine();

        $locationForm = $this->createForm(LocationType::class, $location, []);

        $locationForm->handleRequest($request);

        if ($locationForm->isSubmitted()) {
            if ($locationForm->isValid()) {
                $em = $doctrine->getManager();
                $em->persist($locationForm->getData());
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'Location gespeichert.');

                $url = $this->generateUrl('network-view', [
                    'id' => $location->getNetwork()->getId(),
                ]);

                $url = urldecode($url); // need to decode because of wkv's ?& in urls - symfony encodes them by default
                
                return $this->redirect($url);
            } else {
                $errorString = '';

                foreach ($locationForm->getErrors(true) as $error) {
                    $errorString .= '('.$error->getOrigin()->getName().') '.$error->getMessage();
                }

                $request->getSession()->getFlashBag()->add('error', sprintf(
                    'Fehler<br />%s', $errorString
                ));
            }
        }

        return $this->render('location/edit.html.php', [
            'locationForm' => $locationForm->createView(),
            'location' => $location,
        ]);
    }

    /**
     * @Route("/network/{networkId}/location/clone/{id}", name="location-clone")
     */
    public function cloneAction(Request $request, $networkId, Location $location)
    {
        $this->denyAccessUnlessGranted('ROLE_EDIT_LOCATION');

        $location = clone $location;

        $doctrine = $this->getDoctrine();

        $locationForm = $this->createForm(LocationType::class, $location, []);

        $locationForm->handleRequest($request);

        if ($locationForm->isSubmitted()) {
            if ($locationForm->isValid()) {
                $em = $doctrine->getManager();
                $em->persist($locationForm->getData());
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'Location gespeichert.');

                $url = $this->generateUrl('network-view', [
                    'id' => $location->getNetwork()->getId(),
                ]);

                $url = urldecode($url); // need to decode because of wkv's ?& in urls - symfony encodes them by default
                
                return $this->redirect($url);
            } else {
                $errorString = '';

                foreach ($locationForm->getErrors(true) as $error) {
                    $errorString .= '('.$error->getOrigin()->getName().') '.$error->getMessage();
                }

                $request->getSession()->getFlashBag()->add('error', sprintf(
                    'Fehler<br />%s', $errorString
                ));
            }
        }

        return $this->render('location/create.html.php', [
            'locationForm' => $locationForm->createView(),
        ]);
    }

    /**
     * @Route("/network/{networkId}/location/delete/{id}", name="location-delete")
     */
    public function deleteAction(Request $request, $networkId, Location $location)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($location);
        $em->flush();

        $request->getSession()->getFlashBag()->add('success', 'Location gelöscht');

        $url = $this->generateUrl('network-view', [
            'id' => $location->getNetwork()->getId(),
        ]);

        $url = urldecode($url); // need to decode because of wkv's ?& in urls - symfony encodes them by default
        
        return $this->redirect($url);
    }
}