<?php

/**
 * 
 */

namespace AppBundle\Controller\Location;

use AppBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Location\Location;
use AppBundle\Entity\Location\Card;
use AppBundle\Entity\Location\CardPort;
use AppBundle\Form\Type\Location\CardType;
use AppBundle\Form\Type\Location\CardPortOnlyAttributesType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Entity\Location\CardPortAttribute;
use AppBundle\src\DslamCommunication\Communicator\SshGateway;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\Response;

/**
 * 
 */
class CardController extends BaseController
{
    /**
     * @Route("/card/{id}/show-info-about-card-for-dependent-mdu", name="show-info-about-card-for-dependent-mdu");
     */
    public function showInfoAboutCardForDependentMduAction(Request $request, Card $card)
    {
        $location = $card->getLocation();

        // has this location at least one card with type 'dependent-MDU' and at least one card port with attribute 'olt-connected'
        // and is that port connected to other exsiting port
        $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare("SELECT cp.`id` FROM `card_ports` cp 
            INNER JOIN `card_ports_to_card_port_attributes` cpa ON cpa.`card_port_id` = cp.`id` 
            INNER JOIN `card_port_attributes` cp_a ON cp_a.`id` = cpa.`card_port_attribute_id` 
            INNER JOIN `cards` c ON c.`id` = cp.`card_id` 
            INNER JOIN `card_types` ct ON ct.`id` = c.`card_type_id` 
            WHERE ct.`name` = 'dependent-MDU' 
            AND cp_a.`identifier` = 'olt-connected' 
            AND cp.`card_port_id` IS NOT NULL 
            AND c.`location_id` = ?");

        $stmt->bindValue(1, $location->getId());
        $stmt->execute();

        if ($stmt->rowCount() < 1) {
            // todo: use access denied exception
            throw new \Exception('Dieser Standort erfüllt nicht alle Bedingungen um als unselbstständige MDU auf einem OLT eingerichtet zu werden.');
        }

        $mduBaseCardPortId = $stmt->fetch();
        $mduBaseCardPortId = $mduBaseCardPortId['id'];
        $mduBaseCardPort = $this->getDoctrine()->getRepository('\AppBundle\Entity\Location\CardPort')->findOneById($mduBaseCardPortId);

        $parentLocation = $mduBaseCardPort->getRelatedCardPort()->getCard()->getLocation();

        $dslamCommunicator = $this->get('dslam_chooser')->findCommunicationClass(
            $parentLocation->getType(), $parentLocation->getProcessor(), $parentLocation->getCurrentSoftwareRelease()
        );

        if (null === $dslamCommunicator) {
            // todo: use access denied exception
            throw new \Exception("error - no dslam communicator found");
        }

        $dslamCommunicator
            ->setContainer(new ContainerBuilder()) // set service container to dslam communicator for dependency injection
            ->setGateway(new SshGateway($parentLocation->getIpAddress(), $parentLocation->getLogonPort(), 10)); // set connection gateway to dslam communicator

        // perform the login
        $dslamCommunicator->login(['username' => $parentLocation->getLogonUsername(), 'password' => $parentLocation->getLogonPassword()]);
        
        if ('dependent-MDU' === $card->getCardType()->getName()) {
            $response .= sprintf('<pre>%s</pre>', $dslamCommunicator->showInfoAboutDependentBaseMdu(
                $card
            ));
        } else {
            $response .= sprintf('<pre>%s</pre>', $dslamCommunicator->showInfoAboutDependentMdu(
                $card
            ));
        }

        return new Response($response);
    }

    /**
     * @Route("/card/{id}/deprovision-card-for-dependent-mdu", name="deprovision-card-for-dependent-mdu");
     */
    public function deprovisionCardForDependentMduAction(Request $request, Card $card)
    {
        $location = $card->getLocation();

        // has this location at least one card with type 'dependent-MDU' and at least one card port with attribute 'olt-connected'
        // and is that port connected to other exsiting port
        $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare("SELECT cp.`id` FROM `card_ports` cp 
            INNER JOIN `card_ports_to_card_port_attributes` cpa ON cpa.`card_port_id` = cp.`id` 
            INNER JOIN `card_port_attributes` cp_a ON cp_a.`id` = cpa.`card_port_attribute_id` 
            INNER JOIN `cards` c ON c.`id` = cp.`card_id` 
            INNER JOIN `card_types` ct ON ct.`id` = c.`card_type_id` 
            WHERE ct.`name` = 'dependent-MDU' 
            AND cp_a.`identifier` = 'olt-connected' 
            AND cp.`card_port_id` IS NOT NULL 
            AND c.`location_id` = ?");

        $stmt->bindValue(1, $location->getId());
        $stmt->execute();

        if ($stmt->rowCount() < 1) {
            // todo: use access denied exception
            throw new \Exception('Dieser Standort erfüllt nicht alle Bedingungen um als unselbstständige MDU auf einem OLT eingerichtet zu werden.');
        }

        $mduBaseCardPortId = $stmt->fetch();
        $mduBaseCardPortId = $mduBaseCardPortId['id'];
        $mduBaseCardPort = $this->getDoctrine()->getRepository('\AppBundle\Entity\Location\CardPort')->findOneById($mduBaseCardPortId);

        $parentLocation = $mduBaseCardPort->getRelatedCardPort()->getCard()->getLocation();

        $dslamCommunicator = $this->get('dslam_chooser')->findCommunicationClass(
            $parentLocation->getType(), $parentLocation->getProcessor(), $parentLocation->getCurrentSoftwareRelease()
        );

        if (null === $dslamCommunicator) {
            // todo: use access denied exception
            throw new \Exception("error - no dslam communicator found");
        }

        $dslamCommunicator
            ->setContainer(new ContainerBuilder()) // set service container to dslam communicator for dependency injection
            ->setGateway(new SshGateway($parentLocation->getIpAddress(), $parentLocation->getLogonPort(), 10)); // set connection gateway to dslam communicator

        // perform the login
        $dslamCommunicator->login(['username' => $parentLocation->getLogonUsername(), 'password' => $parentLocation->getLogonPassword()]);
        
        if ('dependent-MDU' === $card->getCardType()->getName()) {
            $response .= sprintf('<pre>%s</pre>', $dslamCommunicator->deprovisionDependentBaseMdu(
                $card
            ));
        } else {
            $response .= sprintf('<pre>%s</pre>', $dslamCommunicator->deprovisionDependentMdu(
                $card
            ));
        }

        return new Response($response);
    }

    /**
     * @Route("/card/{id}/provision-card-for-dependent-mdu", name="provision-card-for-dependent-mdu");
     */
    public function provisionCardForDependentMduAction(Request $request, Card $card)
    {
        $location = $card->getLocation();

        // has this location at least one card with type 'dependent-MDU' and at least one card port with attribute 'olt-connected'
        // and is that port connected to other exsiting port
        $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare("SELECT cp.`id` FROM `card_ports` cp 
            INNER JOIN `card_ports_to_card_port_attributes` cpa ON cpa.`card_port_id` = cp.`id` 
            INNER JOIN `card_port_attributes` cp_a ON cp_a.`id` = cpa.`card_port_attribute_id` 
            INNER JOIN `cards` c ON c.`id` = cp.`card_id` 
            INNER JOIN `card_types` ct ON ct.`id` = c.`card_type_id` 
            WHERE ct.`name` = 'dependent-MDU' 
            AND cp_a.`identifier` = 'olt-connected' 
            AND cp.`card_port_id` IS NOT NULL 
            AND c.`location_id` = ?");

        $stmt->bindValue(1, $location->getId());
        $stmt->execute();

        if ($stmt->rowCount() < 1) {
            // todo: use access denied exception
            throw new \Exception('Dieser Standort erfüllt nicht alle Bedingungen um als unselbstständige MDU auf einem OLT eingerichtet zu werden.');
        }

        $mduBaseCardPortId = $stmt->fetch();
        $mduBaseCardPortId = $mduBaseCardPortId['id'];
        $mduBaseCardPort = $this->getDoctrine()->getRepository('\AppBundle\Entity\Location\CardPort')->findOneById($mduBaseCardPortId);

        $parentLocation = $mduBaseCardPort->getRelatedCardPort()->getCard()->getLocation();

        $dslamCommunicator = $this->get('dslam_chooser')->findCommunicationClass(
            $parentLocation->getType(), $parentLocation->getProcessor(), $parentLocation->getCurrentSoftwareRelease()
        );

        if (null === $dslamCommunicator) {
            // todo: use access denied exception
            throw new \Exception("error - no dslam communicator found");
        }

        $dslamCommunicator
            ->setContainer(new ContainerBuilder()) // set service container to dslam communicator for dependency injection
            ->setGateway(new SshGateway($parentLocation->getIpAddress(), $parentLocation->getLogonPort(), 10)); // set connection gateway to dslam communicator

        // perform the login
        $dslamCommunicator->login(['username' => $parentLocation->getLogonUsername(), 'password' => $parentLocation->getLogonPassword()]);

        if ('dependent-MDU' === $card->getCardType()->getName()) {
            $response .= sprintf('<pre>%s</pre>', $dslamCommunicator->provisionDependentBaseMdu(
                $card
            ));
        } else {
            $response .= sprintf('<pre>%s</pre>', $dslamCommunicator->provisionDependentMdu(
                $card
            ));
        }

        return new Response($response);
    }

    /**
     * @Route("/location/{id}/card/new", name="location-card-create-new")
     * @Route("/location/{id}/card/clone/{cloneCardId}", name="location-card-clone")
     */
    public function createNewAction(Request $request, Location $location, $cloneCardId = null)
    {
        $this->denyAccessUnlessGranted('ROLE_EDIT_LOCATION');

        $doctrine = $this->getDoctrine();

        $card = null;

        if (null !== $cloneCardId) {
            $cloneCard = $this->getDoctrine()->getRepository(Card::class)->findOneById($cloneCardId);

            if (null !== $cloneCard) {
                $card = clone $cloneCard;
            }
        }

        if (null === $card) {
            $card = new Card();
            $card->setLocation($location);
        }

        $cardForm = $this->createForm(CardType::class, $card, [
            'cardTypes' => $doctrine->getRepository('\AppBundle\Entity\Location\CardType')->findAll(),
        ]);

        $cardForm->handleRequest($request);

        if ($cardForm->isSubmitted()) {
            if ($cardForm->isValid()) {
                $em = $doctrine->getManager();

                $amountOfPorts = (int) $card->getFirstPortNumber() + (int) $card->getPortAmount();

                // create card ports
                for ($i = (int) $card->getFirstPortNumber(); $i < $amountOfPorts; $i++) {
                    $cardPort = new CardPort();
                    $cardPort->setCard($card)->setNumber($i);

                    $em->persist($cardPort);
                }

                $em->persist($card);
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'Karte gespeichert.');
                
                return $this->redirectToRoute('location-card-edit', [
                    'locationId' => $card->getLocation()->getId(),
                    'id' => $card->getId(),
                ]);
            } else {
                $errorString = '';

                foreach ($cardForm->getErrors(true) as $error) {
                    $errorString .= '('.$error->getOrigin()->getName().') '.$error->getMessage();
                }

                $request->getSession()->getFlashBag()->add('error', sprintf(
                    'Fehler<br />%s', $errorString
                ));
            }
        }

        return $this->render('location/card/create.html.php', [
            'cardForm' => $cardForm->createView(),
        ]);
    }

    /**
     * @Route("/location/{locationId}/card/edit/{id}", name="location-card-edit")
     */
    public function editAction(Request $request, $locationId, Card $card)
    {
        $this->denyAccessUnlessGranted('ROLE_EDIT_LOCATION');

        $doctrine = $this->getDoctrine();
        $cardPorts = $doctrine->getRepository('\AppBundle\Entity\Location\CardPort')->findByCard($card, ['number' => 'ASC']);
        
        $card->setPorts(array_combine(array_map(function (CardPort $cardPort) {
            return $cardPort->getId();
        }, $cardPorts), $cardPorts));
        
        $possibleAttributes = $doctrine->getRepository(CardPortAttribute::class)->findAll();

        $possibleAttributesIds = array_map(function (CardPortAttribute $attribute) {
            return $attribute->getId();
        }, $possibleAttributes);

        $possibleAttributes = array_combine($possibleAttributesIds, $possibleAttributes);

        $cardPortsCount = count($cardPorts);

        $oldRange = [$card->getFirstPortNumber(), end($cardPorts)->getNumber()];

        $cardForm = $this->createForm(CardType::class, $card, [
            'cardTypes' => $doctrine->getRepository('\AppBundle\Entity\Location\CardType')->findAll(),
        ]);

        $cardForm->handleRequest($request);

        if ($cardForm->isSubmitted()) {
            if ($cardForm->isValid()) {
                $em = $doctrine->getManager();

                $newRange = [(int) $card->getFirstPortNumber(), (int) $card->getFirstPortNumber() + (int) $card->getPortAmount()];

                $postedCardPortAttributes = [];

                if ($request->request->has('card_port_attribute')) {
                    $postedCardPortAttributes = $request->request->get('card_port_attribute');
                }

                $map = [];

                foreach ($cardPorts as $key => $cardPort) {
                    $map[$cardPort->getNumber()] = $cardPort;
                }

                for ($i = $newRange[0]; $i < $newRange[1]; $i++) {
                    if (isset($map[$i])) {
                        $cardPort = $map[$i];

                        unset($map[$i]); // remove from map

                        if (isset($postedCardPortAttributes[$cardPort->getId()])) {
                            $postedAttributes = $postedCardPortAttributes[$cardPort->getId()];

                            foreach ($cardPort->getAttributes() as $attribute) {
                                if (!isset($postedAttributes[$attribute->getId()])) {
                                    $cardPort->removeAttribute($attribute);
                                } else {
                                    unset($postedAttributes[$attribute->getId()]);
                                }
                            }

                            foreach ($postedAttributes as $attributeId => $true) {
                                $cardPort->addAttribute($possibleAttributes[$attributeId]);
                            }
                        } else {
                            foreach ($cardPort->getAttributes() as $attribute) {
                                $cardPort->removeAttribute($attribute);
                            }
                        }

                        $em->persist($cardPort);

                        continue;
                    } else {
                        $cardPort = new CardPort();
                        $cardPort->setCard($card)->setNumber($i);

                        $em->persist($cardPort);
                    }
                }

                foreach ($map as $cardPort) {
                    $em->remove($cardPort);
                }

                unset($map);

                $em->persist($card);
                $em->flush();

                $request->getSession()->getFlashBag()->add('success', 'Karte gespeichert.');

                $url = $this->generateUrl('location-view', [
                    'id' => $card->getLocation()->getId(),
                ]);

                $url = urldecode($url); // need to decode because of wkv's ?& in urls - symfony encodes them by default
                
                return $this->redirect($url);
            } else {
                $errorString = '';

                foreach ($cardForm->getErrors(true) as $error) {
                    $errorString .= '('.$error->getOrigin()->getName().') '.$error->getMessage();
                }

                $request->getSession()->getFlashBag()->add('error', sprintf(
                    'Fehler<br />%s', $errorString
                ));
            }
        }

        $allCards = $doctrine->getRepository('\AppBundle\Entity\Location\Card')->findAll();
        $networksLocationsCards = [];

        foreach ($allCards as $cardObject) {
            $locationName = $cardObject->getLocation()->getName();
            $networkName = $cardObject->getLocation()->getNetwork()->getName();

            if (!isset($networksLocationsCards[$networkName])) {
                $networksLocationsCards[$networkName] = [];
            }

            if (!isset($networksLocationsCards[$networkName][$locationName])) {
                $networksLocationsCards[$networkName][$locationName] = [];
            }

            $networksLocationsCards[$networkName][$locationName][] = $cardObject;
        }

        unset($allCards);

        $mduConnectedCardPortId = null;
        // has this location at least one card with type 'dependent-MDU' and at least one card port with attribute 'olt-connected'
        // and is that port connected to other exsiting port
        $query = $doctrine->getConnection()->prepare("SELECT cp.`id` FROM `card_ports` cp 
            INNER JOIN `card_ports_to_card_port_attributes` cpa ON cpa.`card_port_id` = cp.`id` 
            INNER JOIN `card_port_attributes` cp_a ON cp_a.`id` = cpa.`card_port_attribute_id` 
            INNER JOIN `cards` c ON c.`id` = cp.`card_id` 
            INNER JOIN `card_types` ct ON ct.`id` = c.`card_type_id` 
            WHERE ct.`name` = 'dependent-MDU' 
            AND cp_a.`identifier` = 'olt-connected' 
            AND cp.`card_port_id` IS NOT NULL 
            AND c.`location_id` = ?");

        $query->bindValue(1, $card->getLocation()->getId());
        $query->execute();

        if ($query->rowCount() > 0) {
            $mduConnectedCardPortId = $query->fetch();
            $mduConnectedCardPortId = $mduConnectedCardPortId['id'];
        }

        return $this->render('location/card/edit.html.php', [
            'cardForm' => $cardForm->createView(),
            'card' => $card,
            'cardPorts' => $cardPorts,
            'networksLocationsCards' => $networksLocationsCards,
            'possibleCardPortAttributes' => $possibleAttributes,
            'mduConnectedCardPortId' => $mduConnectedCardPortId,
        ]);
    }

    /**
     * @Route("/location/{locationId}/card/delete/{id}", name="location-card-delete")
     */
    public function deleteAction(Request $request, $locationId, Card $card)
    {
        $em = $this->getDoctrine()->getManager();

        $cardPorts = $this->getDoctrine()->getRepository('AppBundle\Entity\Location\CardPort')->findByCard($card);

        foreach ($cardPorts as $cardPort) {
            $em->remove($cardPort);
        }

        $em->remove($card);
        $em->flush();

        $request->getSession()->getFlashBag()->add('success', 'Karte gelöscht');

        $url = $this->generateUrl('location-view', [
            'id' => $card->getLocation()->getId(),
        ]);

        $url = urldecode($url); // need to decode because of wkv's ?& in urls - symfony encodes them by default
        
        return $this->redirect($url);
    }

    /**
     * @Route("/card/get-unassigned-ports/{id}", name="card-get-unassigned-ports")
     */
    public function getUnassignedCardPortsByCardAction(Request $request, Card $card)
    {
        $selectedCardPortId = $request->get('selected-card-port-id');

        $db = $this->getDoctrine()->getManager()->getConnection();

        if (null !== $selectedCardPortId) {
            // get ports for a certain card, except the ones that are connected to another card-port or a customer
            // if selectedCardPortId was given, get this values too
            $stmt = $db->prepare("SELECT cp.`id`, cp.`number` 
                FROM `card_ports` cp 
                LEFT JOIN `customers` cu ON cu.`card_id` = cp.`card_id`
                WHERE cp.`card_id` = :cardId AND (cp.`id` = :selectedCardPortId OR cp.`card_port_id` IS NULL) AND (cu.`dslam_port` IS NULL OR cu.`dslam_port` != cp.`number`)");

            $stmt->bindValue(':selectedCardPortId', (int) $selectedCardPortId);
        } else {
            // get ports for a certain card, except the ones that are connected to another card-port or a customer
            $stmt = $db->prepare("SELECT cp.`id`, cp.`number` 
                FROM `card_ports` cp 
                LEFT JOIN `customers` cu ON cu.`card_id` = cp.`card_id`
                WHERE cp.`card_id` = :cardId AND cp.`card_port_id` IS NULL AND (cu.`dslam_port` IS NULL OR cu.`dslam_port` != cp.`number`)");
        }

        $stmt->bindValue(':cardId', $card->getId());
        $stmt->execute();

        $cardPorts = $stmt->fetchAll();

        return new JsonResponse($cardPorts);
    }

    /**
     * @Route("/card/update-port/{id}", name="card-update-port")
     */
    public function updateCardPortAction(Request $request, CardPort $cardPort)
    {
        $em = $this->getDoctrine()->getManager();

        $relatedCardPortId = $request->get('related-card-port-id');

        if ($relatedCardPortId === "") {
            // free the port
            $relatedCardPort = $cardPort->getRelatedCardPort();
            $relatedCardPort->setRelatedCardPort(null);
            $cardPort->setRelatedCardPort(null);
        } else {
            // assign the port
            $relatedCardPort = $em->getRepository(CardPort::class)->findOneById($request->get('related-card-port-id'));

            if (!($relatedCardPort instanceOf CardPort)) {
                throw new NotFoundHttpException('Invalid card port id');
            }

            $previousRelatedCardPort = $cardPort->getRelatedCardPort();

            if (null !== $previousRelatedCardPort) { // was assigned previously, has to be freed too
                $previousRelatedCardPort->setRelatedCardPort(null);
                $em->persist($previousRelatedCardPort);
            }

            $cardPort->setRelatedCardPort($relatedCardPort);
            $relatedCardPort->setRelatedCardPort($cardPort);
        }

        $em->persist($cardPort);
        $em->persist($relatedCardPort);
        $em->flush();

        return new JsonResponse(['status' => 'success']);
    }
}
