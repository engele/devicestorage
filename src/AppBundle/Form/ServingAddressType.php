<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ServingAddressType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('zipCode',TextType::class, [
                'label' => 'PLZ:',
                'attr' => ["maxlength"=>10],
                'required' => true,
            ])
            ->add('city',TextType::class, [
                'label' => 'Ort:',
                'attr' => ["maxlength"=>100],
                'required' => true,
            ])
            ->add('district',TextType::class, [
                'label' => 'Ortsteil:',
                'attr' => ["maxlength"=>100],
                'required' => true,
            ])
            ->add('street',TextType::class, [
                'label' => 'Strasse:',
                'attr' => ["maxlength"=>100],
                'required' => true,
            ])
            ->add('houseNumber',TextType::class, [
                'label' => 'Hausnr.:',
                'attr' => ["maxlength"=>10],
                'required' => true,
            ])
            ->add('amountOfBusinessUnits',IntegerType::class, [
                'label' => 'Anzah der Hauseinheiten:',
                'required' => true,
            ])
            ->add('amountOfDwellingUnits',IntegerType::class, [
                'label' => 'Anzahl der Wohnungseinheiten:',
                'required' => true,
            ])
            ->add('rollOutIsPlannedIn',DateType::class, [
                'label' => 'Ausbaujahr:',
                'required' => true,
            ])
            ->add('isRollOutFinished',CheckboxType::class, [
                'label' => 'Rollout beendet:',
                'required' => false,
            ])
            ->add('isBuildingConnected',ChoiceType::class, [
                'label' => 'Hausanschluss gelegt:',
                'choices' => ["nein"=>false, "ja"=>true],
                'required' => true,
            ])
            ->add('isNe4Finished',ChoiceType::class, [
                'label' => 'Ne4 gelegt:',
                'choices' => ["nein"=>false, "ja"=>true],
                'required' => true,
            ])
            ->add('hasGnv',ChoiceType::class, [
                'label' => 'Gnv vorhanden:',
                'choices' => ["nein"=>false, "ja"=>true],
                'required' => true,
            ])
            ->add('gnvNote',TextType::class, [
                'label' => 'Gnv Anmerkung',
                'attr' => ["maxlength"=>5],
                'required' => false,
            ])
            ->add('hasEverHadAnInternetSupplyContract',ChoiceType::class, [
                'label' => 'hatte zuvor Internetvertrag:',
                'choices' => ["nein"=>false, "ja"=>true],
                'required' => true,
            ])
            ->add('hasEverHadABasicTvSupplyContract',ChoiceType::class, [
                'label' => 'hatte zuvor Basis TV-Vertrag:',
                'choices' => ["nein"=>false, "ja"=>true],
                'required' => false,
            ])
            ->add('connectedToKvz',TextType::class, [
                'label' => 'KVZ Nr.',
                'attr' => ["maxlength"=>9],
                'required' => false,
            ])
            ->add('technicalNote',TextType::class, [
                'label' => 'Technischer Hinweis',
                'required' => false,
            ])
            ->add('fieldAndCadastre',TextType::class, [
                'label' => 'Flur / Kataster',
                'required' => false,
            ])
            ->add('note',TextareaType::class, [
                'label' => 'Bemerkung',
                'attr' => ["maxlength"=>1000],
                'required' => false,
            ])
            ->add('damping',IntegerType::class, [
                'label' => 'Dämpfung:',
                'required' => false,
            ])
            ->add('diameter_1',IntegerType::class, [
                'label' => 'Durchmesser 1:',
                'required' => false,
            ])
            ->add('length_1',IntegerType::class, [
                'label' => 'Länge 1:',
                'required' => false,
            ])
            ->add('diameter_2',IntegerType::class, [
                'label' => 'Durchmesser 2:',
                'required' => false,
            ])
            ->add('length_2',IntegerType::class, [
                'label' => 'Länge 2:',
                'required' => false,
            ])
            ->add('diameter_3',IntegerType::class, [
                'label' => 'Durchmesser 3:',
                'required' => false,
            ])
            ->add('length_3',IntegerType::class, [
                'label' => 'Länge 3:',
                'required' => false,
            ])
            ->add('diameter_4',IntegerType::class, [
                'label' => 'Durchmesser 4:',
                'required' => false,
            ])
            ->add('length_4',IntegerType::class, [
                'label' => 'Länge 4:',
                'required' => false,
            ])
            ->add('diameter_5',IntegerType::class, [
                'label' => 'Durchmesser 5:',
                'required' => false,
            ])
            ->add('length_5',IntegerType::class, [
                'label' => 'Länge 5:',
                'required' => false,
            ]);
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ServingAddress'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_servingaddress';
    }


}
