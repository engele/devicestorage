<?php


namespace AppBundle\Form\Type\OtrsAdapter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 *
 */
class OtrsTicketCreateType extends AbstractType
{
    private $otrsAdapterConfig;

    /**
     * OtrsTicketCreateType constructor.
     * @param $otrsAdapterConfig
     */
    public function __construct($otrsAdapterConfig)
    {
        $this->otrsAdapterConfig = $otrsAdapterConfig;
    }

    /**
     * Build form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('internal_customerId', HiddenType::class, [
                'label' => 'internal_customerId',
                'attr' => [
                    'placeholder' => '',
                ],
                'required' => true,
            ])
            ->add('subject', TextType::class, [
                'label' => 'Betreff:',
                'attr' => [
                    'placeholder' => '',
                ],
                'required' => true,
            ])
            ->add('body', TextareaType::class, [
                'label' => 'Text:',
                'attr' => [
                    'placeholder' => '',
                ],
                'required' => true,
            ])
            ->add('queueId', ChoiceType::class, [
                'label' => 'Ticket-Queue:',
                'choices' => $this->otrsAdapterConfig['availableQueues'],
                'attr' => [
                    'placeholder' => 'Ticket-Queue',
                ],
                'required' => true,
            ])
            ->add('ownerId', ChoiceType::class, [
                'label' => 'Ticket-Inhaber:',
                'choices' => $this->otrsAdapterConfig['availableTicketOwner'],
                'attr' => [
                    'placeholder' => 'Ticket-Inhaber',
                ],
                'required' => true,
            ])
            ->add('responsibleId', ChoiceType::class, [
                'label' => 'Ticket-Verantwortlicher:',
                'choices' => $this->otrsAdapterConfig['availableTicketResponsibleUser'],
                'attr' => [
                    'placeholder' => 'Ticket-Verantwortlicher',
                ],
                'required' => true,
            ])
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
//            'data_class' => 'AppBundle\src\OtrsAdapter\Entity\RequestParamsTicketCreate', //
            'csrf_protection' => false,
            'csrf_field_name' => '_token',
        ]);
    }
}