<?php

/**
 *
 */

namespace AppBundle\Form\Type\Location;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\VlanProfile;

/**
 * 
 */
class CardType extends AbstractType
{
    /**
     * Build form
     * 
     * @param FormBuilderInterface $builder
     * @param array                $options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Name',
                'attr' => [
                    'placeholder' => 'Name',
                ],
                'required' => true,
            ])
            ->add('firstPortNumber', NumberType::class, [
                'label' => 'Nummer des ersten Ports',
                'attr' => [
                    'placeholder' => 'z.B. 1',
                ],
                'required' => true,
            ])
            ->add('portAmount', NumberType::class, [
                'label' => 'Port Anzahl',
                'attr' => [
                    'placeholder' => 'z.B. 1',
                ],
                'required' => true,
            ])
            ->add('lineIdentifierPrefix', TextType::class, [
                'label' => 'Line Identifier Prefix',
                'attr' => [
                    'placeholder' => 'z.B. 1/2/3/',
                ],
                'required' => true,
            ])
            ->add('ipAddress', TextType::class, [
                'label' => 'IP-Adresse',
                'attr' => [
                    'placeholder' => 'IPv4 Adresse',
                ],
                'required' => false,
            ])
            ->add('cardType', ChoiceType::class, [
                'label' => 'Karten Typ',
                'choices' => $options['cardTypes'],
                'required' => true,
                'choice_label' => function($cardType, $key, $value) {
                    return $cardType->getName();
                },
                'choice_value' => function ($value) {
                    if ($value instanceof \AppBundle\Entity\Location\CardType) {
                        return $value->getId();
                    }

                    return $value;
                },
            ])
            ->add('portDefaultVlanProfiles', EntityType::class, [
                'label' => 'Default VLAN-Profile für Ports',
                'class' => VlanProfile::class,
                'choice_label' => 'name',
                'required' => false,
                'multiple' => true,
            ])
            ->add('customerDefaultVlanProfiles', EntityType::class, [
                'label' => 'Default VLAN-Profile für Kunden',
                'class' => VlanProfile::class,
                'choice_label' => 'name',
                'required' => false,
                'multiple' => true,
            ])
            ->add('ports', CollectionType::class, [
                'entry_type' => CardPortType::class,
            ])
            ->add('rfOverlayEnabled', CheckboxType::class, [
                'label' => 'Karte unterstützt RF-Overlay',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Speichern',
            ])
        ;
    }

    /**
     * Configure options for form
     * 
     * @param OptionsResolver $resolver
     * 
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => 'AppBundle\Entity\Location\Card',
            ])
            ->setRequired('cardTypes')
        ;
    }
}
