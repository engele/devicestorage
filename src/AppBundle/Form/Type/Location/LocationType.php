<?php

/**
 *
 */

namespace AppBundle\Form\Type\Location;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\VlanProfile;

/**
 * 
 */
class LocationType extends AbstractType
{
    /**
     * Build form
     * 
     * @param FormBuilderInterface $builder
     * @param array                $options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Name',
                'attr' => [
                    'placeholder' => 'Name',
                ],
                'required' => true,
            ])
            ->add('kvzPrefix', TextType::class, [
                'label' => 'KVz Prefix',
                'attr' => [
                    'placeholder' => 'KVz Prefix',
                ],
                'required' => true,
            ])
            ->add('clientIdSuffixStart', NumberType::class, [
                'label' => 'Kundennr. Startwert (die hinteren 4 Ziffern)',
                'attr' => [
                    'placeholder' => 'z.B. 1',
                ],
                'required' => true,
            ])
            ->add('clientIdSuffixEnd', NumberType::class, [
                'label' => 'Kundennr. Endwert (die hinteren 4 Ziffern)',
                'attr' => [
                    'placeholder' => 'z.B. 9999',
                ],
                'required' => true,
            ])
            ->add('vectoring', ChoiceType::class, [
                'label' => 'Vectoring',
                'choices' => [
                    'Ja' => 1,
                    'Nein' => 0,
                ],
                'required' => true,
                'multiple' => false,
                'expanded' => true,
            ])
            ->add('ipAddress', TextType::class, [
                'label' => 'DSLAM IP-Adresse',
                'attr' => [
                    'placeholder' => 'IPv4 Adresse',
                ],
                'required' => false,
            ])
            ->add('realIp', TextType::class, [
                'label' => 'DSLAM IP-Adresse (real)',
                'attr' => [
                    'placeholder' => 'IPv4 Adresse',
                ],
                'required' => false,
            ])
            ->add('configurable', ChoiceType::class, [
                'label' => 'Ist konfigurierbar?',
                'choices' => [
                    'Ja' => 1,
                    'Nein' => 0,
                ],
                'required' => true,
                'multiple' => false,
                'expanded' => true,
            ])
            ->add('logonPort', NumberType::class, [
                'label' => 'Logon Port',
                'attr' => [
                    'placeholder' => 'Logon Port',
                ],
                'required' => false,
            ])
            ->add('logonUsername', TextType::class, [
                'label' => 'Logon Username',
                'attr' => [
                    'placeholder' => 'Logon Username',
                ],
                'required' => false,
            ])
            ->add('logonPassword', TextType::class, [
                'label' => 'Logon Passwort',
                'attr' => [
                    'placeholder' => 'Logon Passwort',
                ],
                'required' => false,
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Typ des Standorts',
                'choices' => \AppBundle\Util\DslamType::getTypes(),
                'placeholder' => 'Auswählen',
                'required' => false,
            ])
            ->add('acsType', ChoiceType::class, [
                'label' => 'ACS Typ',
                'choices' => \AppBundle\Util\AcsType::getTypes(),
                'placeholder' => 'Auswählen',
                'required' => false,
            ])
            ->add('gateway', TextType::class, [
                'label' => 'Gateway IP-Adresse',
                'attr' => [
                    'placeholder' => 'IPv4 Adresse',
                ],
                'required' => false,
            ])
            ->add('subnetmask', TextType::class, [
                'label' => 'Subnetmask (CIDR)',
                'attr' => [
                    'placeholder' => '/24',
                ],
                'required' => false,
            ])
            ->add('processor', TextType::class, [
                'label' => 'Prozessorkarte',
                'attr' => [
                    'placeholder' => 'Prozessorkarte',
                ],
                'required' => false,
            ])
            ->add('currentSoftwareRelease', TextType::class, [
                'label' => 'DSLAM Software Release',
                'attr' => [
                    'placeholder' => 'DSLAM Software Release',
                ],
                'required' => false,
            ])
            ->add('defaultOntFirmwareRelease', TextType::class, [
                'label' => 'Default ONT Firmware Release',
                'attr' => [
                    'placeholder' => 'Default ONT Firmware Release',
                ],
                'required' => false,
            ])
            ->add('vlanPppoeLocal', NumberType::class, [
                'label' => 'VLAN PPPoE Local',
                'attr' => [
                    'placeholder' => 'VLAN PPPoE Local',
                ],
                'required' => false,
            ])
            ->add('vlanIptvLocal', NumberType::class, [
                'label' => 'VLAN IP-TV Local',
                'attr' => [
                    'placeholder' => 'VLAN IP-TV Local',
                ],
                'required' => false,
            ])
            ->add('vlanAcsLocal', NumberType::class, [
                'label' => 'VLAN ACS Local',
                'attr' => [
                    'placeholder' => 'VLAN ACS Local',
                ],
                'required' => false,
            ])
            ->add('vlanPppoe', NumberType::class, [
                'label' => 'VLAN PPPoE',
                'attr' => [
                    'placeholder' => 'VLAN PPPoE',
                ],
                'required' => false,
            ])
            ->add('vlanIptv', NumberType::class, [
                'label' => 'VLAN IP-TV',
                'attr' => [
                    'placeholder' => 'VLAN IP-TV',
                ],
                'required' => false,
            ])
            ->add('vlanAcs', NumberType::class, [
                'label' => 'VLAN ACS',
                'attr' => [
                    'placeholder' => 'VLAN ACS',
                ],
                'required' => false,
            ])
            ->add('vlanDcn', NumberType::class, [
                'label' => 'VLAN DCN',
                'attr' => [
                    'placeholder' => 'VLAN DCN',
                ],
                'required' => false,
            ])
            ->add('acsQosProfile', TextType::class, [
                'label' => 'ACS QoS Profile',
                'attr' => [
                    'placeholder' => 'ACS QoS Profile',
                ],
                'required' => false,
            ])
            ->add('ontSerialNumber', TextType::class, [
                'label' => 'ONT Seriennummer',
                'attr' => [
                    'placeholder' => 'Nur unselbstständige MDU',
                ],
                'required' => false,
            ])
            ->add('ontFirmwareVersion', TextType::class, [
                'label' => 'ONT Firmwareversion',
                'attr' => [
                    'placeholder' => 'Nur unselbstständige MDU',
                ],
                'required' => false,
            ])
            ->add('portDefaultVlanProfiles', EntityType::class, [
                'label' => 'Default VLAN-Profile für Ports',
                'class' => VlanProfile::class,
                'choice_label' => 'name',
                'required' => false,
                'multiple' => true,
            ])
            ->add('customerDefaultVlanProfiles', EntityType::class, [
                'label' => 'Default VLAN-Profile für Kunden',
                'class' => VlanProfile::class,
                'choice_label' => 'name',
                'required' => false,
                'multiple' => true,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Speichern',
            ])
        ;
    }

    /**
     * Configure options for form
     * 
     * @param OptionsResolver $resolver
     * 
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => 'AppBundle\Entity\Location\Location',
            ])
        ;
    }
}
