<?php

/**
 *
 */

namespace AppBundle\Form\Type\Location;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\VlanProfile;

/**
 * 
 */
class CardPortType extends AbstractType
{
    /**
     * Build form
     * 
     * @param FormBuilderInterface $builder
     * @param array                $options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('connectionType', EntityType::class, [
                'label' => 'Anschlusstype',
                'class' => \AppBundle\Entity\Location\CardType::class,
                'choice_label' => 'name',
                'required' => false,
                'multiple' => false,
                'placeholder' => 'Standard (wie Karte)',
            ])
            ->add('maxUnicastMacs', NumberType::class, [
                'label' => 'max. unicast Macs',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Standard',
                ],
            ])
        ;
    }

    /**
     * Configure options for form
     * 
     * @param OptionsResolver $resolver
     * 
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        /*$resolver
            ->setDefaults([
                'data_class' => 'AppBundle\Entity\Location\Card',
            ])
            ->setRequired('cardTypes')
        ;*/
    }
}
