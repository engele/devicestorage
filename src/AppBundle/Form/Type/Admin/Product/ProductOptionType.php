<?php

/**
 *
 */

namespace AppBundle\Form\Type\Admin\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 *
 */
class ProductOptionType extends AbstractType
{
    /**
     * Build form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // disabled has to be false for new entry
        $this->disabled_identifier = !$options['new'];

        $builder
            ->add('identifier', TextType::class, [
                'label' => 'Produktcode',
                'attr' => [
                    'placeholder' => 'Produktcode',
                    'disabled' => $this->disabled_identifier,
                ],
            ])
            ->add('name', TextType::class, [
                'label' => 'Name der Option',
                'attr' => [
                    'placeholder' => 'Name der Option',
                ],
            ])
            ->add('purtelProductId', TextType::class, [
                'label' => 'Purtel Produkt-ID',
                'attr' => [
                    'placeholder' => 'Purtel Produkt-ID',
                ],
                'required' => false,
            ])
            ->add('priceNet', MoneyType::class, [
                'label' => 'Netto Preis',
                'attr' => [
                    'placeholder' => 'Netto Preis',
                ],
            ])
            ->add('priceGross', MoneyType::class, [
                'label' => 'Brutto Preis',
                'attr' => [
                    'placeholder' => 'Brutto Preis',
                ],
            ])
            ->add('active', CheckboxType::class, [
                'label' => 'Aktiv',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Speichern',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'new' => false,
        ]);
    }
}
