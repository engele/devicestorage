<?php

/**
 *
 */

namespace AppBundle\Form\Type\Admin\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\CallbackTransformer;

/**
 * 
 */
class MainProductType extends AbstractType
{
    /**
     * Build form
     * 
     * @param FormBuilderInterface $builder
     * @param array                $options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $categories = [];

        foreach ($options['categories'] as $category) {
            $categories[
                $category->getName()
            ] = $category->getId();
        }

        $cardTypes = [];

        foreach ($options['cardTypes'] as $cardType) {
            $cardTypes[
                $cardType->getName()
            ] = $cardType->getId();
        }

        $builder
            ->add('identifier', TextType::class, [
                'label' => 'Produkt-Code',
                'attr' => [
                    'placeholder' => 'Produkt-Code',
                ],
            ])
            ->add('name', TextType::class, [
                'label' => 'Produktname',
                'attr' => [
                    'placeholder' => 'Produktname',
                ],
            ])
            ->add('category', ChoiceType::class, [
                'label' => 'Kategorie',
                'choices' => $categories,
                'placeholder' => 'Kategorie auswählen',
                'required' => false,
            ])
            ->add('xdslServiceProfile', NumberType::class, [
                'label' => 'xdsl-ServiceProfile',
                'required' => false,
                'attr' => [
                    'placeholder' => 'xdsl-ServiceProfile',
                ],
            ])
            ->add('rtxProfile', NumberType::class, [
                'label' => 'RTX-Profile',
                'required' => false,
                'attr' => [
                    'placeholder' => 'RTX-Profile',
                ],
            ])
            ->add('gponDownload', TextType::class, [
                'label' => 'GPON-Download',
                'required' => false,
                'attr' => [
                    'placeholder' => 'GPON-Download',
                ],
            ])
            ->add('gponUpload', TextType::class, [
                'label' => 'GPON-Upload',
                'required' => false,
                'attr' => [
                    'placeholder' => 'GPON-Upload',
                ],
            ])
            ->add('ethernetUpDown', TextType::class, [
                'label' => 'Ethernet-Up-Down',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Ethernet-Up-Down',
                ],
            ])
            ->add('download', NumberType::class, [
                'label' => 'Download',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Download',
                ],
            ])
            ->add('upload', NumberType::class, [
                'label' => 'Upload',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Upload',
                ],
            ])
            ->add('gfastDownstreamRate', TextType::class, [
                'label' => 'G.fast Downstream-Rate',
                'required' => false,
                'attr' => [
                    'placeholder' => 'G.fast Downstream-Rate',
                ],
            ])
            ->add('gfastUpstreamRate', TextType::class, [
                'label' => 'G.fast Upstream-Rate',
                'required' => false,
                'attr' => [
                    'placeholder' => 'G.fast Upstream-Rate',
                ],
            ])
            ->add('priceNet', NumberType::class, [
                'label' => 'Preis (netto)',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Preis (netto)',
                ],
            ])
            ->add('priceGross', NumberType::class, [
                'label' => 'Preis (brutto)',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Preis (brutto)',
                ],
            ])
            ->add('active', CheckboxType::class, [
                'label' => 'Aktiv',
                'required' => false,
            ])
            ->add('cardType', ChoiceType::class, [
                'label' => 'Anschluss-Typ',
                'choices' => $cardTypes,
                'placeholder' => 'Anschluss-Typ auswählen',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Speichern',
            ])
        ;

        $builder->get('category')
            ->addModelTransformer(new CallbackTransformer(
                function ($category) {
                    if ($category instanceOf \AppBundle\Entity\Product\MainProductCategory) {
                        return (string) $category->getId();
                    }

                    return null;
                },
                function ($categoryId) use ($options) {
                    if (empty($categoryId)) {
                        return $categoryId;
                    }

                    foreach ($options['categories'] as $category) {
                        if ($categoryId == $category->getId()) {
                            return $category;
                        }
                    }

                    return null;
                }
            ))
        ;

        $builder->get('cardType')
            ->addModelTransformer(new CallbackTransformer(
                function ($cardType) {
                    if ($cardType instanceOf \AppBundle\Entity\Location\CardType) {
                        return (string) $cardType->getId();
                    }

                    return null;
                },
                function ($cardTypeId) use ($options) {
                    if (empty($cardTypeId)) {
                        return $cardTypeId;
                    }

                    foreach ($options['cardTypes'] as $cardType) {
                        if ($cardTypeId == $cardType->getId()) {
                            return $cardType;
                        }
                    }

                    return null;
                }
            ))
        ;
    }

    /**
     * Configure options for form
     * 
     * @param OptionsResolver $resolver
     * 
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('categories')
            ->setRequired('cardTypes')
        ;
    }
}
