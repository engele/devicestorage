<?php

/**
 *
 */

namespace AppBundle\Form\Type\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * 
 */
class PurtelAdminAccountType extends AbstractType
{
    /**
     * Build form
     * 
     * @param FormBuilderInterface $builder
     * @param array                $options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('identifier', TextType::class, [
                'label' => 'Unique Identifier Text',
                'attr' => [
                    'placeholder' => 'Unique Identifier Text',
                ],
                'required' => true,
            ])
            ->add('username', TextType::class, [
                'label' => 'Benutzername',
                'attr' => [
                    'placeholder' => 'Enter a username',
                ],
                'required' => true,
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Passwort',
                'attr' => [
                    'placeholder' => 'Passwort',
                ],
                'required' => true,
            ])
            ->add('authorizedUsers', ChoiceType::class, [
                'label' => 'Authorized Users',
                'choices' => $options['users'],
                'choice_value' => function (\AppBundle\Entity\User $entity) {
                    return $entity->getId();
                },
                'choice_label' => function (\AppBundle\Entity\User $value, $key) {
                    return $value->getFullname();
                },
                'multiple' => true,
                'placeholder' => 'Auswählen',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Speichern',
            ])
        ;
    }

    /**
     * Configure options for form
     * 
     * @param OptionsResolver $resolver
     * 
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('users');
    }
}
