<?php

/**
 *
 */

namespace AppBundle\Form\Type\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Entity\Location\CardType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * 
 */
class SpectrumProfileType extends AbstractType
{
    /**
     * Build form
     * 
     * @param FormBuilderInterface $builder
     * @param array                $options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('identifier', TextType::class, [
                'label' => 'Anzeigename',
                'attr' => [
                    'placeholder' => 'Anzeigename',
                ],
            ])
            ->add('profile', TextType::class, [
                'label' => 'Profil',
                'attr' => [
                    'placeholder' => 'Profil',
                ],
            ])
            ->add('connectionType', EntityType::class, [
                'label' => 'Anschlusstyp',
                'class' => CardType::class,
                'choice_label' => 'name',
                'required' => true,
                'multiple' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Speichern',
            ])
        ;
    }
}
