<?php

/**
 *
 */

namespace AppBundle\Form\Type\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use AppBundle\Form\Type\Admin\VlanType;

/**
 * 
 */
class VlanProfileType extends AbstractType
{
    /**
     * Build form
     * 
     * @param FormBuilderInterface $builder
     * @param array                $options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Profilname',
                'attr' => [
                    'placeholder' => 'Profilname',
                ],
            ])
            ->add('pvid', TextType::class, [
                'label' => 'PVID / Nativ VLAN',
                'required' => false,
                'attr' => [
                    'placeholder' => 'PVID / Nativ VLAN',
                ],
            ])
            ->add('vlans', CollectionType::class, [
                'entry_type' => VlanType::class,
                'allow_add' => true,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Speichern',
            ])
        ;
    }
}
