<?php

/**
 *
 */

namespace AppBundle\Form\Type\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * 
 */
class UserAccountType extends AbstractType
{
    /**
     * Build form
     * 
     * @param FormBuilderInterface $builder
     * @param array                $options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('roles', ChoiceType::class, [
                'label' => 'Konto-Typ',
                'choices' => $options['roles'],
                'multiple' => true,
                'placeholder' => 'Auswählen',
            ])
            ->add('username', TextType::class, [
                'label' => 'Benutzername',
                'attr' => [
                    'placeholder' => 'Enter a username',
                ],
            ])
            ->add('fullName', TextType::class, [
                'label' => 'Kompletter Name',
                'attr' => [
                    'placeholder' => 'z.B. Harald Müller',
                ],
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-Mail Adresse',
                'attr' => [
                    'placeholder' => 'E-Mail Adresse',
                ],
            ])
            ->add('groupwareUsername', TextType::class, [
                'label' => 'Groupware Username',
                'attr' => [
                    'placeholder' => 'Groupware Username',
                ],
                'required' => false,
            ])
            ->add('includeInHistoryReporting', CheckboxType::class, [
                'label' => 'Für Verlaufsauswertung aktivieren?',
                'required' => false,
            ])
            ->add('newPassword', PasswordType::class, [
                'label' => 'Passwort',
                'attr' => [
                    'placeholder' => 'Passwort',
                ],
                'required' => false,
            ])
            ->add('newPasswordRepeated', PasswordType::class, [
                'label' => 'Passwort wiederholen',
                'attr' => [
                    'placeholder' => 'Passwort wiederholen',
                ],
                'required' => false,
            ])
            ->add('isActive', CheckboxType::class, [
                'label' => 'Aktiv',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Speichern',
            ])
        ;
    }

    /**
     * Configure options for form
     * 
     * @param OptionsResolver $resolver
     * 
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('roles');
    }
}
