<?php

/**
 *
 */

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * 
 */
class IpRangeType extends AbstractType
{
    /**
     * Build form
     * 
     * @param FormBuilderInterface $builder
     * @param array                $options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Name',
                'attr' => [
                    'placeholder' => 'Name',
                ],
                'required' => true,
            ])
            ->add('startAddress', TextType::class, [
                'label' => 'Erste-Adresse',
                'attr' => [
                    'placeholder' => 'IPv4 Adresse',
                ],
                'required' => true,
            ])
            ->add('endAddress', TextType::class, [
                'label' => 'Letzte-Adresse',
                'attr' => [
                    'placeholder' => 'IPv4 Adresse',
                ],
                'required' => true,
            ])
            ->add('subnetmask', TextType::class, [
                'label' => 'Subnetmask',
                'attr' => [
                    'placeholder' => 'IPv4 Adresse',
                ],
                'required' => false,
            ])
            ->add('gateway', TextType::class, [
                'label' => 'Gateway',
                'attr' => [
                    'placeholder' => 'IPv4 Adresse',
                ],
                'required' => false,
            ])
            ->add('vlanId', NumberType::class, [
                'label' => 'Vlan-Id',
                'attr' => [
                    'placeholder' => 'Nummer',
                ],
                'required' => false,
            ])
            ->add('pppoeType', ChoiceType::class, [
                'label' => 'PPPoE-Typ',
                'choices' => \AppBundle\Util\PppoeType::getTypes(),
                'placeholder' => 'Auswählen',
                'required' => false,
            ])
            ->add('pppoeLocation', TextType::class, [
                'label' => 'Standort des PPPoEs',
                'attr' => [
                    'placeholder' => 'Standort des PPPoEs',
                ],
                'required' => false,
            ])
            ->add('pppoeIp', TextType::class, [
                'label' => 'PPPoE IP-Adresse',
                'attr' => [
                    'placeholder' => 'IPv4 Adresse',
                ],
                'required' => false,
            ])
            ->add('uplinkProvider', ChoiceType::class, [
                'label' => 'Uplink-Provider',
                'choices' => \AppBundle\Util\UplinkProviderType::getTypes(),
                'placeholder' => 'Auswählen',
                'required' => true,
            ])
            ->add('dns1', TextType::class, [
                'label' => 'DNS 1',
                'attr' => [
                    'placeholder' => 'DNS 1',
                ],
                'required' => false,
            ])
            ->add('dns2', TextType::class, [
                'label' => 'DNS 2',
                'attr' => [
                    'placeholder' => 'DNS 2',
                ],
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Speichern',
            ])
        ;
    }

    /**
     * Configure options for form
     * 
     * @param OptionsResolver $resolver
     * 
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\IpRange',
        ]);
    }
}
