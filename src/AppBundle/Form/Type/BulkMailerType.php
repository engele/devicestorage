<?php

/**
 *
 */

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * 
 */
class BulkMailerType extends AbstractType
{
    /**
     * Build form
     * 
     * @param FormBuilderInterface $builder
     * @param array                $options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('addressesFile', FileType::class, [
                'label' => 'Empfänger-Liste',
                'attr' => [
                    'accept' => '.csv',
                    'placeholder' => 'CSV-Datei (Empfänger)',
                ],
                'required' => true,
            ])
            ->add('emails', TextType::class, [
                'label' => 'Empfänger für Statusmail',
                'attr' => [
                    'placeholder' => 'eMail',
                    'inputType' => 'email',
                ],
                'required' => true,
            ])
            ->add('subject', TextType::class, [
                'label' => 'Betreff',
                'attr' => [
                    'placeholder' => 'Betreff',
                ],
                'required' => true,
            ])
            ->add('messageText', HiddenType::class, [
                'label' => 'Nachricht',
                'attr' => [
                    'placeholder' => 'Nachricht...',
                ],
                'required' => true,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Senden',
            ])
            ->add('sendTestMail', SubmitType::class, [
                'label' => 'Test eMail',
            ])
        ;
    }
    
}
