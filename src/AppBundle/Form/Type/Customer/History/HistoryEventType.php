<?php

/**
 *
 */

namespace AppBundle\Form\Type\Customer\History;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * 
 */
class HistoryEventType extends AbstractType
{
    /**
     * Build form
     * 
     * @param FormBuilderInterface $builder
     * @param array                $options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('event', TextType::class, [
                'label' => 'Name',
                'attr' => [
                    'placeholder' => 'Name',
                ],
            ])
            ->add('group', TextType::class, [
                'label' => 'Gruppe',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Gruppe',
                ],
            ])
            ->add('orderNumber', NumberType::class, [
                'label' => 'Reihenfolge',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Reihenfolge',
                ],
            ])
            ->add('priceNet', NumberType::class, [
                'label' => 'Preis (netto)',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Preis (netto)',
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Speichern',
            ])
        ;
    }
}
