<?php

/**
 *
 */

namespace AppBundle\Form\Type\MyAccount;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * 
 */
class ChangePasswordType extends AbstractType
{
    /**
     * Build form
     * 
     * @param FormBuilderInterface $builder
     * @param array                $options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /*$builder
            ->add('oldPassword', PasswordType::class, [
                'label' => 'Altes Passwort',
                'attr' => [
                    'placeholder' => 'Altes Passwort',
                ],
                'required' => true,
            ])
            ->add('newPassword', PasswordType::class, [
                'label' => 'Neues Passwort',
                'attr' => [
                    'placeholder' => 'Neues Passwort',
                ],
                'required' => true,
            ])
            ->add('newPasswordRepeated', PasswordType::class, [
                'label' => 'Neues Passwort (wiederholen)',
                'attr' => [
                    'placeholder' => 'Neues Passwort (wiederholen)',
                ],
                'required' => true,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Speichern',
            ])
        ;*/

        $builder
            ->add('fullName', TextType::class, [
                'label' => 'Kompletter Name',
                'attr' => [
                    'placeholder' => 'z.B. Harald Müller',
                ],
                'required' => true,
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-Mail Adresse',
                'attr' => [
                    'placeholder' => 'E-Mail Adresse',
                ],
                'required' => true,
            ])
            ->add('newPassword', PasswordType::class, [
                'label' => 'Neues Passwort',
                'attr' => [
                    'placeholder' => 'Neues Passwort',
                ],
                'required' => false,
            ])
            ->add('newPasswordRepeated', PasswordType::class, [
                'label' => 'Neues Passwort (wiederholen)',
                'attr' => [
                    'placeholder' => 'Neues Passwort (wiederholen)',
                ],
                'required' => false,
            ])
            ->add('currentPassword', PasswordType::class, [
                'label' => 'Altes Passwort',
                'attr' => [
                    'placeholder' => 'Altes Passwort',
                ],
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Speichern',
            ])
        ;
    }
}
