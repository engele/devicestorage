<?php

/**
 *
 */

namespace AppBundle\Service\BackgroundProcessConnector;

/**
 *
 */
class Command
{
    /**
     * @var string
     */
    protected $command;

    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var string
     */
    protected $cwd;

    /**
     * @var array
     */
    protected $mailAddresses = [];

    /**
     * Alias for Constructor
     * 
     * @return Command
     */
    public static function newInstance() : Command
    {
        return new self();
    }

    /**
     * Set command
     * 
     * @param string $command
     * 
     * @return Command
     */
    public function setCommand($command) : Command
    {
        $this->command = $command;

        return $this;
    }

    /**
     * Set current working directory (cwd)
     * 
     * @param string $cwd
     * 
     * @return Command
     */
    public function setCwd($cwd) : Command
    {
        $this->cwd = $cwd;

        return $this;
    }

    /**
     * Add an option
     * 
     * @param string $option
     * 
     * @return Command
     */
    public function addOption($option) : Command
    {
        $this->options[] = $option;

        return $this;
    }

    /**
     * Set options
     * 
     * @param array $options
     * 
     * @return Command
     */
    public function setOptions($options) : Command
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Add an mail address
     * 
     * @param string $mailAddress
     * 
     * @return Command
     */
    public function addMailAddress($mailAddress) : Command
    {
        $this->mailAddresses[] = $mailAddress;

        return $this;
    }

    /**
     * Set mail addresses
     * 
     * @param array $mailAddresses
     * 
     * @return Command
     */
    public function setMailAddresses($mailAddresses) : Command
    {
        $this->mailAddresses = $mailAddresses;

        return $this;
    }

    /**
     * Get command
     * 
     * @return string
     */
    public function getCommand() : string
    {
        return $this->command;
    }

    /**
     * Get current working direktory (cwd)
     * 
     * @return string
     */
    public function getCwd() : ?string
    {
        return $this->cwd;
    }

    /**
     * Get options
     * 
     * @return array
     */
    public function getOptions() : array
    {
        return $this->options;
    }

    /**
     * Get mail addresses
     * 
     * @return array
     */
    public function getMailAddresses() : array
    {
        return $this->mailAddresses;
    }
}
