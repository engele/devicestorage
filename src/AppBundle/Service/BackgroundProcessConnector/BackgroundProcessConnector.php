<?php

/**
 * 
 */

namespace AppBundle\Service\BackgroundProcessConnector;

require_once __DIR__.'/../../../../web/csvExport/vendor/streamsocket/streamsocket/Lib/StreamSocket.php';
use StreamSocket\Lib\StreamSocket;
use AppBundle\Service\BackgroundProcessConnector\Command;

/**
 * 
 */
class BackgroundProcessConnector
{
    /**
     * Constructor
     */
    public function __construct($backgroundProcessIp, $backgroundProcessPort)
    {
        $this->serverIp = $backgroundProcessIp;
        $this->serverPort = $backgroundProcessPort;
    }

    /**
     * Trigger new process
     * 
     * @param Command $command
     * 
     * @throws \Exception when connection failed
     * 
     * @return int - pid
     */
    public function triggerProcess(Command $command) : int
    {
        // Create new tcp stream socket to DSLAM
        $stream = $this->createConnection();

        $process = json_encode([
            'command' => $command->getCommand(),
            'cwd' => $command->getCwd(),
            'options' => $command->getOptions(),
            'sendOutputTo' => $command->getMailAddresses(),
        ]);

        $stream
            ->waitUntilStringIsFound('Ready to handle data')
            ->write($process)
            ->waitUntilStringIsFound('Process now running')
            ->disconnect();

        $matches;

        preg_match('/pid: (\d+)/', $stream->getLastReadData(), $matches);

        return $matches[1];
    }

    /**
     * Create StreamSocket instance
     * 
     * @throws \Exception when connection failed
     * 
     * @return StreamSocket
     */
    public function createConnection() : StreamSocket
    {
        return StreamSocket::newInstance()->connect(
            StreamSocket::TCP, // connection type
            $this->serverIp,  // connection ip-address
            $this->serverPort, // connection port
            array('sec' => 5, 'usec' => 0) // connection timeout
        );
    }

    /**
     * Check if BackgroundProcessRunner is running
     * 
     * @return boolean
     */
    public function isProcessRunnerReachable() : bool
    {
        try {
            // Create new tcp stream socket to DSLAM
            $stream = $this->createConnection();

            $stream->disconnect();

            return true;
        } catch (\Exception $exception) {} // exception not needed

        return false;
    }

    /**
     * Get current process-log from process by pid
     * 
     * @throws \Exception when connection failed
     * 
     * @return string
     */
    public function getProcessLog($pid) : string
    {
        // Create new tcp stream socket to DSLAM
        $stream = $this->createConnection();

        $stream
            ->waitUntilStringIsFound('Ready to handle data')
            ->write(json_encode(['get-output-for-pid' => $pid]));

        $stream->getLastReadData();

        $data = $stream->waitUntilStringIsFound("\0x04")->getLastReadData();

        $stream->disconnect();

        return $data;
    }

    /**
     * Get current running processes
     * 
     * @throws \Exception when connection failed
     * 
     * @return array
     */
    public function getRunningProcesses() : array
    {
        // Create new tcp stream socket to DSLAM
        $stream = $this->createConnection();

        $stream
            ->waitUntilStringIsFound('Ready to handle data')
            ->write('list-running-processes');

        $stream->getLastReadData();

        $data = $stream->waitUntilStringIsFound("\n")->getLastReadData();

        $stream->disconnect();

        return json_decode($data, true);
    }
}
