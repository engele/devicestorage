<?php

/**
 * This file is part of the wkv project.
 */

namespace Wkv\Entity;

use Doctrine\ORM\Mapping as ORM;

/*
 * Entity class for Category
 * 
 * @ORM\Entity
 * @ORM\Table(name="categories")
 */
class Category
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Category name
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=200, nullable=false)
     */
    protected $name;

    /**
     * Is active?
     * 
     * @var boolean
     * 
     * @ORM\Column(type="boolean")
     */
    protected $active;

    /**
     * Products
     * 
     * @var array
     * 
     * @ORM\ManyToMany(targetEntity="Wkv\Entity\ForWkv\Product")
     * @ORM\JoinTable(name="products_to_categories",
     *      joinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")}
     * )
     */
    protected $products;

    /**
     * Constructor
     */
    public function __construct() {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Category
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add product
     *
     * @param \Wkv\Entity\ForWkv\Product $product
     *
     * @return Category
     */
    public function addProduct(\Wkv\Entity\ForWkv\Product $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param \Wkv\Entity\ForWkv\Product $product
     */
    public function removeProduct(\Wkv\Entity\ForWkv\Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }
}
