<?php

/**
 *
 */

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\ServingAddress;

/**
 *
 */
class ServingAddressRepository extends EntityRepository
{
    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param int $limit
     * @param null $offset
     * 
     * @return array|mixed
     */
    public function findByCustom(array $criteria, ?array $orderBy = ['sa.id'], $limit = null, $offset = null)
    {
        return $this->createQueryBuilder('sa')
            ->where('sa.id LIKE :searchPhrase')
            ->orWhere('sa.zipCode LIKE :searchPhrase')
            ->orWhere('sa.city LIKE :searchPhrase')
            ->orWhere('sa.street LIKE :searchPhrase')
            ->orWhere('sa.houseNumber LIKE :searchPhrase')
            ->setParameter('searchPhrase', '%' . $criteria['searchPhrase'] . '%')
            ->orderBy(implode(',', $orderBy), 'ASC')
            ->getQuery()
            ->setMaxResults($limit)
            ->getResult();
    }

    /**
     * 
     */
    public function findDistinctStreetsByZip($zip)
    {
        return $this->createQueryBuilder('sa')
            ->select('sa.street')
            ->where('sa.zipCode = :zipCode')
            ->distinct()
            ->setParameter('zipCode', $zip)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * 
     */
    public function findDistinctHousenumbersByZipAndStreet($zip, $street)
    {
        return $this->createQueryBuilder('sa')
            ->select('sa.houseNumber')
            ->where('sa.zipCode = :zipCode')
            ->andWhere('sa.street = :street')
            ->distinct()
            ->setParameter('zipCode', $zip)
            ->setParameter('street', $street)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Find one by unique constraint
     */
    public function findOneByUniqueConstraint(ServingAddress $servingAddress)
    {
        return $this->createQueryBuilder('sa')
            ->where('sa.zipCode = :zipCode')
            ->andWhere('sa.city = :city')
            ->andWhere('sa.street = :street')
            ->andWhere('sa.houseNumber = :houseNumber')
            ->setParameter('zipCode', $servingAddress->getZipCode())
            ->setParameter('city', $servingAddress->getCity())
            ->setParameter('street', $servingAddress->getStreet())
            ->setParameter('houseNumber', $servingAddress->getHouseNumber())
            ->getQuery()->getOneOrNullResult();
    }
}
