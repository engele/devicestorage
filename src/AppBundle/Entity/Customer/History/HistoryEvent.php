<?php

/**
 * This file is part of the wkv project.
 */

namespace AppBundle\Entity\Customer\History;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity class for CustomerHistoryEvent
 * 
 * @ORM\Entity
 * @ORM\Table(name="customer_history_events")
 */
class HistoryEvent
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=140, nullable=false, name="event")
     */
    protected $event;

    /**
     * @var integer
     * 
     * @ORM\Column(type="integer", nullable=true, name="order_number")
     */
    protected $orderNumber;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=40, nullable=true, name="group_id")
     */
    protected $group;

    /**
     * Price (net)
     * 
     * @var float
     * 
     * @ORM\Column(type="decimal", precision=7, scale=2, nullable=true, name="price_net")
     */
    protected $priceNet;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set event.
     *
     * @param string $event
     *
     * @return HistoryEvent
     */
    public function setEvent($event)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event.
     *
     * @return string
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set orderNumber.
     *
     * @param int|null $orderNumber
     *
     * @return HistoryEvent
     */
    public function setOrderNumber($orderNumber = null)
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }

    /**
     * Get orderNumber.
     *
     * @return int|null
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * Set group.
     *
     * @param string|null $group
     *
     * @return HistoryEvent
     */
    public function setGroup($group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group.
     *
     * @return string|null
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set priceNet.
     *
     * @param string|null $priceNet
     *
     * @return HistoryEvent
     */
    public function setPriceNet($priceNet = null)
    {
        $this->priceNet = $priceNet;

        return $this;
    }

    /**
     * Get priceNet.
     *
     * @return string|null
     */
    public function getPriceNet()
    {
        return $this->priceNet;
    }
}
