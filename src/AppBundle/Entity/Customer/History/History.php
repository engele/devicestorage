<?php

namespace AppBundle\Entity\Customer\History;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity class for CustomerHistory
 * 
 * @ORM\Entity
 * @ORM\Table(name="customer_history")
 */
class History
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false, name="customer_id")
     */
    protected $customerId;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false, name="event_id")
     */
    protected $eventId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=40, nullable=false, name="created_by")
     */
    protected $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false, name="created_at")
     */
    protected $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false, name="note")
     */
    protected $note;

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) : History
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getCustomerId() : int
    {
        return $this->customerId;
    }

    /**
     * @param int $customerId
     */
    public function setCustomerId(int $customerId) : History
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * @return int
     */
    public function getEventId() : int
    {
        return $this->eventId;
    }

    /**
     * @param int $eventId
     */
    public function setEventId(int $eventId) : History
    {
        $this->eventId = $eventId;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy() : string
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     */
    public function setCreatedBy(string $createdBy) : History
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt) : History
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote() : string
    {
        return $this->note;
    }

    /**
     * @param string $note
     */
    public function setNote(string $note) : History
    {
        $this->note = $note;

        return $this;
    }
}
