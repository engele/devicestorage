<?php

/**
 * This file is part of the DICLINA project.
 */

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity class for customer VLAN profiles
 * 
 * @ORM\Entity
 * @ORM\Table(name="customer_vlan_profiles")
 */
class CustomerVlanProfile
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     * 
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $customerId;

    /**
     * @var AppBundle\Entity\VlanProfile
     * 
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\VlanProfile")
     * @ORM\JoinColumn(name="vlan_profile_id", referencedColumnName="id")
     */
    protected $vlanProfile;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerId.
     *
     * @param int $customerId
     *
     * @return CustomerVlanProfile
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * Get customerId.
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * Set vlanProfile.
     *
     * @param \AppBundle\Entity\VlanProfile|null $vlanProfile
     *
     * @return CustomerVlanProfile
     */
    public function setVlanProfile(\AppBundle\Entity\VlanProfile $vlanProfile = null)
    {
        $this->vlanProfile = $vlanProfile;

        return $this;
    }

    /**
     * Get vlanProfile.
     *
     * @return \AppBundle\Entity\VlanProfile|null
     */
    public function getVlanProfile()
    {
        return $this->vlanProfile;
    }
}
