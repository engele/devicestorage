<?php

/**
 * This file is part of the DICLINA project.
 */

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\Location\CardType;
use AppBundle\Entity\SpectrumProfile;
use Doctrine\Bundle\DoctrineBundle\Registry;

/**
 * Entity class for customers technical data
 * 
 * @ORM\Entity
 * @ORM\Table(name="customer_technical_data")
 */
class TechnicalData
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     * 
     * @ORM\Column(type="integer", nullable=false, unique=true)
     */
    protected $customerId;

    /**
     * @var AppBundle\Entity\VlanProfile
     * 
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\SpectrumProfile")
     * @ORM\JoinColumn(name="spectrum_profile_id", referencedColumnName="id")
     * 
     * @ORM\JoinTable(name="technical_data_spectrum_profile", 
     *      joinColumns={@ORM\JoinColumn(name="technical_data_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="spectrum_profile_id", referencedColumnName="id")}
     * )
     */
    protected $spectrumProfiles;

    /**
     * @param float
     * 
     * @ORM\Column(type="decimal", precision=3, scale=1, nullable=true)
     */
    protected $rfOverlaySignalStrength;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->spectrumProfiles = new ArrayCollection();
    }

    /**
     * Get spectrum profile for certain card type
     * 
     * @param \AppBundle\Entity\Location\CardType $cardType
     * 
     * @return \AppBundle\Entity\SpectrumProfile|null
     */
    public function getSpectrumProfileByCardType(CardType $cardType) : ?SpectrumProfile
    {
        foreach ($this->spectrumProfiles as $spectrumProfile) {
            if ($cardType->getId() === $spectrumProfile->getConnectionType()->getId()) {
                return $spectrumProfile;
            }
        }

        return null;
    }

    /**
     * Get all spectrum profiles for certain card type
     * 
     * @param \AppBundle\Entity\Location\CardType $cardType
     * 
     * @return array
     */
    public function getAllSpectrumProfilesByCardType(CardType $cardType) : iterable
    {
        foreach ($this->spectrumProfiles as $spectrumProfile) {
            if ($cardType->getId() === $spectrumProfile->getConnectionType()->getId()) {
                yield $spectrumProfile;
            }
        }
    }

    /**
     * Get spectrum profile for certain card type by card types name (case-insensitive)
     * 
     * @param string $cardTypeName
     * 
     * @return \AppBundle\Entity\SpectrumProfile|null
     */
    public function getSpectrumProfileByCardTypeName($cardTypeName) : ?SpectrumProfile
    {
        foreach ($this->spectrumProfiles as $spectrumProfile) {
            if (0 === strcasecmp($cardTypeName, $spectrumProfile->getConnectionType()->getName())) {
                return $spectrumProfile;
            }
        }

        return null;
    }

    /**
     * Hast spectrum profile?
     * 
     * @return boolean
     */
    public function hasSpectrumProfile(\AppBundle\Entity\SpectrumProfile $spectrumProfile) : bool
    {
        foreach ($this->spectrumProfiles as $profile) {
            if ($profile->getId() === $spectrumProfile->getId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Create instance from array
     * 
     * @param array $array
     * @param Doctrine\Bundle\DoctrineBundle\Registry $doctrine
     * 
     * @return TechnicalData
     */
    public static function createFromArray(array $array, Registry $doctrine)
    {
        $technicalData = new TechnicalData();

        if (isset($array['spectrumProfiles'])) {
            foreach ($array['spectrumProfiles'] as $spectrumProfile) {
                if ($spectrumProfile instanceof SpectrumProfile) {
                    $technicalData->addSpectrumProfile($spectrumProfile);
                } else {
                    $spectrumProfile = $doctrine->getRepository(SpectrumProfile::class)->findOneById((int) $spectrumProfile);

                    if ($spectrumProfile instanceof $spectrumProfile) {
                        $technicalData->addSpectrumProfile($spectrumProfile);
                    }
                }
            }
        }

        return $technicalData;
    } 

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerId.
     *
     * @param int $customerId
     *
     * @return TechnicalData
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * Get customerId.
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * Add spectrumProfile.
     *
     * @param \AppBundle\Entity\SpectrumProfile $spectrumProfile
     *
     * @return TechnicalData
     */
    public function addSpectrumProfile(\AppBundle\Entity\SpectrumProfile $spectrumProfile)
    {
        $this->spectrumProfiles[] = $spectrumProfile;

        return $this;
    }

    /**
     * Remove spectrumProfile.
     *
     * @param \AppBundle\Entity\SpectrumProfile $spectrumProfile
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSpectrumProfile(\AppBundle\Entity\SpectrumProfile $spectrumProfile)
    {
        return $this->spectrumProfiles->removeElement($spectrumProfile);
    }

    /**
     * Get spectrumProfiles.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpectrumProfiles()
    {
        return $this->spectrumProfiles;
    }

    /**
     * Set rfOverlaySignalStrength.
     *
     * @param string|null $rfOverlaySignalStrength
     *
     * @return TechnicalData
     */
    public function setRfOverlaySignalStrength($rfOverlaySignalStrength = null)
    {
        $this->rfOverlaySignalStrength = $rfOverlaySignalStrength;

        return $this;
    }

    /**
     * Get rfOverlaySignalStrength.
     *
     * @return string|null
     */
    public function getRfOverlaySignalStrength()
    {
        return $this->rfOverlaySignalStrength;
    }
}
