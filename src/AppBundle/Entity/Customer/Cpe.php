<?php

/**
 * This file is part of the wkv project.
 */

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity class for customer_terminals
 * 
 * @ORM\Entity
 * @ORM\Table(name="customer_terminals")
 */
class Cpe
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Customer id
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer", nullable=false, name="customer_id")
     */
    protected $customerId;

    /**
     * IP-Address
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=15, nullable=true, name="ip_address")
     */
    protected $ipAddress;

    /**
     * Type
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=40, nullable=true, name="type")
     */
    protected $type;

    /**
     * MAC-Address
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=17, nullable=true, name="mac_address")
     */
    protected $macAddress;

    /**
     * Serialnumber
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=50, nullable=true, name="serialnumber")
     */
    protected $serialnumber;

    /**
     * Remote username
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=50, nullable=true, name="remote_user")
     */
    protected $remoteUser;

    /**
     * Remote password
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=100, nullable=true, name="remote_password")
     */
    protected $remotePassword;

    /**
     * Remote port
     * 
     * @var string
     * 
     * @ORM\Column(type="integer", nullable=true, name="remote_port")
     */
    protected $remotePort;

    /**
     * TV-ID (??)
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=50, nullable=true, name="tv_id")
     */
    protected $tvId;

    /**
     * Check-ID (smart-card-id?)
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=50, nullable=true, name="check_id")
     */
    protected $checkId;

    /**
     * Activation date
     * 
     * @var \DateTime
     * 
     * @ORM\Column(type="date", nullable=true, name="activation_date")
     */
    protected $activationDate;
}
