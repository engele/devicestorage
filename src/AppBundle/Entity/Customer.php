<?php

/**
 * This file is part of the wkv project.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Registry;
use AppBundle\Util\PppoePasswordGenerator;
use AppBundle\Entity\Customer\CustomerVlanProfile;
use AppBundle\Entity\Customer\TechnicalData;

/**
 * Entity class for Customer
 * 
 * This can later be replaced with customer entity from AppBundle when it exits
 * 
 * Currently it must be a MappedSuperclass because customer is not complete yet and we don't want doctrine to drop the customers-table
 * 
 * @ORM\MappedSuperclass
 */
class Customer
{
    /**
     * @var \AppBundle\Entity\Customer\TechnicalData
     */
    protected $technicalData;

    /**
     * @var ArrayCollection
     */
    protected $vlanProfiles;

    /**
     * @var array
     */
    protected $crossSellingProducts;

    /**
     * @ORM\Column(name="external_id_2")
     */
    protected $externalId2;

    /**
     * @ORM\Column(name="oldcontract_porting")
     */
    protected $oldcontractPorting;

    /**
     * @ORM\Column(type="date", name="connection_wish_date")
     */
    protected $connectionWishDate;

    /**
     * @ORM\Column(name="customer_id")
     */
    protected $externalCustomerId;

    /**
     * @ORM\Column(name="customer_electrisity")
     */
    protected $customerElectrisity;

    /**
     * @ORM\Column(name="customer_electrisity_enddate")
     */
    protected $customerElectrisityEnddate;

    /**
     * @ORM\Column(name="add_charge_terminal")
     */
    protected $addChargeTerminal;

    /**
     * @ORM\Column(name="add_customer_on_fb")
     */
    protected $addCustomerOnFb;

    /**
     * @ORM\Column(name="add_ip_address")
     */
    protected $addIpAddress;

    /**
     * @ORM\Column(name="add_ip_address_cost")
     */
    protected $addIpAddressCost;

    /**
     * @ORM\Column(name="add_phone_lines")
     */
    protected $addPhoneLines;

    /**
     * @ORM\Column(name="add_phone_lines_cost")
     */
    protected $addPhoneLinesCost;

    /**
     * @ORM\Column(name="add_phone_nos")
     */
    protected $addPhoneNos;

    /**
     * @ORM\Column(name="add_port")
     */
    protected $addPort;

    /**
     * @ORM\Column(name="add_satellite")
     */
    protected $addSatellite;

    /**
     * @ORM\Column(name="application")
     */
    protected $application;

    /**
     * @ORM\Column(name="application_text")
     */
    protected $applicationText;

    /**
     * @ORM\Column(name="bank_account_bankname")
     */
    protected $bankAccountBankname;

    /**
     * @ORM\Column(name="bank_account_bic")
     */
    protected $bankAccountBic;

    /**
     * @ORM\Column(name="bank_account_bic_new")
     */
    protected $bankAccountBicNew;

    /**
     * @ORM\Column(name="bank_account_holder_firstname")
     */
    protected $bankAccountHolderFirstname;

    /**
     * @ORM\Column(name="bank_account_holder_lastname")
     */
    protected $bankAccountHolderLastname;

    /**
     * @ORM\Column(name="bank_account_iban")
     */
    protected $bankAccountIban;

    /**
     * @ORM\Column(name="bank_account_number")
     */
    protected $bankAccountNumber;

    /**
     * @ORM\Column(name="bank_account_city")
     */
    protected $bankAccountCity;

    /**
     * @ORM\Column(name="bank_account_emailaddress")
     */
    protected $bankAccountEmailaddress;
    
    /**
     * @ORM\Column(name="bank_account_street")
     */
    protected $bankAccountStreet;

    /**
     * @ORM\Column(name="bank_account_streetno")
     */
    protected $bankAccountStreetno;

    /**
     * @ORM\Column(name="bank_account_zipcode")
     */
    protected $bankAccountZipcode;

    /**
     * @ORM\Column(type="date", name="birthday")
     */
    protected $birthday;

    /**
     * @ORM\Column(name="call_data_record")
     */
    protected $callDataRecord;

    /**
     * @ORM\Column(name="cancel_purtel")
     */
    protected $cancelPurtel;

    /**
     * @ORM\Column(name="cancel_purtel_for_date")
     */
    protected $cancelPurtelForDate;

    /**
     * @ORM\Column(name="cancel_tal")
     */
    protected $cancelTal;

    /**
     * @ORM\Column(name="card_id")
     */
    protected $cardId;

    /**
     * @ORM\Column(name="city")
     */
    protected $city;

    /**
     * @ORM\Column(name="clientid")
     */
    protected $clientId;

    /**
     * @ORM\Column(name="clienttype")
     */
    protected $clientType;

    /**
     * @ORM\Column(name="comment")
     */
    protected $comment;

    /**
     * @ORM\Column(name="company")
     */
    protected $company;

    /**
     * @ORM\Column(name="connect_type")
     */
    protected $connectType;

    /**
     * @ORM\Column(name="connection_activation_date")
     */
    protected $connectionActivationDate;

    /**
     * @ORM\Column(name="connection_address_equal")
     */
    protected $connectionAddressEqual;

    /**
     * @ORM\Column(name="connection_city")
     */
    protected $connectionCity;

    /**
     * @ORM\Column(name="connection_fee_height")
     */
    protected $connectionFeeHeight;

    /**
     * @ORM\Column(name="connection_fee_paid")
     */
    protected $connectionFeePaid;

    /**
     * @ORM\Column(name="connection_inactive_date")
     */
    protected $connectionInactiveDate;

    /**
     * @ORM\Column(name="connection_installation")
     */
    protected $connectionInstallation;

    /**
     * @ORM\Column(name="connection_street")
     */
    protected $connectionStreet;

    /**
     * @ORM\Column(name="connection_streetno")
     */
    protected $connectionStreetno;

    /**
     * @ORM\Column(name="connection_zipcode")
     */
    protected $connectionZipcode;

    /**
     * @ORM\Column(name="contract")
     */
    protected $contract;

    /**
     * @ORM\Column(name="contract_acknowledge")
     */
    protected $contractAcknowledge;

    /**
     * @ORM\Column(name="contract_change_to")
     */
    protected $contractChangeTo;

    /**
     * @ORM\Column(name="contract_comment")
     */
    protected $contractComment;

    /**
     * @ORM\Column(name="contract_id")
     */
    protected $contractId;

    /**
     * @ORM\Column(type="date", name="contract_received_date")
     */
    protected $contractReceivedDate;

    /**
     * @ORM\Column(type="date", name="contract_sent_date")
     */
    protected $contractSentDate;

    /**
     * @ORM\Column(name="contract_text")
     */
    protected $contractText;

    /**
     * @ORM\Column(name="contract_version")
     */
    protected $contractVersion;

    /**
     * @ORM\Column(name="credit_rating_date")
     */
    protected $creditRatingDate;

    /**
     * @ORM\Column(name="credit_rating_link")
     */
    protected $creditRatingLink;

    /**
     * @ORM\Column(name="credit_rating_ok")
     */
    protected $creditRatingOk;

    /**
     * @ORM\Column(name="customer_connect_info_date")
     */
    protected $customerConnectInfoDate;

    /**
     * @ORM\Column(name="customer_connect_info_from")
     */
    protected $customerConnectInfoFrom;

    /**
     * @ORM\Column(name="customer_connect_info_how")
     */
    protected $customerConnectInfoHow;

    /**
     * @ORM\Column(name="direct_debit_allowed")
     */
    protected $directDebitAllowed;

    /**
     * @ORM\Column(name="district")
     */
    protected $district;

    /**
     * @ORM\Column(name="DPBO")
     */
    protected $DPBO;

    /**
     * @ORM\Column(name="dslam_arranged_date")
     */
    protected $dslamArrangedDate;

    /**
     * @ORM\Column(name="dslam_card_no")
     */
    protected $dslamCardNo;

    /**
     * @ORM\Column(name="dslam_location")
     */
    protected $dslamLocation;

    /**
     * @ORM\Column(name="dslam_port")
     */
    protected $dslamPort;

    /**
     * @ORM\Column(name="dtag_line")
     */
    protected $dtagLine;

    /**
     * @ORM\Column(name="dtagbluckage_fullfiled")
     */
    protected $dtagbluckageFullfiled;

    /**
     * @ORM\Column(name="dtagbluckage_fullfiled_date")
     */
    protected $dtagbluckageFullfiledDate;

    /**
     * @ORM\Column(name="dtagbluckage_sended")
     */
    protected $dtagbluckageSended;

    /**
     * @ORM\Column(name="dtagbluckage_sended_date")
     */
    protected $dtagbluckageSendedDate;

    /**
     * @ORM\Column(name="emailaddress")
     */
    protected $emailaddress;

    /**
     * @ORM\Column(name="eu_flat")
     */
    protected $euFlat;

    /**
     * @ORM\Column(type="date", name="exp_date_int")
     */
    protected $expDateInt;

    /**
     * @ORM\Column(type="date", name="exp_date_phone")
     */
    protected $expDatePhone;

    /**
     * @ORM\Column(name="fax")
     */
    protected $fax;

    /**
     * @ORM\Column(name="fb_vorab")
     */
    protected $fbVorab;

    /**
     * @ORM\Column(name="final_purtelproduct_date")
     */
    protected $finalPurtelproductDate;

    /**
     * @ORM\Column(name="firewall_router")
     */
    protected $firewallRouter;

    /**
     * @ORM\Column(name="firmware_version")
     */
    protected $firmwareVersion;

    /**
     * @ORM\Column(name="firstname")
     */
    protected $firstname;

    /**
     * @ORM\Column(name="flat_eu_network")
     */
    protected $flatEuNetwork;

    /**
     * @ORM\Column(name="flat_eu_network_cost")
     */
    protected $flatEuNetworkCost;

    /**
     * @ORM\Column(name="flat_german_network")
     */
    protected $flatGermanNetwork;

    /**
     * @ORM\Column(name="flat_german_network_cost")
     */
    protected $flatGermanNetworkCost;

    /**
     * @ORM\Column(name="fordering_costs")
     */
    protected $forderingCosts;

    /**
     * @ORM\Column(name="german_mobile")
     */
    protected $germanMobile;

    /**
     * @ORM\Column(name="german_mobile_price")
     */
    protected $germanMobilePrice;

    /**
     * @ORM\Column(name="german_network")
     */
    protected $germanNetwork;

    /**
     * @ORM\Column(name="german_network_price")
     */
    protected $germanNetworkPrice;

    /**
     * @ORM\Column(name="gf_branch")
     */
    protected $gfBranch;

    /**
     * @ORM\Column(name="gf_cabling")
     */
    protected $gfCabling;

    /**
     * @ORM\Column(name="gf_cabling_cost")
     */
    protected $gfCablingCost;

    /**
     * @ORM\Column(name="gutschrift")
     */
    protected $gutschrift;

    /**
     * @ORM\Column(name="gutschrift_till")
     */
    protected $gutschriftTill;

    /**
     * @ORM\Column(name="hd_plus_card")
     */
    protected $hdPlusCard;

    /**
     * @ORM\Column(name="higher_availability")
     */
    protected $higherAvailability;

    /**
     * @ORM\Column(name="higher_uplink_one")
     */
    protected $higherUplinkOne;

    /**
     * @ORM\Column(name="higher_uplink_two")
     */
    protected $higherUplinkTwo;

    /**
     * @ORM\Column(name="history")
     */
    protected $history;

    /**
     * @ORM\Column(name="house_connection")
     */
    protected $houseConnection;

    /**
     * @ORM\Column(name="id")
     */
    protected $id;

    /**
     * @ORM\Column(name="implementation")
     */
    protected $implementation;

    /**
     * @ORM\Column(name="implementation_text")
     */
    protected $implementationText;

    /**
     * @ORM\Column(name="international_calls_price")
     */
    protected $internationalCallsPrice;

    /**
     * @ORM\Column(name="ip_address")
     */
    protected $ipAddress;

    /**
     * @ORM\Column(name="ip_address_inet")
     */
    protected $ipAddressInet;

    /**
     * @ORM\Column(name="ip_address_phone")
     */
    protected $ipAddressPhone;

    /**
     * @ORM\Column(name="ip_range_id")
     */
    protected $ipRangeId;

    /**
     * @ORM\Column(name="isdn_backup")
     */
    protected $isdnBackup;

    /**
     * @ORM\Column(name="isdn_backup2")
     */
    protected $isdnBackup2;

    /**
     * @ORM\Column(name="Kommando_query")
     */
    protected $KommandoQuery;

    /**
     * @ORM\Column(name="kvz_addition")
     */
    protected $kvzAddition;

    /**
     * @ORM\Column(name="kvz_name")
     */
    protected $kvzName;

    /**
     * @ORM\Column(name="kvz_standort")
     */
    protected $kvzStandort;

    /**
     * @ORM\Column(name="kvz_toggle_no")
     */
    protected $kvzToggleNo;

    /**
     * @ORM\Column(name="lapse_notice_date_inet")
     */
    protected $lapseNoticeDateInet;

    /**
     * @ORM\Column(name="lapse_notice_date_tel")
     */
    protected $lapseNoticeDateTel;

    /**
     * @ORM\Column(name="lastname")
     */
    protected $lastname;

    /**
     * @ORM\Column(name="line_identifier")
     */
    protected $lineIdentifier;

    /**
     * @ORM\Column(name="little_bar")
     */
    protected $littleBar;

    /**
     * @ORM\Column(name="location_id")
     */
    protected $locationId;

    /**
     * @ORM\Column(name="lsa_kvz_partition_id")
     */
    protected $lsaKvzPartitionId;

    /**
     * @ORM\Column(name="lsa_pin")
     */
    protected $lsaPin;

    /**
     * @ORM\Column(name="mac_address")
     */
    protected $macAddress;

    /**
     * @ORM\Column(name="media_converter")
     */
    protected $mediaConverter;

    /**
     * @ORM\Column(name="mobile_flat")
     */
    protected $mobileFlat;

    /**
     * @ORM\Column(name="mobilephone")
     */
    protected $mobilephone;

    /**
     * @ORM\Column(name="moved")
     */
    protected $moved;

    /**
     * @ORM\Column(name="nb_canceled_date")
     */
    protected $nbCanceledDate;

    /**
     * @ORM\Column(name="nb_canceled_date_phone")
     */
    protected $nbCanceledDatePhone;

    /**
     * @ORM\Column(name="network")
     */
    protected $network;

    /**
     * @ORM\Column(name="network_id")
     */
    protected $networkId = 0;

    /**
     * @ORM\Column(name="new_clientid_permission")
     */
    protected $newClientidPermission;

    /**
     * @ORM\Column(name="new_number_date")
     */
    protected $newNumberDate;

    /**
     * @ORM\Column(name="new_number_from")
     */
    protected $newNumberFrom;

    /**
     * @ORM\Column(name="new_numbers_text")
     */
    protected $newNumbersText;

    /**
     * @ORM\Column(name="new_or_ported_phonenumbers")
     */
    protected $newOrPortedPhonenumbers;

    /**
     * @ORM\Column(name="no_eze")
     */
    protected $noEze;

    /**
     * @ORM\Column(name="no_ping")
     */
    protected $noPing;

    /**
     * @ORM\Column(name="notice_period_internet_int")
     */
    protected $noticePeriodInternetInt;

    /**
     * @ORM\Column(name="notice_period_internet_type")
     */
    protected $noticePeriodInternetType;

    /**
     * @ORM\Column(name="notice_period_phone_int")
     */
    protected $noticePeriodPhoneInt;

    /**
     * @ORM\Column(name="notice_period_phone_type")
     */
    protected $noticePeriodPhoneType;

    /**
     * @ORM\Column(name="oldcontract_active")
     */
    protected $oldContractActive;

    /**
     * @ORM\Column(name="old_provider_comment")
     */
    protected $oldProviderComment;

    /**
     * @ORM\Column(name="oldcontract_address_equal")
     */
    protected $oldcontractAddressEqual;

    /**
     * @ORM\Column(name="oldcontract_city")
     */
    protected $oldcontractCity;

    /**
     * @ORM\Column(name="oldcontract_comment")
     */
    protected $oldcontractComment;

    /**
     * @ORM\Column(name="oldcontract_exists")
     */
    protected $oldcontractExists;

    /**
     * @ORM\Column(name="oldcontract_firstname")
     */
    protected $oldcontractFirstname;

    /**
     * @ORM\Column(name="oldcontract_internet_client_cancelled")
     */
    protected $oldcontractInternetClientCancelled;

    /**
     * @ORM\Column(name="oldcontract_internet_clientno")
     */
    protected $oldcontractInternetClientno;

    /**
     * @ORM\Column(name="oldcontract_internet_provider")
     */
    protected $oldcontractInternetProvider;

    /**
     * @ORM\Column(name="oldcontract_lastname")
     */
    protected $oldcontractLastname;

    /**
     * @ORM\Column(name="oldcontract_phone_client_cancelled")
     */
    protected $oldcontractPhoneClientCancelled;

    /**
     * @ORM\Column(name="oldcontract_phone_clientno")
     */
    protected $oldcontractPhoneClientno;

    /**
     * @ORM\Column(name="oldcontract_phone_provider")
     */
    protected $oldcontractPhoneProvider;

    /**
     * @ORM\Column(name="oldcontract_phone_type")
     */
    protected $oldcontractPhoneType;

    /**
     * @ORM\Column(name="oldcontract_street")
     */
    protected $oldcontractStreet;

    /**
     * @ORM\Column(name="oldcontract_streetno")
     */
    protected $oldcontractStreetno;

    /**
     * @ORM\Column(name="oldcontract_title")
     */
    protected $oldcontractTitle;

    /**
     * @ORM\Column(name="oldcontract_zipcode")
     */
    protected $oldcontractZipcode;

    /**
     * @ORM\Column(name="paper_bill")
     */
    protected $paperBill;

    /**
     * @ORM\Column(name="password")
     */
    protected $password;

    /**
     * @ORM\Column(name="patch_date")
     */
    protected $patchDate;

    /**
     * @ORM\Column(name="payment_performance")
     */
    protected $paymentPerformance;

    /**
     * @ORM\Column(name="phone_adapter")
     */
    protected $phoneAdapter;

    /**
     * @ORM\Column(name="phone_comment")
     */
    protected $phoneComment;

    /**
     * @ORM\Column(name="phone_number_added")
     */
    protected $phoneNumberAdded;

    /**
     * @ORM\Column(name="phone_number_block")
     */
    protected $phoneNumberBlock;

    /**
     * @ORM\Column(name="phoneareacode")
     */
    protected $phoneareacode;

    /**
     * @ORM\Column(name="phoneentry_done_date")
     */
    protected $phoneentryDoneDate;

    /**
     * @ORM\Column(name="phoneentry_done_from")
     */
    protected $phoneentryDoneFrom;

    /**
     * @ORM\Column(name="phonenumber")
     */
    protected $phonenumber;

    /**
     * @ORM\Column(name="portdefault_comment")
     */
    protected $portdefaultComment;

    /**
     * @ORM\Column(name="porting")
     */
    protected $porting;

    /**
     * @ORM\Column(name="porting_text")
     */
    protected $portingText;

    /**
     * @ORM\Column(name="pppoe_config_date")
     */
    protected $pppoeConfigDate;

    /**
     * @ORM\Column(name="productname")
     */
    protected $productname;

    /**
     * @ORM\Column(name="prospect_supply_status")
     */
    protected $prospectSupplyStatus;

    /**
     * @ORM\Column(name="purtel_bluckage_fullfiled_date")
     */
    protected $purtelBluckageFullfiledDate;

    /**
     * @ORM\Column(name="purtel_bluckage_sended_date")
     */
    protected $purtelBluckageSendedDate;

    /**
     * @ORM\Column(name="purtel_customer_reg_date")
     */
    protected $purtelCustomerRegDate;

    /**
     * @ORM\Column(name="purtel_data_record_id")
     */
    protected $purtelDataRecordId;

    /**
     * @ORM\Column(name="purtel_edit_done")
     */
    protected $purtelEditDone;

    /**
     * @ORM\Column(name="purtel_edit_done_from")
     */
    protected $purtelEditDoneFrom;

    /**
     * @ORM\Column(name="purtel_login")
     */
    protected $purtelLogin;

    /**
     * @ORM\Column(name="purtel_passwort")
     */
    protected $purtelPasswort;

    /**
     * @ORM\Column(name="purtel_product")
     */
    protected $purtelProduct;

    /**
     * @ORM\Column(name="purtelproduct_advanced")
     */
    protected $purtelproductAdvanced;

    /**
     * @ORM\Column(name="rDNS_count")
     */
    protected $rDNSCount;

    /**
     * @ORM\Column(name="rDNS_installation")
     */
    protected $rDNSInstallation;

    /**
     * @ORM\Column(name="reached_downstream")
     */
    protected $reachedDownstream;

    /**
     * @ORM\Column(name="reached_upstream")
     */
    protected $reachedUpstream;

    /**
     * @ORM\Column(name="recommended_product")
     */
    protected $recommendedProduct;

    /**
     * @ORM\Column(name="recruited_by")
     */
    protected $recruitedBy;

    /**
     * @ORM\Column(name="reg_answer_date")
     */
    protected $regAnswerDate;

    /**
     * @ORM\Column(type="date", name="registration_date")
     */
    protected $registrationDate;

    /**
     * @ORM\Column(type="date", name="contract_online_date");
     */
    protected $contractOnlineDate;

    /**
     * @ORM\Column(name="remote_login")
     */
    protected $remoteLogin;

    /**
     * @ORM\Column(name="remote_password")
     */
    protected $remotePassword;

    /**
     * @ORM\Column(name="routing_active")
     */
    protected $routingActive;

    /**
     * @ORM\Column(name="routing_active_date")
     */
    protected $routingActiveDate;

    /**
     * @ORM\Column(name="routing_deleted")
     */
    protected $routingDeleted;

    /**
     * @ORM\Column(name="routing_wish_date")
     */
    protected $routingWishDate;

    /**
     * @ORM\Column(name="second_tal")
     */
    protected $secondTal;

    /**
     * @ORM\Column(name="Service")
     */
    protected $Service;

    /**
     * @ORM\Column(name="service_level")
     */
    protected $serviceLevel;

    /**
     * @ORM\Column(name="service_technician")
     */
    protected $serviceTechnician;

    /**
     * @ORM\Column(name="sip_accounts")
     */
    protected $sipAccounts;

    /**
     * @ORM\Column(name="sofortdsl")
     */
    protected $sofortdsl;

    /**
     * @ORM\Column(name="special_conditions_from")
     */
    protected $specialConditionsFrom;

    /**
     * @ORM\Column(name="special_conditions_text")
     */
    protected $specialConditionsText;

    /**
     * @ORM\Column(name="special_conditions_till")
     */
    protected $specialConditionsTill;

    /**
     * @ORM\Column(name="wisocontract_cancel_input_date")
     */
    protected $wisocontractCancelInputDate;

    /**
     * @ORM\Column(name="wisocontract_cancel_input_ack")
     */
    protected $wisocontractCancelInputAck;

    /**
     * @ORM\Column(name="wisocontract_cancel_account_date")
     */
    protected $wisocontractCancelAccountDate;
    /**
     * @ORM\Column(name="wisocontract_cancel_account_finish")
     */
    protected $wisocontractCancelAccountFinish;

    /**
     * @ORM\Column(name="special_termination_internet")
     */
    protected $specialTerminationInternet;

    /**
     * @ORM\Column(name="special_termination_phone")
     */
    protected $specialTerminationPhone;

    /**
     * @ORM\Column(name="Spectrumprofile")
     */
    protected $Spectrumprofile;

    /**
     * @ORM\Column(name="stati_oldcontract_comment")
     */
    protected $statiOldcontractComment;

    /**
     * @ORM\Column(name="stati_port_confirm_date")
     */
    protected $statiPortConfirmDate;

    /**
     * @ORM\Column(name="status")
     */
    protected $status;

    /**
     * @ORM\Column(name="stnz_fb")
     */
    protected $stnzFb;

    /**
     * @ORM\Column(name="street")
     */
    protected $street;

    /**
     * @ORM\Column(name="streetno")
     */
    protected $streetno;

    /**
     * @ORM\Column(name="subnetmask")
     */
    protected $subnetmask;

    /**
     * @ORM\Column(name="tal_assigned_phoneno")
     */
    protected $talAssignedPhoneno;

    /**
     * @ORM\Column(name="tal_cancel_ack_date")
     */
    protected $talCancelAckDate;

    /**
     * @ORM\Column(name="tal_cancel_date")
     */
    protected $talCancelDate;

    /**
     * @ORM\Column(name="tal_canceled_for_date")
     */
    protected $talCanceledForDate;

    /**
     * @ORM\Column(name="tal_canceled_from")
     */
    protected $talCanceledFrom;

    /**
     * @ORM\Column(name="tal_dtag_assignment_no")
     */
    protected $talDtagAssignmentNo;

    /**
     * @ORM\Column(name="tal_dtag_revisor")
     */
    protected $talDtagRevisor;

    /**
     * @ORM\Column(name="tal_lended_fritzbox")
     */
    protected $talLendedFritzbox;

    /**
     * @ORM\Column(name="tal_order")
     */
    protected $talOrder;

    /**
     * @ORM\Column(name="tal_order_ack_date")
     */
    protected $talOrderAckDate;

    /**
     * @ORM\Column(name="tal_order_date")
     */
    protected $talOrderDate;

    /**
     * @ORM\Column(name="tal_order_work")
     */
    protected $talOrderWork;

    /**
     * @ORM\Column(name="tal_orderd_from")
     */
    protected $talOrderdFrom;

    /**
     * @ORM\Column(name="tal_ordered_for_date")
     */
    protected $talOrderedForDate;

    /**
     * @ORM\Column(name="tal_product")
     */
    protected $talProduct;

    /**
     * @ORM\Column(name="tal_releasing_operator")
     */
    protected $talReleasingOperator;

    /**
     * @ORM\Column(name="talorder")
     */
    protected $talorder;

    /**
     * @ORM\Column(name="talorder_text")
     */
    protected $talorderText;

    /**
     * @ORM\Column(name="techdata_comment")
     */
    protected $techdataComment;

    /**
     * @ORM\Column(name="techdata_status")
     */
    protected $techdataStatus;

    /**
     * @ORM\Column(name="telefonbuch_eintrag")
     */
    protected $telefonbuchEintrag;

    /**
     * @ORM\Column(name="terminal_ready")
     */
    protected $terminalReady;

    /**
     * @ORM\Column(name="terminal_ready_from")
     */
    protected $terminalReadyFrom;

    /**
     * @ORM\Column(name="terminal_sended_date")
     */
    protected $terminalSendedDate;

    /**
     * @ORM\Column(name="terminal_sended_from")
     */
    protected $terminalSendedFrom;

    /**
     * @ORM\Column(name="terminal_serialno")
     */
    protected $terminalSerialno;

    /**
     * @ORM\Column(name="terminal_type")
     */
    protected $terminalType;

    /**
     * @ORM\Column(name="terminal_type_exists")
     */
    protected $terminalTypeExists;

    /**
     * @ORM\Column(name="title")
     */
    protected $title;

    /**
     * @ORM\Column(name="tv_service")
     */
    protected $tvService;

    /**
     * @ORM\Column(name="tvs")
     */
    protected $tvs;

    /**
     * @ORM\Column(name="tvs_date")
     */
    protected $tvsDate;

    /**
     * @ORM\Column(name="tvs_from")
     */
    protected $tvsFrom;

    /**
     * @ORM\Column(name="tvs_to")
     */
    protected $tvsTo;

    /**
     * @ORM\Column(name="ventelo_confirmation_date")
     */
    protected $venteloConfirmationDate;

    /**
     * @ORM\Column(name="ventelo_port_letter_date")
     */
    protected $venteloPortLetterDate;

    /**
     * @ORM\Column(name="ventelo_port_letter_from")
     */
    protected $venteloPortLetterFrom;

    /**
     * @ORM\Column(name="ventelo_port_letter_how")
     */
    protected $venteloPortLetterHow;

    /**
     * @ORM\Column(name="ventelo_port_wish_date")
     */
    protected $venteloPortWishDate;

    /**
     * @ORM\Column(name="ventelo_purtel_enter_date")
     */
    protected $venteloPurtelEnterDate;

    /**
     * @ORM\Column(name="ventelo_purtel_enter_from")
     */
    protected $venteloPurtelEnterFrom;

    /**
     * @ORM\Column(name="version")
     */
    protected $version;

    /**
     * @ORM\Column(name="vlan_ID")
     */
    protected $vlanId;

    /**
     * @ORM\Column(name="wisocontract_canceled_date")
     */
    protected $wisocontractCanceledDate;

    /**
     * @ORM\Column(name="wisotelno")
     */
    protected $wisotelno;

    /**
     * @ORM\Column(name="zipcode")
     */
    protected $zipcode;

    /**
     * @ORM\Column(name="wisocontract_cancel_extra")
     */
    protected $wisocontractCancelExtra;


    /**
     * @ORM\Column(name="wisocontract_cancel_date")
     */
    protected $wisocontractCancelDate;


    /**
     * @ORM\Column(name="wisocontract_cancel_ack")
     */
    protected $wisocontractCancelAck;


    /**
     * @ORM\Column(name="wisocontract_rehire")
     */
    protected $wisocontractRehire;


    /**
     * @ORM\Column(name="wisocontract_rehire_person")
     */
    protected $wisocontractRehirePerson;


    /**
     * @ORM\Column(name="wisocontract_canceled_from")
     */
    protected $wisocontractCanceledFrom;


    /**
     * @ORM\Column(name="wisocontract_move")
     */
    protected $wisocontractMove;


    /**
     * @ORM\Column(name="cancel_comment")
     */
    protected $cancelComment;


    /**
     * @ORM\Column(name="new_city")
     */
    protected $newCity;


    /**
     * @ORM\Column(name="new_zipcode")
     */
    protected $newZipcode;


    /**
     * @ORM\Column(name="new_street")
     */
    protected $newStreet;


    /**
     * @ORM\Column(name="new_streetno")
     */
    protected $newStreetno;


    /**
     * @ORM\Column(name="get_hardware_date")
     */
    protected $getHardwareDate;


    /**
     * @ORM\Column(name="pay_hardware_date")
     */
    protected $payHardwareDate;


    /**
     * @ORM\Column(name="move_registration_ack")
     */
    protected $moveRegistrationAck;


    /**
     * @ORM\Column(name="wisocontract_switchoff")
     */
    protected $wisocontractSwitchoff;


    /**
     * @ORM\Column(name="wisocontract_switchoff_finish")
     */
    protected $wisocontractSwitchoffFinish;


    /**
     * @ORM\Column(name="service_level_send")
     */
    protected $serviceLevelSend;

    /**
     * @ORM\Column(name="cudaid")
     */
    protected $cudaid;


    /**
     * @ORM\Column(name="wbci_id")
     */
    protected $wbciId;


    /**
     * @ORM\Column(name="wbci_ekpabg")
     */
    protected $wbciEkpabg;


    /**
     * @ORM\Column(name="wbci_gf")
     */
    protected $wbciGf;


    /**
     * @ORM\Column(name="wbci_eingestellt_am")
     */
    protected $wbciEingestelltAm;


    /**
     * @ORM\Column(name="wbci_eingestellt_von")
     */
    protected $wbciEingestelltVon;


    /**
     * @ORM\Column(name="wbci_ruemva_am")
     */
    protected $wbciRuemvaAm;


    /**
     * @ORM\Column(name="wbci_ruemva_von")
     */
    protected $wbciRuemvaVon;


    /**
     * @ORM\Column(name="wbci_bestaetigt_am")
     */
    protected $wbciBestaetigtAm;


    /**
     * @ORM\Column(name="wbci_bestaetigt_von")
     */
    protected $wbciBestaetigtVon;


    /**
     * @ORM\Column(name="wbci_akmtr")
     */
    protected $wbciAkmtr;


    /**
     * @ORM\Column(name="wbci_akmtr_am")
     */
    protected $wbciAkmtrAm;


    /**
     * @ORM\Column(name="wbci_akmtr_von")
     */
    protected $wbciAkmtrVon;


    /**
     * @ORM\Column(name="wbci_comment")
     */
    protected $wbciComment;


    /**
     * @ORM\Column(name="exp_done_phone")
     */
    protected $expDonePhone;


    /**
     * @ORM\Column(name="exp_done_int")
     */
    protected $expDoneInt;


    /**
     * @ORM\Column(name="tech_free_for_date")
     */
    protected $techFreeForDate;


    /**
     * @ORM\Column(name="tech_free_date")
     */
    protected $techFreeDate;


    /**
     * @ORM\Column(name="purtel_telnr")
     */
    protected $purtelTelnr;


    /**
     * @ORM\Column(name="purtel_use_version")
     */
    protected $purtelUseVersion;


    /**
     * @ORM\Column(name="purtel_login_1")
     */
    protected $purtelLogin1;


    /**
     * @ORM\Column(name="purtel_passwort_1")
     */
    protected $purtelPasswort1;


    /**
     * @ORM\Column(name="purtel_telnr_1")
     */
    protected $purtelTelnr1;


    /**
     * @ORM\Column(name="purtel_login_2")
     */
    protected $purtelLogin2;


    /**
     * @ORM\Column(name="purtel_passwort_2")
     */
    protected $purtelPasswort2;


    /**
     * @ORM\Column(name="purtel_telnr_2")
     */
    protected $purtelTelnr2;


    /**
     * @ORM\Column(name="purtel_login_3")
     */
    protected $purtelLogin3;


    /**
     * @ORM\Column(name="purtel_passwort_3")
     */
    protected $purtelPasswort3;


    /**
     * @ORM\Column(name="purtel_telnr_3")
     */
    protected $purtelTelnr3;


    /**
     * @ORM\Column(name="purtel_delete_date")
     */
    protected $purtelDeleteDate;


    /**
     * @ORM\Column(name="acs_id")
     */
    protected $acsId;


    /**
     * @ORM\Column(name="tag")
     */
    protected $tag;


    /**
     * @ORM\Column(name="cancel_purtel_date")
     */
    protected $cancelPurtelDate;


    /**
     * @ORM\Column(name="cancel_purtel_from")
     */
    protected $cancelPurtelFrom;


    /**
     * @ORM\Column(name="cancel_tel_date")
     */
    protected $cancelTelDate;


    /**
     * @ORM\Column(name="cancel_tel_for_date")
     */
    protected $cancelTelForDate;


    /**
     * @ORM\Column(name="cancel_tel_from")
     */
    protected $cancelTelFrom;


    /**
     * @ORM\Column(name="cancel_product")
     */
    protected $cancelProduct;


    /**
     * @ORM\Column(name="cancel_product_from")
     */
    protected $cancelProductFrom;


    /**
     * @ORM\Column(name="bank_account_mandantenid")
     */
    protected $bankAccountMandantenid;


    /**
     * @ORM\Column(name="bank_account_debitor")
     */
    protected $bankAccountDebitor;


    /**
     * @ORM\Column(name="voucher")
     */
    protected $voucher;


    /**
     * @ORM\Column(name="vectoring")
     */
    protected $vectoring;


    /**
     * @ORM\Column(name="pppoe_pin")
     */
    protected $pppoePin;

    /**
     * @ORM\Column(name="commissioning_from")
     */
    protected $commissioningFrom;

    /**
     * @ORM\Column(name="commissioning_wish_date")
     */
    protected $commissioningWishDate;

    /**
     * @ORM\Column(name="commissioning_date")
     */
    protected $commissioningDate;

    /**
     * @ORM\Column(name="inkasso_bankgebuehren")
     */
    protected $inkassoBankgebuehren;

    /**
     * @ORM\Column(name="inkasso_mahngebuehren")
     */
    protected $inkassoMahngebuehren;

    /**
     * @ORM\Column(name="inkasso_datum_letzte_mahnung")
     */
    protected $inkassoDatumLetzteMahnung;

    /**
     * @ORM\Column(name="inkasso_datum_ruecklastschrift")
     */
    protected $inkassoDatumRuecklastschrift;

    /**
     * @ORM\Column(name="inkasso_grund_ruecklastschrift")
     */
    protected $inkassoGrundRuecklastschrift;

    /**
     * @ORM\Column(name="inkasso_hauptforderung")
     */
    protected $inkassoHauptforderung;

    /**
     * @ORM\Column(name="inkasso_rechnung")
     */
    protected $inkassoRechnung;

    /**
     * @ORM\Column(name="override_inherited_vlan_profiles")
     */
    protected $overrideInheritedVlanProfiles = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->crossSellingProducts = new ArrayCollection();
        $this->vlanProfiles = new ArrayCollection();
        $this->password = PppoePasswordGenerator::generatePassword();
    }

    /**
     * Return effective vlan profiles for this customer or card or location
     * 
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getEffectiveVlanProfiles()
    {
        // if overrideInheritedVlanProfiles is true, only return customers vlan profiles
        if ($this->overrideInheritedVlanProfiles) {
            return new ArrayCollection(
                array_map(function (CustomerVlanProfile $CustomerVlanProfile) {
                    return $CustomerVlanProfile->getVlanProfile();
                }, $this->vlanProfiles->toArray())
            );
        }

        // combine inheritedVlanProfiles and customerVlanProfiles

        $inheritedVlanProfiles = new ArrayCollection();

        if ($this->cardId instanceof \AppBundle\Entity\Location\Card) {
            $vlanProfiles = $this->cardId->getCustomerDefaultVlanProfiles();

            if (!$vlanProfiles->isEmpty()) {
                // use customer default vlan profiles from card
                $inheritedVlanProfiles = $vlanProfiles;
            } else {
                $location = $this->cardId->getLocation();

                if ($location instanceof \AppBundle\Entity\Location\Location) {
                    // use customer default vlan profiles from location - or empty ArrayCollection
                    $inheritedVlanProfiles = $this->cardId->getLocation()->getCustomerDefaultVlanProfiles();
                }
            }
        }

        $effectiveVlanProfiles = [];

        foreach ($inheritedVlanProfiles as $vlanProfile) {
            $effectiveVlanProfiles[$vlanProfile->getId()] = $vlanProfile;
        }

        foreach ($this->vlanProfiles as $customerVlanProfile) {
            $vlanProfile = $customerVlanProfile->getVlanProfile();
            $effectiveVlanProfiles[$vlanProfile->getId()] = $vlanProfile;
        }

        return new ArrayCollection($effectiveVlanProfiles);
    }

    /**
     * Create Customer object from customer data array (which comes from select sql query)
     * 
     * @param array $array
     * @param Doctrine\Bundle\DoctrineBundle\Registry $doctrine
     * 
     * @return Customer
     */
    public static function createFromArray($array = [], Registry $doctrine) : Customer
    {
        $customer = new self();
        $customerMediator = new \AppBundle\Util\CustomerMediator($customer, $doctrine);
        $customerMethods = get_class_methods(self::class);

        if (isset($array['id'])) {
            $customerId = & \Closure::bind(function & () {
                return $this->id;
            }, $customer, $customer)->__invoke();

            $customerId = $array['id'];

            unset($array['id']);
        }

        $mapNames = [
            'clientid' => 'clientId',
            'customer_id' => 'externalCustomerId',
            'clienttype' => 'clientType',
            'oldcontract_active' => 'oldContractActive',
            'talorder' => 'talOrder',
            'external_id_2' => 'externalId2',
            'vlan_ID' => 'vlanId',
        ];

        foreach ($array as $property => $value) {
            if (isset($mapNames[$property])) {
                $property = $mapNames[$property];
            }

            // if property is written in snake-case, convert to camelCase
            if (false !== strpos($property, '_')) {
                $matches = null;

                if (preg_match_all('/(_.)/', $property, $matches) > 0) {
                    foreach ($matches[0] as $match) {
                        $property = preg_replace('/'.$match.'/', strtoupper(substr($match, 1)), $property, 1);
                    }
                }
            }

            $methodName = 'set'.ucfirst($property);

            if (!in_array($methodName, $customerMethods)) {
                throw new \BadMethodCallException(sprintf('Unexpected property %s in values', $property));
            }

            $customerMediator->$methodName($value);
        }

        return $customerMediator->getMediationParty();
    }

    /**
     * Add vlanProfile.
     *
     * @param CustomerVlanProfile $vlanProfile
     *
     * @return Customer
     */
    public function addVlanProfile(CustomerVlanProfile $vlanProfile)
    {
        $this->vlanProfiles[] = $vlanProfile;

        return $this;
    }

    /**
     * Remove vlanProfile.
     *
     * @param CustomerVlanProfile $vlanProfile
     *
     * @return Customer
     */
    public function removeVlanProfile(CustomerVlanProfile $vlanProfile)
    {
        $this->vlanProfiles->remove($vlanProfile);

        return $this;
    }

    /**
     * Get vlanProfiles.
     *
     * @return ArrayCollection
     */
    public function getVlanProfiles()
    {
        return $this->vlanProfiles;
    }

    /**
     * Set vlanProfiles.
     *
     * @param array|ArrayCollection $vlanProfiles
     * 
     * @return Customer
     */
    public function setVlanProfiles($vlanProfiles)
    {
        if ($vlanProfiles instanceof ArrayCollection) {
            $this->vlanProfiles = $vlanProfiles;
        } else {
            $this->vlanProfiles = new ArrayCollection($vlanProfiles);
        }

        return $this;
    }

    /**
     * Set technical data
     * 
     * @param \AppBundle\Entity\Customer\TechnicalData $technicalData
     * 
     * @return Customer
     */
    public function setTechnicalData(TechnicalData $technicalData)
    {
        $this->technicalData = $technicalData;

        return $this;
    }

    /**
     * Get technical data
     * 
     * @return \AppBundle\Entity\Customer\TechnicalData
     */
    public function getTechnicalData()
    {
        return $this->technicalData;
    }

    /**
     * Set crossSellingProducts.
     *
     * @param array $crossSellingProducts
     *
     * @return Customer
     */
    public function setCrossSellingProducts($crossSellingProducts)
    {
        $this->crossSellingProducts = $crossSellingProducts;

        return $this;
    }

    /**
     * Get crossSellingProducts.
     *
     * @return array
     */
    public function getCrossSellingProducts()
    {
        return $this->crossSellingProducts;
    }

    /**
     * Set externalId2.
     *
     * @param string $externalId2
     *
     * @return Customer
     */
    public function setExternalId2($externalId2)
    {
        $this->externalId2 = $externalId2;

        return $this;
    }

    /**
     * Get externalId2.
     *
     * @return string
     */
    public function getExternalId2()
    {
        return $this->externalId2;
    }

    /**
     * Set oldcontractPorting.
     *
     * @param string $oldcontractPorting
     *
     * @return Customer
     */
    public function setOldcontractPorting($oldcontractPorting)
    {
        $this->oldcontractPorting = $oldcontractPorting;

        return $this;
    }

    /**
     * Get oldcontractPorting.
     *
     * @return string
     */
    public function getOldcontractPorting()
    {
        return $this->oldcontractPorting;
    }

    /**
     * Set connectionWishDate.
     *
     * @param string $connectionWishDate
     *
     * @return Customer
     */
    public function setConnectionWishDate($connectionWishDate)
    {
        $this->connectionWishDate = $connectionWishDate;

        return $this;
    }

    /**
     * Get connectionWishDate.
     *
     * @return string
     */
    public function getConnectionWishDate()
    {
        return $this->connectionWishDate;
    }

    /**
     * Set bankAccountEmailaddress.
     *
     * @param string $bankAccountEmailaddress
     *
     * @return Customer
     */
    public function setBankAccountEmailaddress($bankAccountEmailaddress)
    {
        $this->bankAccountEmailaddress = $bankAccountEmailaddress;

        return $this;
    }

    /**
     * Get bankAccountEmailaddress.
     *
     * @return string
     */
    public function getBankAccountEmailaddress()
    {
        return $this->bankAccountEmailaddress;
    }

    /**
     * Set bankAccountCity
     * 
     * @param string $bankAccountCity
     * 
     * @return Customer
     */
    public function setBankAccountCity($bankAccountCity)
    {
        $this->bankAccountCity = $bankAccountCity;

        return $this;
    }

    /**
     * Get bankAccountCity
     * 
     * @return string
     */
    public function getBankAccountCity()
    {
        return $this->bankAccountCity;
    }

    /**
     * @param string $bankAccountStreet
     *
     * @return Customer
     */
    public function setBankAccountStreet($bankAccountStreet)
    {
        $this->bankAccountStreet = $bankAccountStreet;

        return $this;
    }

    /**
     * @return string
     */
    public function getBankAccountStreet()
    {
        return $this->bankAccountStreet;
    }

    /**
     * @param string bankAccountStreetno
     *
     * @return Customer
     */
    public function setBankAccountStreetno($bankAccountStreetno)
    {
        $this->bankAccountStreetno = $bankAccountStreetno;

        return $this;
    }

    /**
     * @return string
     */
    public function getBankAccountStreetno()
    {
        return $this->bankAccountStreetno;
    }

    /**
     * @param string bankAccountZipcode
     *
     * @return Customer
     */
    public function setBankAccountZipcode($bankAccountZipcode)
    {
        $this->bankAccountZipcode = $bankAccountZipcode;

        return $this;
    }

    /**
     * @return string
     */
    public function getBankAccountZipcode()
    {
        return $this->bankAccountZipcode;
    }

    /**
     * Set externalCustomerId.
     *
     * @param string $externalCustomerId
     *
     * @return Customer
     */
    public function setExternalCustomerId($externalCustomerId)
    {
        $this->externalCustomerId = $externalCustomerId;

        return $this;
    }

    /**
     * Get externalCustomerId.
     *
     * @return string
     */
    public function getExternalCustomerId()
    {
        return $this->externalCustomerId;
    }

    /**
     * Set customerElectrisity
     * 
     * @param string $customerElectrisity;
     * 
     * @return Customer
     */
    public function setCustomerElectrisity($customerElectrisity)
    {
        $this->customerElectrisity = $customerElectrisity;

        return $this;
    }

    /**
     * Get customerElectrisity
     * 
     * @return string
     */
    public function getCustomerElectrisity()
    {
        return $this->customerElectrisity;
    }

    /**
     * Set customerElectrisityEnddate
     * 
     * @param string $customerElectrisityEnddate;
     * 
     * @return Customer
     */
    public function setCustomerElectrisityEnddate($customerElectrisityEnddate)
    {
        $this->customerElectrisityEnddate = $customerElectrisityEnddate;

        return $this;
    }

    /**
     * Get customerElectrisityEnddate
     * 
     * @return string
     */
    public function getCustomerElectrisityEnddate()
    {
        return $this->customerElectrisityEnddate;
    }

    /**
     * Set addChargeTerminal.
     *
     * @param string $addChargeTerminal
     *
     * @return Customer
     */
    public function setAddChargeTerminal($addChargeTerminal)
    {
        $this->addChargeTerminal = $addChargeTerminal;

        return $this;
    }

    /**
     * Get addChargeTerminal.
     *
     * @return string
     */
    public function getAddChargeTerminal()
    {
        return $this->addChargeTerminal;
    }

    /**
     * Set addCustomerOnFb.
     *
     * @param string $addCustomerOnFb
     *
     * @return Customer
     */
    public function setAddCustomerOnFb($addCustomerOnFb)
    {
        $this->addCustomerOnFb = $addCustomerOnFb;

        return $this;
    }

    /**
     * Get addCustomerOnFb.
     *
     * @return string
     */
    public function getAddCustomerOnFb()
    {
        return $this->addCustomerOnFb;
    }

    /**
     * Set addIpAddress.
     *
     * @param string $addIpAddress
     *
     * @return Customer
     */
    public function setAddIpAddress($addIpAddress)
    {
        $this->addIpAddress = $addIpAddress;

        return $this;
    }

    /**
     * Get addIpAddress.
     *
     * @return string
     */
    public function getAddIpAddress()
    {
        return $this->addIpAddress;
    }

    /**
     * Set addIpAddressCost.
     *
     * @param string $addIpAddressCost
     *
     * @return Customer
     */
    public function setAddIpAddressCost($addIpAddressCost)
    {
        $this->addIpAddressCost = $addIpAddressCost;

        return $this;
    }

    /**
     * Get addIpAddressCost.
     *
     * @return string
     */
    public function getAddIpAddressCost()
    {
        return $this->addIpAddressCost;
    }

    /**
     * Set addPhoneLines.
     *
     * @param string $addPhoneLines
     *
     * @return Customer
     */
    public function setAddPhoneLines($addPhoneLines)
    {
        $this->addPhoneLines = $addPhoneLines;

        return $this;
    }

    /**
     * Get addPhoneLines.
     *
     * @return string
     */
    public function getAddPhoneLines()
    {
        return $this->addPhoneLines;
    }

    /**
     * Set addPhoneLinesCost.
     *
     * @param string $addPhoneLinesCost
     *
     * @return Customer
     */
    public function setAddPhoneLinesCost($addPhoneLinesCost)
    {
        $this->addPhoneLinesCost = $addPhoneLinesCost;

        return $this;
    }

    /**
     * Get addPhoneLinesCost.
     *
     * @return string
     */
    public function getAddPhoneLinesCost()
    {
        return $this->addPhoneLinesCost;
    }

    /**
     * Set addPhoneNos.
     *
     * @param string $addPhoneNos
     *
     * @return Customer
     */
    public function setAddPhoneNos($addPhoneNos)
    {
        $this->addPhoneNos = $addPhoneNos;

        return $this;
    }

    /**
     * Get addPhoneNos.
     *
     * @return string
     */
    public function getAddPhoneNos()
    {
        return $this->addPhoneNos;
    }

    /**
     * Set addPort.
     *
     * @param string $addPort
     *
     * @return Customer
     */
    public function setAddPort($addPort)
    {
        $this->addPort = $addPort;

        return $this;
    }

    /**
     * Get addPort.
     *
     * @return string
     */
    public function getAddPort()
    {
        return $this->addPort;
    }

    /**
     * Set addSatellite.
     *
     * @param string $addSatellite
     *
     * @return Customer
     */
    public function setAddSatellite($addSatellite)
    {
        $this->addSatellite = $addSatellite;

        return $this;
    }

    /**
     * Get addSatellite.
     *
     * @return string
     */
    public function getAddSatellite()
    {
        return $this->addSatellite;
    }

    /**
     * Set application.
     *
     * @param string $application
     *
     * @return Customer
     */
    public function setApplication($application)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application.
     *
     * @return string
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set applicationText.
     *
     * @param string $applicationText
     *
     * @return Customer
     */
    public function setApplicationText($applicationText)
    {
        $this->applicationText = $applicationText;

        return $this;
    }

    /**
     * Get applicationText.
     *
     * @return string
     */
    public function getApplicationText()
    {
        return $this->applicationText;
    }

    /**
     * Set bankAccountBankname.
     *
     * @param string $bankAccountBankname
     *
     * @return Customer
     */
    public function setBankAccountBankname($bankAccountBankname)
    {
        $this->bankAccountBankname = $bankAccountBankname;

        return $this;
    }

    /**
     * Get bankAccountBankname.
     *
     * @return string
     */
    public function getBankAccountBankname()
    {
        return $this->bankAccountBankname;
    }

    /**
     * Set bankAccountBic.
     *
     * @param string $bankAccountBic
     *
     * @return Customer
     */
    public function setBankAccountBic($bankAccountBic)
    {
        $this->bankAccountBic = $bankAccountBic;

        return $this;
    }

    /**
     * Get bankAccountBic.
     *
     * @return string
     */
    public function getBankAccountBic()
    {
        return $this->bankAccountBic;
    }

    /**
     * Set bankAccountBicNew.
     *
     * @param string $bankAccountBicNew
     *
     * @return Customer
     */
    public function setBankAccountBicNew($bankAccountBicNew)
    {
        $this->bankAccountBicNew = $bankAccountBicNew;

        return $this;
    }

    /**
     * Get bankAccountBicNew.
     *
     * @return string
     */
    public function getBankAccountBicNew()
    {
        return $this->bankAccountBicNew;
    }

    /**
     * Set bankAccountHolderFirstname.
     *
     * @param string $bankAccountHolderFirstname
     *
     * @return Customer
     */
    public function setBankAccountHolderFirstname($bankAccountHolderFirstname)
    {
        $this->bankAccountHolderFirstname = $bankAccountHolderFirstname;

        return $this;
    }

    /**
     * Get bankAccountHolderFirstname.
     *
     * @return string
     */
    public function getBankAccountHolderFirstname()
    {
        return $this->bankAccountHolderFirstname;
    }

    /**
     * Set bankAccountHolderLastname.
     *
     * @param string $bankAccountHolderLastname
     *
     * @return Customer
     */
    public function setBankAccountHolderLastname($bankAccountHolderLastname)
    {
        $this->bankAccountHolderLastname = $bankAccountHolderLastname;

        return $this;
    }

    /**
     * Get bankAccountHolderLastname.
     *
     * @return string
     */
    public function getBankAccountHolderLastname()
    {
        return $this->bankAccountHolderLastname;
    }

    /**
     * Set bankAccountIban.
     *
     * @param string $bankAccountIban
     *
     * @return Customer
     */
    public function setBankAccountIban($bankAccountIban)
    {
        $this->bankAccountIban = $bankAccountIban;

        return $this;
    }

    /**
     * Get bankAccountIban.
     *
     * @return string
     */
    public function getBankAccountIban()
    {
        return $this->bankAccountIban;
    }

    /**
     * Set bankAccountNumber.
     *
     * @param string $bankAccountNumber
     *
     * @return Customer
     */
    public function setBankAccountNumber($bankAccountNumber)
    {
        $this->bankAccountNumber = $bankAccountNumber;

        return $this;
    }

    /**
     * Get bankAccountNumber.
     *
     * @return string
     */
    public function getBankAccountNumber()
    {
        return $this->bankAccountNumber;
    }

    /**
     * Set birthday.
     *
     * @param string $birthday
     *
     * @return Customer
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday.
     *
     * @return string
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set callDataRecord.
     *
     * @param string $callDataRecord
     *
     * @return Customer
     */
    public function setCallDataRecord($callDataRecord)
    {
        $this->callDataRecord = $callDataRecord;

        return $this;
    }

    /**
     * Get callDataRecord.
     *
     * @return string
     */
    public function getCallDataRecord()
    {
        return $this->callDataRecord;
    }

    /**
     * Set cancelPurtel.
     *
     * @param string $cancelPurtel
     *
     * @return Customer
     */
    public function setCancelPurtel($cancelPurtel)
    {
        $this->cancelPurtel = $cancelPurtel;

        return $this;
    }

    /**
     * Get cancelPurtel.
     *
     * @return string
     */
    public function getCancelPurtel()
    {
        return $this->cancelPurtel;
    }

    /**
     * Set cancelPurtelForDate.
     *
     * @param string $cancelPurtelForDate
     *
     * @return Customer
     */
    public function setCancelPurtelForDate($cancelPurtelForDate)
    {
        $this->cancelPurtelForDate = $cancelPurtelForDate;

        return $this;
    }

    /**
     * Get cancelPurtelForDate.
     *
     * @return string
     */
    public function getCancelPurtelForDate()
    {
        return $this->cancelPurtelForDate;
    }

    /**
     * Set cancelTal.
     *
     * @param string $cancelTal
     *
     * @return Customer
     */
    public function setCancelTal($cancelTal)
    {
        $this->cancelTal = $cancelTal;

        return $this;
    }

    /**
     * Get cancelTal.
     *
     * @return string
     */
    public function getCancelTal()
    {
        return $this->cancelTal;
    }

    /**
     * Set cardId.
     *
     * @param string $cardId
     *
     * @return Customer
     */
    public function setCardId($cardId)
    {
        $this->cardId = $cardId;

        return $this;
    }

    /**
     * Get cardId.
     *
     * @return string
     */
    public function getCardId()
    {
        return $this->cardId;
    }

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return Customer
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set clientId.
     *
     * @param string $clientId
     *
     * @return Customer
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * Get clientId.
     *
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Set clientType.
     *
     * @param string $clientType
     *
     * @return Customer
     */
    public function setClientType($clientType)
    {
        $this->clientType = $clientType;

        return $this;
    }

    /**
     * Get clientType.
     *
     * @return string
     */
    public function getClientType()
    {
        return $this->clientType;
    }

    /**
     * Set comment.
     *
     * @param string $comment
     *
     * @return Customer
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set company.
     *
     * @param string $company
     *
     * @return Customer
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set connectType.
     *
     * @param string $connectType
     *
     * @return Customer
     */
    public function setConnectType($connectType)
    {
        $this->connectType = $connectType;

        return $this;
    }

    /**
     * Get connectType.
     *
     * @return string
     */
    public function getConnectType()
    {
        return $this->connectType;
    }

    /**
     * Set connectionActivationDate.
     *
     * @param string $connectionActivationDate
     *
     * @return Customer
     */
    public function setConnectionActivationDate($connectionActivationDate)
    {
        $this->connectionActivationDate = $connectionActivationDate;

        return $this;
    }

    /**
     * Get connectionActivationDate.
     *
     * @return string
     */
    public function getConnectionActivationDate()
    {
        return $this->connectionActivationDate;
    }

    /**
     * Set connectionAddressEqual.
     *
     * @param string $connectionAddressEqual
     *
     * @return Customer
     */
    public function setConnectionAddressEqual($connectionAddressEqual)
    {
        $this->connectionAddressEqual = $connectionAddressEqual;

        return $this;
    }

    /**
     * Get connectionAddressEqual.
     *
     * @return string
     */
    public function getConnectionAddressEqual()
    {
        return $this->connectionAddressEqual;
    }

    /**
     * Set connectionCity.
     *
     * @param string $connectionCity
     *
     * @return Customer
     */
    public function setConnectionCity($connectionCity)
    {
        $this->connectionCity = $connectionCity;

        return $this;
    }

    /**
     * Get connectionCity.
     *
     * @return string
     */
    public function getConnectionCity()
    {
        return $this->connectionCity;
    }

    /**
     * Set connectionFeeHeight.
     *
     * @param string $connectionFeeHeight
     *
     * @return Customer
     */
    public function setConnectionFeeHeight($connectionFeeHeight)
    {
        $this->connectionFeeHeight = $connectionFeeHeight;

        return $this;
    }

    /**
     * Get connectionFeeHeight.
     *
     * @return string
     */
    public function getConnectionFeeHeight()
    {
        return $this->connectionFeeHeight;
    }

    /**
     * Set connectionFeePaid.
     *
     * @param string $connectionFeePaid
     *
     * @return Customer
     */
    public function setConnectionFeePaid($connectionFeePaid)
    {
        $this->connectionFeePaid = $connectionFeePaid;

        return $this;
    }

    /**
     * Get connectionFeePaid.
     *
     * @return string
     */
    public function getConnectionFeePaid()
    {
        return $this->connectionFeePaid;
    }

    /**
     * Set connectionInactiveDate.
     *
     * @param string $connectionInactiveDate
     *
     * @return Customer
     */
    public function setConnectionInactiveDate($connectionInactiveDate)
    {
        $this->connectionInactiveDate = $connectionInactiveDate;

        return $this;
    }

    /**
     * Get connectionInactiveDate.
     *
     * @return string
     */
    public function getConnectionInactiveDate()
    {
        return $this->connectionInactiveDate;
    }

    /**
     * Set connectionInstallation.
     *
     * @param string $connectionInstallation
     *
     * @return Customer
     */
    public function setConnectionInstallation($connectionInstallation)
    {
        $this->connectionInstallation = $connectionInstallation;

        return $this;
    }

    /**
     * Get connectionInstallation.
     *
     * @return string
     */
    public function getConnectionInstallation()
    {
        return $this->connectionInstallation;
    }

    /**
     * Set connectionStreet.
     *
     * @param string $connectionStreet
     *
     * @return Customer
     */
    public function setConnectionStreet($connectionStreet)
    {
        $this->connectionStreet = $connectionStreet;

        return $this;
    }

    /**
     * Get connectionStreet.
     *
     * @return string
     */
    public function getConnectionStreet()
    {
        return $this->connectionStreet;
    }

    /**
     * Set connectionStreetno.
     *
     * @param string $connectionStreetno
     *
     * @return Customer
     */
    public function setConnectionStreetno($connectionStreetno)
    {
        $this->connectionStreetno = $connectionStreetno;

        return $this;
    }

    /**
     * Get connectionStreetno.
     *
     * @return string
     */
    public function getConnectionStreetno()
    {
        return $this->connectionStreetno;
    }

    /**
     * Set connectionZipcode.
     *
     * @param string $connectionZipcode
     *
     * @return Customer
     */
    public function setConnectionZipcode($connectionZipcode)
    {
        $this->connectionZipcode = $connectionZipcode;

        return $this;
    }

    /**
     * Get connectionZipcode.
     *
     * @return string
     */
    public function getConnectionZipcode()
    {
        return $this->connectionZipcode;
    }

    /**
     * Set contract.
     *
     * @param string $contract
     *
     * @return Customer
     */
    public function setContract($contract)
    {
        $this->contract = $contract;

        return $this;
    }

    /**
     * Get contract.
     *
     * @return string
     */
    public function getContract()
    {
        return $this->contract;
    }

    /**
     * Set contractAcknowledge.
     *
     * @param string $contractAcknowledge
     *
     * @return Customer
     */
    public function setContractAcknowledge($contractAcknowledge)
    {
        $this->contractAcknowledge = $contractAcknowledge;

        return $this;
    }

    /**
     * Get contractAcknowledge.
     *
     * @return string
     */
    public function getContractAcknowledge()
    {
        return $this->contractAcknowledge;
    }

    /**
     * Set contractChangeTo.
     *
     * @param string $contractChangeTo
     *
     * @return Customer
     */
    public function setContractChangeTo($contractChangeTo)
    {
        $this->contractChangeTo = $contractChangeTo;

        return $this;
    }

    /**
     * Get contractChangeTo.
     *
     * @return string
     */
    public function getContractChangeTo()
    {
        return $this->contractChangeTo;
    }

    /**
     * Set contractComment.
     *
     * @param string $contractComment
     *
     * @return Customer
     */
    public function setContractComment($contractComment)
    {
        $this->contractComment = $contractComment;

        return $this;
    }

    /**
     * Get contractComment.
     *
     * @return string
     */
    public function getContractComment()
    {
        return $this->contractComment;
    }

    /**
     * Set contractId.
     *
     * @param string $contractId
     *
     * @return Customer
     */
    public function setContractId($contractId)
    {
        $this->contractId = $contractId;

        return $this;
    }

    /**
     * Get contractId.
     *
     * @return string
     */
    public function getContractId()
    {
        return $this->contractId;
    }

    /**
     * Set contractReceivedDate.
     *
     * @param string $contractReceivedDate
     *
     * @return Customer
     */
    public function setContractReceivedDate($contractReceivedDate)
    {
        $this->contractReceivedDate = $contractReceivedDate;

        return $this;
    }

    /**
     * Get contractReceivedDate.
     *
     * @return string
     */
    public function getContractReceivedDate()
    {
        return $this->contractReceivedDate;
    }

    /**
     * Set contractSentDate.
     *
     * @param string $contractSentDate
     *
     * @return Customer
     */
    public function setContractSentDate($contractSentDate)
    {
        $this->contractSentDate = $contractSentDate;

        return $this;
    }

    /**
     * Get contractSentDate.
     *
     * @return string
     */
    public function getContractSentDate()
    {
        return $this->contractSentDate;
    }

    /**
     * Set contractText.
     *
     * @param string $contractText
     *
     * @return Customer
     */
    public function setContractText($contractText)
    {
        $this->contractText = $contractText;

        return $this;
    }

    /**
     * Get contractText.
     *
     * @return string
     */
    public function getContractText()
    {
        return $this->contractText;
    }

    /**
     * Set contractVersion.
     *
     * @param string $contractVersion
     *
     * @return Customer
     */
    public function setContractVersion($contractVersion)
    {
        $this->contractVersion = $contractVersion;

        return $this;
    }

    /**
     * Get contractVersion.
     *
     * @return string
     */
    public function getContractVersion()
    {
        return $this->contractVersion;
    }

    /**
     * Set creditRatingDate.
     *
     * @param string $creditRatingDate
     *
     * @return Customer
     */
    public function setCreditRatingDate($creditRatingDate)
    {
        $this->creditRatingDate = $creditRatingDate;

        return $this;
    }

    /**
     * Get creditRatingDate.
     *
     * @return string
     */
    public function getCreditRatingDate()
    {
        return $this->creditRatingDate;
    }

    /**
     * Set creditRatingLink.
     *
     * @param string $creditRatingLink
     *
     * @return Customer
     */
    public function setCreditRatingLink($creditRatingLink)
    {
        $this->creditRatingLink = $creditRatingLink;

        return $this;
    }

    /**
     * Get creditRatingLink.
     *
     * @return string
     */
    public function getCreditRatingLink()
    {
        return $this->creditRatingLink;
    }

    /**
     * Set creditRatingOk.
     *
     * @param string $creditRatingOk
     *
     * @return Customer
     */
    public function setCreditRatingOk($creditRatingOk)
    {
        $this->creditRatingOk = $creditRatingOk;

        return $this;
    }

    /**
     * Get creditRatingOk.
     *
     * @return string
     */
    public function getCreditRatingOk()
    {
        return $this->creditRatingOk;
    }

    /**
     * Set customerConnectInfoDate.
     *
     * @param string $customerConnectInfoDate
     *
     * @return Customer
     */
    public function setCustomerConnectInfoDate($customerConnectInfoDate)
    {
        $this->customerConnectInfoDate = $customerConnectInfoDate;

        return $this;
    }

    /**
     * Get customerConnectInfoDate.
     *
     * @return string
     */
    public function getCustomerConnectInfoDate()
    {
        return $this->customerConnectInfoDate;
    }

    /**
     * Set customerConnectInfoFrom.
     *
     * @param string $customerConnectInfoFrom
     *
     * @return Customer
     */
    public function setCustomerConnectInfoFrom($customerConnectInfoFrom)
    {
        $this->customerConnectInfoFrom = $customerConnectInfoFrom;

        return $this;
    }

    /**
     * Get customerConnectInfoFrom.
     *
     * @return string
     */
    public function getCustomerConnectInfoFrom()
    {
        return $this->customerConnectInfoFrom;
    }

    /**
     * Set customerConnectInfoHow.
     *
     * @param string $customerConnectInfoHow
     *
     * @return Customer
     */
    public function setCustomerConnectInfoHow($customerConnectInfoHow)
    {
        $this->customerConnectInfoHow = $customerConnectInfoHow;

        return $this;
    }

    /**
     * Get customerConnectInfoHow.
     *
     * @return string
     */
    public function getCustomerConnectInfoHow()
    {
        return $this->customerConnectInfoHow;
    }

    /**
     * Set directDebitAllowed.
     *
     * @param string $directDebitAllowed
     *
     * @return Customer
     */
    public function setDirectDebitAllowed($directDebitAllowed)
    {
        $this->directDebitAllowed = $directDebitAllowed;

        return $this;
    }

    /**
     * Get directDebitAllowed.
     *
     * @return string
     */
    public function getDirectDebitAllowed()
    {
        return $this->directDebitAllowed;
    }

    /**
     * Set district.
     *
     * @param string $district
     *
     * @return Customer
     */
    public function setDistrict($district)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district.
     *
     * @return string
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Set dPBO.
     *
     * @param string $dPBO
     *
     * @return Customer
     */
    public function setDPBO($dPBO)
    {
        $this->DPBO = $dPBO;

        return $this;
    }

    /**
     * Get dPBO.
     *
     * @return string
     */
    public function getDPBO()
    {
        return $this->DPBO;
    }

    /**
     * Set dslamArrangedDate.
     *
     * @param string $dslamArrangedDate
     *
     * @return Customer
     */
    public function setDslamArrangedDate($dslamArrangedDate)
    {
        $this->dslamArrangedDate = $dslamArrangedDate;

        return $this;
    }

    /**
     * Get dslamArrangedDate.
     *
     * @return string
     */
    public function getDslamArrangedDate()
    {
        return $this->dslamArrangedDate;
    }

    /**
     * Set dslamCardNo.
     *
     * @param string $dslamCardNo
     *
     * @return Customer
     */
    public function setDslamCardNo($dslamCardNo)
    {
        $this->dslamCardNo = $dslamCardNo;

        return $this;
    }

    /**
     * Get dslamCardNo.
     *
     * @return string
     */
    public function getDslamCardNo()
    {
        return $this->dslamCardNo;
    }

    /**
     * Set dslamLocation.
     *
     * @param string $dslamLocation
     *
     * @return Customer
     */
    public function setDslamLocation($dslamLocation)
    {
        $this->dslamLocation = $dslamLocation;

        return $this;
    }

    /**
     * Get dslamLocation.
     *
     * @return string
     */
    public function getDslamLocation()
    {
        return $this->dslamLocation;
    }

    /**
     * Set dslamPort.
     *
     * @param string $dslamPort
     *
     * @return Customer
     */
    public function setDslamPort($dslamPort)
    {
        $this->dslamPort = $dslamPort;

        return $this;
    }

    /**
     * Get dslamPort.
     *
     * @return string
     */
    public function getDslamPort()
    {
        return $this->dslamPort;
    }

    /**
     * Set dtagLine.
     *
     * @param string $dtagLine
     *
     * @return Customer
     */
    public function setDtagLine($dtagLine)
    {
        $this->dtagLine = $dtagLine;

        return $this;
    }

    /**
     * Get dtagLine.
     *
     * @return string
     */
    public function getDtagLine()
    {
        return $this->dtagLine;
    }

    /**
     * Set dtagbluckageFullfiled.
     *
     * @param string $dtagbluckageFullfiled
     *
     * @return Customer
     */
    public function setDtagbluckageFullfiled($dtagbluckageFullfiled)
    {
        $this->dtagbluckageFullfiled = $dtagbluckageFullfiled;

        return $this;
    }

    /**
     * Get dtagbluckageFullfiled.
     *
     * @return string
     */
    public function getDtagbluckageFullfiled()
    {
        return $this->dtagbluckageFullfiled;
    }

    /**
     * Set dtagbluckageFullfiledDate.
     *
     * @param string $dtagbluckageFullfiledDate
     *
     * @return Customer
     */
    public function setDtagbluckageFullfiledDate($dtagbluckageFullfiledDate)
    {
        $this->dtagbluckageFullfiledDate = $dtagbluckageFullfiledDate;

        return $this;
    }

    /**
     * Get dtagbluckageFullfiledDate.
     *
     * @return string
     */
    public function getDtagbluckageFullfiledDate()
    {
        return $this->dtagbluckageFullfiledDate;
    }

    /**
     * Set dtagbluckageSended.
     *
     * @param string $dtagbluckageSended
     *
     * @return Customer
     */
    public function setDtagbluckageSended($dtagbluckageSended)
    {
        $this->dtagbluckageSended = $dtagbluckageSended;

        return $this;
    }

    /**
     * Get dtagbluckageSended.
     *
     * @return string
     */
    public function getDtagbluckageSended()
    {
        return $this->dtagbluckageSended;
    }

    /**
     * Set dtagbluckageSendedDate.
     *
     * @param string $dtagbluckageSendedDate
     *
     * @return Customer
     */
    public function setDtagbluckageSendedDate($dtagbluckageSendedDate)
    {
        $this->dtagbluckageSendedDate = $dtagbluckageSendedDate;

        return $this;
    }

    /**
     * Get dtagbluckageSendedDate.
     *
     * @return string
     */
    public function getDtagbluckageSendedDate()
    {
        return $this->dtagbluckageSendedDate;
    }

    /**
     * Set emailaddress.
     *
     * @param string $emailaddress
     *
     * @return Customer
     */
    public function setEmailaddress($emailaddress)
    {
        $this->emailaddress = $emailaddress;

        return $this;
    }

    /**
     * Get emailaddress.
     *
     * @return string
     */
    public function getEmailaddress()
    {
        return $this->emailaddress;
    }

    /**
     * Set euFlat.
     *
     * @param string $euFlat
     *
     * @return Customer
     */
    public function setEuFlat($euFlat)
    {
        $this->euFlat = $euFlat;

        return $this;
    }

    /**
     * Get euFlat.
     *
     * @return string
     */
    public function getEuFlat()
    {
        return $this->euFlat;
    }

    /**
     * Set expDateInt.
     *
     * @param string $expDateInt
     *
     * @return Customer
     */
    public function setExpDateInt($expDateInt)
    {
        $this->expDateInt = $expDateInt;

        return $this;
    }

    /**
     * Get expDateInt.
     *
     * @return string
     */
    public function getExpDateInt()
    {
        return $this->expDateInt;
    }

    /**
     * Set expDatePhone.
     *
     * @param string $expDatePhone
     *
     * @return Customer
     */
    public function setExpDatePhone($expDatePhone)
    {
        $this->expDatePhone = $expDatePhone;

        return $this;
    }

    /**
     * Get expDatePhone.
     *
     * @return string
     */
    public function getExpDatePhone()
    {
        return $this->expDatePhone;
    }

    /**
     * Set fax.
     *
     * @param string $fax
     *
     * @return Customer
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax.
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set fbVorab.
     *
     * @param string $fbVorab
     *
     * @return Customer
     */
    public function setFbVorab($fbVorab)
    {
        $this->fbVorab = $fbVorab;

        return $this;
    }

    /**
     * Get fbVorab.
     *
     * @return string
     */
    public function getFbVorab()
    {
        return $this->fbVorab;
    }

    /**
     * Set finalPurtelproductDate.
     *
     * @param string $finalPurtelproductDate
     *
     * @return Customer
     */
    public function setFinalPurtelproductDate($finalPurtelproductDate)
    {
        $this->finalPurtelproductDate = $finalPurtelproductDate;

        return $this;
    }

    /**
     * Get finalPurtelproductDate.
     *
     * @return string
     */
    public function getFinalPurtelproductDate()
    {
        return $this->finalPurtelproductDate;
    }

    /**
     * Set firewallRouter.
     *
     * @param string $firewallRouter
     *
     * @return Customer
     */
    public function setFirewallRouter($firewallRouter)
    {
        $this->firewallRouter = $firewallRouter;

        return $this;
    }

    /**
     * Get firewallRouter.
     *
     * @return string
     */
    public function getFirewallRouter()
    {
        return $this->firewallRouter;
    }

    /**
     * Set firmwareVersion.
     *
     * @param string $firmwareVersion
     *
     * @return Customer
     */
    public function setFirmwareVersion($firmwareVersion)
    {
        $this->firmwareVersion = $firmwareVersion;

        return $this;
    }

    /**
     * Get firmwareVersion.
     *
     * @return string
     */
    public function getFirmwareVersion()
    {
        return $this->firmwareVersion;
    }

    /**
     * Set firstname.
     *
     * @param string $firstname
     *
     * @return Customer
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname.
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set flatEuNetwork.
     *
     * @param string $flatEuNetwork
     *
     * @return Customer
     */
    public function setFlatEuNetwork($flatEuNetwork)
    {
        $this->flatEuNetwork = $flatEuNetwork;

        return $this;
    }

    /**
     * Get flatEuNetwork.
     *
     * @return string
     */
    public function getFlatEuNetwork()
    {
        return $this->flatEuNetwork;
    }

    /**
     * Set flatEuNetworkCost.
     *
     * @param string $flatEuNetworkCost
     *
     * @return Customer
     */
    public function setFlatEuNetworkCost($flatEuNetworkCost)
    {
        $this->flatEuNetworkCost = $flatEuNetworkCost;

        return $this;
    }

    /**
     * Get flatEuNetworkCost.
     *
     * @return string
     */
    public function getFlatEuNetworkCost()
    {
        return $this->flatEuNetworkCost;
    }

    /**
     * Set flatGermanNetwork.
     *
     * @param string $flatGermanNetwork
     *
     * @return Customer
     */
    public function setFlatGermanNetwork($flatGermanNetwork)
    {
        $this->flatGermanNetwork = $flatGermanNetwork;

        return $this;
    }

    /**
     * Get flatGermanNetwork.
     *
     * @return string
     */
    public function getFlatGermanNetwork()
    {
        return $this->flatGermanNetwork;
    }

    /**
     * Set flatGermanNetworkCost.
     *
     * @param string $flatGermanNetworkCost
     *
     * @return Customer
     */
    public function setFlatGermanNetworkCost($flatGermanNetworkCost)
    {
        $this->flatGermanNetworkCost = $flatGermanNetworkCost;

        return $this;
    }

    /**
     * Get flatGermanNetworkCost.
     *
     * @return string
     */
    public function getFlatGermanNetworkCost()
    {
        return $this->flatGermanNetworkCost;
    }

    /**
     * Set forderingCosts.
     *
     * @param string $forderingCosts
     *
     * @return Customer
     */
    public function setForderingCosts($forderingCosts)
    {
        $this->forderingCosts = $forderingCosts;

        return $this;
    }

    /**
     * Get forderingCosts.
     *
     * @return string
     */
    public function getForderingCosts()
    {
        return $this->forderingCosts;
    }

    /**
     * Set germanMobile.
     *
     * @param string $germanMobile
     *
     * @return Customer
     */
    public function setGermanMobile($germanMobile)
    {
        $this->germanMobile = $germanMobile;

        return $this;
    }

    /**
     * Get germanMobile.
     *
     * @return string
     */
    public function getGermanMobile()
    {
        return $this->germanMobile;
    }

    /**
     * Set germanMobilePrice.
     *
     * @param string $germanMobilePrice
     *
     * @return Customer
     */
    public function setGermanMobilePrice($germanMobilePrice)
    {
        $this->germanMobilePrice = $germanMobilePrice;

        return $this;
    }

    /**
     * Get germanMobilePrice.
     *
     * @return string
     */
    public function getGermanMobilePrice()
    {
        return $this->germanMobilePrice;
    }

    /**
     * Set germanNetwork.
     *
     * @param string $germanNetwork
     *
     * @return Customer
     */
    public function setGermanNetwork($germanNetwork)
    {
        $this->germanNetwork = $germanNetwork;

        return $this;
    }

    /**
     * Get germanNetwork.
     *
     * @return string
     */
    public function getGermanNetwork()
    {
        return $this->germanNetwork;
    }

    /**
     * Set germanNetworkPrice.
     *
     * @param string $germanNetworkPrice
     *
     * @return Customer
     */
    public function setGermanNetworkPrice($germanNetworkPrice)
    {
        $this->germanNetworkPrice = $germanNetworkPrice;

        return $this;
    }

    /**
     * Get germanNetworkPrice.
     *
     * @return string
     */
    public function getGermanNetworkPrice()
    {
        return $this->germanNetworkPrice;
    }

    /**
     * Set gfBranch.
     *
     * @param string $gfBranch
     *
     * @return Customer
     */
    public function setGfBranch($gfBranch)
    {
        $this->gfBranch = $gfBranch;

        return $this;
    }

    /**
     * Get gfBranch.
     *
     * @return string
     */
    public function getGfBranch()
    {
        return $this->gfBranch;
    }

    /**
     * Set gfCabling.
     *
     * @param string $gfCabling
     *
     * @return Customer
     */
    public function setGfCabling($gfCabling)
    {
        $this->gfCabling = $gfCabling;

        return $this;
    }

    /**
     * Get gfCabling.
     *
     * @return string
     */
    public function getGfCabling()
    {
        return $this->gfCabling;
    }

    /**
     * Set gfCablingCost.
     *
     * @param string $gfCablingCost
     *
     * @return Customer
     */
    public function setGfCablingCost($gfCablingCost)
    {
        $this->gfCablingCost = $gfCablingCost;

        return $this;
    }

    /**
     * Get gfCablingCost.
     *
     * @return string
     */
    public function getGfCablingCost()
    {
        return $this->gfCablingCost;
    }

    /**
     * Set gutschrift.
     *
     * @param string $gutschrift
     *
     * @return Customer
     */
    public function setGutschrift($gutschrift)
    {
        $this->gutschrift = $gutschrift;

        return $this;
    }

    /**
     * Get gutschrift.
     *
     * @return string
     */
    public function getGutschrift()
    {
        return $this->gutschrift;
    }

    /**
     * Set gutschriftTill.
     *
     * @param string $gutschriftTill
     *
     * @return Customer
     */
    public function setGutschriftTill($gutschriftTill)
    {
        $this->gutschriftTill = $gutschriftTill;

        return $this;
    }

    /**
     * Get gutschriftTill.
     *
     * @return string
     */
    public function getGutschriftTill()
    {
        return $this->gutschriftTill;
    }

    /**
     * Set hdPlusCard.
     *
     * @param string $hdPlusCard
     *
     * @return Customer
     */
    public function setHdPlusCard($hdPlusCard)
    {
        $this->hdPlusCard = $hdPlusCard;

        return $this;
    }

    /**
     * Get hdPlusCard.
     *
     * @return string
     */
    public function getHdPlusCard()
    {
        return $this->hdPlusCard;
    }

    /**
     * Set higherAvailability.
     *
     * @param string $higherAvailability
     *
     * @return Customer
     */
    public function setHigherAvailability($higherAvailability)
    {
        $this->higherAvailability = $higherAvailability;

        return $this;
    }

    /**
     * Get higherAvailability.
     *
     * @return string
     */
    public function getHigherAvailability()
    {
        return $this->higherAvailability;
    }

    /**
     * Set higherUplinkOne.
     *
     * @param string $higherUplinkOne
     *
     * @return Customer
     */
    public function setHigherUplinkOne($higherUplinkOne)
    {
        $this->higherUplinkOne = $higherUplinkOne;

        return $this;
    }

    /**
     * Get higherUplinkOne.
     *
     * @return string
     */
    public function getHigherUplinkOne()
    {
        return $this->higherUplinkOne;
    }

    /**
     * Set higherUplinkTwo.
     *
     * @param string $higherUplinkTwo
     *
     * @return Customer
     */
    public function setHigherUplinkTwo($higherUplinkTwo)
    {
        $this->higherUplinkTwo = $higherUplinkTwo;

        return $this;
    }

    /**
     * Get higherUplinkTwo.
     *
     * @return string
     */
    public function getHigherUplinkTwo()
    {
        return $this->higherUplinkTwo;
    }

    /**
     * Set history.
     *
     * @param string $history
     *
     * @return Customer
     */
    public function setHistory($history)
    {
        $this->history = $history;

        return $this;
    }

    /**
     * Get history.
     *
     * @return string
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * Set houseConnection.
     *
     * @param string $houseConnection
     *
     * @return Customer
     */
    public function setHouseConnection($houseConnection)
    {
        $this->houseConnection = $houseConnection;

        return $this;
    }

    /**
     * Get houseConnection.
     *
     * @return string
     */
    public function getHouseConnection()
    {
        return $this->houseConnection;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set implementation.
     *
     * @param string $implementation
     *
     * @return Customer
     */
    public function setImplementation($implementation)
    {
        $this->implementation = $implementation;

        return $this;
    }

    /**
     * Get implementation.
     *
     * @return string
     */
    public function getImplementation()
    {
        return $this->implementation;
    }

    /**
     * Set implementationText.
     *
     * @param string $implementationText
     *
     * @return Customer
     */
    public function setImplementationText($implementationText)
    {
        $this->implementationText = $implementationText;

        return $this;
    }

    /**
     * Get implementationText.
     *
     * @return string
     */
    public function getImplementationText()
    {
        return $this->implementationText;
    }

    /**
     * Set internationalCallsPrice.
     *
     * @param string $internationalCallsPrice
     *
     * @return Customer
     */
    public function setInternationalCallsPrice($internationalCallsPrice)
    {
        $this->internationalCallsPrice = $internationalCallsPrice;

        return $this;
    }

    /**
     * Get internationalCallsPrice.
     *
     * @return string
     */
    public function getInternationalCallsPrice()
    {
        return $this->internationalCallsPrice;
    }

    /**
     * Set ipAddress.
     *
     * @param string $ipAddress
     *
     * @return Customer
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress.
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set ipAddressInet.
     *
     * @param string $ipAddressInet
     *
     * @return Customer
     */
    public function setIpAddressInet($ipAddressInet)
    {
        $this->ipAddressInet = $ipAddressInet;

        return $this;
    }

    /**
     * Get ipAddressInet.
     *
     * @return string
     */
    public function getIpAddressInet()
    {
        return $this->ipAddressInet;
    }

    /**
     * Set ipAddressPhone.
     *
     * @param string $ipAddressPhone
     *
     * @return Customer
     */
    public function setIpAddressPhone($ipAddressPhone)
    {
        $this->ipAddressPhone = $ipAddressPhone;

        return $this;
    }

    /**
     * Get ipAddressPhone.
     *
     * @return string
     */
    public function getIpAddressPhone()
    {
        return $this->ipAddressPhone;
    }

    /**
     * Set ipRangeId.
     *
     * @param string $ipRangeId
     *
     * @return Customer
     */
    public function setIpRangeId($ipRangeId)
    {
        $this->ipRangeId = $ipRangeId;

        return $this;
    }

    /**
     * Get ipRangeId.
     *
     * @return string
     */
    public function getIpRangeId()
    {
        return $this->ipRangeId;
    }

    /**
     * Set isdnBackup.
     *
     * @param string $isdnBackup
     *
     * @return Customer
     */
    public function setIsdnBackup($isdnBackup)
    {
        $this->isdnBackup = $isdnBackup;

        return $this;
    }

    /**
     * Get isdnBackup.
     *
     * @return string
     */
    public function getIsdnBackup()
    {
        return $this->isdnBackup;
    }

    /**
     * Set isdnBackup2.
     *
     * @param string $isdnBackup2
     *
     * @return Customer
     */
    public function setIsdnBackup2($isdnBackup2)
    {
        $this->isdnBackup2 = $isdnBackup2;

        return $this;
    }

    /**
     * Get isdnBackup2.
     *
     * @return string
     */
    public function getIsdnBackup2()
    {
        return $this->isdnBackup2;
    }

    /**
     * Set kommandoQuery.
     *
     * @param string $kommandoQuery
     *
     * @return Customer
     */
    public function setKommandoQuery($kommandoQuery)
    {
        $this->KommandoQuery = $kommandoQuery;

        return $this;
    }

    /**
     * Get kommandoQuery.
     *
     * @return string
     */
    public function getKommandoQuery()
    {
        return $this->KommandoQuery;
    }

    /**
     * Set kvzAddition.
     *
     * @param string $kvzAddition
     *
     * @return Customer
     */
    public function setKvzAddition($kvzAddition)
    {
        $this->kvzAddition = $kvzAddition;

        return $this;
    }

    /**
     * Get kvzAddition.
     *
     * @return string
     */
    public function getKvzAddition()
    {
        return $this->kvzAddition;
    }

    /**
     * Set kvzName.
     *
     * @param string $kvzName
     *
     * @return Customer
     */
    public function setKvzName($kvzName)
    {
        $this->kvzName = $kvzName;

        return $this;
    }

    /**
     * Get kvzName.
     *
     * @return string
     */
    public function getKvzName()
    {
        return $this->kvzName;
    }

    /**
     * Set kvzStandort.
     *
     * @param string $kvzStandort
     *
     * @return Customer
     */
    public function setKvzStandort($kvzStandort)
    {
        $this->kvzStandort = $kvzStandort;

        return $this;
    }

    /**
     * Get kvzStandort.
     *
     * @return string
     */
    public function getKvzStandort()
    {
        return $this->kvzStandort;
    }

    /**
     * Set kvzToggleNo.
     *
     * @param string $kvzToggleNo
     *
     * @return Customer
     */
    public function setKvzToggleNo($kvzToggleNo)
    {
        $this->kvzToggleNo = $kvzToggleNo;

        return $this;
    }

    /**
     * Get kvzToggleNo.
     *
     * @return string
     */
    public function getKvzToggleNo()
    {
        return $this->kvzToggleNo;
    }

    /**
     * Set lapseNoticeDateInet.
     *
     * @param string $lapseNoticeDateInet
     *
     * @return Customer
     */
    public function setLapseNoticeDateInet($lapseNoticeDateInet)
    {
        $this->lapseNoticeDateInet = $lapseNoticeDateInet;

        return $this;
    }

    /**
     * Get lapseNoticeDateInet.
     *
     * @return string
     */
    public function getLapseNoticeDateInet()
    {
        return $this->lapseNoticeDateInet;
    }

    /**
     * Set lapseNoticeDateTel.
     *
     * @param string $lapseNoticeDateTel
     *
     * @return Customer
     */
    public function setLapseNoticeDateTel($lapseNoticeDateTel)
    {
        $this->lapseNoticeDateTel = $lapseNoticeDateTel;

        return $this;
    }

    /**
     * Get lapseNoticeDateTel.
     *
     * @return string
     */
    public function getLapseNoticeDateTel()
    {
        return $this->lapseNoticeDateTel;
    }

    /**
     * Set lastname.
     *
     * @param string $lastname
     *
     * @return Customer
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname.
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set lineIdentifier.
     *
     * @param string $lineIdentifier
     *
     * @return Customer
     */
    public function setLineIdentifier($lineIdentifier)
    {
        $this->lineIdentifier = $lineIdentifier;

        return $this;
    }

    /**
     * Get lineIdentifier.
     *
     * @return string
     */
    public function getLineIdentifier()
    {
        return $this->lineIdentifier;
    }

    /**
     * Set littleBar.
     *
     * @param string $littleBar
     *
     * @return Customer
     */
    public function setLittleBar($littleBar)
    {
        $this->littleBar = $littleBar;

        return $this;
    }

    /**
     * Get littleBar.
     *
     * @return string
     */
    public function getLittleBar()
    {
        return $this->littleBar;
    }

    /**
     * Set locationId.
     *
     * @param string $locationId
     *
     * @return Customer
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;

        return $this;
    }

    /**
     * Get locationId.
     *
     * @return string
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * Set lsaKvzPartitionId.
     *
     * @param string $lsaKvzPartitionId
     *
     * @return Customer
     */
    public function setLsaKvzPartitionId($lsaKvzPartitionId)
    {
        $this->lsaKvzPartitionId = $lsaKvzPartitionId;

        return $this;
    }

    /**
     * Get lsaKvzPartitionId.
     *
     * @return string
     */
    public function getLsaKvzPartitionId()
    {
        return $this->lsaKvzPartitionId;
    }

    /**
     * Set lsaPin.
     *
     * @param string $lsaPin
     *
     * @return Customer
     */
    public function setLsaPin($lsaPin)
    {
        $this->lsaPin = $lsaPin;

        return $this;
    }

    /**
     * Get lsaPin.
     *
     * @return string
     */
    public function getLsaPin()
    {
        return $this->lsaPin;
    }

    /**
     * Set macAddress.
     *
     * @param string $macAddress
     *
     * @return Customer
     */
    public function setMacAddress($macAddress)
    {
        $this->macAddress = $macAddress;

        return $this;
    }

    /**
     * Get macAddress.
     *
     * @return string
     */
    public function getMacAddress()
    {
        return $this->macAddress;
    }

    /**
     * Set mediaConverter.
     *
     * @param string $mediaConverter
     *
     * @return Customer
     */
    public function setMediaConverter($mediaConverter)
    {
        $this->mediaConverter = $mediaConverter;

        return $this;
    }

    /**
     * Get mediaConverter.
     *
     * @return string
     */
    public function getMediaConverter()
    {
        return $this->mediaConverter;
    }

    /**
     * Set mobileFlat.
     *
     * @param string $mobileFlat
     *
     * @return Customer
     */
    public function setMobileFlat($mobileFlat)
    {
        $this->mobileFlat = $mobileFlat;

        return $this;
    }

    /**
     * Get mobileFlat.
     *
     * @return string
     */
    public function getMobileFlat()
    {
        return $this->mobileFlat;
    }

    /**
     * Set mobilephone.
     *
     * @param string $mobilephone
     *
     * @return Customer
     */
    public function setMobilephone($mobilephone)
    {
        $this->mobilephone = $mobilephone;

        return $this;
    }

    /**
     * Get mobilephone.
     *
     * @return string
     */
    public function getMobilephone()
    {
        return $this->mobilephone;
    }

    /**
     * Set moved.
     *
     * @param string $moved
     *
     * @return Customer
     */
    public function setMoved($moved)
    {
        $this->moved = $moved;

        return $this;
    }

    /**
     * Get moved.
     *
     * @return string
     */
    public function getMoved()
    {
        return $this->moved;
    }

    /**
     * Set nbCanceledDate.
     *
     * @param string $nbCanceledDate
     *
     * @return Customer
     */
    public function setNbCanceledDate($nbCanceledDate)
    {
        $this->nbCanceledDate = $nbCanceledDate;

        return $this;
    }

    /**
     * Get nbCanceledDate.
     *
     * @return string
     */
    public function getNbCanceledDate()
    {
        return $this->nbCanceledDate;
    }

    /**
     * Set nbCanceledDatePhone.
     *
     * @param string $nbCanceledDatePhone
     *
     * @return Customer
     */
    public function setNbCanceledDatePhone($nbCanceledDatePhone)
    {
        $this->nbCanceledDatePhone = $nbCanceledDatePhone;

        return $this;
    }

    /**
     * Get nbCanceledDatePhone.
     *
     * @return string
     */
    public function getNbCanceledDatePhone()
    {
        return $this->nbCanceledDatePhone;
    }

    /**
     * Set network.
     *
     * @param string $network
     *
     * @return Customer
     */
    public function setNetwork($network)
    {
        $this->network = $network;

        return $this;
    }

    /**
     * Get network.
     *
     * @return string
     */
    public function getNetwork()
    {
        return $this->network;
    }

    /**
     * Set networkId.
     *
     * @param string $networkId
     *
     * @return Customer
     */
    public function setNetworkId($networkId)
    {
        $this->networkId = $networkId;

        return $this;
    }

    /**
     * Get networkId.
     *
     * @return string
     */
    public function getNetworkId()
    {
        return $this->networkId;
    }

    /**
     * Set newClientidPermission.
     *
     * @param string $newClientidPermission
     *
     * @return Customer
     */
    public function setNewClientidPermission($newClientidPermission)
    {
        $this->newClientidPermission = $newClientidPermission;

        return $this;
    }

    /**
     * Get newClientidPermission.
     *
     * @return string
     */
    public function getNewClientidPermission()
    {
        return $this->newClientidPermission;
    }

    /**
     * Set newNumberDate.
     *
     * @param string $newNumberDate
     *
     * @return Customer
     */
    public function setNewNumberDate($newNumberDate)
    {
        $this->newNumberDate = $newNumberDate;

        return $this;
    }

    /**
     * Get newNumberDate.
     *
     * @return string
     */
    public function getNewNumberDate()
    {
        return $this->newNumberDate;
    }

    /**
     * Set newNumberFrom.
     *
     * @param string $newNumberFrom
     *
     * @return Customer
     */
    public function setNewNumberFrom($newNumberFrom)
    {
        $this->newNumberFrom = $newNumberFrom;

        return $this;
    }

    /**
     * Get newNumberFrom.
     *
     * @return string
     */
    public function getNewNumberFrom()
    {
        return $this->newNumberFrom;
    }

    /**
     * Set newNumbersText.
     *
     * @param string $newNumbersText
     *
     * @return Customer
     */
    public function setNewNumbersText($newNumbersText)
    {
        $this->newNumbersText = $newNumbersText;

        return $this;
    }

    /**
     * Get newNumbersText.
     *
     * @return string
     */
    public function getNewNumbersText()
    {
        return $this->newNumbersText;
    }

    /**
     * Set newOrPortedPhonenumbers.
     *
     * @param string $newOrPortedPhonenumbers
     *
     * @return Customer
     */
    public function setNewOrPortedPhonenumbers($newOrPortedPhonenumbers)
    {
        $this->newOrPortedPhonenumbers = $newOrPortedPhonenumbers;

        return $this;
    }

    /**
     * Get newOrPortedPhonenumbers.
     *
     * @return string
     */
    public function getNewOrPortedPhonenumbers()
    {
        return $this->newOrPortedPhonenumbers;
    }

    /**
     * Set noEze.
     *
     * @param string $noEze
     *
     * @return Customer
     */
    public function setNoEze($noEze)
    {
        $this->noEze = $noEze;

        return $this;
    }

    /**
     * Get noEze.
     *
     * @return string
     */
    public function getNoEze()
    {
        return $this->noEze;
    }

    /**
     * Set noPing.
     *
     * @param string $noPing
     *
     * @return Customer
     */
    public function setNoPing($noPing)
    {
        $this->noPing = $noPing;

        return $this;
    }

    /**
     * Get noPing.
     *
     * @return string
     */
    public function getNoPing()
    {
        return $this->noPing;
    }

    /**
     * Set noticePeriodInternetInt.
     *
     * @param string $noticePeriodInternetInt
     *
     * @return Customer
     */
    public function setNoticePeriodInternetInt($noticePeriodInternetInt)
    {
        $this->noticePeriodInternetInt = $noticePeriodInternetInt;

        return $this;
    }

    /**
     * Get noticePeriodInternetInt.
     *
     * @return string
     */
    public function getNoticePeriodInternetInt()
    {
        return $this->noticePeriodInternetInt;
    }

    /**
     * Set noticePeriodInternetType.
     *
     * @param string $noticePeriodInternetType
     *
     * @return Customer
     */
    public function setNoticePeriodInternetType($noticePeriodInternetType)
    {
        $this->noticePeriodInternetType = $noticePeriodInternetType;

        return $this;
    }

    /**
     * Get noticePeriodInternetType.
     *
     * @return string
     */
    public function getNoticePeriodInternetType()
    {
        return $this->noticePeriodInternetType;
    }

    /**
     * Set noticePeriodPhoneInt.
     *
     * @param string $noticePeriodPhoneInt
     *
     * @return Customer
     */
    public function setNoticePeriodPhoneInt($noticePeriodPhoneInt)
    {
        $this->noticePeriodPhoneInt = $noticePeriodPhoneInt;

        return $this;
    }

    /**
     * Get noticePeriodPhoneInt.
     *
     * @return string
     */
    public function getNoticePeriodPhoneInt()
    {
        return $this->noticePeriodPhoneInt;
    }

    /**
     * Set noticePeriodPhoneType.
     *
     * @param string $noticePeriodPhoneType
     *
     * @return Customer
     */
    public function setNoticePeriodPhoneType($noticePeriodPhoneType)
    {
        $this->noticePeriodPhoneType = $noticePeriodPhoneType;

        return $this;
    }

    /**
     * Get noticePeriodPhoneType.
     *
     * @return string
     */
    public function getNoticePeriodPhoneType()
    {
        return $this->noticePeriodPhoneType;
    }

    /**
     * Set oldContractActive.
     *
     * @param string $oldContractActive
     *
     * @return Customer
     */
    public function setOldContractActive($oldContractActive)
    {
        $this->oldContractActive = $oldContractActive;

        return $this;
    }

    /**
     * Get oldContractActive.
     *
     * @return string
     */
    public function getOldContractActive()
    {
        return $this->oldContractActive;
    }

    /**
     * Set oldProviderComment.
     *
     * @param string $oldProviderComment
     *
     * @return Customer
     */
    public function setOldProviderComment($oldProviderComment)
    {
        $this->oldProviderComment = $oldProviderComment;

        return $this;
    }

    /**
     * Get oldProviderComment.
     *
     * @return string
     */
    public function getOldProviderComment()
    {
        return $this->oldProviderComment;
    }

    /**
     * Set oldcontractAddressEqual.
     *
     * @param string $oldcontractAddressEqual
     *
     * @return Customer
     */
    public function setOldcontractAddressEqual($oldcontractAddressEqual)
    {
        $this->oldcontractAddressEqual = $oldcontractAddressEqual;

        return $this;
    }

    /**
     * Get oldcontractAddressEqual.
     *
     * @return string
     */
    public function getOldcontractAddressEqual()
    {
        return $this->oldcontractAddressEqual;
    }

    /**
     * Set oldcontractCity.
     *
     * @param string $oldcontractCity
     *
     * @return Customer
     */
    public function setOldcontractCity($oldcontractCity)
    {
        $this->oldcontractCity = $oldcontractCity;

        return $this;
    }

    /**
     * Get oldcontractCity.
     *
     * @return string
     */
    public function getOldcontractCity()
    {
        return $this->oldcontractCity;
    }

    /**
     * Set oldcontractComment.
     *
     * @param string $oldcontractComment
     *
     * @return Customer
     */
    public function setOldcontractComment($oldcontractComment)
    {
        $this->oldcontractComment = $oldcontractComment;

        return $this;
    }

    /**
     * Get oldcontractComment.
     *
     * @return string
     */
    public function getOldcontractComment()
    {
        return $this->oldcontractComment;
    }

    /**
     * Set oldcontractExists.
     *
     * @param string $oldcontractExists
     *
     * @return Customer
     */
    public function setOldcontractExists($oldcontractExists)
    {
        $this->oldcontractExists = $oldcontractExists;

        return $this;
    }

    /**
     * Get oldcontractExists.
     *
     * @return string
     */
    public function getOldcontractExists()
    {
        return $this->oldcontractExists;
    }

    /**
     * Set oldcontractFirstname.
     *
     * @param string $oldcontractFirstname
     *
     * @return Customer
     */
    public function setOldcontractFirstname($oldcontractFirstname)
    {
        $this->oldcontractFirstname = $oldcontractFirstname;

        return $this;
    }

    /**
     * Get oldcontractFirstname.
     *
     * @return string
     */
    public function getOldcontractFirstname()
    {
        return $this->oldcontractFirstname;
    }

    /**
     * Set oldcontractInternetClientCancelled.
     *
     * @param string $oldcontractInternetClientCancelled
     *
     * @return Customer
     */
    public function setOldcontractInternetClientCancelled($oldcontractInternetClientCancelled)
    {
        $this->oldcontractInternetClientCancelled = $oldcontractInternetClientCancelled;

        return $this;
    }

    /**
     * Get oldcontractInternetClientCancelled.
     *
     * @return string
     */
    public function getOldcontractInternetClientCancelled()
    {
        return $this->oldcontractInternetClientCancelled;
    }

    /**
     * Set oldcontractInternetClientno.
     *
     * @param string $oldcontractInternetClientno
     *
     * @return Customer
     */
    public function setOldcontractInternetClientno($oldcontractInternetClientno)
    {
        $this->oldcontractInternetClientno = $oldcontractInternetClientno;

        return $this;
    }

    /**
     * Get oldcontractInternetClientno.
     *
     * @return string
     */
    public function getOldcontractInternetClientno()
    {
        return $this->oldcontractInternetClientno;
    }

    /**
     * Set oldcontractInternetProvider.
     *
     * @param string $oldcontractInternetProvider
     *
     * @return Customer
     */
    public function setOldcontractInternetProvider($oldcontractInternetProvider)
    {
        $this->oldcontractInternetProvider = $oldcontractInternetProvider;

        return $this;
    }

    /**
     * Get oldcontractInternetProvider.
     *
     * @return string
     */
    public function getOldcontractInternetProvider()
    {
        return $this->oldcontractInternetProvider;
    }

    /**
     * Set oldcontractLastname.
     *
     * @param string $oldcontractLastname
     *
     * @return Customer
     */
    public function setOldcontractLastname($oldcontractLastname)
    {
        $this->oldcontractLastname = $oldcontractLastname;

        return $this;
    }

    /**
     * Get oldcontractLastname.
     *
     * @return string
     */
    public function getOldcontractLastname()
    {
        return $this->oldcontractLastname;
    }

    /**
     * Set oldcontractPhoneClientCancelled.
     *
     * @param string $oldcontractPhoneClientCancelled
     *
     * @return Customer
     */
    public function setOldcontractPhoneClientCancelled($oldcontractPhoneClientCancelled)
    {
        $this->oldcontractPhoneClientCancelled = $oldcontractPhoneClientCancelled;

        return $this;
    }

    /**
     * Get oldcontractPhoneClientCancelled.
     *
     * @return string
     */
    public function getOldcontractPhoneClientCancelled()
    {
        return $this->oldcontractPhoneClientCancelled;
    }

    /**
     * Set oldcontractPhoneClientno.
     *
     * @param string $oldcontractPhoneClientno
     *
     * @return Customer
     */
    public function setOldcontractPhoneClientno($oldcontractPhoneClientno)
    {
        $this->oldcontractPhoneClientno = $oldcontractPhoneClientno;

        return $this;
    }

    /**
     * Get oldcontractPhoneClientno.
     *
     * @return string
     */
    public function getOldcontractPhoneClientno()
    {
        return $this->oldcontractPhoneClientno;
    }

    /**
     * Set oldcontractPhoneProvider.
     *
     * @param string $oldcontractPhoneProvider
     *
     * @return Customer
     */
    public function setOldcontractPhoneProvider($oldcontractPhoneProvider)
    {
        $this->oldcontractPhoneProvider = $oldcontractPhoneProvider;

        return $this;
    }

    /**
     * Get oldcontractPhoneProvider.
     *
     * @return string
     */
    public function getOldcontractPhoneProvider()
    {
        return $this->oldcontractPhoneProvider;
    }

    /**
     * Set oldcontractPhoneType.
     *
     * @param string $oldcontractPhoneType
     *
     * @return Customer
     */
    public function setOldcontractPhoneType($oldcontractPhoneType)
    {
        $this->oldcontractPhoneType = $oldcontractPhoneType;

        return $this;
    }

    /**
     * Get oldcontractPhoneType.
     *
     * @return string
     */
    public function getOldcontractPhoneType()
    {
        return $this->oldcontractPhoneType;
    }

    /**
     * Set oldcontractStreet.
     *
     * @param string $oldcontractStreet
     *
     * @return Customer
     */
    public function setOldcontractStreet($oldcontractStreet)
    {
        $this->oldcontractStreet = $oldcontractStreet;

        return $this;
    }

    /**
     * Get oldcontractStreet.
     *
     * @return string
     */
    public function getOldcontractStreet()
    {
        return $this->oldcontractStreet;
    }

    /**
     * Set oldcontractStreetno.
     *
     * @param string $oldcontractStreetno
     *
     * @return Customer
     */
    public function setOldcontractStreetno($oldcontractStreetno)
    {
        $this->oldcontractStreetno = $oldcontractStreetno;

        return $this;
    }

    /**
     * Get oldcontractStreetno.
     *
     * @return string
     */
    public function getOldcontractStreetno()
    {
        return $this->oldcontractStreetno;
    }

    /**
     * Set oldcontractTitle.
     *
     * @param string $oldcontractTitle
     *
     * @return Customer
     */
    public function setOldcontractTitle($oldcontractTitle)
    {
        $this->oldcontractTitle = $oldcontractTitle;

        return $this;
    }

    /**
     * Get oldcontractTitle.
     *
     * @return string
     */
    public function getOldcontractTitle()
    {
        return $this->oldcontractTitle;
    }

    /**
     * Set oldcontractZipcode.
     *
     * @param string $oldcontractZipcode
     *
     * @return Customer
     */
    public function setOldcontractZipcode($oldcontractZipcode)
    {
        $this->oldcontractZipcode = $oldcontractZipcode;

        return $this;
    }

    /**
     * Get oldcontractZipcode.
     *
     * @return string
     */
    public function getOldcontractZipcode()
    {
        return $this->oldcontractZipcode;
    }

    /**
     * Set paperBill.
     *
     * @param string $paperBill
     *
     * @return Customer
     */
    public function setPaperBill($paperBill)
    {
        $this->paperBill = $paperBill;

        return $this;
    }

    /**
     * Get paperBill.
     *
     * @return string
     */
    public function getPaperBill()
    {
        return $this->paperBill;
    }

    /**
     * Set password.
     *
     * @param string $password
     *
     * @return Customer
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set patchDate.
     *
     * @param string $patchDate
     *
     * @return Customer
     */
    public function setPatchDate($patchDate)
    {
        $this->patchDate = $patchDate;

        return $this;
    }

    /**
     * Get patchDate.
     *
     * @return string
     */
    public function getPatchDate()
    {
        return $this->patchDate;
    }

    /**
     * Set paymentPerformance.
     *
     * @param string $paymentPerformance
     *
     * @return Customer
     */
    public function setPaymentPerformance($paymentPerformance)
    {
        $this->paymentPerformance = $paymentPerformance;

        return $this;
    }

    /**
     * Get paymentPerformance.
     *
     * @return string
     */
    public function getPaymentPerformance()
    {
        return $this->paymentPerformance;
    }

    /**
     * Set phoneAdapter.
     *
     * @param string $phoneAdapter
     *
     * @return Customer
     */
    public function setPhoneAdapter($phoneAdapter)
    {
        $this->phoneAdapter = $phoneAdapter;

        return $this;
    }

    /**
     * Get phoneAdapter.
     *
     * @return string
     */
    public function getPhoneAdapter()
    {
        return $this->phoneAdapter;
    }

    /**
     * Set phoneComment.
     *
     * @param string $phoneComment
     *
     * @return Customer
     */
    public function setPhoneComment($phoneComment)
    {
        $this->phoneComment = $phoneComment;

        return $this;
    }

    /**
     * Get phoneComment.
     *
     * @return string
     */
    public function getPhoneComment()
    {
        return $this->phoneComment;
    }

    /**
     * Set phoneNumberAdded.
     *
     * @param string $phoneNumberAdded
     *
     * @return Customer
     */
    public function setPhoneNumberAdded($phoneNumberAdded)
    {
        $this->phoneNumberAdded = $phoneNumberAdded;

        return $this;
    }

    /**
     * Get phoneNumberAdded.
     *
     * @return string
     */
    public function getPhoneNumberAdded()
    {
        return $this->phoneNumberAdded;
    }

    /**
     * Set phoneNumberBlock.
     *
     * @param string $phoneNumberBlock
     *
     * @return Customer
     */
    public function setPhoneNumberBlock($phoneNumberBlock)
    {
        $this->phoneNumberBlock = $phoneNumberBlock;

        return $this;
    }

    /**
     * Get phoneNumberBlock.
     *
     * @return string
     */
    public function getPhoneNumberBlock()
    {
        return $this->phoneNumberBlock;
    }

    /**
     * Set phoneareacode.
     *
     * @param string $phoneareacode
     *
     * @return Customer
     */
    public function setPhoneareacode($phoneareacode)
    {
        $this->phoneareacode = $phoneareacode;

        return $this;
    }

    /**
     * Get phoneareacode.
     *
     * @return string
     */
    public function getPhoneareacode()
    {
        return $this->phoneareacode;
    }

    /**
     * Set phoneentryDoneDate.
     *
     * @param string $phoneentryDoneDate
     *
     * @return Customer
     */
    public function setPhoneentryDoneDate($phoneentryDoneDate)
    {
        $this->phoneentryDoneDate = $phoneentryDoneDate;

        return $this;
    }

    /**
     * Get phoneentryDoneDate.
     *
     * @return string
     */
    public function getPhoneentryDoneDate()
    {
        return $this->phoneentryDoneDate;
    }

    /**
     * Set phoneentryDoneFrom.
     *
     * @param string $phoneentryDoneFrom
     *
     * @return Customer
     */
    public function setPhoneentryDoneFrom($phoneentryDoneFrom)
    {
        $this->phoneentryDoneFrom = $phoneentryDoneFrom;

        return $this;
    }

    /**
     * Get phoneentryDoneFrom.
     *
     * @return string
     */
    public function getPhoneentryDoneFrom()
    {
        return $this->phoneentryDoneFrom;
    }

    /**
     * Set phonenumber.
     *
     * @param string $phonenumber
     *
     * @return Customer
     */
    public function setPhonenumber($phonenumber)
    {
        $this->phonenumber = $phonenumber;

        return $this;
    }

    /**
     * Get phonenumber.
     *
     * @return string
     */
    public function getPhonenumber()
    {
        return $this->phonenumber;
    }

    /**
     * Set portdefaultComment.
     *
     * @param string $portdefaultComment
     *
     * @return Customer
     */
    public function setPortdefaultComment($portdefaultComment)
    {
        $this->portdefaultComment = $portdefaultComment;

        return $this;
    }

    /**
     * Get portdefaultComment.
     *
     * @return string
     */
    public function getPortdefaultComment()
    {
        return $this->portdefaultComment;
    }

    /**
     * Set porting.
     *
     * @param string $porting
     *
     * @return Customer
     */
    public function setPorting($porting)
    {
        $this->porting = $porting;

        return $this;
    }

    /**
     * Get porting.
     *
     * @return string
     */
    public function getPorting()
    {
        return $this->porting;
    }

    /**
     * Set portingText.
     *
     * @param string $portingText
     *
     * @return Customer
     */
    public function setPortingText($portingText)
    {
        $this->portingText = $portingText;

        return $this;
    }

    /**
     * Get portingText.
     *
     * @return string
     */
    public function getPortingText()
    {
        return $this->portingText;
    }

    /**
     * Set pppoeConfigDate.
     *
     * @param string $pppoeConfigDate
     *
     * @return Customer
     */
    public function setPppoeConfigDate($pppoeConfigDate)
    {
        $this->pppoeConfigDate = $pppoeConfigDate;

        return $this;
    }

    /**
     * Get pppoeConfigDate.
     *
     * @return string
     */
    public function getPppoeConfigDate()
    {
        return $this->pppoeConfigDate;
    }

    /**
     * Set productname.
     *
     * @param string $productname
     *
     * @return Customer
     */
    public function setProductname($productname)
    {
        $this->productname = $productname;

        return $this;
    }

    /**
     * Get productname.
     *
     * @return string
     */
    public function getProductname()
    {
        return $this->productname;
    }

    /**
     * Set prospectSupplyStatus.
     *
     * @param string $prospectSupplyStatus
     *
     * @return Customer
     */
    public function setProspectSupplyStatus($prospectSupplyStatus)
    {
        $this->prospectSupplyStatus = $prospectSupplyStatus;

        return $this;
    }

    /**
     * Get prospectSupplyStatus.
     *
     * @return string
     */
    public function getProspectSupplyStatus()
    {
        return $this->prospectSupplyStatus;
    }

    /**
     * Set purtelBluckageFullfiledDate.
     *
     * @param string $purtelBluckageFullfiledDate
     *
     * @return Customer
     */
    public function setPurtelBluckageFullfiledDate($purtelBluckageFullfiledDate)
    {
        $this->purtelBluckageFullfiledDate = $purtelBluckageFullfiledDate;

        return $this;
    }

    /**
     * Get purtelBluckageFullfiledDate.
     *
     * @return string
     */
    public function getPurtelBluckageFullfiledDate()
    {
        return $this->purtelBluckageFullfiledDate;
    }

    /**
     * Set purtelBluckageSendedDate.
     *
     * @param string $purtelBluckageSendedDate
     *
     * @return Customer
     */
    public function setPurtelBluckageSendedDate($purtelBluckageSendedDate)
    {
        $this->purtelBluckageSendedDate = $purtelBluckageSendedDate;

        return $this;
    }

    /**
     * Get purtelBluckageSendedDate.
     *
     * @return string
     */
    public function getPurtelBluckageSendedDate()
    {
        return $this->purtelBluckageSendedDate;
    }

    /**
     * Set purtelCustomerRegDate.
     *
     * @param string $purtelCustomerRegDate
     *
     * @return Customer
     */
    public function setPurtelCustomerRegDate($purtelCustomerRegDate)
    {
        $this->purtelCustomerRegDate = $purtelCustomerRegDate;

        return $this;
    }

    /**
     * Get purtelCustomerRegDate.
     *
     * @return string
     */
    public function getPurtelCustomerRegDate()
    {
        return $this->purtelCustomerRegDate;
    }

    /**
     * Set purtelDataRecordId.
     *
     * @param string $purtelDataRecordId
     *
     * @return Customer
     */
    public function setPurtelDataRecordId($purtelDataRecordId)
    {
        $this->purtelDataRecordId = $purtelDataRecordId;

        return $this;
    }

    /**
     * Get purtelDataRecordId.
     *
     * @return string
     */
    public function getPurtelDataRecordId()
    {
        return $this->purtelDataRecordId;
    }

    /**
     * Set purtelEditDone.
     *
     * @param string $purtelEditDone
     *
     * @return Customer
     */
    public function setPurtelEditDone($purtelEditDone)
    {
        $this->purtelEditDone = $purtelEditDone;

        return $this;
    }

    /**
     * Get purtelEditDone.
     *
     * @return string
     */
    public function getPurtelEditDone()
    {
        return $this->purtelEditDone;
    }

    /**
     * Set purtelEditDoneFrom.
     *
     * @param string $purtelEditDoneFrom
     *
     * @return Customer
     */
    public function setPurtelEditDoneFrom($purtelEditDoneFrom)
    {
        $this->purtelEditDoneFrom = $purtelEditDoneFrom;

        return $this;
    }

    /**
     * Get purtelEditDoneFrom.
     *
     * @return string
     */
    public function getPurtelEditDoneFrom()
    {
        return $this->purtelEditDoneFrom;
    }

    /**
     * Set purtelLogin.
     *
     * @param string $purtelLogin
     *
     * @return Customer
     */
    public function setPurtelLogin($purtelLogin)
    {
        $this->purtelLogin = $purtelLogin;

        return $this;
    }

    /**
     * Get purtelLogin.
     *
     * @return string
     */
    public function getPurtelLogin()
    {
        return $this->purtelLogin;
    }

    /**
     * Set purtelPasswort.
     *
     * @param string $purtelPasswort
     *
     * @return Customer
     */
    public function setPurtelPasswort($purtelPasswort)
    {
        $this->purtelPasswort = $purtelPasswort;

        return $this;
    }

    /**
     * Get purtelPasswort.
     *
     * @return string
     */
    public function getPurtelPasswort()
    {
        return $this->purtelPasswort;
    }

    /**
     * Set purtelProduct.
     *
     * @param string $purtelProduct
     *
     * @return Customer
     */
    public function setPurtelProduct($purtelProduct)
    {
        $this->purtelProduct = $purtelProduct;

        return $this;
    }

    /**
     * Get purtelProduct.
     *
     * @return string
     */
    public function getPurtelProduct()
    {
        return $this->purtelProduct;
    }

    /**
     * Set purtelproductAdvanced.
     *
     * @param string $purtelproductAdvanced
     *
     * @return Customer
     */
    public function setPurtelproductAdvanced($purtelproductAdvanced)
    {
        $this->purtelproductAdvanced = $purtelproductAdvanced;

        return $this;
    }

    /**
     * Get purtelproductAdvanced.
     *
     * @return string
     */
    public function getPurtelproductAdvanced()
    {
        return $this->purtelproductAdvanced;
    }

    /**
     * Set rDNSCount.
     *
     * @param string $rDNSCount
     *
     * @return Customer
     */
    public function setRDNSCount($rDNSCount)
    {
        $this->rDNSCount = $rDNSCount;

        return $this;
    }

    /**
     * Get rDNSCount.
     *
     * @return string
     */
    public function getRDNSCount()
    {
        return $this->rDNSCount;
    }

    /**
     * Set rDNSInstallation.
     *
     * @param string $rDNSInstallation
     *
     * @return Customer
     */
    public function setRDNSInstallation($rDNSInstallation)
    {
        $this->rDNSInstallation = $rDNSInstallation;

        return $this;
    }

    /**
     * Get rDNSInstallation.
     *
     * @return string
     */
    public function getRDNSInstallation()
    {
        return $this->rDNSInstallation;
    }

    /**
     * Set reachedDownstream.
     *
     * @param string $reachedDownstream
     *
     * @return Customer
     */
    public function setReachedDownstream($reachedDownstream)
    {
        $this->reachedDownstream = $reachedDownstream;

        return $this;
    }

    /**
     * Get reachedDownstream.
     *
     * @return string
     */
    public function getReachedDownstream()
    {
        return $this->reachedDownstream;
    }

    /**
     * Set reachedUpstream.
     *
     * @param string $reachedUpstream
     *
     * @return Customer
     */
    public function setReachedUpstream($reachedUpstream)
    {
        $this->reachedUpstream = $reachedUpstream;

        return $this;
    }

    /**
     * Get reachedUpstream.
     *
     * @return string
     */
    public function getReachedUpstream()
    {
        return $this->reachedUpstream;
    }

    /**
     * Set recommendedProduct.
     *
     * @param string $recommendedProduct
     *
     * @return Customer
     */
    public function setRecommendedProduct($recommendedProduct)
    {
        $this->recommendedProduct = $recommendedProduct;

        return $this;
    }

    /**
     * Get recommendedProduct.
     *
     * @return string
     */
    public function getRecommendedProduct()
    {
        return $this->recommendedProduct;
    }

    /**
     * Set recruitedBy.
     *
     * @param string $recruitedBy
     *
     * @return Customer
     */
    public function setRecruitedBy($recruitedBy)
    {
        $this->recruitedBy = $recruitedBy;

        return $this;
    }

    /**
     * Get recruitedBy.
     *
     * @return string
     */
    public function getRecruitedBy()
    {
        return $this->recruitedBy;
    }

    /**
     * Set regAnswerDate.
     *
     * @param string $regAnswerDate
     *
     * @return Customer
     */
    public function setRegAnswerDate($regAnswerDate)
    {
        $this->regAnswerDate = $regAnswerDate;

        return $this;
    }

    /**
     * Get regAnswerDate.
     *
     * @return string
     */
    public function getRegAnswerDate()
    {
        return $this->regAnswerDate;
    }

    /**
     * Set registrationDate.
     *
     * @param string $registrationDate
     *
     * @return Customer
     */
    public function setRegistrationDate($registrationDate)
    {
        $this->registrationDate = $registrationDate;

        return $this;
    }

    /**
     * Get registrationDate.
     *
     * @return string
     */
    public function getRegistrationDate()
    {
        return $this->registrationDate;
    }

    /**
     * Set contractOnlineDate.
     *
     * @param string $contractOnlineDate
     *
     * @return Customer
     */
    public function setContractOnlineDate($contractOnlineDate)
    {
        $this->contractOnlineDate = $contractOnlineDate;

        return $this;
    }

    /**
     * Get contractOnlineDate.
     *
     * @return string
     */
    public function getContractOnlineDate()
    {
        return $this->contractOnlineDate;
    }

    /**
     * Set remoteLogin.
     *
     * @param string $remoteLogin
     *
     * @return Customer
     */
    public function setRemoteLogin($remoteLogin)
    {
        $this->remoteLogin = $remoteLogin;

        return $this;
    }

    /**
     * Get remoteLogin.
     *
     * @return string
     */
    public function getRemoteLogin()
    {
        return $this->remoteLogin;
    }

    /**
     * Set remotePassword.
     *
     * @param string $remotePassword
     *
     * @return Customer
     */
    public function setRemotePassword($remotePassword)
    {
        $this->remotePassword = $remotePassword;

        return $this;
    }

    /**
     * Get remotePassword.
     *
     * @return string
     */
    public function getRemotePassword()
    {
        return $this->remotePassword;
    }

    /**
     * Set routingActive.
     *
     * @param string $routingActive
     *
     * @return Customer
     */
    public function setRoutingActive($routingActive)
    {
        $this->routingActive = $routingActive;

        return $this;
    }

    /**
     * Get routingActive.
     *
     * @return string
     */
    public function getRoutingActive()
    {
        return $this->routingActive;
    }

    /**
     * Set routingActiveDate.
     *
     * @param string $routingActiveDate
     *
     * @return Customer
     */
    public function setRoutingActiveDate($routingActiveDate)
    {
        $this->routingActiveDate = $routingActiveDate;

        return $this;
    }

    /**
     * Get routingActiveDate.
     *
     * @return string
     */
    public function getRoutingActiveDate()
    {
        return $this->routingActiveDate;
    }

    /**
     * Set routingDeleted.
     *
     * @param string $routingDeleted
     *
     * @return Customer
     */
    public function setRoutingDeleted($routingDeleted)
    {
        $this->routingDeleted = $routingDeleted;

        return $this;
    }

    /**
     * Get routingDeleted.
     *
     * @return string
     */
    public function getRoutingDeleted()
    {
        return $this->routingDeleted;
    }

    /**
     * Set routingWishDate.
     *
     * @param string $routingWishDate
     *
     * @return Customer
     */
    public function setRoutingWishDate($routingWishDate)
    {
        $this->routingWishDate = $routingWishDate;

        return $this;
    }

    /**
     * Get routingWishDate.
     *
     * @return string
     */
    public function getRoutingWishDate()
    {
        return $this->routingWishDate;
    }

    /**
     * Set secondTal.
     *
     * @param string $secondTal
     *
     * @return Customer
     */
    public function setSecondTal($secondTal)
    {
        $this->secondTal = $secondTal;

        return $this;
    }

    /**
     * Get secondTal.
     *
     * @return string
     */
    public function getSecondTal()
    {
        return $this->secondTal;
    }

    /**
     * Set service.
     *
     * @param string $service
     *
     * @return Customer
     */
    public function setService($service)
    {
        $this->Service = $service;

        return $this;
    }

    /**
     * Get service.
     *
     * @return string
     */
    public function getService()
    {
        return $this->Service;
    }

    /**
     * Set serviceLevel.
     *
     * @param string $serviceLevel
     *
     * @return Customer
     */
    public function setServiceLevel($serviceLevel)
    {
        $this->serviceLevel = $serviceLevel;

        return $this;
    }

    /**
     * Get serviceLevel.
     *
     * @return string
     */
    public function getServiceLevel()
    {
        return $this->serviceLevel;
    }

    /**
     * Set serviceTechnician.
     *
     * @param string $serviceTechnician
     *
     * @return Customer
     */
    public function setServiceTechnician($serviceTechnician)
    {
        $this->serviceTechnician = $serviceTechnician;

        return $this;
    }

    /**
     * Get serviceTechnician.
     *
     * @return string
     */
    public function getServiceTechnician()
    {
        return $this->serviceTechnician;
    }

    /**
     * Set sipAccounts.
     *
     * @param string $sipAccounts
     *
     * @return Customer
     */
    public function setSipAccounts($sipAccounts)
    {
        $this->sipAccounts = $sipAccounts;

        return $this;
    }

    /**
     * Get sipAccounts.
     *
     * @return string
     */
    public function getSipAccounts()
    {
        return $this->sipAccounts;
    }

    /**
     * Set sofortdsl.
     *
     * @param string $sofortdsl
     *
     * @return Customer
     */
    public function setSofortdsl($sofortdsl)
    {
        $this->sofortdsl = $sofortdsl;

        return $this;
    }

    /**
     * Get sofortdsl.
     *
     * @return string
     */
    public function getSofortdsl()
    {
        return $this->sofortdsl;
    }

    /**
     * Set specialConditionsFrom.
     *
     * @param string $specialConditionsFrom
     *
     * @return Customer
     */
    public function setSpecialConditionsFrom($specialConditionsFrom)
    {
        $this->specialConditionsFrom = $specialConditionsFrom;

        return $this;
    }

    /**
     * Get specialConditionsFrom.
     *
     * @return string
     */
    public function getSpecialConditionsFrom()
    {
        return $this->specialConditionsFrom;
    }

    /**
     * Set specialConditionsText.
     *
     * @param string $specialConditionsText
     *
     * @return Customer
     */
    public function setSpecialConditionsText($specialConditionsText)
    {
        $this->specialConditionsText = $specialConditionsText;

        return $this;
    }

    /**
     * Get specialConditionsText.
     *
     * @return string
     */
    public function getSpecialConditionsText()
    {
        return $this->specialConditionsText;
    }

    /**
     * Set specialConditionsTill.
     *
     * @param string $specialConditionsTill
     *
     * @return Customer
     */
    public function setSpecialConditionsTill($specialConditionsTill)
    {
        $this->specialConditionsTill = $specialConditionsTill;

        return $this;
    }

    /**
     * Get specialConditionsTill.
     *
     * @return string
     */
    public function getSpecialConditionsTill()
    {
        return $this->specialConditionsTill;
    }

    /**
     * Set wisocontractCancelInputDate.
     *
     * @param string $wisocontractCancelInputDate
     *
     * @return Customer
     */
    public function setWisocontractCancelInputDate($wisocontractCancelInputDate)
    {
        $this->wisocontractCancelInputDate = $wisocontractCancelInputDate;

        return $this;
    }

    /**
     * Get wisocontractCancelInputDate.
     *
     * @return string
     */
    public function getWisocontractCancelInputDate()
    {
        return $this->wisocontractCancelInputDate;
    }

    /**
     * Set wisocontractCancelInputAck.
     *
     * @param string $wisocontractCancelInputAck
     *
     * @return Customer
     */
    public function setWisocontractCancelInputAck($wisocontractCancelInputAck)
    {
        $this->wisocontractCancelInputAck = $wisocontractCancelInputAck;

        return $this;
    }

    /**
     * Get wisocontractCancelInputAck.
     *
     * @return string
     */
    public function getWisocontractCancelInputAck()
    {
        return $this->wisocontractCancelInputAck;
    }

    /**
     * Set wisocontractCancelAccountDate.
     *
     * @param string $wisocontractCancelAccountDate
     *
     * @return Customer
     */
    public function setWisocontractCancelAccountDate($wisocontractCancelAccountDate)
    {
        $this->wisocontractCancelAccountDate = $wisocontractCancelAccountDate;

        return $this;
    }

    /**
     * Get wisocontractCancelAccountDate.
     *
     * @return string
     */
    public function getWisocontractCancelAccountDate()
    {
        return $this->wisocontractCancelAccountDate;
    }

    /**
     * Set wisocontractCancelAccountFinish.
     *
     * @param string $wisocontractCancelAccountFinish
     *
     * @return Customer
     */
    public function setWisocontractCancelAccountFinish($wisocontractCancelAccountFinish)
    {
        $this->wisocontractCancelAccountFinish = $wisocontractCancelAccountFinish;

        return $this;
    }

    /**
     * Get wisocontractCancelAccountFinish.
     *
     * @return string
     */
    public function getWisocontractCancelAccountFinish()
    {
        return $this->wisocontractCancelAccountFinish;
    }

    /**
     * Set specialTerminationInternet.
     *
     * @param string $specialTerminationInternet
     *
     * @return Customer
     */
    public function setSpecialTerminationInternet($specialTerminationInternet)
    {
        $this->specialTerminationInternet = $specialTerminationInternet;

        return $this;
    }

    /**
     * Get specialTerminationInternet.
     *
     * @return string
     */
    public function getSpecialTerminationInternet()
    {
        return $this->specialTerminationInternet;
    }

    /**
     * Set specialTerminationPhone.
     *
     * @param string $specialTerminationPhone
     *
     * @return Customer
     */
    public function setSpecialTerminationPhone($specialTerminationPhone)
    {
        $this->specialTerminationPhone = $specialTerminationPhone;

        return $this;
    }

    /**
     * Get specialTerminationPhone.
     *
     * @return string
     */
    public function getSpecialTerminationPhone()
    {
        return $this->specialTerminationPhone;
    }

    /**
     * Set spectrumprofile.
     *
     * @param string $spectrumprofile
     *
     * @return Customer
     */
    public function setSpectrumprofile($spectrumprofile)
    {
        $this->Spectrumprofile = $spectrumprofile;

        return $this;
    }

    /**
     * Get spectrumprofile.
     *
     * @return string
     */
    public function getSpectrumprofile()
    {
        return $this->Spectrumprofile;
    }

    /**
     * Set statiOldcontractComment.
     *
     * @param string $statiOldcontractComment
     *
     * @return Customer
     */
    public function setStatiOldcontractComment($statiOldcontractComment)
    {
        $this->statiOldcontractComment = $statiOldcontractComment;

        return $this;
    }

    /**
     * Get statiOldcontractComment.
     *
     * @return string
     */
    public function getStatiOldcontractComment()
    {
        return $this->statiOldcontractComment;
    }

    /**
     * Set statiPortConfirmDate.
     *
     * @param string $statiPortConfirmDate
     *
     * @return Customer
     */
    public function setStatiPortConfirmDate($statiPortConfirmDate)
    {
        $this->statiPortConfirmDate = $statiPortConfirmDate;

        return $this;
    }

    /**
     * Get statiPortConfirmDate.
     *
     * @return string
     */
    public function getStatiPortConfirmDate()
    {
        return $this->statiPortConfirmDate;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return Customer
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set stnzFb.
     *
     * @param string $stnzFb
     *
     * @return Customer
     */
    public function setStnzFb($stnzFb)
    {
        $this->stnzFb = $stnzFb;

        return $this;
    }

    /**
     * Get stnzFb.
     *
     * @return string
     */
    public function getStnzFb()
    {
        return $this->stnzFb;
    }

    /**
     * Set street.
     *
     * @param string $street
     *
     * @return Customer
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street.
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set streetno.
     *
     * @param string $streetno
     *
     * @return Customer
     */
    public function setStreetno($streetno)
    {
        $this->streetno = $streetno;

        return $this;
    }

    /**
     * Get streetno.
     *
     * @return string
     */
    public function getStreetno()
    {
        return $this->streetno;
    }

    /**
     * Set subnetmask.
     *
     * @param string $subnetmask
     *
     * @return Customer
     */
    public function setSubnetmask($subnetmask)
    {
        $this->subnetmask = $subnetmask;

        return $this;
    }

    /**
     * Get subnetmask.
     *
     * @return string
     */
    public function getSubnetmask()
    {
        return $this->subnetmask;
    }

    /**
     * Set talAssignedPhoneno.
     *
     * @param string $talAssignedPhoneno
     *
     * @return Customer
     */
    public function setTalAssignedPhoneno($talAssignedPhoneno)
    {
        $this->talAssignedPhoneno = $talAssignedPhoneno;

        return $this;
    }

    /**
     * Get talAssignedPhoneno.
     *
     * @return string
     */
    public function getTalAssignedPhoneno()
    {
        return $this->talAssignedPhoneno;
    }

    /**
     * Set talCancelAckDate.
     *
     * @param string $talCancelAckDate
     *
     * @return Customer
     */
    public function setTalCancelAckDate($talCancelAckDate)
    {
        $this->talCancelAckDate = $talCancelAckDate;

        return $this;
    }

    /**
     * Get talCancelAckDate.
     *
     * @return string
     */
    public function getTalCancelAckDate()
    {
        return $this->talCancelAckDate;
    }

    /**
     * Set talCancelDate.
     *
     * @param string $talCancelDate
     *
     * @return Customer
     */
    public function setTalCancelDate($talCancelDate)
    {
        $this->talCancelDate = $talCancelDate;

        return $this;
    }

    /**
     * Get talCancelDate.
     *
     * @return string
     */
    public function getTalCancelDate()
    {
        return $this->talCancelDate;
    }

    /**
     * Set talCanceledForDate.
     *
     * @param string $talCanceledForDate
     *
     * @return Customer
     */
    public function setTalCanceledForDate($talCanceledForDate)
    {
        $this->talCanceledForDate = $talCanceledForDate;

        return $this;
    }

    /**
     * Get talCanceledForDate.
     *
     * @return string
     */
    public function getTalCanceledForDate()
    {
        return $this->talCanceledForDate;
    }

    /**
     * Set talCanceledFrom.
     *
     * @param string $talCanceledFrom
     *
     * @return Customer
     */
    public function setTalCanceledFrom($talCanceledFrom)
    {
        $this->talCanceledFrom = $talCanceledFrom;

        return $this;
    }

    /**
     * Get talCanceledFrom.
     *
     * @return string
     */
    public function getTalCanceledFrom()
    {
        return $this->talCanceledFrom;
    }

    /**
     * Set talDtagAssignmentNo.
     *
     * @param string $talDtagAssignmentNo
     *
     * @return Customer
     */
    public function setTalDtagAssignmentNo($talDtagAssignmentNo)
    {
        $this->talDtagAssignmentNo = $talDtagAssignmentNo;

        return $this;
    }

    /**
     * Get talDtagAssignmentNo.
     *
     * @return string
     */
    public function getTalDtagAssignmentNo()
    {
        return $this->talDtagAssignmentNo;
    }

    /**
     * Set talDtagRevisor.
     *
     * @param string $talDtagRevisor
     *
     * @return Customer
     */
    public function setTalDtagRevisor($talDtagRevisor)
    {
        $this->talDtagRevisor = $talDtagRevisor;

        return $this;
    }

    /**
     * Get talDtagRevisor.
     *
     * @return string
     */
    public function getTalDtagRevisor()
    {
        return $this->talDtagRevisor;
    }

    /**
     * Set talLendedFritzbox.
     *
     * @param string $talLendedFritzbox
     *
     * @return Customer
     */
    public function setTalLendedFritzbox($talLendedFritzbox)
    {
        $this->talLendedFritzbox = $talLendedFritzbox;

        return $this;
    }

    /**
     * Get talLendedFritzbox.
     *
     * @return string
     */
    public function getTalLendedFritzbox()
    {
        return $this->talLendedFritzbox;
    }

    /**
     * Set talOrder.
     *
     * @param string $talOrder
     *
     * @return Customer
     */
    public function setTalOrder($talOrder)
    {
        $this->talOrder = $talOrder;

        return $this;
    }

    /**
     * Get talOrder.
     *
     * @return string
     */
    public function getTalOrder()
    {
        return $this->talOrder;
    }

    /**
     * Set talOrderAckDate.
     *
     * @param string $talOrderAckDate
     *
     * @return Customer
     */
    public function setTalOrderAckDate($talOrderAckDate)
    {
        $this->talOrderAckDate = $talOrderAckDate;

        return $this;
    }

    /**
     * Get talOrderAckDate.
     *
     * @return string
     */
    public function getTalOrderAckDate()
    {
        return $this->talOrderAckDate;
    }

    /**
     * Set talOrderDate.
     *
     * @param string $talOrderDate
     *
     * @return Customer
     */
    public function setTalOrderDate($talOrderDate)
    {
        $this->talOrderDate = $talOrderDate;

        return $this;
    }

    /**
     * Get talOrderDate.
     *
     * @return string
     */
    public function getTalOrderDate()
    {
        return $this->talOrderDate;
    }

    /**
     * Set talOrderWork.
     *
     * @param string $talOrderWork
     *
     * @return Customer
     */
    public function setTalOrderWork($talOrderWork)
    {
        $this->talOrderWork = $talOrderWork;

        return $this;
    }

    /**
     * Get talOrderWork.
     *
     * @return string
     */
    public function getTalOrderWork()
    {
        return $this->talOrderWork;
    }

    /**
     * Set talOrderdFrom.
     *
     * @param string $talOrderdFrom
     *
     * @return Customer
     */
    public function setTalOrderdFrom($talOrderdFrom)
    {
        $this->talOrderdFrom = $talOrderdFrom;

        return $this;
    }

    /**
     * Get talOrderdFrom.
     *
     * @return string
     */
    public function getTalOrderdFrom()
    {
        return $this->talOrderdFrom;
    }

    /**
     * Set talOrderedForDate.
     *
     * @param string $talOrderedForDate
     *
     * @return Customer
     */
    public function setTalOrderedForDate($talOrderedForDate)
    {
        $this->talOrderedForDate = $talOrderedForDate;

        return $this;
    }

    /**
     * Get talOrderedForDate.
     *
     * @return string
     */
    public function getTalOrderedForDate()
    {
        return $this->talOrderedForDate;
    }

    /**
     * Set talProduct.
     *
     * @param string $talProduct
     *
     * @return Customer
     */
    public function setTalProduct($talProduct)
    {
        $this->talProduct = $talProduct;

        return $this;
    }

    /**
     * Get talProduct.
     *
     * @return string
     */
    public function getTalProduct()
    {
        return $this->talProduct;
    }

    /**
     * Set talReleasingOperator.
     *
     * @param string $talReleasingOperator
     *
     * @return Customer
     */
    public function setTalReleasingOperator($talReleasingOperator)
    {
        $this->talReleasingOperator = $talReleasingOperator;

        return $this;
    }

    /**
     * Get talReleasingOperator.
     *
     * @return string
     */
    public function getTalReleasingOperator()
    {
        return $this->talReleasingOperator;
    }

    /**
     * Set talorderText.
     *
     * @param string $talorderText
     *
     * @return Customer
     */
    public function setTalorderText($talorderText)
    {
        $this->talorderText = $talorderText;

        return $this;
    }

    /**
     * Get talorderText.
     *
     * @return string
     */
    public function getTalorderText()
    {
        return $this->talorderText;
    }

    /**
     * Set techdataComment.
     *
     * @param string $techdataComment
     *
     * @return Customer
     */
    public function setTechdataComment($techdataComment)
    {
        $this->techdataComment = $techdataComment;

        return $this;
    }

    /**
     * Get techdataComment.
     *
     * @return string
     */
    public function getTechdataComment()
    {
        return $this->techdataComment;
    }

    /**
     * Set techdataStatus.
     *
     * @param string $techdataStatus
     *
     * @return Customer
     */
    public function setTechdataStatus($techdataStatus)
    {
        $this->techdataStatus = $techdataStatus;

        return $this;
    }

    /**
     * Get techdataStatus.
     *
     * @return string
     */
    public function getTechdataStatus()
    {
        return $this->techdataStatus;
    }

    /**
     * Set telefonbuchEintrag.
     *
     * @param string $telefonbuchEintrag
     *
     * @return Customer
     */
    public function setTelefonbuchEintrag($telefonbuchEintrag)
    {
        $this->telefonbuchEintrag = $telefonbuchEintrag;

        return $this;
    }

    /**
     * Get telefonbuchEintrag.
     *
     * @return string
     */
    public function getTelefonbuchEintrag()
    {
        return $this->telefonbuchEintrag;
    }

    /**
     * Set terminalReady.
     *
     * @param string $terminalReady
     *
     * @return Customer
     */
    public function setTerminalReady($terminalReady)
    {
        $this->terminalReady = $terminalReady;

        return $this;
    }

    /**
     * Get terminalReady.
     *
     * @return string
     */
    public function getTerminalReady()
    {
        return $this->terminalReady;
    }

    /**
     * Set terminalReadyFrom.
     *
     * @param string $terminalReadyFrom
     *
     * @return Customer
     */
    public function setTerminalReadyFrom($terminalReadyFrom)
    {
        $this->terminalReadyFrom = $terminalReadyFrom;

        return $this;
    }

    /**
     * Get terminalReadyFrom.
     *
     * @return string
     */
    public function getTerminalReadyFrom()
    {
        return $this->terminalReadyFrom;
    }

    /**
     * Set terminalSendedDate.
     *
     * @param string $terminalSendedDate
     *
     * @return Customer
     */
    public function setTerminalSendedDate($terminalSendedDate)
    {
        $this->terminalSendedDate = $terminalSendedDate;

        return $this;
    }

    /**
     * Get terminalSendedDate.
     *
     * @return string
     */
    public function getTerminalSendedDate()
    {
        return $this->terminalSendedDate;
    }

    /**
     * Set terminalSendedFrom.
     *
     * @param string $terminalSendedFrom
     *
     * @return Customer
     */
    public function setTerminalSendedFrom($terminalSendedFrom)
    {
        $this->terminalSendedFrom = $terminalSendedFrom;

        return $this;
    }

    /**
     * Get terminalSendedFrom.
     *
     * @return string
     */
    public function getTerminalSendedFrom()
    {
        return $this->terminalSendedFrom;
    }

    /**
     * Set terminalSerialno.
     *
     * @param string $terminalSerialno
     *
     * @return Customer
     */
    public function setTerminalSerialno($terminalSerialno)
    {
        $this->terminalSerialno = $terminalSerialno;

        return $this;
    }

    /**
     * Get terminalSerialno.
     *
     * @return string
     */
    public function getTerminalSerialno()
    {
        return $this->terminalSerialno;
    }

    /**
     * Set terminalType.
     *
     * @param string $terminalType
     *
     * @return Customer
     */
    public function setTerminalType($terminalType)
    {
        $this->terminalType = $terminalType;

        return $this;
    }

    /**
     * Get terminalType.
     *
     * @return string
     */
    public function getTerminalType()
    {
        return $this->terminalType;
    }

    /**
     * Set terminalTypeExists.
     *
     * @param string $terminalTypeExists
     *
     * @return Customer
     */
    public function setTerminalTypeExists($terminalTypeExists)
    {
        $this->terminalTypeExists = $terminalTypeExists;

        return $this;
    }

    /**
     * Get terminalTypeExists.
     *
     * @return string
     */
    public function getTerminalTypeExists()
    {
        return $this->terminalTypeExists;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Customer
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set tvService.
     *
     * @param string $tvService
     *
     * @return Customer
     */
    public function setTvService($tvService)
    {
        $this->tvService = $tvService;

        return $this;
    }

    /**
     * Get tvService.
     *
     * @return string
     */
    public function getTvService()
    {
        return $this->tvService;
    }

    /**
     * Set tvs.
     *
     * @param string $tvs
     *
     * @return Customer
     */
    public function setTvs($tvs)
    {
        $this->tvs = $tvs;

        return $this;
    }

    /**
     * Get tvs.
     *
     * @return string
     */
    public function getTvs()
    {
        return $this->tvs;
    }

    /**
     * Set tvsDate.
     *
     * @param string $tvsDate
     *
     * @return Customer
     */
    public function setTvsDate($tvsDate)
    {
        $this->tvsDate = $tvsDate;

        return $this;
    }

    /**
     * Get tvsDate.
     *
     * @return string
     */
    public function getTvsDate()
    {
        return $this->tvsDate;
    }

    /**
     * Set tvsFrom.
     *
     * @param string $tvsFrom
     *
     * @return Customer
     */
    public function setTvsFrom($tvsFrom)
    {
        $this->tvsFrom = $tvsFrom;

        return $this;
    }

    /**
     * Get tvsFrom.
     *
     * @return string
     */
    public function getTvsFrom()
    {
        return $this->tvsFrom;
    }

    /**
     * Set tvsTo.
     *
     * @param string $tvsTo
     *
     * @return Customer
     */
    public function setTvsTo($tvsTo)
    {
        $this->tvsTo = $tvsTo;

        return $this;
    }

    /**
     * Get tvsTo.
     *
     * @return string
     */
    public function getTvsTo()
    {
        return $this->tvsTo;
    }

    /**
     * Set venteloConfirmationDate.
     *
     * @param string $venteloConfirmationDate
     *
     * @return Customer
     */
    public function setVenteloConfirmationDate($venteloConfirmationDate)
    {
        $this->venteloConfirmationDate = $venteloConfirmationDate;

        return $this;
    }

    /**
     * Get venteloConfirmationDate.
     *
     * @return string
     */
    public function getVenteloConfirmationDate()
    {
        return $this->venteloConfirmationDate;
    }

    /**
     * Set venteloPortLetterDate.
     *
     * @param string $venteloPortLetterDate
     *
     * @return Customer
     */
    public function setVenteloPortLetterDate($venteloPortLetterDate)
    {
        $this->venteloPortLetterDate = $venteloPortLetterDate;

        return $this;
    }

    /**
     * Get venteloPortLetterDate.
     *
     * @return string
     */
    public function getVenteloPortLetterDate()
    {
        return $this->venteloPortLetterDate;
    }

    /**
     * Set venteloPortLetterFrom.
     *
     * @param string $venteloPortLetterFrom
     *
     * @return Customer
     */
    public function setVenteloPortLetterFrom($venteloPortLetterFrom)
    {
        $this->venteloPortLetterFrom = $venteloPortLetterFrom;

        return $this;
    }

    /**
     * Get venteloPortLetterFrom.
     *
     * @return string
     */
    public function getVenteloPortLetterFrom()
    {
        return $this->venteloPortLetterFrom;
    }

    /**
     * Set venteloPortLetterHow.
     *
     * @param string $venteloPortLetterHow
     *
     * @return Customer
     */
    public function setVenteloPortLetterHow($venteloPortLetterHow)
    {
        $this->venteloPortLetterHow = $venteloPortLetterHow;

        return $this;
    }

    /**
     * Get venteloPortLetterHow.
     *
     * @return string
     */
    public function getVenteloPortLetterHow()
    {
        return $this->venteloPortLetterHow;
    }

    /**
     * Set venteloPortWishDate.
     *
     * @param string $venteloPortWishDate
     *
     * @return Customer
     */
    public function setVenteloPortWishDate($venteloPortWishDate)
    {
        $this->venteloPortWishDate = $venteloPortWishDate;

        return $this;
    }

    /**
     * Get venteloPortWishDate.
     *
     * @return string
     */
    public function getVenteloPortWishDate()
    {
        return $this->venteloPortWishDate;
    }

    /**
     * Set venteloPurtelEnterDate.
     *
     * @param string $venteloPurtelEnterDate
     *
     * @return Customer
     */
    public function setVenteloPurtelEnterDate($venteloPurtelEnterDate)
    {
        $this->venteloPurtelEnterDate = $venteloPurtelEnterDate;

        return $this;
    }

    /**
     * Get venteloPurtelEnterDate.
     *
     * @return string
     */
    public function getVenteloPurtelEnterDate()
    {
        return $this->venteloPurtelEnterDate;
    }

    /**
     * Set venteloPurtelEnterFrom.
     *
     * @param string $venteloPurtelEnterFrom
     *
     * @return Customer
     */
    public function setVenteloPurtelEnterFrom($venteloPurtelEnterFrom)
    {
        $this->venteloPurtelEnterFrom = $venteloPurtelEnterFrom;

        return $this;
    }

    /**
     * Get venteloPurtelEnterFrom.
     *
     * @return string
     */
    public function getVenteloPurtelEnterFrom()
    {
        return $this->venteloPurtelEnterFrom;
    }

    /**
     * Set version.
     *
     * @param string $version
     *
     * @return Customer
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version.
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set vlanId.
     *
     * @param string $vlanId
     *
     * @return Customer
     */
    public function setVlanId($vlanId)
    {
        $this->vlanId = $vlanId;

        return $this;
    }

    /**
     * Get vlanId.
     *
     * @return string
     */
    public function getVlanId()
    {
        return $this->vlanId;
    }

    /**
     * Set wisocontractCanceledDate.
     *
     * @param string $wisocontractCanceledDate
     *
     * @return Customer
     */
    public function setWisocontractCanceledDate($wisocontractCanceledDate)
    {
        $this->wisocontractCanceledDate = $wisocontractCanceledDate;

        return $this;
    }

    /**
     * Get wisocontractCanceledDate.
     *
     * @return string
     */
    public function getWisocontractCanceledDate()
    {
        return $this->wisocontractCanceledDate;
    }

    /**
     * Set wisotelno.
     *
     * @param string $wisotelno
     *
     * @return Customer
     */
    public function setWisotelno($wisotelno)
    {
        $this->wisotelno = $wisotelno;

        return $this;
    }

    /**
     * Get wisotelno.
     *
     * @return string
     */
    public function getWisotelno()
    {
        return $this->wisotelno;
    }

    /**
     * Set zipcode.
     *
     * @param string $zipcode
     *
     * @return Customer
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode.
     *
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set wisocontractCancelExtra.
     *
     * @param string $wisocontractCancelExtra
     *
     * @return Customer
     */
    public function setWisocontractCancelExtra($wisocontractCancelExtra)
    {
        $this->wisocontractCancelExtra = $wisocontractCancelExtra;

        return $this;
    }

    /**
     * Get wisocontractCancelExtra.
     *
     * @return string
     */
    public function getWisocontractCancelExtra()
    {
        return $this->wisocontractCancelExtra;
    }

    /**
     * Set wisocontractCancelDate.
     *
     * @param string $wisocontractCancelDate
     *
     * @return Customer
     */
    public function setWisocontractCancelDate($wisocontractCancelDate)
    {
        $this->wisocontractCancelDate = $wisocontractCancelDate;

        return $this;
    }

    /**
     * Get wisocontractCancelDate.
     *
     * @return string
     */
    public function getWisocontractCancelDate()
    {
        return $this->wisocontractCancelDate;
    }

    /**
     * Set wisocontractCancelAck.
     *
     * @param string $wisocontractCancelAck
     *
     * @return Customer
     */
    public function setWisocontractCancelAck($wisocontractCancelAck)
    {
        $this->wisocontractCancelAck = $wisocontractCancelAck;

        return $this;
    }

    /**
     * Get wisocontractCancelAck.
     *
     * @return string
     */
    public function getWisocontractCancelAck()
    {
        return $this->wisocontractCancelAck;
    }

    /**
     * Set wisocontractRehire.
     *
     * @param string $wisocontractRehire
     *
     * @return Customer
     */
    public function setWisocontractRehire($wisocontractRehire)
    {
        $this->wisocontractRehire = $wisocontractRehire;

        return $this;
    }

    /**
     * Get wisocontractRehire.
     *
     * @return string
     */
    public function getWisocontractRehire()
    {
        return $this->wisocontractRehire;
    }

    /**
     * Set wisocontractRehirePerson.
     *
     * @param string $wisocontractRehirePerson
     *
     * @return Customer
     */
    public function setWisocontractRehirePerson($wisocontractRehirePerson)
    {
        $this->wisocontractRehirePerson = $wisocontractRehirePerson;

        return $this;
    }

    /**
     * Get wisocontractRehirePerson.
     *
     * @return string
     */
    public function getWisocontractRehirePerson()
    {
        return $this->wisocontractRehirePerson;
    }

    /**
     * Set wisocontractCanceledFrom.
     *
     * @param string $wisocontractCanceledFrom
     *
     * @return Customer
     */
    public function setWisocontractCanceledFrom($wisocontractCanceledFrom)
    {
        $this->wisocontractCanceledFrom = $wisocontractCanceledFrom;

        return $this;
    }

    /**
     * Get wisocontractCanceledFrom.
     *
     * @return string
     */
    public function getWisocontractCanceledFrom()
    {
        return $this->wisocontractCanceledFrom;
    }

    /**
     * Set wisocontractMove.
     *
     * @param string $wisocontractMove
     *
     * @return Customer
     */
    public function setWisocontractMove($wisocontractMove)
    {
        $this->wisocontractMove = $wisocontractMove;

        return $this;
    }

    /**
     * Get wisocontractMove.
     *
     * @return string
     */
    public function getWisocontractMove()
    {
        return $this->wisocontractMove;
    }

    /**
     * Set cancelComment.
     *
     * @param string $cancelComment
     *
     * @return Customer
     */
    public function setCancelComment($cancelComment)
    {
        $this->cancelComment = $cancelComment;

        return $this;
    }

    /**
     * Get cancelComment.
     *
     * @return string
     */
    public function getCancelComment()
    {
        return $this->cancelComment;
    }

    /**
     * Set newCity.
     *
     * @param string $newCity
     *
     * @return Customer
     */
    public function setNewCity($newCity)
    {
        $this->newCity = $newCity;

        return $this;
    }

    /**
     * Get newCity.
     *
     * @return string
     */
    public function getNewCity()
    {
        return $this->newCity;
    }

    /**
     * Set newZipcode.
     *
     * @param string $newZipcode
     *
     * @return Customer
     */
    public function setNewZipcode($newZipcode)
    {
        $this->newZipcode = $newZipcode;

        return $this;
    }

    /**
     * Get newZipcode.
     *
     * @return string
     */
    public function getNewZipcode()
    {
        return $this->newZipcode;
    }

    /**
     * Set newStreet.
     *
     * @param string $newStreet
     *
     * @return Customer
     */
    public function setNewStreet($newStreet)
    {
        $this->newStreet = $newStreet;

        return $this;
    }

    /**
     * Get newStreet.
     *
     * @return string
     */
    public function getNewStreet()
    {
        return $this->newStreet;
    }

    /**
     * Set newStreetno.
     *
     * @param string $newStreetno
     *
     * @return Customer
     */
    public function setNewStreetno($newStreetno)
    {
        $this->newStreetno = $newStreetno;

        return $this;
    }

    /**
     * Get newStreetno.
     *
     * @return string
     */
    public function getNewStreetno()
    {
        return $this->newStreetno;
    }

    /**
     * Set getHardwareDate.
     *
     * @param string $getHardwareDate
     *
     * @return Customer
     */
    public function setGetHardwareDate($getHardwareDate)
    {
        $this->getHardwareDate = $getHardwareDate;

        return $this;
    }

    /**
     * Get getHardwareDate.
     *
     * @return string
     */
    public function getGetHardwareDate()
    {
        return $this->getHardwareDate;
    }

    /**
     * Set payHardwareDate.
     *
     * @param string $payHardwareDate
     *
     * @return Customer
     */
    public function setPayHardwareDate($payHardwareDate)
    {
        $this->payHardwareDate = $payHardwareDate;

        return $this;
    }

    /**
     * Get payHardwareDate.
     *
     * @return string
     */
    public function getPayHardwareDate()
    {
        return $this->payHardwareDate;
    }

    /**
     * Set moveRegistrationAck.
     *
     * @param string $moveRegistrationAck
     *
     * @return Customer
     */
    public function setMoveRegistrationAck($moveRegistrationAck)
    {
        $this->moveRegistrationAck = $moveRegistrationAck;

        return $this;
    }

    /**
     * Get moveRegistrationAck.
     *
     * @return string
     */
    public function getMoveRegistrationAck()
    {
        return $this->moveRegistrationAck;
    }

    /**
     * Set wisocontractSwitchoff.
     *
     * @param string $wisocontractSwitchoff
     *
     * @return Customer
     */
    public function setWisocontractSwitchoff($wisocontractSwitchoff)
    {
        $this->wisocontractSwitchoff = $wisocontractSwitchoff;

        return $this;
    }

    /**
     * Get wisocontractSwitchoff.
     *
     * @return string
     */
    public function getWisocontractSwitchoff()
    {
        return $this->wisocontractSwitchoff;
    }

    /**
     * Set wisocontractSwitchoffFinish.
     *
     * @param string $wisocontractSwitchoffFinish
     *
     * @return Customer
     */
    public function setWisocontractSwitchoffFinish($wisocontractSwitchoffFinish)
    {
        $this->wisocontractSwitchoffFinish = $wisocontractSwitchoffFinish;

        return $this;
    }

    /**
     * Get wisocontractSwitchoffFinish.
     *
     * @return string
     */
    public function getWisocontractSwitchoffFinish()
    {
        return $this->wisocontractSwitchoffFinish;
    }

    /**
     * Set serviceLevelSend.
     *
     * @param string $serviceLevelSend
     *
     * @return Customer
     */
    public function setServiceLevelSend($serviceLevelSend)
    {
        $this->serviceLevelSend = $serviceLevelSend;

        return $this;
    }

    /**
     * Get serviceLevelSend.
     *
     * @return string
     */
    public function getServiceLevelSend()
    {
        return $this->serviceLevelSend;
    }

    /**
     * Set cudaid.
     *
     * @param string $cudaid
     *
     * @return Customer
     */
    public function setCudaid($cudaid)
    {
        $this->cudaid = $cudaid;

        return $this;
    }

    /**
     * Get cudaid.
     *
     * @return string
     */
    public function getCudaid()
    {
        return $this->cudaid;
    }

    /**
     * Set wbciId.
     *
     * @param string $wbciId
     *
     * @return Customer
     */
    public function setWbciId($wbciId)
    {
        $this->wbciId = $wbciId;

        return $this;
    }

    /**
     * Get wbciId.
     *
     * @return string
     */
    public function getWbciId()
    {
        return $this->wbciId;
    }

    /**
     * Set wbciEkpabg.
     *
     * @param string $wbciEkpabg
     *
     * @return Customer
     */
    public function setWbciEkpabg($wbciEkpabg)
    {
        $this->wbciEkpabg = $wbciEkpabg;

        return $this;
    }

    /**
     * Get wbciEkpabg.
     *
     * @return string
     */
    public function getWbciEkpabg()
    {
        return $this->wbciEkpabg;
    }

    /**
     * Set wbciGf.
     *
     * @param string $wbciGf
     *
     * @return Customer
     */
    public function setWbciGf($wbciGf)
    {
        $this->wbciGf = $wbciGf;

        return $this;
    }

    /**
     * Get wbciGf.
     *
     * @return string
     */
    public function getWbciGf()
    {
        return $this->wbciGf;
    }

    /**
     * Set wbciEingestelltAm.
     *
     * @param string $wbciEingestelltAm
     *
     * @return Customer
     */
    public function setWbciEingestelltAm($wbciEingestelltAm)
    {
        $this->wbciEingestelltAm = $wbciEingestelltAm;

        return $this;
    }

    /**
     * Get wbciEingestelltAm.
     *
     * @return string
     */
    public function getWbciEingestelltAm()
    {
        return $this->wbciEingestelltAm;
    }

    /**
     * Set wbciEingestelltVon.
     *
     * @param string $wbciEingestelltVon
     *
     * @return Customer
     */
    public function setWbciEingestelltVon($wbciEingestelltVon)
    {
        $this->wbciEingestelltVon = $wbciEingestelltVon;

        return $this;
    }

    /**
     * Get wbciEingestelltVon.
     *
     * @return string
     */
    public function getWbciEingestelltVon()
    {
        return $this->wbciEingestelltVon;
    }

    /**
     * Set wbciRuemvaAm.
     *
     * @param string $wbciRuemvaAm
     *
     * @return Customer
     */
    public function setWbciRuemvaAm($wbciRuemvaAm)
    {
        $this->wbciRuemvaAm = $wbciRuemvaAm;

        return $this;
    }

    /**
     * Get wbciRuemvaAm.
     *
     * @return string
     */
    public function getWbciRuemvaAm()
    {
        return $this->wbciRuemvaAm;
    }

    /**
     * Set wbciRuemvaVon.
     *
     * @param string $wbciRuemvaVon
     *
     * @return Customer
     */
    public function setWbciRuemvaVon($wbciRuemvaVon)
    {
        $this->wbciRuemvaVon = $wbciRuemvaVon;

        return $this;
    }

    /**
     * Get wbciRuemvaVon.
     *
     * @return string
     */
    public function getWbciRuemvaVon()
    {
        return $this->wbciRuemvaVon;
    }

    /**
     * Set wbciBestaetigtAm.
     *
     * @param string $wbciBestaetigtAm
     *
     * @return Customer
     */
    public function setWbciBestaetigtAm($wbciBestaetigtAm)
    {
        $this->wbciBestaetigtAm = $wbciBestaetigtAm;

        return $this;
    }

    /**
     * Get wbciBestaetigtAm.
     *
     * @return string
     */
    public function getWbciBestaetigtAm()
    {
        return $this->wbciBestaetigtAm;
    }

    /**
     * Set wbciBestaetigtVon.
     *
     * @param string $wbciBestaetigtVon
     *
     * @return Customer
     */
    public function setWbciBestaetigtVon($wbciBestaetigtVon)
    {
        $this->wbciBestaetigtVon = $wbciBestaetigtVon;

        return $this;
    }

    /**
     * Get wbciBestaetigtVon.
     *
     * @return string
     */
    public function getWbciBestaetigtVon()
    {
        return $this->wbciBestaetigtVon;
    }

    /**
     * Set wbciAkmtr.
     *
     * @param string $wbciAkmtr
     *
     * @return Customer
     */
    public function setWbciAkmtr($wbciAkmtr)
    {
        $this->wbciAkmtr = $wbciAkmtr;

        return $this;
    }

    /**
     * Get wbciAkmtr.
     *
     * @return string
     */
    public function getWbciAkmtr()
    {
        return $this->wbciAkmtr;
    }

    /**
     * Set wbciAkmtrAm.
     *
     * @param string $wbciAkmtrAm
     *
     * @return Customer
     */
    public function setWbciAkmtrAm($wbciAkmtrAm)
    {
        $this->wbciAkmtrAm = $wbciAkmtrAm;

        return $this;
    }

    /**
     * Get wbciAkmtrAm.
     *
     * @return string
     */
    public function getWbciAkmtrAm()
    {
        return $this->wbciAkmtrAm;
    }

    /**
     * Set wbciAkmtrVon.
     *
     * @param string $wbciAkmtrVon
     *
     * @return Customer
     */
    public function setWbciAkmtrVon($wbciAkmtrVon)
    {
        $this->wbciAkmtrVon = $wbciAkmtrVon;

        return $this;
    }

    /**
     * Get wbciAkmtrVon.
     *
     * @return string
     */
    public function getWbciAkmtrVon()
    {
        return $this->wbciAkmtrVon;
    }

    /**
     * Set wbciComment.
     *
     * @param string $wbciComment
     *
     * @return Customer
     */
    public function setWbciComment($wbciComment)
    {
        $this->wbciComment = $wbciComment;

        return $this;
    }

    /**
     * Get wbciComment.
     *
     * @return string
     */
    public function getWbciComment()
    {
        return $this->wbciComment;
    }

    /**
     * Set expDonePhone.
     *
     * @param string $expDonePhone
     *
     * @return Customer
     */
    public function setExpDonePhone($expDonePhone)
    {
        $this->expDonePhone = $expDonePhone;

        return $this;
    }

    /**
     * Get expDonePhone.
     *
     * @return string
     */
    public function getExpDonePhone()
    {
        return $this->expDonePhone;
    }

    /**
     * Set expDoneInt.
     *
     * @param string $expDoneInt
     *
     * @return Customer
     */
    public function setExpDoneInt($expDoneInt)
    {
        $this->expDoneInt = $expDoneInt;

        return $this;
    }

    /**
     * Get expDoneInt.
     *
     * @return string
     */
    public function getExpDoneInt()
    {
        return $this->expDoneInt;
    }

    /**
     * Set techFreeForDate.
     *
     * @param string $techFreeForDate
     *
     * @return Customer
     */
    public function setTechFreeForDate($techFreeForDate)
    {
        $this->techFreeForDate = $techFreeForDate;

        return $this;
    }

    /**
     * Get techFreeForDate.
     *
     * @return string
     */
    public function getTechFreeForDate()
    {
        return $this->techFreeForDate;
    }

    /**
     * Set techFreeDate.
     *
     * @param string $techFreeDate
     *
     * @return Customer
     */
    public function setTechFreeDate($techFreeDate)
    {
        $this->techFreeDate = $techFreeDate;

        return $this;
    }

    /**
     * Get techFreeDate.
     *
     * @return string
     */
    public function getTechFreeDate()
    {
        return $this->techFreeDate;
    }

    /**
     * Set purtelTelnr.
     *
     * @param string $purtelTelnr
     *
     * @return Customer
     */
    public function setPurtelTelnr($purtelTelnr)
    {
        $this->purtelTelnr = $purtelTelnr;

        return $this;
    }

    /**
     * Get purtelTelnr.
     *
     * @return string
     */
    public function getPurtelTelnr()
    {
        return $this->purtelTelnr;
    }

    /**
     * Set purtelUseVersion.
     *
     * @param string $purtelUseVersion
     *
     * @return Customer
     */
    public function setPurtelUseVersion($purtelUseVersion)
    {
        $this->purtelUseVersion = $purtelUseVersion;

        return $this;
    }

    /**
     * Get purtelUseVersion.
     *
     * @return string
     */
    public function getPurtelUseVersion()
    {
        return $this->purtelUseVersion;
    }

    /**
     * Set purtelLogin1.
     *
     * @param string $purtelLogin1
     *
     * @return Customer
     */
    public function setPurtelLogin1($purtelLogin1)
    {
        $this->purtelLogin1 = $purtelLogin1;

        return $this;
    }

    /**
     * Get purtelLogin1.
     *
     * @return string
     */
    public function getPurtelLogin1()
    {
        return $this->purtelLogin1;
    }

    /**
     * Set purtelPasswort1.
     *
     * @param string $purtelPasswort1
     *
     * @return Customer
     */
    public function setPurtelPasswort1($purtelPasswort1)
    {
        $this->purtelPasswort1 = $purtelPasswort1;

        return $this;
    }

    /**
     * Get purtelPasswort1.
     *
     * @return string
     */
    public function getPurtelPasswort1()
    {
        return $this->purtelPasswort1;
    }

    /**
     * Set purtelTelnr1.
     *
     * @param string $purtelTelnr1
     *
     * @return Customer
     */
    public function setPurtelTelnr1($purtelTelnr1)
    {
        $this->purtelTelnr1 = $purtelTelnr1;

        return $this;
    }

    /**
     * Get purtelTelnr1.
     *
     * @return string
     */
    public function getPurtelTelnr1()
    {
        return $this->purtelTelnr1;
    }

    /**
     * Set purtelLogin2.
     *
     * @param string $purtelLogin2
     *
     * @return Customer
     */
    public function setPurtelLogin2($purtelLogin2)
    {
        $this->purtelLogin2 = $purtelLogin2;

        return $this;
    }

    /**
     * Get purtelLogin2.
     *
     * @return string
     */
    public function getPurtelLogin2()
    {
        return $this->purtelLogin2;
    }

    /**
     * Set purtelPasswort2.
     *
     * @param string $purtelPasswort2
     *
     * @return Customer
     */
    public function setPurtelPasswort2($purtelPasswort2)
    {
        $this->purtelPasswort2 = $purtelPasswort2;

        return $this;
    }

    /**
     * Get purtelPasswort2.
     *
     * @return string
     */
    public function getPurtelPasswort2()
    {
        return $this->purtelPasswort2;
    }

    /**
     * Set purtelTelnr2.
     *
     * @param string $purtelTelnr2
     *
     * @return Customer
     */
    public function setPurtelTelnr2($purtelTelnr2)
    {
        $this->purtelTelnr2 = $purtelTelnr2;

        return $this;
    }

    /**
     * Get purtelTelnr2.
     *
     * @return string
     */
    public function getPurtelTelnr2()
    {
        return $this->purtelTelnr2;
    }

    /**
     * Set purtelLogin3.
     *
     * @param string $purtelLogin3
     *
     * @return Customer
     */
    public function setPurtelLogin3($purtelLogin3)
    {
        $this->purtelLogin3 = $purtelLogin3;

        return $this;
    }

    /**
     * Get purtelLogin3.
     *
     * @return string
     */
    public function getPurtelLogin3()
    {
        return $this->purtelLogin3;
    }

    /**
     * Set purtelPasswort3.
     *
     * @param string $purtelPasswort3
     *
     * @return Customer
     */
    public function setPurtelPasswort3($purtelPasswort3)
    {
        $this->purtelPasswort3 = $purtelPasswort3;

        return $this;
    }

    /**
     * Get purtelPasswort3.
     *
     * @return string
     */
    public function getPurtelPasswort3()
    {
        return $this->purtelPasswort3;
    }

    /**
     * Set purtelTelnr3.
     *
     * @param string $purtelTelnr3
     *
     * @return Customer
     */
    public function setPurtelTelnr3($purtelTelnr3)
    {
        $this->purtelTelnr3 = $purtelTelnr3;

        return $this;
    }

    /**
     * Get purtelTelnr3.
     *
     * @return string
     */
    public function getPurtelTelnr3()
    {
        return $this->purtelTelnr3;
    }

    /**
     * Set purtelDeleteDate.
     *
     * @param string $purtelDeleteDate
     *
     * @return Customer
     */
    public function setPurtelDeleteDate($purtelDeleteDate)
    {
        $this->purtelDeleteDate = $purtelDeleteDate;

        return $this;
    }

    /**
     * Get purtelDeleteDate.
     *
     * @return string
     */
    public function getPurtelDeleteDate()
    {
        return $this->purtelDeleteDate;
    }

    /**
     * Set acsId.
     *
     * @param string $acsId
     *
     * @return Customer
     */
    public function setAcsId($acsId)
    {
        $this->acsId = $acsId;

        return $this;
    }

    /**
     * Get acsId.
     *
     * @return string
     */
    public function getAcsId()
    {
        return $this->acsId;
    }

    /**
     * Set tag.
     *
     * @param string $tag
     *
     * @return Customer
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag.
     *
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set cancelPurtelDate.
     *
     * @param string $cancelPurtelDate
     *
     * @return Customer
     */
    public function setCancelPurtelDate($cancelPurtelDate)
    {
        $this->cancelPurtelDate = $cancelPurtelDate;

        return $this;
    }

    /**
     * Get cancelPurtelDate.
     *
     * @return string
     */
    public function getCancelPurtelDate()
    {
        return $this->cancelPurtelDate;
    }

    /**
     * Set cancelPurtelFrom.
     *
     * @param string $cancelPurtelFrom
     *
     * @return Customer
     */
    public function setCancelPurtelFrom($cancelPurtelFrom)
    {
        $this->cancelPurtelFrom = $cancelPurtelFrom;

        return $this;
    }

    /**
     * Get cancelPurtelFrom.
     *
     * @return string
     */
    public function getCancelPurtelFrom()
    {
        return $this->cancelPurtelFrom;
    }

    /**
     * Set cancelTelDate.
     *
     * @param string $cancelTelDate
     *
     * @return Customer
     */
    public function setCancelTelDate($cancelTelDate)
    {
        $this->cancelTelDate = $cancelTelDate;

        return $this;
    }

    /**
     * Get cancelTelDate.
     *
     * @return string
     */
    public function getCancelTelDate()
    {
        return $this->cancelTelDate;
    }

    /**
     * Set cancelTelForDate.
     *
     * @param string $cancelTelForDate
     *
     * @return Customer
     */
    public function setCancelTelForDate($cancelTelForDate)
    {
        $this->cancelTelForDate = $cancelTelForDate;

        return $this;
    }

    /**
     * Get cancelTelForDate.
     *
     * @return string
     */
    public function getCancelTelForDate()
    {
        return $this->cancelTelForDate;
    }

    /**
     * Set cancelTelFrom.
     *
     * @param string $cancelTelFrom
     *
     * @return Customer
     */
    public function setCancelTelFrom($cancelTelFrom)
    {
        $this->cancelTelFrom = $cancelTelFrom;

        return $this;
    }

    /**
     * Get cancelTelFrom.
     *
     * @return string
     */
    public function getCancelTelFrom()
    {
        return $this->cancelTelFrom;
    }

    /**
     * Set cancelProduct.
     *
     * @param string $cancelProduct
     *
     * @return Customer
     */
    public function setCancelProduct($cancelProduct)
    {
        $this->cancelProduct = $cancelProduct;

        return $this;
    }

    /**
     * Get cancelProduct.
     *
     * @return string
     */
    public function getCancelProduct()
    {
        return $this->cancelProduct;
    }

    /**
     * Set cancelProductFrom.
     *
     * @param string $cancelProductFrom
     *
     * @return Customer
     */
    public function setCancelProductFrom($cancelProductFrom)
    {
        $this->cancelProductFrom = $cancelProductFrom;

        return $this;
    }

    /**
     * Get cancelProductFrom.
     *
     * @return string
     */
    public function getCancelProductFrom()
    {
        return $this->cancelProductFrom;
    }

    /**
     * Set bankAccountMandantenid.
     *
     * @param string $bankAccountMandantenid
     *
     * @return Customer
     */
    public function setBankAccountMandantenid($bankAccountMandantenid)
    {
        $this->bankAccountMandantenid = $bankAccountMandantenid;

        return $this;
    }

    /**
     * Get bankAccountMandantenid.
     *
     * @return string
     */
    public function getBankAccountMandantenid()
    {
        return $this->bankAccountMandantenid;
    }

    /**
     * Set bankAccountDebitor.
     *
     * @param string $bankAccountDebitor
     *
     * @return Customer
     */
    public function setBankAccountDebitor($bankAccountDebitor)
    {
        $this->bankAccountDebitor = $bankAccountDebitor;

        return $this;
    }

    /**
     * Get bankAccountDebitor.
     *
     * @return string
     */
    public function getBankAccountDebitor()
    {
        return $this->bankAccountDebitor;
    }

    /**
     * Set voucher.
     *
     * @param string $voucher
     *
     * @return Customer
     */
    public function setVoucher($voucher)
    {
        $this->voucher = $voucher;

        return $this;
    }

    /**
     * Get voucher.
     *
     * @return string
     */
    public function getVoucher()
    {
        return $this->voucher;
    }

    /**
     * Set vectoring.
     *
     * @param string $vectoring
     *
     * @return Customer
     */
    public function setVectoring($vectoring)
    {
        $this->vectoring = $vectoring;

        return $this;
    }

    /**
     * Get vectoring.
     *
     * @return string
     */
    public function getVectoring()
    {
        return $this->vectoring;
    }

    /**
     * Set pppoePin.
     *
     * @param string $pppoePin
     *
     * @return Customer
     */
    public function setPppoePin($pppoePin)
    {
        $this->pppoePin = $pppoePin;

        return $this;
    }

    /**
     * Get pppoePin.
     *
     * @return string
     */
    public function getPppoePin()
    {
        return $this->pppoePin;
    }

    /**
     * Set commissioningFrom
     *
     * @param string $commissioningFrom
     *
     * @return Customer
     */
    public function setCommissioningFrom($commissioningFrom)
    {
        $this->commissioningFrom = $commissioningFrom;

        return $this;
    }

    /**
     * Get commissioningFrom
     *
     * @return string
     */
    public function getCommissioningFrom()
    {
        return $this->commissioningFrom;
    }

    /**
     * Set commissioningWishDate
     *
     * @param string $commissioningWishDate
     *
     * @return Customer
     */
    public function setCommissioningWishDate($commissioningWishDate)
    {
        $this->commissioningWishDate = $commissioningWishDate;

        return $this;
    }

    /**
     * Get commissioningWishDate
     *
     * @return string
     */
    public function getCommissioningWishDate()
    {
        return $this->commissioningWishDate;
    }

    /**
     * Set commissioningDate
     *
     * @param string $commissioningDate
     *
     * @return Customer
     */
    public function setCommissioningDate($commissioningDate)
    {
        $this->commissioningDate = $commissioningDate;

        return $this;
    }

    /**
     * Get commissioningDate
     *
     * @return string
     */
    public function getCommissioningDate()
    {
        return $this->commissioningDate;
    }

    /**
     * Set inkassoBankgebuehren
     *
     * @param string $inkassoBankgebuehren
     *
     * @return Customer
     */
    public function setInkassoBankgebuehren($inkassoBankgebuehren)
    {
        $this->inkassoBankgebuehren = $inkassoBankgebuehren;

        return $this;
    }

    /**
     * Get inkassoBankgebuehren
     *
     * @return string
     */
    public function getInkassoBankgebuehren()
    {
        return $this->inkassoBankgebuehren;
    }
    
    /**
     * Set inkassoMahngebuehren
     *
     * @param string $inkassoMahngebuehren
     *
     * @return Customer
     */
    public function setInkassoMahngebuehren($inkassoMahngebuehren)
    {
        $this->inkassoMahngebuehren = $inkassoMahngebuehren;

        return $this;
    }

    /**
     * Get inkassoMahngebuehren
     *
     * @return string
     */
    public function getInkassoMahngebuehren()
    {
        return $this->inkassoMahngebuehren;
    }

    /**
     * Set inkassoDatumLetzteMahnung
     *
     * @param string $inkassoDatumLetzteMahnung
     *
     * @return Customer
     */
    public function setInkassoDatumLetzteMahnung($inkassoDatumLetzteMahnung)
    {
        $this->inkassoDatumLetzteMahnung = $inkassoDatumLetzteMahnung;

        return $this;
    }

    /**
     * Get inkassoDatumLetzteMahnung
     *
     * @return string
     */
    public function getInkassoDatumLetzteMahnung()
    {
        return $this->inkassoDatumLetzteMahnung;
    }

    /**
     * Set inkassoDatumRuecklastschrift
     *
     * @param string $inkassoDatumRuecklastschrift
     *
     * @return Customer
     */
    public function setInkassoDatumRuecklastschrift($inkassoDatumRuecklastschrift)
    {
        $this->inkassoDatumRuecklastschrift = $inkassoDatumRuecklastschrift;

        return $this;
    }

    /**
     * Get inkassoDatumLetzteMahnung
     *
     * @return string
     */
    public function getInkassoDatumRuecklastschrift()
    {
        return $this->inkassoDatumRuecklastschrift;
    }
    
    /**
     * Set inkassoGrundRuecklastschrift
     *
     * @param string $inkassoGrundRuecklastschrift
     *
     * @return Customer
     */
    public function setInkassoGrundRuecklastschrift($inkassoGrundRuecklastschrift)
    {
        $this->inkassoGrundRuecklastschrift = $inkassoGrundRuecklastschrift;

        return $this;
    }

    /**
     * Get inkassoGrundRuecklastschrift
     *
     * @return string
     */
    public function getInkassoGrundRuecklastschrift()
    {
        return $this->inkassoGrundRuecklastschrift;
    }
    
    /**
     * Set inkassoHauptforderung
     *
     * @param string $inkassoHauptforderung
     *
     * @return Customer
     */
    public function setInkassoHauptforderung($inkassoHauptforderung)
    {
        $this->inkassoHauptforderung = $inkassoHauptforderung;

        return $this;
    }

    /**
     * Get inkassoHauptforderung
     *
     * @return string
     */
    public function getInkassoHauptforderung()
    {
        return $this->inkassoHauptforderung;
    }
    
    /**
     * Set inkassoRechnung
     *
     * @param string $inkassoRechnung
     *
     * @return Customer
     */
    public function setInkassoRechnung($inkassoRechnung)
    {
        $this->inkassoRechnung = $inkassoRechnung;

        return $this;
    }

    /**
     * Get inkassoRechnung
     *
     * @return string
     */
    public function getInkassoRechnung()
    {
        return $this->inkassoRechnung;
    }

    /**
     * Set overrideInheritedVlanProfiles
     *
     * @param string $overrideInheritedVlanProfiles
     *
     * @return Customer
     */
    public function setOverrideInheritedVlanProfiles($overrideInheritedVlanProfiles)
    {
        $this->overrideInheritedVlanProfiles = $overrideInheritedVlanProfiles;

        return $this;
    }

    /**
     * Get overrideInheritedVlanProfiles
     *
     * @return string
     */
    public function getOverrideInheritedVlanProfiles()
    {
        return $this->overrideInheritedVlanProfiles;
    }
}
