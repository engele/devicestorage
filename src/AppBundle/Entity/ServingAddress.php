<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * Entity class for serving-addresses
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ServingAddressRepository")
 * @ORM\Table(name="serving_address",
 *  uniqueConstraints={
 *      @UniqueConstraint(name="unique_address", columns={"zip_code", "city", "street", "house_number"})
 *  }
 * )
 */
class ServingAddress
{
    /**
     * Unique identifier
     *
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * zipCode
     *
     * @var string
     *
     * @ORM\Column(type="string", length=10, nullable=false, name="zip_code")
     */
    protected $zipCode;

    /**
     * city
     *
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false, name="city")
     */
    protected $city;

    /**
     * district
     *
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false, name="district")
     */
    protected $district;

    /**
     * street
     *
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false, name="street")
     */
    protected $street;

    /**
     * house number
     *
     * @var string
     *
     * @ORM\Column(type="string", length=10, nullable=false, name="house_number")
     */
    protected $houseNumber;

    /**
     * Amount of business units
     * (Anzahl Gewerbeeinheiten)
     *
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $amountOfBusinessUnits = 0; // Feld zur Eingabe einer natürlichen Zahl

    /**
     * Amount of dewelling units
     * (Anzahl Wohneinheiten)
     *
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $amountOfDwellingUnits = 0; // Feld zur Eingabe einer natürlichen Zahl

    /**
     * Roll-Out is planned in
     * (Ausbaujahr)
     *
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $rollOutIsPlannedIn; // Feld zur Eingabe einer Jahreszahl

    /**
     * Is roll-out finished
     *
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $isRollOutFinished = false; // Checkbox

    /**
     * Is building connected
     * (Ausbaustatus Hausanschluss)
     *
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $isBuildingConnected = false; // Dropdown (Ja = true / Nein = false)

    /**
     * Is NE4 finished
     * (Ausbausstatus NE4)
     *
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $isNe4Finished = false; // Dropdown (Ja = true / Nein = false)

    /**
     * Has GNV
     *
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $hasGnv = false; // Dropdown (Ja = true / Nein = false)

    /**
     * GNV note
     *
     * @var string
     *
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    protected $gnvNote; // Input-Feld: type=text, maxlength=5

    /**
     * Has ever had an internet supply contract
     * (Liefervertrag Internet/Telefon)
     *
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $hasEverHadAnInternetSupplyContract = false; // Dropdown (Ja = true / Nein = false)

    /**
     * Has ever had a basic-TV supply contract
     * (Liefervertrag Basis-TV)
     *
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $hasEverHadABasicTvSupplyContract = false; // Dropdown (Ja = true / Nein = false)

    /**
     * Connected to kvz
     * (KVZ Nr.)
     *
     * @var string
     *
     * @ORM\Column(type="string", length=9, nullable=true)
     */
    protected $connectedToKvz; // Input-Feld: type=text, maxlength=9

    /**
     * Technical note
     * (Technischer Hinweis)
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $technicalNote; // Input-Feld: type=text

    /**
     * Field and cadastre
     * (Flur/Kataster)
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $fieldAndCadastre; // Input-Feld: type=text

    /**
     * Note
     *
     * @var string
     *
     * @ORM\Column(type="text", length=6000, nullable=true)
     */
    protected $note; // Textarea

    /**
     * damping
     *
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true, name="damping")
     */
    protected $damping;

    /**
     * diameter 1
     *
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true, name="diameter_1")
     */
    protected $diameter_1;

    /**
     * length 1
     *
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true, name="length_1")
     */
    protected $length_1;

    /**
     * diameter 1
     *
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true, name="diameter_2")
     */
    protected $diameter_2;

    /**
     * length 2
     *
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true, name="length_2")
     */
    protected $length_2;

    /**
     * diameter 3
     *
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true, name="diameter_3")
     */
    protected $diameter_3;

    /**
     * length 3
     *
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true, name="length_3")
     */
    protected $length_3;

    /**
     * diameter 4
     *
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true, name="diameter_4")
     */
    protected $diameter_4;

    /**
     * length 4
     *
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true, name="length_4")
     */
    protected $length_4;

    /**
     * diameter 5
     *
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true, name="diameter_5")
     */
    protected $diameter_5;

    /**
     * length 5
     *
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true, name="length_5")
     */
    protected $length_5;






    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set zipCode.
     *
     * @param string $zipCode
     *
     * @return ServingAddress
     */
    public function setZipCode($zipCode) : ServingAddress
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode.
     *
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return ServingAddress
     */
    public function setCity($city) : ServingAddress
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set district.
     *
     * @param string $district
     *
     * @return ServingAddress
     */
    public function setDistrict($district) : ServingAddress
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district.
     *
     * @return string
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Set street.
     *
     * @param string $street
     *
     * @return ServingAddress
     */
    public function setStreet($street) : ServingAddress
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street.
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set houseNumber.
     *
     * @param string $houseNumber
     *
     * @return ServingAddress
     */
    public function setHouseNumber($houseNumber) : ServingAddress
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    /**
     * Get houseNumber.
     *
     * @return string
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * Set damping.
     *
     * @param int|null $damping
     *
     * @return ServingAddress
     */
    public function setDamping($damping = null) : ServingAddress
    {
        $this->damping = $damping;

        return $this;
    }

    /**
     * Get damping.
     *
     * @return int|null
     */
    public function getDamping()
    {
        return $this->damping;
    }

    /**
     * Set diameter1.
     *
     * @param int|null $diameter1
     *
     * @return ServingAddress
     */
    public function setDiameter1($diameter1 = null) : ServingAddress
    {
        $this->diameter_1 = $diameter1;

        return $this;
    }

    /**
     * Get diameter1.
     *
     * @return int|null
     */
    public function getDiameter1()
    {
        return $this->diameter_1;
    }

    /**
     * Set length1.
     *
     * @param int|null $length1
     *
     * @return ServingAddress
     */
    public function setLength1($length1 = null) : ServingAddress
    {
        $this->length_1 = $length1;

        return $this;
    }

    /**
     * Get length1.
     *
     * @return int|null
     */
    public function getLength1()
    {
        return $this->length_1;
    }

    /**
     * Set diameter2.
     *
     * @param int|null $diameter2
     *
     * @return ServingAddress
     */
    public function setDiameter2($diameter2 = null) : ServingAddress
    {
        $this->diameter_2 = $diameter2;

        return $this;
    }

    /**
     * Get diameter2.
     *
     * @return int|null
     */
    public function getDiameter2()
    {
        return $this->diameter_2;
    }

    /**
     * Set length2.
     *
     * @param int|null $length2
     *
     * @return ServingAddress
     */
    public function setLength2($length2 = null) : ServingAddress
    {
        $this->length_2 = $length2;

        return $this;
    }

    /**
     * Get length2.
     *
     * @return int|null
     */
    public function getLength2()
    {
        return $this->length_2;
    }

    /**
     * Set diameter3.
     *
     * @param int|null $diameter3
     *
     * @return ServingAddress
     */
    public function setDiameter3($diameter3 = null) : ServingAddress
    {
        $this->diameter_3 = $diameter3;

        return $this;
    }

    /**
     * Get diameter3.
     *
     * @return int|null
     */
    public function getDiameter3()
    {
        return $this->diameter_3;
    }

    /**
     * Set length3.
     *
     * @param int|null $length3
     *
     * @return ServingAddress
     */
    public function setLength3($length3 = null) : ServingAddress
    {
        $this->length_3 = $length3;

        return $this;
    }

    /**
     * Get length3.
     *
     * @return int|null
     */
    public function getLength3()
    {
        return $this->length_3;
    }

    /**
     * Set diameter4.
     *
     * @param int|null $diameter4
     *
     * @return ServingAddress
     */
    public function setDiameter4($diameter4 = null) : ServingAddress
    {
        $this->diameter_4 = $diameter4;

        return $this;
    }

    /**
     * Get diameter4.
     *
     * @return int|null
     */
    public function getDiameter4()
    {
        return $this->diameter_4;
    }

    /**
     * Set length4.
     *
     * @param int|null $length4
     *
     * @return ServingAddress
     */
    public function setLength4($length4 = null) : ServingAddress
    {
        $this->length_4 = $length4;

        return $this;
    }

    /**
     * Get length4.
     *
     * @return int|null
     */
    public function getLength4()
    {
        return $this->length_4;
    }

    /**
     * Set diameter5.
     *
     * @param int|null $diameter5
     *
     * @return ServingAddress
     */
    public function setDiameter5($diameter5 = null) : ServingAddress
    {
        $this->diameter_5 = $diameter5;

        return $this;
    }

    /**
     * Get diameter5.
     *
     * @return int|null
     */
    public function getDiameter5()
    {
        return $this->diameter_5;
    }

    /**
     * Set length5.
     *
     * @param int|null $length5
     *
     * @return ServingAddress
     */
    public function setLength5($length5 = null) : ServingAddress
    {
        $this->length_5 = $length5;

        return $this;
    }

    /**
     * Get length5.
     *
     * @return int|null
     */
    public function getLength5()
    {
        return $this->length_5;
    }

    /**
     * Set amountOfBusinessUnits.
     *
     * @param int $amountOfBusinessUnits
     *
     * @return ServingAddress
     */
    public function setAmountOfBusinessUnits($amountOfBusinessUnits)
    {
        $this->amountOfBusinessUnits = $amountOfBusinessUnits;

        return $this;
    }

    /**
     * Get amountOfBusinessUnits.
     *
     * @return int
     */
    public function getAmountOfBusinessUnits()
    {
        return $this->amountOfBusinessUnits;
    }

    /**
     * Set amountOfDwellingUnits.
     *
     * @param int $amountOfDwellingUnits
     *
     * @return ServingAddress
     */
    public function setAmountOfDwellingUnits($amountOfDwellingUnits)
    {
        $this->amountOfDwellingUnits = $amountOfDwellingUnits;

        return $this;
    }

    /**
     * Get amountOfDwellingUnits.
     *
     * @return int
     */
    public function getAmountOfDwellingUnits()
    {
        return $this->amountOfDwellingUnits;
    }

    /**
     * Set rollOutIsPlannedIn.
     *
     * @param \DateTime|null $rollOutIsPlannedIn
     *
     * @return ServingAddress
     */
    public function setRollOutIsPlannedIn($rollOutIsPlannedIn = null)
    {
        $this->rollOutIsPlannedIn = $rollOutIsPlannedIn;

        return $this;
    }

    /**
     * Get rollOutIsPlannedIn.
     *
     * @return \DateTime|null
     */
    public function getRollOutIsPlannedIn()
    {
        return $this->rollOutIsPlannedIn;
    }

    /**
     * Set isRollOutFinished.
     *
     * @param bool $isRollOutFinished
     *
     * @return ServingAddress
     */
    public function setIsRollOutFinished($isRollOutFinished)
    {
        $this->isRollOutFinished = $isRollOutFinished;

        return $this;
    }

    /**
     * Get isRollOutFinished.
     *
     * @return bool
     */
    public function getIsRollOutFinished()
    {
        return $this->isRollOutFinished;
    }

    /**
     * Set isBuildingConnected.
     *
     * @param bool $isBuildingConnected
     *
     * @return ServingAddress
     */
    public function setIsBuildingConnected($isBuildingConnected)
    {
        $this->isBuildingConnected = $isBuildingConnected;

        return $this;
    }

    /**
     * Get isBuildingConnected.
     *
     * @return bool
     */
    public function getIsBuildingConnected()
    {
        return $this->isBuildingConnected;
    }

    /**
     * Set isNe4Finished.
     *
     * @param bool $isNe4Finished
     *
     * @return ServingAddress
     */
    public function setIsNe4Finished($isNe4Finished)
    {
        $this->isNe4Finished = $isNe4Finished;

        return $this;
    }

    /**
     * Get isNe4Finished.
     *
     * @return bool
     */
    public function getIsNe4Finished()
    {
        return $this->isNe4Finished;
    }

    /**
     * Set hasGnv.
     *
     * @param bool $hasGnv
     *
     * @return ServingAddress
     */
    public function setHasGnv($hasGnv)
    {
        $this->hasGnv = $hasGnv;

        return $this;
    }

    /**
     * Get hasGnv.
     *
     * @return bool
     */
    public function getHasGnv()
    {
        return $this->hasGnv;
    }

    /**
     * Set gnvNote.
     *
     * @param string|null $gnvNote
     *
     * @return ServingAddress
     */
    public function setGnvNote($gnvNote = null)
    {
        $this->gnvNote = $gnvNote;

        return $this;
    }

    /**
     * Get gnvNote.
     *
     * @return string|null
     */
    public function getGnvNote()
    {
        return $this->gnvNote;
    }

    /**
     * Set hasEverHadAnInternetSupplyContract.
     *
     * @param bool $hasEverHadAnInternetSupplyContract
     *
     * @return ServingAddress
     */
    public function setHasEverHadAnInternetSupplyContract($hasEverHadAnInternetSupplyContract)
    {
        $this->hasEverHadAnInternetSupplyContract = $hasEverHadAnInternetSupplyContract;

        return $this;
    }

    /**
     * Get hasEverHadAnInternetSupplyContract.
     *
     * @return bool
     */
    public function getHasEverHadAnInternetSupplyContract()
    {
        return $this->hasEverHadAnInternetSupplyContract;
    }

    /**
     * Set hasEverHadABasicTvSupplyContract.
     *
     * @param bool $hasEverHadABasicTvSupplyContract
     *
     * @return ServingAddress
     */
    public function setHasEverHadABasicTvSupplyContract($hasEverHadABasicTvSupplyContract)
    {
        $this->hasEverHadABasicTvSupplyContract = $hasEverHadABasicTvSupplyContract;

        return $this;
    }

    /**
     * Get hasEverHadABasicTvSupplyContract.
     *
     * @return bool
     */
    public function getHasEverHadABasicTvSupplyContract()
    {
        return $this->hasEverHadABasicTvSupplyContract;
    }

    /**
     * Set connectedToKvz.
     *
     * @param string|null $connectedToKvz
     *
     * @return ServingAddress
     */
    public function setConnectedToKvz($connectedToKvz = null)
    {
        $this->connectedToKvz = $connectedToKvz;

        return $this;
    }

    /**
     * Get connectedToKvz.
     *
     * @return string|null
     */
    public function getConnectedToKvz()
    {
        return $this->connectedToKvz;
    }

    /**
     * Set technicalNote.
     *
     * @param string|null $technicalNote
     *
     * @return ServingAddress
     */
    public function setTechnicalNote($technicalNote = null)
    {
        $this->technicalNote = $technicalNote;

        return $this;
    }

    /**
     * Get technicalNote.
     *
     * @return string|null
     */
    public function getTechnicalNote()
    {
        return $this->technicalNote;
    }

    /**
     * Set fieldAndCadastre.
     *
     * @param string|null $fieldAndCadastre
     *
     * @return ServingAddress
     */
    public function setFieldAndCadastre($fieldAndCadastre = null)
    {
        $this->fieldAndCadastre = $fieldAndCadastre;

        return $this;
    }

    /**
     * Get fieldAndCadastre.
     *
     * @return string|null
     */
    public function getFieldAndCadastre()
    {
        return $this->fieldAndCadastre;
    }

    /**
     * Set note.
     *
     * @param string|null $note
     *
     * @return ServingAddress
     */
    public function setNote($note = null)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note.
     *
     * @return string|null
     */
    public function getNote()
    {
        return $this->note;
    }
}
