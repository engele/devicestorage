<?php

/**
 * This file is part of the wkv project.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Product\Traits\PricingTrait;

/**
 * Entity class for Customer Options (CrossSelling-Products)
 * 
 * @ORM\Entity
 * @ORM\Table(name="customer_options")
 */
class CustomerCrossSellingProduct
{
    use PricingTrait;

    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Customer id
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer", nullable=false, name="cust_id")
     */
    protected $customerId;

    /**
     * Option id
     * 
     * @var string
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Product\CrossSellingProduct")
     * @ORM\JoinColumn(name="opt_id", referencedColumnName="option", nullable=false)
     */
    protected $crossSellingProduct;

    /**
     * Price (net)
     * 
     * @var float
     * 
     * @ORM\Column(type="decimal", precision=12, scale=5, nullable=true)
     */
    protected $priceNet;

    /**
     * Price (gross)
     * 
     * @var float
     * 
     * @ORM\Column(type="decimal", precision=12, scale=5, nullable=true)
     */
    protected $priceGross;

    /**
     * Activation date
     * 
     * @var \DateTime
     * 
     * @ORM\Column(type="date", nullable=true)
     */
    protected $activationDate;

    /**
     * Deactivation date
     * 
     * @var \DateTime
     * 
     * @ORM\Column(type="date", nullable=true)
     */
    protected $deactivationDate;

    /**
     * Tax rate
     * 
     * @var AppBundle\Entity\Product\TaxRate
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Product\TaxRate")
     * @ORM\JoinColumn(name="tax_rate_id", referencedColumnName="id", nullable=true)
     */
    protected $taxRate;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerId.
     *
     * @param int $customerId
     *
     * @return CustomerCrossSellingProduct
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * Get customerId.
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * Set priceNet.
     *
     * @param string|null $priceNet
     *
     * @return CustomerCrossSellingProduct
     */
    public function setPriceNet($priceNet = null)
    {
        $this->priceNet = $priceNet;

        return $this;
    }

    /**
     * Get priceNet.
     *
     * @return string|null
     */
    public function getPriceNet()
    {
        return $this->priceNet;
    }

    /**
     * Set priceGross.
     *
     * @param string|null $priceGross
     *
     * @return CustomerCrossSellingProduct
     */
    public function setPriceGross($priceGross = null)
    {
        $this->priceGross = $priceGross;

        return $this;
    }

    /**
     * Get priceGross.
     *
     * @return string|null
     */
    public function getPriceGross()
    {
        return $this->priceGross;
    }

    /**
     * Set activationDate.
     *
     * @param \DateTime|null $activationDate
     *
     * @return CustomerCrossSellingProduct
     */
    public function setActivationDate($activationDate = null)
    {
        $this->activationDate = $activationDate;

        return $this;
    }

    /**
     * Get activationDate.
     *
     * @return \DateTime|null
     */
    public function getActivationDate()
    {
        return $this->activationDate;
    }

    /**
     * Set deactivationDate.
     *
     * @param \DateTime|null $deactivationDate
     *
     * @return CustomerCrossSellingProduct
     */
    public function setDeactivationDate($deactivationDate = null)
    {
        $this->deactivationDate = $deactivationDate;

        return $this;
    }

    /**
     * Get deactivationDate.
     *
     * @return \DateTime|null
     */
    public function getDeactivationDate()
    {
        return $this->deactivationDate;
    }

    /**
     * Set crossSellingProduct.
     *
     * @param \AppBundle\Entity\Product\CrossSellingProduct $crossSellingProduct
     *
     * @return CustomerCrossSellingProduct
     */
    public function setCrossSellingProduct(\AppBundle\Entity\Product\CrossSellingProduct $crossSellingProduct)
    {
        $this->crossSellingProduct = $crossSellingProduct;

        return $this;
    }

    /**
     * Get crossSellingProduct.
     *
     * @return \AppBundle\Entity\Product\CrossSellingProduct
     */
    public function getCrossSellingProduct()
    {
        return $this->crossSellingProduct;
    }

    /**
     * Set taxRate.
     *
     * @param \AppBundle\Entity\Product\TaxRate|null $taxRate
     *
     * @return CustomerCrossSellingProduct
     */
    public function setTaxRate(\AppBundle\Entity\Product\TaxRate $taxRate = null)
    {
        $this->taxRate = $taxRate;

        return $this;
    }

    /**
     * Get taxRate.
     *
     * @return \AppBundle\Entity\Product\TaxRate|null
     */
    public function getTaxRate()
    {
        return $this->taxRate;
    }
}
