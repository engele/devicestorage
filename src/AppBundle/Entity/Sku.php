<?php

/**
 * This file is part of the wkv project.
 */

namespace Wkv\Entity;

use Doctrine\ORM\Mapping as ORM;

/*
 * Entity class for sku
 * 
 * @ORM\Entity
 * @ORM\Table(name="products_sku")
 */
class Sku
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $sku;

    /**
     * Product
     * 
     * @var Product
     * 
     * @ORM\ManyToOne(targetEntity="Wkv\Entity\ForWkv\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * @ORM\Id
     */
    protected $product;

    /**
     * Product attribute
     * 
     * @var ProductAttribute
     * 
     * @ORM\ManyToOne(targetEntity="Wkv\Entity\ForWkv\ProductAttribute")
     * @ORM\JoinColumn(name="attribute_id", referencedColumnName="id")
     * @ORM\Id
     */
    protected $productAttribute;

    /**
     * Product attribute option
     * 
     * @var ProductAttributeOption
     * 
     * @ORM\ManyToOne(targetEntity="Wkv\Entity\ForWkv\ProductAttributeOption")
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     * @ORM\Id
     */
    protected $productAttributeOption;

    /**
     * Set sku
     *
     * @param string $sku
     *
     * @return Sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set product
     *
     * @param \Wkv\Entity\ForWkv\Product $product
     *
     * @return Sku
     */
    public function setProduct(\Wkv\Entity\ForWkv\Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Wkv\Entity\ForWkv\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set productAttribute
     *
     * @param \Wkv\Entity\ForWkv\ProductAttribute $productAttribute
     *
     * @return Sku
     */
    public function setProductAttribute(\Wkv\Entity\ForWkv\ProductAttribute $productAttribute)
    {
        $this->productAttribute = $productAttribute;

        return $this;
    }

    /**
     * Get productAttribute
     *
     * @return \Wkv\Entity\ForWkv\ProductAttribute
     */
    public function getProductAttribute()
    {
        return $this->productAttribute;
    }

    /**
     * Set productAttributeOption
     *
     * @param \Wkv\Entity\ForWkv\ProductAttributeOption $productAttributeOption
     *
     * @return Sku
     */
    public function setProductAttributeOption(\Wkv\Entity\ForWkv\ProductAttributeOption $productAttributeOption)
    {
        $this->productAttributeOption = $productAttributeOption;

        return $this;
    }

    /**
     * Get productAttributeOption
     *
     * @return \Wkv\Entity\ForWkv\ProductAttributeOption
     */
    public function getProductAttributeOption()
    {
        return $this->productAttributeOption;
    }
}
