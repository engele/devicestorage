<?php

/**
 * This file is part of the wkv project.
 * 
 * only for ikv imports
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity class for product
 * 
 * @ORM\Entity
 * @ORM\Table(name="customer_products")
 */
class CustomerProduct
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Product id
     * 
     * @var integer
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    protected $productId;

    /**
     * Customer id
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $customerId;

    /**
     * Price (net)
     * 
     * @var float
     * 
     * @ORM\Column(type="decimal", precision=12, scale=5, nullable=true)
     */
    protected $priceNet;

    /**
     * Activation date
     * 
     * @var \DateTime
     * 
     * @ORM\Column(type="date", nullable=true)
     */
    protected $activationDate;

    /**
     * Deactivation date
     * 
     * @var \DateTime
     * 
     * @ORM\Column(type="date", nullable=true)
     */
    protected $deactivationDate;

    /**
     * Wurde dieser Eintrag bereits korrekt zum Kunden übertragen?
     * 
     * @var boolean
     * 
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $propperlyCopyedToCustomer = false;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerId
     *
     * @param integer $customerId
     *
     * @return CustomerProduct
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * Get customerId
     *
     * @return integer
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * Set priceNet
     *
     * @param string $priceNet
     *
     * @return CustomerProduct
     */
    public function setPriceNet($priceNet)
    {
        $this->priceNet = $priceNet;

        return $this;
    }

    /**
     * Get priceNet
     *
     * @return string
     */
    public function getPriceNet()
    {
        return $this->priceNet;
    }

    /**
     * Set activationDate
     *
     * @param \DateTime $activationDate
     *
     * @return CustomerProduct
     */
    public function setActivationDate($activationDate)
    {
        $this->activationDate = $activationDate;

        return $this;
    }

    /**
     * Get activationDate
     *
     * @return \DateTime
     */
    public function getActivationDate()
    {
        return $this->activationDate;
    }

    /**
     * Set deactivationDate
     *
     * @param \DateTime $deactivationDate
     *
     * @return CustomerProduct
     */
    public function setDeactivationDate($deactivationDate)
    {
        $this->deactivationDate = $deactivationDate;

        return $this;
    }

    /**
     * Get deactivationDate
     *
     * @return \DateTime
     */
    public function getDeactivationDate()
    {
        return $this->deactivationDate;
    }

    /**
     * Set productId
     *
     * @param \AppBundle\Entity\Product $productId
     *
     * @return CustomerProduct
     */
    public function setProductId(\AppBundle\Entity\Product $productId = null)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return \AppBundle\Entity\Product
     */
    public function getProductId()
    {
        return $this->productId;
    }
}
