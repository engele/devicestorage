<?php

/**
 * This file is part of the wkv project.
 */

namespace AppBundle\Entity\Location;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity class for main produkte group
 * 
 * @ORM\Entity
 * @ORM\Table(name="card_types")
 */
class CardType
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Name
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=64, nullable=false)
     */
    protected $name;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return CardType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
