<?php

/**
 * This file is part of the DICLINA project.
 */

namespace AppBundle\Entity\Location;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity class for LSA-KVZ-Partition
 * 
 * @ORM\Entity
 * @ORM\Table(name="lsa_kvz_partitions")
 */
class LsaKvz
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Location
     * 
     * @var AppBundle\Entity\Location\Location
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location\Location")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id", nullable=false)
     */
    protected $location;

    /**
     * Name
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=64, nullable=false, name="kvz_name")
     */
    protected $name;

    /**
     * Name
     * 
     * @var string
     * 
     * @ORM\Column(type="integer", length=11, nullable=false, name="pin_amount")
     */
    protected $pinAmount;

    /**
     * Name
     * 
     * @var string
     * 
     * @ORM\Column(type="integer", length=11, nullable=true, name="dpbo")
     */
    protected $dpbo;

    /**
     * Name
     * 
     * @var string
     * 
     * @ORM\Column(type="integer", length=11, nullable=true, name="esel_wert")
     */
    protected $esel;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return LsaKvz
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set pinAmount.
     *
     * @param int $pinAmount
     *
     * @return LsaKvz
     */
    public function setPinAmount($pinAmount)
    {
        $this->pinAmount = $pinAmount;

        return $this;
    }

    /**
     * Get pinAmount.
     *
     * @return int
     */
    public function getPinAmount()
    {
        return $this->pinAmount;
    }

    /**
     * Set dpbo.
     *
     * @param int $dpbo
     *
     * @return LsaKvz
     */
    public function setDpbo($dpbo)
    {
        $this->dpbo = $dpbo;

        return $this;
    }

    /**
     * Get dpbo.
     *
     * @return int
     */
    public function getDpbo()
    {
        return $this->dpbo;
    }

    /**
     * Set esel.
     *
     * @param int $esel
     *
     * @return LsaKvz
     */
    public function setEsel($esel)
    {
        $this->esel = $esel;

        return $this;
    }

    /**
     * Get esel.
     *
     * @return int
     */
    public function getEsel()
    {
        return $this->esel;
    }

    /**
     * Set location.
     *
     * @param \AppBundle\Entity\Location\Location $location
     *
     * @return LsaKvz
     */
    public function setLocation(\AppBundle\Entity\Location\Location $location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location.
     *
     * @return \AppBundle\Entity\Location\Location
     */
    public function getLocation()
    {
        return $this->location;
    }
}
