<?php

/**
 * This file is part of the DICLINA project.
 */

namespace AppBundle\Entity\Location;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entity class for Card
 * 
 * @ORM\Entity
 * @ORM\Table(name="cards")
 */
class Card
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     * 
     * @ORM\Column(type="integer", length=11, nullable=false, name="first_port_number")
     */
    protected $firstPortNumber;

    /**
     * @var integer
     * 
     * @ORM\Column(type="integer", length=11, nullable=false, name="port_amount")
     */
    protected $portAmount;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=64, nullable=false, name="name")
     */
    protected $name;

    /**
     * @var \AppBundle\Entity\Location\CardType
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location\CardType")
     * @ORM\JoinColumn(name="card_type_id", referencedColumnName="id")
     */
    protected $cardType;

    /**
     * @var \AppBundle\Entity\Location\Location
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location\Location")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     */
    protected $location;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=12, nullable=false, name="Line")
     */
    protected $lineIdentifierPrefix;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $ipAddress;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\VlanProfile")
     * @ORM\JoinTable(name="card_port_default_vlan_profiles", 
     *      joinColumns={@ORM\JoinColumn(name="location_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="vlan_profile_id", referencedColumnName="id")}
     * )
     */
    protected $portDefaultVlanProfiles;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\VlanProfile")
     * @ORM\JoinTable(name="card_customer_default_vlan_profiles", 
     *      joinColumns={@ORM\JoinColumn(name="location_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="vlan_profile_id", referencedColumnName="id")}
     * )
     */
    protected $customerDefaultVlanProfiles;

    /**
     * Card ports
     * 
     * @var ArrayCollection
     */
    protected $ports;

    /**
     * Enable RF-Overlay for customer on this card
     * 
     * @var boolean
     * 
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $rfOverlayEnabled = false;

    /**
     * Set card ports
     * 
     * @param ArrayCollection|array $ports
     * 
     * @return Card
     */
    public function setPorts($ports) : Card
    {
        if ($ports instanceof ArrayCollection) {
            $this->ports = $ports;
        } else {
            $this->ports = new ArrayCollection($ports);
        }

        return $this;
    }

    /**
     * Get card ports
     * 
     * @return ArrayCollection
     */
    public function getPorts()
    {
        return $this->ports;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set portAmount.
     *
     * @param int $portAmount
     *
     * @return Card
     */
    public function setPortAmount($portAmount)
    {
        $this->portAmount = $portAmount;

        return $this;
    }

    /**
     * Get portAmount.
     *
     * @return int
     */
    public function getPortAmount()
    {
        return $this->portAmount;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Card
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lineIdentifierPrefix.
     *
     * @param string $lineIdentifierPrefix
     *
     * @return Card
     */
    public function setLineIdentifierPrefix($lineIdentifierPrefix)
    {
        $this->lineIdentifierPrefix = $lineIdentifierPrefix;

        return $this;
    }

    /**
     * Get lineIdentifierPrefix.
     *
     * @return string
     */
    public function getLineIdentifierPrefix()
    {
        return $this->lineIdentifierPrefix;
    }

    /**
     * Set ipAddress.
     *
     * @param string|null $ipAddress
     *
     * @return Card
     */
    public function setIpAddress($ipAddress = null)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress.
     *
     * @return string|null
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set cardType.
     *
     * @param \AppBundle\Entity\Location\CardType|null $cardType
     *
     * @return Card
     */
    public function setCardType(\AppBundle\Entity\Location\CardType $cardType = null)
    {
        $this->cardType = $cardType;

        return $this;
    }

    /**
     * Get cardType.
     *
     * @return \AppBundle\Entity\Location\CardType|null
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * Set location.
     *
     * @param \AppBundle\Entity\Location\Location|null $location
     *
     * @return Card
     */
    public function setLocation(\AppBundle\Entity\Location\Location $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location.
     *
     * @return \AppBundle\Entity\Location\Location|null
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set firstPortNumber.
     *
     * @param int $firstPortNumber
     *
     * @return Card
     */
    public function setFirstPortNumber($firstPortNumber)
    {
        $this->firstPortNumber = $firstPortNumber;

        return $this;
    }

    /**
     * Get firstPortNumber.
     *
     * @return int
     */
    public function getFirstPortNumber()
    {
        return $this->firstPortNumber;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->portDefaultVlanProfiles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->customerDefaultVlanProfiles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add portDefaultVlanProfile.
     *
     * @param \AppBundle\Entity\VlanProfile $portDefaultVlanProfile
     *
     * @return Card
     */
    public function addPortDefaultVlanProfile(\AppBundle\Entity\VlanProfile $portDefaultVlanProfile)
    {
        $this->portDefaultVlanProfiles[] = $portDefaultVlanProfile;

        return $this;
    }

    /**
     * Remove portDefaultVlanProfile.
     *
     * @param \AppBundle\Entity\VlanProfile $portDefaultVlanProfile
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePortDefaultVlanProfile(\AppBundle\Entity\VlanProfile $portDefaultVlanProfile)
    {
        return $this->portDefaultVlanProfiles->removeElement($portDefaultVlanProfile);
    }

    /**
     * Get portDefaultVlanProfiles.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPortDefaultVlanProfiles()
    {
        return $this->portDefaultVlanProfiles;
    }

    /**
     * Add customerDefaultVlanProfile.
     *
     * @param \AppBundle\Entity\VlanProfile $customerDefaultVlanProfile
     *
     * @return Card
     */
    public function addCustomerDefaultVlanProfile(\AppBundle\Entity\VlanProfile $customerDefaultVlanProfile)
    {
        $this->customerDefaultVlanProfiles[] = $customerDefaultVlanProfile;

        return $this;
    }

    /**
     * Remove customerDefaultVlanProfile.
     *
     * @param \AppBundle\Entity\VlanProfile $customerDefaultVlanProfile
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCustomerDefaultVlanProfile(\AppBundle\Entity\VlanProfile $customerDefaultVlanProfile)
    {
        return $this->customerDefaultVlanProfiles->removeElement($customerDefaultVlanProfile);
    }

    /**
     * Get customerDefaultVlanProfiles.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomerDefaultVlanProfiles()
    {
        return $this->customerDefaultVlanProfiles;
    }

    /**
     * Set rfOverlayEnabled.
     *
     * @param bool|null $rfOverlayEnabled
     *
     * @return Card
     */
    public function setRfOverlayEnabled($rfOverlayEnabled = null)
    {
        $this->rfOverlayEnabled = $rfOverlayEnabled;

        return $this;
    }

    /**
     * Get rfOverlayEnabled.
     *
     * @return bool|null
     */
    public function getRfOverlayEnabled()
    {
        return $this->rfOverlayEnabled;
    }
}
