<?php

/**
 * This file is part of the wkv project.
 */

namespace AppBundle\Entity\Location;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity class for Card Port Attribute
 * 
 * @ORM\Entity
 * @ORM\Table(name="card_port_attributes")
 */
class CardPortAttribute
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $label;

    /**
     * This values are used as identifier in the code of this application.
     * Make sure, not to change one of the values which are already used somewhere in the code
     * because it will most likely break some functionalities.
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    protected $identifier;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label.
     *
     * @param string $label
     *
     * @return CardPortAttribute
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set identifier.
     *
     * @param string $identifier
     *
     * @return CardPortAttribute
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }
}
