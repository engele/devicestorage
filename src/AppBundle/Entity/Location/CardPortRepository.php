<?php

/**
 * 
 */

namespace AppBundle\Entity\Location;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Location\Card;

/**
 * 
 */
class CardPortRepository extends EntityRepository
{
    /**
     * Find one card port by card and number
     */
    public function findOneByCardAndNumber(Card $card, $number)
    {
        if (!is_numeric($number)) {
            return null;
        }

        return $this->createQueryBuilder('cp')
            ->where('cp.card = :card')
            ->andWhere('cp.number = :number')
            ->setParameter('card', $card)
            ->setParameter('number', $number)
            ->getQuery()->getSingleResult();
    }
}
