<?php

/**
 * This file is part of the wkv project.
 */

namespace AppBundle\Entity\Location;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entity class for Card
 * 
 * @ORM\Table(name="card_ports")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Location\CardPortRepository")
 */
class CardPort implements \ArrayAccess
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     * 
     * @ORM\Column(type="integer", length=11, nullable=false)
     */
    protected $number;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=12, nullable=true)
     */
    protected $color;

    /**
     * @var \AppBundle\Entity\Location\Card
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location\Card")
     * @ORM\JoinColumn(name="card_id", referencedColumnName="id")
     */
    protected $card;

    /**
     * @var \AppBundle\Entity\Location\CardPort
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location\CardPort")
     * @ORM\JoinColumn(name="card_port_id", referencedColumnName="id")
     */
    protected $relatedCardPort;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Location\CardPortAttribute")
     * @ORM\JoinTable(name="card_ports_to_card_port_attributes", 
     *      joinColumns={@ORM\JoinColumn(name="card_port_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="card_port_attribute_id", referencedColumnName="id")}
     * )
     */
    protected $attributes;

    /**
     * @var \AppBundle\Entity\Location\CardType
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location\CardType")
     * @ORM\JoinColumn(name="connection_type_id", referencedColumnName="id")
     */
    protected $connectionType;

    /**
     * @var integer
     * 
     * @ORM\Column(type="integer", length=3, nullable=true)
     */
    protected $maxUnicastMacs;

    /**
     * @var mixed
     */
    protected $offset;

    /**
     * Constructor
     */
    public function __construct() {
        $this->attributes = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        if (!is_null($offset)) {
            $this->$offset = $value;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset) {
        return isset($this->$offset);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset) {
        unset($this->$offset);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset) {
        return isset($this->$offset) ? $this->$offset : null;
    }

    /**
     * Has a certain attribute?
     * 
     * @param string $identifier
     * 
     * @return boolean
     */
    public function hasAttribute(string $identifier) : bool
    {
        foreach ($this->attributes as $attribute) {
            if ($attribute->getIdentifier() === $identifier) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number.
     *
     * @param int $number
     *
     * @return CardPort
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number.
     *
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set color.
     *
     * @param string|null $color
     *
     * @return CardPort
     */
    public function setColor($color = null)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color.
     *
     * @return string|null
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set relatedCardPort.
     *
     * @param \AppBundle\Entity\Location\CardPort|null $relatedCardPort
     *
     * @return CardPort
     */
    public function setRelatedCardPort(\AppBundle\Entity\Location\CardPort $relatedCardPort = null)
    {
        $this->relatedCardPort = $relatedCardPort;

        return $this;
    }

    /**
     * Get relatedCardPort.
     *
     * @return \AppBundle\Entity\Location\CardPort|null
     */
    public function getRelatedCardPort()
    {
        return $this->relatedCardPort;
    }

    /**
     * Set card.
     *
     * @param \AppBundle\Entity\Location\Card|null $card
     *
     * @return CardPort
     */
    public function setCard(\AppBundle\Entity\Location\Card $card = null)
    {
        $this->card = $card;

        return $this;
    }

    /**
     * Get card.
     *
     * @return \AppBundle\Entity\Location\Card|null
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * Add attribute.
     *
     * @param \AppBundle\Entity\Location\CardPortAttribute $attribute
     *
     * @return CardPort
     */
    public function addAttribute(\AppBundle\Entity\Location\CardPortAttribute $attribute)
    {
        $this->attributes[] = $attribute;

        return $this;
    }

    /**
     * Remove attribute.
     *
     * @param \AppBundle\Entity\Location\CardPortAttribute $attribute
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeAttribute(\AppBundle\Entity\Location\CardPortAttribute $attribute)
    {
        return $this->attributes->removeElement($attribute);
    }

    /**
     * Get attributes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Set connectionType.
     *
     * @param \AppBundle\Entity\Location\CardType|null $connectionType
     *
     * @return CardPort
     */
    public function setConnectionType(\AppBundle\Entity\Location\CardType $connectionType = null)
    {
        $this->connectionType = $connectionType;

        return $this;
    }

    /**
     * Get connectionType.
     *
     * @return \AppBundle\Entity\Location\CardType|null
     */
    public function getConnectionType()
    {
        return $this->connectionType;
    }

    /**
     * Set maxUnicastMacs.
     *
     * @param int|null $maxUnicastMacs
     *
     * @return CardPort
     */
    public function setMaxUnicastMacs($maxUnicastMacs = null)
    {
        $this->maxUnicastMacs = $maxUnicastMacs;

        return $this;
    }

    /**
     * Get maxUnicastMacs.
     *
     * @return int|null
     */
    public function getMaxUnicastMacs()
    {
        return $this->maxUnicastMacs;
    }
}
