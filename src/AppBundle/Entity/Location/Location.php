<?php

/**
 * This file is part of the wkv project.
 */

namespace AppBundle\Entity\Location;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entity class for Location
 * 
 * @ORM\Entity
 * @ORM\Table(name="locations")
 */
class Location
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \AppBundle\Entity\Network
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Network")
     * @ORM\JoinColumn(name="network_id", referencedColumnName="id")
     */
    protected $network;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=128, nullable=false, name="name")
     */
    protected $name;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=64, nullable=false, name="kvz_prefix")
     */
    protected $kvzPrefix;

    /**
     * @var integer
     * 
     * @ORM\Column(type="integer", length=11, nullable=false, name="cid_suffix_start")
     */
    protected $clientIdSuffixStart;

    /**
     * @var integer
     * 
     * @ORM\Column(type="integer", length=11, nullable=false, name="cid_suffix_end")
     */
    protected $clientIdSuffixEnd;

    /**
     * @var boolean
     * 
     * @ORM\Column(type="boolean", nullable=false, name="vectoring")
     */
    protected $vectoring;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=6, nullable=true, name="logon_port")
     */
    protected $logonPort;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=32, nullable=true, name="dslam_ip_adress")
     */
    protected $ipAddress;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=32, nullable=true, name="logon_id")
     */
    protected $logonUsername;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=32, nullable=true, name="logon_pw")
     */
    protected $logonPassword;

    /**
     * @var boolean
     * 
     * @ORM\Column(type="boolean", nullable=false, name="dslam_conf")
     */
    protected $configurable;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=32, nullable=true, name="dslam_type")
     */
    protected $type;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=8, nullable=true, name="acs_type")
     */
    protected $acsType = 'zeag';

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=32, nullable=true, name="dslam_ip_real")
     */
    protected $realIp;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=32, nullable=true, name="dslam_router")
     */
    protected $gateway;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=4, nullable=true, name="netz")
     */
    protected $subnetmask = '/24';

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=8, nullable=true, name="prozessor")
     */
    protected $processor;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=100, nullable=true, name="dslam_software_release")
     */
    protected $currentSoftwareRelease;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $defaultOntFirmwareRelease;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=4, nullable=true, name="vlan_pppoe_local")
     */
    protected $vlanPppoeLocal;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=4, nullable=true, name="vlan_iptv_local")
     */
    protected $vlanIptvLocal;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=4, nullable=true, name="vlan_acs_local")
     */
    protected $vlanAcsLocal;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=4, nullable=true, name="vlan_pppoe")
     */
    protected $vlanPppoe;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=4, nullable=true, name="vlan_iptv")
     */
    protected $vlanIptv;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=4, nullable=true, name="vlan_acs")
     */
    protected $vlanAcs;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=4, nullable=true, name="vlan_dcn")
     */
    protected $vlanDcn;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $acsQosProfile;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $ontSerialNumber;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $ontFirmwareVersion;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\VlanProfile")
     * @ORM\JoinTable(name="location_port_default_vlan_profiles", 
     *      joinColumns={@ORM\JoinColumn(name="location_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="vlan_profile_id", referencedColumnName="id")}
     * )
     */
    protected $portDefaultVlanProfiles;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\VlanProfile")
     * @ORM\JoinTable(name="location_customer_default_vlan_profiles", 
     *      joinColumns={@ORM\JoinColumn(name="location_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="vlan_profile_id", referencedColumnName="id")}
     * )
     */
    protected $customerDefaultVlanProfiles;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Location
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set kvzPrefix.
     *
     * @param string $kvzPrefix
     *
     * @return Location
     */
    public function setKvzPrefix($kvzPrefix)
    {
        $this->kvzPrefix = $kvzPrefix;

        return $this;
    }

    /**
     * Get kvzPrefix.
     *
     * @return string
     */
    public function getKvzPrefix()
    {
        return $this->kvzPrefix;
    }

    /**
     * Set clientIdSuffixStart.
     *
     * @param int $clientIdSuffixStart
     *
     * @return Location
     */
    public function setClientIdSuffixStart($clientIdSuffixStart)
    {
        $this->clientIdSuffixStart = $clientIdSuffixStart;

        return $this;
    }

    /**
     * Get clientIdSuffixStart.
     *
     * @return int
     */
    public function getClientIdSuffixStart()
    {
        return $this->clientIdSuffixStart;
    }

    /**
     * Set clientIdSuffixEnd.
     *
     * @param int $clientIdSuffixEnd
     *
     * @return Location
     */
    public function setClientIdSuffixEnd($clientIdSuffixEnd)
    {
        $this->clientIdSuffixEnd = $clientIdSuffixEnd;

        return $this;
    }

    /**
     * Get clientIdSuffixEnd.
     *
     * @return int
     */
    public function getClientIdSuffixEnd()
    {
        return $this->clientIdSuffixEnd;
    }

    /**
     * Set vectoring.
     *
     * @param bool $vectoring
     *
     * @return Location
     */
    public function setVectoring($vectoring)
    {
        $this->vectoring = $vectoring;

        return $this;
    }

    /**
     * Get vectoring.
     *
     * @return bool
     */
    public function getVectoring()
    {
        return $this->vectoring;
    }

    /**
     * Set logonPort.
     *
     * @param string|null $logonPort
     *
     * @return Location
     */
    public function setLogonPort($logonPort = null)
    {
        $this->logonPort = $logonPort;

        return $this;
    }

    /**
     * Get logonPort.
     *
     * @return string|null
     */
    public function getLogonPort()
    {
        return $this->logonPort;
    }

    /**
     * Set ipAddress.
     *
     * @param string|null $ipAddress
     *
     * @return Location
     */
    public function setIpAddress($ipAddress = null)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress.
     *
     * @return string|null
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set logonUsername.
     *
     * @param string|null $logonUsername
     *
     * @return Location
     */
    public function setLogonUsername($logonUsername = null)
    {
        $this->logonUsername = $logonUsername;

        return $this;
    }

    /**
     * Get logonUsername.
     *
     * @return string|null
     */
    public function getLogonUsername()
    {
        return $this->logonUsername;
    }

    /**
     * Set logonPassword.
     *
     * @param string|null $logonPassword
     *
     * @return Location
     */
    public function setLogonPassword($logonPassword = null)
    {
        $this->logonPassword = $logonPassword;

        return $this;
    }

    /**
     * Get logonPassword.
     *
     * @return string|null
     */
    public function getLogonPassword()
    {
        return $this->logonPassword;
    }

    /**
     * Set configurable.
     *
     * @param bool $configurable
     *
     * @return Location
     */
    public function setConfigurable($configurable)
    {
        $this->configurable = $configurable;

        return $this;
    }

    /**
     * Get configurable.
     *
     * @return bool
     */
    public function getConfigurable()
    {
        return $this->configurable;
    }

    /**
     * Set type.
     *
     * @param string|null $type
     *
     * @return Location
     */
    public function setType($type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set acsType.
     *
     * @param string|null $acsType
     *
     * @return Location
     */
    public function setAcsType($acsType = null)
    {
        $this->acsType = $acsType;

        return $this;
    }

    /**
     * Get acsType.
     *
     * @return string|null
     */
    public function getAcsType()
    {
        return $this->acsType;
    }

    /**
     * Set realIp.
     *
     * @param string|null $realIp
     *
     * @return Location
     */
    public function setRealIp($realIp = null)
    {
        $this->realIp = $realIp;

        return $this;
    }

    /**
     * Get realIp.
     *
     * @return string|null
     */
    public function getRealIp()
    {
        return $this->realIp;
    }

    /**
     * Set gateway.
     *
     * @param string|null $gateway
     *
     * @return Location
     */
    public function setGateway($gateway = null)
    {
        $this->gateway = $gateway;

        return $this;
    }

    /**
     * Get gateway.
     *
     * @return string|null
     */
    public function getGateway()
    {
        return $this->gateway;
    }

    /**
     * Set subnetmask.
     *
     * @param string|null $subnetmask
     *
     * @return Location
     */
    public function setSubnetmask($subnetmask = null)
    {
        $this->subnetmask = $subnetmask;

        return $this;
    }

    /**
     * Get subnetmask.
     *
     * @return string|null
     */
    public function getSubnetmask()
    {
        return $this->subnetmask;
    }

    /**
     * Set processor.
     *
     * @param string|null $processor
     *
     * @return Location
     */
    public function setProcessor($processor = null)
    {
        $this->processor = $processor;

        return $this;
    }

    /**
     * Get processor.
     *
     * @return string|null
     */
    public function getProcessor()
    {
        return $this->processor;
    }

    /**
     * Set currentSoftwareRelease.
     *
     * @param string|null $currentSoftwareRelease
     *
     * @return Location
     */
    public function setCurrentSoftwareRelease($currentSoftwareRelease = null)
    {
        $this->currentSoftwareRelease = $currentSoftwareRelease;

        return $this;
    }

    /**
     * Get currentSoftwareRelease.
     *
     * @return string|null
     */
    public function getCurrentSoftwareRelease()
    {
        return $this->currentSoftwareRelease;
    }

    /**
     * Set vlanPppoeLocal.
     *
     * @param string|null $vlanPppoeLocal
     *
     * @return Location
     */
    public function setVlanPppoeLocal($vlanPppoeLocal = null)
    {
        $this->vlanPppoeLocal = $vlanPppoeLocal;

        return $this;
    }

    /**
     * Get vlanPppoeLocal.
     *
     * @return string|null
     */
    public function getVlanPppoeLocal()
    {
        return $this->vlanPppoeLocal;
    }

    /**
     * Set vlanIptvLocal.
     *
     * @param string|null $vlanIptvLocal
     *
     * @return Location
     */
    public function setVlanIptvLocal($vlanIptvLocal = null)
    {
        $this->vlanIptvLocal = $vlanIptvLocal;

        return $this;
    }

    /**
     * Get vlanIptvLocal.
     *
     * @return string|null
     */
    public function getVlanIptvLocal()
    {
        return $this->vlanIptvLocal;
    }

    /**
     * Set vlanAcsLocal.
     *
     * @param string|null $vlanAcsLocal
     *
     * @return Location
     */
    public function setVlanAcsLocal($vlanAcsLocal = null)
    {
        $this->vlanAcsLocal = $vlanAcsLocal;

        return $this;
    }

    /**
     * Get vlanAcsLocal.
     *
     * @return string|null
     */
    public function getVlanAcsLocal()
    {
        return $this->vlanAcsLocal;
    }

    /**
     * Set vlanPppoe.
     *
     * @param string|null $vlanPppoe
     *
     * @return Location
     */
    public function setVlanPppoe($vlanPppoe = null)
    {
        $this->vlanPppoe = $vlanPppoe;

        return $this;
    }

    /**
     * Get vlanPppoe.
     *
     * @return string|null
     */
    public function getVlanPppoe()
    {
        return $this->vlanPppoe;
    }

    /**
     * Set vlanIptv.
     *
     * @param string|null $vlanIptv
     *
     * @return Location
     */
    public function setVlanIptv($vlanIptv = null)
    {
        $this->vlanIptv = $vlanIptv;

        return $this;
    }

    /**
     * Get vlanIptv.
     *
     * @return string|null
     */
    public function getVlanIptv()
    {
        return $this->vlanIptv;
    }

    /**
     * Set vlanAcs.
     *
     * @param string|null $vlanAcs
     *
     * @return Location
     */
    public function setVlanAcs($vlanAcs = null)
    {
        $this->vlanAcs = $vlanAcs;

        return $this;
    }

    /**
     * Get vlanAcs.
     *
     * @return string|null
     */
    public function getVlanAcs()
    {
        return $this->vlanAcs;
    }

    /**
     * Set vlanDcn.
     *
     * @param string|null $vlanDcn
     *
     * @return Location
     */
    public function setVlanDcn($vlanDcn = null)
    {
        $this->vlanDcn = $vlanDcn;

        return $this;
    }

    /**
     * Get vlanDcn.
     *
     * @return string|null
     */
    public function getVlanDcn()
    {
        return $this->vlanDcn;
    }

    /**
     * Set network.
     *
     * @param \AppBundle\Entity\Network|null $network
     *
     * @return Location
     */
    public function setNetwork(\AppBundle\Entity\Network $network = null)
    {
        $this->network = $network;

        return $this;
    }

    /**
     * Get network.
     *
     * @return \AppBundle\Entity\Network|null
     */
    public function getNetwork()
    {
        return $this->network;
    }

    /**
     * Set acsQosProfile.
     *
     * @param string|null $acsQosProfile
     *
     * @return Location
     */
    public function setAcsQosProfile($acsQosProfile = null)
    {
        $this->acsQosProfile = $acsQosProfile;

        return $this;
    }

    /**
     * Get acsQosProfile.
     *
     * @return string|null
     */
    public function getAcsQosProfile()
    {
        return $this->acsQosProfile;
    }

    /**
     * Set defaultOntFirmwareRelease.
     *
     * @param string|null $defaultOntFirmwareRelease
     *
     * @return Location
     */
    public function setDefaultOntFirmwareRelease($defaultOntFirmwareRelease = null)
    {
        $this->defaultOntFirmwareRelease = $defaultOntFirmwareRelease;

        return $this;
    }

    /**
     * Get defaultOntFirmwareRelease.
     *
     * @return string|null
     */
    public function getDefaultOntFirmwareRelease()
    {
        return $this->defaultOntFirmwareRelease;
    }

    /**
     * Set ontSerialNumber.
     *
     * @param string|null $ontSerialNumber
     *
     * @return Location
     */
    public function setOntSerialNumber($ontSerialNumber = null)
    {
        $this->ontSerialNumber = $ontSerialNumber;

        return $this;
    }

    /**
     * Get ontSerialNumber.
     *
     * @return string|null
     */
    public function getOntSerialNumber()
    {
        return $this->ontSerialNumber;
    }

    /**
     * Set ontFirmwareVersion.
     *
     * @param string|null $ontFirmwareVersion
     *
     * @return Location
     */
    public function setOntFirmwareVersion($ontFirmwareVersion = null)
    {
        $this->ontFirmwareVersion = $ontFirmwareVersion;

        return $this;
    }

    /**
     * Get ontFirmwareVersion.
     *
     * @return string|null
     */
    public function getOntFirmwareVersion()
    {
        return $this->ontFirmwareVersion;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->portDefaultVlanProfiles = new ArrayCollection();
        $this->customerDefaultVlanProfiles = new ArrayCollection();
    }

    /**
     * Add portDefaultVlanProfile.
     *
     * @param \AppBundle\Entity\VlanProfile $portDefaultVlanProfile
     *
     * @return Location
     */
    public function addPortDefaultVlanProfile(\AppBundle\Entity\VlanProfile $portDefaultVlanProfile)
    {
        $this->portDefaultVlanProfiles[] = $portDefaultVlanProfile;

        return $this;
    }

    /**
     * Remove portDefaultVlanProfile.
     *
     * @param \AppBundle\Entity\VlanProfile $portDefaultVlanProfile
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePortDefaultVlanProfile(\AppBundle\Entity\VlanProfile $portDefaultVlanProfile)
    {
        return $this->portDefaultVlanProfiles->removeElement($portDefaultVlanProfile);
    }

    /**
     * Get portDefaultVlanProfiles.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPortDefaultVlanProfiles()
    {
        return $this->portDefaultVlanProfiles;
    }

    /**
     * Add customerDefaultVlanProfile.
     *
     * @param \AppBundle\Entity\VlanProfile $customerDefaultVlanProfile
     *
     * @return Location
     */
    public function addCustomerDefaultVlanProfile(\AppBundle\Entity\VlanProfile $customerDefaultVlanProfile)
    {
        $this->customerDefaultVlanProfiles[] = $customerDefaultVlanProfile;

        return $this;
    }

    /**
     * Remove customerDefaultVlanProfile.
     *
     * @param \AppBundle\Entity\VlanProfile $customerDefaultVlanProfile
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCustomerDefaultVlanProfile(\AppBundle\Entity\VlanProfile $customerDefaultVlanProfile)
    {
        return $this->customerDefaultVlanProfiles->removeElement($customerDefaultVlanProfile);
    }

    /**
     * Get customerDefaultVlanProfiles.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomerDefaultVlanProfiles()
    {
        return $this->customerDefaultVlanProfiles;
    }
}
