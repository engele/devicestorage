<?php

/**
 * This file is part of the wkv project.
 */

namespace Wkv\Entity;

use Doctrine\ORM\Mapping as ORM;

/*
 * Entity class for product attribute option
 * 
 * @ORM\Entity
 * @ORM\Table(name="product_attribute_options")
 */
class ProductAttributeOption
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Option name
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=200, nullable=false)
     */
    protected $name;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ProductAttributeOption
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
