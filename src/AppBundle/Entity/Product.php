<?php

/**
 * This file is part of the wkv project.
 * 
 * only for ikv imports
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity class for product
 * 
 * @ORM\Entity
 * @ORM\Table(name="products")
 */
class Product
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Product identifier
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=20, nullable=false, unique=true)
     */
    protected $identifier;

    /**
     * Product name
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=200, nullable=false)
     */
    protected $name;

    /**
     * Purtel product id
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer", length=8, nullable=true)
     */
    protected $purtelProductId;

    /**
     * Price (net)
     * 
     * @var float
     * 
     * @ORM\Column(type="decimal", precision=12, scale=5, nullable=true)
     */
    protected $defaultPriceNet;

    /**
     * Price (gross)
     * 
     * @var float
     * 
     * @ORM\Column(type="decimal", precision=12, scale=5, nullable=true)
     */
    protected $defaultPriceGross;

    /**
     * Item category
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    protected $itemCategory;

    /**
     * Product type
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $type;

    /**
     * Is active?
     * 
     * @var boolean
     * 
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $active = true;
}
