<?php

/**
 * This file is part of the DICLINA project.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entity class for VLAN Profiles
 * 
 * @ORM\Entity
 * @ORM\Table(name="vlan_profiles")
 */
class VlanProfile
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * 
     * @ORM\Column(type="text", length=512, nullable=false)
     */
    protected $name;

    /**
     * Port VLAN (also known as nativ VLAN)
     * 
     * @var integer
     * 
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $pvid;

    /**
     * @var Doctrine\Common\Collections\ArrayCollection
     * 
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Vlan", mappedBy="vlanProfile")
     */
    protected $vlans;

    /**
     * Constructor
     */
    public function __construct() {
        $this->vlans = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return VlanProfile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set pvid.
     *
     * @param int|null $pvid
     *
     * @return VlanProfile
     */
    public function setPvid($pvid = null)
    {
        $this->pvid = $pvid;

        return $this;
    }

    /**
     * Get pvid.
     *
     * @return int|null
     */
    public function getPvid()
    {
        return $this->pvid;
    }

    /**
     * Add vlan.
     *
     * @param \AppBundle\Entity\Vlan $vlan
     *
     * @return VlanProfile
     */
    public function addVlan(\AppBundle\Entity\Vlan $vlan)
    {
        $this->vlans[] = $vlan;

        return $this;
    }

    /**
     * Remove vlan.
     *
     * @param \AppBundle\Entity\Vlan $vlan
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVlan(\AppBundle\Entity\Vlan $vlan)
    {
        return $this->vlans->removeElement($vlan);
    }

    /**
     * Get vlans.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVlans()
    {
        return $this->vlans;
    }
}
