<?php

/**
 * This file is part of the DICLINA project.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity class for spectrum profiles
 * 
 * @ORM\Entity
 * @ORM\Table(name="spectrum_profiles")
 */
class SpectrumProfile
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Product identifier
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=64, nullable=false, unique=true)
     */
    protected $identifier;

    /**
     * Profile
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=30, nullable=false)
     */
    protected $profile;

    /**
     * @var \AppBundle\Entity\Location\CardType
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location\CardType")
     * @ORM\JoinColumn(name="connection_type_id", referencedColumnName="id")
     */
    protected $connectionType;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set profile.
     *
     * @param string $profile
     *
     * @return SpectrumProfile
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile.
     *
     * @return string
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set connectionType.
     *
     * @param \AppBundle\Entity\Location\CardType|null $connectionType
     *
     * @return SpectrumProfile
     */
    public function setConnectionType(\AppBundle\Entity\Location\CardType $connectionType = null)
    {
        $this->connectionType = $connectionType;

        return $this;
    }

    /**
     * Get connectionType.
     *
     * @return \AppBundle\Entity\Location\CardType|null
     */
    public function getConnectionType()
    {
        return $this->connectionType;
    }

    /**
     * Set identifier.
     *
     * @param string $identifier
     *
     * @return SpectrumProfile
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }
}
