<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="purtel_admin_accounts")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PurtelAdminAccountRepository")
 */
class PurtelAdminAccount
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    private $identifier;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinTable(name="users_to_purtel_admin_accounts", 
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="purtel_admin_account_id", referencedColumnName="id")}
     * )
     */
    private $authorizedUsers;

    /**
     * Constructor
     */
    public function __construct() {
        $this->authorizedUsers = [];
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier.
     *
     * @param string $identifier
     *
     * @return PurtelAdminAccount
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return PurtelAdminAccount
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password.
     *
     * @param string $password
     *
     * @return PurtelAdminAccount
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Add authorizedUser.
     *
     * @param \AppBundle\Entity\User $authorizedUser
     *
     * @return PurtelAdminAccount
     */
    public function addAuthorizedUser(\AppBundle\Entity\User $authorizedUser)
    {
        $this->authorizedUsers[] = $authorizedUser;

        return $this;
    }

    /**
     * Remove authorizedUser.
     *
     * @param \AppBundle\Entity\User $authorizedUser
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeAuthorizedUser(\AppBundle\Entity\User $authorizedUser)
    {
        return $this->authorizedUsers->removeElement($authorizedUser);
    }

    /**
     * Get authorizedUsers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAuthorizedUsers()
    {
        return $this->authorizedUsers;
    }
}
