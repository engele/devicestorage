<?php

/**
 * This file is part of the wkv project.
 */

namespace Wkv\Entity;

use Doctrine\ORM\Mapping as ORM;

/*
 * Entity class for product attribute
 * 
 * @ORM\Entity
 * @ORM\Table(name="product_attributes")
 */
class ProductAttribute
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Attribute name
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=200, nullable=false)
     */
    protected $name;

    /**
     * Is active?
     * 
     * @var boolean
     * 
     * @ORM\Column(type="boolean")
     */
    protected $active;

    /**
     * Attribute options
     * 
     * @var array
     * 
     * @ORM\ManyToMany(targetEntity="Wkv\Entity\ForWkv\ProductAttributeOption")
     * @ORM\JoinTable(name="product_attributes_to_attribute_options",
     *      joinColumns={@ORM\JoinColumn(name="attribute_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="option_id", referencedColumnName="id")}
     * )
     */
    protected $options;

    /**
     * Constructor
     */
    public function __construct() {
        $this->options = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ProductAttribute
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return ProductAttribute
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add option
     *
     * @param \Wkv\Entity\ForWkv\ProductAttributeOption $option
     *
     * @return ProductAttribute
     */
    public function addOption(\Wkv\Entity\ForWkv\ProductAttributeOption $option)
    {
        $this->options[] = $option;

        return $this;
    }

    /**
     * Remove option
     *
     * @param \Wkv\Entity\ForWkv\ProductAttributeOption $option
     */
    public function removeOption(\Wkv\Entity\ForWkv\ProductAttributeOption $option)
    {
        $this->options->removeElement($option);
    }

    /**
     * Get options
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptions()
    {
        return $this->options;
    }
}
