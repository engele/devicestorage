<?php

/**
 * This file is part of the wkv project.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity class for IpRange
 * 
 * @ORM\Entity
 * @ORM\Table(name="ip_ranges")
 */
class IpRange
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=32, nullable=false, name="start_address")
     */
    protected $startAddress;

    /**
     * @var string
     * 
     *  @ORM\Column(type="string", length=32, nullable=false, name="end_address")
     */
    protected $endAddress;

    /**
     * @var string
     * 
     *  @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $name;

    /**
     * @var string
     * 
     *  @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $subnetmask;

    /**
     * @var string
     * 
     *  @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $gateway;

    /**
     * @var string
     * 
     *  @ORM\Column(type="string", length=16, nullable=true, name="vlan_ID")
     */
    protected $vlanId;

    /**
     * @var string
     * 
     *  @ORM\Column(type="string", length=32, nullable=true, name="pppoe_ip")
     */
    protected $pppoeIp;

    /**
     * @var string
     * 
     *  @ORM\Column(type="string", length=32, nullable=true, name="pppoe_type")
     */
    protected $pppoeType;

    /**
     * @var string
     * 
     *  @ORM\Column(type="string", length=32, nullable=true, name="pppoe_location")
     */
    protected $pppoeLocation;

    /**
     * @var string
     * 
     *  @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $uplinkProvider;

    /**
     * @var string
     * 
     *  @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $dns1;

    /**
     * @var string
     * 
     *  @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $dns2;

    /**
     * Count amount of possible ips for this range
     * 
     * @return integer|null
     */
    public function getIpsTotal()
    {
        if (null !== $this->startAddress && null !== $this->endAddress) {
            return ip2long($this->endAddress) - (ip2long($this->startAddress) - 1);
        }

        return null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startAddress
     *
     * @param string $startAddress
     *
     * @return IpRange
     */
    public function setStartAddress($startAddress)
    {
        $this->startAddress = $startAddress;

        return $this;
    }

    /**
     * Get startAddress
     *
     * @return string
     */
    public function getStartAddress()
    {
        return $this->startAddress;
    }

    /**
     * Set endAddress
     *
     * @param string $endAddress
     *
     * @return IpRange
     */
    public function setEndAddress($endAddress)
    {
        $this->endAddress = $endAddress;

        return $this;
    }

    /**
     * Get endAddress
     *
     * @return string
     */
    public function getEndAddress()
    {
        return $this->endAddress;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return IpRange
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set subnetmask
     *
     * @param string $subnetmask
     *
     * @return IpRange
     */
    public function setSubnetmask($subnetmask)
    {
        $this->subnetmask = $subnetmask;

        return $this;
    }

    /**
     * Get subnetmask
     *
     * @return string
     */
    public function getSubnetmask()
    {
        return $this->subnetmask;
    }

    /**
     * Set gateway
     *
     * @param string $gateway
     *
     * @return IpRange
     */
    public function setGateway($gateway)
    {
        $this->gateway = $gateway;

        return $this;
    }

    /**
     * Get gateway
     *
     * @return string
     */
    public function getGateway()
    {
        return $this->gateway;
    }

    /**
     * Set vlanId
     *
     * @param string $vlanId
     *
     * @return IpRange
     */
    public function setVlanId($vlanId)
    {
        $this->vlanId = $vlanId;

        return $this;
    }

    /**
     * Get vlanId
     *
     * @return string
     */
    public function getVlanId()
    {
        return $this->vlanId;
    }

    /**
     * Set pppoeType
     *
     * @param string $pppoeType
     *
     * @return IpRange
     */
    public function setPppoeType($pppoeType)
    {
        $this->pppoeType = $pppoeType;

        return $this;
    }

    /**
     * Get pppoeType
     *
     * @return string
     */
    public function getPppoeType()
    {
        return $this->pppoeType;
    }

    /**
     * Set pppoeLocation
     *
     * @param string $pppoeLocation
     *
     * @return IpRange
     */
    public function setPppoeLocation($pppoeLocation)
    {
        $this->pppoeLocation = $pppoeLocation;

        return $this;
    }

    /**
     * Get pppoeLocation
     *
     * @return string
     */
    public function getPppoeLocation()
    {
        return $this->pppoeLocation;
    }

    /**
     * Set pppoeIp
     *
     * @param string $pppoeIp
     *
     * @return IpRange
     */
    public function setPppoeIp($pppoeIp)
    {
        $this->pppoeIp = $pppoeIp;

        return $this;
    }

    /**
     * Get pppoeIp
     *
     * @return string
     */
    public function getPppoeIp()
    {
        return $this->pppoeIp;
    }

    /**
     * Set uplinkProvider.
     *
     * @param string|null $uplinkProvider
     *
     * @return IpRange
     */
    public function setUplinkProvider($uplinkProvider = null)
    {
        $this->uplinkProvider = $uplinkProvider;

        return $this;
    }

    /**
     * Get uplinkProvider.
     *
     * @return string|null
     */
    public function getUplinkProvider()
    {
        return $this->uplinkProvider;
    }

    /**
     * Set dns1.
     *
     * @param string|null $dns1
     *
     * @return IpRange
     */
    public function setDns1($dns1 = null)
    {
        $this->dns1 = $dns1;

        return $this;
    }

    /**
     * Get dns1.
     *
     * @return string|null
     */
    public function getDns1()
    {
        return $this->dns1;
    }

    /**
     * Set dns2.
     *
     * @param string|null $dns2
     *
     * @return IpRange
     */
    public function setDns2($dns2 = null)
    {
        $this->dns2 = $dns2;

        return $this;
    }

    /**
     * Get dns2.
     *
     * @return string|null
     */
    public function getDns2()
    {
        return $this->dns2;
    }
}
