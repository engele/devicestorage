<?php

/**
 * This file is part of the wkv project.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity class for Location
 * 
 * @ORM\Entity
 * @ORM\Table(name="networks")
 */
class Network
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=32, nullable=false, name="id_string")
     */
    protected $identifier;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=128, nullable=false, name="name")
     */
    protected $name;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=64, nullable=true, name="partner")
     */
    protected $partner;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=30, nullable=false, name="network_ready")
     */
    protected $networkReady;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=10, nullable=true, name="area_code")
     */
    protected $areaCode;

    /**
     * @var string
     * 
     * @ORM\Column(type="decimal", precision=7, scale=2, nullable=true, name="fix_cost")
     */
    protected $fixCost;

    /**
     * @var string
     * 
     * @ORM\Column(type="decimal", precision=7, scale=2, nullable=true, name="var_cost")
     */
    protected $varCost;

    /**
     * @var string
     * 
     * @ORM\Column(type="decimal", precision=7, scale=2, nullable=true, name="var_percent")
     */
    protected $varPercent;

    /**
     * @var boolean
     * 
     * @ORM\Column(type="boolean", nullable=false, name="no_stat")
     */
    protected $noStat;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier.
     *
     * @param string $identifier
     *
     * @return Network
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Network
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set partner.
     *
     * @param string|null $partner
     *
     * @return Network
     */
    public function setPartner($partner = null)
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * Get partner.
     *
     * @return string|null
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * Set networkReady.
     *
     * @param string $networkReady
     *
     * @return Network
     */
    public function setNetworkReady($networkReady)
    {
        $this->networkReady = $networkReady;

        return $this;
    }

    /**
     * Get networkReady.
     *
     * @return string
     */
    public function getNetworkReady()
    {
        return $this->networkReady;
    }

    /**
     * Set areaCode.
     *
     * @param string|null $areaCode
     *
     * @return Network
     */
    public function setAreaCode($areaCode = null)
    {
        $this->areaCode = $areaCode;

        return $this;
    }

    /**
     * Get areaCode.
     *
     * @return string|null
     */
    public function getAreaCode()
    {
        return $this->areaCode;
    }

    /**
     * Set fixCost.
     *
     * @param string|null $fixCost
     *
     * @return Network
     */
    public function setFixCost($fixCost = null)
    {
        $this->fixCost = $fixCost;

        return $this;
    }

    /**
     * Get fixCost.
     *
     * @return string|null
     */
    public function getFixCost()
    {
        return $this->fixCost;
    }

    /**
     * Set varCost.
     *
     * @param string|null $varCost
     *
     * @return Network
     */
    public function setVarCost($varCost = null)
    {
        $this->varCost = $varCost;

        return $this;
    }

    /**
     * Get varCost.
     *
     * @return string|null
     */
    public function getVarCost()
    {
        return $this->varCost;
    }

    /**
     * Set varPercent.
     *
     * @param string|null $varPercent
     *
     * @return Network
     */
    public function setVarPercent($varPercent = null)
    {
        $this->varPercent = $varPercent;

        return $this;
    }

    /**
     * Get varPercent.
     *
     * @return string|null
     */
    public function getVarPercent()
    {
        return $this->varPercent;
    }

    /**
     * Set noStat.
     *
     * @param bool $noStat
     *
     * @return Network
     */
    public function setNoStat($noStat)
    {
        $this->noStat = $noStat;

        return $this;
    }

    /**
     * Get noStat.
     *
     * @return bool
     */
    public function getNoStat()
    {
        return $this->noStat;
    }
}
