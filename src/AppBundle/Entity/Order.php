<?php

/**
 * This file is part of the wkv project.
 */

namespace Wkv\Entity;

use Doctrine\ORM\Mapping as ORM;

/*
 * Entity class for order
 * 
 * @ORM\Entity
 * @ORM\Table(name="orders")
 */
class Order
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Customer
     * 
     * @var Customer
     * 
     * @ORM\ManyToOne(targetEntity="Wkv\Entity\ForWkv\Customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    protected $customer;

    /**
     * Sku
     * 
     * @var Sku
     * 
     * @ORM\ManyToOne(targetEntity="Wkv\Entity\ForWkv\Sku")
     * @ORM\JoinColumn(name="sku", referencedColumnName="sku")
     */
    protected $sku;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customer
     *
     * @param \Wkv\Entity\ForWkv\Customer $customer
     *
     * @return Order
     */
    public function setCustomer(\Wkv\Entity\ForWkv\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \Wkv\Entity\ForWkv\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set sku
     *
     * @param \Wkv\Entity\ForWkv\Sku $sku
     *
     * @return Order
     */
    public function setSku(\Wkv\Entity\ForWkv\Sku $sku = null)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return \Wkv\Entity\ForWkv\Sku
     */
    public function getSku()
    {
        return $this->sku;
    }
}
