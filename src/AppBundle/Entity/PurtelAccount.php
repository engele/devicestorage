<?php

/**
 * This file is part of the wkv project.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity class for PurtelAccount
 * 
 * @ORM\Entity
 * @ORM\Table(name="purtel_account", 
 *      indexes={
 *          @ORM\Index(columns={"cust_id"}), 
 *          @ORM\Index(columns={"purtel_login"})
 *      }
 * )
 */
class PurtelAccount
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=20, nullable=true, name="purtel_login")
     */
    protected $purtelLogin;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=20, nullable=true, name="purtel_password")
     */
    protected $purtelPassword;

    /**
     * @var string
     * 
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $master;

    /**
     * @var string
     * 
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $porting;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=64, nullable=true, name="nummer")
     */
    protected $nummer;

    /**
     * Area Code - must be string because it may start with 0
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    protected $areaCode;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $nummernblock;

    /**
     * @var string
     * 
     * @ORM\Column(type="integer", length=4, nullable=false)
     */
    protected $typ = 1;

    /**
     * @var string
     * 
     * @ORM\Column(type="integer", length=11, nullable=true, name="cust_id")
     */
    protected $customerId;

    /**
     * @var string
     * 
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $phoneBookInformation;

    /**
     * @var string
     * 
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $phoneBookDigitalMedia;

    /**
     * @var string
     * 
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $phoneBookInverseSearch;

    /**
     * @var string
     * 
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $itemizedBill;

    /**
     * @var string
     * 
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $itemizedBillAnonymized;

    /**
     * @var string
     * 
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $itemizedBillShorted;

    /**
     * Convert object to array (for compatibility reasons)
     * 
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'purtel_login' => $this->purtelLogin,
            'purtel_password' => $this->purtelPassword,
            'master' => $this->master,
            'porting' => $this->porting,
            'area_code' => $this->areaCode,
            'nummer' => $this->nummer,
            'nummernblock' => $this->nummernblock,
            'typ' => $this->typ,
            'cust_id' => $this->customerId,
            'phone_book_information' => $this->phoneBookInformation,
            'phone_book_digital_media' => $this->phoneBookDigitalMedia,
            'phone_book_inverse_search' => $this->phoneBookInverseSearch,
            'itemized_bill' => $this->itemizedBill,
            'itemized_bill_anonymized' => $this->itemizedBillAnonymized,
            'itemized_bill_shorted' => $this->itemizedBillShorted,
        ];
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set purtelLogin
     *
     * @param string $purtelLogin
     *
     * @return PurtelAccount
     */
    public function setPurtelLogin($purtelLogin)
    {
        $this->purtelLogin = $purtelLogin;

        return $this;
    }

    /**
     * Get purtelLogin
     *
     * @return string
     */
    public function getPurtelLogin()
    {
        return $this->purtelLogin;
    }

    /**
     * Set purtelPassword
     *
     * @param string $purtelPassword
     *
     * @return PurtelAccount
     */
    public function setPurtelPassword($purtelPassword)
    {
        $this->purtelPassword = $purtelPassword;

        return $this;
    }

    /**
     * Get purtelPassword
     *
     * @return string
     */
    public function getPurtelPassword()
    {
        return $this->purtelPassword;
    }

    /**
     * Set master
     *
     * @param boolean $master
     *
     * @return PurtelAccount
     */
    public function setMaster($master)
    {
        $this->master = $master;

        return $this;
    }

    /**
     * Get master
     *
     * @return boolean
     */
    public function getMaster()
    {
        return $this->master;
    }

    /**
     * Set porting
     *
     * @param boolean $porting
     *
     * @return PurtelAccount
     */
    public function setPorting($porting)
    {
        $this->porting = $porting;

        return $this;
    }

    /**
     * Get porting
     *
     * @return boolean
     */
    public function getPorting()
    {
        return $this->porting;
    }

    /**
     * Set nummer
     *
     * @param string $nummer
     *
     * @return PurtelAccount
     */
    public function setNummer($nummer)
    {
        $this->nummer = $nummer;

        return $this;
    }

    /**
     * Get nummer
     *
     * @return string
     */
    public function getNummer()
    {
        return $this->nummer;
    }

    /**
     * Set nummernblock
     *
     * @param string $nummernblock
     *
     * @return PurtelAccount
     */
    public function setNummernblock($nummernblock)
    {
        $this->nummernblock = $nummernblock;

        return $this;
    }

    /**
     * Get nummernblock
     *
     * @return string
     */
    public function getNummernblock()
    {
        return $this->nummernblock;
    }

    /**
     * Set typ
     *
     * @param integer $typ
     *
     * @return PurtelAccount
     */
    public function setTyp($typ)
    {
        $this->typ = $typ;

        return $this;
    }

    /**
     * Get typ
     *
     * @return integer
     */
    public function getTyp()
    {
        return $this->typ;
    }

    /**
     * Set customerId
     *
     * @param integer $customerId
     *
     * @return PurtelAccount
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * Get customerId
     *
     * @return integer
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * Set phoneBookInformation
     *
     * @param boolean $phoneBookInformation
     *
     * @return PurtelAccount
     */
    public function setPhoneBookInformation($phoneBookInformation)
    {
        $this->phoneBookInformation = $phoneBookInformation;

        return $this;
    }

    /**
     * Get phoneBookInformation
     *
     * @return boolean
     */
    public function getPhoneBookInformation()
    {
        return $this->phoneBookInformation;
    }

    /**
     * Set phoneBookDigitalMedia
     *
     * @param boolean $phoneBookDigitalMedia
     *
     * @return PurtelAccount
     */
    public function setPhoneBookDigitalMedia($phoneBookDigitalMedia)
    {
        $this->phoneBookDigitalMedia = $phoneBookDigitalMedia;

        return $this;
    }

    /**
     * Get phoneBookDigitalMedia
     *
     * @return boolean
     */
    public function getPhoneBookDigitalMedia()
    {
        return $this->phoneBookDigitalMedia;
    }

    /**
     * Set phoneBookInverseSearch
     *
     * @param boolean $phoneBookInverseSearch
     *
     * @return PurtelAccount
     */
    public function setPhoneBookInverseSearch($phoneBookInverseSearch)
    {
        $this->phoneBookInverseSearch = $phoneBookInverseSearch;

        return $this;
    }

    /**
     * Get phoneBookInverseSearch
     *
     * @return boolean
     */
    public function getPhoneBookInverseSearch()
    {
        return $this->phoneBookInverseSearch;
    }

    /**
     * Set itemizedBill
     *
     * @param boolean $itemizedBill
     *
     * @return PurtelAccount
     */
    public function setItemizedBill($itemizedBill)
    {
        $this->itemizedBill = $itemizedBill;

        return $this;
    }

    /**
     * Get itemizedBill
     *
     * @return boolean
     */
    public function getItemizedBill()
    {
        return $this->itemizedBill;
    }

    /**
     * Set itemizedBillAnonymized
     *
     * @param boolean $itemizedBillAnonymized
     *
     * @return PurtelAccount
     */
    public function setItemizedBillAnonymized($itemizedBillAnonymized)
    {
        $this->itemizedBillAnonymized = $itemizedBillAnonymized;

        return $this;
    }

    /**
     * Get itemizedBillAnonymized
     *
     * @return boolean
     */
    public function getItemizedBillAnonymized()
    {
        return $this->itemizedBillAnonymized;
    }

    /**
     * Set itemizedBillShorted
     *
     * @param boolean $itemizedBillShorted
     *
     * @return PurtelAccount
     */
    public function setItemizedBillShorted($itemizedBillShorted)
    {
        $this->itemizedBillShorted = $itemizedBillShorted;

        return $this;
    }

    /**
     * Get itemizedBillShorted
     *
     * @return boolean
     */
    public function getItemizedBillShorted()
    {
        return $this->itemizedBillShorted;
    }

    /**
     * Set areaCode.
     *
     * @param string|null $areaCode
     *
     * @return PurtelAccount
     */
    public function setAreaCode($areaCode = null)
    {
        $this->areaCode = $areaCode;

        return $this;
    }

    /**
     * Get areaCode.
     *
     * @return string|null
     */
    public function getAreaCode()
    {
        return $this->areaCode;
    }
}
