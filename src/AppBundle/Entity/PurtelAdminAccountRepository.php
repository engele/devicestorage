<?php

/**
 * 
 */

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * 
 */
class PurtelAdminAccountRepository extends EntityRepository
{
    /**
     * Find all purtel-admin-accounts for $user
     */
    public function findByAuthorizedUser($user)
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.authorizedUsers', 'au')
            ->where('au = :user')
            ->setParameter('user', $user)
            ->getQuery()->getResult();
    }
}
