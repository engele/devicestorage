<?php

/**
 * This file is part of the wkv project.
 */

namespace Wkv\Entity;

use Doctrine\ORM\Mapping as ORM;

/*
 * Entity class for comparison item variation
 * 
 * @ORM\Entity
 * @ORM\Table(name="customers")
 */
class Customer
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
