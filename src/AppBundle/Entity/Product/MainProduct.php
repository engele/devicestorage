<?php

/**
 * This file is part of the wkv project.
 */

namespace AppBundle\Entity\Product;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity class for produkte (current main product)
 * 
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Product\MainProductRepository")
 * @ORM\Table(name="produkte")
 */
class MainProduct
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Product identifier
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=64, nullable=false, unique=true, name="produkt")
     */
    protected $identifier;
// @ORM\Column(type="integer", nullable=true, name="group_id")
    /**
     * Group id
     * 
     * @var \AppBundle\Entity\Product\MainProductCategory
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Product\MainProductCategory")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id", nullable=true)
     */
    protected $category;

    /**
     * Service profile
     * 
     * @var string
     * 
     * @ORM\Column(type="integer", nullable=true, name="produkt_index")
     */
    protected $xdslServiceProfile;

    /**
     * RTX profile
     * 
     * @var string
     * 
     * @ORM\Column(type="integer", nullable=true, name="rtx_profile")
     */
    protected $rtxProfile;

    /**
     * GPON download
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=32, nullable=true, name="gpon_download")
     */
    protected $gponDownload;

    /**
     * GPON upload
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=32, nullable=true, name="gpon_upload")
     */
    protected $gponUpload;

    /**
     * Ethernet up down
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=32, nullable=true, name="ethernet_up_down")
     */
    protected $ethernetUpDown;

    /**
     * Download
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=30, nullable=true, name="down")
     */
    protected $download;

    /**
     * Upload
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=30, nullable=true, name="up")
     */
    protected $upload;

    /**
     * G.fast upstream rate
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    protected $gfastUpstreamRate;

    /**
     * G.fast downstream rate
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    protected $gfastDownstreamRate;

    /**
     * Product name
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=255, nullable=false, name="produkt_bezeichnung")
     */
    protected $name;

    /**
     * Price (net)
     * 
     * @var float
     * 
     * @ORM\Column(type="decimal", precision=7, scale=2, nullable=true, name="Preis_Netto")
     */
    protected $priceNet;

    /**
     * Price (gross)
     * 
     * @var float
     * 
     * @ORM\Column(type="decimal", precision=7, scale=2, nullable=true, name="Preis_Brutto")
     */
    protected $priceGross;

    /**
     * Is active?
     * 
     * @var boolean
     * 
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $active = true;

    /**
     * Card-Type (Broadband connection type)
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location\CardType")
     * @ORM\JoinColumn(name="card_type_id", referencedColumnName="id", nullable=true)
     */
    protected $cardType;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier.
     *
     * @param string $identifier
     *
     * @return MainProduct
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set xdslServiceProfile.
     *
     * @param int|null $xdslServiceProfile
     *
     * @return MainProduct
     */
    public function setXdslServiceProfile($xdslServiceProfile = null)
    {
        $this->xdslServiceProfile = $xdslServiceProfile;

        return $this;
    }

    /**
     * Get xdslServiceProfile.
     *
     * @return int|null
     */
    public function getXdslServiceProfile()
    {
        return $this->xdslServiceProfile;
    }

    /**
     * Set gponDownload.
     *
     * @param string|null $gponDownload
     *
     * @return MainProduct
     */
    public function setGponDownload($gponDownload = null)
    {
        $this->gponDownload = $gponDownload;

        return $this;
    }

    /**
     * Get gponDownload.
     *
     * @return string|null
     */
    public function getGponDownload()
    {
        return $this->gponDownload;
    }

    /**
     * Set gponUpload.
     *
     * @param string|null $gponUpload
     *
     * @return MainProduct
     */
    public function setGponUpload($gponUpload = null)
    {
        $this->gponUpload = $gponUpload;

        return $this;
    }

    /**
     * Get gponUpload.
     *
     * @return string|null
     */
    public function getGponUpload()
    {
        return $this->gponUpload;
    }

    /**
     * Set ethernetUpDown.
     *
     * @param string|null $ethernetUpDown
     *
     * @return MainProduct
     */
    public function setEthernetUpDown($ethernetUpDown = null)
    {
        $this->ethernetUpDown = $ethernetUpDown;

        return $this;
    }

    /**
     * Get ethernetUpDown.
     *
     * @return string|null
     */
    public function getEthernetUpDown()
    {
        return $this->ethernetUpDown;
    }

    /**
     * Set download.
     *
     * @param string|null $download
     *
     * @return MainProduct
     */
    public function setDownload($download = null)
    {
        $this->download = $download;

        return $this;
    }

    /**
     * Get download.
     *
     * @return string|null
     */
    public function getDownload()
    {
        return $this->download;
    }

    /**
     * Set upload.
     *
     * @param string|null $upload
     *
     * @return MainProduct
     */
    public function setUpload($upload = null)
    {
        $this->upload = $upload;

        return $this;
    }

    /**
     * Get upload.
     *
     * @return string|null
     */
    public function getUpload()
    {
        return $this->upload;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return MainProduct
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set priceNet.
     *
     * @param string|null $priceNet
     *
     * @return MainProduct
     */
    public function setPriceNet($priceNet = null)
    {
        $this->priceNet = $priceNet;

        return $this;
    }

    /**
     * Get priceNet.
     *
     * @return string|null
     */
    public function getPriceNet()
    {
        return $this->priceNet;
    }

    /**
     * Set priceGross.
     *
     * @param string|null $priceGross
     *
     * @return MainProduct
     */
    public function setPriceGross($priceGross = null)
    {
        $this->priceGross = $priceGross;

        return $this;
    }

    /**
     * Get priceGross.
     *
     * @return string|null
     */
    public function getPriceGross()
    {
        return $this->priceGross;
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return MainProduct
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set category.
     *
     * @param \AppBundle\Entity\Product\MainProductCategory|null $category
     *
     * @return MainProduct
     */
    public function setCategory(\AppBundle\Entity\Product\MainProductCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return \AppBundle\Entity\Product\MainProductCategory|null
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set cardType.
     *
     * @param \AppBundle\Entity\Location\CardType|null $cardType
     *
     * @return MainProduct
     */
    public function setCardType(\AppBundle\Entity\Location\CardType $cardType = null)
    {
        $this->cardType = $cardType;

        return $this;
    }

    /**
     * Get cardType.
     *
     * @return \AppBundle\Entity\Location\CardType|null
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * Set rtxProfile.
     *
     * @param int|null $rtxProfile
     *
     * @return MainProduct
     */
    public function setRtxProfile($rtxProfile = null)
    {
        $this->rtxProfile = $rtxProfile;

        return $this;
    }

    /**
     * Get rtxProfile.
     *
     * @return int|null
     */
    public function getRtxProfile()
    {
        return $this->rtxProfile;
    }

    /**
     * Set gfastUpstreamRate.
     *
     * @param string|null $gfastUpstreamRate
     *
     * @return MainProduct
     */
    public function setGfastUpstreamRate($gfastUpstreamRate = null)
    {
        $this->gfastUpstreamRate = $gfastUpstreamRate;

        return $this;
    }

    /**
     * Get gfastUpstreamRate.
     *
     * @return string|null
     */
    public function getGfastUpstreamRate()
    {
        return $this->gfastUpstreamRate;
    }

    /**
     * Set gfastDownstreamRate.
     *
     * @param string|null $gfastDownstreamRate
     *
     * @return MainProduct
     */
    public function setGfastDownstreamRate($gfastDownstreamRate = null)
    {
        $this->gfastDownstreamRate = $gfastDownstreamRate;

        return $this;
    }

    /**
     * Get gfastDownstreamRate.
     *
     * @return string|null
     */
    public function getGfastDownstreamRate()
    {
        return $this->gfastDownstreamRate;
    }
}
