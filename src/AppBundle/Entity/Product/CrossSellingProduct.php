<?php
/**
 * This file is part of the wkv project.
 */

namespace AppBundle\Entity\Product;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Product\Traits\PricingTrait;

/**
 * Entity class for CrossSelling-Products
 * 
 * @ORM\Entity
 * @ORM\Table(name="optionen")
 */
class CrossSellingProduct
{
    use PricingTrait;

    /**
     * Unique identifier
     *
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * Option identifier
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=32, nullable=false, name="option", unique=true)
     */
    protected $identifier;

    /**
     * Option name
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=255, nullable=false, name="option_bezeichnung")
     */
    protected $name;

    /**
     * Purtel product id
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer", length=8, nullable=true)
     */
    protected $purtelProductId;

    /**
     * Price (net)
     * 
     * @var float
     * 
     * @ORM\Column(type="decimal", precision=7, scale=2, nullable=true, name="Preis_Netto")
     */
    protected $priceNet;

    /**
     * Price (gross)
     * 
     * @var float
     * 
     * @ORM\Column(type="decimal", precision=7, scale=2, nullable=true, name="Preis_Brutto")
     */
    protected $priceGross;

    /**
     * contract period (in months)
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer", length=2, nullable=true)
     */
    protected $contractPeriod;

    /**
     * Is active?
     * 
     * @var boolean
     * 
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $active = true;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier.
     *
     * @param string $identifier
     *
     * @return CrossSellingProduct
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return CrossSellingProduct
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set purtelProductId.
     *
     * @param int|null $purtelProductId
     *
     * @return CrossSellingProduct
     */
    public function setPurtelProductId($purtelProductId = null)
    {
        $this->purtelProductId = $purtelProductId;

        return $this;
    }

    /**
     * Get purtelProductId.
     *
     * @return int|null
     */
    public function getPurtelProductId()
    {
        return $this->purtelProductId;
    }

    /**
     * Set priceNet.
     *
     * @param string|null $priceNet
     *
     * @return CrossSellingProduct
     */
    public function setPriceNet($priceNet = null)
    {
        $this->priceNet = $priceNet;

        return $this;
    }

    /**
     * Get priceNet.
     *
     * @return string|null
     */
    public function getPriceNet()
    {
        return $this->priceNet;
    }

    /**
     * Set priceGross.
     *
     * @param string|null $priceGross
     *
     * @return CrossSellingProduct
     */
    public function setPriceGross($priceGross = null)
    {
        $this->priceGross = $priceGross;

        return $this;
    }

    /**
     * Get priceGross.
     *
     * @return string|null
     */
    public function getPriceGross()
    {
        return $this->priceGross;
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return CrossSellingProduct
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set contractPeriod.
     *
     * @param int|null $contractPeriod
     *
     * @return CrossSellingProduct
     */
    public function setContractPeriod($contractPeriod = null)
    {
        $this->contractPeriod = $contractPeriod;

        return $this;
    }

    /**
     * Get contractPeriod.
     *
     * @return int|null
     */
    public function getContractPeriod()
    {
        return $this->contractPeriod;
    }
}
