<?php

/**
 * 
 */

namespace AppBundle\Entity\Product\Traits;

/**
 * 
 */
trait PricingTrait
{
    /**
     * Calculate gross by net and tax-rate
     * 
     * @param float $priceNet
     * @param float $taxRate
     * @param integer $scale
     * 
     * @return float
     */
    public function calculateGross($priceNet, $taxRate, $scale = 2)
    {
        $taxAmount = bcmul(bcdiv($priceNet, 100, 5), $taxRate, 5);

        return bcadd($priceNet, $taxAmount, $scale);
    }

    /**
     * Calculate net by gross and tax-rate
     * 
     * @param float $priceGross
     * @param float $taxRate
     * @param integer $scale
     * 
     * @return float
     */
    public function calculateNet($priceGross, $taxRate, $scale = 2)
    {
        $onePercent = bcdiv($priceGross, bcadd(100, $taxRate, 0), 5);

        return bcmul($onePercent, 100, $scale);
    }
}
