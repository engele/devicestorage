<?php

/**
 * 
 */

namespace AppBundle\Entity\Product;

use Doctrine\ORM\EntityRepository;

/**
 * 
 */
class MainProductRepository extends EntityRepository
{
    /**
     * Find all active products
     */
    public function findAllActive()
    {
        return $this->createQueryBuilder('p')
            ->where('p.active = :active')
            ->setParameter('active', true)
            ->getQuery()->getResult();
    }

    /**
     * Find all products allowed for certain card-type
     * 
     * @param integer $cardTypeId
     */
    public function findAllowedForCardType($cardTypeId)
    {
        return $this->createQueryBuilder('p')
            ->where('p.active = :active AND (p.cardType = :cardTypeId OR p.cardType IS NULL)')
            ->setParameter('active', true)
            ->setParameter('cardTypeId', $cardTypeId)
            ->getQuery()->getResult();
    }
}
