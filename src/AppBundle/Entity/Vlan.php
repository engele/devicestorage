<?php

/**
 * This file is part of the DICLINA project.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity class for VLANs
 * 
 * @ORM\Entity
 * @ORM\Table(name="vlans")
 */
class Vlan
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     * 
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $vlanId;

    /**
     * @var integer
     * 
     * @ORM\Column(type="string", nullable=false)
     */
    protected $tagging = 'single-tagged';

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $qosProfile;

    /**
     * @var integer
     * 
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $networkVlan;

    /**
     * @var AppBundle\Entity\VlanProfile
     * 
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\VlanProfile", inversedBy="vlans")
     * @ORM\JoinColumn(name="vlan_profile_id", referencedColumnName="id")
     */
    protected $vlanProfile;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vlanId.
     *
     * @param int $vlanId
     *
     * @return Vlan
     */
    public function setVlanId($vlanId)
    {
        $this->vlanId = $vlanId;

        return $this;
    }

    /**
     * Get vlanId.
     *
     * @return int
     */
    public function getVlanId()
    {
        return $this->vlanId;
    }

    /**
     * Set tagging.
     *
     * @param string $tagging
     *
     * @return Vlan
     */
    public function setTagging($tagging)
    {
        $this->tagging = $tagging;

        return $this;
    }

    /**
     * Get tagging.
     *
     * @return string
     */
    public function getTagging()
    {
        return $this->tagging;
    }

    /**
     * Set qosProfile.
     *
     * @param string|null $qosProfile
     *
     * @return Vlan
     */
    public function setQosProfile($qosProfile = null)
    {
        $this->qosProfile = $qosProfile;

        return $this;
    }

    /**
     * Get qosProfile.
     *
     * @return string|null
     */
    public function getQosProfile()
    {
        return $this->qosProfile;
    }

    /**
     * Set networkVlan.
     *
     * @param int|null $networkVlan
     *
     * @return Vlan
     */
    public function setNetworkVlan($networkVlan = null)
    {
        $this->networkVlan = $networkVlan;

        return $this;
    }

    /**
     * Get networkVlan.
     *
     * @return int|null
     */
    public function getNetworkVlan()
    {
        return $this->networkVlan;
    }

    /**
     * Set vlanProfile.
     *
     * @param \AppBundle\Entity\VlanProfile|null $vlanProfile
     *
     * @return Vlan
     */
    public function setVlanProfile(\AppBundle\Entity\VlanProfile $vlanProfile = null)
    {
        $this->vlanProfile = $vlanProfile;

        return $this;
    }

    /**
     * Get vlanProfile.
     *
     * @return \AppBundle\Entity\VlanProfile|null
     */
    public function getVlanProfile()
    {
        return $this->vlanProfile;
    }
}
