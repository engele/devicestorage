<?php

/**
 * This file is part of the wkv project.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/*...............
 * Entity class for Kvz
 * 
 * @ORM\Entity
 * @ORM\Table(name="kvz")
 */
class Kvz
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=64, nullable=false)
     */
    protected $identifier;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $locationDescription;

    /**
     * @var \AppBundle\Entity\Location\Location
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location\Location")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id", nullable=false)
     */
    protected $msam;
}
