<?php

/**
 * This file is part of the wkv project.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/*...............
 * Entity class for Lsa
 * 
 * @ORM\Entity
 * @ORM\Table(name="lsa")
 */
class Lsa
{
    /**
     * Unique identifier
     * 
     * @var integer
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Some name to recognize the las
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $identifier;

    /**
     * @var integer
     * 
     * @ORM\Column(type="integer", nullable=false, name="pin_amount")
     */
    protected $pinAmount;

    /**
     * @var integer
     * 
     * @ORM\Column(type="integer", length=4, nullable=true, name="dpbo")
     */
    protected $dpbo;

    /**
     * @var integer
     * 
     * @ORM\Column(type="integer", nullable=true, name="esel_wert")
     */
    protected $esel;

    /**
     * @ManyToOne(targetEntity="AppBundle\Entity\Kvz")
     * @JoinColumn(name="kvz_id", referencedColumnName="id")
     */
    protected $kvz;
}
