<?php
/**
 * 
 */

namespace AppBundle\Menu;

use Symfony\Menu\Menu;

/**
 * Currently MainMenu.php is a clone of this.
 * Every changes to this file needs to be done there too
 */
class SidebarWork extends Menu
{
    /**
     * {@inheritdoc}
     * 
     * Added "requiredRole" as a workaround for acl not working with query-string urls
     */
    protected $items = [
        [
            'name' => 'Übersicht',
            'icon' => '<i class="m-menu__link-icon flaticon-line-graph"></i>',
            'path' => 'overview',
            'requiredRole' => 'ROLE_VIEW_OVERVIEW',
        ],
        [
            'name' => 'Statistiken',
            'icon' => '<i class="m-menu__link-icon flaticon-graphic-2"></i>',
            'path' => 'statistics',
            'requiredRole' => 'ROLE_VIEW_STATISTICS',
        ],
        [
            'name' => 'CSV Export',
            'icon' => '<i class="m-menu__link-icon flaticon-download"></i>',
            'path' => 'csv-export',
            'requiredRole' => 'ROLE_USE_CSV_EXPORT',
        ],
        [
            'name' => 'Buchhaltung',
            'icon' => '<i class="m-menu__link-icon flaticon-folder"></i>',
            'path' => 'accounting',
            'requiredRole' => 'ROLE_VIEW_ACCOUNTANCY',
        ],
        [
            'name' => 'Bulkmailer',
            'icon' => '<i class="m-menu__link-icon flaticon-mail"></i>',
            'path' => 'bulkmailer',
            'requiredRole' => 'ROLE_ADMIN',
        ],
        [
            'name' => 'Kundensuche',
            'icon' => '<i class="m-menu__link-icon flaticon-search"></i>',
            'path' => 'customer-search',
            'requiredRole' => 'ROLE_VIEW_CUSTOMER_SEARCH',
        ],
        [
            'name' => 'Neuer Interessent',
            'icon' => '<i class="m-menu__link-icon flaticon-user-add"></i>',
            'path' => 'potential-customer',
            'requiredRole' => 'ROLE_CREATE_POTENTIAL_CUSTOMER',
        ],
        [
            'name' => 'Neuer Vertrag',
            'icon' => '<i class="m-menu__link-icon flaticon-add"></i>',
            'path' => 'new-contract',
        ],
        [
            'name' => 'Netzübersicht',
            'icon' => '<i class="m-menu__link-icon flaticon-network"></i>',
            'path' => 'network',
            'requiredRole' => 'ROLE_VIEW_NETWORK',
        ],
        [
            'name' => 'IP-Block Übersicht',
            'icon' => '<i class="m-menu__link-icon flaticon-technology-2"></i>',
            'path' => 'ip-range',
            'requiredRole' => 'ROLE_VIEW_IP_RANGE',
        ],
        [
            'name' => 'Technik Hilfe',
            'icon' => '<i class="m-menu__link-icon flaticon-questions-circular-button"></i>',
            'path' => 'technics',
            'requiredRole' => 'ROLE_VIEW_TECH_HELP',
        ],
        [
            'name' => 'Administration',
            'icon' => '<i class="m-menu__link-icon flaticon-settings"></i>',
            'path' => 'administration',
            'requiredRole' => 'ROLE_VIEW_ADMINISTRATION',
        ],

    ];
}

?>