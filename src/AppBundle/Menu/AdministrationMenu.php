<?php
/**
 * 
 */

namespace AppBundle\Menu;

use Symfony\Menu\Menu;

/**
 * 
 */
class AdministrationMenu extends Menu
{
    /**
     * {@inheritdoc}
     * 
     * Added "requiredRole" as a workaround for acl not working with query-string urls
     */
    protected $items = [
        [
            'name' => 'Übersicht',
            'path' => 'administration_overview',
            'requiredRole' => 'ROLE_VIEW_ADMINISTRATION',
        ],
        [
            'name' => 'Produkte verwalten',
            'path' => 'admin-products-overview',
            'requiredRole' => 'ROLE_EDIT_PRODUCTS',
        ],
        [
            'name' => 'Produktoptionen verwalten',
            'path' => 'admin-product-options-overview',
            'requiredRole' => 'ROLE_EDIT_PRODUCTS',
        ],
        [
            'name' => 'Verlauf-Events verwalten',
            'path' => 'admin-history-events',
            'requiredRole' => 'ROLE_EDIT_HISTORY_EVENTS',
        ],
        [
            'name' => 'Spectrum-Profile verwalten',
            'path' => 'admin-spectrum-profiles',
            'requiredRole' => 'ROLE_VIEW_SPECTRUM_PROFILE',
        ],
        [
            'name' => 'Mail-Templates',
            'path' => 'administration_mail_templates',
            'requiredRole' => 'ROLE_CREATE_MAIL_TEMPLATES',
        ],
        [
            'name' => 'VLAN-Profile',
            'path' => 'admin-vlan-profiles',
            'requiredRole' => 'ROLE_VIEW_VLAN_PROFILES',
        ],
        [
            'name' => 'Benutzer verwalten',
            'path' => 'admin-user-accounts',
            'requiredRole' => 'ROLE_ADMIN',
        ],
    ];
}

?>