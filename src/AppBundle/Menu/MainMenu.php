<?php
/**
 * 
 */

namespace AppBundle\Menu;

use Symfony\Menu\Menu;

/**
 * Currently SidebarWork.php is a clone of this.
 * Every changes to this file needs to be done there too
 */
class MainMenu extends Menu
{
    /**
     * {@inheritdoc}
     * 
     * Added "requiredRole" as a workaround for acl not working with query-string urls
     */
    protected $items = [
        [
            'name' => 'Übersicht',
            'icon' => '<span class="ui-icon ui-icon-info"></span>',
            'path' => 'overview',
            'requiredRole' => 'ROLE_VIEW_OVERVIEW',
        ],
        [
            'name' => 'Statistiken',
            'icon' => '<span class="ui-icon ui-icon-info"></span>',
            'path' => 'statistics',
            'requiredRole' => 'ROLE_VIEW_STATISTICS',
        ],
        [
            'name' => 'CSV Export',
            'icon' => '<span class="ui-icon ui-icon-info"></span>',
            'path' => 'csv-export',
            'requiredRole' => 'ROLE_USE_CSV_EXPORT',
        ],
        [
            'name' => 'Buchhaltung',
            'icon' => '<span class="ui-icon ui-icon-info"></span>',
            'path' => 'accounting',
            'requiredRole' => 'ROLE_VIEW_ACCOUNTANCY',
        ],
        [
            'name' => 'Bulkmailer',
            'icon' => '<span class="ui-icon ui-icon-info"></span>',
            'path' => 'bulkmailer',
            'requiredRole' => 'ROLE_ADMIN',
        ],
        [
            'name' => 'Kundensuche',
            'icon' => '<span class="ui-icon ui-icon-pencil"></span>',
            'path' => 'customer-search',
            'requiredRole' => 'ROLE_VIEW_CUSTOMER_SEARCH',
        ],
        [
            'name' => 'Neuer Interessent',
            'icon' => '<span class="ui-icon ui-icon-pencil"></span>',
            'path' => 'potential-customer',
            'requiredRole' => 'ROLE_CREATE_POTENTIAL_CUSTOMER',
        ],
        [
            'name' => 'Neuer Vertrag',
            'icon' => '<span class="ui-icon ui-icon-pencil"></span>',
            'path' => 'new-contract',
        ],
        [
            'name' => 'Netzübersicht',
            'icon' => '<span class="ui-icon ui-icon-wrench"></span>',
            'path' => 'network',
            'requiredRole' => 'ROLE_VIEW_NETWORK',
        ],
        [
            'name' => 'IP-Block Übersicht',
            'icon' => '<span class="ui-icon ui-icon-wrench"></span>',
            'path' => 'ip-range',
            'requiredRole' => 'ROLE_VIEW_IP_RANGE',
        ],
        [
            'name' => 'Versorgungsadressen',
            'icon' => '<span class="ui-icon ui-icon-wrench"></span>',
            'path' => 'servingaddress_index',
            'requiredRole' => 'ROLE_VIEW_SERVING_ADDRESSES',
        ],
        [
            'name' => 'Technik Hilfe',
            'icon' => '<span class="ui-icon ui-icon-wrench"></span>',
            'path' => 'technics',
            'requiredRole' => 'ROLE_VIEW_TECH_HELP',
        ],
        [
            'name' => 'Administration',
            'icon' => '<span class="ui-icon ui-icon-wrench"></span>',
            'path' => 'administration',
            'requiredRole' => 'ROLE_VIEW_ADMINISTRATION',
        ],
        [
            'name' => 'Mein Konto',
            'icon' => '<span class="ui-icon ui-icon-pencil"></span>',
            'path' => 'my-account',
            'requiredRole' => 'ROLE_USER',
        ],
    ];
}

?>