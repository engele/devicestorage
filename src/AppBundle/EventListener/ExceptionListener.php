<?php

/**
 * 
 */

namespace AppBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Templating\EngineInterface;

/**
 * 
 */
class ExceptionListener
{
    /**
     * Template engine
     * 
     * @var Symfony\Bundle\FrameworkBundle\Templating\DelegatingEngine
     */
    private $templating;

    /**
     * Mailer
     * 
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * Send email to this address
     * 
     * @var string
     */
    private $mailErrorsTo;

    /**
     * Send emails from this address
     * 
     * @var string
     */
    private $mailErrorsFrom;

    /**
     * Constructor
     * 
     * Set default values
     * 
     * @param EngineInterface $templating
     * @param \Swift_Mailer $mailer
     * @param string $mailErrorsTo
     * @param string $mailErrorsFrom
     */
    public function __construct(EngineInterface $templating, \Swift_Mailer $mailer, $mailErrorsTo, $mailErrorsFrom)
    {
        $this->templating = $templating;
        $this->mailer = $mailer;
        $this->mailErrorsTo = $mailErrorsTo;
        $this->mailErrorsFrom = $mailErrorsFrom;
    }

    /**
     * Subscribed "On Kernel Exception" Event
     * 
     * Send Exception via email
     * 
     * @param \Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event
     * 
     * @return void
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        $request = $event->getRequest();

        /*if ($exception instanceof NotFoundHttpException) {
            $path['_controller'] = 'AppBundle\Controller\DefaultController::indexAction';

            $subRequest = $event->getRequest()->duplicate(null, null, $path);
            $response = $event->getKernel()->handle($subRequest, HttpKernelInterface::SUB_REQUEST);

            $event->setResponse($response);
        }*/

        $message = $this->templating->render('maintenance/mail/unexpected-behaviour-exception.html.php', [
            'exception' => $exception,
            'request' => $request,
        ]);

        $message = \Swift_Message::newInstance()
            ->setSubject('Error in application')
            ->setFrom($this->mailErrorsFrom)
            ->setTo($this->mailErrorsTo)
            ->setBody(
                $message,
                'text/html'
            )
        ;

        $sendingResponse = $this->mailer->send($message);

        /*if ($exception instanceof UnexpectedBehaviourException) {
            $content = 'Sehr geehrter Anwender,<br />der von Ihnen verwendete Browser scheint eine notwendige Funktionalität nicht zu untersützen.<br />';

            if ($sendingResponse) {
                $content .= '<br />Ein Techniker wurde bereits informiert und wird versuchen Sie baldmöglichst zu erreichen.<br /><br />';
            } else {
                $content .= 'Zur Lösung dieses Problems, kontaktieren Sie bitte einen Techniker.';
            }

            $event->setResponse(new Response(
                $content,
                200
            ));
        }*/
    }
}
