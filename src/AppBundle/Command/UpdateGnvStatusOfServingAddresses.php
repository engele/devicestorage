<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Goutte\Client;
use AppBundle\Entity\ServingAddress;

/**
 * 
 */
class UpdateGnvStatusOfServingAddresses extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('UpdateGnvStatusOfServingAddresses')
            ->setDescription('')
        ;
    }

    /**
     * @param array $findBy
     * 
     * @return array = ['log' => (string), 'addresses' => []]
     */
    protected function findAddress($addressData) : array
    {
        $doctrine = $this->getContainer()->get('doctrine');

        $return = ['log' => '', 'addresses' => [],];

        $address = $doctrine->getRepository(ServingAddress::class)->findOneBy([
            'zipCode' => $addressData['zipCode'],
            'street' => $addressData['street'],
            'houseNumber' => $addressData['houseNumber'],
        ]);

        if ($address instanceof ServingAddress) {
            // adresse gefunden
            $return['addresses'][] = $address;

            return $return;
        }

        // adresse nicht gefunden

        //echo "new address : ".sprintf("%s ; %s ; %s ; %s", $lfdNr, $zipCode, $street, $houseNumber)."\n";

        if (false !== strpos($addressData['houseNumber'], '-')) {
            $houseNumbers = explode('-', $addressData['houseNumber']);

            if (count($houseNumbers) !== 2) {
                $return['log'] = "1 komische hausnummer : "
                .sprintf("%s ; %s ; %s ; %s", $addressData['lfdNr'], $addressData['zipCode'], $addressData['street'], $addressData['houseNumber'])."\n";
                
                return $return;
            }

            if (!empty($houseNumberAddition)) {
                // use last element with addition
                $houseNumbers[count($houseNumbers) - 1] = $houseNumbers[count($houseNumbers) - 1].$houseNumberAddition;
            }

            $found = 0;
            $foundArray = [];

            foreach ($houseNumbers as $houseNumberX) {
                $houseNumberX = trim($houseNumberX);

                $address = $doctrine->getRepository(ServingAddress::class)->findOneBy([
                    'zipCode' => $addressData['zipCode'],
                    'street' => $addressData['street'],
                    'houseNumber' => $houseNumberX,
                ]);

                if (!($address instanceof ServingAddress)) {
                    //echo "\t1 new address : "
                    //.sprintf("%s ; %s ; %s ; %s", $addressData['lfdNr'], $addressData['zipCode'], $addressData['street'], $houseNumberX)."\n";
                    
                    continue;   
                }

                $foundArray[] = $address;
                $found++;
            }

            if ($found !== count($houseNumbers)) {
                // was machen, wenn nicht alle in der db existieren?
                
                if (0 === $found) {
                    // keine gefunden
                } else {
                    // mindestens eine mögliche adresse gefunden
//                    echo "\t1 nicht alle gefunden (".$found."/".count($houseNumbers).") \n";
                }
            } else {
                // alle beide hausnummern gefunden, updaten und weiter
                $return['addresses'] = $foundArray;

                return $return;
            }
        }


        if (false !== strpos($addressData['houseNumberSimple'], '-')) {
            $houseNumbers = explode('-', $addressData['houseNumberSimple']);

            if (count($houseNumbers) !== 2) {
                $return['log'] = "2 komische hausnummer : "
                .sprintf("%s ; %s ; %s ; %s", $addressData['lfdNr'], $addressData['zipCode'], $addressData['street'], $addressData['houseNumberSimple'])."\n";
                
                return $return;
            }

            if (!empty($houseNumberAddition)) {
                // use last element with addition
                $houseNumbers[count($houseNumbers) - 1] = $houseNumbers[count($houseNumbers) - 1].$houseNumberAddition;
            }

            $found = 0;
            $foundArray = [];

            foreach ($houseNumbers as $houseNumberX) {
                $houseNumberX = trim($houseNumberX);

                $address = $doctrine->getRepository(ServingAddress::class)->findOneBy([
                    'zipCode' => $addressData['zipCode'],
                    'street' => $addressData['street'],
                    'houseNumber' => $houseNumberX,
                ]);

                if (!($address instanceof ServingAddress)) {
                    //echo "\t2 new address : "
                    //.sprintf("%s ; %s ; %s ; %s", $addressData['lfdNr'], $addressData['zipCode'], $addressData['street'], $houseNumberX)."\n";
                    
                    continue;   
                }

                $foundArray[] = $address;
                $found++;
            }

            if ($found !== count($houseNumbers)) {
                // was machen, wenn nicht alle in der db existieren?

                if (0 === $found) {
                    // keine gefunden
                } else {
                    // mindestens eine mögliche adresse gefunden
//                    echo "\t2 nicht alle gefunden (".$found."/".count($houseNumbers).") \n";
                }
            } else {
                // alle beide hausnummern gefunden, updaten und weiter
                $return['addresses'] = $foundArray;

                return $return;
            }
        }

        if (false !== strpos($addressData['houseNumber'], '+')) {
            $houseNumbers = explode('+', $addressData['houseNumber']);

            if (count($houseNumbers) !== 2) {
                $return['log'] = "3 komische hausnummer : "
                .sprintf("%s ; %s ; %s ; %s", $addressData['lfdNr'], $addressData['zipCode'], $addressData['street'], $addressData['houseNumber'])."\n";
                
                return $return;
            }

            if (!empty($houseNumberAddition)) {
                // use last element with addition
                $houseNumbers[count($houseNumbers) - 1] = $houseNumbers[count($houseNumbers) - 1].$houseNumberAddition;
            }

            $found = 0;
            $foundArray = [];

            foreach ($houseNumbers as $houseNumberX) {
                $houseNumberX = trim($houseNumberX);

                $address = $doctrine->getRepository(ServingAddress::class)->findOneBy([
                    'zipCode' => $addressData['zipCode'],
                    'street' => $addressData['street'],
                    'houseNumber' => $houseNumberX,
                ]);

                if (!($address instanceof ServingAddress)) {
                    //echo "\t3 new address : "
                    //.sprintf("%s ; %s ; %s ; %s", $addressData['lfdNr'], $addressData['zipCode'], $addressData['street'], $houseNumberX)."\n";
                    
                    continue;   
                }

                $foundArray[] = $address;
                $found++;
            }

            if ($found !== count($houseNumbers)) {
                // was machen, wenn nicht alle in der db existieren?
                
                if (0 === $found) {
                    // keine gefunden
                } else {
                    // mindestens eine mögliche adresse gefunden
//                    echo "\t3 nicht alle gefunden (".$found."/".count($houseNumbers).") \n";
                }
            } else {
                // alle beide hausnummern gefunden, updaten und weiter
                $return['addresses'] = $foundArray;

                return $return;
            }
        }

        if (false !== strpos($addressData['houseNumberSimple'], '+')) {
            $houseNumbers = explode('+', $addressData['houseNumberSimple']);

            if (count($houseNumbers) !== 2) {
                $return['log'] = "4 komische hausnummer : "
                .sprintf("%s ; %s ; %s ; %s", $addressData['lfdNr'], $addressData['zipCode'], $addressData['street'], $addressData['houseNumberSimple'])."\n";
                
                return $return;
            }

            if (!empty($houseNumberAddition)) {
                // use last element with addition
                $houseNumbers[count($houseNumbers) - 1] = $houseNumbers[count($houseNumbers) - 1].$houseNumberAddition;
            }

            $found = 0;
            $foundArray = [];

            foreach ($houseNumbers as $houseNumberX) {
                $houseNumberX = trim($houseNumberX);

                $address = $doctrine->getRepository(ServingAddress::class)->findOneBy([
                    'zipCode' => $addressData['zipCode'],
                    'street' => $addressData['street'],
                    'houseNumber' => $houseNumberX,
                ]);

                if (!($address instanceof ServingAddress)) {
                    //echo "\t4 new address : "
                    //.sprintf("%s ; %s ; %s ; %s", $addressData['lfdNr'], $addressData['zipCode'], $addressData['street'], $houseNumberX)."\n";
                    
                    continue;   
                }

                $foundArray[] = $address;
                $found++;
            }

            if ($found !== count($houseNumbers)) {
                // was machen, wenn nicht alle in der db existieren?
                
                if (0 === $found) {
                    // keine gefunden
                } else {
                    // mindestens eine mögliche adresse gefunden
//                    echo "\t4 nicht alle gefunden (".$found."/".count($houseNumbers).") \n";
                }
            } else {
                // alle beide hausnummern gefunden, updaten und weiter
                $return['addresses'] = $foundArray;

                return $return;
            }
        }






        $return['log'] = "new address : ".sprintf("%s ; %s ; %s ; %s", 
            $addressData['lfdNr'], $addressData['zipCode'], $addressData['street'], $addressData['houseNumber']
        )."\n";

        return $return;

        /*$address = $doctrine->getRepository(ServingAddress::class)->findOneBy([
            'zipCode' => $zipCode,
            'street' => $street,
            'houseNumber' => $houseNumber.,
        ]);

        if (!($address instanceof ServingAddress)) {
            echo "new address : ".sprintf("%s ; %s ; %s ; %s", $lfdNr, $zipCode, $street, $houseNumber)."\n";
        }*/
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/tools/import/CsvReader.php';

        $content = file_get_contents('/tmp/gnv.csv');

        $position = strpos($content, ';"KVZ Nr.";"Ausbau-gebiet";');

        if (!$position) {
            throw new \Exception('couldn\'t find ";"KVZ Nr.";"Ausbau-gebiet";" in csv-file');
        }

        $content = '"1. ldf. nr"'.substr($content, $position);
        $content = preg_replace(
            '/;"Technischer Hinweis";;"Haus-nummer";"Zusatz";"Postleitzahl";"Ort";/',
            ';"Technischer Hinweis";"strasse";"anschluss-Haus-nummer";"anschluss-Zusatz";"anschluss-Postleitzahl";"anschluss-Ort";',
            $content,
            1
        );
        $content = preg_replace('/;"Bemerkung";;"Schlüssel";/', ';"Bemerkung";"unwichtig-1";"Schlüssel";', $content, 1);
        $content = preg_replace('/;"Versorgungs-einheiten";"Nachname";"Vorname";/', ';"Versorgungs-einheiten";"eigent.-Nachname";"eigent.-Vorname";', $content, 1);

        $tmpFilename = '/tmp/gnv-76gtv5e.csv';

        file_put_contents($tmpFilename, $content);

        $csv = new \CsvReader($tmpFilename);

        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $emptyToNull = function ($value) {
            if (empty($value)) {
                return null;
            }

            return $value;
        };

        $countColumns = count($csv->headlineReversed);

        foreach ($csv->data as $key => $line) {
            if (count($line) < $countColumns) {
                $line = array_pad($line, $countColumns, null);
            }

            $lfdNr = trim($line[$csv->headlineReversed['1. ldf. nr']]);
            $street = trim($line[$csv->headlineReversed['strasse']]);
            $houseNumberSimple = trim($line[$csv->headlineReversed['anschluss-Haus-nummer']]);
            $houseNumberAddition = trim($line[$csv->headlineReversed['anschluss-Zusatz']]);
            $houseNumber = $emptyToNull(trim($line[$csv->headlineReversed['anschluss-Haus-nummer']]).$houseNumberAddition);
            $zipCode = trim($line[$csv->headlineReversed['anschluss-Postleitzahl']]);
            $technicalNote = $emptyToNull(trim($line[$csv->headlineReversed['Technischer Hinweis']]));

            if (empty($street) && empty($houseNumber)) {
                // incomplete row
                continue;
            }

            if (false !== stripos($technicalNote, 'Doppelter Eintrag')) {
                //echo "Doppelter Eintrag\n";
                continue;
            }

            $return = $this->findAddress([
                'lfdNr' => $lfdNr,
                'street' => $street,
                'houseNumberSimple' => $houseNumberSimple,
                'houseNumberAddition' => $houseNumberAddition,
                'houseNumber' => $houseNumber,
                'zipCode' => $zipCode,
            ]);

            if (!empty($return['log'])) {
                //echo $return['log'];

                $a = fopen('php://stdout', 'r');
                die(var_dump($line));
                fputcsv($a, $line, ";", '"');
                fclose($a);
            }
// technischer hinweis= Doppelter Eintrag
            if (!empty($return['addresses'])) {
                foreach ($return['addresses'] as $address) {
                    /*
                        anzahl gewerbeeinh
                        anzahl wohneinh
                        ausbaujahr
                        ausbaustatus ha                 technischer hinweis = Hausanschluss ist fertig
                        gnv_final / hat gnv
                        flur / kataster
                        eigentümer
                    */
                }
            }            
        }        

        echo "\n";











        return;




        

        $countColumns = count($csv->headlineReversed);

        foreach ($csv->data as $key => $line) {
            if (count($line) < $countColumns) {
                $line = array_pad($line, $countColumns, null);
            }

            $zipCode = $emptyToNull(trim($line[$csv->headlineReversed['PLZ']]));

            if (1 !== preg_match('/^\d{5}$/', $zipCode)) {
                var_dump($line, $zipCode);die();
            }

            $city = $emptyToNull(trim($line[$csv->headlineReversed['Ort']]));

            if (empty($city)) {
                var_dump($line, $line[$csv->headlineReversed['Ort']], $csv->headlineReversed['Ort']);die();
            }

            $district = $emptyToNull(trim($line[$csv->headlineReversed['Ortsteil']]));
            $street = $emptyToNull(trim($line[$csv->headlineReversed['Strasse']]));
            $houseNumber = $emptyToNull(trim($line[$csv->headlineReversed['Hausnummer']]).trim($line[$csv->headlineReversed['Hausnummerzusatz']]));
            
            $amountOfBusinessUnits = trim($line[$csv->headlineReversed['Anzahl Gewerbeeinheiten']]);
            $amountOfDwellingUnits = trim($line[$csv->headlineReversed['Anzahl Wohneinheiten']]);
            
            $rollOutIsPlannedIn = trim($line[$csv->headlineReversed['Ausbaujahr']]);
            $isRollOutFinished = false;

            if (false !== strpos($rollOutIsPlannedIn, 'bereits ausgebaut')) {
                $isRollOutFinished = true;
                $rollOutIsPlannedIn = null;
            } elseif (false !== strpos($rollOutIsPlannedIn, 'k.A.')) {
                $rollOutIsPlannedIn = null;
            } elseif (empty($rollOutIsPlannedIn)) {
                $rollOutIsPlannedIn = null;
            } else {
                try {
                    $x = \DateTime::createFromFormat('Y', $rollOutIsPlannedIn);

                    if (!($x instanceof \DateTime)) {
                        var_dump($x, $rollOutIsPlannedIn);die();
                    }

                    $x->setDate($x->format('Y'), 1, 1);

                    if ($x->format('Y') !== $rollOutIsPlannedIn) {
                        throw new \Exception('invalid date');
                    }

                    $rollOutIsPlannedIn = $x;
                } catch (\Exception $e) {
                    var_dump($e->getMessage(), $e->getLine(), $line, $rollOutIsPlannedIn);die();
                }
            }
            
            $isBuildingConnected = $emptyToNull(trim($line[$csv->headlineReversed['Ausbaustatus HA']]));

            if (false !== strpos($isBuildingConnected, 'JA')) {
                $isBuildingConnected = true;
            } else {
                $isBuildingConnected = false;
            }

            //$isNe4Finished = $emptyToNull(trim($line[$csv->headlineReversed['Ausbaustatus NE 4']]));
            $isNe4Finished = false;

            $gnvNote = null;
            $hasGnv = $emptyToNull(trim($line[$csv->headlineReversed['GNV_final']]));

            if (false !== strpos($hasGnv, 'NEIN')) {
                $hasGnv = false;
            } elseif (false !== stripos($hasGnv, 'prüfen')) {
                $hasGnv = false;
            } elseif (1 === preg_match('/^\d{5}$/', $hasGnv)) {
                $gnvNote = $hasGnv;
                $hasGnv = true;
            } elseif (empty($hasGnv)) {
                $hasGnv = false;
            } else {
                var_dump($hasGnv);die();
            }

            //$hasEverHadAnInternetSupplyContract = $emptyToNull(trim($line[$csv->headlineReversed['Liefervertrag Internet/Telefonie']]));
            $hasEverHadAnInternetSupplyContract = false;
            //$hasEverHadABasicTvSupplyContract = $emptyToNull(trim($line[$csv->headlineReversed['Liefervertrag Basis-TV']]));
            $hasEverHadABasicTvSupplyContract = false;
            
            $connectedToKvz = $emptyToNull(trim($line[$csv->headlineReversed['KVZ Nr.']]));

            if (false !== stripos($connectedToKvz, 'nicht im ausbaugebiet')) {
                $connectedToKvz = null;
            }

            $technicalNote = $emptyToNull(trim($line[$csv->headlineReversed['Technischer Hinweis']]));
            $fieldAndCadastre = $emptyToNull(trim($line[$csv->headlineReversed['Flur/Kataster']]));

            $note = sprintf("Eigentümer\n- Nachname: %s\n- Vorname: %s\n- Straße: %s\n- Hausnummer: %s\n- PLZ: %s\n- Ort: %s\n"
                ."\nTelefon: %s\n\nAnsprechpartner: %s\n\nBemerkung:\n%s",
                trim($line[$csv->headlineReversed['Nachname Eigentümer']]),
                trim($line[$csv->headlineReversed['Vorname Eigentümer']]),
                trim($line[$csv->headlineReversed['Straße Eigentümer']]),
                trim($line[$csv->headlineReversed['Hausnummer ET']]).trim($line[$csv->headlineReversed['Zusatz ET']]),
                trim($line[$csv->headlineReversed['Postleitzahl ET']]),
                trim($line[$csv->headlineReversed['Ort ET']]),
                trim($line[$csv->headlineReversed['Telefon ET']]),
                trim($line[$csv->headlineReversed['Ansprech-partner']]),
                trim($line[$csv->headlineReversed['Bemerkung']]),
                trim($line[$csv->headlineReversed['Technischer Hinweis']])
            );

            $servingAddress = new ServingAddress();
            $servingAddress
                ->setZipCode($zipCode)
                ->setCity($city)
                ->setDistrict($district)
                ->setStreet($street)
                ->setHouseNumber($houseNumber)
                ->setAmountOfBusinessUnits($amountOfBusinessUnits)
                ->setAmountOfDwellingUnits($amountOfDwellingUnits)
                ->setRollOutIsPlannedIn($rollOutIsPlannedIn)
                ->setIsRollOutFinished($isRollOutFinished)
                ->setIsBuildingConnected($isBuildingConnected)
                ->setIsNe4Finished($isNe4Finished)
                ->setHasGnv($hasGnv)
                ->setGnvNote($gnvNote)
                ->setHasEverHadAnInternetSupplyContract($hasEverHadAnInternetSupplyContract)
                ->setHasEverHadABasicTvSupplyContract($hasEverHadABasicTvSupplyContract)
                ->setConnectedToKvz($connectedToKvz)
                ->setTechnicalNote($technicalNote)
                ->setFieldAndCadastre($fieldAndCadastre)
                ->setNote($note);

            $foundServingAddress = $doctrine->getRepository(ServingAddress::class)->findOneByUniqueConstraint($servingAddress);

            if (null !== $foundServingAddress) {
                echo "dupplicate addresse ".sprintf("%s %s %s", $street, $houseNumber, $zipCode)."\n";

                continue;
            }

            $em->persist($servingAddress);
        }

        $em->flush();

        echo "all done\n";
    }
}

?>