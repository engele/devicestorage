<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Goutte\Client;

/**
 * 
 */
class KmuMigration extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('KmuMigration')
            ->setDescription('')
        ;
    }

    /**
     * 
     */
    protected function getStammdatenForm($purtelAccount)
    {
        $client = new Client();

        $crawler = $client->request('GET', 'https://www2.purtel.com/res/'.$this->purtelReseller.'/index.php?username='
            .$this->purtelSuperUser.'&passwort='
            .$this->purtelSuperUserPassword.'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1&admin_ns_suchbegriff='
            .$purtelAccount.'&tab_id=9&marke=Stammdaten');

        $form = $crawler->selectButton('Ändern')->form();

        return $form;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/tools/import/CsvReader.php';
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';

        $this->purtelSuperUser = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
        $this->purtelSuperUserPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
        $this->purtelReseller = \Wisotel\Configuration\Configuration::get('purtelReseller');

        $db = $this->getContainer()->get('doctrine')->getManager()->getConnection();

        $kmuCsv = new \CsvReader('/tmp/Rufnummern_20180821-2.csv');

        $sortedByDimariId = [];
$xxxx = [
'498418813402',
'498419935412',
'4984114903757',
'49841886681',
'49841886682',
'49841886683',
'49841886685',
'49841886686',
'49841886687',
'498456759590',
'498456759592',
'498456759501',
'498456759502',
'498456759504',
'498456759505',
'4984562786057',
'4984190006834',
'4984586725493',
'498418855571',
'498418855572',
'498418855574',
'498418855575',
'498418855576',
'498418813131',
'498418813132',
'498418813133',
'498418813135',
'498418813136',
'498417932',
];
        foreach ($kmuCsv->data as $key => $line) {
            $dimariId = trim($line[$kmuCsv->headlineReversed['Dimari Kd.Nr.']]);
            $easybellId = trim($line[$kmuCsv->headlineReversed['kundennummer easybell']]);

            if (empty($dimariId)) {
                echo "empty dimari id - ".$easybellId."\n";

                continue;
            }
if (in_array(trim($line[$kmuCsv->headlineReversed['a_rn']]), $xxxx)) {
    echo $dimariId.' - '.$easybellId."\n";
}
            $sortedByDimariId[$dimariId] = $kmuCsv->findLines([
                'Dimari Kd.Nr.' => $dimariId,
            ]);
        }
die();
        foreach ($sortedByDimariId as $dimariId => $lines) {
            $customerQ = $db->query("SELECT id, clientid FROM customers WHERE customer_id = '".$dimariId."'");

            if ($customerQ->rowCount() < 1) {
                echo "no customer found for dimari-id - ".$dimariId."\n";

                continue;
            } //elseif ($customerQ->rowCount() > 1) {
                $customers = [];

                while ($customer = $customerQ->fetch(\PDO::FETCH_ASSOC)) {
                    $customers[] = $customer;
                }

                //echo "customer ".$customerQ->rowCount()." mal zu dimari-id gefunden - ".$dimariId."\n";
            //}


            foreach ($lines as $line) { // loop thru all numbers for this dimariId
                $number = trim($line[$kmuCsv->headlineReversed['a_rn']]);

                // to be sure
                // check if number is only once in csv
                $x = $kmuCsv->findLines([
                    'a_rn' => $number,
                ]);

                if (count($x) !== 1) {
                    echo "found number (".$number.") ".count($x)." mal in csv\n";
                }

                unset($x);

                // see if customer has an purtel account with this number
                $purtelAccountForNumber = null;
                $numberInIkv = preg_replace('/^49/', '0', $number);


                foreach ($customers as $customer) {
                    $numbersExistInIkvQ = $db->query("SELECT * 
                        FROM purtel_account 
                        WHERE cust_id = ".$customer['id']." AND nummer = '".$numberInIkv."'");

                    if ($numbersExistInIkvQ->rowCount() < 1) {
                        continue;
                    } elseif ($numbersExistInIkvQ->rowCount() > 1) {
                        echo "nummer (".$numberInIkv.") komischerweise mehrfach in purtel_account für customerid (".$customer['id'].")\n";

                        continue;
                    }

                    if (null !== $purtelAccountForNumber) {
                        echo "mehrere kunden haben diese nummer (".$number.")\n";
                        die();
                    }

                    $purtelAccountForNumber = $numbersExistInIkvQ->fetch(\PDO::FETCH_ASSOC);
                }

                if (null === $purtelAccountForNumber) {
                    echo "nummer (".$number.") nicht in purtel_account gefunden ...\n";

                    if (count($customers) > 1) {
                        echo "hmm.. dimariId (".$dimariId.") hat mehrere kunden in der ikv aber die nummer wurde nicht gefunden. Welcher kunde soll denn den account nun bekommen??\n";
                        die();
                    }

                    $customer = end($customers);
                    echo "anglegen - ".$numberInIkv." customerId: ".$customer['id']."\n";
                    die("sollte nicht mehr nötig sein");
                    // purtel account muss angelegt werden
                        // in ikv
                        // bei purtel
                } else {
                    echo "purtel_account für nummer (".$number.") gefunden!\n";

                    // purtel account muss überprüft werden
                    // hat purtel_login und pw?
                    if (empty($purtelAccountForNumber['purtel_login']) || empty($purtelAccountForNumber['purtel_password'])) {
                        echo "purtel_login oder password leer - nummer (".$number.")\n";
                        
                        continue;
                    }

                    // gibt es den account auch sicher noch bei purtel?
                    
                    /*
                    $stammdaten = $this->getStammdatenForm($purtelAccountForNumber['purtel_login']);
                    // kein erro, alles ok!
                    unset($stammdaten);
                    */

                    // alles ok!

                    $sipPassword = trim($line[$kmuCsv->headlineReversed['SIP Zugangspasswort']]);
                    
                    if (empty($sipPassword)) {
                        echo "sip-pw leer - ".$number."\n";

                        continue;
                    }

                    $sipUsername = trim($line[$kmuCsv->headlineReversed['SIP Zugangskennung']]);
                    
                    if (empty($sipUsername)) {
                        echo "sip-user leer - ".$number."\n";

                        continue;
                    }


                    $customerQ = $db->query("SELECT id, clientid FROM customers WHERE id = ".$purtelAccountForNumber['cust_id']);

                    if ($customerQ->rowCount() !== 1) {
                        die("unpassende anzahl kunden gefunden - line: ".__LINE__);
                    }

                    $customer = $customerQ->fetch(\PDO::FETCH_ASSOC);


                    $updatePostData = [
                        'anschluss' => $purtelAccountForNumber['purtel_login'],
                        'kundennummer_extern' => $customer['clientid'],
                        'sipusername' => $sipUsername,
                        'passwort' => $sipPassword,
                    ];

                    if (!$this->updatePurtelAccount($updatePostData)) {
                        echo "failed to update purtel account\n";
                        var_dump( $updatePostData );

                        continue;
                    }

                    $db->query("UPDATE purtel_account SET `purtel_password` = '".$sipPassword."' WHERE id = ".$purtelAccountForNumber['id']);

                    if (!empty($db->error)) {
                        die($db->error);
                    }

//die("stopped");
                    //  write sip-username to some comment-field
                        // eigentlich unnötig, da gleich wir rufnummer (49 statt 0 am anfang)

                }
            }
        }

        echo "completely done\n";
    }

    /**
     * @param array $updatePostData
     * 
     * @return boolean
     */
    private function updatePurtelAccount($updatePostData)
    {
        /*$updatePostData = array_merge(
            \Wisotel\Configuration\Configuration::get('purtelAccountValues'),
            $updatePostData
        );*/

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'changecontract'
        );

        $url .= '&'.utf8_decode(http_build_query($updatePostData));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            //CURLOPT_URL             => $PurtelUrl1,
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));

        $result = curl_exec($curl);

        curl_close($curl);

        $result = trim($result);
        $result = explode(';', $result);

        if ('+OK' === $result[0]) {
            return true;
        }

        var_dump($result);

        return false;
    }
}

?>