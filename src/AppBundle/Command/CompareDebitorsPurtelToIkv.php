<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Goutte\Client;

/**
 * 
 */
class CompareDebitorsPurtelToIkv extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('CompareDebitorsPurtelToIkv')
            ->setDescription('')
        ;
    }

    /**
     * 
     * 
     * @return array|null
     */
    protected function getAccounts($clientId = null)
    {
        $parameter = [
            'erweitert' => 2,
        ];

        if (null !== $clientId) {
            $parameter['kundennummer_extern'] = $clientId;
        }

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'getaccounts'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * 
     * 
     * @return array|null
     */
    protected function getSepas($clientId)
    {
        $parameter = [
            'art' => 1,
        ];

        if (null !== $clientId) {
            $parameter['kundennummer_extern'] = $clientId;
        }

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'createsepa'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);
var_dump($result);
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
        }

        $result = array_map(function ($line) {
            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * 
     * 
     * @return array|null
     */
    protected function getStammdaten($anschluss = null)
    {
        $timeZone = new \DateTimeZone('Europe/Berlin');
        $today = new \DateTime('now', $timeZone);

        $parameter = [
            'periode' => $today->format('Ymd'),
            'erweitert' => '10',
        ];

        if (null !== $anschluss) {
            $parameter['anschluss'] = $anschluss;
        }

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'stammdatenexport'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            while (1 === preg_match('/(\{[^;]+);(.*\})/', $line)) {
                $line = preg_replace('/(\{[^;]+);(.*\})/', '$1,-,$2', $line);
            }

            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * extract all sepa mandates from crawler
     * 
     * @return array
     */
    protected function findAllSepaMandateIds($crawler)
    {
        return $crawler
            ->filterXPath('//input[contains(@value, "Löschen Mandat ID: ")]')
            ->evaluate('substring-after(@value, "Löschen Mandat ID: ")');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $db = $this->getContainer()->get('doctrine')->getManager()->getConnection();

        //require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/tools/import/CsvReader.php';
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';

        $this->purtelSuperUser = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
        $this->purtelSuperUserPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
        //$this->purtelSuperUser = '673845'; // jle account
        //$this->purtelSuperUserPassword = 'e400b84f4752';
        $this->purtelReseller = 'res100260';


        $accounts = $this->getAccounts();
        $accountsHeadline = array_shift($accounts);

        foreach ($accounts as $account) {
            $stammdaten = $this->getStammdaten($account[1]);
            $purtelAccount = $account[1];

            echo sprintf("Prüfe %s :\n", $account[1]);
            
            if (empty($stammdaten)) {
                echo "\tVermutlich Testanschluss\n";

                continue;
            }

            if (!empty($stammdaten[1][123])) {
                // ist sub-account
                echo "\tist Sub-Account - nothing to do\n";

                continue;
            }



            // lade stammdaten
            /*$client = new Client();

            $crawler = $client->request('GET', 'https://www2.purtel.com/res/'.$this->purtelReseller.'/index.php?username='
                .$this->purtelSuperUser.'&passwort='
                .$this->purtelSuperUserPassword.'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1&admin_ns_suchbegriff='
                .$purtelAccount.'&tab_id=9&marke=Stammdaten');

            if ($crawler->selectButton('Ändern')->count() < 1) {
                echo "purtel account does not exist - purtel: ".$purtelAccount." - ".$realClientId."\n";

                continue;
            }*/

            $q = $db->query("SELECT `cust_id` FROM `purtel_account` WHERE `purtel_login` = ".$account[1]." LIMIT 1");

            if ($q->rowCount() < 1) {
                echo "\tikv user in purtel_account not found - account: ".$account[1]."\n";

                continue;
            }

            $customerId = $q->fetch(\PDO::FETCH_ASSOC);
            $customerId = $customerId['cust_id'];

            $q = $db->query("SELECT `bank_account_debitor`, `bank_account_mandantenid` FROM `customers` WHERE `id` = '".$customerId."'");

            if ($q->rowCount() < 1) {
                echo "\tikv user not found - customerId: ".$customerId."\n";

                continue;
            }

            $data = $q->fetch(\PDO::FETCH_ASSOC);


            //$stammdatenForm = $crawler->selectButton('Ändern')->form();

            if ((string) $data['bank_account_debitor'] !== (string) $stammdaten[1][49]) {
                echo sprintf("\tdebitor diff - %s : %s\n",
                    $data['bank_account_debitor'],
                    $stammdaten[1][49]
                );

                if (!empty($stammdaten[1][49]) && empty($data['bank_account_debitor'])) {
                    echo "\tUPDATE `customers` SET `bank_account_debitor` = '".$stammdaten[1][49]."' WHERE `id` = ".$customerId."\n";

                    if ($db->query("UPDATE `customers` SET `bank_account_debitor` = '".$stammdaten[1][49]."' WHERE `id` = ".$customerId)) {
                        echo "\tupdated ikv\n";
                    } else {
                        echo "\tupdate ikv failed!\n";
                    }
                } elseif (empty($stammdaten[1][49]) && !empty($data['bank_account_debitor'])) {
                    echo "\tDebitor fehlt bei purtel\n";
                } else {
                    echo "\tprüfen!!!\n";

                    continue;
                }
            }


            echo "\tok\n";

            //mandat_mandatsid


            // userfield3
/*49]=>
    string(10) "userfield3
    */


            
        }
    }
}