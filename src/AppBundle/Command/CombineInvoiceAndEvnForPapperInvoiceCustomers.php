<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Goutte\Client;

/**
 * 
 */
class CombineInvoiceAndEvnForPapperInvoiceCustomers extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('CombineInvoiceAndEvnForPapperInvoiceCustomers')
            ->setDescription('')
        ;
    }

    /**
     * 
     * 
     * @return array|null
     */
    protected function getAccounts($clientId = null)
    {
        $parameter = [
            'erweitert' => 2,
        ];

        if (null !== $clientId) {
            $parameter['kundennummer_extern'] = $clientId;
        }

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'getaccounts'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * 
     * 
     * @return array|null
     */
    protected function getRechnungen($anschluss = null, \DateTime $fromDate, \DateTime $tillDate, $format = null)
    {
        $parameter = [
            'von_datum' => $fromDate->format('Ymd'),
            'bis_datum' => $tillDate->format('Ymd'),
            'erweitert' => 3,
        ];

        if (null !== $anschluss) {
            $parameter['anschluss'] = $anschluss;
        }

        if (null !== $format) {
            $parameter['format'] = $format;
        }

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'getrechnungen'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);

        if ('SAP' == $format) {
            return $content;
        }
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * 
     * 
     * @return array|null
     */
    protected function getStammdaten($anschluss = null)
    {
        $timeZone = new \DateTimeZone('Europe/Berlin');
        $today = new \DateTime('now', $timeZone);

        $parameter = [
            'periode' => $today->format('Ymd'),
            //'periode' => '201806',
            'erweitert' => '5',
        ];

        if (null !== $anschluss) {
            $parameter['anschluss'] = $anschluss;
        }

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'stammdatenexport'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            while (1 === preg_match('/(\{[^;]+);(.*\})/', $line)) {
                $line = preg_replace('/(\{[^;]+);(.*\})/', '$1,-,$2', $line);
            }

            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * 
     * 
     * @return array|null
     */
    protected function getDokuments($anschluss)
    {
        $parameter = [
            'anschluss' => $anschluss,
        ];

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'get_dokument'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);

        // 
        if (!empty($content)) {
            $content = "\n".$content;
        }
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            while (1 === preg_match('/(\{[^;]+);(.*\})/', $line)) {
                $line = preg_replace('/(\{[^;]+);(.*\})/', '$1,-,$2', $line);
            }

            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * 
     */
    protected function getStammdatenForm($purtelAccount)
    {
        $client = new Client();

        $crawler = $client->request('GET', 'https://www2.purtel.com/res/'.$this->purtelReseller.'/index.php?username='
            .$this->purtelSuperUser.'&passwort='
            .$this->purtelSuperUserPassword.'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1&admin_ns_suchbegriff='
            .$purtelAccount.'&tab_id=9&marke=Stammdaten');

        $form = $crawler->selectButton('Ändern')->form();

        return $form;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/tools/import/CsvReader.php';
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';

        $this->purtelSuperUser = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
        $this->purtelSuperUserPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
        $this->purtelReseller = 'res100260';

        $fromDate = new \DateTime('01.09.2018');
        $tillDate = new \DateTime('30.09.2018');
        $periode = '201809'; // jjjjmm

        $filesdir = $this->getContainer()->get('kernel')->getRootDir().'/../evn-rechnungen-combined/'.$periode;

        if (!is_dir($filesdir)) {
            mkdir($filesdir, 0755, true);
        }

        if (file_exists($filesdir.'/all-combined.pdf')) {
            echo "file exists (".$filesdir.'/all-combined.pdf'.") - aborting\n";

            return;
        }

        $accounts = $this->getAccounts();
        $accountsHeadline = array_shift($accounts);
/*  [0]=>
  array(7) {
    [0]=>
    string(19) "kundennummer_extern"
    [1]=>
    string(9) "anschluss"
    [2]=>
    string(11) "sipusername"
    [3]=>
    string(8) "passwort"
    [4]=>
    string(8) "gesperrt"
    [5]=>
    string(7) "produkt"
    [6]=>
    string(8) "callerid"
  }
*/

        $combinedFiles = [];

        foreach ($accounts as $account) {
            $clientId = $account[0];
            $anschluss = $account[1];

            echo $clientId."\n";

            //$anschluss = '675128'; // 50000.18.0164

            $rechnungen = $this->getRechnungen($anschluss, $fromDate, $tillDate);

            if (null === $rechnungen) {
                echo "keine rechungen - continue\n";

                continue;
            }

/*if (in_array($clientId, ['30000.18.0006'])) {
continue;
}*/
            try {
                $stammdaten = $this->getStammdatenForm($anschluss);
            } catch (\GuzzleHttp\Exception\ConnectException $exception) {
                echo "getStammdatenForm exception : ".$exception->getMessage()." - continue\n";

                continue;
            }

            if ("1" != $stammdaten['anschluss_postversand']->getValue()) {
                echo "keine papierrechung. continue\n";

                continue;
            }

            $rechnungenHeadline = array_shift($rechnungen);

            if (count($rechnungen) !== 1) {
                echo "beim kunden ".count($rechnungen)." Rechnungen gefunden. 1 erwartet. continue";

                continue;
            }

            //$rechnung = $rechnungen[0];

            /*
              [0]=>
                  array(24) {
                    [0]=>
                    string(2) "id"
                    [1]=>
                    string(5) "datum"
                    [2]=>
                    string(7) "mandant"
                    [3]=>
                    string(9) "anschluss"
                    [4]=>
                    string(8) "renummer"
                    [5]=>
                    string(6) "betrag"
                    [6]=>
                    string(12) "umsatzsteuer"
                    [7]=>
                    string(8) "waehrung"
                    [8]=>
                    string(10) "dollarkurs"
                    [9]=>
                    string(10) "reformular"
                    [10]=>
                    string(14) "rechnungsdatum"
                    [11]=>
                    string(17) "faelligkeitsdatum"
                    [12]=>
                    string(10) "unterkonto"
                    [13]=>
                    string(12) "export_netto"
                    [14]=>
                    string(13) "export_steuer"
                    [15]=>
                    string(13) "export_brutto"
                    [16]=>
                    string(14) "export_zahlung"
                    [17]=>
                    string(14) "export_debitor"
                    [18]=>
                    string(21) "export_rechnungsdatum"
                    [19]=>
                    string(18) "export_faelligkeit"
                    [20]=>
                    string(19) "kundennummer_extern"
                    [21]=>
                    string(21) "zahlart_rechnungslauf"
                    [22]=>
                    string(18) "zahlart_stammdaten"
                    [23]=>
                    string(7) "billing"
                  }

            */

            $documents = $this->getDokuments($anschluss);

            if (empty($documents)) {
                echo "keine dokumente gefunden. continue";

                continue;
            }

            $pdfRechung = null;
            $pdfRechungFilename = null;
            $pdfEvn = null;
            $pdfEvnFilename = null;

            foreach ($documents[1] as $filename) {
                if (1 === preg_match('/\/Rechnung_\d+_'.$periode.'_.*\.pdf/', $filename)) {
                    if (null !== $pdfRechung) {
                        echo "mehfach rechnung für gleiche periode gefunden. continue\n";
                        
                        var_dump($filename, $pdfRechung);
                        
                        continue 2;
                    }

                    $pdfRechung = $filename;
                    $pdfRechungFilename = basename($filename);

                    continue;
                }

                if (1 === preg_match('/\/EVN_\d+_'.$periode.'_.*\.pdf/', $filename)) {
                    if (null !== $pdfEvn) {
                        echo "mehfach ENV_ für gleiche periode gefunden. continue\n";
                        
                        var_dump($filename, $pdfEvn);
                        
                        continue 2;
                    }

                    $pdfEvn = $filename;
                    $pdfEvnFilename = basename($filename);

                    continue;
                }
            }

            if (empty($pdfRechung) || empty($pdfEvn)) {
                echo "env oder rechnung nicht gefunden. continue\n";

                continue;
            }

            $customerFilesDir = $filesdir.'/'.$anschluss;

            if (!is_dir($customerFilesDir)) {
                mkdir($customerFilesDir);
            } else {
                echo "irgendwie wurde dieser Anschluss schon bearbeitet. komisch. prüfen. continue\n";

                continue;
            }

            $streamContext = stream_context_create([
                'ssl' => [
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true,
                ],
            ]);

            file_put_contents(
                $customerFilesDir.'/'.$pdfRechungFilename,
                file_get_contents($pdfRechung, false, $streamContext)
            );
            file_put_contents(
                $customerFilesDir.'/'.$pdfEvnFilename,
                file_get_contents($pdfEvn, false, $streamContext)
            );

            // prüfen, ob die files gedownloaded wurden ..

            // prüfen, ob evn leer ist
            $evnHtmlContent = shell_exec(sprintf('pdftohtml -hidden -stdout %s',
                escapeshellarg($customerFilesDir.'/'.$pdfEvnFilename)
            ));

            if (false === stripos($evnHtmlContent, 'ZielCall')) {
                // evn ist leer
                if (!copy($customerFilesDir.'/'.$pdfRechungFilename, $customerFilesDir.'/combined.pdf')) {
                    echo "failed to set rechnung from 'no-evn' to combined.pdf - aborting\n";

                    return;
                }
            } else {
                $pdfUnite = shell_exec(sprintf("pdfunite '%s' '%s' '%s' 2>&1",
                    $customerFilesDir.'/'.$pdfRechungFilename,
                    $customerFilesDir.'/'.$pdfEvnFilename,
                    $customerFilesDir.'/combined.pdf'
                ));

                if (!empty($pdfUnite)) {
                    echo "pdfunite error ".$pdfUnite." - continue\n";

                    continue;
                }
            }

            //$combinedFiles[] = $customerFilesDir.'/combined.pdf';
            if (file_exists($filesdir.'/all-combined.pdf')) {
                $pdfUnite = shell_exec(sprintf("pdfunite '%s' '%s' '%s' 2>&1",
                    $filesdir.'/all-combined.pdf',
                    $customerFilesDir.'/combined.pdf',
                    $filesdir.'/all-combined2.pdf'
                ));

                if (!empty($pdfUnite)) {
                    echo sprintf("pdfunite error (to total combined) %s - command: %s continue\n",
                        $pdfUnite,
                        sprintf("pdfunite '%s' '%s' '%s' 2>&1",
                            $filesdir.'/all-combined.pdf',
                            $customerFilesDir.'/combined.pdf',
                            $filesdir.'/all-combined2.pdf'
                        )
                    );

                    continue;
                }

                if (!unlink($filesdir.'/all-combined.pdf')) {
                    echo "unable to delete old all-combined.pdf\n";
                }

                if (!rename($filesdir.'/all-combined2.pdf', $filesdir.'/all-combined.pdf')) {
                    echo "unable to rename all-combined2.pdf to all-combined.pdf\n";
                }
            } else {
                if (!copy($customerFilesDir.'/combined.pdf', $filesdir.'/all-combined.pdf')) {
                    echo "failed to set all-combined.pdf from first combined.pdf - aborting\n";

                    return;
                }
            }
        }

        // combine all combined pdf into one

        /*$commandFiles = '';

        foreach ($combinedFiles as $filename) {
            $commandFiles .= "'".$filename."' ";
        }

        $pdfUnite = shell_exec(sprintf("pdfunite %s'%s'",
            $commandFiles,
            $filesdir.'/all-combined.pdf'
        ));*/

        echo "completely done\n";
    }
}

?>