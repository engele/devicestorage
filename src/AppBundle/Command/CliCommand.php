<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use AppBundle\Entity\User;

/**
 * 
 */
class CliCommand extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('Admin:cli')
            ->setDescription('')
            ->addArgument(
                'which',
                InputArgument::REQUIRED,
                'example|example|...'
            )
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        switch ($input->getArgument('which')) {
            /**
             * Create Admin Account
             */
            case 'create-admin-account':
                $username = 'admin';
                $password = 'admin';
                $email = 'your-email@example.de';
                $fullName = 'This is admin';

                $user = new User();

                $encodedPassword = $this->getContainer()->get('security.password_encoder')->encodePassword($user, $password);

                $user->setUsername($username)
                    ->setPassword($encodedPassword)
                    ->setEmail($email)
                    ->setFullName($fullName)
                    ->setRoles(array('ROLE_ADMIN'));

                $em = $this->getContainer()->get('doctrine')->getManager();
                $em->persist($user);

                try {
                    $em->flush();

                    echo "Created Admin-Account!\n";
                } catch (\Doctrine\DBAL\Exception\UniqueConstraintViolationException $exception) {
                    echo "Admin-Account exists\n";
                }

                return;
        }

        throw new InvalidArgumentException('which');
    }
}

?>