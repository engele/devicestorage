<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Goutte\Client;

/**
 * 
 */
class FindDuplicatesInZusatzleistungen extends ContainerAwareCommand
{
    protected $purtelMainProducts = [ // price = brutto
        '4578' => ['name' => 'Leistungsfrei', 'price' => '0', 'sap' => null],
        '4582' => ['name' => 'Fon / 24 Monate', 'price' => '1495', 'sap' => '412080'],
        '4794' => ['name' => 'Web 100 / 24 Monate', 'price' => '2495', 'sap' => '412080'],
        '4796' => ['name' => 'HD-TV & Fon / 24 Monate', 'price' => '2495', 'sap' => '412080'],
        '4798' => ['name' => 'Komfort 100 / 24 Monate', 'price' => '2995', 'sap' => '412080'],
        '4842' => ['name' => 'HD-TV / 24 Monate', 'price' => '995', 'sap' => '412080'],
        '4870' => ['name' => 'HD-TV & Web 100 / 24 Monate', 'price' => '3495', 'sap' => '412080'],
        '4876' => ['name' => 'Premium 100 / 24 Monate', 'price' => '3995', 'sap' => '412080'],
        '4979' => ['name' => 'Fon', 'price' => '1495', 'sap' => '412080'],
        '4981' => ['name' => 'HD-TV & Web 100', 'price' => '3495', 'sap' => '412080'],
        '4983' => ['name' => 'Web 100', 'price' => '2495', 'sap' => '412080'],
        '4985' => ['name' => 'Komfort 100', 'price' => '2995', 'sap' => '412080'],
        '4987' => ['name' => 'Premium 100', 'price' => '3995', 'sap' => '412080'],
        '4989' => ['name' => 'HD-TV & Fon', 'price' => '2495', 'sap' => '412080'],
        '4991' => ['name' => 'HD-TV', 'price' => '995', 'sap' => '412080'],
        '5130' => ['name' => 'Business I3 / 36 Monate', 'price' => '5343', 'sap' => '412230'],
        '5132' => ['name' => 'Business S1-AA/2', 'price' => '2368', 'sap' => '412220'],
        '5142' => ['name' => 'Glasfaser Premium 100', 'price' => '4990', 'sap' => '412080'],
        '5144' => ['name' => 'Glasfaser Komfort 100', 'price' => '3990', 'sap' => '412080'],
        '5146' => ['name' => 'Glasfaser Web', 'price' => '3490', 'sap' => '412080'],
        '5150' => ['name' => 'Glasfaser Fon', 'price' => '2190', 'sap' => '412080'],
        '5152' => ['name' => 'Glasfaser HD-TV', 'price' => '1390', 'sap' => '412080'],
        '5154' => ['name' => 'Glasfaser COM-FIX GeWo ', 'price' => '0', 'sap' => '412080'],
        '5156' => ['name' => 'Glasfaser Premium 50', 'price' => '3990', 'sap' => '412080'],
        '5158' => ['name' => 'Glasfaser Premium 100 / 24 Monate', 'price' => '4990', 'sap' => '412080'],
        '5160' => ['name' => 'Glasfaser Premium 50 / 24 Monate', 'price' => '3990', 'sap' => '412080'],
        '5162' => ['name' => 'Glasfaser Komfort 100 / 24 Monate', 'price' => '3990', 'sap' => '412080'],
        '5164' => ['name' => 'Glasfaser Komfort 50', 'price' => '2990', 'sap' => '412080'],
        '5166' => ['name' => 'Glasfaser Komfort 50 / 24 Monate', 'price' => '2990', 'sap' => '412080'],
        '5168' => ['name' => 'Glasfaser Web 100', 'price' => '2990', 'sap' => '412080'],
        '5170' => ['name' => 'Glasfaser Web 100 / 24 Monate', 'price' => '2990', 'sap' => '412080'],
        '5172' => ['name' => 'Glasfaser Web 50', 'price' => '2489', 'sap' => '412080'],
        '5174' => ['name' => 'Glasfaser Web 50 / 24 Monate', 'price' => '2489', 'sap' => '412080'],
        '5258' => ['name' => 'Glasfaser Fon / 24 Monate', 'price' => '1990', 'sap' => '412080'],
        '5260' => ['name' => 'Glasfaser Fon und HD-TV', 'price' => '2990', 'sap' => '412080'],
        '5262' => ['name' => 'Glasfaser Fon und HD-TV / 24 Monate', 'price' => '2990', 'sap' => '412080'],
        '5264' => ['name' => 'Glasfaser HD-TV / 24 Monate', 'price' => '1390', 'sap' => '412080'],
        '5266' => ['name' => 'Glasfaser Web 50 und HD-TV / 24 Monate', 'price' => '3490', 'sap' => '412080'],
        '5268' => ['name' => 'Glasfaser Web 25', 'price' => '1990', 'sap' => '412080'],
        '5270' => ['name' => 'Glasfaser Web 25 / 24 Monate', 'price' => '1990', 'sap' => '412080'],
        '5272' => ['name' => 'Glasfaser Komfort 25 / 24 Monate', 'price' => '2489', 'sap' => '412080'],
        '5274' => ['name' => 'Glasfaser Premium 25 / 24 Monate', 'price' => '3490', 'sap' => '412080'],
        '5360' => ['name' => 'Business S1-MGA/2', 'price' => '2368', 'sap' => '412220'],
        '5362' => ['name' => 'Business S2-AA/5', 'price' => '4748', 'sap' => '412220'],
        '5364' => ['name' => 'Business S3-AA/10', 'price' => '11888', 'sap' => '412220'],
        '5366' => ['name' => 'Business S4 AA/30', 'price' => '11888', 'sap' => '412220'],
        '5444' => ['name' => 'Premium 100 / 24 Monate + LP1', 'price' => '3995', 'sap' => '412080'],
        '5494' => ['name' => 'Glasfaser Komfort 50 + LP 1', 'price' => '2990', 'sap' => '412080'],
        '5496' => ['name' => 'Glasfaser COM-FIX GeWo + LP 1', 'price' => '0', 'sap' => '412080'],
        '5498' => ['name' => 'Glasfaser COM-FIX GeWo + LP 1 & LP 2', 'price' => '0', 'sap' => '412080'],
        '5500' => ['name' => 'Glasfaser COM-FIX GeWo + LP 2', 'price' => '0', 'sap' => '412080'],
        '5502' => ['name' => 'Glasfaser Fon und HD-TV + LP 1', 'price' => '2990', 'sap' => '412080'],
        '5504' => ['name' => 'Glasfaser Fon + LP 1', 'price' => '2190', 'sap' => '412080'],
        '5506' => ['name' => 'Glasfaser Komfort 100 + LP 1', 'price' => '3990', 'sap' => '412080'],
        '5508' => ['name' => 'Glasfaser Komfort 100 + LP 2', 'price' => '3990', 'sap' => '412080'],
        '5510' => ['name' => 'Glasfaser Komfort 25 + LP 1', 'price' => '2489', 'sap' => '412080'],
        '5512' => ['name' => 'Glasfaser Komfort 50 + LP 2', 'price' => '2990', 'sap' => '412080'],
        '5514' => ['name' => 'Glasfaser Premium 100 + LP 1', 'price' => '4990', 'sap' => '412080'],
        '5516' => ['name' => 'Glasfaser Premium 50 + LP 1', 'price' => '3990', 'sap' => '412080'],
        '5518' => ['name' => 'Glasfaser Premium 50 + LP 2', 'price' => '3990', 'sap' => '412080'],
        '5520' => ['name' => 'Premium 100 + LP1', 'price' => '3990', 'sap' => '412080'],
        '5524' => ['name' => 'Komfort 100 + LP 1', 'price' => '2990', 'sap' => '412080'],
        '5526' => ['name' => 'Komfort 100 + LP 2', 'price' => '2990', 'sap' => '412080'],
        '5528' => ['name' => 'Komfort 100 / 24 Monate + LP 1', 'price' => '2990', 'sap' => '412080'],
        '5530' => ['name' => 'Komfort 100 / 24 Monate + LP 2', 'price' => '2990', 'sap' => '412080'],
        '5664' => ['name' => 'Business I2 / 36 Monate', 'price' => '4153', 'sap' => '412230'],
        '5666' => ['name' => 'Business I1 / 36 Monate', 'price' => '2963', 'sap' => '412230'],
        '5532' => ['name' => 'Test-voip-dabei', 'price' => '0', 'sap' => '412080'],
        '5838' => ['name' => 'Willkommen - Komfort 100 / 24 Monate', 'price' => '1495', 'sap' => '412080'],
        '5840' => ['name' => 'Willkommen - Web 100 / 24 Monate', 'price' => '1495', 'sap' => '412080'],
        '5852' => ['name' => 'KabelKiosk Basis HD', 'price' => '490', 'sap' => '412080'],
        '5858' => ['name' => 'KabelKiosk Family HD inkl. Basis HD', 'price' => '1990', 'sap' => '412080'],
    ];

    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('FindDuplicatesInZusatzleistungen')
            ->setDescription('')
        ;
    }

    /**
     * 
     * 
     * @return array|null
     */
    protected function getAccounts($clientId = null)
    {
        $parameter = [
            'erweitert' => 2,
        ];

        if (null !== $clientId) {
            $parameter['kundennummer_extern'] = $clientId;
        }

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'getaccounts'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * 
     * 
     * @return array|null
     */
    protected function getStammdaten($anschluss = null)
    {
        $timeZone = new \DateTimeZone('Europe/Berlin');
        $today = new \DateTime('now', $timeZone);

        $parameter = [
            'periode' => $today->format('Ymd'),
            //'periode' => '201806',
            'erweitert' => '5',
        ];

        if (null !== $anschluss) {
            $parameter['anschluss'] = $anschluss;
        }

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'stammdatenexport'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            while (1 === preg_match('/(\{[^;]+);(.*\})/', $line)) {
                $line = preg_replace('/(\{[^;]+);(.*\})/', '$1,-,$2', $line);
            }

            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * 
     */
    protected function getProductsFromPurtel($purtelAccount)
    {
        // hole alle "gebuchten" Produkte des Kunden
        $bookedProducts = [];
       
        $client = new Client();
        
        $crawler = $client->request('GET', 'https://www2.purtel.com/res/'.$this->purtelReseller.'/index.php?username='
            .$this->purtelSuperUser.'&passwort='
            .$this->purtelSuperUserPassword.'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1&admin_ns_suchbegriff='
            .$purtelAccount.'&tab_id=3&marke=Zusatzleistungen');

        $crawler->selectButton('Ändern')->each(function ($node) use (&$bookedProducts) {
            $element = $node->parents()->eq(2);

            $matches = null;

            preg_match('/^\[ID (\d+)\] (Produkt|Crossselling) \[(\d+)\] (.*)$/', $element->filter('tr > td')->first()->text(), $matches);

            $form = $node->form();

            $bookedProduct = [
                'id' => $matches[1],
                'productId' => $matches[3],
                'name' => $matches[4],
                'form' => $form,
            ];

            $bookedProducts[$bookedProduct['productId']] = $bookedProduct;
        });

        return [
            'bookedProducts' => $bookedProducts,
            'usedCrawler' => $crawler,
        ];
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/tools/import/CsvReader.php';
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';

        $this->purtelSuperUser = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
        $this->purtelSuperUserPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
        $this->purtelReseller = 'res100260';

        $accounts = $this->getAccounts();
        $accountsHeadline = array_shift($accounts);
/*  [0]=>
  array(7) {
    [0]=>
    string(19) "kundennummer_extern"
    [1]=>
    string(9) "anschluss"
    [2]=>
    string(11) "sipusername"
    [3]=>
    string(8) "passwort"
    [4]=>
    string(8) "gesperrt"
    [5]=>
    string(7) "produkt"
    [6]=>
    string(8) "callerid"
  }
*/

        foreach ($accounts as $account) {
            echo sprintf("checking %s:\n",
                $account[1]
            );

            try {
                $products = $this->getProductsFromPurtel($account[1]);
            } catch (\GuzzleHttp\Exception\ConnectException $exception) {
                echo sprintf("unable to load product for anschluss %s - error: %s\n",
                    $account[1],
                    $exception->getMessage()
                );

                continue;
            }

            $alreadySeenProducts = [];

            foreach ($products['bookedProducts'] as $data) {
                if (isset($alreadySeenProducts[$data['productId']])) {
                    $isMainProduct = false;

                    if (isset($this->$purtelMainProducts[$data['productId']])) {
                        $isMainProduct = true;
                    }

                    echo sprintf("Anschluss %s hat mehrfach das Produkt mit der Id %s - %s\n",
                        $account[1],
                        $data['productId'],
                        $isMainProduct ? 'Haupt-Produkt' : 'Crossselling'
                    );

                    continue;
                }

                $alreadySeenProducts[$data['productId']] = true;
            }
        }

        echo "completely done\n";
    }
}

?>