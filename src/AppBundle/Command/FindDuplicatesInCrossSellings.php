<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Goutte\Client;

/**
 * 
 */
class FindDuplicatesInCrossSellings extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('FindDuplicatesInCrossSellings')
            ->setDescription('')
        ;
    }

    /**
     * 
     * 
     * @return array|null
     */
    protected function getAccounts($clientId = null)
    {
        $parameter = [
            'erweitert' => 2,
        ];

        if (null !== $clientId) {
            $parameter['kundennummer_extern'] = $clientId;
        }

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'getaccounts'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * 
     * 
     * @return array|null
     */
    protected function getStammdaten($anschluss = null)
    {
        $timeZone = new \DateTimeZone('Europe/Berlin');
        $today = new \DateTime('now', $timeZone);

        $parameter = [
            'periode' => $today->format('Ymd'),
            //'periode' => '201806',
            'erweitert' => '5',
        ];

        if (null !== $anschluss) {
            $parameter['anschluss'] = $anschluss;
        }

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'stammdatenexport'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            while (1 === preg_match('/(\{[^;]+);(.*\})/', $line)) {
                $line = preg_replace('/(\{[^;]+);(.*\})/', '$1,-,$2', $line);
            }

            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/tools/import/CsvReader.php';
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';

        $this->purtelSuperUser = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
        $this->purtelSuperUserPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
        $this->purtelReseller = 'res100260';

        $accounts = $this->getAccounts();
        $accountsHeadline = array_shift($accounts);
/*  [0]=>
  array(7) {
    [0]=>
    string(19) "kundennummer_extern"
    [1]=>
    string(9) "anschluss"
    [2]=>
    string(11) "sipusername"
    [3]=>
    string(8) "passwort"
    [4]=>
    string(8) "gesperrt"
    [5]=>
    string(7) "produkt"
    [6]=>
    string(8) "callerid"
  }
*/

        foreach ($accounts as $account) {
            $stammdaten = $this->getStammdaten($account[1]);
            
            if (!empty($stammdaten)) {
                $crossSellings = null;
                $duplicates = [];

                preg_match_all('/(\d+)=/', $stammdaten[1][81], $crossSellings);

                if (isset($crossSellings[1]) && !empty($crossSellings[1])) {
                    $found = [];

                    foreach ($crossSellings[1] as $productId) {
                        if (isset($found[$productId])) {
                            if (isset($duplicates[$productId])) {
                                $duplicates[$productId]++;
                            } else {
                                $duplicates[$productId] = 2;
                            }
                        }

                        $found[$productId] = true;
                    }
                }

                if (!empty($duplicates)) {
                    echo "multiple crosssellings found - ".$account[1]."\n";

                    foreach ($duplicates as $productId => $counter) {
                        echo " - ".$counter."x ".$productId."\n";
                    }

                    echo "\n";
                } else {
                    echo $account[1]." - good\n";
                }

                //var_dump($account[1], $stammdaten[1][81], $crossSellings);
            }
        }

        echo "completely done\n";
    }
}

?>