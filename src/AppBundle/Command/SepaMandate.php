<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Goutte\Client;

/**
 * 
 */
class SepaMandate extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('SepaMandate')
            ->setDescription('')
        ;
    }

    /**
     * extract active sepa mandates from crawler
     * 
     * @return array
     */
    protected function findActiveSepaMandateIds($crawler)
    {
        return $crawler
            ->filterXPath('//input[contains(@value, "Deaktivieren Mandat ID: ")]')
            ->evaluate('substring-after(@value, "Deaktivieren Mandat ID: ")');
    }

    /**
     * extract inactive sepa mandates from crawler
     * 
     * @return array
     */
    protected function findInctiveSepaMandateIds($crawler)
    {
        return $crawler
            ->filterXPath('//input[contains(@value, "Aktivieren Mandat ID: ")]')
            ->evaluate('substring-after(@value, "Aktivieren Mandat ID: ")');
    }

    /**
     * extract all sepa mandates from crawler
     * 
     * @return array
     */
    protected function findAllSepaMandateIds($crawler)
    {
        return $crawler
            ->filterXPath('//input[contains(@value, "Löschen Mandat ID: ")]')
            ->evaluate('substring-after(@value, "Löschen Mandat ID: ")');
    }

    /**
     * @return boolean
     */
    protected function deleteSepaMandate($sepaMandateId, $crawler)
    {
        $form = $crawler->selectButton('Löschen Mandat ID: '.$sepaMandateId)->form();

        $client = new Client();

        $responseCrawler = $client->submit($form);

        return $responseCrawler->selectButton('Löschen Mandat ID: '.$sepaMandateId)->count() < 1;
    }

    /**
     * @return boolean
     */
    protected function createNewSepa($purtelAccount, $mandatsReferenz)
    {
        // create (inactive) sepa mandate at purtel
        $client = new Client();
            
        $crawler = $client->request('GET', 'https://www2.purtel.com/res/'.$this->purtelReseller.'/index.php?username='
            .$this->purtelSuperUser.'&passwort='
            .$this->purtelSuperUserPassword.'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1&admin_ns_suchbegriff='
            .$purtelAccount.'&tab_id=9&marke=Stammdaten');

        $form = $crawler->selectButton('SEPA-Mandat erstellen!')->form();

        $form['mandat_lfd_nummer_ausblenden']->tick();
        $form['mandat_mandatsid']->setValue($mandatsReferenz);
        $form['mandat_sequenz']->select('FRST');

        $fixEncodeing = [
            'mandat_inhaber',
            'mandat_inhaber_adresse',
            'mandat_inhaber_plz',
            'mandat_inhaber_ort',
            'mandat_bank',
        ];

        foreach ($fixEncodeing as $element) {
            $form[$element]->setValue(utf8_decode(
                $form[$element]->getValue()
            ));
        }

        $responseCrawler = $client->submit($form);


        // after creating a sepa mandate it is deactivated
        // activate it now
        $inactiveMandates = $this->findInctiveSepaMandateIds($responseCrawler);

        if (count($inactiveMandates) < 1) {
            echo "nach erstellung wurde kein inaktives mandat gefunden - purtel: ".$purtelAccount."\n";

            return false;
        }

        $mandatsId = $inactiveMandates[0];

        $form = $responseCrawler->selectButton('Aktivieren Mandat ID: '.$mandatsId)->form();

        $responseCrawler = $client->submit($form);


        // verify that correct sepa mandate is now active
        $activeMandates = $this->findActiveSepaMandateIds($responseCrawler);

        if (count($activeMandates) < 1) {
            echo "nach aktivierung wurde kein aktives mandat gefunden - purtel: ".$purtelAccount."\n";

            return false;
        }

        $mandatsId = $activeMandates[0];

        $match = [];

        if (1 !== preg_match('/<td align="center">'.$mandatsId.'<\/td>[\s\n]+<td align="center">(.*)<\/td>/', $responseCrawler->html(), $match)) {
            echo "erwatetes html mit mandatsreferenz nicht gefunden - purtel: ".$purtelAccount."\n";
            
            return false;
        }

        $referenz = $match[1];

        if ($mandatsReferenz === $referenz) {
            return true;
        }

        echo "richtiges mandat wurden nach der aktivierung nicht gefunden - purtel: ".$purtelAccount."\n";

        return false;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/tools/import/CsvReader.php';

        $db = $this->getContainer()->get('doctrine')->getManager()->getConnection();

        $this->purtelSuperUser = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
        $this->purtelSuperUserPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
        $this->purtelReseller = 'res100260';


        //$csv = new \CsvReader($this->getContainer()->get('kernel')->getRootDir().'/../web/tools/import/comin/export.csv');
        $csv = new \CsvReader('/tmp/sepas.csv');

        foreach ($csv->data as $value){
            $clientid = empty($value[$csv->headlineReversed['Kontonummer IKV']]) ? null : $value[$csv->headlineReversed['Kontonummer IKV']];
            $iban = empty($value[$csv->headlineReversed['IBAN']]) ? null : $value[$csv->headlineReversed['IBAN']];
            $kontoinhaber = empty($value[$csv->headlineReversed['Name 1']]) ? null : $value[$csv->headlineReversed['Name 1']];
            $mandatenid = empty($value[$csv->headlineReversed['Mandatsreferenz']]) ? null : $value[$csv->headlineReversed['Mandatsreferenz']];
            $debitor = empty($value[$csv->headlineReversed['Debitor']]) ? null : $value[$csv->headlineReversed['Debitor']];

            $q = $db->query("SELECT `clientid`, `bank_account_holder_lastname`, `city`, `zipcode`, `street`, `streetno`, `firstname`, `lastname`, `bank_account_bic_new`, `bank_account_iban`, `bank_account_bankname` FROM `customers` WHERE `id` = '".$clientid."'");

            if ($q->rowCount() < 1) {
                echo "ikv user not found - ".$clientid."\n";
                continue;
            }

            $data = $q->fetch(\PDO::FETCH_ASSOC);
            $realClientId = $data['clientid'];

            $q = $db->query("SELECT `purtel_login` FROM `purtel_account` WHERE `master` = 1 AND `cust_id` = ".$clientid);

            if ($q->rowCount() < 1) {
                echo "purtel master account not found - ".$realClientId."\n";
                continue;
            }

            $purtelAccount = $q->fetch(\PDO::FETCH_ASSOC);
            $purtelAccount = $purtelAccount['purtel_login'];

            // bankdaten noch mal utf8_decoded übertragen
            $inhaber = $data['bank_account_holder_lastname'];

            if (empty($inhaber)) {
                $inhaber = $data['firstname'].' '.$data['lastname'];
            }

            $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s'
                .'&art=1&kundennummer_extern=%s&inhaber=%s&IBAN=%s&BIC=%s&bank=%s&inhaber_adresse=%s&inhaber_plz=%s&inhaber_ort=%s'
                .'&userfield3=%s&anschluss=%s',
                $this->purtelSuperUser,
                $this->purtelSuperUserPassword,
                'changecontract',
                $realClientId,
                urlencode(utf8_decode($inhaber)),
                urlencode(utf8_decode($data['bank_account_iban'])),
                urlencode(utf8_decode($data['bank_account_bic_new'])),
                urlencode(utf8_decode($data['bank_account_bankname'])),
                urlencode(utf8_decode($data['street'].' '.$data['streetno'])),
                urlencode(utf8_decode($data['zipcode'])),
                urlencode(utf8_decode($data['city'])),
                urlencode(utf8_decode($debitor)),
                $purtelAccount
            );

            $curl = curl_init($url);

            curl_setopt_array($curl, array(
                CURLOPT_HEADER => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYPEER => false,
            ));

            $result = curl_exec($curl);

            curl_close($curl);

            if ('+OK' !== $result) {
                echo "error while updating data - ".$realClientId."\n";

                var_dump($result);

                continue;
            }

            // lade stammdaten
            $client = new Client();

            $crawler = $client->request('GET', 'https://www2.purtel.com/res/'.$this->purtelReseller.'/index.php?username='
                .$this->purtelSuperUser.'&passwort='
                .$this->purtelSuperUserPassword.'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1&admin_ns_suchbegriff='
                .$purtelAccount.'&tab_id=9&marke=Stammdaten');

            if ($crawler->selectButton('Ändern')->count() < 1) {
                echo "purtel account does not exist - purtel: ".$purtelAccount." - ".$realClientId."\n";

                continue;
            }

            // alle sepa mandate löschen
            $allSepaMandates = $this->findAllSepaMandateIds($crawler);

            if (count($allSepaMandates) > 0) {
                foreach ($allSepaMandates as $sepaMandateId) {
                    if (!$this->deleteSepaMandate($sepaMandateId, $crawler)) {
                        echo "sepa was not deleted - mandateId: ".$sepaMandateId." - ".$realClientId." - skipped..\n";

                        continue 2;
                    }
                }
            }

            if ($this->createNewSepa($purtelAccount, $mandatenid)) {
                echo "mandat korrekt angelegt - ".$realClientId."\n";
            } else {
                echo "mandat wurde NICHT angelegt - ".$realClientId."\n";
            }
        }

        echo "all done\n";
    }
}

?>