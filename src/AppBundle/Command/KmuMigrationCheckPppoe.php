<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Goutte\Client;
use Symfony\Component\HttpFoundation\Request;

/**
 * 
 */
class KmuMigrationCheckPppoe extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('KmuMigrationCheckPppoe')
            ->setDescription('')
        ;
    }

    /**
     * 
     */
    protected function getStammdatenForm($purtelAccount)
    {
        $client = new Client();

        $crawler = $client->request('GET', 'https://www2.purtel.com/res/'.$this->purtelReseller.'/index.php?username='
            .$this->purtelSuperUser.'&passwort='
            .$this->purtelSuperUserPassword.'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1&admin_ns_suchbegriff='
            .$purtelAccount.'&tab_id=9&marke=Stammdaten');

        $form = $crawler->selectButton('Ändern')->form();

        return $form;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/tools/import/CsvReader.php';
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';

        $this->purtelSuperUser = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
        $this->purtelSuperUserPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
        $this->purtelReseller = \Wisotel\Configuration\Configuration::get('purtelReseller');

        $db = $this->getContainer()->get('doctrine')->getManager()->getConnection();

        $kmuCsv = new \CsvReader('/tmp/Rufnummern_20180821-2.csv');

        $sortedByDimariId = [];

        foreach ($kmuCsv->data as $key => $line) {
            $dimariId = trim($line[$kmuCsv->headlineReversed['Dimari Kd.Nr.']]);
            $easybellId = trim($line[$kmuCsv->headlineReversed['kundennummer easybell']]);

            if (empty($dimariId)) {
                echo "empty dimari id - ".$easybellId."\n";

                continue;
            }

            $sortedByDimariId[$dimariId] = $kmuCsv->findLines([
                'Dimari Kd.Nr.' => $dimariId,
            ]);
        }

        $request = Request::createFromGlobals();

        foreach ($sortedByDimariId as $dimariId => $lines) {
            $customerQ = $db->query("SELECT id, clientid, pppoe_pin, `password` FROM customers WHERE customer_id = '".$dimariId."'");

            if ($customerQ->rowCount() < 1) {
                echo "no customer found for dimari-id - ".$dimariId."\n";

                continue;
            }

            $customers = [];

            while ($customer = $customerQ->fetch(\PDO::FETCH_ASSOC)) {
                if (empty($customer['pppoe_pin']) && empty($customer['password'])) {
                    echo "kein intet pwd oder pppoe-pin for dimari - ".$dimariId."\n";
                }

                $axirosAcsController = $this->getContainer()->get('AppBundle\Controller\AxirosAcsController');
                $axirosAcsController->setContainer($this->getContainer());
                $response = $axirosAcsController->fetchCpeIdByCid2Action($request, $customer['id']);

                $content = $response->getContent();

                if (false === strpos($content, '<span style="color: green;">Success</span>')) {
                    echo "unable to get cpe-id for customer ".$customer['id']."\n";

                    die($content);
                }




                /*$response = $this->getContainer()->forward('AppBundle\Controller\AxirosAcsController::fetchCpeIdByCid2Action', array(
                    'request'  => $request,
                    'id' => $customer['id'],
                ));

                if (200 !== $response->getStatus()) {
                    echo "status != 200 for customer - ".$customer['id']."\n";

                    continue;
                }

                die(dump( $response ));*/

                $customers[] = $customer;
            }
        }

        echo "completely done\n";
    }
}

?>