<?php

namespace AppBundle\Command\BackgroundProcessRunner;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use React\EventLoop\Factory;
use React\Socket\Server;
use React\Socket\ConnectionInterface;
use React\Stream\ReadableResourceStream;

/**
 * 
 */
class BackgroundProcessRunner extends ContainerAwareCommand
{
    /**
     * @var array
     */
    protected $runningProcesses = [];

    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('BackgroundProcessRunner')
            ->setDescription('')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $listenAt = sprintf('%s:%d',
            $this->getContainer()->getParameter('background_process_runner_ip'),
            $this->getContainer()->getParameter('background_process_runner_port')
        );
        
        $loop = Factory::create();
        $socket = new Server($listenAt, $loop);

        $socket->on('connection', function (ConnectionInterface $conn) use ($loop) {
            $conn->write("Ready to handle data\n");

            $conn->on('data', function ($data) use ($conn, $loop) {
                if ('list-running-processes' == $data) {
                    $conn->write(json_encode(array_map(function ($process) {
                        return [
                            'pid' => $process['pid'],
                            'command' => $process['recievedCommand']['command'].' '.implode(' ', $process['recievedCommand']['options']),
                            'startedAt' => $process['startedAt']->format(\DateTime::ATOM),
                        ];
                    }, $this->runningProcesses))."\n");

                    return;
                }

                $recievedCommand = json_decode($data, true);

                var_dump("Recieved command", $recievedCommand);

                if (isset($recievedCommand['get-output-for-pid'])) {
                    if (!isset($this->runningProcesses[$recievedCommand['get-output-for-pid']])) {
                        $conn->write("\0x04");

                        return;
                    }
                    
                    $conn->write($this->runningProcesses[$recievedCommand['get-output-for-pid']]['stdout']."\0x04");

                    return;
                }

                $pipes = null;
                $resource = proc_open(
                    $recievedCommand['command'].' '.implode(' ', array_map('escapeshellarg', $recievedCommand['options'])),
                    [
                        ['pipe', 'r'], 
                        ['pipe', 'w'], 
                        ['pipe', 'w']
                    ],
                    $pipes,
                    $recievedCommand['cwd']
                );

                if (!is_resource($resource)) {
                    $conn->write("Error: Unable to spawn process\n");

                    var_dump("Unable to spawn process");

                    return;
                }

                $status = proc_get_status($resource);

                $this->runningProcesses[$status['pid']] = [
                    'pid' => $status['pid'],
                    'resource' => $resource,
                    'pipes' => $pipes,
                    'startedAt' => new \DateTime(),
                    'recievedCommand' => $recievedCommand,
                    'stdout' => '',
                ];

                $conn->write("Process now running - pid: ".$status['pid']."\n");

                var_dump("Spawned new process", $this->runningProcesses[$status['pid']]);

                $pid = $status['pid'];

                $x = new ReadableResourceStream($pipes[1], $loop);
                $x->on('data', function ($data) use ($pid) {
                    $this->runningProcesses[$pid]['stdout'] .= $data;
                });

                $y = new ReadableResourceStream($pipes[2], $loop);
                $y->on('data', function ($data) use ($pid) {
                    $this->runningProcesses[$pid]['stdout'] .= $data;
                });
            });
        });

        $loop->addPeriodicTimer(5, function () {
            foreach ($this->runningProcesses as $key => $process) {
                $status = proc_get_status($process['resource']);

                if ($status['running']) {
                    continue;
                }

                // process has stopped, send mail
                var_dump("Process has stopped", $process);

                try {
                    $from = 'ProcessRunner';

                    if ($this->getContainer()->hasParameter('mailer_from')) {
                        $from = $this->getContainer()->getParameter('mailer_from');
                    }

                    $body = '<pre>'.nl2br(var_export($process, true)."\n\n".$process['stdout']).'</pre>';

                    $message = \Swift_Message::newInstance()
                        ->setSubject('Process has stopped')
                        ->setFrom($from)
                        ->setTo($process['recievedCommand']['sendOutputTo'])
                        /*->setBody(
                            $this->twig->render('email/send-results.html.twig', [
                                'results' => $resultsToSend,
                            ]),
                            'text/html'
                        )*/
                        ->setBody($body, 'text/html');
                    ;

                    if (!$this->getMailer()->send($message)) {
                        echo "message sending to ".$process['sendOutputTo']." failed!\n";
                    }
                } catch (\Exception $exception) {
                    var_dump($exception->getMessage());
                }

                foreach ($process['pipes'] as $pipe) {
                    if (is_resource($pipe)) {
                        fclose($pipe);
                    }
                }

                // remove from running processes
                unset($this->runningProcesses[$key]);
            }
        });

        $loop->run();
    }

    /**
     * Create new instance of Swift_Mailer.
     * Do not use symfonys service - $this->getContainer()->get('mailer') - here because it spools mails
     * and they only get send when script ends - but it never ends..
     */
    protected function getMailer() : \Swift_Mailer
    {
        $container = $this->getContainer();

        $transport = new \Swift_SmtpTransport($container->getParameter('mailer_host'), $container->getParameter('mailer_port'));

        if ($container->hasParameter('mailer_user')) {
            $transport->setUsername($container->getParameter('mailer_user'));
        }

        if ($container->hasParameter('mailer_password')) {
            $transport->setPassword($container->getParameter('mailer_password'));
        }

        if ($container->hasParameter('mailer_encryption')) {
            $transport->setEncryption($container->getParameter('mailer_encryption'));
        }

        return new \Swift_Mailer($transport);
    }
}

?>