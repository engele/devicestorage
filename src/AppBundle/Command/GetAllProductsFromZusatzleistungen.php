<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Goutte\Client;

/**
 * 
 */
class GetAllProductsFromZusatzleistungen extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('GetAllProductsFromZusatzleistungen')
            ->setDescription('')
        ;
    }

    /**
     * 
     */
    protected function getProductsFromPurtel($purtelAccount)
    {
        // hole alle "gebuchten" Produkte des Kunden
        $bookedProducts = [];
       
        $client = new Client();
        
        $crawler = $client->request('GET', 'https://www2.purtel.com/res/'.$this->purtelReseller.'/index.php?username='
            .$this->purtelSuperUser.'&passwort='
            .$this->purtelSuperUserPassword.'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1&admin_ns_suchbegriff='
            .$purtelAccount.'&tab_id=3&marke=Zusatzleistungen');

        $crawler->selectButton('Ändern')->each(function ($node) use (&$bookedProducts) {
            $element = $node->parents()->eq(2);

            $matches = null;

            preg_match('/^\[ID (\d+)\] (Produkt|Crossselling) \[(\d+)\] (.*)$/', $element->filter('tr > td')->first()->text(), $matches);

            $form = $node->form();

            $bookedProduct = [
                'id' => $matches[1],
                'productId' => $matches[3],
                'name' => $matches[4],
                'form' => $form,
            ];

            $bookedProducts[$bookedProduct['productId']] = $bookedProduct;
        });

        return [
            'bookedProducts' => $bookedProducts,
            'usedCrawler' => $crawler,
        ];
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $deactivierungsvalueToString = [
            0 => 'aktiv lassen',
            1 => 'deaktivieren nach Laufzeit',
            2 => 'deaktivieren und nullen',
        ];

        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/tools/import/CsvReader.php';
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';

        //$this->purtelSuperUser = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
        //$this->purtelSuperUserPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
        $this->purtelSuperUser = '673845'; // jle account
        $this->purtelSuperUserPassword = 'e400b84f4752';

        $this->purtelReseller = \Wisotel\Configuration\Configuration::get('purtelReseller');


        

        echo sprintf('"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s"',
            'aktiv',
            'ikv',
            'purtelAccount',
            'buchungsDatum',
            'laufzeit',
            'deaktivierung',
            'berechnen',
            'productId',
            'productName',
            'basisentgeldBrutto',
            'basisentgeldNetto',
            'einrichtungBrutto',
            'einrichtungNetto',
            'istGutschrift'
        )."\n";

        $db = $this->getContainer()->get('doctrine')->getManager()->getConnection();

        $q = $db->query("SELECT pa.`purtel_login`, c.`clientid` 
            FROM `purtel_account` pa 
            INNER JOIN `customers` c ON c.`id` = pa.`cust_id` 
            WHERE pa.`master` = 1");

        while ($data = $q->fetch(\PDO::FETCH_ASSOC)) {
            $purtelAccount = $data['purtel_login'];
            $clientId = $data['clientid'];

            try {
                $products = $this->getProductsFromPurtel($purtelAccount);
            } catch (\GuzzleHttp\Exception\ConnectException $exception) {
                echo sprintf("unable to load product for anschluss %s - error: %s\n",
                    $purtelAccount,
                    $exception->getMessage()
                );

                continue;
            }

            foreach ($products['bookedProducts'] as $productData) {
                // Basisentgeld (bruttobasiert)
                $basisentgeldBrutto = $productData['form']['produkt_leistung_grundgebuehr_bruttobasiert']->getValue();
                $basisentgeldNetto = $productData['form']['produkt_leistung_grundgebuehr_nettobasiert']->getValue();

                // Einrichtungsgebühr (bruttobasiert)
                $einrichtungBrutto = $productData['form']['produkt_leistung_einrichtung_bruttobasiert']->getValue();
                $einrichtungNetto = $productData['form']['produkt_leistung_einrichtung_nettobasiert']->getValue();

                $productId = $productData['productId'];
                $productName = $productData['name'];

                $buchungsDatum = sprintf('%s-%s-%s',
                    $productData['form']['produkt_leistung_tag']->getValue(),
                    $productData['form']['produkt_leistung_monat']->getValue(),
                    $productData['form']['produkt_leistung_jahr']->getValue()
                );

                $laufzeit = $productData['form']['produkt_leistung_laufzeit']->getValue();

                $deaktivierung = '';

                if (isset($productData['form']['produkt_deaktivieren'])) {
                    $deaktivierung = $productData['form']['produkt_deaktivieren']->getValue();
                    $deaktivierung = $deactivierungsvalueToString[$deaktivierung];
                }

                $aktiv = $productData['form']['produkt_leistung_aktiv']->getValue();

                $berechnen = '';
                
                if (isset($productData['form']['produkt_anteilig_berechnen'])) {
                    $berechnen = $productData['form']['produkt_anteilig_berechnen']->getValue();
                }

                $isGutschrift = false;

                if ($basisentgeldBrutto < 0 || $einrichtungBrutto < 0) {
                    $isGutschrift = true;
                }

                echo sprintf('"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s"',
                    $aktiv,
                    $clientId,
                    $purtelAccount,
                    $buchungsDatum,
                    $laufzeit,
                    $deaktivierung,
                    $berechnen,
                    $productId,
                    $productName,
                    $basisentgeldBrutto,
                    $basisentgeldNetto,
                    $einrichtungBrutto,
                    $einrichtungNetto,
                    $isGutschrift ? '1' : '0'
                )."\n";
            }
        }

        echo "completely done\n";
    }
}

?>