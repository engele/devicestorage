<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Goutte\Client;

/**
 * 
 */
class PurtelFixSapBuchungskontenBeiBuchungen extends ContainerAwareCommand
{
    protected $purtelMainProducts = [ // price = brutto
        '4578' => ['name' => 'Leistungsfrei', 'price' => '0', 'sap' => null],
        '4582' => ['name' => 'Fon / 24 Monate', 'price' => '1495', 'sap' => '412080'],
        '4794' => ['name' => 'Web 100 / 24 Monate', 'price' => '2495', 'sap' => '412080'],
        '4796' => ['name' => 'HD-TV & Fon / 24 Monate', 'price' => '2495', 'sap' => '412080'],
        '4798' => ['name' => 'Komfort 100 / 24 Monate', 'price' => '2995', 'sap' => '412080'],
        '4842' => ['name' => 'HD-TV / 24 Monate', 'price' => '995', 'sap' => '412080'],
        '4870' => ['name' => 'HD-TV & Web 100 / 24 Monate', 'price' => '3495', 'sap' => '412080'],
        '4876' => ['name' => 'Premium 100 / 24 Monate', 'price' => '3995', 'sap' => '412080'],
        '4979' => ['name' => 'Fon', 'price' => '1495', 'sap' => '412080'],
        '4981' => ['name' => 'HD-TV & Web 100', 'price' => '3495', 'sap' => '412080'],
        '4983' => ['name' => 'Web 100', 'price' => '2495', 'sap' => '412080'],
        '4985' => ['name' => 'Komfort 100', 'price' => '2995', 'sap' => '412080'],
        '4987' => ['name' => 'Premium 100', 'price' => '3995', 'sap' => '412080'],
        '4989' => ['name' => 'HD-TV & Fon', 'price' => '2495', 'sap' => '412080'],
        '4991' => ['name' => 'HD-TV', 'price' => '995', 'sap' => '412080'],
        '5130' => ['name' => 'Business I3 / 36 Monate', 'price' => '5343', 'sap' => '412230'],
        '5132' => ['name' => 'Business S1-AA/2', 'price' => '2368', 'sap' => '412220'],
        '5142' => ['name' => 'Glasfaser Premium 100', 'price' => '4990', 'sap' => '412080'],
        '5144' => ['name' => 'Glasfaser Komfort 100', 'price' => '3990', 'sap' => '412080'],
        '5146' => ['name' => 'Glasfaser Web', 'price' => '3490', 'sap' => '412080'],
        '5150' => ['name' => 'Glasfaser Fon', 'price' => '2190', 'sap' => '412080'],
        '5152' => ['name' => 'Glasfaser HD-TV', 'price' => '1390', 'sap' => '412080'],
        '5154' => ['name' => 'Glasfaser COM-FIX GeWo ', 'price' => '0', 'sap' => '412080'],
        '5156' => ['name' => 'Glasfaser Premium 50', 'price' => '3990', 'sap' => '412080'],
        '5158' => ['name' => 'Glasfaser Premium 100 / 24 Monate', 'price' => '4990', 'sap' => '412080'],
        '5160' => ['name' => 'Glasfaser Premium 50 / 24 Monate', 'price' => '3990', 'sap' => '412080'],
        '5162' => ['name' => 'Glasfaser Komfort 100 / 24 Monate', 'price' => '3990', 'sap' => '412080'],
        '5164' => ['name' => 'Glasfaser Komfort 50', 'price' => '2990', 'sap' => '412080'],
        '5166' => ['name' => 'Glasfaser Komfort 50 / 24 Monate', 'price' => '2990', 'sap' => '412080'],
        '5168' => ['name' => 'Glasfaser Web 100', 'price' => '2990', 'sap' => '412080'],
        '5170' => ['name' => 'Glasfaser Web 100 / 24 Monate', 'price' => '2990', 'sap' => '412080'],
        '5172' => ['name' => 'Glasfaser Web 50', 'price' => '2489', 'sap' => '412080'],
        '5174' => ['name' => 'Glasfaser Web 50 / 24 Monate', 'price' => '2489', 'sap' => '412080'],
        '5258' => ['name' => 'Glasfaser Fon / 24 Monate', 'price' => '1990', 'sap' => '412080'],
        '5260' => ['name' => 'Glasfaser Fon und HD-TV', 'price' => '2990', 'sap' => '412080'],
        '5262' => ['name' => 'Glasfaser Fon und HD-TV / 24 Monate', 'price' => '2990', 'sap' => '412080'],
        '5264' => ['name' => 'Glasfaser HD-TV / 24 Monate', 'price' => '1390', 'sap' => '412080'],
        '5266' => ['name' => 'Glasfaser Web 50 und HD-TV / 24 Monate', 'price' => '3490', 'sap' => '412080'],
        '5268' => ['name' => 'Glasfaser Web 25', 'price' => '1990', 'sap' => '412080'],
        '5270' => ['name' => 'Glasfaser Web 25 / 24 Monate', 'price' => '1990', 'sap' => '412080'],
        '5272' => ['name' => 'Glasfaser Komfort 25 / 24 Monate', 'price' => '2489', 'sap' => '412080'],
        '5274' => ['name' => 'Glasfaser Premium 25 / 24 Monate', 'price' => '3490', 'sap' => '412080'],
        '5360' => ['name' => 'Business S1-MGA/2', 'price' => '2368', 'sap' => '412220'],
        '5362' => ['name' => 'Business S2-AA/5', 'price' => '4748', 'sap' => '412220'],
        '5364' => ['name' => 'Business S3-AA/10', 'price' => '11888', 'sap' => '412220'],
        '5366' => ['name' => 'Business S4 AA/30', 'price' => '11888', 'sap' => '412220'],
        '5444' => ['name' => 'Premium 100 / 24 Monate + LP1', 'price' => '3995', 'sap' => '412080'],
        '5494' => ['name' => 'Glasfaser Komfort 50 + LP 1', 'price' => '2990', 'sap' => '412080'],
        '5496' => ['name' => 'Glasfaser COM-FIX GeWo + LP 1', 'price' => '0', 'sap' => '412080'],
        '5498' => ['name' => 'Glasfaser COM-FIX GeWo + LP 1 & LP 2', 'price' => '0', 'sap' => '412080'],
        '5500' => ['name' => 'Glasfaser COM-FIX GeWo + LP 2', 'price' => '0', 'sap' => '412080'],
        '5502' => ['name' => 'Glasfaser Fon und HD-TV + LP 1', 'price' => '2990', 'sap' => '412080'],
        '5504' => ['name' => 'Glasfaser Fon + LP 1', 'price' => '2190', 'sap' => '412080'],
        '5506' => ['name' => 'Glasfaser Komfort 100 + LP 1', 'price' => '3990', 'sap' => '412080'],
        '5508' => ['name' => 'Glasfaser Komfort 100 + LP 2', 'price' => '3990', 'sap' => '412080'],
        '5510' => ['name' => 'Glasfaser Komfort 25 + LP 1', 'price' => '2489', 'sap' => '412080'],
        '5512' => ['name' => 'Glasfaser Komfort 50 + LP 2', 'price' => '2990', 'sap' => '412080'],
        '5514' => ['name' => 'Glasfaser Premium 100 + LP 1', 'price' => '4990', 'sap' => '412080'],
        '5516' => ['name' => 'Glasfaser Premium 50 + LP 1', 'price' => '3990', 'sap' => '412080'],
        '5518' => ['name' => 'Glasfaser Premium 50 + LP 2', 'price' => '3990', 'sap' => '412080'],
        '5520' => ['name' => 'Premium 100 + LP1', 'price' => '3990', 'sap' => '412080'],
        '5524' => ['name' => 'Komfort 100 + LP 1', 'price' => '2990', 'sap' => '412080'],
        '5526' => ['name' => 'Komfort 100 + LP 2', 'price' => '2990', 'sap' => '412080'],
        '5528' => ['name' => 'Komfort 100 / 24 Monate + LP 1', 'price' => '2990', 'sap' => '412080'],
        '5530' => ['name' => 'Komfort 100 / 24 Monate + LP 2', 'price' => '2990', 'sap' => '412080'],
        '5664' => ['name' => 'Business I2 / 36 Monate', 'price' => '4153', 'sap' => '412230'],
        '5666' => ['name' => 'Business I1 / 36 Monate', 'price' => '2963', 'sap' => '412230'],
        '5532' => ['name' => 'Test-voip-dabei', 'price' => '0', 'sap' => '412080'],
        '5838' => ['name' => 'Willkommen - Komfort 100 / 24 Monate', 'price' => '1495', 'sap' => '412080'],
        '5840' => ['name' => 'Willkommen - Web 100 / 24 Monate', 'price' => '1495', 'sap' => '412080'],
        '5852' => ['name' => 'KabelKiosk Basis HD', 'price' => '490', 'sap' => '412080'],
        '5858' => ['name' => 'KabelKiosk Family HD inkl. Basis HD', 'price' => '1990', 'sap' => '412080'],
    ];

    protected $purtelCrossSellingProducts = [
        '7572' => ['name' => '2-4. Telefonleitung', 'price' => null, 'sap' => '412080'],
        '7524' => ['name' => '2. Telefonleitung', 'price' => null, 'sap' => '412080'],
        '7588' => ['name' => '2. Telefonleitung inkl.', 'price' => null, 'sap' => '412080'],
        '7574' => ['name' => 'Anschlusssperrung', 'price' => null, 'sap' => '412260'],
        '7568' => ['name' => 'AVM FritzBox 7360 Kauf', 'price' => null, 'sap' => '412260'],
        '7534' => ['name' => 'AVM FritzBox 7360 Miete', 'price' => null, 'sap' => '412080'],
        '7570' => ['name' => 'AVM FritzBox 7360 Miete / 24 Monate', 'price' => null, 'sap' => '412080'],
        '7560' => ['name' => 'AVM FritzBox 7390 Kauf', 'price' => null, 'sap' => '412260'],
        '7562' => ['name' => 'AVM FritzBox 7390 Miete', 'price' => null, 'sap' => '412080'],
        '7616' => ['name' => 'AVM FritzBox 7390 Miete / 24 Monate', 'price' => null, 'sap' => '412080'],
        '7532' => ['name' => 'AVM FritzBox 7490 Kauf', 'price' => null, 'sap' => '412260'],
        '7618' => ['name' => 'AVM FritzBox 7490 Miete', 'price' => null, 'sap' => '412080'],
        '7540' => ['name' => 'AVM FritzBox 7490 Miete / 24 Monate', 'price' => null, 'sap' => '412080'],
        '7628' => ['name' => 'AVM FritzBox 7560 Kauf', 'price' => null, 'sap' => '412260'],
        '7630' => ['name' => 'AVM FritzBox 7560 Miete', 'price' => null, 'sap' => '412080'],
        '7632' => ['name' => 'AVM FritzBox 7560 Miete / 24 Monate', 'price' => null, 'sap' => '412080'],
        '7712' => ['name' => 'Bearbeitungsgebühr ', 'price' => null, 'sap' => '412260'],
        '7578' => ['name' => 'Berechnung nicht zurückgesendeter Hardware 1.Jahr: AVM Fritz!Box Fon Wlan 7360', 'price' => null, 'sap' => '412260'],
        '7582' => ['name' => 'Berechnung nicht zurückgesendeter Hardware 1.Jahr: AVM Fritz!Box Fon Wlan 7390 ', 'price' => null, 'sap' => '412260'],
        '7620' => ['name' => 'Berechnung nicht zurückgesendeter Hardware 1.Jahr: AVM Fritz!Box Fon Wlan 7490', 'price' => null, 'sap' => '412260'],
        '7580' => ['name' => 'Berechnung nicht zurückgesendeter Hardware 2.Jahr: AVM Fritz!Box Fon Wlan 7360', 'price' => null, 'sap' => '412260'],
        '7576' => ['name' => 'Berechnung nicht zurückgesendeter Hardware 2.Jahr: AVM Fritz!Box Fon Wlan 7390', 'price' => null, 'sap' => '412260'],
        '7622' => ['name' => 'Berechnung nicht zurückgesendeter Hardware 2.Jahr: AVM Fritz!Box Fon Wlan 7490', 'price' => null, 'sap' => '412260'],
        '7780' => ['name' => 'Business AE Installationsarbeiten - Arbeiten nach Aufwand (a 10 Min.)', 'price' => null, 'sap' => '412230'],
        '7764' => ['name' => 'Business CLIP Übermittlung der Rufnummer des Anrufers ', 'price' => null, 'sap' => '412080'],
        '7796' => ['name' => 'Business Deutschland-Festnetz-Flatrate S-1', 'price' => null, 'sap' => '412220'],
        '7798' => ['name' => 'Business Deutschland-Festnetz-Flatrate S-2', 'price' => null, 'sap' => '412220'],
        '7800' => ['name' => 'Business Deutschland-Festnetz-Flatrate S-3', 'price' => null, 'sap' => '412220'],
        '7802' => ['name' => 'Business Deutschland-Festnetz-Flatrate S-4', 'price' => null, 'sap' => '412220'],
        '7716' => ['name' => 'Business Einrichtungspreis bei 12 Monaten Lfz.', 'price' => null, 'sap' => '412230'],
        '7718' => ['name' => 'Business Einrichtungspreis bei 24 Monaten Lfz.', 'price' => null, 'sap' => '412230'],
        '7720' => ['name' => 'Business Einrichtungspreis bei 36 Monaten Lfz.', 'price' => null, 'sap' => '412230'],
        '7756' => ['name' => 'Business Feste IP-Adresse', 'price' => null, 'sap' => '412230'],
        '7744' => ['name' => 'Business Grundgebühr Zusatz Internet-1, Internet-2, Internet-3 ', 'price' => null, 'sap' => '412230'],
        '7790' => ['name' => 'Business Internet-1 25/2,5 Mbit/s ', 'price' => null, 'sap' => '412230'],
        '7792' => ['name' => 'Business Internet-2 50/5 Mbit/s ', 'price' => null, 'sap' => '412230'],
        '7794' => ['name' => 'Business Internet-3 100/10 Mbit/s', 'price' => null, 'sap' => '412230'],
        '7758' => ['name' => 'Business Kundeneigene Hardware', 'price' => null, 'sap' => '412260'],
        '7748' => ['name' => 'Business Länderpaket Option 1', 'price' => null, 'sap' => '412220'],
        '7750' => ['name' => 'Business Länderpaket Option 2', 'price' => null, 'sap' => '412220'],
        '7746' => ['name' => 'Business National Mobilfunk TOP', 'price' => null, 'sap' => '412220'],
        '7782' => ['name' => 'Business National Mobilfunknetz S-1', 'price' => null, 'sap' => '412220'],
        '7784' => ['name' => 'Business National Mobilfunknetz S-2', 'price' => null, 'sap' => '412220'],
        '7786' => ['name' => 'Business National Mobilfunknetz S-3', 'price' => null, 'sap' => '412220'],
        '7788' => ['name' => 'Business National Mobilfunknetz S-4', 'price' => null, 'sap' => '412220'],
        '7754' => ['name' => 'Business Nichtteilnahme am (Sepa-) Lastschriftverfahren', 'price' => null, 'sap' => '412230'],
        '7734' => ['name' => 'Business Papierrechnung Geschäftskunde', 'price' => null, 'sap' => '412230'],
        '7714' => ['name' => 'Business S-1 CPE Miete', 'price' => null, 'sap' => '412220'],
        '7736' => ['name' => 'Business S-1 Fritzbox 7490 Kauf', 'price' => null, 'sap' => '412260'],
        '7738' => ['name' => 'Business S-1 Fritzbox 7490 Miete', 'price' => null, 'sap' => '412230'],
        '7722' => ['name' => 'Business S-2 CPE Miete', 'price' => null, 'sap' => '412220'],
        '7724' => ['name' => 'Business S-3 CPE Kauf', 'price' => null, 'sap' => '412260'],
        '7726' => ['name' => 'Business S-3 CPE Miete', 'price' => null, 'sap' => '412220'],
        '7728' => ['name' => 'Business S-4 CPE Kauf', 'price' => null, 'sap' => '412260'],
        '7730' => ['name' => 'Business S-4 CPE Miete', 'price' => null, 'sap' => '412220'],
        '7760' => ['name' => 'Business TV-Anschluss', 'price' => null, 'sap' => '412230'],
        '7752' => ['name' => 'Business Umzugspauschale', 'price' => null, 'sap' => '412230'],
        '7732' => ['name' => 'Business Vertragswechsel', 'price' => null, 'sap' => '412230'],
        '7694' => ['name' => 'Einrichtungsgebühr 1', 'price' => null, 'sap' => '412260'],
        '7690' => ['name' => 'Einrichtungsgebühr 1 / 24 Monate  ', 'price' => null, 'sap' => '412260'],
        '7696' => ['name' => 'Einrichtungsgebühr 2', 'price' => null, 'sap' => '412260'],
        '7330' => ['name' => 'Einrichtungsgebühr 2 / 24 Monate', 'price' => null, 'sap' => '412260'],
        '7698' => ['name' => 'Einrichtungsgebühr 3', 'price' => null, 'sap' => '412260'],
        '7692' => ['name' => 'Einrichtungsgebühr 3 / 24 Monate  ', 'price' => null, 'sap' => '412260'],
        '7566' => ['name' => 'Erstattung Anschlusspreis', 'price' => null, 'sap' => '412260'],
        '7586' => ['name' => 'Fehlerbeseitigung bei ungerechtfertigter Störung', 'price' => null, 'sap' => '412260'],
        '7626' => ['name' => 'Finderlohn', 'price' => null, 'sap' => '552020'],
        '7536' => ['name' => 'FTTB-Switch Kauf', 'price' => null, 'sap' => '412260'],
        '7608' => ['name' => 'FTTB-Switch zur Miete', 'price' => null, 'sap' => '412080'],
        '8016' => ['name' => 'Glasfaser HD-TV Kombi', 'price' => null, 'sap' => '412080'],
        '7340' => ['name' => 'Glasfasergrundgebühr', 'price' => null, 'sap' => '412080'],
        '7762' => ['name' => 'GTN Medienconverter', 'price' => null, 'sap' => '412260'],
        '8018' => ['name' => 'Gutschrift Rechnungskorrektur', 'price' => null, 'sap' => '412080'],
        '7704' => ['name' => 'Gutschrift Sondervereinbarung Vertrieb', 'price' => null, 'sap' => '552020'],
        '7778' => ['name' => 'Gutschrift Sondervereinbarung Vertrieb einmalig', 'price' => null, 'sap' => '552020'],
        '7700' => ['name' => 'Gutschrift SWI Aktion 24 Monate', 'price' => null, 'sap' => '552020'],
        '7590' => ['name' => 'Gutschrift Teilbereitstellung', 'price' => null, 'sap' => '412080'],
        '7706' => ['name' => 'Installations- und Servicearbeiten', 'price' => null, 'sap' => '412260'],
        '7610' => ['name' => 'KabelKiosk Aktivierungsgebühr inkl. Smart Card', 'price' => null, 'sap' => '412260'],
        '7594' => ['name' => 'KabelKiosk Basis HD', 'price' => null, 'sap' => '412080'],
        '7708' => ['name' => 'KabelKiosk Basis HD inkl.', 'price' => null, 'sap' => '412080'],
        '7612' => ['name' => 'KabelKiosk Conax Neotion CI+ Modul', 'price' => null, 'sap' => '412260'],
        '7598' => ['name' => 'KabelKiosk Duo Paket', 'price' => null, 'sap' => '412080'],
        '7538' => ['name' => 'KabelKiosk Family HD inkl. Basis HD', 'price' => null, 'sap' => '412080'],
        '7710' => ['name' => 'KabelKiosk Family HD inkl. Basis HD bei Glasfaser Premium', 'price' => null, 'sap' => '412080'],
        '7600' => ['name' => 'KabelKiosk International 1', 'price' => null, 'sap' => '412080'],
        '7602' => ['name' => 'KabelKiosk International 2', 'price' => null, 'sap' => '412080'],
        '7604' => ['name' => 'KabelKiosk International 3', 'price' => null, 'sap' => '412080'],
        '7606' => ['name' => 'KabelKiosk International 4', 'price' => null, 'sap' => '412080'],
        '7596' => ['name' => 'KabelKiosk Single Paket', 'price' => null, 'sap' => '412080'],
        '7564' => ['name' => 'Kunde ist im Besitz eigener Hardware', 'price' => null, 'sap' => '412260'],
        '7548' => ['name' => 'Kunde wirbt Kunde', 'price' => null, 'sap' => '552020'],
        '7526' => ['name' => 'Länderpaket Option 1', 'price' => null, 'sap' => '412080'],
        '7528' => ['name' => 'Länderpaket Option 2', 'price' => null, 'sap' => '412080'],
        '7776' => ['name' => 'Materialkosten', 'price' => null, 'sap' => '412260'],
        '7636' => ['name' => 'nicht zurückgesendete Smartcard', 'price' => null, 'sap' => '412260'],
        '7556' => ['name' => 'Nichtteilnahme am (Sepa-) Lastschriftverfahren', 'price' => null, 'sap' => '412080'],
        '7530' => ['name' => 'Papierrechnung', 'price' => null, 'sap' => '412080'],
        '7550' => ['name' => 'Rabatt auf Grundgebühr', 'price' => null, 'sap' => '412260'],
        '7634' => ['name' => 'Receiver Humax Fox C inkl. Festplatte extern 1 TB', 'price' => null, 'sap' => '412080'],
        '7688' => ['name' => 'Receiver Technisat ISIO inkl. Festplatte extern 1 TB', 'price' => null, 'sap' => '412080'],
        '7584' => ['name' => 'Rufnummernmitnahme', 'price' => null, 'sap' => '412260'],
        '7552' => ['name' => 'Startguthaben 100 Euro', 'price' => null, 'sap' => '552020'],
        '7278' => ['name' => 'Startguthaben 200 Euro', 'price' => null, 'sap' => '552020'],
        '7558' => ['name' => 'Umzugspauschale', 'price' => null, 'sap' => '412260'],
        '7614' => ['name' => 'Upgrade Bandbreite 100 ', 'price' => null, 'sap' => '412080'],
        '8092' => ['name' => 'Upgrade Bandbreite 50', 'price' => null, 'sap' => '412080'],
        '7638' => ['name' => 'Zusätzliche Technikeranfahrt', 'price' => null, 'sap' => '412260'],
        '7530' => ['name' => 'Papierrechung', 'price' => null, 'sap' => '412080'],
    ];

    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('PurtelFixSapBuchungskontenBeiBuchungen')
            ->setDescription('')
        ;
    }

    /**
     * Get buchungen for all accounts by kundennummer_extern
     * 
     * @return array|null
     */
    protected function getBuchungen($clientId = null)
    {
        $timeZone = new \DateTimeZone('Europe/Berlin');
        $today = new \DateTime('now', $timeZone);

        $parameter = [
            'von_datum' => '20100101',
            'bis_datum' => $today->format('Ymd'),
            'erweitert' => '6',
        ];

        if (null !== $clientId) {
            $parameter['kundennummer_extern'] = $clientId;
        }

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'getbuchungen'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * 
     */
    public function findProductIdByBuchung($buchung)
    {
        $textToProductName = [
            '/^{productName} Endabrechnung wegen Tarifwechsel\s*$/',
            '/^Basisentgelt {productName}\s*$/',
            '/^Einmalige Bereitstellung {productName}\s*$/',
            '/^Basisentgelt {productName} anteilig f..?r \d+ Tage?$/',
            '/^{productName}\s*$/',
            '/^{productName}$/',
            '/^\d x {productName}$/',
            '/^{productName} anteilig f..?r \d+ Tage?$/',
            '/^{productName} Gutschrift Restlaufzeit wegen Tarifwechsel$/',
            '/^\d x {productName} anteilig f..?r \d+ Tage?$/',
            '/^{productName} anteilige Gutschrift f..?r \d+ Tage?$/',
            '/^{productName} vom [\d\.-]+$/',
        ];

        foreach ($this->purtelMainProducts as $productId => $data) {
            foreach ($textToProductName as $pattern) {
                $productName = str_replace(
                    [
                        '+',
                        '/',
                        'ü',
                        'ä',
                        'ö',
                        'ß',
                    ],
                    [
                        '\+',
                        '\/',
                        '.',
                        '.',
                        '.',
                        '.',
                    ],
                    $data['name']
                );

                $pattern = str_replace('{productName}', $productName, $pattern);

                if (1 === preg_match($pattern, utf8_decode($buchung[3]))) {
                    return $productId;
                }
            }
        }

        foreach ($this->purtelCrossSellingProducts as $productId => $data) {
            foreach ($textToProductName as $pattern) {
                $productName = str_replace(
                    [
                        '+',
                        '/',
                        'ü',
                        'ä',
                        'ö',
                        'ß',
                    ],
                    [
                        '\+',
                        '\/',
                        '.',
                        '.',
                        '.',
                        '.',
                    ],
                    $data['name']
                );

                $pattern = str_replace('{productName}', $productName, $pattern);

                if (1 === preg_match($pattern, utf8_decode($buchung[3]))) {
                    return $productId;

                    break 2;
                }
            }
        }

        return null;
    }

    /**
     * 
     * 
     * @return array|null
     */
    protected function getAccounts($clientId = null)
    {
        $parameter = [
            'erweitert' => 2,
        ];

        if (null !== $clientId) {
            $parameter['kundennummer_extern'] = $clientId;
        }

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'getaccounts'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * 
     */
    protected function getStammdatenForm($purtelAccount)
    {
        $client = new Client();

        $crawler = $client->request('GET', 'https://www2.purtel.com/res/'.$this->purtelReseller.'/index.php?username='
            .$this->purtelSuperUser.'&passwort='
            .$this->purtelSuperUserPassword.'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1&admin_ns_suchbegriff='
            .$purtelAccount.'&tab_id=9&marke=Stammdaten');

        $form = $crawler->selectButton('Ändern')->form();

        return $form;
    }

    /**
     * 
     */
    protected function isTestaccount($form)
    {
        return !empty($form['testanschluss_neu']->getValue());
    }

    /**
     * update buchung at purtel
     * 
     * @return boolean
     */
    function updateBuchung($buchung)
    {
        $updatePostData = [
            'anschluss' => $buchung[2],
            'verwendungszweck' => $buchung[3],
            'aendern' => 1,
            'id' => $buchung[0],
            //'datum' => $buchung[7],
            'sap_buchungskonto' => $buchung[23],
        ];

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'setbuchung'
        );
        
        $url .= '&'.utf8_decode(http_build_query($updatePostData));

        echo "transmitted data to update buchung:\n\t".http_build_query($updatePostData)."\n";

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));

        $result = curl_exec($curl);

        curl_close($curl);

        $result = trim($result);
        $result = explode(';', $result);

        if ('+OK' === $result[0]) {
            return true;
        }

        var_dump('Failed to update buchung', $result);

        return false;
    }

    /**
     * 
     */
    function deleteBuchung($buchung)
    {
        $updatePostData = [
            'anschluss' => $buchung[2],
            'verwendungszweck' => $buchung[3],
            'loeschen' => 1,
            'id' => $buchung[0],
        ];

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'setbuchung'
        );
        
        $url .= '&'.utf8_decode(http_build_query($updatePostData));

        echo "transmitted data to update buchung:\n\t".http_build_query($updatePostData)."\n";

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));

        $result = curl_exec($curl);

        curl_close($curl);

        $result = trim($result);
        $result = explode(';', $result);

        if ('+OK' === $result[0]) {
            return true;
        }

        var_dump('Failed to update buchung', $result);

        return false;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/tools/import/CsvReader.php';
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';

        $this->purtelSuperUser = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
        $this->purtelSuperUserPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
        $this->purtelReseller = 'res100260';

        $allAccounts = $this->getAccounts();
        $allAccountsHeadline = array_shift($allAccounts);
        $allAccounts = array_column($allAccounts, 4, 1);

        $buchungen = $this->getBuchungen();
        $buchungenHeadline = array_shift($buchungen);

        foreach ($buchungen as $key => $buchung) {
            if (!empty($buchung[6])) {
                continue;
            }

            if ('verrechnung' === $buchung[12]) {
                continue;
            }
 
            $clientId = $buchung[9];
            $purtelAccount = $buchung[2];
            $verwendungszweck = $buchung[3];
            $sapBuchungskonto = $buchung[23];

            echo "starting - ".$purtelAccount." - ".$verwendungszweck."\n";

            $toDelete = [
                '/^Portierung \d+ Rufnummern? \(\d+\)$/',
                '/^Neuschaltung \d+ Rufnummern? \(\d+\)$/',
                '/^Neuschaltung \d+ Rufnummern? \(\d+\) je 0,00 Euro$/',
            ];

            foreach ($toDelete as $pattern) {
                if (1 === preg_match($pattern, $verwendungszweck)) {
                    if (!$this->deleteBuchung($buchung)) {
                        echo "buchung wurde nicht geloescht\n";
                    } else {
                        echo "buchung geloescht\n";
                    }

                    continue 2;
                }
            }


            $productId = $this->findProductIdByBuchung($buchung);

            if (null === $productId) {
                if (false !== strpos($verwendungszweck, 'Gutschrift einmalig')) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }
                if (false !== strpos($verwendungszweck, 'Kulanzgutschrift Telefoniest')) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }
                if (false !== strpos($verwendungszweck, 'Gutschrift Sondervereinbarung')) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }
                /*if (false !== strpos($verwendungszweck, 'Gutschrift Teilbereitstellung - ')) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }*/
                if (false !== strpos($verwendungszweck, 'Kunde wirbt Kunde Stefan Prechtl')) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }
                if (false !== strpos($verwendungszweck, 'Glasfaser Komfort')) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }
                if (false !== strpos($verwendungszweck, 'Kulanzgutschrift Ausfall Telefonie/Intenret')) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }
                if (false !== strpos($verwendungszweck, 'Gutschrift Restlaufzeit wegen Tarifwechsel')) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }
                if (false !== strpos($verwendungszweck, 'Basisentgelt ')) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }
                if (false !== strpos($verwendungszweck, 'Basisentgelt ')) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }
                if (false !== strpos($verwendungszweck, 'Gutschrift Telefoniest')) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }
                if (false !== strpos($verwendungszweck, 'Fehleranalyse bei ungerechtfertigter St')) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }
                if (false !== strpos($verwendungszweck, 'Materialkosten')) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }
                if (false !== strpos($verwendungszweck, 'bis Schaltung TV')) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }
                if (false !== strpos($verwendungszweck, 'Mindestvertragslaufzeit 1 Monat')) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }
                if (false !== strpos($verwendungszweck, '1 TV Dose erneuert')) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }
                if (false !== strpos($verwendungszweck, 'TV-Dose neu verschalten')) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }
                if ('691946' === (string) $purtelAccount) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }
                if (false !== strpos($verwendungszweck, 'Web 100 anteilig ab 19.04.2018')) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }
                if ('Glasfaser Premium' === $verwendungszweck) {
                    echo sprintf("unable to identify product - (%s) current sap %s\n", $verwendungszweck, $sapBuchungskonto);
                    continue;
                }

                if (0 === strpos($verwendungszweck, 'Gutschrift Teilbereitstellung')) {
                    $productId = '7590';
                } else if (0 === strpos($verwendungszweck, 'Papierrechnung')) {
                    $productId = '7530';
                } else {
                    echo sprintf("product not found - (%s) sap:%s - %s\n",
                        $verwendungszweck,
                        $sapBuchungskonto,
                        $purtelAccount
                    );

                    continue;
                }
            }

            $product = null;

            if (isset($this->purtelMainProducts[$productId])) {
                $product = $this->purtelMainProducts[$productId];
            } elseif (isset($this->purtelCrossSellingProducts[$productId])) {
                $product = $this->purtelCrossSellingProducts[$productId];
            }
/*
if ('verrechnung' == $buchung[12]) continue;
//if ($key > 4) die(var_dump($buchung, (string) $sapBuchungskonto, (string) $product['sap']));
if (!isset($allAccounts[$purtelAccount])) {
    echo "account - ".$purtelAccount." existiert nicht\n";
    continue;
}

$stammdatenForm = $this->getStammdatenForm($purtelAccount);

if ($this->isTestaccount($stammdatenForm)) {
    echo "account - ".$purtelAccount." ist testanschluss\n";
    continue;
}

$abrechnenUeber = $stammdatenForm['konto_von_neu']->getValue();

if (!empty($abrechnenUeber)) {
    echo "buchung-id: ".$buchung[0]." - account: ".$purtelAccount." - datum: ".$buchung[7]." - verwendungszweck: ".$verwendungszweck." - hat abrechnen ueber\n";
    continue;

    if (!isset($allAccounts[$abrechnenUeber])) {
        echo "abrechnen ueber account - ".$abrechnenUeber." existiert nicht\n";
        continue;
    }
    $abrechnenUeberStammdatenForm = $this->getStammdatenForm($abrechnenUeber);
    if ($this->isTestaccount($abrechnenUeberStammdatenForm)) {
        echo "abrechnen ueber account - ".$abrechnenUeber." ist testanschluss\n";
        continue;
    }
    $abrechnenUeberAbrechnenUeber = $abrechnenUeberStammdatenForm['konto_von_neu']->getValue();
    if (!empty($abrechnenUeberAbrechnenUeber)) {
        echo "abrechnen ueber account ".$abrechnenUeber." hat auch noch mal abrechnen ueber - ".$abrechnenUeberAbrechnenUeber."\n";
        continue;
    }
}

echo "buchung-id: ".$buchung[0]." - account: ".$purtelAccount." - datum: ".$buchung[7]." - verwendungszweck: ".$verwendungszweck." - alles ok\n";
continue;
*/


            if ((string) $sapBuchungskonto !== (string) $product['sap']) {
                echo "update\n";

                echo sprintf("anderes konto - von %s nach %s\n", $sapBuchungskonto, $product['sap']);

                //die(var_dump( $buchung ));
                $buchung[23] = $product['sap'];

                if (!$this->updateBuchung($buchung)) {
                    var_dump('failed', $buchung);
                } else {
                    "update ok\n";
                }

                //die(var_dump( $buchung ));
            } else {
                echo " - nothing to do\n";
            }
        }

        echo "all done\n";
    }
}

?>