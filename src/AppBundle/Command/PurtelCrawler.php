<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Goutte\Client;

/**
 * 
 */
class PurtelCrawler extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('PurtelCrawler')
            ->setDescription('')
            /*->addArgument(
                'which',
                InputArgument::REQUIRED,
                'example|example|...'
            )*/
        ;
    }

    protected function addProduct()
    {
        try {
            $mainPurtelAccount = '647545';
            $res = 'res100260';
            $purtelUser = '673846';
            $purtelPassword = '8446a25fefd7';
            // 678028

            $client = new Client();

            $crawler = $client->request('GET', 'https://www2.purtel.com/res/'.$res.'/index.php?username='
                .$purtelUser.'&passwort='
                .$purtelPassword.'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1&admin_ns_suchbegriff='
                .$mainPurtelAccount.'&tab_id=3&marke=Zusatzleistungen');

            $form = $crawler->selectButton('Wechseln')->form();
            $form['produkt_wechseln']->select(4582);

            $cw = $client->submit($form);

            var_dump($cw->html());
        } catch (\Exception $e) {
            echo sprintf("Message: %s\nFile: %s\nLine: %s\n-------------------------\n",
                $e->getMessage(),
                $e->getFile(),
                $e->getLine()
            );

            foreach ($e->getTrace() as $xx => $e2) {
                echo sprintf("File: %s\nLine: %s\nFunction: %s\n----\n",
                    $e2['file'],
                    $e2['line'],
                    $e2['function']
                );
            }
            
            die();
        }
        
    }

    protected function addCrossselling()
    {
        try {
            $mainPurtelAccount = '647545';
            $res = 'res100260';
            $purtelUser = '673846';
            $purtelPassword = '8446a25fefd7';
            // 678028
            $date = new \DateTime('2018-01-03');

            $client = new Client();

            $crawler = $client->request('GET', 'https://www2.purtel.com/res/'.$res.'/index.php?username='
                .$purtelUser.'&passwort='
                .$purtelPassword.'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1&admin_ns_suchbegriff='
                .$mainPurtelAccount.'&tab_id=3&marke=Zusatzleistungen');

            $form = $crawler->selectButton('Hinzufügen')->form();
            $form['show_produkt_nebenstelle']->select(7568);
            $form['produkt_menge']->setValue(1);
            $form['produkt_anteilig_berechnen']->setValue(1);
            
            if ($date instanceOf \DateTime) {
                $form['produkt_tag']->setValue($date->format('d'));
                $form['produkt_monat']->setValue($date->format('m'));
                $form['produkt_jahr']->setValue($date->format('Y'));
            }

            $cw = $client->submit($form);

            var_dump($cw->html());
        } catch (\Exception $e) {
            echo sprintf("Message: %s\nFile: %s\nLine: %s\n-------------------------\n",
                $e->getMessage(),
                $e->getFile(),
                $e->getLine()
            );

            foreach ($e->getTrace() as $xx => $e2) {
                echo sprintf("File: %s\nLine: %s\nFunction: %s\n----\n",
                    $e2['file'],
                    $e2['line'],
                    $e2['function']
                );
            }
            
            die();
        }
        
    }

    protected function changeStatus($status)
    {
        try {
            $mainPurtelAccount = '647545';
            $purtelProductId = '4582';
            $date = new \DateTime('2018-01-01');

            $res = 'res100260';
            $purtelUser = '673846';
            $purtelPassword = '8446a25fefd7';

            $client = new Client();

            $crawler = $client->request('GET', 'https://www2.purtel.com/res/'.$res.'/index.php?username='
                .$purtelUser.'&passwort='
                .$purtelPassword.'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1&admin_ns_suchbegriff='
                .$mainPurtelAccount.'&tab_id=3&marke=Zusatzleistungen');

            //$crawler->filter('form[action="index.php#crossselling_nebenstelle"] input[name="admin_crossselling_nebenstelle_editieren"]')->each(function ($node) use($client) {
            $crawler->selectButton('Ändern')->each(function ($node) use($client, $purtelProductId, $status, $date) {
                if (false === strpos($node->parents()->eq(2)->html(), '] Produkt ['.$purtelProductId.'] ')) {
                    return;
                }

                $form = $node->form();

                unset($form['admin_crossselling_nebenstelle_loeschen']);

                if ($status) {
                    $form['produkt_leistung_aktiv']->tick();

                    if ($date instanceOf \DateTime) {
                        $form['produkt_leistung_tag']->setValue($date->format('d'));
                        $form['produkt_leistung_monat']->setValue($date->format('m'));
                        $form['produkt_leistung_jahr']->setValue($date->format('Y'));
                    }
                } else {
                    $form['produkt_leistung_aktiv']->untick();
                }

                $cw = $client->submit($form);

                var_dump($cw->html());
            });
        } catch (\Exception $e) {
            echo sprintf("Message: %s\nFile: %s\nLine: %s\n-------------------------\n",
                $e->getMessage(),
                $e->getFile(),
                $e->getLine()
            );

            foreach ($e->getTrace() as $xx => $e2) {
                echo sprintf("File: %s\nLine: %s\nFunction: %s\n----\n",
                    $e2['file'],
                    $e2['line'],
                    $e2['function']
                );
            }
            
            die();
        }
    }

    /**
     * 
     */
    protected function getCrossellings()
    {
        $oneTimeProducts = [
            '10016',
            '10024',
            '10026',
            '10037',
            '10040',
            '10044',
            '10045',
            '10046',
            '10047',
            '10048',
            '10054',
            '10059',
            '10060',
            '10061',
            '10077',
            '10078',
            '10083',
            '10180',
            '10182',
            '10201',
            '10202',
            '10204',
            '10208',
            '10210',
            '10212',
            '10213',
            '10214',
            '10226',
            '10230',
            '10266',
            '10268',
            '10270',
            '10272',
            '10275',
            '10276',
            '10295',
            '10300',
            '10313',
            '10314',
            '10315',
            '10316',
            '10317',
            '10318',
            '10328',
            '10343',
            '10041',
            '10076',
            '10051',
            '10298',
            '10209',
            '10205',
        ];

        $ignoredProducts = [
            '10006',
            '1002',
            '10257',
            '10311',
            '10038',
            '1001',
            '10228',
            '10187',
            '10211',
            '10206',
            '10207',
            '10062',
            '10053',
            '10203',
            '10183',
            '10186',
            '10260',
            '10191',
            '10199',
            '10193',
            '10305',
            '10346',
            '10127',
            '10261',
            '10304',
            '10312',
            '10194',
            '10004',
            '10229',
            '10042',
            '10034',
            '10192',
            '10058',
            '10283',
        ];

        $danielSpecial = [
            '14240' => 'sven 01.04.2018',
            '14380' => 'sven 01.04.2018',
            '14683' => 'sven 01.04.2018',
            '14847' => 'sven 01.04.2018',
            '14866' => 'sven 01.04.2018',
            '15418' => 'sven 01.04.2018, wichtig ohne GS Teilbereitstellung',
            '15431' => 'sven 01.04.2018',
            '15496' => 'sven 01.04.2018',
            '15527' => 'sven 01.04.2018',
            '15866' => 'sven 01.04.2018',
            '16271' => 'sven 01.04.2018',
            '16408' => 'sven 01.04.2018, wichtig ohne Aktion Testwochen',
            '16514' => 'sven 01.04.2018',
            '16729' => 'sven 01.04.2018, wichtig ohne Aktion Testwochen',
            '16760' => 'sven 01.04.2018',
            '16972' => 'sven 01.04.2018',
            '17074' => 'sven 01.04.2018',
            '17108' => 'sven 01.04.2018, wichtig ohne Aktion WoWi',
            '17142' => 'sven 01.04.2018',
            '17143' => 'sven 01.04.2018',
            '17371' => 'sven 01.04.2018',
            '17899' => 'sven 01.04.2018, wichtig ohne GS Teilbereitstellung',
            '18562' => 'sven 01.04.2018',
            '18640' => 'sven 01.04.2018',
            '18710' => '"im purtel korrigiert und aktiviert zum 01.04.2018 mit Crossselling',
            '18859' => 'sven 01.04.2018',
            '19066' => '"im purtel korrigiert und aktiviert zum 01.04.2018 mit Crossselling',
            '19122' => 'sven 01.04.2018',
            '19164' => 'sven 01.04.2018',
            '19210' => 'sven 01.04.2018',
            '19259' => 'sven 01.04.2018',
            '19317' => 'sven 01.04.2018',
            '19333' => 'sven 01.04.2018',
            '19540' => 'sven 01.04.2018',
            '19732' => '"im purtel korrigiert und aktiviert zum 01.04.2018 mit Crossselling',
            '20027' => '"im purtel korrigiert und aktiviert zum 01.04.2018 mit Crossselling',
            '20028' => '"im purtel korrigiert und aktiviert zum 01.04.2018 mit Crossselling',
            '20129' => 'sven 01.04.2018',
            '20207' => '"im purtel korrigiert und aktiviert zum 01.04.2018 mit Crossselling',
            '20371' => '"im purtel korrigiert und aktiviert zum 01.04.2018 mit Crossselling',
        ];

        $skipCustomer = [
            '5508',
        ];


        $csvColumns = [
            'kundennummer' => '100260',
            'purtelAccount' => null,
            'datum' => null,
            'produkt_id' => null,
            'einrichtung' => null,
            'grundgebuehr' => null,
            'laufzeit' => null,
            'aktiv' => 1,
            'menge' => 1,
            'anteilig' => 0,
            'verrechnen' => 0,
            'deaktivieren' => 0,
            'einrichtung_nettobasiert' => 0,
            'einrichtung_bruttobasiert' => 0,
            'basisentgelt_nettobasiert' => null,
            'basisentgelt_bruttobasiert' => null,
            'Produktname' => null,
            'Dimari-Kundennummer' => null,
            'ikv-Kundennummer' => null,
            'Cluster' => null,
        ];

        $db = $this->getContainer()->get('doctrine')->getManager()->getConnection();

        $csvOut = [];

        $customerProducts = $db->query("SELECT cp.*, c.`customer_id` as dimariId, c.`clientid` as ikvId, l.`name` as cluster 
            FROM `customer_products` cp 
            LEFT JOIN `customers` c ON c.`id` = cp.`customer_id` 
            LEFT JOIN `locations` l ON c.`location_id` = l.`id`
            WHERE c.`location_id` = 2
        ");
        // location_id = 2 - Cluster 28

        while ($customerProduct = $customerProducts->fetch(\PDO::FETCH_ASSOC)) {
            if ('1' === $customerProduct['propperly_copyed_to_customer']) {
                // main products are already done -> skip
                continue;
            }

            if (in_array($customerProduct['customer_id'], $skipCustomer)) {
                /*trigger_error(sprintf('skipped - customer (%s)',
                    $customerProduct['customer_id']
                ), E_USER_NOTICE);*/

                continue;
            }

            $productQ = $db->query("SELECT * FROM `products` WHERE `id` = ".$customerProduct['product_id']);

            if (1 !== $productQ->rowCount()) {
                die("invalid amount of products (".$productQ->rowCount().") - product (".$customerProduct['product_id'].") - ".$customerProduct['customer_id']);
            }

            $product = $productQ->fetch(\PDO::FETCH_ASSOC);

            if (in_array($product['identifier'], $ignoredProducts)) {
                /*trigger_error(sprintf('skipped - customers (%s) product (%s) is ignored',
                    $customerProduct['customer_id'],
                    $product['identifier']
                ), E_USER_NOTICE);*/

                continue;
            }

            if (in_array($product['identifier'], $oneTimeProducts)) {
                /*trigger_error(sprintf('skipped - customers (%s) product (%s) is one time product',
                    $customerProduct['customer_id'],
                    $product['identifier']
                ), E_USER_NOTICE);*/

                continue;
            }

            if (isset($danielSpecial[$customerProduct['dimariId']])) {
                switch ($danielSpecial[$customerProduct['dimariId']]) {
                    case '"im purtel korrigiert und aktiviert zum 01.04.2018 mit Crossselling':
                        /*trigger_error(sprintf('skipped - customers (%s) product (%s) was ignored by danielSpecial (already done)',
                            $customerProduct['customer_id'],
                            $product['identifier']
                        ), E_USER_NOTICE);*/

                        continue 2;

                    case 'sven 01.04.2018, wichtig ohne GS Teilbereitstellung':
                        if ('10057' === $product['identifier']) {
                            /*trigger_error(sprintf('skipped - customers (%s) product (%s) was ignored by danielSpecial (GS Teilbereitstellung)',
                                $customerProduct['customer_id'],
                                $product['identifier']
                            ), E_USER_NOTICE);*/

                            continue 2;
                        }

                        break;
                        
                    //case 'sven 01.04.2018, wichtig ohne Aktion Testwochen': // 10207 - already in ignoredProducts
                    //case 'sven 01.04.2018, wichtig ohne Aktion WoWi': // 10305 - already in ignoredProducts
                    
                    case 'sven 01.04.2018':
                        $customerProduct['activation_date'] = '2018-04-01';
                        break;
                }
            }

            if (empty($product['purtel_product_id'])) {
                echo "no purtel-product-id for product (".$customerProduct['product_id'].") - ".$customerProduct['customer_id']."\n";
                //die();
                continue; // can not use without purtel-product-id
            }

            $purtelMasterQuery = $db->query("SELECT * FROM `purtel_account` WHERE `master` = 1 AND `cust_id` = ".$customerProduct['customer_id']);

            if (1 !== $purtelMasterQuery->rowCount()) {
                echo 'invalid amount of purtel master accounts ('.$purtelMasterQuery->rowCount().') - '.$customerProduct['customer_id']."\n";
                //die();
                continue;
            }

            $purtelMasterAccount = $purtelMasterQuery->fetch(\PDO::FETCH_ASSOC);

            if (empty($purtelMasterAccount['purtel_login'])) {
                /*trigger_error(sprintf('skipped - customers (%s) purtel-account has no purtel_login (may be was status "offen")',
                    $customerProduct['customer_id']
                ), E_USER_NOTICE);*/

                continue;
            }

            //$date = null;
            $laufzeitMonate = 0;

            if (empty($customerProduct['activation_date'])) {
                // activation_date is required
                echo "no activation date - ".$customerProduct['customer_id']."\n";
                continue;
            }

            $activationDate = new \DateTime($customerProduct['activation_date']);

            if (!empty($customerProduct['deactivation_date'])) {
                $deactivationDate = new \DateTime($customerProduct['deactivation_date']);

                if ($deactivationDate <= new \DateTime()) {
                    /*trigger_error(sprintf('skipped - customers (%s) date of deactivation is in past (%s)',
                        $customerProduct['customer_id'],
                        $customerProduct['deactivation_date']
                    ), E_USER_NOTICE);*/

                    continue; // skip because already outdated
                }

                $laufzeit = $activationDate->diff($deactivationDate);
                $laufzeitMonate = ($laufzeit->y * 12) + $laufzeit->m;

                if ($laufzeit->d > 0) {
                    $laufzeitMonate++;
                }
            }

            //$date = str_replace('-', '', $customerProduct['activation_date']);

            $priceNet = null;
            $priceGross = null;

            if (empty($customerProduct['price_net'])) {
                if (empty($product['default_price_gross'])) {
                    if (empty($product['default_price_net'])) {
                        $priceNet = 0;
                        $priceGross = 0;
                    } else {
                        $priceNet = (float) $product['default_price_net'];
                        $priceNet *= 100;
                        $priceNet = round($priceNet, 0);
                    }
                } else {
                    $priceGross = (float) $product['default_price_gross'];
                    $priceGross *= 100;
                    $priceGross = round($priceGross, 0);
                }
            } else {
                $priceNet = (float) $customerProduct['price_net'];
                $priceNet *= 100;
                $priceNet = round($priceNet, 0);
            }

            // existiert überhaupt das passende cross-selling-produkt in der ikv?
            $optionQ = $db->query("SELECT * FROM `optionen` WHERE `option` = 'Dimari-".$product['purtel_product_id']."'");

            if ($optionQ->rowCount() !== 1) {
                //echo "missing option - Dimari-".$product['purtel_product_id']."\n";
                //var_dump($product, $customerProduct['customer_id']);
                echo "X-".$customerProduct['id']."-".$customerProduct['customer_id'].";".$product['identifier'].";\"".$product['name']."\";".$product['purtel_product_id']."\n";
                continue;
            }

            $csvOut[] = array_merge($csvColumns, [
                'purtelAccount' => $purtelMasterAccount['purtel_login'],
                //'datum' => $date,
                //'datum' => $customerProduct['activation_date'],$activationDate
                'datum' => $activationDate,
                'produkt_id' => $product['purtel_product_id'],
                'basisentgelt_nettobasiert' => $priceNet,
                'basisentgelt_bruttobasiert' => $priceGross,
                'laufzeit' => $laufzeitMonate,
                'deaktivieren' => $laufzeitMonate > 0 ? 1 : 0,
                'Produktname' => $product['name'],
                'Dimari-Kundennummer' => $customerProduct['dimariId'],
                'ikv-Kundennummer' => $customerProduct['ikvId'],
                'cId' => $customerProduct['customer_id'],
                'Cluster' => $customerProduct['cluster'],
            ]);

            //echo "set propperly_copyed_to_customer, move to customer_options - customer_products_id = ".$customerProduct['id']."\n";
        }

        return $csvOut;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //die($this->addCrossselling());
        //die($this->changeStatus(1));
        //die($this->addProduct());

        $productsFromPurtel = [
            '4578' => ['name' => 'Leistungsfrei', 'price' => '0'],
            '4582' => ['name' => 'Fon / 24 Monate', 'price' => '1495'],
            '4794' => ['name' => 'Web 100 / 24 Monate', 'price' => '2495'],
            '4796' => ['name' => 'HD-TV & Fon / 24 Monate', 'price' => '2495'],
            '4798' => ['name' => 'Komfort 100 / 24 Monate', 'price' => '2995'],
            '4842' => ['name' => 'HD-TV / 24 Monate', 'price' => '995'],
            '4870' => ['name' => 'HD-TV & Web 100 / 24 Monate', 'price' => '3495'],
            '4876' => ['name' => 'Premium 100 / 24 Monate', 'price' => '3995'],
            '4979' => ['name' => 'Fon', 'price' => '1495'],
            '4981' => ['name' => 'HD-TV & Web 100', 'price' => '3495'],
            '4983' => ['name' => 'Web 100', 'price' => '2495'],
            '4985' => ['name' => 'Komfort 100', 'price' => '2995'],
            '4987' => ['name' => 'Premium 100', 'price' => '3995'],
            '4989' => ['name' => 'HD-TV & Fon', 'price' => '2495'],
            '4991' => ['name' => 'HD-TV', 'price' => '995'],
            '5130' => ['name' => 'Business I3 / 36 Monate', 'price' => '5343'],
            '5132' => ['name' => 'Business S1-AA/2', 'price' => '2368'],
            '5142' => ['name' => 'Glasfaser Premium 100', 'price' => '4990'],
            '5144' => ['name' => 'Glasfaser Komfort 100', 'price' => '3990'],
            '5146' => ['name' => 'Glasfaser Web', 'price' => '3490'],
            '5150' => ['name' => 'Glasfaser Fon', 'price' => '2190'],
            '5152' => ['name' => 'Glasfaser HD-TV', 'price' => '1390'],
            '5154' => ['name' => 'Glasfaser COM-FIX GeWo ', 'price' => '0'],
            '5156' => ['name' => 'Glasfaser Premium 50', 'price' => '3990'],
            '5158' => ['name' => 'Glasfaser Premium 100 / 24 Monate', 'price' => '4990'],
            '5160' => ['name' => 'Glasfaser Premium 50 / 24 Monate', 'price' => '3990'],
            '5162' => ['name' => 'Glasfaser Komfort 100 / 24 Monate', 'price' => '3990'],
            '5164' => ['name' => 'Glasfaser Komfort 50', 'price' => '2990'],
            '5166' => ['name' => 'Glasfaser Komfort 50 / 24 Monate', 'price' => '2990'],
            '5168' => ['name' => 'Glasfaser Web 100', 'price' => '2990'],
            '5170' => ['name' => 'Glasfaser Web 100 / 24 Monate', 'price' => '2990'],
            '5172' => ['name' => 'Glasfaser Web 50', 'price' => '2489'],
            '5174' => ['name' => 'Glasfaser Web 50 / 24 Monate', 'price' => '2489'],
            '5258' => ['name' => 'Glasfaser Fon / 24 Monate', 'price' => '1990'],
            '5260' => ['name' => 'Glasfaser Fon und HD-TV', 'price' => '2990'],
            '5262' => ['name' => 'Glasfaser Fon und HD-TV / 24 Monate', 'price' => '2990'],
            '5264' => ['name' => 'Glasfaser HD-TV / 24 Monate', 'price' => '1390'],
            '5266' => ['name' => 'Glasfaser Web 50 und HD-TV / 24 Monate', 'price' => '3490'],
            '5268' => ['name' => 'Glasfaser Web 25', 'price' => '1990'],
            '5270' => ['name' => 'Glasfaser Web 25 / 24 Monate', 'price' => '1990'],
            '5272' => ['name' => 'Glasfaser Komfort 25 / 24 Monate', 'price' => '2489'],
            '5274' => ['name' => 'Glasfaser Premium 25 / 24 Monate', 'price' => '3490'],
            '5360' => ['name' => 'Business S1-MGA/2', 'price' => '2368'],
            '5362' => ['name' => 'Business S2-AA/5 ', 'price' => '4748'],
            '5364' => ['name' => 'Business S3-AA/10 ', 'price' => '11888'],
            '5366' => ['name' => 'Business S4 AA/30 ', 'price' => '11888'],
            '5444' => ['name' => 'Premium 100 / 24 Monate + LP1', 'price' => '3995'],
            '5494' => ['name' => 'Glasfaser Komfort 50 + LP 1', 'price' => '2990'],
            '5496' => ['name' => 'Glasfaser COM-FIX GeWo + LP 1', 'price' => '0'],
            '5498' => ['name' => 'Glasfaser COM-FIX GeWo + LP 1 & LP 2', 'price' => '0'],
            '5500' => ['name' => 'Glasfaser COM-FIX GeWo + LP 2', 'price' => '0'],
            '5502' => ['name' => 'Glasfaser Fon und HD-TV + LP 1', 'price' => '2990'],
            '5504' => ['name' => 'Glasfaser Fon + LP 1', 'price' => '2190'],
            '5506' => ['name' => 'Glasfaser Komfort 100 + LP 1', 'price' => '3990'],
            '5508' => ['name' => 'Glasfaser Komfort 100 + LP 2', 'price' => '3990'],
            '5510' => ['name' => 'Glasfaser Komfort 25 + LP 1', 'price' => '2489'],
            '5512' => ['name' => 'Glasfaser Komfort 50 + LP 2', 'price' => '2990'],
            '5514' => ['name' => 'Glasfaser Premium 100 + LP 1', 'price' => '4990'],
            '5516' => ['name' => 'Glasfaser Premium 50 + LP 1', 'price' => '3990'],
            '5518' => ['name' => 'Glasfaser Premium 50 + LP 2', 'price' => '3990'],
            '5520' => ['name' => 'Premium 100 + LP1', 'price' => '3990'],
            '5524' => ['name' => 'Komfort 100 + LP 1', 'price' => '2990'],
            '5526' => ['name' => 'Komfort 100 + LP 2', 'price' => '2990'],
            '5528' => ['name' => 'Komfort 100 / 24 Monate + LP 1', 'price' => '2990'],
            '5530' => ['name' => 'Komfort 100 / 24 Monate + LP 2', 'price' => '2990'],
            '5664' => ['name' => 'Business I2 / 36 Monate', 'price' => '4153'],
            '5666' => ['name' => 'Business I1 / 36 Monate', 'price' => '2963'],
            // crosssellings
            '7572' => ['name' => '2-4. Telefonleitung', 'price' => null],
            '7524' => ['name' => '2. Telefonleitung', 'price' => null],
            '7588' => ['name' => '2. Telefonleitung inkl.', 'price' => null],
            '7574' => ['name' => 'Anschlusssperrung', 'price' => null],
            '7568' => ['name' => 'AVM FritzBox 7360 Kauf', 'price' => null],
            '7534' => ['name' => 'AVM FritzBox 7360 Miete', 'price' => null],
            '7570' => ['name' => 'AVM FritzBox 7360 Miete / 24 Monate', 'price' => null],
            '7560' => ['name' => 'AVM FritzBox 7390 Kauf', 'price' => null],
            '7562' => ['name' => 'AVM FritzBox 7390 Miete', 'price' => null],
            '7616' => ['name' => 'AVM FritzBox 7390 Miete / 24 Monate', 'price' => null],
            '7532' => ['name' => 'AVM FritzBox 7490 Kauf', 'price' => null],
            '7618' => ['name' => 'AVM FritzBox 7490 Miete', 'price' => null],
            '7540' => ['name' => 'AVM FritzBox 7490 Miete / 24 Monate', 'price' => null],
            '7628' => ['name' => 'AVM FritzBox 7560 Kauf', 'price' => null],
            '7630' => ['name' => 'AVM FritzBox 7560 Miete', 'price' => null],
            '7632' => ['name' => 'AVM FritzBox 7560 Miete / 24 Monate', 'price' => null],
            '7712' => ['name' => 'Bearbeitungsgebühr ', 'price' => null],
            '7578' => ['name' => 'Berechnung nicht zurückgesendeter Hardware 1.Jahr: AVM Fritz!Box Fon Wlan 7360', 'price' => null],
            '7582' => ['name' => 'Berechnung nicht zurückgesendeter Hardware 1.Jahr: AVM Fritz!Box Fon Wlan 7390 ', 'price' => null],
            '7620' => ['name' => 'Berechnung nicht zurückgesendeter Hardware 1.Jahr: AVM Fritz!Box Fon Wlan 7490', 'price' => null],
            '7580' => ['name' => 'Berechnung nicht zurückgesendeter Hardware 2.Jahr: AVM Fritz!Box Fon Wlan 7360', 'price' => null],
            '7576' => ['name' => 'Berechnung nicht zurückgesendeter Hardware 2.Jahr: AVM Fritz!Box Fon Wlan 7390', 'price' => null],
            '7622' => ['name' => 'Berechnung nicht zurückgesendeter Hardware 2.Jahr: AVM Fritz!Box Fon Wlan 7490', 'price' => null],
            '7780' => ['name' => 'Business AE Installationsarbeiten - Arbeiten nach Aufwand (a 10 Min.)', 'price' => null],
            '7764' => ['name' => 'Business CLIP Übermittlung der Rufnummer des Anrufers ', 'price' => null],
            '7796' => ['name' => 'Business Deutschland-Festnetz-Flatrate S-1', 'price' => null],
            '7798' => ['name' => 'Business Deutschland-Festnetz-Flatrate S-2', 'price' => null],
            '7800' => ['name' => 'Business Deutschland-Festnetz-Flatrate S-3', 'price' => null],
            '7802' => ['name' => 'Business Deutschland-Festnetz-Flatrate S-4', 'price' => null],
            '7716' => ['name' => 'Business Einrichtungspreis bei 12 Monaten Lfz.', 'price' => null],
            '7718' => ['name' => 'Business Einrichtungspreis bei 24 Monaten Lfz.', 'price' => null],
            '7720' => ['name' => 'Business Einrichtungspreis bei 36 Monaten Lfz.', 'price' => null],
            '7756' => ['name' => 'Business Feste IP-Adresse', 'price' => null],
            '7744' => ['name' => 'Business Grundgebühr Zusatz Internet-1, Internet-2, Internet-3 ', 'price' => null],
            '7790' => ['name' => 'Business Internet-1 25/2,5 Mbit/s ', 'price' => null],
            '7792' => ['name' => 'Business Internet-2 50/5 Mbit/s ', 'price' => null],
            '7794' => ['name' => 'Business Internet-3 100/10 Mbit/s', 'price' => null],
            '7758' => ['name' => 'Business Kundeneigene Hardware', 'price' => null],
            '7748' => ['name' => 'Business Länderpaket Option 1', 'price' => null],
            '7750' => ['name' => 'Business Länderpaket Option 2', 'price' => null],
            '7746' => ['name' => 'Business National Mobilfunk TOP', 'price' => null],
            '7782' => ['name' => 'Business National Mobilfunknetz S-1', 'price' => null],
            '7784' => ['name' => 'Business National Mobilfunknetz S-2', 'price' => null],
            '7786' => ['name' => 'Business National Mobilfunknetz S-3', 'price' => null],
            '7788' => ['name' => 'Business National Mobilfunknetz S-4', 'price' => null],
            '7754' => ['name' => 'Business Nichtteilnahme am (Sepa-) Lastschriftverfahren', 'price' => null],
            '7734' => ['name' => 'Business Papierrechnung Geschäftskunde', 'price' => null],
            '7714' => ['name' => 'Business S-1 CPE Miete', 'price' => null],
            '7736' => ['name' => 'Business S-1 Fritzbox 7490 Kauf', 'price' => null],
            '7738' => ['name' => 'Business S-1 Fritzbox 7490 Miete', 'price' => null],
            '7722' => ['name' => 'Business S-2 CPE Miete', 'price' => null],
            '7724' => ['name' => 'Business S-3 CPE Kauf', 'price' => null],
            '7726' => ['name' => 'Business S-3 CPE Miete', 'price' => null],
            '7728' => ['name' => 'Business S-4 CPE Kauf', 'price' => null],
            '7730' => ['name' => 'Business S-4 CPE Miete', 'price' => null],
            '7760' => ['name' => 'Business TV-Anschluss', 'price' => null],
            '7752' => ['name' => 'Business Umzugspauschale', 'price' => null],
            '7732' => ['name' => 'Business Vertragswechsel', 'price' => null],
            '7694' => ['name' => 'Einrichtungsgebühr 1', 'price' => null],
            '7690' => ['name' => 'Einrichtungsgebühr 1 / 24 Monate  ', 'price' => null],
            '7696' => ['name' => 'Einrichtungsgebühr 2', 'price' => null],
            '7330' => ['name' => 'Einrichtungsgebühr 2 / 24 Monate', 'price' => null],
            '7698' => ['name' => 'Einrichtungsgebühr 3', 'price' => null],
            '7692' => ['name' => 'Einrichtungsgebühr 3 / 24 Monate  ', 'price' => null],
            '7566' => ['name' => 'Erstattung Anschlusspreis', 'price' => null],
            '7586' => ['name' => 'Fehlerbeseitigung bei ungerechtfertigter Störung', 'price' => null],
            '7626' => ['name' => 'Finderlohn', 'price' => null],
            '7536' => ['name' => 'FTTB-Switch Kauf', 'price' => null],
            '7608' => ['name' => 'FTTB-Switch zur Miete', 'price' => null],
            '8016' => ['name' => 'Glasfaser HD-TV Kombi', 'price' => null],
            '7340' => ['name' => 'Glasfasergrundgebühr', 'price' => null],
            '7762' => ['name' => 'GTN Medienconverter ', 'price' => null],
            '8018' => ['name' => 'Gutschrift Rechnungskorrektur', 'price' => null],
            '7704' => ['name' => 'Gutschrift Sondervereinbarung Vertrieb', 'price' => null],
            '7778' => ['name' => 'Gutschrift Sondervereinbarung Vertrieb einmalig', 'price' => null],
            '7700' => ['name' => 'Gutschrift SWI Aktion 24 Monate', 'price' => null],
            '7590' => ['name' => 'Gutschrift Teilbereitstellung', 'price' => null],
            '7706' => ['name' => 'Installations- und Servicearbeiten', 'price' => null],
            '7610' => ['name' => 'KabelKiosk Aktivierungsgebühr inkl. Smart Card', 'price' => null],
            '7594' => ['name' => 'KabelKiosk Basis HD', 'price' => null],
            '7708' => ['name' => 'KabelKiosk Basis HD inkl.', 'price' => null],
            '7612' => ['name' => 'KabelKiosk Conax Neotion CI+ Modul', 'price' => null],
            '7598' => ['name' => 'KabelKiosk Duo Paket', 'price' => null],
            '7538' => ['name' => 'KabelKiosk Family HD inkl. Basis HD', 'price' => null],
            '7710' => ['name' => 'KabelKiosk Family HD inkl. Basis HD bei Glasfaser Premium', 'price' => null],
            '7600' => ['name' => 'KabelKiosk International 1', 'price' => null],
            '7602' => ['name' => 'KabelKiosk International 2', 'price' => null],
            '7604' => ['name' => 'KabelKiosk International 3', 'price' => null],
            '7606' => ['name' => 'KabelKiosk International 4', 'price' => null],
            '7596' => ['name' => 'KabelKiosk Single Paket', 'price' => null],
            '7564' => ['name' => 'Kunde ist im Besitz eigener Hardware', 'price' => null],
            '7548' => ['name' => 'Kunde wirbt Kunde', 'price' => null],
            '7526' => ['name' => 'Länderpaket Option 1', 'price' => null],
            '7528' => ['name' => 'Länderpaket Option 2', 'price' => null],
            '7776' => ['name' => 'Materialkosten', 'price' => null],
            '7636' => ['name' => 'nicht zurückgesendete Smartcard', 'price' => null],
            '7556' => ['name' => 'Nichtteilnahme am (Sepa-) Lastschriftverfahren', 'price' => null],
            '7530' => ['name' => 'Papierrechung', 'price' => null],
            '7550' => ['name' => 'Rabatt auf Grundgebühr', 'price' => null],
            '7634' => ['name' => 'Receiver Humax Fox C inkl. Festplatte extern 1 TB', 'price' => null],
            '7688' => ['name' => 'Receiver Technisat ISIO inkl. Festplatte extern 1 TB', 'price' => null],
            '7584' => ['name' => 'Rufnummernmitnahme', 'price' => null],
            '7552' => ['name' => 'Startguthaben 100 Euro', 'price' => null],
            '7278' => ['name' => 'Startguthaben 200 Euro', 'price' => null],
            '7558' => ['name' => 'Umzugspauschale', 'price' => null],
            '7614' => ['name' => 'Upgrade Bandbreite 100 ', 'price' => null],
            '8092' => ['name' => 'Upgrade Bandbreite 50', 'price' => null],
            '7638' => ['name' => 'Zusätzliche Technikeranfahrt', 'price' => null],
        ];



        $db = $this->getContainer()->get('doctrine')->getManager()->getConnection();

        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';

        $purtelSuperUser = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
        $purtelSuperUserPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
        $purtelReseller = 'res100260';


        // gruppiere crossselling-produkte nach purtelAccount
        $groupByPurtelAccount = [];

        foreach ($this->getCrossellings() as $crossSellingProduct) {
            if (!isset($groupByPurtelAccount[$crossSellingProduct['purtelAccount']])) {
                $groupByPurtelAccount[$crossSellingProduct['purtelAccount']] = [];
            }

            $groupByPurtelAccount[$crossSellingProduct['purtelAccount']][] = $crossSellingProduct;
        }

        
        foreach ($groupByPurtelAccount as $purtelAccount => $crossSellingProducts) {
            $ikvKundennummer = $crossSellingProducts[0]['ikv-Kundennummer'];

            // hole alle "gebuchten" Produkte des Kunden
            $bookedProducts = [];
            
            $client = new Client();
            
            $crawler = $client->request('GET', 'https://www2.purtel.com/res/'.$purtelReseller.'/index.php?username='
                .$purtelSuperUser.'&passwort='
                .$purtelSuperUserPassword.'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1&admin_ns_suchbegriff='
                .$purtelAccount.'&tab_id=3&marke=Zusatzleistungen');

            $crawler->selectButton('Ändern')->each(function ($node) use (&$bookedProducts) {
                $element = $node->parents()->eq(2);

                $matches = null;

                preg_match('/^\[ID (\d+)\] (Produkt|Crossselling) \[(\d+)\] (.*)$/', $element->filter('tr > td')->first()->text(), $matches);

                $form = $node->form();

                $bookedProduct = [
                    'id' => $matches[1],
                    'productId' => $matches[3],
                    'name' => $matches[4],
                    'form' => $form,
                ];

                $bookedProducts[$bookedProduct['productId']] = $bookedProduct;
            });

            // hole hauptprodukt
            $customersCurrentIkvProductQ = $db->query("SELECT c.`purtel_product`, p.`name`, p.`purtel_product_id` 
                FROM `customers` c 
                LEFT JOIN `products` p ON p.`identifier` = REPLACE(c.`purtel_product`, 'Dimari-', '') 
                WHERE c.`id` = ".$crossSellingProducts[0]['cId']);

            if (1 !== $customersCurrentIkvProductQ->rowCount()) {
                // komischerweise gibt es diesen Kunden mehr oder weniger als ein mal
                die(var_dump("invalid amount of customers", $customerId, $customersCurrentIkvProductQ));
            }

            $customersCurrentIkvProduct = $customersCurrentIkvProductQ->fetch(\PDO::FETCH_ASSOC);
            $customersCurrentIkvProduct_purtel = null;


            
            echo "PurtelAccount: ".$purtelAccount."\n";


            // finde das gebuchte Haupt-Product, falls existent
            foreach ($bookedProducts as $key => $bookedProduct) {
                // dieses gebuchte Produkt ist das Haupt-Produkt
                if ($bookedProduct['productId'] === $customersCurrentIkvProduct['purtel_product_id']) {
                    // datensatz des gebuchten produktes (vom haupt-produkt)
                    $customersCurrentIkvProduct_purtel = $bookedProduct;

                    $mainProductFromCustomerProducts = $db->query("SELECT cp.*, p.`name`, p.`purtel_product_id` 
                        FROM `customer_products` cp 
                        LEFT JOIN `products` p ON p.`id` = cp.`product_id` 
                        WHERE cp.`propperly_copyed_to_customer` = 1 
                        AND cp.`customer_id` = ".$crossSellingProducts[0]['cId']);

                    if (1 !== $mainProductFromCustomerProducts->rowCount()) {
                        // komischerweise gibt es diesen Kunden mehr oder weniger als ein mal
                        die(var_dump("mainProductFromCustomerProducts invalid amount", $mainProductFromCustomerProducts));
                    }

                    $mainProductFromCustomerProducts = $mainProductFromCustomerProducts->fetch(\PDO::FETCH_ASSOC);

                    // hänge den Datensazt für das Haupt-Produkt an die CrossSellingProducts an.
                    // Dann wird das auch in der späteren Schleife verabeitet
                    $crossSellingProducts[] = [
                        'produkt_id' => $customersCurrentIkvProduct['purtel_product_id'],
                        'mainProduct' => true,
                        'datum' => new \DateTime($mainProductFromCustomerProducts['activation_date']),
                    ];

                    break;
                }
            }

            // für alle produkte werden auch buchungen benötigt
            $requiredBuchungen = $crossSellingProducts;


            // 
            $todo = [];

            // vergleiche die Produkte, die gebucht sein müssen, mit denen, die tatsächlich momentan gebucht sind
            foreach ($crossSellingProducts as $key => $crossSellingProduct) {
                echo $crossSellingProduct['produkt_id']."\n";
                
                // produkt noch nicht gebucht, wird dann weiter unten erledigt
                if (!isset($bookedProducts[$crossSellingProduct['produkt_id']])) {
                    echo "nicht gebucht - verarbeitung folgt unten\n";
                    continue;
                }

                $bookedProduct = $bookedProducts[$crossSellingProduct['produkt_id']];

                // wirf es aus der liste, so dass unten nur noch die übrig bleiben, die noch angelegt werden müssen und die, 
                // die der Kunden eigentlich gar nicht haben sollte
                unset($crossSellingProducts[$key], $bookedProducts[$crossSellingProduct['produkt_id']]);

                if ("1" !== $bookedProduct['form']['produkt_leistung_aktiv']->getValue()) {
                    echo "product nicht aktiv\n";

                    // aktiviere und datum setzten (buchung?)
                    //$todo[] = ...;
                    $todo[] = [
                        'todo' => 'aktiviere und datum setzten (buchung?)',
                        'crossSellingProduct' => $crossSellingProduct,
                        'bookedProduct' => $bookedProduct,
                    ];

                    continue;
                }

                // datum usw..

                if ($bookedProduct['form']['produkt_leistung_tag']->getValue() !== $crossSellingProduct['datum']->format('d')
                    || $bookedProduct['form']['produkt_leistung_monat']->getValue() !== $crossSellingProduct['datum']->format('m')
                    || $bookedProduct['form']['produkt_leistung_jahr']->getValue() !== $crossSellingProduct['datum']->format('Y')) {

                    echo "not same date\n";

                    // richtiges datum setzten
                    //$todo[] = ...;
                    $todo[] = [
                        'todo' => 'richtiges datum setzten (buchung?)',
                        'crossSellingProduct' => $crossSellingProduct,
                        'bookedProduct' => $bookedProduct,
                    ];

                    continue;
                }

                // preise vergleichen/korrigieren ?
                // ggf. $todo[] = ...;
                //--------------------------------------------------------
            }

            // diese produkte sind auch gebucht, obwohl der kunde sie nicht haben sollte
            foreach ($bookedProducts as $key => $bookedProduct) {
                if ("1" == $bookedProduct['form']['produkt_leistung_aktiv']->getValue()) {
                    echo "Produkt sollte deaktiviert werden (todo Daniel):\n";
                    var_dump($bookedProduct['productId'], $bookedProduct['name']);

                    // da etwas aktiv ist, dass da nicht sein sollte, nichts machen. Comin (Daniel) will die dann manuell machen.
                    continue 2;
                } // else .. wenn inaktiv, dann egal
            }


            $buchungen = $this->getBuchungen($ikvKundennummer);

            if (is_array($buchungen)) {
                unset($buchungen[0]);

                foreach ($requiredBuchungen as $requiredBuchungenKey => $crossSellingProduct) {
                    $purtelProduct = $productsFromPurtel[$crossSellingProduct['produkt_id']];
                    $productNameAtPurtel = $purtelProduct['name'];

                    $foundBuchung = null;

                    foreach ($buchungen as $key => $buchung) {
                        if ('0' !== $buchung[6]) {
                            unset($buchungen[$key]);

                            continue;
                        }

                        if ('Basisentgelt '.$productNameAtPurtel === $buchung[3]) {
                            $foundBuchung = $key;

                            break;
                        }

                        if ($productNameAtPurtel === $buchung[3]) {
                            $foundBuchung = $key;

                            break;
                        }

                        if (1 === preg_match('/^'.$productNameAtPurtel.' anteilig f.r \d+ Tage$/', $buchung[3])) {
                            $foundBuchung = $key;

                            break;
                        }
                    }

                    if (null === $foundBuchung) {
                        continue;
                    }

                    $buchung = $buchungen[$foundBuchung];

                    unset($buchungen[$foundBuchung], $requiredBuchungen[$requiredBuchungenKey]);
                    
                    // buchung prüfen, ggf korrigieren
                    // $todo[] = ...;
                    //-----------------------------------------------------------
                }

                // diese Buchungen existieren, obwohl sie der Kunde nicht haben sollte
                foreach ($buchungen as $key => $buchung) {
                    echo "Diese Buchungen sollten gelöscht werden (todo Daniel):\n";
                    var_dump($buchung);

                    // nichts machen, daniel machts manuell
                    continue 2;
                }
            }

            foreach ($requiredBuchungen as $crossSellingProduct) {
                if (false !== strpos($crossSellingProduct['Produktname'], 'nderpaket')) {
                    echo "Hat Länderpaket Option (todo Daniel)\n";
                    // bei länderpaket option wird ein anderes produkt benötigt.
                    // eigentlich hätte bereits deshalb ein fehler passieren sollen,
                    // aber möglicherweiße wurde dieser Kunde vergessen zu ändern.
                    // daniel macht das manuell
                    continue 2;
                }
            }

            // diese produkte sind NICHT gebucht, obwohl der kunde sie haben muss
            foreach ($crossSellingProducts as $key => $crossSellingProduct) {
                echo "Product muss noch gebucht (hinterlegt) werden:\n";
                //var_dump($crossSellingProduct);
                var_dump($crossSellingProduct['produkt_id'], $crossSellingProduct['Produktname']);
            }

            // diese Produkte benötigen noch eine Buchung
            foreach ($requiredBuchungen as $key => $crossSellingProduct) {
                echo "Diese Produkte benötigen noch eine Buchung:\n";
                var_dump($crossSellingProduct['produkt_id']);
            }

            // änderungen durchführen
            foreach ($todo as $array) {
                switch ($array['todo']) {
                    case '':
                        break;

                    case 'aktiviere und datum setzten (buchung?)':
                        break;
                }
            }

            // sicherheitshaber diesen Kunden noch mal "aktiv" setzten !!!


            
            //die(var_dump($buchungen));
//die();
echo "\n";

            

        }

        die("asd");
    }

    /**
     * Get buchungen for all accounts by kundennummer_extern
     * 
     * @return array
     */
    protected function getBuchungen($clientId)
    {
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';

        $purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
        $purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

        $kundennummer_extern = $clientId;

        $timeZone = new \DateTimeZone('Europe/Berlin');
        $today = new \DateTime('now', $timeZone);

        $parameter = [
            'kundennummer_extern' => $kundennummer_extern,
            'von_datum' => '20180101',
            'bis_datum' => $today->format('Ymd'),
            //'erweitert' => '2',
        ];

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($purtelSuperUsername),
            urlencode($purtelSuperPassword),
            'getbuchungen'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }
}

?>