<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Goutte\Client;

/**
 * 
 */
class FindCustomersWithoutActiveProducts extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('FindCustomersWithoutActiveProducts')
            ->setDescription('')
        ;
    }

    /**
     * 
     */
    protected function getProductsFromPurtel($purtelAccount)
    {
        // hole alle "gebuchten" Produkte des Kunden
        $bookedProducts = [];
       
        $client = new Client();
        
        $crawler = $client->request('GET', 'https://www2.purtel.com/res/'.$this->purtelReseller.'/index.php?username='
            .$this->purtelSuperUser.'&passwort='
            .$this->purtelSuperUserPassword.'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1&admin_ns_suchbegriff='
            .$purtelAccount.'&tab_id=3&marke=Zusatzleistungen');

        $crawler->selectButton('Ändern')->each(function ($node) use (&$bookedProducts) {
            $element = $node->parents()->eq(2);

            $matches = null;

            preg_match('/^\[ID (\d+)\] (Produkt|Crossselling) \[(\d+)\] (.*)$/', $element->filter('tr > td')->first()->text(), $matches);

            $form = $node->form();

            $bookedProduct = [
                'id' => $matches[1],
                'productId' => $matches[3],
                'name' => $matches[4],
                'form' => $form,
            ];

            $bookedProducts[$bookedProduct['productId']] = $bookedProduct;
        });

        return [
            'bookedProducts' => $bookedProducts,
            'usedCrawler' => $crawler,
        ];
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $deactivierungsvalueToString = [
            0 => 'aktiv lassen',
            1 => 'deaktivieren nach Laufzeit',
            2 => 'deaktivieren und nullen',
        ];

        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/tools/import/CsvReader.php';
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';

        $this->purtelSuperUser = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
        $this->purtelSuperUserPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
        $this->purtelReseller = \Wisotel\Configuration\Configuration::get('purtelReseller');

        $db = $this->getContainer()->get('doctrine')->getManager()->getConnection();

        $q = $db->query("SELECT pa.`purtel_login`, c.`clientid` 
            FROM `purtel_account` pa 
            INNER JOIN `customers` c ON c.`id` = pa.`cust_id` 
            WHERE pa.`master` = 1");

        while ($data = $q->fetch(\PDO::FETCH_ASSOC)) {
            $purtelAccount = $data['purtel_login'];
            $clientId = $data['clientid'];

            try {
                $products = $this->getProductsFromPurtel($purtelAccount);
            } catch (\GuzzleHttp\Exception\ConnectException $exception) {
                echo sprintf("unable to load product for anschluss %s - error: %s\n",
                    $purtelAccount,
                    $exception->getMessage()
                );

                continue;
            }

            if (empty($products['bookedProducts'])) {
                echo $clientId." - keine Produkte\n";

                continue;
            }

            $hasActiveProduct = false;

            // we could even check if customer has an active main-product
            foreach ($products['bookedProducts'] as $productData) {
                $aktiv = $productData['form']['produkt_leistung_aktiv']->getValue();

                if (1 == $aktiv) {
                    $hasActiveProduct = true;

                    break;
                }
            }

            if (!$hasActiveProduct) {
                echo $clientId." - keine Produkte\n";

                continue;
            }
        }

        echo "completely done\n";
    }
}

?>