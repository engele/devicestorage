<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use AppBundle\Entity\User;

/**
 * 
 */
class BulkMailerCommand extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('BulkMailerCommand')
            ->setDescription('')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        
        require_once $container->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';
        require_once $container->get('kernel')->getRootDir().'/../web/tools/import/CsvReader.php';

        $notificationEmailAddress = [
            'sven.siebrands@wisotel.com',
            'karl-heinz.legat@wisotel.com',
            'david.brix@wisotel.com',
            'Giuseppina.Gregorio@tkt-teleconsult.de',
            'ramon.delarosa@wisotel.com',
        ];

        $csv = new \CsvReader('/tmp/Aktive-Kunden---COLT_20181015112432.csv');

        $subject = 'WiSoTEL Kundeninformation';

$messageText = <<<TEXT_MESSAGE
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 plus MathML 2.0//EN" "http://www.w3.org/Math/DTD/mathml2/xhtml-math11-f.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><!--This file was converted to xhtml by LibreOffice - see http://cgit.freedesktop.org/libreoffice/core/tree/filter/source/xslt for the code.--><head profile="http://dublincore.org/documents/dcmi-terms/"><meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8"/><title xml:lang="en-US">- no title specified</title><meta name="DCTERMS.title" content="" xml:lang="en-US"/><meta name="DCTERMS.language" content="en-US" scheme="DCTERMS.RFC4646"/><meta name="DCTERMS.source" content="http://xml.openoffice.org/odf2xhtml"/><meta name="DCTERMS.creator" content="Giuseppina Gregorio"/><meta name="DCTERMS.issued" content="2018-09-24T08:48:00" scheme="DCTERMS.W3CDTF"/><meta name="DCTERMS.contributor" content="Giuseppina Gregorio"/><meta name="DCTERMS.modified" content="2018-09-24T08:50:00" scheme="DCTERMS.W3CDTF"/><meta name="DCTERMS.provenance" content="" xml:lang="en-US"/><meta name="DCTERMS.subject" content="," xml:lang="en-US"/><link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" hreflang="en"/><link rel="schema.DCTERMS" href="http://purl.org/dc/terms/" hreflang="en"/><link rel="schema.DCTYPE" href="http://purl.org/dc/dcmitype/" hreflang="en"/><link rel="schema.DCAM" href="http://purl.org/dc/dcam/" hreflang="en"/><style type="text/css">
    @page {  }
    table { border-collapse:collapse; border-spacing:0; empty-cells:show }
    td, th { vertical-align:top; font-size:12pt;}
    h1, h2, h3, h4, h5, h6 { clear:both;}
    ol, ul { margin:0; padding:0;}
    li { list-style: none; margin:0; padding:0;}
    /* "li span.odfLiEnd" - IE 7 issue*/
    li span. { clear: both; line-height:0; width:0; height:0; margin:0; padding:0; }
    span.footnodeNumber { padding-right:1em; }
    span.annotation_style_by_filter { font-size:95%; font-family:Arial; background-color:#fff000;  margin:0; border:0; padding:0;  }
    * { margin:0;}
    .P1 { font-size:12pt; line-height:100%; margin-bottom:0cm; margin-top:0cm; text-align:left ! important; font-family:Calibri; writing-mode:lr-tb; }
    .P2 { font-size:12pt; line-height:100%; margin-bottom:0cm; margin-top:0cm; text-align:center ! important; font-family:Times New Roman; writing-mode:lr-tb; }
    .P3 { font-size:12pt; line-height:100%; margin-bottom:0cm; margin-top:0cm; text-align:left ! important; font-family:Times New Roman; writing-mode:lr-tb; }
    .Standard { font-size:12pt; font-family:Times New Roman; writing-mode:lr-tb; margin-top:0cm; margin-bottom:0cm; line-height:100%; text-align:left ! important; }
    .Internet_20_link { color:#0000ff; text-decoration:underline; }
    .T1 { font-family:Calibri; }
    .T2 { font-family:Calibri; font-weight:bold; }
    .T4 { color:#000000; font-family:Calibri; }
    /* ODF styles with no properties representable as CSS */
     { }
    </style>
</head>
<body dir="ltr" style="max-width:50.59cm;margin-top:0.5cm; margin-bottom:2cm; margin-left:0.5cm; margin-right:2cm; ">
<p class="Standard"><span class="T1">Sehr geehrte Damen und Herren,</span></p><p class="Standard"><span class="T1">liebe WiSoTEL-Kunden,</span></p><p class="Standard"><span class="T1"> </span></p><p class="Standard"><span class="T1">um die Qualität unserer Dienste ständig zu verbessern, sind Wartungsarbeiten an unseren Netzen erforderlich. </span></p><p class="P1"> </p><p class="Standard"><span class="T1">Diese Maßnahme wird durchgeführt am</span></p><p class="Standard"><span class="T1"> </span></p><p class="P2"><span class="T2">Freitag, den 02.11.2018 ca. 23 Uhr bis Samstag, den 03.11.2018 ca. 5 Uhr. </span></p><p class="Standard"><span class="T1"> </span></p><p class="Standard"><span class="T1">In diesem Zeitraum wird es zu Ausfällen von bis zu ca. 3 Stunden </span><a id="_GoBack"/><span class="T1">im Netz kommen.</span></p><p class="Standard"><span class="T1"> </span></p><p class="Standard"><span class="T1">Wir bemühen uns, die Unannehmlichkeiten für Sie so gering wie möglich zu halten, und bedanken uns für Ihr Verständnis.</span></p><p class="Standard"><span class="T1"> </span></p><p class="Standard"><span class="T1"> </span></p><p class="Standard"><span class="T1">Freundliche Grüße aus Backnang</span></p><p class="Standard"><span class="T1"> </span></p><p class="Standard"><span class="T1">Ihre WiSoTEL Kundenbetreuung<br/>--------------------------------------------------------------<br/>WiSoTEL GmbH</span></p><p class="Standard"><span class="T1">Gesellschaft für Kommunikationslösungen<br/>Kuchengrund 8<br/>D - 71522 Backnang<br/><br/>Tel.:        +49 (0) 7191 3668-600<br/>Fax:        +49 (0) 7191 3668-999<br/>e-mail:    </span><a href="mailto:kundenbetreuung@wisotel.com" class="Internet_20_link"><span class="T1">kundenbetreuung@wisotel.com</span></a><span class="T1"><br/>info:       </span><a href="http://www.wisotel.com/" class="Internet_20_link"><span class="T1">http://www.wisotel.com</span></a><span class="T1"><br/>----------------------------------------------------------------<br/></span><span class="T4">Geschäftsleitung: Thomas Berkel<br/>Amtsgericht Stuttgart, HRB 72 4829<br/>Ust-IdNr.: DE 258275665<br/>----------------------------------------------------------------</span><span class="T1"> </span></p><p class="Standard"> </p></body></html>
TEXT_MESSAGE;
    
        $headline = array_shift($csv->data);

        $emailAddressKey = array_search('(emailaddress)', $headline);

        if (false === $emailAddressKey) {
            $emailAddressKey = array_search('(customers.emailaddress)', $headline);
        }

        if (false === $emailAddressKey) {
            echo "E-Mail-Address-Headline not found in csv - stopping\n";
            exit;
        }

        //$mailer = $this->getContainer()->get('mailer'); // let symfony do it - but it spools mails as default

        $transport = (new \Swift_SmtpTransport($container->getParameter('mailer_host'), $container->getParameter('mailer_port')))
            ->setUsername($container->getParameter('mailer_user'))
            ->setPassword($container->getParameter('mailer_password'))
            //->setEncryption($container->getParameter('mailer_encryption'))
        ;

        $mailer = new \Swift_Mailer($transport);

        foreach ($csv->data as $lineNum => $value){
            $emailAddress = trim($value[$emailAddressKey]);
//$emailAddress = $notificationEmailAddress[0];

            if (empty($emailAddress)) {
                echo "empty emailaddress - line ".$lineNum."\n";

                continue;
            }

            try {
                $message = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom('kundenbetreuung@wisotel.com')
                    ->setTo($emailAddress)
                    /*->setBody(
                        $this->twig->render('email/send-results.html.twig', [
                            'results' => $resultsToSend,
                        ]),
                        'text/html'
                    )*/
                    ->setBody($messageText, 'text/html');
                ;
            } catch (\Swift_RfcComplianceException $exception) {
                echo "message sending to ".$emailAddress." failed! - ".$exception->getMessage()."\n";

                continue;
            }

            if ($mailer->send($message)) {
                echo "message send to ".$emailAddress." success\n";
            } else {
                echo "message sending to ".$emailAddress." failed!\n";
            }
//break;
            sleep(5); // delay for 5 seconds
        }

        echo "all done\n";

        $message = \Swift_Message::newInstance()
            ->setSubject('Alle E-Mails wurden versendet - '.$subject)
            ->setFrom('kundenbetreuung@wisotel.com')
            ->setTo($notificationEmailAddress)
            ->setBody(sprintf("Alle E-Mails wurden versendet. Vorgang beendet.<br /><br />Verwendete Datei: %s<br /><br />Nachricht:<br /><br /><br />%s",
                $csv->getFilename(),
                $messageText
            ), 'text/html');
        ;

        if ($mailer->send($message)) {
            echo "message send to notificationEmailAddress (".$notificationEmailAddress[0].") success\n";
        } else {
            echo "message sending to notificationEmailAddress (".$notificationEmailAddress[0].") failed!\n";
        }
    }
}

?>