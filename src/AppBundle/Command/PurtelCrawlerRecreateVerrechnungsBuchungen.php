<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Goutte\Client;

/**
 * 
 */
class PurtelCrawlerRecreateVerrechnungsBuchungen extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('PurtelCrawlerRecreateVerrechnungsBuchungen')
            ->setDescription('')
        ;
    }

    /**
     * Convert any netto price into a gross price in cents
     * 
     * @return int|null
     */
    protected function convertToPurtelBruttoPrice($nettoPrice)
    {
        if (empty($nettoPrice)) {
            return 0;
        }

        if ((float) $nettoPrice > 0) {
            $tax = bcmul($nettoPrice, '0.19', 5);
            $gross = bcadd($nettoPrice, $tax, 5);
            $grossInCents = bcmul($gross, 100, 5);
        } else {
            // no tax bei gutschriften
            $grossInCents = bcmul($nettoPrice, 100, 5);
        }

        return round($grossInCents, 0);
    }

    /**
     * 
     */
    protected function getProductsFromPurtel($purtelAccount)
    {
        // hole alle "gebuchten" Produkte des Kunden
        $bookedProducts = [];
       
        $client = new Client();
        
        $crawler = $client->request('GET', 'https://www2.purtel.com/res/'.$this->purtelReseller.'/index.php?username='
            .$this->purtelSuperUser.'&passwort='
            .$this->purtelSuperUserPassword.'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1&admin_ns_suchbegriff='
            .$purtelAccount.'&tab_id=3&marke=Zusatzleistungen');

        $crawler->selectButton('Ändern')->each(function ($node) use (&$bookedProducts) {
            $element = $node->parents()->eq(2);

            $matches = null;

            preg_match('/^\[ID (\d+)\] (Produkt|Crossselling) \[(\d+)\] (.*)$/', $element->filter('tr > td')->first()->text(), $matches);

            $form = $node->form();

            $bookedProduct = [
                'id' => $matches[1],
                'productId' => $matches[3],
                'name' => $matches[4],
                'form' => $form,
            ];

            $bookedProducts[$bookedProduct['productId']] = $bookedProduct;
        });

        return [
            'bookedProducts' => $bookedProducts,
            'usedCrawler' => $crawler,
        ];
    }

    /**
     * Update booked product at purtel
     * 
     * @return boolean
     */
    protected function updateBookedProductAtPurtel($bookedProduct, \DateTime $date, $laufzeit, $price)
    {
        $form = $bookedProduct['form'];

        unset($form['admin_crossselling_nebenstelle_loeschen']); // important! otherwise product will be delete at purtel

        $form['produkt_leistung_tag']->setValue($date->format('d'));
        $form['produkt_leistung_monat']->setValue($date->format('m'));
        $form['produkt_leistung_jahr']->setValue($date->format('Y'));

        $form['produkt_leistung_grundgebuehr']->setValue($price);

        if (1 === preg_match('/^\d*$/', $laufzeit)) {
            $form['produkt_leistung_laufzeit']->setValue($laufzeit);
        }

        $form['produkt_leistung_aktiv']->tick();

        // nur zur protokollierung
        echo "updateBookedProductAtPurtel Values:\n";
        
        $printValues = [
            'produkt_leistung_tag',
            'produkt_leistung_monat',
            'produkt_leistung_jahr',
            'produkt_leistung_grundgebuehr',
            'produkt_leistung_laufzeit',
            'produkt_leistung_aktiv',
        ];

        foreach ($printValues as $key) {
            echo sprintf("\t%s: %s\n", $key, $form[$key]->getValue());
        }

        $client = new Client();

        $crawler = $client->submit($form);

        $dataPropperlySubmitted = false;
        $productFound = false;

        $crawler->selectButton('Ändern')->each(function ($node) use (&$bookedProduct, $date, $laufzeit, $price, &$productFound, &$dataPropperlySubmitted) {
            $element = $node->parents()->eq(2);

            $matches = null;

            preg_match('/^\[ID (\d+)\] (Produkt|Crossselling) \[(\d+)\] (.*)$/', $element->filter('tr > td')->first()->text(), $matches);

            if ((string) $bookedProduct['productId'] !== (string) $matches[3]) {
                return;
            }

            $productFound = true;

            $responseForm = $node->form();

            // is active?
            if ("1" !== (string) $responseForm['produkt_leistung_aktiv']->getValue()) {
                echo "updateBookedProductAtPurtel response - product not active - product: ".$bookedProduct['productId']."\n";

                return;
            }

            // correct date?
            if ($responseForm['produkt_leistung_tag']->getValue() !== $date->format('d')
                || $responseForm['produkt_leistung_monat']->getValue() !== $date->format('m')
                || $responseForm['produkt_leistung_jahr']->getValue() !== $date->format('Y')) {
                
                echo "updateBookedProductAtPurtel response - incorrect date - product: ".$bookedProduct['productId']."\n";

                return;
            }

            // correct laufzeit?
            $laufzeitAtPurtel = (string) $responseForm['produkt_leistung_laufzeit']->getValue();

            if (1 === preg_match('/^\d*$/', $laufzeit) && $laufzeit > 0) {
                if ($laufzeitAtPurtel !== (string) $laufzeit) {
                    echo sprintf("updateBookedProductAtPurtel response - incorrect laufzeit - product: %s - bei purtel %s - expected %s\n",
                        $bookedProduct['productId'],
                        $laufzeitAtPurtel,
                        $laufzeit
                    );

                    return;
                }
            } elseif (!empty($laufzeitAtPurtel) && $laufzeitAtPurtel !== '0') {
                echo sprintf("updateBookedProductAtPurtel response - incorrect laufzeit - product: %s - bei purtel %s - expected %s\n",
                    $bookedProduct['productId'],
                    $laufzeitAtPurtel,
                    $laufzeit
                );

                return;
            }

            unset($laufzeitAtPurtel); // no longer needed

            // correct price?
            if ((string) $price !== (string) $responseForm['produkt_leistung_grundgebuehr']->getValue()) {
                echo sprintf("updateBookedProductAtPurtel response - incorrect price - product: %s - price at purtel: %s - expected price: %s\n",
                    $bookedProduct['productId'],
                    $responseForm['produkt_leistung_grundgebuehr']->getValue(),
                    $price
                );

                return;
            }

            $dataPropperlySubmitted = true;
        });

        if (!$productFound) {
            echo "updateBookedProductAtPurtel response - product is gone - product: ".$bookedProduct['productId']."\n";
        }

        return $dataPropperlySubmitted;
    }

    /**
     * Update booked product at purtel
     * 
     * @return boolean
     */
    protected function deleteBookedProductAtPurtel($bookedProduct)
    {
        $form = $bookedProduct['form'];

        //unset($form['admin_crossselling_nebenstelle_loeschen']); // important! otherwise product will be delete at purtel

        //$form['produkt_leistung_aktiv']->tick();

        $client = new Client();

        $crawler = $client->submit($form);

        $productFound = false;

        $crawler->selectButton('Ändern')->each(function ($node) use (&$bookedProduct, &$productFound) {
            $element = $node->parents()->eq(2);

            $matches = null;

            preg_match('/^\[ID (\d+)\] (Produkt|Crossselling) \[(\d+)\] (.*)$/', $element->filter('tr > td')->first()->text(), $matches);

            if ((string) $bookedProduct['productId'] !== (string) $matches[3]) {
                return;
            }

            $productFound = true;
        });

        return !$productFound ? true : false;
    }

    /**
     * add crossselling product
     * 
     * @return boolean
     */
    protected function addCrossselling($crawler, $productId, \DateTime $date)
    {
        $client = new Client();

        $form = $crawler->selectButton('Hinzufügen')->form();

        $form['show_produkt_nebenstelle']->select($productId);
        $form['produkt_menge']->setValue(1);
        $form['produkt_anteilig_berechnen']->setValue(1);
        $form['produkt_tag']->setValue($date->format('d'));
        $form['produkt_monat']->setValue($date->format('m'));
        $form['produkt_jahr']->setValue($date->format('Y'));

        $responseCrawler = $client->submit($form);

        $bookedProductForm = null;

        $responseCrawler->selectButton('Ändern')->each(function ($node) use (&$bookedProductForm, &$productId) {
            $element = $node->parents()->eq(2);

            $matches = null;

            preg_match('/^\[ID (\d+)\] (Produkt|Crossselling) \[(\d+)\] (.*)$/', $element->filter('tr > td')->first()->text(), $matches);

            if ((string) $productId !== (string) $matches[3]) {
                return;
            }

            $bookedProductForm = $node->form();
        });

        if (null === $bookedProductForm) {
            return false;
        }

        return true;
    }

    /**
     * Get buchungen for all accounts by kundennummer_extern
     * 
     * @return array|null
     */
    protected function getBuchungen($clientId)
    {
        $kundennummer_extern = $clientId;

        $timeZone = new \DateTimeZone('Europe/Berlin');
        $today = new \DateTime('now', $timeZone);

        $parameter = [
            'kundennummer_extern' => $kundennummer_extern,
            'von_datum' => '20180101',
            'bis_datum' => $today->format('Ymd'),
            'erweitert' => '5',
        ];

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'getbuchungen'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/tools/import/CsvReader.php';
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';

        $this->purtelSuperUser = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
        $this->purtelSuperUserPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
        $this->purtelReseller = 'res100260';

        $danielCsv = new \CsvReader('/tmp/Buchungsliste Siebrands Verrechnung mit Gegenbuchung 28062018.csv');

        $products = [
            '7278' => 'Startguthaben 200 Euro',
            '7552' => 'Startguthaben 100 Euro',
            '7626' => 'Finderlohn',
        ];

        $skipCustomer = [
        ];

        $donePurtelAccounts = [];

        foreach ($danielCsv->data as $line) {
            $purtelAccount = $line[$danielCsv->headlineReversed['rufnummer']];
            $verwendungszweck = $line[$danielCsv->headlineReversed['verwendungszweck']];
            $betrag_haben = $line[$danielCsv->headlineReversed['betrag_haben']];
            $date = $line[$danielCsv->headlineReversed['datum']];
            $clientId = $line[$danielCsv->headlineReversed['kundennummer_extern']];

            echo $clientId." - ".$purtelAccount."\n";

            if (in_array($clientId, $skipCustomer)) {
                echo "skipped..\n";

                continue;
            }

            if (isset($donePurtelAccounts[$purtelAccount])) {
                echo "customer already done - seen now ".++$donePurtelAccounts[$purtelAccount]." times\n";

                continue;
            }

            $match = null;
            preg_match('/^(\d{4})(\d{2})(\d{2})\d{6}$/', $date, $match);

            $dateY = $match[1];
            $dateM = $match[2];
            $dateD = $match[3];

            $expectedDate = new \DateTime($dateY.'-'.$dateM.'-'.$dateD);

            $foundProduct = null;

            $productsFromPurtel = $this->getProductsFromPurtel($purtelAccount);

            try {
                foreach ($productsFromPurtel['bookedProducts'] as $purtelProduct) {
                    switch ($purtelProduct['productId']) {
                        case '7278': // Startguthaben 200 Euro
                        case '7552': // Startguthaben 100 Euro
                        case '7626': // Finderlohn
                            if (null !== $foundProduct) {
                                throw new \Exception(sprintf('Kunde (Purtel: %s) hat mehrfach Gutschriften CrossSellings gebucht - manuell machen.', 
                                    $purtelAccount
                                ));
                            }

                            $foundProduct = $purtelProduct;

                            break;
                    }
                }
            } catch (\Exception $exception) {
                echo $exception->getMessage()."\n";
                $donePurtelAccounts[$purtelAccount] = 1;

                continue;
            }

            $foundBuchung = null;

            $buchungen = $this->getBuchungen($clientId);

            try {
                if (is_array($buchungen)) {
                    if (isset($buchungen[0]) && 'id' == $buchungen[0][0]) {
                        unset($buchungen[0]); // remove first row if it is headline
                    }

                    foreach ($buchungen as $buchungKey => $buchung) {
                        if (empty($buchung[6]) || '0' == $buchung[6]) { // $buchung[6] = "erledigt"
                            $buchungIsGutschrift = false;

                            if (false !== strpos(utf8_decode($buchung[3]), 'Startguthaben 200 Euro')) {
                                $buchungIsGutschrift = true;
                            } else if (false !== strpos(utf8_decode($buchung[3]), 'Startguthaben 100 Euro')) {
                                $buchungIsGutschrift = true;
                            } else if (false !== strpos(utf8_decode($buchung[3]), 'Finderlohn')) {
                                $buchungIsGutschrift = true;
                            }

                            if (!$buchungIsGutschrift) {
                                continue;
                            }

                            if (null !== $foundBuchung) {
                                throw new \Exception(sprintf('Kunde (Purtel: %s) hat mehrfach Gutschriften Buchung - manuell machen.', 
                                    $purtelAccount
                                ));
                            }

                            $foundBuchung = $buchung;
                        }
                    }
                }
            } catch (\Exception $exception) {
                echo $exception->getMessage()."\n";
                $donePurtelAccounts[$purtelAccount] = 1;

                continue;
            }

            // kein kunde hatte das
            if (null !== $foundBuchung) {
                sprintf("found buchung %s\n", $foundBuchung[3]);

                echo "muss noch programmiert werden - continue\n";
                $donePurtelAccounts[$purtelAccount] = 1;
                
                continue;
            }

            $expectedProductId = array_search($verwendungszweck, $products);

            if (null !== $foundProduct) {
                echo "product - ".$foundProduct['productId']."\n";
                
                // same as in csv
                if (!is_numeric($expectedProductId)) {
                    echo sprintf("produkt (%s) unbekannt - manuell\n", $verwendungszweck);
                    $donePurtelAccounts[$purtelAccount] = 1;

                    continue;
                }

                if ((string) $foundProduct['productId'] !== (string) $expectedProductId) {
                    echo "falsches gutschrift-produkt aktiv - manuell\n";
                    $donePurtelAccounts[$purtelAccount] = 1;

                    continue;

                }

                if ($this->deleteBookedProductAtPurtel($foundProduct)) {
                    echo "produkt geloescht\n";
                } else {
                    echo "produkt konnte nicht geloescht werden - manuell\n";

                    continue;
                }
            }

            /*if (null === $foundBuchung && null === $foundProduct) {
                echo "weder produkt noch buchung\n";

                continue;
            }*/

            // create crossselling

            if ($this->addCrossselling($productsFromPurtel['usedCrawler'], $expectedProductId, $expectedDate)) {
                echo "produkt erfolgreich angelegt\n";
            } else {
                echo sprintf("produkt (%s) konnte nicht angelegt werden\n", $verwendungszweck);
            }

            $donePurtelAccounts[$purtelAccount] = 1;
        }












/*


        $groupedCrossSellingProducts = $this->getCrossellings();

        // append customersOnlyWithMainProduct to groupedCrossSellingProducts
        foreach ($this->customersOnlyWithMainProduct as $purtelAccount => $data) {
            $groupedCrossSellingProducts[$purtelAccount] = $data;
        }

        foreach ($groupedCrossSellingProducts as $purtelAccount => $crossSellingProducts) {
            if (isset($crossSellingProducts['cId'])) {
                $customerId = $crossSellingProducts['cId'];
                $clientId = $crossSellingProducts['ikv-Kundennummer'];
                $dimariId = $crossSellingProducts['Dimari-Kundennummer'];
                $crossSellingProducts = []; // empty here
            } else {
                $customerId = $crossSellingProducts[0]['cId'];
                $clientId = $crossSellingProducts[0]['ikv-Kundennummer'];
                $dimariId = $crossSellingProducts[0]['Dimari-Kundennummer'];
            }

            echo "\tstarting customer ".$clientId." (dimari: ".$dimariId.")\n";

            if (in_array($customerId, $skipCustomer) || in_array($clientId, $skipCustomer)) {
                echo "skipped..\n";

                continue;
            }

            $customerDataFromDanielCsv = $this->danielCsv->findLines([
                'ikv' => $clientId,
            ]);

            if (empty($customerDataFromDanielCsv)) {
                echo sprintf("customer (%s) not found on danielCsv (%s)\n",
                    $clientId,
                    $this->danielCsv->getFilename()
                );

                continue;
            } elseif (count($customerDataFromDanielCsv) > 1) {
                echo sprintf("customer (%s) found multiple times (%s) on danielCsv (%s)\n",
                    $clientId,
                    count($customerDataFromDanielCsv),
                    $this->danielCsv->getFilename()
                );

                continue;
            }

            try {
                $customersCurrentIkvProduct = $this->getMainProduct($customerId);
            } catch (\Exception $e) {
                echo $e->getMessage()." (line: ".$e->getLine().") (todo Daniel) - ".$clientId."\n";

                continue;
            }

            //$customersCurrentIkvProduct_purtel = null;

            if ($this->hasLaenderpaket($customersCurrentIkvProduct, $crossSellingProducts)) {
                echo "Hat Länderpaket Option (todo Daniel) - customer: ".$clientId."\n";

                continue;
            }

            // hole produkte des kunden von purtel
            $productsFromPurtel = $this->getProductsFromPurtel($purtelAccount);

            // sind produkte bei purtel hinterlegt, die der kunde gar nicht haben sollte?
            $tmpBookedProductsFromPurtel = $productsFromPurtel['bookedProducts'];

            if (isset($tmpBookedProductsFromPurtel[$customersCurrentIkvProduct['purtel_product_id']])) {
                unset($tmpBookedProductsFromPurtel[$customersCurrentIkvProduct['purtel_product_id']]); // remove main product from stack
            }

            foreach ($crossSellingProducts as $crossSelling) {
                if (isset($tmpBookedProductsFromPurtel[$crossSelling['purtel_product_id']])) {
                    unset($tmpBookedProductsFromPurtel[$crossSelling['purtel_product_id']]); // remove cross selling product from stack
                }
            }

            $otherProductsActive = false;

            if (!empty($tmpBookedProductsFromPurtel)) {
                foreach ($tmpBookedProductsFromPurtel as $productId => $data) {
                    if ("1" == $data['form']['produkt_leistung_aktiv']->getValue()) {
                        $otherProductsActive = true;

                        $name = 'unknown name';

                        if (isset($this->purtelMainProducts[$productId])) {
                            $name = $this->purtelMainProducts[$productId]['name'];
                        } elseif (isset($this->purtelCrossSellingProducts[$productId])) {
                            $name = $this->purtelCrossSellingProducts[$productId]['name'];
                        }

                        echo sprintf("unerwartetes product (%s) '%s' beim kunde aktiv - ".$clientId."\n",
                            $productId,
                            $name
                        );
                    }
                }
            }

            unset($tmpBookedProductsFromPurtel); // no longer needed

            if ($otherProductsActive) {
                echo "other products active - mauell - ".$clientId."\n";

                continue;
            }



            
            //var_dump($productsFromPurtel['bookedProducts']);
            //var_dump($crossSellingProducts);
            //var_dump($customersCurrentIkvProduct);

            

            $foundAMainProduct = null;

            // suche nach irgendeinem hautprodukt
            foreach ($productsFromPurtel['bookedProducts'] as $productId => $data) {
                if ("1" !== $data['form']['produkt_leistung_aktiv']->getValue()) {
                    // nicht aktiv
                    continue;
                }

                if (isset($this->purtelMainProducts[$productId])) {
                    if (null !== $foundAMainProduct) {
                        echo "multiple Hauptprodukte beim Kunde aktiv (manuell schauen) - customerId: ".$clientId."\n";

                        continue 2;
                    }

                    $foundAMainProduct = $productId;
                }
            }

            // falls hauptprodukt bereits aktiv - prüfen, ob es das richtige ist
            if (null !== $foundAMainProduct && (string) $customersCurrentIkvProduct['purtel_product_id'] !== (string) $foundAMainProduct) {
                // kunde hat ein aktives hauptprodukt, allerdings ist es nicht das, dass er haben sollte
                echo "anderes Hauptprodukt bereits aktiv (manuell schauen) - customerId: ".$clientId."\n";
                echo sprintf("erwartet (%s) %s tasächlich aktiv (%s) %s\n",
                    $customersCurrentIkvProduct['purtel_product_id'],
                    $this->purtelMainProducts[$customersCurrentIkvProduct['purtel_product_id']]['name'],
                    $foundAMainProduct,
                    $this->purtelMainProducts[$foundAMainProduct]['name']
                );

                continue;
            }

            // ist das technische hauptprodukt das, dass er haben sollte?
            $currentTechnicalPurtelProducts = $this->getCurrentTechnicalPurtelProducts($clientId);

            $currentTechnicalPurtelProduct = null;

            foreach ($currentTechnicalPurtelProducts as $array) {
                if (null === $currentTechnicalPurtelProduct) {
                    $currentTechnicalPurtelProduct = $array[4];
                    continue;
                }

                if ($currentTechnicalPurtelProduct !== $array[4]) {
                    echo sprintf("not same product for all accounts (%s) - %s & %s - customer (%s)\n",
                        $array[1],
                        $currentTechnicalPurtelProduct,
                        $array[4],
                        $clientId
                    );

                    continue 2;
                }
            }

            if ((string) $customersCurrentIkvProduct['purtel_product_id'] !== (string) $currentTechnicalPurtelProduct) {
                echo "hat technisch anderes hauptprodukt als er haben sollte (manuell schauen) - customerId: ".$clientId."\n";
                echo sprintf("erwartet (%s) %s tasächlich aktiv (%s) %s\n",
                    $customersCurrentIkvProduct['purtel_product_id'],
                    $this->purtelMainProducts[$customersCurrentIkvProduct['purtel_product_id']]['name'],
                    $currentTechnicalPurtelProduct,
                    $this->purtelMainProducts[$currentTechnicalPurtelProduct]['name']
                );

                continue;
            }

            // existiert ein gebuchtes (aktiv & inaktiv) produkt bei purtel schon für das hauptprodukt?
            if (isset($productsFromPurtel['bookedProducts'][$customersCurrentIkvProduct['purtel_product_id']])) {
                $bookedMainProduct = $productsFromPurtel['bookedProducts'][$customersCurrentIkvProduct['purtel_product_id']];

                $needUpdating = false;

                // is active?
                if ("1" !== $bookedMainProduct['form']['produkt_leistung_aktiv']->getValue()) {
                    $needUpdating = true;
                }

                // correct date?
                if ($bookedMainProduct['form']['produkt_leistung_tag']->getValue() !== $customersCurrentIkvProduct['activation_date']->format('d')
                    || $bookedMainProduct['form']['produkt_leistung_monat']->getValue() !== $customersCurrentIkvProduct['activation_date']->format('m')
                    || $bookedMainProduct['form']['produkt_leistung_jahr']->getValue() !== $customersCurrentIkvProduct['activation_date']->format('Y')) {
                    
                    $needUpdating = true;
                }

                // correct laufzeit?
                $laufzeitAtPurtel = (string) $bookedMainProduct['form']['produkt_leistung_laufzeit']->getValue();

                if (1 === preg_match('/^\d*$/', $customersCurrentIkvProduct['laufzeit']) && $customersCurrentIkvProduct['laufzeit'] > 0) {
                    if ($laufzeitAtPurtel !== (string) $customersCurrentIkvProduct['laufzeit']) {
                        $needUpdating = true;
                    }
                } elseif (!empty($laufzeitAtPurtel) && $laufzeitAtPurtel !== '0') {
                    $needUpdating = true;
                }

                unset($laufzeitAtPurtel); // no longer needed

                // correct price?
                if ((string) $customersCurrentIkvProduct['basisentgelt_bruttobasiert'] !== (string) $bookedMainProduct['form']['produkt_leistung_grundgebuehr']->getValue()) {
                    $needUpdating = true;
                }

                if ($needUpdating) {
                    // dann einfach mit aktiv und richtigem datum schreiben
                    //echo "dann einfach mit aktiv und richtigem datum schreiben - ".$clientId."\n";

                    $response = $this->updateBookedProductAtPurtel(
                        $bookedMainProduct,
                        $customersCurrentIkvProduct['activation_date'],
                        $customersCurrentIkvProduct['laufzeit'],
                        $customersCurrentIkvProduct['basisentgelt_bruttobasiert']
                    );

                    if (!$response) {
                        echo "hauptprodukt wurde fehlerhaft bei purtel übernommen - ".$clientId."\n";

                        continue;
                    }

                    echo "hauptprodukt wurde ordentlich bei purtel aktualisiert - ".$clientId."\n";
                } else {
                    echo "hauptprodukt (datum, laufzeit, preis und aktiv) stimmt bereits - ".$clientId."\n";
                }
            } else {
                // hauptprodukt anlegen
                //echo "hauptprodukt anlegen - ".$clientId."\n";

                $response = $this->changeAndUpdateMainProductAtPurtel(
                    $productsFromPurtel['usedCrawler'],
                    $customersCurrentIkvProduct['purtel_product_id'],
                    $customersCurrentIkvProduct['activation_date'],
                    $customersCurrentIkvProduct['laufzeit'],
                    $customersCurrentIkvProduct['basisentgelt_bruttobasiert']
                );

                if (!$response) {
                    echo "hauptprodukt wurde fehlerhaft oder gar nicht bei purtel angelegt - ".$clientId."\n";

                    continue;
                }

                echo "hauptprodukt wurde ordentlich bei purtel angelegt und datum gesetzt - ".$clientId."\n";
            }

            

            // go thru $crossSellingProducts


            foreach ($crossSellingProducts as $crossSelling) {
                // do we know a price?
                if (null === $crossSelling['basisentgelt_bruttobasiert']) {
                    echo "preis für crossselling (".$crossSelling['purtel_product_id'].") fehlt, Produkt nicht übertragen - ".$clientId."\n";

                    continue;
                }

                // existiert ein gebuchtes (aktiv & inaktiv) produkt bei purtel schon für das crossselling produkt?
                if (isset($productsFromPurtel['bookedProducts'][$crossSelling['purtel_product_id']])) {
                    $bookedCrossSellingProduct = $productsFromPurtel['bookedProducts'][$crossSelling['purtel_product_id']];
                    
                    $needUpdating = false;

                    // is active?
                    if ("1" !== $bookedCrossSellingProduct['form']['produkt_leistung_aktiv']->getValue()) {
                        $needUpdating = true;
                    }

                    // correct date?
                    if ($bookedCrossSellingProduct['form']['produkt_leistung_tag']->getValue() !== $crossSelling['activation_date']->format('d')
                        || $bookedCrossSellingProduct['form']['produkt_leistung_monat']->getValue() !== $crossSelling['activation_date']->format('m')
                        || $bookedCrossSellingProduct['form']['produkt_leistung_jahr']->getValue() !== $crossSelling['activation_date']->format('Y')) {
                        
                        $needUpdating = true;
                    }

                    // correct laufzeit?
                    $laufzeitAtPurtel = (string) $bookedCrossSellingProduct['form']['produkt_leistung_laufzeit']->getValue();

                    if (1 === preg_match('/^\d*$/', $crossSelling['laufzeit']) && $crossSelling['laufzeit'] > 0) {
                        if ($laufzeitAtPurtel !== (string) $crossSelling['laufzeit']) {
                            $needUpdating = true;
                        }
                    } elseif (!empty($laufzeitAtPurtel) && $laufzeitAtPurtel !== '0') {
                        $needUpdating = true;
                    }

                    unset($laufzeitAtPurtel); // no longer needed

                    // correct price?
                    if ((string) $crossSelling['basisentgelt_bruttobasiert'] !== (string) $bookedCrossSellingProduct['form']['produkt_leistung_grundgebuehr']->getValue()) {
                        $needUpdating = true;
                    }

                    if ($needUpdating) {
                        // crossselling dann einfach mit aktiv und richtigem datum schreiben
                        //echo "crossselling dann einfach mit aktiv und richtigem datum schreiben - ".$clientId."\n";

                        $response = $this->updateBookedProductAtPurtel(
                            $bookedCrossSellingProduct,
                            $crossSelling['activation_date'],
                            $crossSelling['laufzeit'],
                            $crossSelling['basisentgelt_bruttobasiert']
                        );

                        if (!$response) {
                            echo "crossselling (".$crossSelling['purtel_product_id'].") wurde fehlerhaft bei purtel übernommen - stopped this customer - ".$clientId."\n";

                            continue 2;
                        }

                        echo "crossselling (".$crossSelling['purtel_product_id'].") wurde ordentlich bei purtel übernommen - ".$clientId."\n";
                    } else {
                        echo "crossselling (".$crossSelling['purtel_product_id'].") (datum, laufzeit, preis und aktiv) stimmt bereits - ".$clientId."\n";
                    }
                } else {
                    // crossselling produkt anlegen
                    //echo "crossselling anlegen - ".$clientId."\n";

                    $response = $this->addCrossselling(
                        $productsFromPurtel['usedCrawler'],
                        $crossSelling['purtel_product_id'],
                        $crossSelling['activation_date'],
                        $crossSelling['laufzeit'],
                        $crossSelling['basisentgelt_bruttobasiert']
                    );

                    if (!$response) {
                        echo "crossselling (".$crossSelling['purtel_product_id'].") wurde fehlerhaft order gar nicht bei purtel angelegt - ".$clientId."\n";

                        continue;
                    }

                    echo "crossselling (".$crossSelling['purtel_product_id'].") wurde ordentlich bei purtel angelegt und datum gesetzt - ".$clientId."\n";
                }
            }


            // hole noch mal die produkte des kunden von purtel
            // (damit die von uns angelegten auch dabei sind)
            $productsFromPurtel = $this->getProductsFromPurtel($purtelAccount);

            // go thru buchungen

            $allBuchungen = $this->getBuchungen($clientId);
            $matchedBuchungen = $this->matchBuchungenToProductIds($allBuchungen);
            $matchedBuchungenOnlyOnePerProduct = [];

            // see if multiple buchungen for one product exist
            $multipleBuchungenForOneProductFound = false;

            foreach ($matchedBuchungen as $productId => $buchungen) {
                if (count($buchungen) > 1) {
                    // have this multiple buchungen same verwendungszweck
                    $multipleBuchungenHaveSameVerwendungszweck = true;
                    $verwendungszweck = null;

                    foreach ($buchungen as $buchung) {
                        if (null === $verwendungszweck) {
                            $verwendungszweck = $buchung['buchung'][3];

                            continue;
                        }

                        if ($verwendungszweck !== $buchung['buchung'][3]) {
                            $multipleBuchungenHaveSameVerwendungszweck = false;
                        }
                    }

                    if ($multipleBuchungenHaveSameVerwendungszweck) {
                        // if this are multiple buchungen for current main product,
                        // we can safely delete them.

                        // is this buchung for main product?
                        if ($productId == $customersCurrentIkvProduct['purtel_product_id']) {
                            // delete buchungen
                            foreach ($buchungen as $buchung) {
                                echo "delete multiple buchung - ".$clientId."\n";
                                var_dump($buchung);

                                // remove from allBuchungen otherwise still thinks this buchungen exists
                                unset($allBuchungen[$buchung['originalKey']]);


                                if ($this->deleteBuchung($buchung['buchung'])) {
                                    echo "deleting multiple success - ".$clientId."\n";
                                } else {
                                    echo "deleting multiple failed - ".$clientId."\n";
                                }

                            }

                            continue;
                        }
                    }

                    $multipleBuchungenForOneProductFound = true;

                    $productName = 'unknown product';

                    if (isset($this->purtelMainProducts[$productId])) {
                        $productName = $this->purtelMainProducts[$productId]['name'];
                    } elseif (isset($this->purtelCrossSellingProducts[$productId])) {
                        $productName = $this->purtelCrossSellingProducts[$productId]['name'];
                    }

                    foreach ($buchungen as $buchung) {
                        echo sprintf("multiple buchungen for product (%s) %s found - %s - %s\n",
                            $productId,
                            $productName,
                            $buchung['buchung'][3],
                            $clientId
                        );
                    }
                } else {
                    $matchedBuchungenOnlyOnePerProduct[$productId] = end($buchungen);
                }
            }

            if ($multipleBuchungenForOneProductFound) {
                echo "multiple buchungen for one product found - buchungen manuell machen - ".$clientId."\n";

                continue;
            }

            // continue with the nicer name
            $matchedBuchungen = $matchedBuchungenOnlyOnePerProduct;

            unset($matchedBuchungenOnlyOnePerProduct);

            // sind buchungen bei purtel hinterlegt, die der kunde gar nicht haben sollte?
            $buchungenToBeDeleted = [
                '/Endabrechnung wegen Tarifwechsel$/',
                '/^Papierrechung anteilig/',
            ];

            $tmpBuchungen = $allBuchungen;

            if (isset($matchedBuchungen[$customersCurrentIkvProduct['purtel_product_id']])) {
                unset($tmpBuchungen[$matchedBuchungen[$customersCurrentIkvProduct['purtel_product_id']]['originalKey']]); // remove buchung for main product from stack
            }

            foreach ($crossSellingProducts as $crossSelling) {
                if (isset($matchedBuchungen[$crossSelling['purtel_product_id']])) {
                    // only handle stuff we have a price for
                    if (null === $crossSelling['basisentgelt_bruttobasiert']) {
                        continue;
                    }

                    unset($tmpBuchungen[$matchedBuchungen[$crossSelling['purtel_product_id']]['originalKey']]); // remove buchung for cross selling product from stack
                }
            }

            $otherBuchungActive = false;

            if (!empty($tmpBuchungen)) {
                foreach ($tmpBuchungen as $buchung) {
                    if (empty($buchung[6]) || '0' == $buchung[6]) { // $buchung[6] = "erledigt"
                        foreach ($buchungenToBeDeleted as $pattern) {
                            if (1 === preg_match($pattern, utf8_decode($buchung[3]))) {
                                echo "delete buchung - ".$clientId."\n";
                                var_dump($buchung);

                                if ($this->deleteBuchung($buchung)) {
                                    echo "deleting success - ".$clientId."\n";
                                } else {
                                    echo "deleting failed - ".$clientId."\n";
                                }

                                continue 2;
                            }
                        }

                        // ignore this buchung
                        if (1 === preg_match('/kulanzgutschrift/i', $buchung[3])) {
                            continue;
                        }

                        $otherBuchungActive = true;

                        echo sprintf("unerwartete buchung (%s) beim kunde aktiv - ".$clientId."\n",
                            $buchung[3]
                        );
                    }
                }
            }

            unset($tmpBuchungen); // no longer needed

            if ($otherBuchungActive) {
                echo "other buchung active - mauell - ".$clientId."\n";

                continue;
            }

            //$buchungsDate = new \DateTime('2018-02-28'); // tag der buchungen
            //$amountOfDaysForAnteiligeBuchung = 1;
            //$daysOfMonth = 28;
            //$amountOfDaysForGegenbuchung = $daysOfMonth - $amountOfDaysForAnteiligeBuchung;
            $dateOfActivationAtPurtel = end($customerDataFromDanielCsv);
            $dateOfActivationAtPurtel = trim($dateOfActivationAtPurtel[$this->danielCsv->headlineReversed['Vertragsbeginn purtel']]);
            $dateOfActivationAtPurtel = new \DateTime($dateOfActivationAtPurtel);
            $buchungsDate = $dateOfActivationAtPurtel; // tag der buchungen
            $amountOfDaysForGegenbuchung = ((int) $dateOfActivationAtPurtel->format('d')) - 1;
            $daysOfMonth = (int) $dateOfActivationAtPurtel->format('t');
            $amountOfDaysForAnteiligeBuchung = $daysOfMonth - $amountOfDaysForGegenbuchung;

            //$namePattern = '/^{productName} anteilig f..?r \d+ Tage$/';
            $namePattern = '/^{productName} anteilige Gutschrift f..?r '.$amountOfDaysForGegenbuchung.' Tage$/';
            
            $productAtPurtel = $this->purtelMainProducts[$customersCurrentIkvProduct['purtel_product_id']];
            //$verwendungszweck = $productAtPurtel['name'].' anteilig für 1 Tage';
            $verwendungszweck = $productAtPurtel['name'].' anteilige Gutschrift für '.$amountOfDaysForGegenbuchung.' Tage';
            //$expectedPrice = bcmul(
            //    bcdiv($customersCurrentIkvProduct['basisentgelt_bruttobasiert'], $daysOfMonth, 8),
            //    $amountOfDaysForAnteiligeBuchung,
            //    8
            //);
            $expectedPrice = bcmul(
                bcdiv($customersCurrentIkvProduct['basisentgelt_bruttobasiert'], $daysOfMonth, 8),
                $amountOfDaysForGegenbuchung,
                8
            );

            // existiert eine buchung (aktiv & inaktiv) bei purtel schon für das hauptprodukt?
            if (isset($matchedBuchungen[$customersCurrentIkvProduct['purtel_product_id']])) {
                $buchung = $matchedBuchungen[$customersCurrentIkvProduct['purtel_product_id']]['buchung'];

                $needUpdating = false;

                // correct date?
                if (0 !== strpos($buchung[7], $buchungsDate->format('Ymd'))) {
                    $needUpdating = true;
                }

                // correct price?
                // wenn gutschrift, dann muss soll gebucht werden, da auf der rechnung schon standartmäßig gutgeschrieben wird
                if ($expectedPrice < 0) {
                    if (0 !== bccomp($expectedPrice * -1, $buchung[4], 8)) {
                        $needUpdating = true;
                    }
                } elseif (0 !== bccomp($expectedPrice, $buchung[5], 8)) {
                    $needUpdating = true;
                }

                // correct name?
                $productName = str_replace(
                    [
                        '/',
                        'ü',
                        'ä',
                        'ö',
                        'ß',
                    ],
                    [
                        '\/',
                        '.',
                        '.',
                        '.',
                        '.',
                    ],
                    $productAtPurtel['name']
                );

                $expectedNamePattern = str_replace('{productName}', $productName, $namePattern);

                if (1 !== preg_match($expectedNamePattern, utf8_decode($buchung[3]))) {
                    $needUpdating = true;
                }

                // correct sap-konto?
                if ((string) $buchung[23] !== (string) $productAtPurtel['sap']) {
                    $needUpdating = true;
                }

                if ($needUpdating) {
                    echo "update buchung - ".$clientId."\n";
                    var_dump($buchung);

                    $buchung[7] = $buchungsDate->format('Ymd').'000000'; // datum

                    // wenn gutschrift, dann muss soll gebucht werden, da auf der rechnung schon standartmäßig gutgeschrieben wird
                    if ($expectedPrice < 0) {
                        $buchung[4] = $expectedPrice * -1; // preis
                        $buchung[5] = null;
                    } else {
                        $buchung[5] = $expectedPrice; // preis
                        $buchung[4] = null;
                    }

                    $buchung[3] = $verwendungszweck; // verwendungszweck
                    $buchung[23] = $productAtPurtel['sap']; // sap-Konto

                    if ($this->updateBuchung($buchung, 'update')) {
                        echo "update succes\n";
                    } else {
                        echo "update failed\n";
                    }

                } else {
                    echo "buchung fuer ".$productAtPurtel['name']." stimmt bereits - ".$clientId."\n";
                }
            } else {
                echo "buchung anlegen - ".$clientId."\n";

                $buchung = [
                    2 => $purtelAccount, // anschluss
                    3 => $verwendungszweck, // verwendungszweck
                    7 => $buchungsDate->format('Ymd').'000000', // datum
                    23 => $productAtPurtel['sap'], // sap-Konto
                ];

                if ($expectedPrice < 0) {
                    $buchung[4] = $expectedPrice * -1; // preis
                } else {
                    $buchung[5] = $expectedPrice; // preis
                }

                if ($this->createBuchung($buchung, $clientId)) {
                    echo "create buchung succes\n";
                } else {
                    echo "create buchung failed\n";
                }

            }

            // buchungen für crossselling produkte
            foreach ($crossSellingProducts as $crossSelling) {
                // do we know a price?
                if (null === $crossSelling['basisentgelt_bruttobasiert']) {
                    echo "preis für crossselling (".$crossSelling['purtel_product_id'].") fehlt, keine Buchung erfolgt - ".$clientId."\n";

                    continue;
                }

                $productAtPurtel = $this->purtelCrossSellingProducts[$crossSelling['purtel_product_id']];
                //$verwendungszweck = $productAtPurtel['name'].' anteilig für 1 Tage';
                $verwendungszweck = $productAtPurtel['name'].' anteilige Gutschrift für '.$amountOfDaysForGegenbuchung.' Tage';
                //$expectedPrice = bcmul(
                //    bcdiv($crossSelling['basisentgelt_bruttobasiert'], $daysOfMonth, 8),
                //    $amountOfDaysForAnteiligeBuchung,
                //    8
                //);
                $expectedPrice = bcmul(
                    bcdiv($crossSelling['basisentgelt_bruttobasiert'], $daysOfMonth, 8),
                    $amountOfDaysForGegenbuchung,
                    8
                );

                // a special something - price has to be 0 for certain products
                if (in_array($crossSelling['purtel_product_id'], ['7754', '7556'])) {
                    $expectedPrice = 0;
                }

                // wenn die laufzeit eines produktes heute (now!) bereits abgelaufen ist,
                // und das produkt "deaktivieren nach Laufzeit" order "deaktivieren und nullen" eingestellt hat,
                // dann wird das produkt automatisch nach dem anlegen gleich wieder ohne datum deaktiviert
                // und dadurch wird nicht mehr die regelmäßige standardbuchung für die monate zwischen heute und "buchungstag"
                // erzeugt.
                // daher müssen die buchungen dafür auch noch angelegt werden - aber, wichtig, nicht mehr als "gegenbuchung"

                // ohne laufzeit, kein problem. daher erster check, ob es überhaupt eine beschränkte laufzeit gibt
                if (1 === preg_match('/^\d*$/', $crossSelling['laufzeit']) && $crossSelling['laufzeit'] > 0) {
                    $deactivationDate = clone $crossSelling['activation_date']; // in case we need original again
                    $deactivationDate->setTime(0, 0, 0, 0);
                    $deactivationDate->add(new \DateInterval('P'.$crossSelling['laufzeit'].'M'));

                    $now = new \DateTime('now + 4 weeks'); // 4 weeks buffer
                    $now->setTime(0, 0, 0, 0);

                    if ($now->diff($deactivationDate)->invert > 0) {
                        // das produkt ist heute bereits abgelaufen, war aber beim datum von getCrossellings() noch aktiv

                        if (isset($productsFromPurtel['bookedProducts'][$crossSelling['purtel_product_id']])) {
                            $bookedCrossSellingProduct = $productsFromPurtel['bookedProducts'][$crossSelling['purtel_product_id']];

                            if (isset($bookedCrossSellingProduct['form']['produkt_deaktivieren'])) {
                                // ist es auf "deaktivieren nach Laufzeit" oder "deaktivieren und nullen" eingestellt
                                if ('0' !== (string) $bookedCrossSellingProduct['form']['produkt_deaktivieren']->getValue()) {
                                    // problemfall erreicht..

                                    $diffInterval = $buchungsDate->diff($deactivationDate);
                                    $months = ($diffInterval->y * 12) + $diffInterval->m;

                                    if ($diffInterval->d > 0) {
                                        $months++;
                                    }

                                    echo sprintf("ACHTUNG! spezialfall! Laufzeit mittlerweile beendet - "
                                        ."Buchungen fuer Produkt (%s) muessen manuell fuer %s Monate angelegt werden - %s\n",
                                        $productAtPurtel['name'],
                                        $months,
                                        $clientId
                                    );

                                    continue; // alles wurde hier abgearbeitet
                                }
                            }
                        }
                    }
                }

                
                // existiert eine buchung (aktiv & inaktiv) bei purtel schon für das crossselling produkt?
                if (isset($matchedBuchungen[$crossSelling['purtel_product_id']])) {
                    $buchung = $matchedBuchungen[$crossSelling['purtel_product_id']]['buchung'];

                    $needUpdating = false;

                    // correct date?
                    if (0 !== strpos($buchung[7], $buchungsDate->format('Ymd'))) {
                        $needUpdating = true;
                    }

                    // correct price?
                    if ($expectedPrice < 0) {
                        if (0 !== bccomp($expectedPrice * -1, $buchung[4], 8)) {
                            $needUpdating = true;
                        }
                    } elseif (0 !== bccomp($expectedPrice, $buchung[5], 8)) {
                        $needUpdating = true;
                    }

                    // correct name?
                    $productName = str_replace(
                        [
                            '/',
                            'ü',
                            'ä',
                            'ö',
                            'ß',
                        ],
                        [
                            '\/',
                            '.',
                            '.',
                            '.',
                            '.',
                        ],
                        $productAtPurtel['name']
                    );

                    $expectedNamePattern = str_replace('{productName}', $productName, $namePattern);

                    if (1 !== preg_match($expectedNamePattern, utf8_decode($buchung[3]))) {
                        $needUpdating = true;
                    }

                    // correct sap-konto?
                    if ((string) $buchung[23] !== (string) $productAtPurtel['sap']) {
                        $needUpdating = true;
                    }

                    if ($needUpdating) {
                        if (0 == $expectedPrice) {
                            echo "Buchung mit 0 euro gefunden - kann geloescht werden - ".$clientId."\n";
                            var_dump($buchung);
                            //if ($this->deleteBuchung($buchung)) {
                            //    echo "deleting success - ".$clientId."\n";
                            //} else {
                            //    echo "deleting failed - ".$clientId."\n";
                            //}

                            continue;
                        } else {
                            echo "update buchung - ".$clientId."\n";
                            var_dump($buchung);

                            $buchung[7] = $buchungsDate->format('Ymd').'000000'; // datum

                            if ($expectedPrice < 0) {
                                $buchung[4] = $expectedPrice * -1; // preis
                                $buchung[5] = null;
                            } else {
                                $buchung[5] = $expectedPrice; // preis
                                $buchung[4] = null;
                            }

                            $buchung[3] = $verwendungszweck; // verwendungszweck
                            $buchung[23] = $productAtPurtel['sap']; // sap-konto

                            if ($this->updateBuchung($buchung, 'update')) {
                                echo "update succes\n";
                            } else {
                                echo "update failed\n";
                            }

                        }
                    } else {
                        echo "crossselling buchung fuer ".$productAtPurtel['name']." stimmt bereits - ".$clientId."\n";
                    }
                } else {
                    if (0 == $expectedPrice) {
                        echo "crossselling buchung (".$crossSelling['purtel_product_id'].") price = 0 - keine buchung sinvoll - ".$clientId."\n";

                        continue;
                    }

                    echo "crossselling buchung (".$crossSelling['purtel_product_id'].") anlegen - ".$clientId."\n";

                    $buchung = [
                        2 => $purtelAccount, // anschluss
                        3 => $verwendungszweck, // verwendungszweck
                        7 => $buchungsDate->format('Ymd').'000000', // datum
                        23 => $productAtPurtel['sap'], // sap-konto
                    ];

                    if ($expectedPrice < 0) {
                        $buchung[4] = $expectedPrice * -1; // preis
                    } else {
                        $buchung[5] = $expectedPrice; // preis
                    }

                    if ($this->createBuchung($buchung, $clientId)) {
                        echo "create crossselling buchung (".$crossSelling['purtel_product_id'].") succes\n";
                    } else {
                        echo "create crossselling buchung (".$crossSelling['purtel_product_id'].") failed\n";
                    }

                }
            }

            echo "successfuly finished customer ".$clientId."\n";
            

            //var_dump($matchedBuchungen);
//echo "\tsleeping 1 sec\n";
//sleep(1);
//die();

        }
*/
        echo "completely done\n";
    }
}

?>