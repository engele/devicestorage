<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Goutte\Client;

/**
 * 
 */
class PurtelCrawlerCreateNullEuroBuchungen extends ContainerAwareCommand
{
    protected $purtelMainProducts = [ // price = brutto
        '4578' => ['name' => 'Leistungsfrei', 'price' => '0', 'sap' => null],
        '4582' => ['name' => 'Fon / 24 Monate', 'price' => '1495', 'sap' => '412080'],
        '4794' => ['name' => 'Web 100 / 24 Monate', 'price' => '2495', 'sap' => '412080'],
        '4796' => ['name' => 'HD-TV & Fon / 24 Monate', 'price' => '2495', 'sap' => '412080'],
        '4798' => ['name' => 'Komfort 100 / 24 Monate', 'price' => '2995', 'sap' => '412080'],
        '4842' => ['name' => 'HD-TV / 24 Monate', 'price' => '995', 'sap' => '412080'],
        '4870' => ['name' => 'HD-TV & Web 100 / 24 Monate', 'price' => '3495', 'sap' => '412080'],
        '4876' => ['name' => 'Premium 100 / 24 Monate', 'price' => '3995', 'sap' => '412080'],
        '4979' => ['name' => 'Fon', 'price' => '1495', 'sap' => '412080'],
        '4981' => ['name' => 'HD-TV & Web 100', 'price' => '3495', 'sap' => '412080'],
        '4983' => ['name' => 'Web 100', 'price' => '2495', 'sap' => '412080'],
        '4985' => ['name' => 'Komfort 100', 'price' => '2995', 'sap' => '412080'],
        '4987' => ['name' => 'Premium 100', 'price' => '3995', 'sap' => '412080'],
        '4989' => ['name' => 'HD-TV & Fon', 'price' => '2495', 'sap' => '412080'],
        '4991' => ['name' => 'HD-TV', 'price' => '995', 'sap' => '412080'],
        '5130' => ['name' => 'Business I3 / 36 Monate', 'price' => '5343', 'sap' => '412230'],
        '5132' => ['name' => 'Business S1-AA/2', 'price' => '2368', 'sap' => '412220'],
        '5142' => ['name' => 'Glasfaser Premium 100', 'price' => '4990', 'sap' => '412080'],
        '5144' => ['name' => 'Glasfaser Komfort 100', 'price' => '3990', 'sap' => '412080'],
        '5146' => ['name' => 'Glasfaser Web', 'price' => '3490', 'sap' => '412080'],
        '5150' => ['name' => 'Glasfaser Fon', 'price' => '2190', 'sap' => '412080'],
        '5152' => ['name' => 'Glasfaser HD-TV', 'price' => '1390', 'sap' => '412080'],
        '5154' => ['name' => 'Glasfaser COM-FIX GeWo ', 'price' => '0', 'sap' => '412080'],
        '5156' => ['name' => 'Glasfaser Premium 50', 'price' => '3990', 'sap' => '412080'],
        '5158' => ['name' => 'Glasfaser Premium 100 / 24 Monate', 'price' => '4990', 'sap' => '412080'],
        '5160' => ['name' => 'Glasfaser Premium 50 / 24 Monate', 'price' => '3990', 'sap' => '412080'],
        '5162' => ['name' => 'Glasfaser Komfort 100 / 24 Monate', 'price' => '3990', 'sap' => '412080'],
        '5164' => ['name' => 'Glasfaser Komfort 50', 'price' => '2990', 'sap' => '412080'],
        '5166' => ['name' => 'Glasfaser Komfort 50 / 24 Monate', 'price' => '2990', 'sap' => '412080'],
        '5168' => ['name' => 'Glasfaser Web 100', 'price' => '2990', 'sap' => '412080'],
        '5170' => ['name' => 'Glasfaser Web 100 / 24 Monate', 'price' => '2990', 'sap' => '412080'],
        '5172' => ['name' => 'Glasfaser Web 50', 'price' => '2489', 'sap' => '412080'],
        '5174' => ['name' => 'Glasfaser Web 50 / 24 Monate', 'price' => '2489', 'sap' => '412080'],
        '5258' => ['name' => 'Glasfaser Fon / 24 Monate', 'price' => '1990', 'sap' => '412080'],
        '5260' => ['name' => 'Glasfaser Fon und HD-TV', 'price' => '2990', 'sap' => '412080'],
        '5262' => ['name' => 'Glasfaser Fon und HD-TV / 24 Monate', 'price' => '2990', 'sap' => '412080'],
        '5264' => ['name' => 'Glasfaser HD-TV / 24 Monate', 'price' => '1390', 'sap' => '412080'],
        '5266' => ['name' => 'Glasfaser Web 50 und HD-TV / 24 Monate', 'price' => '3490', 'sap' => '412080'],
        '5268' => ['name' => 'Glasfaser Web 25', 'price' => '1990', 'sap' => '412080'],
        '5270' => ['name' => 'Glasfaser Web 25 / 24 Monate', 'price' => '1990', 'sap' => '412080'],
        '5272' => ['name' => 'Glasfaser Komfort 25 / 24 Monate', 'price' => '2489', 'sap' => '412080'],
        '5274' => ['name' => 'Glasfaser Premium 25 / 24 Monate', 'price' => '3490', 'sap' => '412080'],
        '5360' => ['name' => 'Business S1-MGA/2', 'price' => '2368', 'sap' => '412220'],
        '5362' => ['name' => 'Business S2-AA/5', 'price' => '4748', 'sap' => '412220'],
        '5364' => ['name' => 'Business S3-AA/10', 'price' => '11888', 'sap' => '412220'],
        '5366' => ['name' => 'Business S4 AA/30', 'price' => '11888', 'sap' => '412220'],
        '5444' => ['name' => 'Premium 100 / 24 Monate + LP1', 'price' => '3995', 'sap' => '412080'],
        '5494' => ['name' => 'Glasfaser Komfort 50 + LP 1', 'price' => '2990', 'sap' => '412080'],
        '5496' => ['name' => 'Glasfaser COM-FIX GeWo + LP 1', 'price' => '0', 'sap' => '412080'],
        '5498' => ['name' => 'Glasfaser COM-FIX GeWo + LP 1 & LP 2', 'price' => '0', 'sap' => '412080'],
        '5500' => ['name' => 'Glasfaser COM-FIX GeWo + LP 2', 'price' => '0', 'sap' => '412080'],
        '5502' => ['name' => 'Glasfaser Fon und HD-TV + LP 1', 'price' => '2990', 'sap' => '412080'],
        '5504' => ['name' => 'Glasfaser Fon + LP 1', 'price' => '2190', 'sap' => '412080'],
        '5506' => ['name' => 'Glasfaser Komfort 100 + LP 1', 'price' => '3990', 'sap' => '412080'],
        '5508' => ['name' => 'Glasfaser Komfort 100 + LP 2', 'price' => '3990', 'sap' => '412080'],
        '5510' => ['name' => 'Glasfaser Komfort 25 + LP 1', 'price' => '2489', 'sap' => '412080'],
        '5512' => ['name' => 'Glasfaser Komfort 50 + LP 2', 'price' => '2990', 'sap' => '412080'],
        '5514' => ['name' => 'Glasfaser Premium 100 + LP 1', 'price' => '4990', 'sap' => '412080'],
        '5516' => ['name' => 'Glasfaser Premium 50 + LP 1', 'price' => '3990', 'sap' => '412080'],
        '5518' => ['name' => 'Glasfaser Premium 50 + LP 2', 'price' => '3990', 'sap' => '412080'],
        '5520' => ['name' => 'Premium 100 + LP1', 'price' => '3990', 'sap' => '412080'],
        '5524' => ['name' => 'Komfort 100 + LP 1', 'price' => '2990', 'sap' => '412080'],
        '5526' => ['name' => 'Komfort 100 + LP 2', 'price' => '2990', 'sap' => '412080'],
        '5528' => ['name' => 'Komfort 100 / 24 Monate + LP 1', 'price' => '2990', 'sap' => '412080'],
        '5530' => ['name' => 'Komfort 100 / 24 Monate + LP 2', 'price' => '2990', 'sap' => '412080'],
        '5664' => ['name' => 'Business I2 / 36 Monate', 'price' => '4153', 'sap' => '412230'],
        '5666' => ['name' => 'Business I1 / 36 Monate', 'price' => '2963', 'sap' => '412230'],
        '5532' => ['name' => 'Test-voip-dabei', 'price' => '0', 'sap' => '412080'],
        '5838' => ['name' => 'Willkommen - Komfort 100 / 24 Monate', 'price' => '1495', 'sap' => '412080'],
        '5840' => ['name' => 'Willkommen - Web 100 / 24 Monate', 'price' => '1495', 'sap' => '412080'],
        '5852' => ['name' => 'KabelKiosk Basis HD', 'price' => '490', 'sap' => '412080'],
        '5858' => ['name' => 'KabelKiosk Family HD inkl. Basis HD', 'price' => '1990', 'sap' => '412080'],
    ];

    protected $purtelCrossSellingProducts = [
        '7572' => ['name' => '2-4. Telefonleitung', 'price' => null, 'sap' => '412080'],
        '7524' => ['name' => '2. Telefonleitung', 'price' => null, 'sap' => '412080'],
        '7588' => ['name' => '2. Telefonleitung inkl.', 'price' => null, 'sap' => '412080'],
        '7574' => ['name' => 'Anschlusssperrung', 'price' => null, 'sap' => '412260'],
        '7568' => ['name' => 'AVM FritzBox 7360 Kauf', 'price' => null, 'sap' => '412260'],
        '7534' => ['name' => 'AVM FritzBox 7360 Miete', 'price' => null, 'sap' => '412080'],
        '7570' => ['name' => 'AVM FritzBox 7360 Miete / 24 Monate', 'price' => null, 'sap' => '412080'],
        '7560' => ['name' => 'AVM FritzBox 7390 Kauf', 'price' => null, 'sap' => '412260'],
        '7562' => ['name' => 'AVM FritzBox 7390 Miete', 'price' => null, 'sap' => '412080'],
        '7616' => ['name' => 'AVM FritzBox 7390 Miete / 24 Monate', 'price' => null, 'sap' => '412080'],
        '7532' => ['name' => 'AVM FritzBox 7490 Kauf', 'price' => null, 'sap' => '412260'],
        '7618' => ['name' => 'AVM FritzBox 7490 Miete', 'price' => null, 'sap' => '412080'],
        '7540' => ['name' => 'AVM FritzBox 7490 Miete / 24 Monate', 'price' => null, 'sap' => '412080'],
        '7628' => ['name' => 'AVM FritzBox 7560 Kauf', 'price' => null, 'sap' => '412260'],
        '7630' => ['name' => 'AVM FritzBox 7560 Miete', 'price' => null, 'sap' => '412080'],
        '7632' => ['name' => 'AVM FritzBox 7560 Miete / 24 Monate', 'price' => null, 'sap' => '412080'],
        '7712' => ['name' => 'Bearbeitungsgebühr ', 'price' => null, 'sap' => '412260'],
        '7578' => ['name' => 'Berechnung nicht zurückgesendeter Hardware 1.Jahr: AVM Fritz!Box Fon Wlan 7360', 'price' => null, 'sap' => '412260'],
        '7582' => ['name' => 'Berechnung nicht zurückgesendeter Hardware 1.Jahr: AVM Fritz!Box Fon Wlan 7390 ', 'price' => null, 'sap' => '412260'],
        '7620' => ['name' => 'Berechnung nicht zurückgesendeter Hardware 1.Jahr: AVM Fritz!Box Fon Wlan 7490', 'price' => null, 'sap' => '412260'],
        '7580' => ['name' => 'Berechnung nicht zurückgesendeter Hardware 2.Jahr: AVM Fritz!Box Fon Wlan 7360', 'price' => null, 'sap' => '412260'],
        '7576' => ['name' => 'Berechnung nicht zurückgesendeter Hardware 2.Jahr: AVM Fritz!Box Fon Wlan 7390', 'price' => null, 'sap' => '412260'],
        '7622' => ['name' => 'Berechnung nicht zurückgesendeter Hardware 2.Jahr: AVM Fritz!Box Fon Wlan 7490', 'price' => null, 'sap' => '412260'],
        '7780' => ['name' => 'Business AE Installationsarbeiten - Arbeiten nach Aufwand (a 10 Min.)', 'price' => null, 'sap' => '412230'],
        '7764' => ['name' => 'Business CLIP Übermittlung der Rufnummer des Anrufers ', 'price' => null, 'sap' => '412080'],
        '7796' => ['name' => 'Business Deutschland-Festnetz-Flatrate S-1', 'price' => null, 'sap' => '412220'],
        '7798' => ['name' => 'Business Deutschland-Festnetz-Flatrate S-2', 'price' => null, 'sap' => '412220'],
        '7800' => ['name' => 'Business Deutschland-Festnetz-Flatrate S-3', 'price' => null, 'sap' => '412220'],
        '7802' => ['name' => 'Business Deutschland-Festnetz-Flatrate S-4', 'price' => null, 'sap' => '412220'],
        '7716' => ['name' => 'Business Einrichtungspreis bei 12 Monaten Lfz.', 'price' => null, 'sap' => '412230'],
        '7718' => ['name' => 'Business Einrichtungspreis bei 24 Monaten Lfz.', 'price' => null, 'sap' => '412230'],
        '7720' => ['name' => 'Business Einrichtungspreis bei 36 Monaten Lfz.', 'price' => null, 'sap' => '412230'],
        '7756' => ['name' => 'Business Feste IP-Adresse', 'price' => null, 'sap' => '412230'],
        '7744' => ['name' => 'Business Grundgebühr Zusatz Internet-1, Internet-2, Internet-3 ', 'price' => null, 'sap' => '412230'],
        '7790' => ['name' => 'Business Internet-1 25/2,5 Mbit/s ', 'price' => null, 'sap' => '412230'],
        '7792' => ['name' => 'Business Internet-2 50/5 Mbit/s ', 'price' => null, 'sap' => '412230'],
        '7794' => ['name' => 'Business Internet-3 100/10 Mbit/s', 'price' => null, 'sap' => '412230'],
        '7758' => ['name' => 'Business Kundeneigene Hardware', 'price' => null, 'sap' => '412260'],
        '7748' => ['name' => 'Business Länderpaket Option 1', 'price' => null, 'sap' => '412220'],
        '7750' => ['name' => 'Business Länderpaket Option 2', 'price' => null, 'sap' => '412220'],
        '7746' => ['name' => 'Business National Mobilfunk TOP', 'price' => null, 'sap' => '412220'],
        '7782' => ['name' => 'Business National Mobilfunknetz S-1', 'price' => null, 'sap' => '412220'],
        '7784' => ['name' => 'Business National Mobilfunknetz S-2', 'price' => null, 'sap' => '412220'],
        '7786' => ['name' => 'Business National Mobilfunknetz S-3', 'price' => null, 'sap' => '412220'],
        '7788' => ['name' => 'Business National Mobilfunknetz S-4', 'price' => null, 'sap' => '412220'],
        '7754' => ['name' => 'Business Nichtteilnahme am (Sepa-) Lastschriftverfahren', 'price' => null, 'sap' => '412230'],
        '7734' => ['name' => 'Business Papierrechnung Geschäftskunde', 'price' => null, 'sap' => '412230'],
        '7714' => ['name' => 'Business S-1 CPE Miete', 'price' => null, 'sap' => '412220'],
        '7736' => ['name' => 'Business S-1 Fritzbox 7490 Kauf', 'price' => null, 'sap' => '412260'],
        '7738' => ['name' => 'Business S-1 Fritzbox 7490 Miete', 'price' => null, 'sap' => '412230'],
        '7722' => ['name' => 'Business S-2 CPE Miete', 'price' => null, 'sap' => '412220'],
        '7724' => ['name' => 'Business S-3 CPE Kauf', 'price' => null, 'sap' => '412260'],
        '7726' => ['name' => 'Business S-3 CPE Miete', 'price' => null, 'sap' => '412220'],
        '7728' => ['name' => 'Business S-4 CPE Kauf', 'price' => null, 'sap' => '412260'],
        '7730' => ['name' => 'Business S-4 CPE Miete', 'price' => null, 'sap' => '412220'],
        '7760' => ['name' => 'Business TV-Anschluss', 'price' => null, 'sap' => '412230'],
        '7752' => ['name' => 'Business Umzugspauschale', 'price' => null, 'sap' => '412230'],
        '7732' => ['name' => 'Business Vertragswechsel', 'price' => null, 'sap' => '412230'],
        '7694' => ['name' => 'Einrichtungsgebühr 1', 'price' => null, 'sap' => '412260'],
        '7690' => ['name' => 'Einrichtungsgebühr 1 / 24 Monate  ', 'price' => null, 'sap' => '412260'],
        '7696' => ['name' => 'Einrichtungsgebühr 2', 'price' => null, 'sap' => '412260'],
        '7330' => ['name' => 'Einrichtungsgebühr 2 / 24 Monate', 'price' => null, 'sap' => '412260'],
        '7698' => ['name' => 'Einrichtungsgebühr 3', 'price' => null, 'sap' => '412260'],
        '7692' => ['name' => 'Einrichtungsgebühr 3 / 24 Monate  ', 'price' => null, 'sap' => '412260'],
        '7566' => ['name' => 'Erstattung Anschlusspreis', 'price' => null, 'sap' => '412260'],
        '7586' => ['name' => 'Fehlerbeseitigung bei ungerechtfertigter Störung', 'price' => null, 'sap' => '412260'],
        '7626' => ['name' => 'Finderlohn', 'price' => null, 'sap' => '552020'],
        '7536' => ['name' => 'FTTB-Switch Kauf', 'price' => null, 'sap' => '412260'],
        '7608' => ['name' => 'FTTB-Switch zur Miete', 'price' => null, 'sap' => '412080'],
        '8016' => ['name' => 'Glasfaser HD-TV Kombi', 'price' => null, 'sap' => '412080'],
        '7340' => ['name' => 'Glasfasergrundgebühr', 'price' => null, 'sap' => '412080'],
        '7762' => ['name' => 'GTN Medienconverter', 'price' => null, 'sap' => '412260'],
        '8018' => ['name' => 'Gutschrift Rechnungskorrektur', 'price' => null, 'sap' => '412080'],
        '7704' => ['name' => 'Gutschrift Sondervereinbarung Vertrieb', 'price' => null, 'sap' => '552020'],
        '7778' => ['name' => 'Gutschrift Sondervereinbarung Vertrieb einmalig', 'price' => null, 'sap' => '552020'],
        '7700' => ['name' => 'Gutschrift SWI Aktion 24 Monate', 'price' => null, 'sap' => '552020'],
        '7590' => ['name' => 'Gutschrift Teilbereitstellung', 'price' => null, 'sap' => '412080'],
        '7706' => ['name' => 'Installations- und Servicearbeiten', 'price' => null, 'sap' => '412260'],
        '7610' => ['name' => 'KabelKiosk Aktivierungsgebühr inkl. Smart Card', 'price' => null, 'sap' => '412260'],
        '7594' => ['name' => 'KabelKiosk Basis HD', 'price' => null, 'sap' => '412080'],
        '7708' => ['name' => 'KabelKiosk Basis HD inkl.', 'price' => null, 'sap' => '412080'],
        '7612' => ['name' => 'KabelKiosk Conax Neotion CI+ Modul', 'price' => null, 'sap' => '412260'],
        '7598' => ['name' => 'KabelKiosk Duo Paket', 'price' => null, 'sap' => '412080'],
        '7538' => ['name' => 'KabelKiosk Family HD inkl. Basis HD', 'price' => null, 'sap' => '412080'],
        '7710' => ['name' => 'KabelKiosk Family HD inkl. Basis HD bei Glasfaser Premium', 'price' => null, 'sap' => '412080'],
        '7600' => ['name' => 'KabelKiosk International 1', 'price' => null, 'sap' => '412080'],
        '7602' => ['name' => 'KabelKiosk International 2', 'price' => null, 'sap' => '412080'],
        '7604' => ['name' => 'KabelKiosk International 3', 'price' => null, 'sap' => '412080'],
        '7606' => ['name' => 'KabelKiosk International 4', 'price' => null, 'sap' => '412080'],
        '7596' => ['name' => 'KabelKiosk Single Paket', 'price' => null, 'sap' => '412080'],
        '7564' => ['name' => 'Kunde ist im Besitz eigener Hardware', 'price' => null, 'sap' => '412260'],
        '7548' => ['name' => 'Kunde wirbt Kunde', 'price' => null, 'sap' => '552020'],
        '7526' => ['name' => 'Länderpaket Option 1', 'price' => null, 'sap' => '412080'],
        '7528' => ['name' => 'Länderpaket Option 2', 'price' => null, 'sap' => '412080'],
        '7776' => ['name' => 'Materialkosten', 'price' => null, 'sap' => '412260'],
        '7636' => ['name' => 'nicht zurückgesendete Smartcard', 'price' => null, 'sap' => '412260'],
        '7556' => ['name' => 'Nichtteilnahme am (Sepa-) Lastschriftverfahren', 'price' => null, 'sap' => '412080'],
        '7530' => ['name' => 'Papierrechnung', 'price' => null, 'sap' => '412080'],
        '7550' => ['name' => 'Rabatt auf Grundgebühr', 'price' => null, 'sap' => '412260'],
        '7634' => ['name' => 'Receiver Humax Fox C inkl. Festplatte extern 1 TB', 'price' => null, 'sap' => '412080'],
        '7688' => ['name' => 'Receiver Technisat ISIO inkl. Festplatte extern 1 TB', 'price' => null, 'sap' => '412080'],
        '7584' => ['name' => 'Rufnummernmitnahme', 'price' => null, 'sap' => '412260'],
        '7552' => ['name' => 'Startguthaben 100 Euro', 'price' => null, 'sap' => '552020'],
        '7278' => ['name' => 'Startguthaben 200 Euro', 'price' => null, 'sap' => '552020'],
        '7558' => ['name' => 'Umzugspauschale', 'price' => null, 'sap' => '412260'],
        '7614' => ['name' => 'Upgrade Bandbreite 100 ', 'price' => null, 'sap' => '412080'],
        '8092' => ['name' => 'Upgrade Bandbreite 50', 'price' => null, 'sap' => '412080'],
        '7638' => ['name' => 'Zusätzliche Technikeranfahrt', 'price' => null, 'sap' => '412260'],
        '7530' => ['name' => 'Papierrechung', 'price' => null, 'sap' => '412080'],
    ];

    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('PurtelCrawlerCreateNullEuroBuchungen')
            ->setDescription('')
        ;
    }

    /**
     * Convert any netto price into a gross price in cents
     * 
     * @return int|null
     */
    protected function convertToPurtelBruttoPrice($nettoPrice)
    {
        if (empty($nettoPrice)) {
            return 0;
        }

        if ((float) $nettoPrice > 0) {
            $tax = bcmul($nettoPrice, '0.19', 5);
            $gross = bcadd($nettoPrice, $tax, 5);
            $grossInCents = bcmul($gross, 100, 5);
        } else {
            // no tax bei gutschriften
            $grossInCents = bcmul($nettoPrice, 100, 5);
        }

        return round($grossInCents, 0);
    }

    /**
     * 
     */
    protected function getProductsFromPurtel($purtelAccount)
    {
        // hole alle "gebuchten" Produkte des Kunden
        $bookedProducts = [];
       
        $client = new Client();
        
        $crawler = $client->request('GET', 'https://www2.purtel.com/res/'.$this->purtelReseller.'/index.php?username='
            .$this->purtelSuperUser.'&passwort='
            .$this->purtelSuperUserPassword.'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1&admin_ns_suchbegriff='
            .$purtelAccount.'&tab_id=3&marke=Zusatzleistungen');

        $crawler->selectButton('Ändern')->each(function ($node) use (&$bookedProducts) {
            $element = $node->parents()->eq(2);

            $matches = null;

            preg_match('/^\[ID (\d+)\] (Produkt|Crossselling) \[(\d+)\] (.*)$/', $element->filter('tr > td')->first()->text(), $matches);

            $form = $node->form();

            $bookedProduct = [
                'id' => $matches[1],
                'productId' => $matches[3],
                'name' => $matches[4],
                'form' => $form,
            ];

            $bookedProducts[$bookedProduct['productId']] = $bookedProduct;
        });

        return [
            'bookedProducts' => $bookedProducts,
            'usedCrawler' => $crawler,
        ];
    }

    /**
     * Update booked product at purtel
     * 
     * @return boolean
     */
    protected function updateBookedProductAtPurtel($bookedProduct, \DateTime $date, $laufzeit, $price)
    {
        $form = $bookedProduct['form'];

        unset($form['admin_crossselling_nebenstelle_loeschen']); // important! otherwise product will be delete at purtel

        $form['produkt_leistung_tag']->setValue($date->format('d'));
        $form['produkt_leistung_monat']->setValue($date->format('m'));
        $form['produkt_leistung_jahr']->setValue($date->format('Y'));

        $form['produkt_leistung_grundgebuehr']->setValue($price);

        if (1 === preg_match('/^\d*$/', $laufzeit)) {
            $form['produkt_leistung_laufzeit']->setValue($laufzeit);
        }

        $form['produkt_leistung_aktiv']->tick();

        // nur zur protokollierung
        echo "updateBookedProductAtPurtel Values:\n";
        
        $printValues = [
            'produkt_leistung_tag',
            'produkt_leistung_monat',
            'produkt_leistung_jahr',
            'produkt_leistung_grundgebuehr',
            'produkt_leistung_laufzeit',
            'produkt_leistung_aktiv',
        ];

        foreach ($printValues as $key) {
            echo sprintf("\t%s: %s\n", $key, $form[$key]->getValue());
        }

        $client = new Client();

        $crawler = $client->submit($form);

        $dataPropperlySubmitted = false;
        $productFound = false;

        $crawler->selectButton('Ändern')->each(function ($node) use (&$bookedProduct, $date, $laufzeit, $price, &$productFound, &$dataPropperlySubmitted) {
            $element = $node->parents()->eq(2);

            $matches = null;

            preg_match('/^\[ID (\d+)\] (Produkt|Crossselling) \[(\d+)\] (.*)$/', $element->filter('tr > td')->first()->text(), $matches);

            if ((string) $bookedProduct['productId'] !== (string) $matches[3]) {
                return;
            }

            $productFound = true;

            $responseForm = $node->form();

            // is active?
            if ("1" !== (string) $responseForm['produkt_leistung_aktiv']->getValue()) {
                echo "updateBookedProductAtPurtel response - product not active - product: ".$bookedProduct['productId']."\n";

                return;
            }

            // correct date?
            if ($responseForm['produkt_leistung_tag']->getValue() !== $date->format('d')
                || $responseForm['produkt_leistung_monat']->getValue() !== $date->format('m')
                || $responseForm['produkt_leistung_jahr']->getValue() !== $date->format('Y')) {
                
                echo "updateBookedProductAtPurtel response - incorrect date - product: ".$bookedProduct['productId']."\n";

                return;
            }

            // correct laufzeit?
            $laufzeitAtPurtel = (string) $responseForm['produkt_leistung_laufzeit']->getValue();

            if (1 === preg_match('/^\d*$/', $laufzeit) && $laufzeit > 0) {
                if ($laufzeitAtPurtel !== (string) $laufzeit) {
                    echo sprintf("updateBookedProductAtPurtel response - incorrect laufzeit - product: %s - bei purtel %s - expected %s\n",
                        $bookedProduct['productId'],
                        $laufzeitAtPurtel,
                        $laufzeit
                    );

                    return;
                }
            } elseif (!empty($laufzeitAtPurtel) && $laufzeitAtPurtel !== '0') {
                echo sprintf("updateBookedProductAtPurtel response - incorrect laufzeit - product: %s - bei purtel %s - expected %s\n",
                    $bookedProduct['productId'],
                    $laufzeitAtPurtel,
                    $laufzeit
                );

                return;
            }

            unset($laufzeitAtPurtel); // no longer needed

            // correct price?
            if ((string) $price !== (string) $responseForm['produkt_leistung_grundgebuehr']->getValue()) {
                echo sprintf("updateBookedProductAtPurtel response - incorrect price - product: %s - price at purtel: %s - expected price: %s\n",
                    $bookedProduct['productId'],
                    $responseForm['produkt_leistung_grundgebuehr']->getValue(),
                    $price
                );

                return;
            }

            $dataPropperlySubmitted = true;
        });

        if (!$productFound) {
            echo "updateBookedProductAtPurtel response - product is gone - product: ".$bookedProduct['productId']."\n";
        }

        return $dataPropperlySubmitted;
    }

    /**
     * Update booked product at purtel
     * 
     * @return boolean
     */
    protected function deleteBookedProductAtPurtel($bookedProduct)
    {
        $form = $bookedProduct['form'];

        //unset($form['admin_crossselling_nebenstelle_loeschen']); // important! otherwise product will be delete at purtel

        //$form['produkt_leistung_aktiv']->tick();

        $client = new Client();

        $crawler = $client->submit($form);

        $productFound = false;

        $crawler->selectButton('Ändern')->each(function ($node) use (&$bookedProduct, &$productFound) {
            $element = $node->parents()->eq(2);

            $matches = null;

            preg_match('/^\[ID (\d+)\] (Produkt|Crossselling) \[(\d+)\] (.*)$/', $element->filter('tr > td')->first()->text(), $matches);

            if ((string) $bookedProduct['productId'] !== (string) $matches[3]) {
                return;
            }

            $productFound = true;
        });

        return !$productFound ? true : false;
    }

    /**
     * add crossselling product
     * 
     * @return boolean
     */
    protected function addCrossselling($crawler, $productId, \DateTime $date)
    {
        $client = new Client();

        $form = $crawler->selectButton('Hinzufügen')->form();

        $form['show_produkt_nebenstelle']->select($productId);
        $form['produkt_menge']->setValue(1);
        $form['produkt_anteilig_berechnen']->setValue(1);
        $form['produkt_tag']->setValue($date->format('d'));
        $form['produkt_monat']->setValue($date->format('m'));
        $form['produkt_jahr']->setValue($date->format('Y'));

        $responseCrawler = $client->submit($form);

        $bookedProductForm = null;

        $responseCrawler->selectButton('Ändern')->each(function ($node) use (&$bookedProductForm, &$productId) {
            $element = $node->parents()->eq(2);

            $matches = null;

            preg_match('/^\[ID (\d+)\] (Produkt|Crossselling) \[(\d+)\] (.*)$/', $element->filter('tr > td')->first()->text(), $matches);

            if ((string) $productId !== (string) $matches[3]) {
                return;
            }

            $bookedProductForm = $node->form();
        });

        if (null === $bookedProductForm) {
            return false;
        }

        return true;
    }

    /**
     * Get buchungen for all accounts by kundennummer_extern
     * 
     * @return array|null
     */
    protected function getBuchungen($clientId = null)
    {
        $timeZone = new \DateTimeZone('Europe/Berlin');
        $today = new \DateTime('now', $timeZone);

        $parameter = [
            'von_datum' => '20100101',
            'bis_datum' => $today->format('Ymd'),
            'erweitert' => '6',
        ];

        if (null !== $clientId) {
            //$parameter['kundennummer_extern'] = $clientId;
            $parameter['anschluss'] = $clientId;
        }

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'getbuchungen'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * 
     * 
     * @return array|null
     */
    protected function getStammdaten($anschluss = null)
    {
        $timeZone = new \DateTimeZone('Europe/Berlin');
        $today = new \DateTime('now', $timeZone);

        $parameter = [
            'periode' => $today->format('Ymd'),
            'erweitert' => '4',
        ];

        if (null !== $anschluss) {
            $parameter['anschluss'] = $anschluss;
        }

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'stammdatenexport'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * 
     */
    public function findProductIdByBuchung($buchung)
    {
        $textToProductName = [
            '/^{productName} Endabrechnung wegen Tarifwechsel\s*$/',
            '/^Basisentgelt {productName}\s*$/',
            '/^Einmalige Bereitstellung {productName}\s*$/',
            '/^Basisentgelt {productName} anteilig f..?r \d+ Tage?$/',
            '/^{productName}\s*$/',
            '/^{productName}$/',
            '/^\d x {productName}$/',
            '/^{productName} anteilig f..?r \d+ Tage?$/',
            '/^{productName} Gutschrift Restlaufzeit wegen Tarifwechsel$/',
            '/^\d x {productName} anteilig f..?r \d+ Tage?$/',
            '/^{productName} anteilige Gutschrift f..?r \d+ Tage?$/',
            '/^{productName} vom [\d\.-]+$/',
        ];

        foreach ($this->purtelMainProducts as $productId => $data) {
            foreach ($textToProductName as $pattern) {
                $productName = str_replace(
                    [
                        '+',
                        '/',
                        'ü',
                        'ä',
                        'ö',
                        'ß',
                    ],
                    [
                        '\+',
                        '\/',
                        '.',
                        '.',
                        '.',
                        '.',
                    ],
                    $data['name']
                );

                $pattern = str_replace('{productName}', $productName, $pattern);

                if (1 === preg_match($pattern, utf8_decode($buchung[3]))) {
                    return $productId;
                }
            }
        }

        foreach ($this->purtelCrossSellingProducts as $productId => $data) {
            foreach ($textToProductName as $pattern) {
                $productName = str_replace(
                    [
                        '+',
                        '/',
                        'ü',
                        'ä',
                        'ö',
                        'ß',
                    ],
                    [
                        '\+',
                        '\/',
                        '.',
                        '.',
                        '.',
                        '.',
                    ],
                    $data['name']
                );

                $pattern = str_replace('{productName}', $productName, $pattern);

                if (1 === preg_match($pattern, utf8_decode($buchung[3]))) {
                    return $productId;

                    break 2;
                }
            }
        }

        return null;
    }

    /**
     * create buchung at purtel
     */
    protected function createBuchung($buchung)
    {
        /*$updatePostData = [
            'anschluss' => $buchung[2],
            'verwendungszweck' => $buchung[3],
            'datum' => $buchung[7],
            'sap_buchungskonto' => $buchung[23],
        ];*/
        $updatePostData = $buchung;
        //$updatePostData['verwendungszweck'] = '-ZAP-'.$updatePostData['verwendungszweck'];

        /*if (isset($buchung[5])) {
            $updatePostData['betrag_haben'] = $buchung[5];
        } else {
            $updatePostData['betrag_soll'] = $buchung[4];
        }*/

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'setbuchung'
        );
        
        $url .= '&'.utf8_decode(http_build_query($updatePostData));

        echo "transmitted data to update buchung:\n\t".http_build_query($updatePostData)."\n";

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));

        $result = curl_exec($curl);

        curl_close($curl);

        $result = trim($result);
        $result = explode(';', $result);

        if ('+OK' === $result[0]) {
            return true;
            /* prüfen, ob das update tatsächlich noch notwendig ist
            $newBuchungen = $this->getBuchungen($updatePostData['anschluss']);
            $foundNewBuchung = null;

            foreach ($newBuchungen as $newBuchung) {
                if ($updatePostData['verwendungszweck'] === $newBuchung[3]) {
                    $foundNewBuchung = $newBuchung;

                    break;
                }
            }

            if (null === $foundNewBuchung) {
                echo "angelegte buchung wurde nicht wieder gefunden - ".$updatePostData['anschluss']."\n";

                return false;
            }

            $foundNewBuchung[7] = $updatePostData['datum'];
            $foundNewBuchung[3] = substr($foundNewBuchung[3], 5);

            $updateBuchung = [
                'anschluss' => $foundNewBuchung[2],
                'verwendungszweck' => $foundNewBuchung[3],
                'aendern' => 1,
                'id' => $foundNewBuchung[0],
                'datum' => $foundNewBuchung[7],
                'sap_buchungskonto' => $foundNewBuchung[23],
            ];

            return $this->updateBuchung($updateBuchung);
            */
        }

        var_dump('Failed to create buchung', $result);

        return false;
    }

    /**
     * 
     */
    public function updateBuchung($buchung)
    {
        $updatePostData = $buchung;

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'setbuchung'
        );
        
        $url .= '&'.utf8_decode(http_build_query($updatePostData));

        echo "transmitted data to update buchung:\n\t".http_build_query($updatePostData)."\n";

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));

        $result = curl_exec($curl);

        curl_close($curl);

        $result = trim($result);
        $result = explode(';', $result);

        if ('+OK' === $result[0]) {
            return true;
        }

        var_dump('Failed to update buchung', $result);

        return false;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/tools/import/CsvReader.php';
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';

        $this->purtelSuperUser = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
        $this->purtelSuperUserPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
        $this->purtelReseller = 'res100260';

        $danielCsv = new \CsvReader('/tmp/Mandant_100260_CS_mit_0.csv');

        $skipCustomer = [
        ];

        $doneData = [];

        foreach ($danielCsv->data as $line) {
            $purtelAccount = $line[$danielCsv->headlineReversed['rufnummer']];
            $date = $line[$danielCsv->headlineReversed['datum']];
            $active = $line[$danielCsv->headlineReversed['aktiv']];
            $clientId = $line[$danielCsv->headlineReversed['kundennummer_extern']];
            $mainProductId = $line[$danielCsv->headlineReversed['produktcreatorid']];
            $crossSellingProductId = $line[$danielCsv->headlineReversed['produkt_id']];

            if (empty($clientId)) {
                $stammdaten = $this->getStammdaten($purtelAccount);

                $clientId = $stammdaten[1][75];

                if (empty($clientId)) {
                    echo "hat keine externe kundennummer - ".$purtelAccount." - manuell\n";

                    continue;
                }
            }

            $acctualProduct = null;

            if (!empty($mainProductId)) {
                $acctualProduct = $mainProductId;
            } elseif (!empty($crossSellingProductId)) {
                $acctualProduct = $crossSellingProductId;
            } else {
                echo "no product id - continue - ".$clientId." - ".$purtelAccount."\n";
                continue;
            }

            echo $clientId." - ".$purtelAccount." - ".$acctualProduct."\n";

            if (in_array($clientId, $skipCustomer)) {
                echo "skipped..\n";

                continue;
            }

            if ('4578' === (string) $acctualProduct) {
                echo "is leistungsfrei - continue\n";
                continue;
            }

            if ('1' !== (string) $active) {
                echo "is inactive - continue\n";
                continue;
            }

            $doneDataIdentifier = $clientId.'-'.$acctualProduct;

            if (isset($doneData[$doneDataIdentifier])) {
                echo "customer and product already done - ".$doneDataIdentifier."\n";

                continue;
            }

            $doneData[$doneDataIdentifier] = true;

            $match = null;
            preg_match('/^(\d{4})(\d{2})(\d{2})$/', $date, $match);

            $dateY = $match[1];
            $dateM = $match[2];
            $dateD = $match[3];

            $expectedDate = new \DateTime($dateY.'-'.$dateM.'-'.$dateD);

////////////////////////////////////////
            if ((int) $dateY < 2018) {
                echo "date (".$date.") in vergangenheit - continue\n";
                continue;
            }
            if ((int) $dateM < 3) {
                echo "date (".$date.") in vergangenheit - continue\n";
                continue;
            }
////////////////////////////////////////

            $product = null;

            if (isset($this->purtelMainProducts[$acctualProduct])) {
                $product = $this->purtelMainProducts[$acctualProduct];
            } elseif (isset($this->purtelCrossSellingProducts[$acctualProduct])) {
                $product = $this->purtelCrossSellingProducts[$acctualProduct];
            }
            
            //$buchungen = $this->getBuchungen($clientId);
            $buchungen = $this->getBuchungen($purtelAccount);

            if (!is_array($buchungen)) {
                // hat keine anderen buchungen
                // -> create buchung

                $buchung = [
                    'anschluss' => $purtelAccount,
                    'verwendungszweck' => 'Basisentgelt '.$product['name'],
                    'datum' => $date.'000000',
                    'sap_buchungskonto' => $product['sap'],
                    'betrag_haben' => '0',
                    'basisentgelt_bruttobasiert' => '0',
                ];

                echo "create buchung for ".$buchung['verwendungszweck']."\n";

                if ($this->createBuchung($buchung)) {
                    echo "buchung angelegt\n";
                } else {
                    echo "buchung konnte nicht angelegt werden\n";
                }

                continue;
            }

            $buchungenHeadline = array_shift($buchungen);

            $buchungenForProduct = [];

            foreach ($buchungen as $buchung) {
                if (!empty($buchung[6])) {
                    continue;
                }

                $productId = $this->findProductIdByBuchung($buchung);

                if (null === $productId) {
                    continue;
                }

                if ((string) $productId === (string) $acctualProduct) {
                    $buchungenForProduct[] = $buchung;
                }
            }

            if (!empty($buchungenForProduct)) {
                echo "buchung(en) zu dem produkt (".$product['name'].") bereits vorhanden:\n";

                foreach ($buchungenForProduct as $buchung) {
                    $price = null;

                    if (!empty($buchung[4])) {
                        $price = $buchung[4];
                    } else if (!empty($buchung[5])) {
                        $price = $buchung[5];
                    } else if (!empty($buchung[24])) {
                        $price = $buchung[24];
                    } else if (!empty($buchung[25])) {
                        $price = $buchung[25];
                    } else if (!empty($buchung[26])) {
                        $price = $buchung[26];
                    } else if (!empty($buchung[27])) {
                        $price = $buchung[27];
                    }

                    $buchungsDate = substr($buchung[7], 0, 8);

                    echo sprintf("\t- %s - price: %s - date: %s\n",
                        $buchung[3],
                        null === $price ? 'stimmt' : $price,
                        (string) $date === (string) $buchungsDate ? 'stimmt' : $buchungsDate
                    );
                }

                echo "manuell - continue\n";

                continue;
            }

            $buchung = [
                'anschluss' => $purtelAccount,
                'verwendungszweck' => 'Basisentgelt '.$product['name'],
                'datum' => $date.'000000',
                'sap_buchungskonto' => $product['sap'],
                'betrag_haben' => '0',
                'basisentgelt_bruttobasiert' => '0',
            ];

            echo "create buchung for ".$buchung['verwendungszweck']."\n";

            if ($this->createBuchung($buchung)) {
                echo "buchung angelegt\n";
            } else {
                echo "buchung konnte nicht angelegt werden\n";
            }

            continue;
        }







        echo "completely done\n";
    }
}

?>