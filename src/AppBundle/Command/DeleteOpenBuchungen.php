<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Goutte\Client;

/**
 * 
 */
class DeleteOpenBuchungen extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('DeleteOpenBuchungen')
            ->setDescription('')
        ;
    }

    /**
     * Get buchungen for all accounts by kundennummer_extern
     * 
     * @return array|null
     */
    protected function getBuchungen($clientId = null)
    {
        $kundennummer_extern = $clientId;

        $timeZone = new \DateTimeZone('Europe/Berlin');
        $today = new \DateTime('now', $timeZone);

        $parameter = [
            'von_datum' => '20180501',
            'bis_datum' => '20180531',
            'erweitert' => '6',
        ];

        if (!empty($kundennummer_extern)) {
            $parameter['kundennummer_extern'] = $kundennummer_extern;
        }

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'getbuchungen'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * delete buchung at purtel
     * 
     * @return boolean
     */
    function deleteBuchung($buchung, $anschluss)
    {
        /*$updatePostData = [
            'anschluss' => $buchung[2],
            'verwendungszweck' => $buchung[3],
            'loeschen' => 1,
            'id' => $buchung[0],
        ];*/
        $updatePostData = [
            'anschluss' => $anschluss,
            'loeschen' => 1,
            'id' => $buchung,
        ];

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'setbuchung'
        );
        
        $url .= '&'.utf8_decode(http_build_query($updatePostData));

        echo "transmitted data to update buchung:\n\t".http_build_query($updatePostData)."\n";

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));

        $result = curl_exec($curl);

        curl_close($curl);

        $result = trim($result);
        $result = explode(';', $result);

        if ('+OK' === $result[0]) {
            return true;
        }

        var_dump('Failed to update buchung', $result);

        return false;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/tools/import/CsvReader.php';
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';

        $this->purtelSuperUser = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
        $this->purtelSuperUserPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
        $this->purtelReseller = 'res100260';

        
        $buchungenToDelete = [
'15547902' => '693719',
'15548000' => '693980',
'15548154' => '696805',
'15547936' => '693803',
'15548080' => '696695',
'15548082' => '696695',
'15548154' => '696805',
        ];

        foreach ($buchungenToDelete as $buchungsId => $anschluss) {
            $response = $this->deleteBuchung($buchungsId, $anschluss);

            echo $buchungsId." - ".($response? 'success' : 'failed')."\n";
        }

        echo "completely done\n";
    }
}

?>