<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;

/**
 * 
 */
class BulkMailerCommandForBackgroundProcessRunner extends ContainerAwareCommand
{
    /**
     * @var \Swift_Mailer
     */
    //protected $mailer = null;

    /**
     * @var array
     */
    protected $testmodeAdresses = null;

    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('BulkMailerCommandForBackgroundProcessRunner')
            ->setDescription('')
            ->addOption(
                'message-file',
                'm',
                InputOption::VALUE_REQUIRED,
                'message-file...'
            )
            ->addOption(
                'addresses-file',
                'a',
                InputOption::VALUE_REQUIRED,
                'addresses-file...'
            )
            ->addOption(
                'subject',
                's',
                InputOption::VALUE_REQUIRED,
                'subject...'
            )
            ->addOption(
                'testmode',
                't',
                InputOption::VALUE_OPTIONAL,
                'testmode...',
                null
            )
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     * 
     * @todo
     *      bei $message->embed(new \Swift_Image($data[1]
     *      sollte noch der message name aus dem html ausgelesen werden. summernote hängt die info dem tag an
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        if (!empty($input->getOption('testmode'))) {
            $this->testmodeAdresses = unserialize($input->getOption('testmode'));
        }

        require_once $container->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';
        require_once $container->get('kernel')->getRootDir().'/../web/tools/import/CsvReader.php';

        $csv = new \CsvReader($input->getOption('addresses-file'));

        $subject = $input->getOption('subject');

        $from = $this->getContainer()->getParameter('mailer_from');

        $messageText = file_get_contents($input->getOption('message-file'));

        $imageTags = [];

        preg_match_all('/<img.+?src=["\']([^"\']+)["\']/', $messageText, $imageTags, PREG_SET_ORDER);
    
        // try regular headline (casual files not from csvExport)
        $headline = $csv->headline;

        $emailAddressKey = array_search('(emailaddress)', $headline);

        if (false === $emailAddressKey) {
            $emailAddressKey = array_search('(customers.emailaddress)', $headline);
        }

        if (false === $emailAddressKey) {
            // regular headline failed
            // now try "second"-line as headline (comming from csvExport)

            $headline = array_shift($csv->data);

            $emailAddressKey = array_search('(emailaddress)', $headline);

            if (false === $emailAddressKey) {
                $emailAddressKey = array_search('(customers.emailaddress)', $headline);
            }

            if (false === $emailAddressKey) {
                echo "E-Mail-Address-Headline not found in csv - stopping\n";
                exit;
            }
        }

        $placeholders = $this->headlinesToPlaceholders($headline);

        foreach ($csv->data as $lineNum => $value){
            $emailAddress = trim($value[$emailAddressKey]);

            if (!empty($this->testmodeAdresses)) {
                $emailAddress = $this->testmodeAdresses;
            }

            if (empty($emailAddress)) {
                echo "empty emailaddress - line ".$lineNum."\n";

                continue;
            }

            if (!is_array($emailAddress)) {
                $emailAddress = [$emailAddress];
            }

            try {
                $message = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom($from)
                    ->setTo($emailAddress)
                    /*->setBody(
                        $this->twig->render('email/send-results.html.twig', [
                            'results' => $resultsToSend,
                        ]),
                        'text/html'
                    )*/
                    //->setBody($messageText, 'text/html')
                ;

                $messageTextTmp = $messageText;

                foreach ($imageTags as $key => $imageTag) {
                    $completeImageTag = $imageTag[0];
                    $srcValue = $imageTag[1];

                    if (1 === preg_match('/^https?:\/\//i', $srcValue)) {
                        continue;
                    }

                    $data = explode(';', $srcValue);
                    $filetype = preg_replace('/^data:/', '', $data[0]);
                    $data[1] = base64_decode(preg_replace('/^base64,/', '', $data[1]));

                    $new = str_replace(
                        $srcValue,
                        $message->embed(new \Swift_Image($data[1], null, $filetype)), // todo img.jpg aus dem img-tag extrahieren - summernote liefert das mit 
                        $completeImageTag
                    );

                    $messageTextTmp = str_replace($completeImageTag, $new, $messageTextTmp);
                }

                foreach ($placeholders as $columnKey => $pattern) {
                    $messageTextTmp = preg_replace($pattern, trim($value[$columnKey]), $messageTextTmp);
                }

                $message->setBody($messageTextTmp, 'text/html');
            } catch (\Swift_RfcComplianceException $exception) {
               echo "message sending to ".implode(";", $emailAddress)." failed! - ".$exception->getMessage()."\n";

                continue;
            }

            if ($this->getMailer()->send($message)) {
                echo "message send to ".implode(";", $emailAddress)." success\n";
            } else {
                echo "message sending to ".implode(";", $emailAddress)." failed!\n";
            }

            if (!empty($this->testmodeAdresses)) {
                break;
            }

            sleep(5); // delay for 5 seconds
        }

        echo "all done\n";

        unlink($input->getOption('message-file'));
        unlink($input->getOption('addresses-file'));
    }

    /**
     * Create new instance of Swift_Mailer.
     * Do not use symfonys service - $this->getContainer()->get('mailer') - here because it spools mails
     * and they only get send when script ends - but it never ends..
     */
    protected function getMailer() : \Swift_Mailer
    {
        /*if ($this->mailer instanceof \Swift_Mailer) {
            return $this->mailer;
        }*/

        $container = $this->getContainer();

        $transport = new \Swift_SmtpTransport($container->getParameter('mailer_host'), $container->getParameter('mailer_port'));

        if ($container->hasParameter('mailer_user')) {
            $transport->setUsername($container->getParameter('mailer_user'));
        }

        if ($container->hasParameter('mailer_password')) {
            $transport->setPassword($container->getParameter('mailer_password'));
        }

        if ($container->hasParameter('mailer_encryption')) {
            $transport->setEncryption($container->getParameter('mailer_encryption'));
        }

        //return $this->mailer = new \Swift_Mailer($transport);
        return new \Swift_Mailer($transport);
    }

    /**
     * Convert arrays from headline to placeholders
     */
    protected function headlinesToPlaceholders(array $headlines) : array
    {
        $placeholders = [];

        foreach ($headlines as $value) {
            $placeholders[] = sprintf('/\{\{ *%s *\}\}/',
                str_replace(
                    ['\\', '/', '[', ']', '.', '*', '?'],
                    ['\\\\', '\/', '\[', '\]', '\.', '\*', '\?'],
                    $value
                )
            );
        }

        return $placeholders;
    }
}

?>