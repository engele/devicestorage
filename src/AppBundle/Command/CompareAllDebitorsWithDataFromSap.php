<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Goutte\Client;

/**
 * 
 */
class CompareAllDebitorsWithDataFromSap extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('CompareAllDebitorsWithDataFromSap')
            ->setDescription('')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/tools/import/CsvReader.php';

        $db = $this->getContainer()->get('doctrine')->getManager()->getConnection();

        $this->purtelSuperUser = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
        $this->purtelSuperUserPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
        $this->purtelReseller = 'res100260';

        $csv = new \CsvReader('/tmp/all-debitors.csv');

        foreach ($csv->data as $key => $line) {
            $iban = trim($line[$csv->headlineReversed['IBAN']]);
            $debitor = trim($line[$csv->headlineReversed['Debitor']]);
            $manadatsreferez = trim($line[$csv->headlineReversed['Mandatsreferenz']]);
            $customerId = trim($line[$csv->headlineReversed['Alte Kontonummer']]);
            $kontoInhaber = trim($line[$csv->headlineReversed['Kontoinhaber']]);

            if (empty($customerId)) {
                // suche nach debitor?
                $q = $db->query("SELECT * FROM `customers` WHERE `bank_account_debitor` = '".$debitor."'");

                if ($q->rowCount() < 1) {
                    echo "customer not found - no customerId - also not found by debitor - ".$debitor."\n";

                    continue;
                } elseif ($q->rowCount() > 1) {
                    echo "customer not found - no customerId - found by debitor! but multiple times (".$q->rowCount().") - ".$debitor."\n";

                    continue;
                }

                $customer = $q->fetch(\PDO::FETCH_ASSOC);
            } else {
                $q = $db->query("SELECT * FROM `customers` WHERE `id` = '".$customerId."'");

                if ($q->rowCount() < 1) {
                    $q = $db->query("SELECT * FROM `customers` WHERE `bank_account_debitor` = '".$debitor."'");

                    if ($q->rowCount() < 1) {
                        echo "customer not found - not by customerId (".$customerId.") - also not found by debitor - ".$debitor."\n";

                        continue;
                    } elseif ($q->rowCount() > 1) {
                        echo "customer not found - not by customerId (".$customerId.") - found by debitor! but multiple times (".$q->rowCount().") - ".$debitor."\n";

                        continue;
                    }

                    $customer = $q->fetch(\PDO::FETCH_ASSOC);
                } elseif ($q->rowCount() > 1) {
                    echo "customer found - by customerId (".$customerId.") - but found multiple times (".$q->rowCount().")\n";

                    continue;
                } else {
                    $customer = $q->fetch(\PDO::FETCH_ASSOC);
                }
            }

            // compare data ...
            $fails = 0;

            if ($iban != $customer['bank_account_iban']) {
                $fails++;
                echo sprintf("- bank_account_iban - ".$customer['id']."\n-- %s :: %s\n",
                    $iban,
                    $customer['bank_account_iban']
                );
            }

            if ($debitor != $customer['bank_account_debitor']) {
                $fails++;
                echo sprintf("- bank_account_debitor - ".$customer['id']."\n-- %s :: %s\n",
                    $debitor,
                    $customer['bank_account_debitor']
                );
            }

            if ($manadatsreferez != $customer['bank_account_mandantenid']) {
                $fails++;
                echo sprintf("- bank_account_mandantenid - ".$customer['id']."\n-- %s :: %s\n",
                    $manadatsreferez,
                    $customer['bank_account_mandantenid']
                );
            }

            // try upper case and normal
            if ($kontoInhaber != strtoupper($customer['bank_account_holder_lastname']) && $kontoInhaber != $customer['bank_account_holder_lastname']) {
                $bank_account_holder_lastname = str_replace(['ä', 'ö', 'ü', 'ß', 'Ä', 'Ö', 'Ü'],['a', 'o', 'u', 'ss', 'A', 'O', 'U'], $customer['bank_account_holder_lastname']);
                // try without umlaute
                if ($kontoInhaber != strtoupper($bank_account_holder_lastname) && $kontoInhaber != $bank_account_holder_lastname) {
                    if (!empty($customer['bank_account_holder_lastname']) && !empty($kontoInhaber)) { // only complain if a name is set
                        $fails++;
                        echo sprintf("- bank_account_holder_lastname - ".$customer['id']."\n-- %s :: %s\n",
                            $kontoInhaber,
                            $customer['bank_account_holder_lastname']
                        );
                    }
                }
            }

            if (0 === $fails) {
                echo "Kunde passt - ".$customer['id']."\n";
            }
        }

        echo "all done\n";

    }
}

?>
