<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use AppBundle\Entity\User;

/**
 * 
 */
class MergeHelpersCliCommand extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('Merge-Helpers:cli')
            ->setDescription('')
            ->addArgument(
                'which',
                InputArgument::REQUIRED,
                'example|example|...'
            )
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        switch ($input->getArgument('which')) {
            case 'useraccounts-from-wkv':
                require_once __DIR__.'/../../../web/config.inc';
                require_once __DIR__.'/../../../web/_conf/database.inc';

                $query = $db->query('SELECT * FROM `kv_user`');

                $i = 0;

                while ($kvUser = $query->fetch_assoc()) {
                    //var_dump($kvUser);

                    $iv = substr ($SecKey.'#'.$kvUser['create'], -16);

                    $password = trim(openssl_decrypt($kvUser['passwd'], 'AES-256-CBC', $SecKey, OPENSSL_RAW_DATA, $iv));

                    //var_dump($password);

                    $user = new User();

                    $encodedPassword = $this->getContainer()->get('security.password_encoder')->encodePassword($user, $password);

                    $user->setUsername($kvUser['account'])
                        ->setPassword($encodedPassword)
                        ->setEmail($i++.'sven.siebrands@wisotel.com')
                        ->setFullName($kvUser['fullname'])
                        ->setGroupwareUsername($kvUser['egw_login'])
                        ->setRoles(array('ROLE_ADMIN'));

                    $em = $this->getContainer()->get('doctrine')->getManager();
                    $em->persist($user);
                    $em->flush();

                    echo "Merged (".$user->getUsername().")\n\n";
                }

                return;
        }

        throw new InvalidArgumentException('which');
    }
}

?>