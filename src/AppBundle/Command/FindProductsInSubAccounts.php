<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Goutte\Client;

/**
 * 
 */
class FindProductsInSubAccounts extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('FindProductsInSubAccounts')
            ->setDescription('')
        ;
    }

    /**
     * 
     * 
     * @return array|null
     */
    protected function getAccounts($clientId = null)
    {
        $parameter = [
            'erweitert' => 2,
        ];

        if (null !== $clientId) {
            $parameter['kundennummer_extern'] = $clientId;
        }

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'getaccounts'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * 
     * 
     * @return array|null
     */
    protected function getStammdaten($anschluss = null)
    {
        $timeZone = new \DateTimeZone('Europe/Berlin');
        $today = new \DateTime('now', $timeZone);

        $parameter = [
            'periode' => $today->format('Ymd'),
            'erweitert' => '10',
        ];

        if (null !== $anschluss) {
            $parameter['anschluss'] = $anschluss;
        }

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($this->purtelSuperUser),
            urlencode($this->purtelSuperUserPassword),
            'stammdatenexport'
        );
        
        $url .= '&'.utf8_decode(http_build_query($parameter));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $content = trim($result);
        
        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            return null;
            //throw new Exception('keine Buchungen gefunden');
        }

        $result = array_map(function ($line) {
            while (1 === preg_match('/(\{[^;]+);(.*\})/', $line)) {
                $line = preg_replace('/(\{[^;]+);(.*\})/', '$1,-,$2', $line);
            }

            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));

        return $result;
    }

    /**
     * 
     */
    protected function getProductsFromPurtel($purtelAccount)
    {
        // hole alle "gebuchten" Produkte des Kunden
        $bookedProducts = [];
       
        $client = new Client();
        
        $crawler = $client->request('GET', 'https://www2.purtel.com/res/'.$this->purtelReseller.'/index.php?username='
            .$this->purtelSuperUser.'&passwort='
            .$this->purtelSuperUserPassword.'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1&admin_ns_suchbegriff='
            .$purtelAccount.'&tab_id=3&marke=Zusatzleistungen');

        $crawler->selectButton('Ändern')->each(function ($node) use (&$bookedProducts) {
            $element = $node->parents()->eq(2);

            $matches = null;

            preg_match('/^\[ID (\d+)\] (Produkt|Crossselling) \[(\d+)\] (.*)$/', $element->filter('tr > td')->first()->text(), $matches);

            $form = $node->form();

            $bookedProduct = [
                'id' => $matches[1],
                'productId' => $matches[3],
                'name' => $matches[4],
                'form' => $form,
            ];

            $bookedProducts[$bookedProduct['productId']] = $bookedProduct;
        });

        return [
            'bookedProducts' => $bookedProducts,
            'usedCrawler' => $crawler,
        ];
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * 
     * @throws InvalidArgumentException
     * 
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/tools/import/CsvReader.php';
        require_once $this->getContainer()->get('kernel')->getRootDir().'/../web/vendor/wisotel/configuration/Configuration.php';

        //$this->purtelSuperUser = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
        //$this->purtelSuperUserPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
        $this->purtelSuperUser = '673845'; // jle account
        $this->purtelSuperUserPassword = 'e400b84f4752';
        $this->purtelReseller = 'res100260';

        $accounts = $this->getAccounts();
        $accountsHeadline = array_shift($accounts);

        foreach ($accounts as $account) {
            $stammdaten = $this->getStammdaten($account[1]);

            echo sprintf("Prüfe %s :\n", $account[1]);
            
            if (!empty($stammdaten)) {
                if (empty($stammdaten[1][123])) {
                    // ist Hauptaccount, alles ok
                    echo "\tist Hauptaccount, nichts zu tun\n";

                    continue;
                }

                $crossSellings = null;

                // pruefe, ob cp ein aktiv ist
                preg_match_all('/(\d+)=/', $stammdaten[1][124], $crossSellings);

                if (isset($crossSellings[1]) && !empty($crossSellings[1])) {
                    // hat crosssellings, darf nicht sein
                    echo "\that aktiv(e) Cross-Selling-Produkte\n";

                    continue;
                }

                // hole von gui um zu sehen, ob eventuell inaktive zusatzleistungen da sind
                $allProducts = $this->getProductsFromPurtel($account[1]);

                if (!empty($allProducts['bookedProducts'])) {
                    echo "\that aktive oder inaktive Produkte\n";

                    continue;
                }

                echo "\tkeine Produkte gefunden - alles ok!\n";
            } else {
                echo "\t ist wohl Testanschluss\n";
            }
        }

        echo "completely done\n";
    }
}

?>