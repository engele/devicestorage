<?php

/**
 * @deprecated
 *      Database schema and usage has changed, do not use this file anymore
 */

require __DIR__.'/../_conf/database.inc';

/*$networks = [];

$query = $db->query("SELECT * FROM `networks`");

while ($data = $query->fetch_assoc()) {
    $networks[] = $data;
}*/


$query = $db->query(
    "SELECT 
    c.`id` as 'c.id', 
    c.`port_amount` as 'c.port_amount', 
    c.`name` as 'c.name', 
    lkp.`id` as 'lkp.id', 
    lkp.`kvz_name` as 'lkp.kvz_name', 
    lkp.`pin_amount` as 'lkp.pin_amount', 
    l.`network_id` as 'l.network_id', 
    l.`id` as 'l.id', 
    l.`name` as 'l.name', 
    n.`id_string` as 'n.id_string', 
    n.`name` as 'n.name' 
    FROM `cards` c 
    LEFT JOIN `lsa_kvz_partitions` lkp ON lkp.`location_id` = c.`location_id` 
    INNER JOIN `locations` l ON l.`id` = c.`location_id`
    INNER JOIN `networks` n ON n.`id` = l.`network_id`
    ORDER BY n.`id_string` ASC, l.`name` ASC, lkp.`kvz_name` ASC, c.`name` ASC
    "
);


/*
locations
| id | network_id | name                                                         | kvz_prefix           | cid_suffix_start | cid_suffix_end | vlan_pppoe | vlan_iptv | vlan_acs | vectoring | logon_port | dslam_ip_adress | logon_id | logon_pw      | dslam_conf | dslam_type | vlan_pppoe_local | vlan_iptv_local | vlan_acs_local | acs_type | dslam_ip_real | dslam_router | netz | vlan_dcn | prozessor | dslam_software_release | pppoe_type |


networks
| id | id_string | name                                         | partner | network_ready | area_code | fix_cost | var_cost | var_percent | no_stat


cards
| id  | port_amount | name                                    | card_type_id | location_id | Line      | ip_dslam | dslam_name


lsa_kvz_partitions
| id | location_id | kvz_name                 | pin_amount | dpbo | default_VLAN | VLAN1 | VLAN2 | VLAN3 | esel_wert


defective_card_ports
| id | card_id | value


defective_lsa_pins
| id  | lsa_kvz_partition_id | value
*/


echo '"Netz (id)";"Netz (name)";"DSLAM Standort";"KVZ";"KVZ (pins)";"KVZ (pins defekt)";"Karte (unique id)";"Karte";"Karte (ports)";"Karte (ports defekt)";"Kunden auf Karte (gesamt)";"Kunden auf Karte (aktiv)";"Kunden auf Karte (nur gepatched)"'."\n";

while ($data = $query->fetch_assoc()) {
    //var_dump($data);
    $defectiveCardQ = $db->query("SELECT COUNT(`id`) as 'count' FROM `defective_card_ports` WHERE `card_id` = ".$data['c.id']);
    $defectiveCard = $defectiveCardQ->fetch_row();
    $defectiveCard = $defectiveCard[0];

    $defectiveLsa = 0;

    if (null !== $data['lkp.id']) {
        $defectiveLsaQ = $db->query("SELECT COUNT(`id`) as 'count' FROM `defective_lsa_pins` WHERE `lsa_kvz_partition_id` = ".$data['lkp.id']);
        $defectiveLsa = $defectiveLsaQ->fetch_row();
        $defectiveLsa = $defectiveLsa[0];
    }

    $customersCounter = [
        'active' => 0,
        'onlyPatched' => 0,
        'total' => 0,
    ];

    $customersQ = $db->query("SELECT `id`, `connection_activation_date`, `patch_date`, `version` 
        FROM `customers` 
        WHERE `card_id` = ".$data['c.id']
    );

    while ($customer = $customersQ->fetch_assoc()) {
        if (!empty($customer['version'])) {
            continue;
        }

        $customersCounter['total']++;

        if (!empty($customer['connection_activation_date'])) {
            $customersCounter['active']++;
        } elseif (!empty($customer['patch_date'])) {
            $customersCounter['onlyPatched']++;
        }
    }

    echo sprintf('"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s"',
        $data['n.id_string'],
        $data['n.name'],
        $data['l.name'],
        $data['lkp.kvz_name'],
        $data['lkp.pin_amount'],
        $defectiveLsa,
        $data['c.id'],
        $data['c.name'],
        $data['c.port_amount'],
        $defectiveCard,
        $customersCounter['total'],
        $customersCounter['active'],
        $customersCounter['onlyPatched']
    )."\n";
}