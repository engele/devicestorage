<html lang="de" xmlns="http://www.w3.org/1999/xhtml" xml:lang="de">
<head>
  <title>Verschiebt bestehende Purtelaccounts</title>
</head>
<body>
<?php
  require_once '../config.inc';
  require_once $StartPath.'/_conf/database.inc';
  require_once $StartPath.'/customer/_conf/database.inc';
  require_once $StartPath.'/vendor/wisotel/configuration/Configuration.php';

  $username       = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
  $password       = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
  
  $action         = 'getaccounts';        // "kundennummer_extern";"anschluss";"passwort";"gesperrt"
  $action2        = 'getnumbers';         // "anschluss";"rufnummer";"art";"kundennummer_extern"
  $header         = array();
  $account        = array();
  $telefon        = array();
  
  $db_cust_result = $db->query($sql['customers']);

  while ($customer = $db_cust_result->fetch_assoc()) {
    if ($customer['version'] && $customer['purtel_use_version']) $customer['clientid'] .= '_'.$customer['version'];
    if ($customer['clientid']) {
      // get purtel accounts
      $PurtelUrl  = 'https://ipcom.purtel.com/index.php?super_username='.urlencode($username).'&super_passwort='.urlencode($password).'&action='.urlencode($action).'&erweitert=3&kundennummer_extern='.urlencode($customer['clientid']);
      $curl = curl_init ($PurtelUrl);
      $curl_options = array (
        CURLOPT_URL             => $PurtelUrl,
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
      );
      curl_setopt_array($curl, $curl_options);
      $result = curl_exec($curl);
      $resultArray = split ("\n", $result);
      $header = str_getcsv (array_shift($resultArray), ";");
      echo "<hr>".$customer['clientid'].' | '.$customer['id'].' | '.$customer['firstname'].' | '.$customer['lastname'].' | <br>';
      // Purtel Account
      $master = 1;
      foreach ($resultArray as $result) {
        if ($result) {
          $hasTel = 0;
          list($account['kundennummer_extern'], $account['anschluss'], $account['sipusername'], $account['passwort'], $account['gesperrt'],$account['produkt'],$account['callerid'],$account['trunk'],$account['trunkformat'],$account['laenge'],$account['kopfnummer'],$account['fueller'],$account['stellen']) = str_getcsv ($result, ";");
          echo $account['anschluss']."<br>";
          if ($account['trunk'] != 1) {
            // get purtel telefonnumbers pro anschluss
            $PurtelUrl  = 'https://ipcom.purtel.com/index.php?super_username='.urlencode($username).'&super_passwort='.urlencode($password).'&action='.urlencode($action2).'&kundennummer_extern='.urlencode($customer['clientid']).'&anschluss='.urlencode($account['anschluss']);
            $curl = curl_init ($PurtelUrl);
            $curl_options = array (
              CURLOPT_URL             => $PurtelUrl,
              CURLOPT_HEADER          => false,
              CURLOPT_RETURNTRANSFER  => true,
              CURLOPT_SSL_VERIFYPEER  => false,
            );
            curl_setopt_array($curl, $curl_options);
            $result = curl_exec($curl);
            $resultArray = split ("\n", $result);
            array_shift($resultArray);
            foreach ($resultArray as $result) {
              if ($result) {
                list($telefon['anschluss'], $telefon['rufnummer'],  $telefon['art'], $telefon['kundennummer_extern']) = str_getcsv ($result, ";");
                echo $telefon['rufnummer'].' | ';
                $telefon['rufnummer'] ='0'.substr($telefon['rufnummer'],4); 
                $hasTel = 1;
                $sql = "SELECT count(*) as count FROM purtel_account WHERE purtel_login = '".$account['anschluss']."' AND nummer = '".$telefon['rufnummer']."'";
                $db_result = $db->query($sql);
                $tel_exist = $db_result->fetch_assoc();
                $db_result->free_result();
                if ($tel_exist['count'] == 0) {
                  $sql_insert  = "INSERT INTO purtel_account SET \n";
                  $sql_insert .= "purtel_login = '".$account['anschluss']."',\n";
                  $sql_insert .= "purtel_password = '".$account['passwort']."',\n";
                  $sql_insert .= "master = '".$master."',\n";
                  $sql_insert .= "porting = '0',\n";
                  $sql_insert .= "nummer = '".$telefon['rufnummer']."',\n";
                  $sql_insert .= "typ = '".$telefon['art']."',\n";
                  $sql_insert .= "cust_id = '".$customer['id']."'\n";
                  $db_save_result = $db->query($sql_insert);
                  echo "hinzugefügt<br>";
                } else {
                  echo "vorhanden<br>";
                }
              }
            }
          }
          if (!$hasTel) {
            $sql = "SELECT count(*) as count FROM purtel_account WHERE purtel_login = '".$account['anschluss']."'";
            $db_result = $db->query($sql);
            $acc_exist = $db_result->fetch_assoc();
            $db_result->free_result();
            if ($acc_exist['count'] == 0) {
              $sql_insert  = "INSERT INTO purtel_account SET \n";
              $sql_insert .= "purtel_login = '".$account['anschluss']."',\n";
              $sql_insert .= "purtel_password = '".$account['passwort']."',\n";
              $sql_insert .= "master = '".$master."',\n";
              $sql_insert .= "porting = '0',\n";
              $sql_insert .= "nummer = '',\n";
              $sql_insert .= "typ = '',\n";
              $sql_insert .= "cust_id = '".$customer['id']."'\n";
              $db_save_result = $db->query($sql_insert);
              echo $account['anschluss']." | hinzugefügt<br>";
            } else {
              echo $account['anschluss']." | vorhanden<br>";
            }
          }
          $master = 0;
        }
      }
    }
  }
  $db_cust_result->close();
?>
</body>
</html>
