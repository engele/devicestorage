<?php

require __DIR__.'/../_conf/database.inc';
require_once __DIR__.'/../vendor/wisotel/configuration/Configuration.php';
require_once __DIR__.'/../customer/Lib/PppoeUsername/PppoeUsername.php';

$purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

//$q = $db->query("SELECT * FROM `customers` WHERE `id` BETWEEN 5382 AND 5505");
//$q = $db->query("SELECT * FROM `customers` WHERE (`id` BETWEEN 5802 AND 6246) OR (`id` IN (12209, 12705, 12834, 13429, 13605, 14899, 15156, 18952))");
//$q = $db->query("SELECT * FROM `customers` WHERE `id` BETWEEN 6296 AND 7552");
$q = $db->query("SELECT * FROM `customers` WHERE `id` BETWEEN 7588 AND 7967");

$ignoreCustomer = [
];

while ($customer = $q->fetch_assoc()) {
    if (in_array($customer['clientid'], $ignoreCustomer) || in_array($customer['id'], $ignoreCustomer)) {
        echo "skipped ".$customer['id']."\n";

        continue;
    }

    $purtelQ = $db->query("SELECT * FROM `purtel_account` WHERE `cust_id` = '".$customer['id']."' AND `master` = 1");

    if ($purtelQ->num_rows !== 1) {
        var_dump('unpassende anzahl purtel accounts', $customer['id']);

        continue;
    }

    $purtelMaster = $purtelQ->fetch_assoc();


    echo "Kunde: ".$customer['id']."\n";

    if (empty($customer['mac_address'])) {
        echo "empty mac - ".$customer['customer_id']." - ".$customer['clientid']."\n";

        continue;
    }

    $match = preg_match('/([0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2})|([0-9a-f]{12})/i', $customer['mac_address']);

    if (1 !== $match) {
        echo "invalid mac - ".$customer['id']."\n";

        continue;
    }

    //var_dump($purtelMaster['purtel_login']);
    //continue;


    // create radius account

    $post = [
        'StartPath' => '%2Fvar%2Fwww%2Fikv%2Fweb',
        'SelPath' => 'customer',
        'StartURL' => 'http%3A%2F%2Fikv1.com-in.net',
        'action' => 'activate',
        'type' => 'both',
        'acs' => json_encode([
            'custId' => $customer['id'],
            'clientId' => $customer['clientid'],
            'macAddress' => $customer['mac_address'],
            'password' => $customer['password'],
            'version' => $customer['version'],
            'area_code' => '0841',
        ], JSON_FORCE_OBJECT),
    ];

    $curl = curl_init('http://ikv1.com-in.net/customer/ajaxAcs.php');

    curl_setopt_array($curl, array(
        CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4,
        //CURLOPT_HEADER          => true,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => $post,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $result = trim($result);
    
    //var_dump($result);

    if (false !== strpos($result, 'Code: 5')) {
        var_dump($result);

        continue;
    }


        

    //die();
}


