<?php

$timeZone = new \DateTimeZone('Europe/Berlin');

$startAt = new \DateTime('2018-09-12 00:10:00', $timeZone);
$now = new \DateTime('now', $timeZone);

echo sprintf("starting at: %s - Now is: %s\n",
    $startAt->format('d.m.Y H:i:s'),
    $now->format('d.m.Y H:i:s')
);

while (($now = new \DateTime('now', $timeZone)) < $startAt) {
    echo sprintf("sleeping... %s - Now is: %s\n", $startAt->format('d.m.Y H:i:s'), $now->format('d.m.Y H:i:s'));
    sleep(600);
}

require __DIR__.'/../_conf/database.inc';
require_once __DIR__.'/../vendor/wisotel/configuration/Configuration.php';
require_once __DIR__.'/../customer/Lib/PppoeUsername/PppoeUsername.php';

$purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

//$q = $db->query("SELECT * FROM `customers` WHERE `id` BETWEEN 16126 AND 16431");
$q = $db->query("SELECT * FROM `customers` WHERE `id` IN(8566, 8566, 8566, 8566, 8566, 8567, 8567, 6279, 6279, 6279, 7558, 8570, 8570, 8570, 8570, 8570, 8570, 8572, 9824, 8573, 8576, 8576, 8576, 8576, 8577, 8577, 8577, 8577, 8577, 8578, 8578, 8579, 8580, 8580, 8581, 8581, 8581, 8581, 8581, 8581, 8582, 6280, 6280, 6280, 8584, 8588, 8588, 8588, 8588, 8588, 8588, 8588, 8588, 8588, 8588, 7241, 5495, 5495, 5495, 8590, 8591, 8591, 8591, 8591, 8591, 8591, 8591, 8591, 8592, 8594, 8595, 8596, 8597, 8597, 8597, 8597, 8597, 8597, 7556, 7556, 7556, 8599, 7937, 7937, 7937, 7937, 7937, 7937, 7937, 7937, 8420, 8420, 8420, 8420, 8420, 8420, 8420, 8601, 8602, 8603, 8603, 8603, 8603, 8603, 8603, 8603, 8603, 8603, 8604, 8604, 8604, 8604, 8604, 8604, 8604, 8604, 8604, 5500, 5500, 5500, 5500, 5500, 5500, 5500, 5500, 5500, 8605, 8605, 8605, 8606, 8607, 8609, 8610, 8611, 8612, 8612, 8086, 8613, 8613, 8613, 8613, 8613, 8613, 8613, 8613, 8613, 8614, 5501, 5769, 5769, 8616, 8616, 8616, 8618, 8618, 8618, 8619, 8620, 8621, 8622, 8622, 8622, 8623, 8624, 8624, 8624, 8625, 8625, 8625, 8625, 8625, 8625, 8625, 8625, 8625, 8626, 8627, 8627, 8627, 8627, 8627, 8627, 8627, 8627, 8627, 8569, 8569, 8569, 8569, 8569, 8569, 8569, 5505, 8628, 8628, 8628, 8628, 8628, 8628, 8628, 8628, 8628, 8630, 8631, 8631, 8631, 8632, 8633, 8633, 8633, 7559, 7559, 7559, 7559, 7559, 7559, 7559, 7559, 7559, 8634, 8634, 8634, 8421, 8421, 8421, 8421, 8421, 8635, 8636, 8636, 8636, 8011, 8637, 8638, 8639, 7560, 8082, 8640, 8640, 8640, 8640, 8640, 8640, 8640, 8640, 8640, 8641, 8642, 8643, 8643, 8644, 8644, 8644, 8644, 8644, 8644, 8644, 8644, 8646, 8646, 8646, 8646, 8646, 8646, 8646, 8646, 8646, 8647, 8647, 8647, 8648, 8648, 8648, 8649, 8650, 8652, 8653, 8654, 8655, 8655, 8655, 8655, 8655, 8655, 8655, 8655, 8655, 8655, 8655, 8656, 8656, 8656, 8656, 8656, 8656, 8656, 8656, 8039, 8039, 8039, 8657, 8657, 8657, 8657, 8657, 8657, 8658, 8658, 8658, 8658, 8084, 8659, 8659, 8659, 8659, 8659, 8659, 8659)");
//$q = $db->query("SELECT * FROM `customers` WHERE (`id` BETWEEN 5802 AND 6246) OR (`id` IN (12209, 12705, 12834, 13429, 13605, 14899, 15156, 18952))");
//$q = $db->query("SELECT * FROM `customers` WHERE `id` BETWEEN 6296 AND 7552 AND `location_id` = 25"); // Cluster 26

$ignoreCustomer = [
];

while ($customer = $q->fetch_assoc()) {
    if (in_array($customer['clientid'], $ignoreCustomer) || in_array($customer['id'], $ignoreCustomer)) {
        echo "skipped ".$customer['id']."\n";

        continue;
    }

    $purtelQ = $db->query("SELECT * FROM `purtel_account` WHERE `cust_id` = '".$customer['id']."' AND `master` = 1");

    if ($purtelQ->num_rows !== 1) {
        var_dump('unpassende anzahl purtel accounts', $customer['id']);

        continue;
    }

    $purtelMaster = $purtelQ->fetch_assoc();


    $purtelQ = $db->query("SELECT * FROM `purtel_account` WHERE `cust_id` = '".$customer['id']."'");
    
    $purtelAccounts = [];
    
    while ($rn = $purtelQ->fetch_assoc()) {
        if (!empty($rn['nummer'])) {
            $purtelAccounts[$rn['nummer']] = $rn;
        }
    }


    $postData = [
        'kundennummer_extern' => $customer['clientid'],
        //'kundennummer_extern' => '50000.18.0225',
    ];

    if ($customer['purtel_use_version'] == 1 && $customer['version'] > 0) {
        $postData['kundennummer_extern'] += '_'.$customer['version'];
    }




    echo "Kunde: ".$customer['id']."\n";

    
    


    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'getnumbers'
    );
    
    $url .= '&'.utf8_decode(http_build_query($postData));

    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        //CURLOPT_URL             => $PurtelUrl1,
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $result = trim($result);
    //echo $result."\n";

    $result = explode("\n", $result);

    /*
    "anschluss";"rufnummer";"art";"kundennummer_extern"
    "675889";"004984114909889";"1";"50000.18.0225"
    "675889";"004984114901173";"1";"50000.18.0225"
    */

    $foundCounter = 0;

    foreach ($result as $key => $line) {
        if (0 === $key) {
            continue;
        }

        $line = str_getcsv($line, ';', '"');
        $result[$key] = $line;

        if (isset($purtelAccounts[$line[1]])) {
            $foundCounter++;
            continue;
        }

        $rn = preg_replace('/^0049/', '0', $line[1]);

        if (isset($purtelAccounts[$rn])) {
            $foundCounter++;
            continue;
        }

        //var_dump($line);
    }

    if ($foundCounter === count($result) - 1) {
        //echo "alles da\n";

        $post = [
            'StartPath' => '%2Fvar%2Fwww%2Fikv%2Fweb',
            'SelPath' => 'customer',
            'StartURL' => 'http%3A%2F%2Fikv1.com-in.net',
            'CustId' => $customer['id'],
            'CustClientid' => $postData['kundennummer_extern'],
        ];

        $curl = curl_init('http://ikv1.com-in.net/customer/ajaxPurtelPhone.php');

        curl_setopt_array($curl, array(
            CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4,
            //CURLOPT_HEADER          => true,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $post,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $result = trim($result);
        
        if (false === strpos($result, 'status":0')) {
            var_dump($result);

            continue;
        }

    } else {
        echo "fehlt\n";
        var_dump($result);
    }
}


