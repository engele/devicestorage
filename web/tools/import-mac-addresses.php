<?php

/**
 * Import customer-data from Dimari and Telefonica
 * 
 * Nur die ersten 129 Kunden
 */

define('NETWORK_ID', 35);
define('NETZ', 50000);
define('CLIENT_ID_YEAR', 17);
$aaaaaaaaaaaaaa = [
];
/**
 * Read contents from csv-file
 *
 * @param string $filename
 *
 * @throws InvalidArgumentException
 *
 * @return array
 */
function readCsvFile($filename)
{
    if (!file_exists($filename) || !is_readable($filename)) {
        throw new InvalidArgumentException('Can not open file for reading');
    }

    $content = file_get_contents($filename);

    if (!is_string($content) && !empty($content)) {
        throw new InvalidArgumentException('invalid file');
    }

    $delimiter = null;

    foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
        if (false !== strpos($content, $delimiter_)) {
            $delimiter = $delimiter_;
            break;
        }
    }

    if (null == $delimiter) {
        throw new InvalidArgumentException('can not use file');
    }

    return array_map(function ($line) {
        return str_getcsv($line, ';');
    }, explode($delimiter, $content));
}

//$dimariCsv = readCsvFile('/tmp/CL28_Stammdaten_geprueft.csv');
$dimariCsv = readCsvFile('/tmp/cluster33.csv');
$telefonicaCsv = readCsvFile('/tmp/telefonica.csv');

$dimariHeader = array_shift($dimariCsv);
$dimariHeaderReversed = [];
$telefonicaHeader = array_shift($telefonicaCsv);
$telefonicaHeaderReversed = [];

foreach ($dimariHeader as $column => $name) {
    $dimariHeaderReversed[$name] = $column;
}
foreach ($telefonicaHeader as $column => $name) {
    $telefonicaHeaderReversed[$name] = $column;
}

// strip last empty line from csv-data
if (count(end($dimariCsv)) !== count($dimariHeader)) {
    array_pop($dimariCsv);
}
if (count(end($telefonicaCsv)) !== count($telefonicaHeader)) {
    array_pop($telefonicaCsv);
}


$dataSortedByCustomerId = [];

// sort by kundenummer
foreach ($dimariCsv as $key => $line) {
    $customerId = trim($line[$dimariHeaderReversed['KUNDENNUMMER']]);

    if (!isset($dataSortedByCustomerId[$customerId])) {
        $dataSortedByCustomerId[$customerId] = [];
    }

    $dataSortedByCustomerId[$customerId][$key] = & $dimariCsv[$key];
}


require __DIR__.'/../_conf/database.inc';

class Product
{
    public $id;
    public $identifier;
    public $name;
    public $itemCategory = null;
    public $defaultPriceNet = null;

    public static function createFromArray($data)
    {
        $mapping = [
            'id' => 'id',
            'identifier' => 'identifier',
            'name' => 'name',
            'item_category' => 'itemCategory',
            'default_price_net' => 'defaultPriceNet',
        ];

        $product = new self();

        foreach ($mapping as $dataKey => $objectParameter) {
            if (!isset($data[$dataKey])) {
                continue;
            }

            $product->{$objectParameter} = $data[$dataKey];
        }
        
        return $product;
    }
}

class PurtelAccount
{
    public $purtelLogin;
    public $purtelPassword;
    public $master;
    public $porting;
    public $nummer;
    public $nummernblock;
    public $areaCode;
    public $typ = 1;
    public $customerId;
    public $phoneBookInformation;
    public $phoneBookDigitalMedia;
    public $phoneBookInverseSearch;
    public $itemizedBill;
    public $itemizedBillAnonymized;
    public $itemizedBillShorted;
}

$products = [];

$getCustomerProductsFromCompleteDataForCustomer = function ($completeDataForCustomer) use (&$dimariHeaderReversed, &$products)
{
    global $db;

    $customerProducts = [];

    foreach ($completeDataForCustomer as $key => $line) {
        $productId = trim($line[$dimariHeaderReversed['PRODUCT_ID']]);

        if (empty($productId)) {
            continue;
        }

        $name = trim($line[$dimariHeaderReversed['PRODUKTBESCHREIBUNG']]);
        $itemCategory = trim($line[$dimariHeaderReversed['KATEGORIE']]);
        $itemCategory = empty($itemCategory) ? null : $itemCategory;
        $defaultPriceNet = trim($line[$dimariHeaderReversed['Preis netto']]);
        $defaultPriceNet = empty($defaultPriceNet) ? null : (float) str_replace(",", ".", $defaultPriceNet);

        if (isset($products[$productId])) { // already seen this product?
            $product = $products[$productId];
        } else {
            $productQuery = $db->query("SELECT * FROM `products` WHERE `identifier` = '".$productId."'");

            if (1 === $productQuery->num_rows) { // already have product in database?
                $product = Product::createFromArray($productQuery->fetch_assoc());
            } else {
                // is new product
                $product = Product::createFromArray([
                    'identifier' => $productId,
                    'name' => $name,
                    'item_category' => $itemCategory,
                    'default_price_net' => $defaultPriceNet,
                ]);
            }

            $products[$productId] = $product;
        }

        $activationDate = trim($line[$dimariHeaderReversed['Prod.Aktivierung']]);
        $activationDate = empty($activationDate) ? null : new \DateTime($activationDate);

        $deactivationDate = trim($line[$dimariHeaderReversed['Prod.Deaktivierung']]);
        $deactivationDate = empty($deactivationDate) ? null : new \DateTime($deactivationDate);

        $customerProducts[$key] = [
            'product' => $product,
            'priceNet' => $defaultPriceNet,
            'activationDate' => $activationDate,
            'deactivationDate' => $deactivationDate,
        ];
    }

    return $customerProducts;
};

/**
 * Find lines with specific column content in telefonica-data
 * 
 * @return array|null
 */
$findInTelefonica = function ($matching) use (&$telefonicaCsv, &$telefonicaHeaderReversed)
{
    $matches = null;

    foreach ($matching as $column => $matchingValue) {
        if (null === $matches) {
            $matches = [];

            foreach ($telefonicaCsv as $key => $line) {
                if ($line[$telefonicaHeaderReversed[$column]] === $matchingValue) {
                    $matches[$key] = $line;
                }
            }
        } elseif (count($matches) > 0) {
            foreach ($matches as $key => $line) {
                if ($line[$telefonicaHeaderReversed[$column]] !== $matchingValue) {
                    unset($matches[$key]);
                }
            }
        } else {
            return null;
        }
    }

    return count($matches) > 0 ? $matches : null;
};

$getCustomerPurtelAccountsFromCompleteDataForCustomer = function ($completeDataForCustomer) use (&$dimariHeaderReversed, $findInTelefonica, $getCustomerProductsFromCompleteDataForCustomer, $aaaaaaaaaaaaaa)
{
    $customerPurtelAccounts = [];

    foreach ($completeDataForCustomer as $key => $line) {
        $subscriberId = trim($line[$dimariHeaderReversed['SUBSCRIBER_ID']]);
        $telNr = trim($line[$dimariHeaderReversed['TEL_RUFNR']]);

        if (!empty($subscriberId)) {
            $telefonicaRn = $subscriberId;
        } else if (!empty($telNr)) {
            $telefonicaRn = $telNr;
        } else {
            continue; // no $subscriberId and no $telNr -> no need for purtel-account
        }

        $purtelAccount = new PurtelAccount();

        $telefonicaRnOrginal = $telefonicaRn;
        $telefonicaRn = preg_replace('/[^0-9]/', '', $telefonicaRn);
        $telefonicaRn = preg_replace('/^0/', '', $telefonicaRn);

        $matchedLines = $findInTelefonica([
            'rn' => $telefonicaRn,
        ]);

        if (!empty($matchedLines)) { // has exactly 1 item
            $purtelAccount->purtelLogin = $telefonicaRn;
        } elseif (empty($telNr)) {
            //echo sprintf("warning - no rufnummer and not found in telefonica - %s", $telefonicaRnOrginal)."\n";
            if (in_array($telefonicaRnOrginal, $aaaaaaaaaaaaaa)) {
                echo "+ rn : ".$telefonicaRn."\n";
                $purtelAccount->purtelLogin = $telefonicaRn;
            }
        }

        $purtelAccount->nummer = preg_replace('/[^0-9]/', '', $telNr);

        $areaCode = explode('-', $telNr);

        if (count($areaCode) === 2) {
            $purtelAccount->areaCode = $areaCode[0];
        }

        if (empty($customerPurtelAccounts)) { // set first account as "master"
            $purtelAccount->master = 1;
        }

        $mapping = [
            'Auskunft' => 'phoneBookInformation',
            'Digitale Medien' => 'phoneBookDigitalMedia',
            'Inverssuche' => 'phoneBookInverseSearch',
            'EVN' => 'itemizedBill',
            'anonymisieren' => 'itemizedBillAnonymized',
            'reduziert' => 'itemizedBillShorted',
        ];

        foreach ($mapping as $dataKey => $objectParameter) {
            $value = trim($line[$dimariHeaderReversed[$dataKey]]);

            if (!empty($value) && '0' !== $value) {
                $purtelAccount->{$objectParameter} = $value;
            }
        }

        $customerPurtelAccounts[$key] = $purtelAccount;
    }

    return $customerPurtelAccounts;
};

$locations = [];

$getLocationIdForDistrict = function ($district) use (&$locations)
{
    global $db;

    if (isset($locations[$district])) { // already seen this location?
        return $locations[$district];
    } else {
        $query = $db->query("SELECT `id` FROM `locations` WHERE `name` = '".$district."'");

        if (1 === $query->num_rows) { // already have location in database?
            $location = $query->fetch_assoc();

            return $locations[$district] = $location['id'];
        }
    }
die("asd");
    $db->query(sprintf('INSERT INTO `locations` (`%s`, `%s`, `%s`, `%s`, `%s`) VALUES ("%s", "%s", "%s", "%s", "%s")',
        'network_id',
        'name',
        'kvz_prefix',
        'cid_suffix_start',
        'cid_suffix_end',
        NETWORK_ID,
        $district,
        $district,
        '1',
        '9999'
    ));

    if (!empty($db->error)) {
        throw new Exception($db->error);
    }

    return $locations[$district] = $db->insert_id;
};







$clientIdStartingString = NETZ.'.'.CLIENT_ID_YEAR.'.';
$lastClientId = 0;

$lastClientIdQuery = $db->query("SELECT `clientid` FROM `customers` 
    WHERE `clientid` LIKE '".$clientIdStartingString."%' ORDER BY `clientid` DESC LIMIT 1"
);

if (1 === $lastClientIdQuery->num_rows) {
    $lastClientId = $lastClientIdQuery->fetch_row();
    $lastClientId = $lastClientId[0];
    $lastClientId = explode('.', $lastClientId);
    $lastClientId = (int) end($lastClientId);
}

$foundPurtelLogins = [];
$importedCustomerIds = [];

$counter = 0;
$alreadyImportedCustomers = [];

foreach ($dimariCsv as $key => $line) {
    // strip ' from beginning and end of column
    foreach ($line as $k => $v) {
        if ("'" === substr($v, 0, 1) && "'" === substr($v, -1)) {
            $line[$k] = substr($v, 1, -1);
        }
    }

    $customerId = trim($line[$dimariHeaderReversed['KUNDENNUMMER']]);

    if (isset($alreadyImportedCustomers[$customerId])) {
        continue; // already imported all data for this customer in an earlier loop
    }

    // see if a customer with same customer_id already exists in database
    $findCustomerInDbQuery = $db->query("SELECT `id`, `mac_address` FROM `customers` WHERE `customer_id` = ".$customerId);

    if ($findCustomerInDbQuery->num_rows > 0) {
        // a customer with same customer_id already exists in database!
        $customerDataFromDb = $findCustomerInDbQuery->fetch_assoc();

        /*echo sprintf("\nData for KUNDENNUMMER = %s already exists in database (%s)\nSkipped...\n",
            $customerId,
            $customerDataFromDb['id']
        );*/

        $alreadyImportedCustomers[$customerId] = true;

        if (count($dataSortedByCustomerId[$customerId]) !== 1) {
            echo sprintf("not only one customer found - %s\n", $customerId);
            continue;
        }

        $cwmp = trim($line[$dimariHeaderReversed['Fritz Box Seriennr']]);
        $cwmp = preg_replace('/^offline/', '', $cwmp);

        $matchMac = null;

        preg_match('/([0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2})|([0-9a-f]{12})/i', $cwmp, $matchMac);

        if (isset($matchMac[0])) {
            $cwmp = preg_replace('/[^0-9a-f]/i', '', $matchMac[0]);

            if (empty($customerDataFromDb['mac_address'])) {
                // insert

                $db->query("UPDATE `customers` SET `mac_address` = '".$cwmp."' WHERE `id` = ".$customerDataFromDb['id']);

                if (!empty($db->error)) {
                    echo sprintf("db error - %s", $db->error);
                    continue;
                }

                echo "imported: ".$cwmp."\n";
            } else {
                if ($customerDataFromDb['mac_address'] !== $cwmp) {
                    echo sprintf("Kunde hat schon CWMP aber andere (%s) - %s\n", $cwmp, $customerId);
                }
            }
        } else {
            echo sprintf("Keine valide CWMP (%s) - %s\n", $cwmp, $customerId);
            continue;
        }

        continue;
    }
echo "öhh..\n";
var_dump($customerId);
continue;
}

var_dump($counter);

if (!empty($importedCustomerIds)) {
    echo sprintf('imported id between %s - %s', $importedCustomerIds[0], end($importedCustomerIds))."\n";
}
