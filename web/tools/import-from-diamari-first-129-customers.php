<?php

/**
 * Import customer-data from Dimari and Telefonica
 * 
 * Nur die ersten 129 Kunden
 */

/**
 * Read contents from csv-file
 *
 * @param string $filename
 *
 * @throws InvalidArgumentException
 *
 * @return array
 */
function readCsvFile($filename)
{
    if (!file_exists($filename) || !is_readable($filename)) {
        throw new InvalidArgumentException('Can not open file for reading');
    }

    $content = file_get_contents($filename);

    if (!is_string($content) && !empty($content)) {
        throw new InvalidArgumentException('invalid file');
    }

    $delimiter = null;

    foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
        if (false !== strpos($content, $delimiter_)) {
            $delimiter = $delimiter_;
            break;
        }
    }

    if (null == $delimiter) {
        throw new InvalidArgumentException('can not use file');
    }

    return array_map(function ($line) {
        return str_getcsv($line, ';');
    }, explode($delimiter, $content));
}

$dimariCsv = readCsvFile('/tmp/Stammdaten_versuch1.csv');
$productsCsv = readCsvFile('/tmp/Dimari_Export_Cluster_28.csv');

$dimariHeader = array_shift($dimariCsv);
$dimariHeaderReversed = [];
$productsHeader = array_shift($productsCsv);
$productsHeaderReversed = [];

foreach ($dimariHeader as $column => $name) {
    $dimariHeaderReversed[$name] = $column;
}
foreach ($productsHeader as $column => $name) {
    $productsHeaderReversed[$name] = $column;
}

if (count(end($productsCsv)) !== count($productsHeader)) {
    array_pop($productsCsv);
}
if (count(end($dimariCsv)) !== count($dimariHeader)) {
    array_pop($dimariCsv);
}



$ids = [
    10729,
    11138,
    11985,
    12016,
    12032,
    12115,
    12182,
    12252,
    12262,
    12293,
    12413,
    12906,
    12908,
    12940,
    13114,
    13278,
    13338,
    13388,
    13470,
    13628,
    13715,
    13716,
    13717,
    13718,
    13760,
    13761,
    13763,
    13767,
    13774,
    13775,
    13776,
    13778,
    13828,
    13833,
    13843,
    13844,
    13847,
    13947,
    14115,
    14134,
    14172,
    14173,
    14250,
    14301,
    14359,
    14485,
    14527,
    14546,
    14570,
    14572,
    14582,
    14583,
    14584,
    14599,
    14662,
    14667,
    14668,
    14669,
    14675,
    14702,
    14716,
    14721,
    14723,
    14724,
    14726,
    14728,
    14737,
    14745,
    14749,
    14756,
    14759,
    14775,
    14862,
    14889,
    14901,
    14955,
    14963,
    14984,
    15086,
    15097,
    15221,
    15245,
    15293,
    15307,
    15314,
    15316,
    15319,
    15320,
    15364,
    15371,
    15424,
    15444,
    15520,
    15579,
    15603,
    15604,
    15609,
    15637,
    15836,
    15852,
    15956,
    15971,
    16200,
    16436,
    16451,
    16490,
    16576,
    16616,
    16628,
    16767,
    16866,
    17047,
    17064,
    17238,
    17405,
    17485,
    17510,
    17868,
    17869,
    18540,
    18677,
    18694,
    18904,
    18970,
    19090,
    19190,
    19310,
    19711,
    19855,
];


require __DIR__.'/../_conf/database.inc';



$products = [];

foreach ($productsCsv as $key => $line) {
    $productId = $line[$productsHeaderReversed['PRODUCT_ID']];
    $name = trim($line[$productsHeaderReversed['PRODUKTBESCHREIBUNG']]);
    $itemCategory = trim($line[$productsHeaderReversed['KATEGORIE']]);
    $priceNet = trim($line[$productsHeaderReversed['Preis netto']]);
    $priceNet = empty($priceNet) ? null : (float) str_replace(",", ".", $priceNet);

    if (!isset($products[$productId])) {
        if (!empty($name) && !empty($itemCategory)) {
            $products[$productId] = [
                'name' => $name,
                'itemCategory' => $itemCategory,
                'priceNet' => $priceNet,
            ];
        }
    } else {
        if ($products[$productId]['name'] !== $name || $products[$productId]['itemCategory'] !== $itemCategory) {
            echo "wrong data\n";
            var_dump($products[$productId], $name, $itemCategory, $priceNet);
        }

        if ($priceNet > $products[$productId]['priceNet']) {
            $products[$productId]['priceNet'] = $priceNet;
        }
    }
}

foreach ($products as $productId => $data) {
    $product = $db->query("SELECT * FROM `products` WHERE `identifier` = '".$productId."'");

    if (1 === $product->num_rows) {
        $product = $product->fetch_assoc();
        $products[$productId]['id'] = $product['id'];
    } else {
        $data['name'] = addslashes($data['name']);

        $db->query(sprintf("INSERT INTO `products` (`name`, `active`, `identifier`, `default_price_net`, `item_category`) VALUES (%s, 1, %s, %s, %s)", 
            "'".$data['name']."'",
            "'".$productId."'",
            null === $data['priceNet'] ? 'NULL' : $data['priceNet'],
            "'".$data['itemCategory']."'"
        ));

        if (!empty($db->error)) {
            var_dump($db->error);
            die();
        }

        $products[$productId]['id'] = $db->insert_id;
    }
}

//var_dump($products);

//die("End products\n");

$foundIds = [];
$counter = 1;

foreach ($dimariCsv as $key => $line) {
    if (in_array($line[$dimariHeaderReversed['KUNDENNUMMER']], $ids)) {
        if (!isset($foundIds[$line[$dimariHeaderReversed['KUNDENNUMMER']]])) {
            if (empty($line[$dimariHeaderReversed['PIN FB']])) {
                continue;
            }

            $customerIdQuery = "SELECT `id` FROM `customers` WHERE `customer_id` = ".$line[$dimariHeaderReversed['KUNDENNUMMER']];

            $result = $db->query($customerIdQuery);

            if ($id = $result->fetch_assoc()) {
                $id = $id['id'];
            } else {
                echo "\t\tid not found : ".$line[$dimariHeaderReversed['KUNDENNUMMER']]."\n";

                continue;
            }

            $terminal_serialno = '';

            if (!empty($line[$dimariHeaderReversed['Fritz Box Seriennr']]) && 'not found' !== $line[$dimariHeaderReversed['Fritz Box Seriennr']]) {
                $terminal_serialno = $line[$dimariHeaderReversed['Fritz Box Seriennr']];
            }

            $line_identifier = '';

            if (!empty($line[$dimariHeaderReversed['Line ID']])) {
                $line_identifier = $line[$dimariHeaderReversed['Line ID']];
            }

            $pppoe_pin = '';

            if (!empty($line[$dimariHeaderReversed['PIN FB']])) {
                $pppoe_pin = $line[$dimariHeaderReversed['PIN FB']];
            }

            $purtelAccount = null;

            if (!empty($line[$dimariHeaderReversed['TEL_RUFNR']])) {
                $nummer = preg_replace('/[^0-9]/', '', $line[$dimariHeaderReversed['TEL_RUFNR']]);

                $query = $db->query("SELECT * FROM `purtel_account` WHERE `nummer` = '".$nummer."'");

                if ($query->num_rows !== 1) {
                    echo "\t\t\tnummer-select not 1 (".$nummer.")\n";
                    
                    continue;
                }

                $purtelAccount = $query->fetch_assoc();
                $purtelAccount = $purtelAccount['id'];
            }

            $commentQuery = $db->query("SELECT `comment` FROM `customers` WHERE `id` = ".$id);

            if ($commentQuery->num_rows !== 1) {
                echo "\t\tcould not fetch comment from customer\n";

                continue;
            }

            $comment = $commentQuery->fetch_assoc();
            $comment = $comment['comment'];
            $additionalComment = "";

            if (!empty($line[$dimariHeaderReversed['Mandatsreferenz']]) && 'keine' !== $line[$dimariHeaderReversed['Mandatsreferenz']]) {
                $additionalComment .= "Mandatsreferenz: ".$line[$dimariHeaderReversed['Mandatsreferenz']]."\n";
            }

            if (!empty($line[$dimariHeaderReversed['Web-Benutzername']]) && 'kein' !== $line[$dimariHeaderReversed['Web-Benutzername']]) {
                $additionalComment .= "Web-Benutzername: ".$line[$dimariHeaderReversed['Web-Benutzername']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['Web-Passwort']])) {
                $additionalComment .= "Web-Passwort: ".$line[$dimariHeaderReversed['Web-Passwort']]."\n";
            }

            if (!empty($line[$dimariHeaderReversed['TEL_RUFNR']])) {
                $additionalComment .= "TEL_RUFNR: ".$line[$dimariHeaderReversed['TEL_RUFNR']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['TEL_ANREDE']])) {
                $additionalComment .= "TEL_ANREDE: ".$line[$dimariHeaderReversed['TEL_ANREDE']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['TEL_FIRMA']])) {
                $additionalComment .= "TEL_FIRMA: ".$line[$dimariHeaderReversed['TEL_FIRMA']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['TEL_NAME']])) {
                $additionalComment .= "TEL_NAME: ".$line[$dimariHeaderReversed['TEL_NAME']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['TEL_VORNAME']])) {
                $additionalComment .= "TEL_VORNAME: ".$line[$dimariHeaderReversed['TEL_VORNAME']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['TEL_Straße']])) {
                $additionalComment .= "TEL_Straße: ".$line[$dimariHeaderReversed['TEL_Straße']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['TEL_HAUSNR']])) {
                $additionalComment .= "TEL_HAUSNR: ".$line[$dimariHeaderReversed['TEL_HAUSNR']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['TEL_Hausnr.zus.']])) {
                $additionalComment .= "TEL_Hausnr.zus.: ".$line[$dimariHeaderReversed['TEL_Hausnr.zus.']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['TEL_PLZ']])) {
                $additionalComment .= "TEL_PLZ: ".$line[$dimariHeaderReversed['TEL_PLZ']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['TEL_ORT']])) {
                $additionalComment .= "TEL_ORT: ".$line[$dimariHeaderReversed['TEL_ORT']]."\n";
            }

            if (!empty($line[$dimariHeaderReversed['INSTAL_ANREDE']])) {
                $additionalComment .= "INSTAL_ANREDE: ".$line[$dimariHeaderReversed['INSTAL_ANREDE']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['INSTAL_FIRMA']])) {
                $additionalComment .= "INSTAL_FIRMA: ".$line[$dimariHeaderReversed['INSTAL_FIRMA']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['INSTAL_NAME']])) {
                $additionalComment .= "INSTAL_NAME: ".$line[$dimariHeaderReversed['INSTAL_NAME']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['INSTAL_VORNAME']])) {
                $additionalComment .= "INSTAL_VORNAME: ".$line[$dimariHeaderReversed['INSTAL_VORNAME']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['INSTAL_Straße']])) {
                $additionalComment .= "INSTAL_Straße: ".$line[$dimariHeaderReversed['INSTAL_Straße']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['INSTAL_HAUSNR']])) {
                $additionalComment .= "INSTAL_HAUSNR: ".$line[$dimariHeaderReversed['INSTAL_HAUSNR']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['INSTAL_Hausnr.zus.']])) {
                $additionalComment .= "INSTAL_Hausnr.zus.: ".$line[$dimariHeaderReversed['INSTAL_Hausnr.zus.']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['INSTAL_PLZ']])) {
                $additionalComment .= "INSTAL_PLZ: ".$line[$dimariHeaderReversed['INSTAL_PLZ']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['INSTAL_ORT']])) {
                $additionalComment .= "INSTAL_ORT: ".$line[$dimariHeaderReversed['INSTAL_ORT']]."\n";
            }

            if (!empty($line[$dimariHeaderReversed['Interne Bestellnummer']])) {
                $additionalComment .= "Interne Bestellnummer: ".$line[$dimariHeaderReversed['Interne Bestellnummer']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['Land1']])) {
                $additionalComment .= "Land1: ".$line[$dimariHeaderReversed['Land1']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['Land2']])) {
                $additionalComment .= "Land2: ".$line[$dimariHeaderReversed['Land2']]."\n";
            }

            if (!empty($line[$dimariHeaderReversed['ACS']])) {
                $additionalComment .= "ACS: ".$line[$dimariHeaderReversed['ACS']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['CPRODUCTID']])) {
                $additionalComment .= "CPRODUCTID: ".$line[$dimariHeaderReversed['CPRODUCTID']]."\n";
            }
            if (!empty($line[$dimariHeaderReversed['2.Ltg']]) && '0' !== $line[$dimariHeaderReversed['2.Ltg']]) {
                $additionalComment .= "2.Ltg: ".$line[$dimariHeaderReversed['2.Ltg']]."\n";
            }

            if (!empty($additionalComment)) {
                $comment .= "\n".$additionalComment;
            }


            $paper_bill = '';

            if (!empty($line[$dimariHeaderReversed['Papierrg']]) && '0' !== $line[$dimariHeaderReversed['Papierrg']]) {
                $paper_bill = 'yes';
            }


            $customerProducts = [];

            foreach ($productsCsv as $key => $productsCsvLine) {
                if ($line[$dimariHeaderReversed['KUNDENNUMMER']] == $productsCsvLine[$productsHeaderReversed['KUNDENNUMMER']]) {
                    $productId = $productsCsvLine[$productsHeaderReversed['PRODUCT_ID']];

                    if (empty($productId)) {
                        continue;
                    }

                    $priceNet = trim($productsCsvLine[$productsHeaderReversed['Preis netto']]);
                    $priceNet = empty($priceNet) ? null : (float) str_replace(",", ".", $priceNet);

                    $activationDate = trim($productsCsvLine[$productsHeaderReversed['Prod.Aktivierung']]);
                    $activationDate = empty($activationDate) ? null : new \DateTime($activationDate);

                    $deactivationDate = trim($productsCsvLine[$productsHeaderReversed['Prod.Deaktivierung']]);
                    $deactivationDate = empty($deactivationDate) ? null : new \DateTime($deactivationDate);

                    $customerProducts[] = [
                        'id' => $products[$productId]['id'],
                        'priceNet' => $priceNet,
                        'activationDate' => $activationDate,
                        'deactivationDate' => $deactivationDate,
                    ];
                }
            }

            if (!empty($customerProducts)) {
                foreach ($customerProducts as $data) {
                    $query = sprintf("INSERT INTO `customer_products` (`product_id`, `customer_id`, `price_net`, `activation_date`, `deactivation_date`) VALUES (%s, %s, %s, %s, %s)",
                        $data['id'],
                        $id,
                        null === $data['priceNet'] ? 'NULL' : $data['priceNet'],
                        null === $data['activationDate'] ? 'NULL' : "'".$data['activationDate']->format('Y-m-d')."'",
                        null === $data['deactivationDate'] ? 'NULL' : "'".$data['deactivationDate']->format('Y-m-d')."'"
                    );

                    $db->query($query);

                    if (!empty($db->error)) {
                        var_dump($db->error);die();
                    }
                }
            }



// ALTER TABLE `customers` ADD COLUMN `pppoe_pin` VARCHAR(25) NULL DEFAULT NULL;

// ALTER TABLE `purtel_account` ADD COLUMN `phone_book_information` TINYINT(1) NULL DEFAULT NULL;
// ALTER TABLE `purtel_account` ADD COLUMN `phone_book_digital_media` TINYINT(1) NULL DEFAULT NULL;
// ALTER TABLE `purtel_account` ADD COLUMN `phone_book_digital_inverse_search` TINYINT(1) NULL DEFAULT NULL;

// ALTER TABLE `purtel_account` ADD COLUMN `itemized_bill` TINYINT(1) NULL DEFAULT NULL;
// ALTER TABLE `purtel_account` ADD COLUMN `itemized_bill_anonymized` TINYINT(1) NULL DEFAULT NULL;
// ALTER TABLE `purtel_account` ADD COLUMN `itemized_bill_shorted` TINYINT(1) NULL DEFAULT NULL;


            // if $purtelAccount not null
                // add data to this purtel_account
            // else
                // select purtel_account by customer_id
                // if num_rows > 1
                    // use master (if set)
                    // if no master, print error message
                // else
                    // add to this purtel_account

            $telefonbuch_eintrag = null;
            $phone_book_information = null;
            $phone_book_digital_media = null;
            $phone_book_digital_inverse_search = null;
            $itemized_bill = null;
            $itemized_bill_anonymized = null;
            $itemized_bill_shorted = null;

            if (!empty($line[$dimariHeaderReversed['Telefonbuch']]) && '0' !== $line[$dimariHeaderReversed['Telefonbuch']]) {
                $telefonbuch_eintrag = 'Ja';
            }

            if (!empty($line[$dimariHeaderReversed['Auskunft']]) && '0' !== $line[$dimariHeaderReversed['Auskunft']]) {
                $phone_book_information = $line[$dimariHeaderReversed['Telefonbuch']];
            }
            if (!empty($line[$dimariHeaderReversed['Digitale Medien']]) && '0' !== $line[$dimariHeaderReversed['Digitale Medien']]) {
                $phone_book_digital_media = $line[$dimariHeaderReversed['Digitale Medien']];
            }
            if (!empty($line[$dimariHeaderReversed['Inverssuche']]) && '0' !== $line[$dimariHeaderReversed['Inverssuche']]) {
                $phone_book_digital_inverse_search = $line[$dimariHeaderReversed['Inverssuche']];
            }

            if (!empty($line[$dimariHeaderReversed['EVN']]) && '0' !== $line[$dimariHeaderReversed['EVN']]) {
                $itemized_bill = $line[$dimariHeaderReversed['EVN']];
            }
            if (!empty($line[$dimariHeaderReversed['anonymisieren']]) && '0' !== $line[$dimariHeaderReversed['anonymisieren']]) {
                $itemized_bill_anonymized = $line[$dimariHeaderReversed['anonymisieren']];
            }
            if (!empty($line[$dimariHeaderReversed['reduziert']]) && '0' !== $line[$dimariHeaderReversed['reduziert']]) {
                $itemized_bill_shorted = $line[$dimariHeaderReversed['reduziert']];
            }

            $addToPurtelAccount = null;

            if (null !== $purtelAccount) {
                $addToPurtelAccount = $purtelAccount;
            } else {
                $purtelAccountQuery = $db->query("SELECT * FROM `purtel_account` WHERE `cust_id` = ".$id);

                if (1 === $purtelAccountQuery->num_rows) {
                    $purtelAccount = $purtelAccountQuery->fetch_assoc();
                    $$addToPurtelAccount = $purtelAccount['id'];
                } elseif ($purtelAccountQuery->num_rows > 1) {
                    while ($data = $purtelAccountQuery->fetch_assoc()) {
                        if ('1' === $data['master']) {
                            $addToPurtelAccount = $data['id'];

                            break;
                        }
                    }
                }
            }

            if (null !== $addToPurtelAccount) {
                $db->query(sprintf("UPDATE `purtel_account` SET 
                    `phone_book_information` = %s, 
                    `phone_book_digital_media` = %s, 
                    `phone_book_inverse_search` = %s, 
                    `itemized_bill` = %s, 
                    `itemized_bill_anonymized` = %s, 
                    `itemized_bill_shorted` = %s 
                    WHERE `id` = ".$addToPurtelAccount,
                    null === $phone_book_information ? 'NULL' : $phone_book_information,
                    null === $phone_book_digital_media ? 'NULL' : $phone_book_digital_media,
                    null === $phone_book_digital_inverse_search ? 'NULL' : $phone_book_digital_inverse_search,
                    null === $itemized_bill ? 'NULL' : $itemized_bill,
                    null === $itemized_bill_anonymized ? 'NULL' : $itemized_bill_anonymized,
                    null === $itemized_bill_shorted ? 'NULL' : $itemized_bill_shorted
                ));

                if (!empty($db->error)) {
                    die(var_dump($db->error));
                }
            } else {
                $additionalComment = '';

                if (null !== $phone_book_information) {
                    $additionalComment .= "Telefonbucheintrag (Auskunft): ".$phone_book_information."\n";
                }
                if (null !== $phone_book_digital_media) {
                    $additionalComment .= "Telefonbucheintrag (Digitale Medien): ".$phone_book_digital_media."\n";
                }
                if (null !== $phone_book_digital_inverse_search) {
                    $additionalComment .= "Telefonbucheintrag (Inverssuche): ".$phone_book_digital_inverse_search."\n";
                }
                if (null !== $itemized_bill) {
                    $additionalComment .= "EVN: ".$itemized_bill."\n";
                }
                if (null !== $itemized_bill_anonymized) {
                    $additionalComment .= "EVN (anonymisieren): ".$itemized_bill_anonymized."\n";
                }
                if (null !== $itemized_bill_shorted) {
                    $additionalComment .= "EVN (reduziert): ".$itemized_bill_shorted."\n";
                }

                if (!empty($additionalComment)) {
                    $comment .= "\n".$additionalComment;
                }
            }


            $db->query(sprintf("UPDATE `customers` SET 
                `terminal_serialno` = %s, 
                `line_identifier` = %s, 
                `pppoe_pin` = %s, 
                `comment` = %s, 
                `paper_bill` = %s, 
                `telefonbuch_eintrag` = %s 
                WHERE `id` = ".$id,
                null === $terminal_serialno ? 'NULL' : "'".$terminal_serialno."'",
                null === $line_identifier ? 'NULL' : "'".$line_identifier."'",
                null === $pppoe_pin ? 'NULL' : "'".$pppoe_pin."'",
                null === $comment ? 'NULL' : "'".$comment."'",
                null === $paper_bill ? 'NULL' : "'".$paper_bill."'",
                null === $telefonbuch_eintrag ? 'NULL' : "'".$telefonbuch_eintrag."'"
            ));

            if (!empty($db->error)) {
                die(var_dump($db->error));
            }

            echo $line[$dimariHeaderReversed['KUNDENNUMMER']]." (".$counter.")\n";

            $foundIds[$line[$dimariHeaderReversed['KUNDENNUMMER']]] = true;
            $counter++;
        }
    }
}


?>
