<?php
  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename=root_'.date ("Ym").'.csv');  

  require_once('../config.inc');

  $csv_handle = fopen('php://output', 'w');

  $purtel     = array();
  $username   = '506716';
  $password   = 'KaQu07GTb1D9vRBC';
  $action     = 'stammdatenexport';
  $parameter  = array (
    'periode'   => date('Ym'),
    'erweitert' => '9',
  );    
  $PurtelUrl  = 'https://ipcom.purtel.com/index.php?super_username=' . urlencode($username) . '&super_passwort=' . urlencode($password) . '&action=' . urlencode($action);
  foreach ($parameter as $key => $value) {
    $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
  }
  $curl = curl_init ($PurtelUrl);
  $curl_options = array (
    CURLOPT_URL             => $PurtelUrl,
    CURLOPT_HEADER          => false,
    CURLOPT_RETURNTRANSFER  => true,
    CURLOPT_SSL_VERIFYPEER  => false,
  );
  curl_setopt_array($curl, $curl_options);
  $result       = curl_exec($curl);
  $lines        = explode ("\n", $result);
  array_pop($lines);
  $headline_csv = array_shift ($lines);
  $headline     = str_getcsv($headline_csv, ';');
  while ($line = array_shift ($lines)) {
    $purt_csv = $line;
    $purt     = str_getcsv($purt_csv, ';');
    foreach ($headline as $ind => $head) {
      $temp[$head] = $purt[$ind];
    }
    $purtel[$temp['Kundennummer']] = $temp;
  }
  curl_close($curl);
  $headline = array (
    utf8_decode('Kundennummer'),
    utf8_decode('Kundennummer KV'),
    utf8_decode('Anschluß'),
    utf8_decode('Name'),
    utf8_decode('Straße'),
    utf8_decode('Ort'),
    utf8_decode('Zahlart'),
    utf8_decode('Veränderung')
  );
  fputcsv($csv_handle, $headline, ';');

  foreach ($purtel as $temp) {
    #$temp['betrag'] = str_replace ('.', ',', $temp['betrag'] / 100);
    $csv = array (
      $temp['Kundennummer'],
      $temp['kundennummer_extern'],
      $temp['ennummer'],
      $temp['Name1'].' '.$temp['Name2'],
      $temp['Strasse'],
      $temp['PLZ'].' '.$temp['Ort'],
      $temp['AktuelleZahlart'],
      $temp['Veraenderung'],
    );
    fputcsv($csv_handle, $csv, ';');
  }
  fclose ($csv_handle);
?>
