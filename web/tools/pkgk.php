<?php
  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename=pkgk_'.date ("Ym").'.csv');  

  require_once('../config.inc');
  require_once($StartPath.'/_conf/database.inc');

  $csv_handle = fopen('php://output', 'w');

  $select = "select * from customers order by clientid";

  $db_cust = $db->query( $select);
  $custs   = $db_cust->fetch_all(MYSQLI_ASSOC);
  $db_cust->close();
  
  $username   = '506716';
  $password   = 'KaQu07GTb1D9vRBC';
  $action     = 'stammdatenexport';
  $parameter  = array (
    'erweitert' => 9,
    'periode'   => date('Ym'),
  );    
  $PurtelUrl  = 'https://ipcom.purtel.com/index.php?super_username=' . urlencode($username) . '&super_passwort=' . urlencode($password) . '&action=' . urlencode($action);
  foreach ($parameter as $key => $value) {
    $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
  }
  $curl = curl_init ($PurtelUrl);
  $curl_options = array (
    CURLOPT_URL             => $PurtelUrl,
    CURLOPT_HEADER          => false,
    CURLOPT_RETURNTRANSFER  => true,
    CURLOPT_SSL_VERIFYPEER  => false,
  );
  curl_setopt_array($curl, $curl_options);
  $result       = curl_exec($curl);
  $lines        = explode ("\n", $result);
  array_pop($lines);
  $headline_csv = array_shift ($lines);
  $headline     = str_getcsv($headline_csv, ';');
  while ($line = array_shift ($lines)) {
    $purt_csv = $line;
    $purt     = str_getcsv($purt_csv, ';');
    foreach ($headline as $ind => $head) {
      $temp[$head] = $purt[$ind];
    }
    $purtel[$temp['kundennummer_extern']] = $temp;
  }
  curl_close($curl);
  $headline = array (
    utf8_decode('Kundennummer'),
    utf8_decode('Vorname'),
    utf8_decode('Nachname'),
    utf8_decode('Firma'),
    utf8_decode('Straße'),
    utf8_decode('Ort'),
    utf8_decode('Kundenart KV'),
    utf8_decode('Kundenart Purtel')
  );
  fputcsv($csv_handle, $headline, ';');

  foreach ($custs as $temp) {
    $temp['street'] = $temp['street'].' '.$temp['streetno'];
    $temp['city']   = $temp['zipcode'].' '.$temp['city'];
    if ($temp['clienttype'] == 'commercial') $temp['clienttype'] = 'GK'; else $temp['clienttype'] = 'PK';
    if (key_exists($temp['clientid'], $purtel)) {
      if ($purtel[$temp['clientid']]['Kundenart'] == 1) $temp['purtel'] = 'GK'; else $temp['purtel'] = 'PK';
    } else {
      $temp['purtel'] = 'in Purtel nicht vorhanden';
    }
    $csv = array (
      utf8_decode($temp['clientid']),
      utf8_decode($temp['firstname']),
      utf8_decode($temp['lastname']),
      utf8_decode($temp['company']),
      utf8_decode($temp['street']),
      utf8_decode($temp['city']),
      $temp['clienttype'],
      $temp['purtel']
    );
    fputcsv($csv_handle, $csv, ';');
  }
  fclose ($csv_handle);
  $db->close();
?>
