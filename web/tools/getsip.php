<?php
  require_once('../config.inc');
  require_once($StartPath.'/_conf/database.inc');

  $purtelac   = array();
  $purtelnr   = array();
  $username   = '506716';
  $password   = 'KaQu07GTb1D9vRBC';

  $action     = 'getaccounts';
  $parameter  = array (
    'erweitert' => 4
  );    
  $PurtelUrl  = 'https://ipcom.purtel.com/index.php?super_username=' . urlencode($username) . '&super_passwort=' . urlencode($password) . '&action=' . urlencode($action);
  foreach ($parameter as $key => $value) {
    $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
  }
  $curl = curl_init ($PurtelUrl);
  $curl_options = array (
    CURLOPT_URL             => $PurtelUrl,
    CURLOPT_HEADER          => false,
    CURLOPT_RETURNTRANSFER  => true,
    CURLOPT_SSL_VERIFYPEER  => false,
  );
  curl_setopt_array($curl, $curl_options);
  $result       = curl_exec($curl);
  $lines        = explode ("\n", $result);
  array_pop($lines);
  $headline_csv = array_shift ($lines);
  $headline     = str_getcsv($headline_csv, ';');
  while ($line = array_shift ($lines)) {
    $purt_csv = $line;
    $purt     = str_getcsv($purt_csv, ';');
    foreach ($headline as $ind => $head) {
      $temp[$head] = $purt[$ind];
    }
    if (!key_exists ($temp['kundennummer_extern'], $purtelac)) $purtelac[$temp['kundennummer_extern']] =array();
    array_push ($purtelac[$temp['kundennummer_extern']], $temp);
  }
  curl_close($curl);

  $action     = 'getnumbers';
  $parameter  = array (
  );    
  $PurtelUrl  = 'https://ipcom.purtel.com/index.php?super_username=' . urlencode($username) . '&super_passwort=' . urlencode($password) . '&action=' . urlencode($action);
  foreach ($parameter as $key => $value) {
    $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
  }
  $curl = curl_init ($PurtelUrl);
  $curl_options = array (
    CURLOPT_URL             => $PurtelUrl,
    CURLOPT_HEADER          => false,
    CURLOPT_RETURNTRANSFER  => true,
    CURLOPT_SSL_VERIFYPEER  => false,
  );
  curl_setopt_array($curl, $curl_options);
  $result       = curl_exec($curl);
  $lines        = explode ("\n", $result);
  array_pop($lines);
  $headline_csv = array_shift ($lines);
  $headline     = str_getcsv($headline_csv, ';');
  while ($line = array_shift ($lines)) {
    $purt_csv = $line;
    $purt     = str_getcsv($purt_csv, ';');
    foreach ($headline as $ind => $head) {
      $temp[$head] = $purt[$ind];
    }
    if (!key_exists ($temp['anschluss'], $purtelnr)) $purtelnr[$temp['anschluss']] =array();
    array_push ($purtelnr[$temp['anschluss']], $temp);
  }
  curl_close($curl);

  $select = "select * from customers order by clientid";
  $db_cust = $db->query($select);
  $custs   = $db_cust->fetch_all(MYSQLI_ASSOC);
  $db_cust->close();
?>
<html lang="de" xmlns="http://www.w3.org/1999/xhtml" xml:lang="de">
<head>
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
  <title>purtel accounts</title>
</head>
<body>
  <table border='1'>
  <tr>
    <th>clientid</th>
    <th>firstname</th>
    <th>lastname</th>
    <th>Purtel KV</th>
    <th>Purtel</th>
  </tr>    
<?php
  foreach ($custs as $temp) {
    if ($temp['clientid']) {
      echo '<tr>';
      echo '<td>'.$temp['clientid'].'</td>';
      echo '<td>'.$temp['firstname'].'</td>';
      echo '<td>'.$temp['lastname'].'</td>';
      echo '<td>'.$temp['purtel_login'].'</td>';
      $purid = '';
      if (key_exists($temp['clientid'], $purtelac)) {
        foreach ($purtelac[$temp['clientid']] as $pur) {
          $insert = "insert into purtel_accounts set ";
          if ($temp['purtel_login'] and $temp['purtel_login'] != $pur['anschluss']) $insert .= "accounting = '".$temp['purtel_login']."', ";
          $insert .= "clientid = '".$temp['clientid']."', ";
          $insert .= "purtel_login = '".$pur['anschluss']."', ";
          $insert .= "purtel_passwort = '".$pur['passwort']."', ";
          $insert .= "closed = '".$pur['gesperrt']."'; ";
          $db_ins = $db->query($insert);
          $purid .= $pur['anschluss'].' ';
        }
      }
      echo '<td>'.$purid.'</td>';
      echo '</tr>';
    }
  }

  foreach ($purtelnr as $purteltmp) {
    foreach ($purteltmp as $temp) {
      $insert = "insert into purtel_telnumber set ";
      $insert .= "purtel_login = '".$temp['anschluss']."', ";
      $insert .= "telnumber = '".$temp['rufnummer']."', ";
      $insert .= "type = '".$temp['art']."'; ";
      $db_ins = $db->query($insert);
    }
  }

  $db->close();
?>
  </table>
</body>
</html>
