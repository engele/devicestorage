<?php

require __DIR__.'/../_conf/database.inc';

$a = [
    '62.96.91.0/24',
    '62.96.251.16/28',
    '185.8.84.0/21',
    '185.65.196.0/22',
    '195.178.0.0/24',
    '195.178.1.0/24',
    '213.61.107.0/24',
];

foreach ($a as $range) {
    $range = explode('/', $range);

    $firstAddress = $range[0];
    $cidr = $range[1];

    if (false === filter_var($firstAddress, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
        continue;
    }

    $bytes = 32 - (int) $cidr;

    $firstAddress = ip2long($firstAddress);
    $lastAddress = $firstAddress + (1 << $bytes);
    $lastAddress--;

    //$firstAddress = long2ip($firstAddress);
    //$lastAddress = long2ip($lastAddress);

    $q = $db->query("SELECT `emailaddress` FROM `customers` WHERE INET_ATON(`ip_address`) BETWEEN '".$firstAddress."' AND '".$lastAddress."'");

    while ($d = $q->fetch_assoc()) {
        if (!empty($d['emailaddress'])) {
            echo $d['emailaddress']."\n";
        }
    }
}


