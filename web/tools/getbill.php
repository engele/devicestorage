<?php
  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename=bills_'.date ("Ym").'.csv');  

  require_once('../config.inc');

  $csv_handle = fopen('php://output', 'w');

  $purtel     = array();
  $username   = '506716';
  $password   = 'KaQu07GTb1D9vRBC';
  $action     = 'getrechnungen';
  $parameter  = array (
    'von_datum' => '20150701',
    'bis_datum' => '20150731',
    'erweitert' => 1
  );    
  $PurtelUrl  = 'https://ipcom.purtel.com/index.php?super_username=' . urlencode($username) . '&super_passwort=' . urlencode($password) . '&action=' . urlencode($action);
  foreach ($parameter as $key => $value) {
    $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
  }
  $curl = curl_init ($PurtelUrl);
  $curl_options = array (
    CURLOPT_URL             => $PurtelUrl,
    CURLOPT_HEADER          => false,
    CURLOPT_RETURNTRANSFER  => true,
    CURLOPT_SSL_VERIFYPEER  => false,
  );
  curl_setopt_array($curl, $curl_options);
  $result       = curl_exec($curl);
  $lines        = explode ("\n", $result);
  array_pop($lines);
  $headline_csv = array_shift ($lines);
  $headline     = str_getcsv($headline_csv, ';');
  while ($line = array_shift ($lines)) {
    $purt_csv = $line;
    $purt     = str_getcsv($purt_csv, ';');
    foreach ($headline as $ind => $head) {
      $temp[$head] = $purt[$ind];
    }
    array_push($purtel,$temp);
  }
  curl_close($curl);
  $headline = array (
    utf8_decode('Buchungs-ID'),
    utf8_decode('Datum'),
    utf8_decode('Mandant'),
    utf8_decode('Anschluß'),
    utf8_decode('Kundennr.'),
    utf8_decode('Name'),
    utf8_decode('Straße'),
    utf8_decode('Ort'),
    utf8_decode('Rechnungsnr.'),
    utf8_decode('Betrag'),
    utf8_decode('Brutto'),
    utf8_decode('Netto'),
    utf8_decode('Umsatzsteuer'),
    utf8_decode('Zahlart'),
    utf8_decode('Zahlung'),
    utf8_decode('Debitor'),
    utf8_decode('Rechnungsdatum')
  );
  fputcsv($csv_handle, $headline, ';');
  foreach ($purtel as $temp) {

    $purtel_root  = array();
    $username     = '506716';
    $password     = 'KaQu07GTb1D9vRBC';
    $action       = 'stammdatenexport';
    $parameter    = array (
      'periode'   => '201507',
      'anschluss' => $temp['anschluss'],
      'erweitert' => '9',
    );    
    $PurtelUrl  = 'https://ipcom.purtel.com/index.php?super_username=' . urlencode($username) . '&super_passwort=' . urlencode($password) . '&action=' . urlencode($action);
    foreach ($parameter as $key => $value) {
      $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
    }
    $curl = curl_init ($PurtelUrl);
    $curl_options = array (
      CURLOPT_URL             => $PurtelUrl,
      CURLOPT_HEADER          => false,
      CURLOPT_RETURNTRANSFER  => true,
      CURLOPT_SSL_VERIFYPEER  => false,
    );
    curl_setopt_array($curl, $curl_options);
    $result       = curl_exec($curl);
    $lines        = explode ("\n", $result);
    array_pop($lines);
    $headline_csv = array_shift ($lines);
    $headline     = str_getcsv($headline_csv, ';');
    while ($line = array_shift ($lines)) {
      $purt_csv = $line;
      $purt     = str_getcsv($purt_csv, ';');
      foreach ($headline as $ind => $head) {
        $temp1[$head] = $purt[$ind];
      }
    }
    curl_close($curl);
   
    $temp['betrag'] = str_replace ('.', ',', $temp['betrag'] / 100);
    $temp['datum'] = date('d.m.Y',strtotime($temp['datum']));
    $temp['rechnungsdatum'] = date('d.m.Y',strtotime($temp['rechnungsdatum']));
    $csv = array (
      $temp['id'],
      $temp['datum'],
      $temp['mandant'],
      $temp['anschluss'],
      $temp['kundennummer_extern'],
      $temp1['Name1'].' '.$temp1['Name2'],
      $temp1['Strasse'],
      $temp1['PLZ'].' '.$temp1['Ort'],
      $temp['renummer'],
      $temp['betrag'],
      $temp['export_brutto'],
      $temp['export_netto'],
      $temp['export_steuer'],
      $temp1['AktuelleZahlart'],
      $temp['export_zahlung'],
      $temp['export_debitor'],
      $temp['rechnungsdatum'],
    );
    fputcsv($csv_handle, $csv, ';');
  }
  fclose ($csv_handle);
  $db->close();
?>
