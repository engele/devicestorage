<?php

/**
 * Read contents from csv-file
 *
 * @param string $filename
 *
 * @throws InvalidArgumentException
 *
 * @return array
 */
function readCsvFile($filename)
{
    if (!file_exists($filename) || !is_readable($filename)) {
        throw new InvalidArgumentException('Can not open file for reading');
    }

    $content = file_get_contents($filename);

    if (!is_string($content) && !empty($content)) {
        throw new InvalidArgumentException('invalid file');
    }

    $delimiter = null;

    foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
        if (false !== strpos($content, $delimiter_)) {
            $delimiter = $delimiter_;
            break;
        }
    }

    if (null == $delimiter) {
        throw new InvalidArgumentException('can not use file');
    }

    return array_map(function ($line) {
        return str_getcsv($line, ';');
    }, explode($delimiter, $content));
}

$dataCsv = readCsvFile('/tmp/test_reportc.csv');
$xxxCsv = readCsvFile('/tmp/CL28_Stammdaten_geprueft-22.02.18.csv');

$dataHeader = array_shift($dataCsv);
$dataHeaderReversed = [];

foreach ($dataHeader as $column => $name) {
    $dataHeaderReversed[$name] = $column;
}

if (count(end($dataCsv)) !== count($dataHeader)) {
    array_pop($dataCsv);
}

$xxxHeader = array_shift($xxxCsv);
$xxxHeaderReversed = [];

foreach ($xxxHeader as $column => $name) {
    $xxxHeaderReversed[$name] = $column;
}

if (count(end($xxxCsv)) !== count($xxxHeader)) {
    array_pop($xxxCsv);
}

/**
 * Find lines with specific column content in telefonica-data
 * 
 * @return array|null
 */
$findInXxxCsv = function ($matching) use (&$xxxCsv, &$xxxHeaderReversed)
{
    $matches = null;

    foreach ($matching as $column => $matchingValue) {
        if (null === $matches) {
            $matches = [];

            foreach ($xxxCsv as $key => $line) {
                if ($line[$xxxHeaderReversed[$column]] === $matchingValue) {
                    $matches[$key] = $line;
                }
            }
        } elseif (count($matches) > 0) {
            foreach ($matches as $key => $line) {
                if ($line[$xxxHeaderReversed[$column]] !== $matchingValue) {
                    unset($matches[$key]);
                }
            }
        } else {
            return null;
        }
    }

    return count($matches) > 0 ? $matches : null;
};
/**
 * Find lines with specific column content in telefonica-data
 * 
 * @return array|null
 */
$findInDataCsv = function ($matching) use (&$dataCsv, &$dataHeaderReversed)
{
    $matches = null;

    foreach ($matching as $column => $matchingValue) {
        if (null === $matches) {
            $matches = [];

            foreach ($dataCsv as $key => $line) {
                if ($line[$dataHeaderReversed[$column]] === $matchingValue) {
                    $matches[$key] = $line;
                }
            }
        } elseif (count($matches) > 0) {
            foreach ($matches as $key => $line) {
                if ($line[$dataHeaderReversed[$column]] !== $matchingValue) {
                    unset($matches[$key]);
                }
            }
        } else {
            return null;
        }
    }

    return count($matches) > 0 ? $matches : null;
};


// columns to compare
$columnsToCompare = [
    'KUNDENNUMMER',
    'VERTRAGSNUMMER',
    'VERTRAGSUNTERSCHRIFT',
    'VERTRAGSAKTIVIERUNG',
    'VERTRAGSDEAKTIVIERUNG',
    'STATUS',
    'ANREDE',
    'FIRMA',
    'NAME',
    'VORNAME',
    'Straße',
    'HAUSNR',
    'Hausnr.zus.',
    'PLZ',
    'ORT',
    'Emailadr.',
    'GEBURTSDATUM',
    'ORTSTEIL',
    'ADRESSTYPE',
    'PRODUCT_ID',
    'PRODUKTBESCHREIBUNG',
    'KATEGORIE',
    'Preis netto',
    'Prod.Aktivierung',
    'Prod.Deaktivierung',
    'Vertragsunterschrift am',
    'Lastschriftzahler',
    'BLZ',
    'KONTONUMMER',
    'IBAN',
    'BIC',
    'KTOINHABER',
    'RABATT',
    'ABRECHNUNGSGRUPPE',
    'KUNDENGRUPPE',
    'MAHNSPERRE',
    'Fritz Box Seriennr',
    'Line ID',
    'COV_ID',
    'ACS',
    'Aktivierung Telefonie',
    'PORTIERUNGSTERMIN',
    'SUBSCRIBER_ID',
    'CPRODUCTID',
    'Mandatsreferenz',
    'Web-Benutzername',
    'Web-Passwort',
    //'PIN FB',
    'Telefonbuch',
    'Auskunft',
    'Digitale Medien',
    'Inverssuche',
    'EVN',
    'anonymisieren',
    'reduziert',
    //'Interne Bestellnummer',
    '2.Ltg',
    'Land1',
    'Land2',
    'Papierrg',
    'TEL_RUFNR',
    'TEL_ANREDE',
    'TEL_FIRMA',
    'TEL_NAME',
    'TEL_VORNAME',
    'TEL_Straße',
    'TEL_HAUSNR',
    'TEL_Hausnr.zus.',
    'TEL_PLZ',
    'TEL_ORT',
    'INSTAL_ANREDE',
    'INSTAL_FIRMA',
    'INSTAL_NAME',
    'INSTAL_VORNAME',
    'INSTAL_Straße',
    'INSTAL_HAUSNR',
    'INSTAL_Hausnr.zus.',
    'INSTAL_PLZ',
    'INSTAL_ORT',
];



require __DIR__.'/../_conf/database.inc';


$comparedKundennummern = [];

echo "starting..\n";

$counter = 0;
$counterInData = 0;
$counterInXxx = 0;

foreach ($dataCsv as $key => $line) {
    $line = array_map('utf8_encode', $line);
    $dataCsv[$key] = $line;

    $kundennummer = $line[$dataHeaderReversed['KUNDENNUMMER']];

    if (empty($kundennummer)) {
        var_dump($line);
        continue;
    }

    if (isset($comparedKundennummern[$kundennummer])) {
        continue;
    }

    $ortsteil = trim($line[$dataHeaderReversed['ORTSTEIL']]);

    if ('Cluster 28' !== $ortsteil) {
        continue;
    }

    $counter++;

    // skip all further lines with this KUNDENNUMMER
    $comparedKundennummern[$kundennummer] = true;

    $inData = $findInDataCsv([
        'KUNDENNUMMER' => $kundennummer,
    ]);

    $inXxx = $findInXxxCsv([
        'KUNDENNUMMER' => $kundennummer,
    ]);

    $counterInData += count($inData);
    $counterInXxx += count($inXxx);

    if (count($inXxx) !== count($inData)) {
        //var_dump(count($inXxx), count($inData), $kundennummer);
        echo "in data\n";
        foreach ($inData as $l) {
            echo implode('###;###', $l)."\n";    
        }
        
        echo "\nin xxx\n";
        if (is_array($inXxx) && !empty($inXxx)) {
            foreach ($inXxx as $l) {
                echo implode('###;###', $l)."\n";    
            }
        } else {
            echo "leer\n";
        }
        continue;
    }

    /*foreach ($inData as $value) {

    }*/
}

echo "\nreached end!\n";

var_dump($counter);
var_dump($counterInData);
var_dump($counterInXxx);
