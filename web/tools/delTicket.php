<?php
  require_once ('../_conf/database.inc');
  require_once ('_conf/database.inc');

  $time = time() - 3600;

  $db_move = $dbegw->prepare($sql['move_tickets_extra']);
  $db_move->bind_param('i', $time);
  $db_move->execute();
  $db_move->close();

  $db_move = $dbegw->prepare($sql['move_tickets']);
  $db_move->bind_param('i', $time);
  $db_move->execute();
  $db_move->close();
 
  $db_del = $dbegw->prepare($sql['del_tickets_extra']);
  $db_del->bind_param('i', $time);
  $db_del->execute();
  $db_del->close();

  $db_del = $dbegw->prepare($sql['del_tickets']);
  $db_del->bind_param('i', $time);
  $db_del->execute();
  $db_del->close();

  $dbegw->close();
?>
