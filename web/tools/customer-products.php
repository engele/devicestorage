<?php

class Customer
{
    public $id;
    public $clientId;
    public $network;

    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->clientId = $data['clientid'];
        $this->network = $data['network'];
    }
}

class Product
{
    public $name;
    public $customers = [];
    public $countCustomersByNetwork = [];
    
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    public function addCustomer(Customer $customer)
    {
        $this->customers[] = $customer;

        if (!isset($this->countCustomersByNetwork[$customer->network])) {
            $this->countCustomersByNetwork[$customer->network] = [];
        }

        $this->countCustomersByNetwork[$customer->network][] = $customer;
    }
}



$networks = [];

$db = new mysqli('10.20.6.6', 'zkv', 'zkv', 'zkv');

$query = $db->query('SELECT * FROM `customers` WHERE (`connection_activation_date` IS NOT NULL AND `connection_activation_date` != "") AND (`wisocontract_canceled_date` IS NULL OR `wisocontract_canceled_date` = "")');

$list = [];

while ($customer = $query->fetch_assoc()) {
    if (empty($customer['purtel_product']) && empty($customer['productname'])) {
        continue;
    }

    if (empty($customer['network'])) {
        $firstDot = strpos($customer['clientid'], '.');
        $customer['network'] = substr($customer['clientid'], 0, $firstDot);
    }

    $networks[$customer['network']] = true;

    $productName = !empty($customer['purtel_product']) ? $customer['purtel_product'] : $customer['productname'];

    if (!isset($list[$productName])) {
        $product = new Product();
        $product->setName($productName);

        $list[$productName] = $product;
    } else {
        $product = $list[$productName];
    }

    $product->addCustomer(new Customer($customer));
}

$sort = [];

foreach ($list as $productName => $product) {
    $count = count($product->customers);

    if (!isset($sort[$count])) {
        $sort[$count] = [];
    }

    $sort[$count][] = $product;
}

/*
krsort($sort);

foreach ($sort as $count => $products) {
    foreach ($products as $product) {
        echo "(".$count.") ".$product->name."\n";

        $c_sort = [];

        foreach ($product->countCustomersByNetwork as $network => $customers) {
            $c_count = count($customers);

            if (!isset($c_sort[$c_count])) {
                $c_sort[$c_count] = [];
            }

            $c_sort[$c_count][] = $network;
        }

        krsort($c_sort);

        foreach ($c_sort as $c_count => $networks) {
            foreach ($networks as $network) {
                echo "    (".$c_count.") ".$network."\n";
            }
        }
    }
}
*/





$csv = [];

ksort($networks);

$networkNames = array_keys($networks);

array_unshift($networkNames, '');

$csv[] = $networkNames;

foreach ($sort as $count => $products) {
    foreach ($products as $product) {
        $row = [];
        $row[] = $product->name;

        foreach ($networks as $network => $true) {
            $count = 0;

            if (isset($product->countCustomersByNetwork[$network])) {
                $count = count($product->countCustomersByNetwork[$network]);
            }

            $row[] = $count;
        }

        $csv[] = $row;
    }
}


$csvHandle = fopen('php://output', 'w');

foreach ($csv as $key => $array) {
    if (0 === $key) {
        $array = array_map(function ($x) {
            return " ".$x;
        }, $array);
    }
    
    fputcsv($csvHandle, $array, ';');
}

fclose($csvHandle);







?>

