<?php
  require_once('../config.inc');
  require_once('../_conf/database.inc');
  require_once('_conf/database.inc');

  $cust      = array();
  $periode  = date ('Ym');
  // kv stammdaten
  $db_result = $db->query($sql['customer']);

  while ($db_row = $db_result->fetch_assoc()) {
    $cust[$db_row['clientid']]['wkv'] = $db_row;
  }

  // purtel stammdaten
  foreach ($cust AS $temp) {
    if ($temp['wkv']['purtel_login']) {
      $purtel_root  = array();
      $username     = '506716';
      $password     = 'KaQu07GTb1D9vRBC';
      $action       = 'stammdatenexport';
      $parameter    = array (
        'periode'   => $periode,
        'erweitert' => '9',
        'anschluss' => $temp['wkv']['purtel_login'],
      );    
      $PurtelUrl  = 'https://ipcom.purtel.com/index.php?super_username=' . urlencode($username) . '&super_passwort=' . urlencode($password) . '&action=' . urlencode($action);
      foreach ($parameter as $key => $value) {
        $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
      }
      $curl = curl_init ($PurtelUrl);
      $curl_options = array (
        CURLOPT_URL             => $PurtelUrl,
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
      );
      curl_setopt_array($curl, $curl_options);
      $result = curl_exec($curl);
      $lines  = explode ("\n", $result);
      array_pop($lines);
      $headline_csv = array_shift ($lines);
      $headline     = str_getcsv($headline_csv, ';');
      while ($line = array_shift ($lines)) {
        $purt_csv = $line;
        $purt     = str_getcsv($purt_csv, ';');
        foreach ($headline as $ind => $head) {
          $temp1[$head] = utf8_encode($purt[$ind]);
        }
        $cust[$temp1['kundennummer_extern']]['purtel'] = $temp1;
      }
      curl_close($curl);
    }
  }
  $db_result->close();
  $db->close();

  echo "<hr>";
  echo "<table border=1>";
  echo "<tr><th>Feld</th><th>KV</th><th>Purtel</th></tr>";
  foreach ($cust as $temp) {
    echo "<tr bgcolor='#CACACA'><td><strong>Kundennummer</strong></td><td><strong>".$temp['wkv']['clientid']."</strong></td><td><strong>".$temp['purtel']['kundennummer_extern']."</strong></td></tr>";
    echo "<tr><td>Purtelnummer</td><td>".$temp['wkv']['purtel_login']."</td><td>".$temp['purtel']['anschluss']."</td></tr>";
    echo "<tr><td>Kontoinhaber</td><td>".$temp['wkv']['bank_account_holder_lastname']."</td><td>".$temp['purtel']['Kontoinhaber']."</td></tr>";
    echo "<tr><td>Bank</td><td>".$temp['wkv']['bank_account_bankname']."</td><td>".$temp['purtel']['BezeichnungBank1']."</td></tr>";
    echo "<tr><td>IBAN</td><td>".$temp['wkv']['bank_account_iban']."</td><td>".$temp['purtel']['IBAN']."</td></tr>";
    echo "<tr><td>BIC</td><td>".$temp['wkv']['bank_account_bic_new']."</td><td>".$temp['purtel']['BIC']."</td></tr>";
  }
  echo "</table>";
  ?>
