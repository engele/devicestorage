<?php

require __DIR__.'/../_conf/database.inc';

$q = $db->query("SELECT p.*, c.`clientid` 
    FROM `purtel_account` p 
    INNER JOIN `customers` c ON c.`id` = p.`cust_id` 
    WHERE (p.`cust_id` BETWEEN 5802 AND 6246) OR (c.`customer_id` IN (12209, 12705, 12834, 13429, 13605, 14899, 15156, 18952))");


$ignoreCustomer = [
];


$sortedByCustomerLogin = [];

while ($d = $q->fetch_assoc()) {
    if (in_array($d['cust_id'], $ignoreCustomer)) {
        continue;
    }

    $customer = $d['cust_id'];

    if (!isset($sortedByCustomerLogin[$customer])) {
        $sortedByCustomerLogin[$customer] = [];
    }

    $sortedByCustomerLogin[$customer][] = $d;
}


$outArray = [
    'anschlussnummer' => null,
    'Kundennummer_extern' => null,
    'ONKZ' => null,
    'MSN 1' => null,
    'MSN 2' => null,
    'MSN 3' => null,
    'MSN 4' => null,
    'MSN 5' => null,
    'MSN 6' => null,
    'MSN 7' => null,
    'MSN 8' => null,
    'MSN 9' => null,
    'MSN 10' => null,
    'Kopfnummer' => null,
    'Abfragestelle' => null,
    'Block' => null,
    'PKI_AB' => 'D061',
    'PKI_AUF' => 'D027',
    'Termin' => '07.03.2018',
    'WBCI Vorab_ID' => 'DEU.COMIN.VMAS', // + 000001
];


$counter = 4;

$csvHandle = fopen('php://output', 'w');

fputcsv($csvHandle, array_keys($outArray), ';');

foreach ($sortedByCustomerLogin as $customerId => $data) {
    $purtelMasterAccount = null;
    $externalCustomerId = null;
    $onkz_ = null;
    $phonenumbers = [];

    foreach ($data as $purtelAccount) {
        if ($purtelAccount['master'] === '1') {
            $purtelMasterAccount = $purtelAccount['purtel_login'];
            $externalCustomerId = $purtelAccount['clientid'];
        }

        if (empty($purtelAccount['nummer'])) {
            continue;
        }

        /*$onkzLength = 4;

        if (0 === strpos($purtelAccount['nummer'], '0845')) {
            $onkzLength = 5;
        }

        $onkz = substr($purtelAccount['nummer'], 0, $onkzLength);

        if (null !== $onkz_ && $onkz_ !== $onkz) {
            var_dump($customerId);
            die('not same onkz');
        }

        $onkz_ = $onkz;
        */
        $onkzLength = strlen($purtelAccount['area_code']);

        $onkz = $purtelAccount['area_code'];

        if (null !== $onkz_ && $onkz_ !== $onkz) {
            var_dump($customerId);
            die('not same onkz');
        }

        $onkz_ = $onkz;

        $phonenumbers[] = substr($purtelAccount['nummer'], $onkzLength);
    }

    if (null === $purtelMasterAccount) {
        var_dump($customerId);
        die("no master");
    }

    if (empty($externalCustomerId)) {
        var_dump($customerId);
        die("no clientid");
    }

    if (empty($phonenumbers)) {
        continue;
    }

    if (count($phonenumbers) > 10) {
        var_dump($customerId);
        die("too many phonenumbers");
    }

    $outData = $outArray;
    $outData['anschlussnummer'] = $purtelMasterAccount;
    $outData['Kundennummer_extern'] = $externalCustomerId;
    $outData['ONKZ'] = $onkz_;
    $outData['WBCI Vorab_ID'] .= '1'.str_pad((string) ++$counter, 5, '0', STR_PAD_LEFT);


    foreach ($phonenumbers as $index => $number) {
        $outData['MSN '.($index + 1)] = $number;
    }

    fputcsv($csvHandle, $outData, ';');
}

fclose($csvHandle);




