<?php
require_once('../config.inc');
require_once('../_conf/database.inc');
require_once('../_conf/global-config.php');
require_once('../vendor/wisotel/configuration/Configuration.php');
$axirosAcs = \Wisotel\Configuration::get('axirosAcs');


$sql       = "SELECT id, clientid, mac_address FROM customers";
$acs_lst   = $axirosAcs['url']."/live/CPEManager/AXServiceStorage/Interfaces/rest/v1/services/voice/cpeid/%s/action/getList";

$acs_url   = $axirosAcs['url']."/live/CPEManager/AXServiceStorage/Interfaces/rest/v1/services/voice/cpeid/%s/action/modify";
$action    = '';
$voip      = array();
$acs_pppoe = array();
$acs_voice = array();
$username  = $axirosAcs['username'];
$password  = $axirosAcs['password'];

$acs_post = json_encode($acs_pppoe);
$acs_post = str_replace('[]', '{}', $acs_post);

$db_mac = $db->query($sql);
echo "<table border = 1>\n";
echo "<tr><th>count</th><th>id</th><th>clientid</th><th>mac_address</th><th>cpeid</th><th>Passwd</th><th>area_code</th><th>username</th><th>directory_number</th><th>registrar</th><th>creation_time</th></tr>";
while ($db_mac_row = $db_mac->fetch_assoc()) {
#    if ($db_mac_row['mac_address'] == '00040E-3810D5600D43') {
    if ($db_mac_row['mac_address']) {
        $acs['acsId'] = substr($db_mac_row['mac_address'],7,12);
        $acs['acsPw'] = substr($db_mac_row['mac_address'],-12,12);
        $url          = sprintf ($acs_lst, $acs['acsId']);
        $curlHandler = curl_init();
        curl_setopt($curlHandler, CURLOPT_URL, $url);
        curl_setopt($curlHandler, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curlHandler, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($curlHandler, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curlHandler, CURLOPT_USERPWD, $username.':'.$password);
        curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandler, CURLOPT_POST, true);
        curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $acs_post);
        $execstr  = curl_exec($curlHandler);
        curl_close($curlHandler);
        $exec = json_decode($execstr, true);
        foreach ($exec['result']['Services'] AS $temp) {
           if (preg_match ('/^0/', $temp['properties']['area_code'])) { 
                $voipitem['id']               = $db_mac_row['id'];
                $voipitem['clientid']         = $db_mac_row['clientid'];
                $voipitem['mac_address']      = $db_mac_row['mac_address'];
                $voipitem['acsId']            = $acs['acsId'];
                $voipitem['acsPw']            = $acs['acsPw'];
                $voipitem['area_code']        = substr($temp['properties']['area_code'],1);
                $voipitem['username']         = $temp['properties']['username'];
                $voipitem['password']         = $temp['properties']['password'];
                $voipitem['directory_number'] = $temp['properties']['directory_number'];
                $voipitem['registrar']        = $temp['properties']['registrar'];
                $voipitem['creation_time']    = date('d.m.Y H:i:s', $temp['creation_time']);
                array_push ($voip, $voipitem);
           }
        }
    }
}
$db_mac->free();

foreach ($voip AS $key => $value) {
    echo "<tr>";
    echo "<td>".$key."</td>";
    echo "<td>".$value['id']."</td>";
    echo "<td>".$value['clientid']."</td>";
    echo "<td>".$value['mac_address']."</td>";
    echo "<td>".$value['acsId']."</td>";
    echo "<td>".$value['acsPw']."</td>";
    echo "<td>".$value['area_code']."</td>";
    echo "<td>".$value['username']."</td>";
    echo "<td>".$value['directory_number']."</td>";
    echo "<td>".$value['registrar']."</td>";
    echo "<td>".$value['creation_time']."</td>";

    $acs_voice['CommandOptions']                         = array();
    $acs_voice['ServiceIdentifiers']['cpeid']            = $value['acsId'];
    $acs_voice['ServiceIdentifiers']['directory_number'] = $value['directory_number'];
    $acs_voice['ServiceParameters']['area_code']         = $value['area_code'];
    $acs_voice['ServiceParameters']['registrar']         = $value['registrar'];
    // $acs_voice['ServiceParameters']['username']          = $value['username'];
    // $acs_voice['ServiceParameters']['password']          = $value['password'];
    $acs_voice['ServiceParameters']['directory_number']  = $value['directory_number'];
    $acs_post = json_encode($acs_voice);
    $acs_post = str_replace('[]', '{}', $acs_post);
    $url      = sprintf ($acs_url, $value['acsId']);

    $curlModify = curl_init();
    curl_setopt($curlModify, CURLOPT_URL, $url);
    curl_setopt($curlModify, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($curlModify, CURLOPT_FRESH_CONNECT, TRUE);
    curl_setopt($curlModify, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curlModify, CURLOPT_USERPWD, $username.':'.$password);
    curl_setopt($curlModify, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curlModify, CURLOPT_POST, true);
    curl_setopt($curlModify, CURLOPT_POSTFIELDS, $acs_post);
    $modstr  = curl_exec($curlModify);
    curl_close($curlModify);
    echo "<td>".$modstr."</td>";
    echo "</tr>";
}
echo "\n</table>\n";

?>
