<?php

/**
 * Read contents from csv-file
 *
 * @param string $filename
 *
 * @throws InvalidArgumentException
 *
 * @return array
 */
function readCsvFile($filename)
{
    if (!file_exists($filename) || !is_readable($filename)) {
        throw new InvalidArgumentException('Can not open file for reading');
    }

    $content = file_get_contents($filename);

    if (!is_string($content) && !empty($content)) {
        throw new InvalidArgumentException('invalid file');
    }

    $delimiter = null;

    foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
        if (false !== strpos($content, $delimiter_)) {
            $delimiter = $delimiter_;
            break;
        }
    }

    if (null == $delimiter) {
        throw new InvalidArgumentException('can not use file');
    }

    return array_map(function ($line) {
        return str_getcsv($line, ';');
    }, explode($delimiter, $content));
}

$productsCsv = readCsvFile('/tmp/Matching_Produkte_und_Tarife_purtel_&_Dimari_inkl._SAP_Nummern.csv');
//$telefonicaCsv = readCsvFile('/tmp/telefonica.csv');

$productsHeader = array_shift($productsCsv);
$productsHeaderReversed = [];
//$telefonicaHeader = array_shift($telefonicaCsv);
//$telefonicaHeaderReversed = [];

foreach ($productsHeader as $column => $name) {
    $productsHeaderReversed[$name] = $column;
}
//foreach ($telefonicaHeader as $column => $name) {
//    $telefonicaHeaderReversed[$name] = $column;
//}

//if (count(end($telefonicaCsv)) !== count($telefonicaHeader)) {
//    array_pop($telefonicaCsv);
//}
if (count(end($productsCsv)) !== count($productsHeader)) {
    array_pop($productsCsv);
}


require __DIR__.'/../_conf/database.inc';


$products = [];
$getProductsQuery = $db->query("SELECT * FROM `products`");

while ($product = $getProductsQuery->fetch_assoc()) {
    if (!empty($product['identifier'])) {
        $products[$product['identifier']] = $product;
    } else {
        $products[$product['name']] = $product;
    }
}

$getProductsQuery->close();



/*
    # - Neue Produkte eintragen
    # - Produkt-Typ updaten (auch Preise usw)
    # - Haupt-Produkte in richtige Tabelle übertragen
    # - Kunden das richtige Haupt-Produkt zuweisen
    - (Kunden die Cross-Selling-Produkte zuweisen)

    - Beim Import wurden mehrfach gleiche Rufnummer importiert!!!!!
*/


// Import & update `products`
foreach ($productsCsv as $key => $line) {
    $status = trim($line[$productsHeaderReversed['Status Plattform Purtel']]);

    if ('entfällt' === $status) {
        continue;
    }

    $identifier = trim($line[$productsHeaderReversed['Dimari Produkt ID']]);
    $name = trim($line[$productsHeaderReversed['PRODUKTBESCHREIBUNG']]);
    $purtelProductId = trim($line[$productsHeaderReversed['Purtel Produkt ID']]);
    $purtelProductId = str_replace('ID-', '', $purtelProductId);
    $category = trim($line[$productsHeaderReversed['KATEGORIE']]);
    $priceNet = trim($line[$productsHeaderReversed['Preis netto']]);
    $priceGross = trim($line[$productsHeaderReversed['Preis Brutto']]);
    $type = trim($line[$productsHeaderReversed['Produktart']]);

    foreach (['priceNet', 'priceGross'] as $var) {
        if (empty(${$var})) {
            ${$var} = null;

            continue;
        }

        ${$var} = str_replace(',', '.', ${$var});
        ${$var} = (float) ${$var};
    }

    if (empty($identifier)) {
        die("empty identifier");
    }

    if (empty($purtelProductId) || 1 !== preg_match('/[0-9]+/', $purtelProductId)) {
        die(var_dump('no purtel id'));
    }

    if (false !== strpos($type, 'Vorleistungsprodukt')) {
        $type = 'Crossselling/Vorleistungsprodukt';
    }

    if (isset($products[$identifier])) {
        // maybe update product

        $query = sprintf("UPDATE `products` SET %s = %s, %s = %s, %s = %s, %s = %s, %s = %s, %s = %s, %s = %s WHERE `id` = %d",
            '`name`',
            "'".$name."'",
            '`identifier`',
            "'".$identifier."'",
            '`default_price_net`',
            null === $priceNet ? 'NULL' : $priceNet,
            '`default_price_gross`',
            null === $priceGross ? 'NULL' : $priceGross,
            '`item_category`',
            null === $category ? 'NULL' : "'".$category."'",
            '`purtel_product_id`',
            null === $purtelProductId ? 'NULL' : "'".$purtelProductId."'",
            '`type`',
            null === $type ? 'NULL' : "'".$type."'",

            $products[$identifier]['id']
        );

        $db->query($query);

        if (!empty($db->error)) {
            die('error '.$db->error);
        }
    } else {
        // product not found by identifier
        // try products without identifier by name

        if (isset($products[$name])) {
            // maybe update product

            die('da ist was');
        } else {
            // product not found at all
            
            $query = sprintf("INSERT INTO `products` (%s, %s, %s, %s, %s, %s, %s, %s) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                '`name`',
                '`active`',
                '`identifier`',
                '`default_price_net`',
                '`default_price_gross`',
                '`item_category`',
                '`purtel_product_id`',
                '`type`',

                "'".$name."'",
                "1",
                "'".$identifier."'",
                null === $priceNet ? 'NULL' : $priceNet,
                null === $priceGross ? 'NULL' : $priceGross,
                null === $category ? 'NULL' : "'".$category."'",
                null === $purtelProductId ? 'NULL' : "'".$purtelProductId."'",
                null === $type ? 'NULL' : "'".$type."'"
            );

            $db->query($query);

            if (empty($db->error)) {
                $products[$identifier] = [
                    'id' => $db->insert_id,
                    'name' => $name,
                    'active' => 1,
                    'identifier' => $identifier,
                    'default_price_net' => $priceNet,
                    'default_price_gross' => $priceGross,
                    'item_category' => $category,
                ];
            }
        }
    }
}





$productCategoryToProdukteGroupId = [
    'Grundgebühren' => 100,
];

// create missing `produkte`
foreach ($products as $product) {
    if ('Tarif' !== $product['type']) {
        continue;
    }

    $existsQuery = $db->query("SELECT * FROM `produkte` WHERE `produkt_bezeichnung` = '".$product['name']."'");

    if ($existsQuery->num_rows > 0) {
        continue;
    } else {
        $identifier = 'Dimari-'.$product['identifier'];

        $existsQuery = $db->query("SELECT * FROM `produkte` WHERE `produkt` = '".$identifier."'");

        if ($existsQuery->num_rows > 0) {
            continue;
        }

        // insert into
        $name = $product['name'];
        $priceNet = $product['default_price_net'];
        $priceGross = $product['default_price_gross'];
        $groupId = null;

        if (isset($productCategoryToProdukteGroupId[$product['item_category']])) {
            $groupId = $productCategoryToProdukteGroupId[$product['item_category']];
        }

        $query = sprintf("INSERT INTO `produkte` (%s, %s, %s, %s, %s, %s) VALUES (%s, %s, %s, %s, %s, 1)",
            '`produkt`',
            '`group_id`',
            '`produkt_bezeichnung`',
            '`Preis_Netto`',
            '`Preis_Brutto`',
            '`active`',

            "'".$identifier."'",
            null === $groupId ? 'NULL' : "'".$groupId."'",
            "'".$name."'",
            null === $priceNet ? 'NULL' : $priceNet,
            null === $priceGross ? 'NULL' : $priceGross
        );

        $db->query($query);
        
        if (!empty($db->error)) {
            die('error-'.$db->error);
        }
    }
}






/*
customer_products
+------------------------------+---------------+------+-----+---------+----------------+
| Field                        | Type          | Null | Key | Default | Extra          |
+------------------------------+---------------+------+-----+---------+----------------+
| id                           | int(11)       | NO   | PRI | NULL    | auto_increment |
| product_id                   | int(11)       | YES  | MUL | NULL    |                |
| customer_id                  | int(11)       | NO   |     | NULL    |                |
| price_net                    | decimal(12,5) | YES  |     | NULL    |                |
| activation_date              | date          | YES  |     | NULL    |                |
| deactivation_date            | date          | YES  |     | NULL    |                |
| propperly_copyed_to_customer | tinyint(1)    | NO   |     | NULL    |                |
+------------------------------+---------------+------+-----+---------+----------------+
*/
die("dddd");
// Assign "main-product" to customer
$findCustomersMailProducts = $db->query("SELECT cp.*, p.`identifier`, p.`type` 
    FROM customer_products cp 
    INNER JOIN `products` p 
    ON p.`id` = cp.`product_id` 
    WHERE p.`type` = 'Tarif' AND cp.`propperly_copyed_to_customer` != 1"
);

while ($data = $findCustomersMailProducts->fetch_assoc()) {
    $identifier = 'Dimari-'.$data['identifier'];

    $db->query("UPDATE `customers` 
        SET `productname` = '".$identifier."', `purtel_product` = '".$identifier."' 
        WHERE `id` = ".$data['customer_id']);

    if (!empty($db->error)) {
        var_dump('data-id', $data['id']);
        die('error-'.$db->error);
    }

    // mark this entity as "propperly_copyed_to_customer"
    $db->query("UPDATE `customer_products` SET `propperly_copyed_to_customer` = 1 WHERE `id` = ".$data['id']);

    if (!empty($db->error)) {
        var_dump('data-id', $data['id']);
        die('error-'.$db->error);
    }
}














/*

'Einmalgebühren' => ,
'Hardware' => ,
'Servicepauschalen' => ,
'Features' => ,
'Parallel Call Kapazität' => ,
'Gutschriften' => ,
'Grundgebühren CS' => ,
'Optionen' => ,
'Zusatzoptionen' => ,

*/