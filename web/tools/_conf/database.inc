<?php
$sql = array (
    'customer' => "
        SELECT * FROM customers WHERE version IS NULL AND clientid NOT LIKE '0000_.%' ORDER BY clientid
    ",

    'max_clientid' => "
        SELECT MAX(clientid) FROM customers
        WHERE clientid LIKE ?
        ORDER BY clientid
    ",

    'produkte' => "
        SELECT produkt, produkt_bezeichnung FROM produkte
    ",

    'optionen' => "
        SELECT option, option_bezeichnung FROM optionen
    ",
    
    // delete tickets
    'move_tickets' => "
        INSERT INTO egw_tracker_wisotel 
        SELECT * FROM egw_tracker
        WHERE (tr_description IS NULL
        AND (tr_startdate IS NULL OR tr_startdate < ?))
        OR tr_tracker = 120
    ",
    
    'del_tickets' => "
        DELETE FROM egw_tracker
        WHERE (tr_description IS NULL
        AND (tr_startdate IS NULL OR tr_startdate < ?))
        OR tr_tracker = 120
    ",
    
    'move_tickets_extra' => "
        INSERT INTO egw_tracker_extra_wisotel 
        SELECT * FROM egw_tracker_extra 
        WHERE tr_id IN (
            SELECT tr_id FROM egw_tracker 
            WHERE (tr_description IS NULL
            AND (tr_startdate IS NULL OR tr_startdate < ?))
            OR tr_tracker = 120
        )
    ",

    'del_tickets_extra' => "
        DELETE FROM egw_tracker_extra 
        WHERE tr_id IN (
            SELECT tr_id FROM egw_tracker 
            WHERE (tr_description IS NULL
            AND (tr_startdate IS NULL OR tr_startdate < ?))
            OR tr_tracker = 120
        )
    "
);

$GLOBALS['sql'] = & $sql;
?>
