<?php

/**
 * Read contents from csv-file
 *
 * @param string $filename
 *
 * @throws InvalidArgumentException
 *
 * @return array
 */
function readCsvFile($filename)
{
    if (!file_exists($filename) || !is_readable($filename)) {
        throw new InvalidArgumentException('Can not open file for reading');
    }

    $content = file_get_contents($filename);

    if (!is_string($content) && !empty($content)) {
        throw new InvalidArgumentException('invalid file');
    }

    $delimiter = null;

    foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
        if (false !== strpos($content, $delimiter_)) {
            $delimiter = $delimiter_;
            break;
        }
    }

    if (null == $delimiter) {
        throw new InvalidArgumentException('can not use file');
    }

    return array_map(function ($line) {
        return str_getcsv($line, ';');
    }, explode($delimiter, $content));
}

$dataCsv = readCsvFile('/tmp/merge-rufnummer-mit-telefonica-liste.csv');
$xxxCsv = readCsvFile('/tmp/xxx.csv');

$dataHeader = array_shift($dataCsv);
$dataHeaderReversed = [];

foreach ($dataHeader as $column => $name) {
    $dataHeaderReversed[$name] = $column;
}

if (count(end($dataCsv)) !== count($dataHeader)) {
    array_pop($dataCsv);
}

$xxxHeader = array_shift($xxxCsv);
$xxxHeaderReversed = [];

foreach ($xxxHeader as $column => $name) {
    $xxxHeaderReversed[$name] = $column;
}

if (count(end($xxxCsv)) !== count($xxxHeader)) {
    array_pop($xxxCsv);
}


$dataByCustomer = [];

// sort by customer
foreach ($dataCsv as $line) {
    foreach ($line as $column => $value) {
        $line[$column] = trim($value);
    }

    $customerId = $line[$dataHeaderReversed['customerid']];

    if (!isset($dataByCustomer[$customerId])) {
        $dataByCustomer[$customerId] = [];
    }

    $dataByCustomer[$customerId][] = $line;
}


//var_dump(count($dataByCustomer));
//var_dump(count($dataCsv), count($dataByCustomer), $dataByCustomer);



/**
 * Find lines with specific column content in telefonica-data
 * 
 * @return array|null
 */
$findInXxx = function ($matching) use (&$xxxCsv, &$xxxHeaderReversed)
{
    $matches = null;

    foreach ($matching as $column => $matchingValue) {
        if (null === $matches) {
            $matches = [];

            foreach ($xxxCsv as $key => $line) {
                if ($line[$xxxHeaderReversed[$column]] === $matchingValue) {
                    $matches[$key] = $line;
                }
            }
        } elseif (count($matches) > 0) {
            foreach ($matches as $key => $line) {
                if ($line[$xxxHeaderReversed[$column]] !== $matchingValue) {
                    unset($matches[$key]);
                }
            }
        } else {
            return null;
        }
    }

    return count($matches) > 0 ? $matches : null;
};



require __DIR__.'/../_conf/database.inc';


$output = [];

foreach ($dataByCustomer as $telefonicaCustomerId => $lines) {
    foreach ($lines as $key => $line) {
        $line['dimariId'] = null;
        $line['ikv-customer-id'] = null;
        $line['ikv-id'] = null;

        $inXxx = $findInXxx(['Rufnummer' => '49'.$line[$dataHeaderReversed['rn']]]);

        $rn = '0'.$line[$dataHeaderReversed['rn']];

        // see if one customer already has this rn 
        $query = "SELECT pa.*, c.`clientid`, c.`customer_id`, c.`id` as ikv_id 
            FROM `purtel_account` pa INNER JOIN `customers` c ON c.`id` = pa.`cust_id` 
            WHERE pa.`nummer` = '".$rn."'";

        $q = $db->query($query);

        if ($q->num_rows > 0) {
            // found rn
            $d = $q->fetch_assoc();

            $line['dimariId'] = $d['customer_id'];
            $line['ikv-customer-id'] = $d['clientid'];
            $line['ikv-id'] = $d['ikv_id'];

            $output[] = $line; // add to output

            continue;
        }

        // see if one customer already has this diamriId
        if (is_array($inXxx)) {
            foreach ($inXxx as $inXxxLine) {
                $dimariId = trim($inXxxLine[3]);

                if (1 === preg_match('/^\d+$/', $dimariId)) {
                    $line['dimariId'] = $dimariId;

                    $query = "SELECT `clientid`, `id` FROM `customers` WHERE `customer_id` = '".$dimariId."'";

                    $q = $db->query($query);

                    if ($q->num_rows > 0) {
                        // found dimariId
                        $d = $q->fetch_assoc();

                        $line['ikv-customer-id'] = $d['clientid'];
                        $line['ikv-id'] = $d['id'];

                        $output[] = $line; // add to output

                        continue 2;
                    }
                }
            }
        }

        $output[] = $line; // add to output
    }
}


if (count($output) !== count($dataCsv)) {
    die(var_dump('passt nicht'));
}

/*
array(15) {
  [0]=>
  string(8) "reseller"
  [1]=>
  string(3) "ndc"
  [2]=>
  string(2) "rn"            +
  [3]=>
  string(10) "customerid"
  [4]=>
  string(9) "firstname"     +
  [5]=>
  string(8) "lastname"      +
  [6]=>
  string(6) "street"        +
  [7]=>
  string(11) "housenumber"  +
  [8]=>
  string(7) "zipcode"       +
  [9]=>
  string(4) "city"          +
  [10]=>
  string(9) "Rufnummer"    
  [11]=>
  string(6) "Status"        +
  [12]=>
  string(6) "CPE ID"        +
  [13]=>
  string(0) ""
  [14]=>
  string(0) ""
}
*/

echo 'Link zur IKV,IKV-Kundennummer,Dimari-Id,rufnummer,firstname,lastname,street,streetno,city,zipcode,Telefonica-Id,CPE-ID'."\n";

$clientIdStartingString = '00000.18.';

foreach ($output as $line) {
    $line['angelegt'] = null;

    $rn = '0'.$line[$dataHeaderReversed['rn']];

    if (!empty($line['ikv-customer-id'])) {
        echo sprintf('%s,"%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s"',
            '=HYPERLINK("http://ikv2.com-in.net/index.php?menu=customer&func=vertrag&id='.$line['ikv-id'].'"; "Zum Kunden (ikv)")',
            $line['ikv-customer-id'],
            $line['dimariId'],
            $rn,
            $line[$dataHeaderReversed['firstname']],
            $line[$dataHeaderReversed['lastname']],
            $line[$dataHeaderReversed['street']],
            $line[$dataHeaderReversed['housenumber']],
            $line[$dataHeaderReversed['city']],
            $line[$dataHeaderReversed['zipcode']],
            $line[$dataHeaderReversed['customerid']],
            $line[$dataHeaderReversed['CPE ID']]
        )."\n";

        continue;
    }

    $lastClientId = 0;

    $lastClientIdQuery = $db->query("SELECT `clientid` FROM `customers` 
        WHERE `clientid` LIKE '".$clientIdStartingString."%' ORDER BY `clientid` DESC LIMIT 1"
    );

    if (1 === $lastClientIdQuery->num_rows) {
        $lastClientId = $lastClientIdQuery->fetch_row();
        $lastClientId = $lastClientId[0];
        $lastClientId = explode('.', $lastClientId);
        $lastClientId = (int) end($lastClientId);
    }


    $clientId = $clientIdStartingString.str_pad(++$lastClientId, 4, "0", STR_PAD_LEFT);

    $comment = "Rufnummer: ".$rn."\n\n";

    if (!empty($line[$dataHeaderReversed['Status']])) {
        $comment .= "Status:\n".$line[$dataHeaderReversed['Status']]."\n";
    }

    $q = "INSERT INTO `customers` (`clientid`, `firstname`, `lastname`, `street`, `streetno`, `city`, `zipcode`, `comment`, `customer_id`, `mac_address`) 
        VALUES ('".$clientId."', 
            '".$line[$dataHeaderReversed['firstname']]."', 
            '".$line[$dataHeaderReversed['lastname']]."', 
            '".$line[$dataHeaderReversed['street']]."', 
            '".$line[$dataHeaderReversed['housenumber']]."', 
            '".$line[$dataHeaderReversed['city']]."', 
            '".$line[$dataHeaderReversed['zipcode']]."', 
            '".$comment."', 
            '".$line['dimariId']."', 
            '".$line[$dataHeaderReversed['CPE ID']]."')";
    
    //echo $q."\n\n";
    $db->query($q);

    if (!empty($db->error)) {
        die(var_dump($db->error));
    }

    $cId = $db->insert_id;

    echo sprintf('%s,"%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s"',
        '=HYPERLINK("http://ikv2.com-in.net/index.php?menu=customer&func=vertrag&id='.$cId.'"; "Zum Kunden (ikv)")',
        $clientId,
        $line['dimariId'],
        $rn,
        $line[$dataHeaderReversed['firstname']],
        $line[$dataHeaderReversed['lastname']],
        $line[$dataHeaderReversed['street']],
        $line[$dataHeaderReversed['housenumber']],
        $line[$dataHeaderReversed['city']],
        $line[$dataHeaderReversed['zipcode']],
        $line[$dataHeaderReversed['customerid']],
        $line[$dataHeaderReversed['CPE ID']]
    )."\n";
}

//var_dump($dataHeader);