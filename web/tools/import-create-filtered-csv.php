<?php

/**
 * Read contents from csv-file
 *
 * @param string $filename
 *
 * @throws InvalidArgumentException
 *
 * @return array
 */
function readCsvFile($filename)
{
    if (!file_exists($filename) || !is_readable($filename)) {
        throw new InvalidArgumentException('Can not open file for reading');
    }

    $content = file_get_contents($filename);

    if (!is_string($content) && !empty($content)) {
        throw new InvalidArgumentException('invalid file');
    }

    $delimiter = null;

    foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
        if (false !== strpos($content, $delimiter_)) {
            $delimiter = $delimiter_;
            break;
        }
    }

    if (null == $delimiter) {
        throw new InvalidArgumentException('can not use file');
    }

    return array_map(function ($line) {
        return str_getcsv($line, ';');
    }, explode($delimiter, $content));
}

$csv = readCsvFile('/tmp/20180301_emigranten_tanja.csv');

$csvHeader = array_shift($csv);
$csvHeaderReversed = [];

foreach ($csvHeader as $column => $name) {
    $csvHeaderReversed[$name] = $column;
}

// strip last empty line from csv-data
if (count(end($csv)) !== count($csvHeader)) {
    array_pop($csv);
}

/**
 * Matches filters
 * 
 * @param array $line
 * 
 * @return boolean
 */
function matchesFilter(array $line)
{
    global $csvHeaderReversed;

    if ('Cluster 33' === trim($line[$csvHeaderReversed['ORTSTEIL']])) {
        return true;
    }

    return false;
}

$filteredCsv = [];

foreach ($csv as $key => $line) {
    if (matchesFilter($line)) {
        $filteredCsv[$key] = & $csv[$key];
    }
}

$csvHandle = fopen('php://output', 'w');

fputcsv($csvHandle, $csvHeader, ';');

foreach ($filteredCsv as $key => $line) {
    fputcsv($csvHandle, $line, ';');
}

fclose($csvHandle);
