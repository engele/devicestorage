<?php
  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename=accounts_'.date ("Ym").'.csv');  

  require_once('../config.inc');

  $csv_handle = fopen('php://output', 'w');

  $purtel     = array();
  $username   = '506716';
  $password   = 'KaQu07GTb1D9vRBC';
  $action     = 'getbuchungen';
  $parameter  = array (
    'von_datum' => $fromDate,
    'bis_datum' => $toDate,
    'erweitert' => 3
  );    
  $PurtelUrl  = 'https://ipcom.purtel.com/index.php?super_username=' . urlencode($username) . '&super_passwort=' . urlencode($password) . '&action=' . urlencode($action);
  foreach ($parameter as $key => $value) {
    $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
  }
  $curl = curl_init ($PurtelUrl);
  $curl_options = array (
    CURLOPT_URL             => $PurtelUrl,
    CURLOPT_HEADER          => false,
    CURLOPT_RETURNTRANSFER  => true,
    CURLOPT_SSL_VERIFYPEER  => false,
  );
  curl_setopt_array($curl, $curl_options);
  $result       = curl_exec($curl);
  $lines        = explode ("\n", $result);
  array_pop($lines);
  $headline_csv = array_shift ($lines);
  $headline     = str_getcsv($headline_csv, ';');
  while ($line = array_shift ($lines)) {
    $purt_csv = $line;
    $purt     = str_getcsv($purt_csv, ';');
    foreach ($headline as $ind => $head) {
      $temp[$head] = $purt[$ind];
    }
    array_push($purtel,$temp);
  }
  curl_close($curl);
  $headline = array (
    utf8_decode('Buchungs-ID'),
    utf8_decode('Rechnungsnr.'),
    utf8_decode('Buchungsdatum'),
    utf8_decode('Mandant'),
    utf8_decode('Anschluß'),
    utf8_decode('Unterkonto'),
    utf8_decode('Kundennr.'),
    utf8_decode('Kundenart'),
    utf8_decode('Verwendungszweck'),
    utf8_decode('Betrag_Soll'),
    utf8_decode('Betrag_Haben'),
    utf8_decode('Mitarbeiter'),
    utf8_decode('Buchungsart'),
    utf8_decode('Erledigt'),
    utf8_decode('Entgeldtyp'),
  );
  fputcsv($csv_handle, $headline, ';');
  foreach ($purtel as $temp) {
    $temp['betrag_soll'] = str_replace ('.', ',', $temp['betrag_soll'] / 100);
    $temp['betrag_haben'] = str_replace ('.', ',', $temp['betrag_haben'] / 100);
    $temp['datum'] = date('d.m.Y h:m',strtotime($temp['datum']));
    if ($temp['kundenart'] == 1) $temp['kundenart'] = 'GK'; else $temp['kundenart'] = 'PK';
    if ($temp['erledigt'] == 1) $temp['erledigt'] = 'ja'; else $temp['erledigt'] = 'nein';
    switch ($temp['entgelttyp']){
      case 0:
        $temp['entgelttyp'] = 'Sonstige Buchungen';
        break;
      case 1:
        $temp['entgelttyp'] = 'Einmalig Produkt';
        break;
       case 2:
        $temp['entgelttyp'] = 'Einmalig CrossSelling';
        break;
      case 3:
        $temp['entgelttyp'] = 'Laufend Produkt';
        break;
      case 4:
        $temp['entgelttyp'] = 'Laufend CrossSelling';
        break;
      case 5:
        $temp['entgelttyp'] = 'Einmalig Modul';
        break;
      case 6:
        $temp['entgelttyp'] = 'Laufend Modul';
        break;
      case 7:
        $temp['entgelttyp'] = 'Verbindungen';
        break;
    }
    $csv = array (
      $temp['id'],
      $temp['renummer'],
      $temp['datum'],
      $temp['mandant'],
      $temp['anschluss'],
      $temp['unterkonto'],
      $temp['kundennummer_extern'],
      $temp['kundenart'],
      $temp['verwendungszweck'],
      $temp['betrag_soll'],
      $temp['betrag_haben'],
      $temp['mitarbeiter'],
      $temp['art'],
      $temp['erledigt'],
      $temp['entgelttyp'],
    );
    fputcsv($csv_handle, $csv, ';');
  }
  fclose ($csv_handle);
?>
