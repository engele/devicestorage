<?php

/*
 * Todo
 *
 * use area_code from purtel_account
 */



require __DIR__.'/../_conf/database.inc';
require_once __DIR__.'/../vendor/wisotel/configuration/Configuration.php';

$purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

//$query = $db->query("SELECT * FROM `purtel_account`");
//$query = $db->query("SELECT * FROM `purtel_account` WHERE `cust_id` BETWEEN 5802 AND 6246");
$query = $db->query("SELECT * FROM `purtel_account` WHERE `cust_id` = 5835");

$accounts = [];

while ($d = $query->fetch_assoc()) {
    if (0 === strpos($d['purtel_login'], '84')) {
        $accounts[] = $d;

        continue;
    }

    if (null === $d['purtel_login'] && 0 === strpos($d['nummer'], '08')) {
        $accounts[] = $d;

        continue;
    }
}


$accountsByUser = [];

foreach ($accounts as $account) {
    if (!isset($accountsByUser[$account['cust_id']])) {
        $accountsByUser[$account['cust_id']] = [];
    }

    $accountsByUser[$account['cust_id']][] = $account;
}


$skipUsers = [
];

var_dump(count($accountsByUser));

if (empty($accountsByUser)) {
    die('nothing to do');
}

foreach ($accountsByUser as $userId => $accounts) {
    //sleep(90);
    usleep(100000);

    echo "start: ".$userId."\n";

    if (in_array($userId, $skipUsers)) {
        echo "skipped\n\n";
        continue;
    }


    $customerQuery = $db->query("SELECT * FROM `customers` WHERE `id` = ".$userId);

    if ($customerQuery->num_rows !== 1) {
        die('kunde nicht gefunden');
    }

    $customer = $customerQuery->fetch_assoc();

    $network = [
        'id' => null,
        'id_string' => null,
        'name' => null,
        'partner' => null,
        'network_ready' => null,
        'area_code' => null,
        'fix_cost' => null,
        'var_cost' => null,
        'var_percent' => null,
        'no_stat' => null,
    ];

    $networkQuery = $db->query("SELECT * FROM `networks` WHERE `id` = ".$customer['network_id']);

    if ($networkQuery->num_rows !== 1) {
        die('network nicht gefunden');
    }

    $network = $networkQuery->fetch_assoc();


    $purtelMasterAccount = [
        'purtel_login' => null,
    ];

    $purtelMasterAccountQuery = $db->query("SELECT * FROM `purtel_account` WHERE `cust_id` = ".$customer['id']." AND `master` = 1");

    if ($purtelMasterAccountQuery->num_rows !== 1) {
        var_dump($customer['id']);
        echo "purtel master nicht gefunden\n";
        continue;
        //die('purtel master nicht gefunden');
    }

    $purtelMasterAccount = $purtelMasterAccountQuery->fetch_assoc();


    $postData = \Wisotel\Configuration\Configuration::get('purtelAccountValues');

    $postData['anrede'] = $customer['title'] == 'Herr' ? '1' : '2';
    $postData['vorname'] = $customer['firstname'];
    $postData['nachname'] = $customer['lastname'];
    $postData['adresse'] = $customer['street'];

    $houseNrMatch = [];

    preg_match('/(\d+)(.*)/', $customer['streetno'], $houseNrMatch);

    $postData['haus_nr'] = '';

    if (isset($houseNrMatch[1])) {
        $postData['haus_nr'] = $houseNrMatch[1];
    }
    if (isset($houseNrMatch[2])) {
        $postData['zusatz'] = $houseNrMatch[2];
    }

    $postData['plz'] = $customer['zipcode'];
    $postData['ort'] = $customer['city'];
    $postData['version'] = $customer['version'];
    $postData['purtel_use_version'] = $customer['purtel_use_version'];

    $postData['kundennummer_extern'] = $customer['clientid'];

    if ($customer['purtel_use_version'] == 1 && $customer['version'] > 0) {
        $postData['kundennummer_extern'] += '_'.$customer['version'];
    }

    $postData['geburtsdatum'] = $customer['birthday'];

    if ($postData['geburtsdatum'] != '') {
       $birthday = explode('.', $postData['geburtsdatum']);
       $postData['geburtsdatum'] = $birthday[2].$birthday[1].$birthday[0];
    }

    $postData['email'] = $postData['anrede'] == '' ? $customer['emailaddress'] : $customer['bank_account_emailaddress'];

    $postData['ortsnetz'] = $network['area_code'];

    $postData['ip_eingeschraenkt'] = 2;


    // create sip

    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'createcontract'
    );

    $url .= '&'.utf8_decode(http_build_query(array_merge($postData, ['skip_timeout' => 1])));


    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        //CURLOPT_URL             => $PurtelUrl1,
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $result = trim($result);
    $result = explode(';', $result);

    if ('+OK' !== $result[0]) {
        var_dump('Failed to create sip', $result, $userId);
        echo $url."\n\n";

        continue;
    }


    // set password to master account

    $password = str_replace('passwort=', '', $result[3]);

    $db->query("UPDATE `purtel_account` SET `purtel_password` = '".$password."' WHERE `id` = ".$purtelMasterAccount['id']);

    if (!empty($db->error)) {
         var_dump('Failed to set password to master', $result, $userId);

        continue;
    }

    // add number oder purtel_login (as number) to sip

    $username = str_replace('benutzername=', '', $result[2]);

    foreach ($accounts as $account) {
        if (!empty($account['nummer'])) { // account has number
            // update purtel_login
            $db->query("UPDATE `purtel_account` SET `purtel_login` = '".$username."' WHERE `id` = ".$account['id']);

            if (!empty($db->error)) {
                var_dump($db->error);
                var_dump('Failed update purtel_login', $result, $userId, $account['id']);

                continue;
            }
        } else {
            // set 0+purtel_login as number and update purtel_login
            $db->query("UPDATE `purtel_account` SET 
                `purtel_login` = '".$username."', `nummer` = '0".$account['purtel_login']."' 
                WHERE `id` = ".$account['id']);

            if (!empty($db->error)) {
                var_dump($db->error);
                var_dump('Failed update nummer and purtel_login', $result, $userId, $account['id']);

                continue;
            }
        }
    }


    // update account

    $updatePostData = $postData;

    $updatePostData['anschluss'] = $username;

    $updatePostData['firma'] = $customer['company'];

    $updatePostData['email'] = $customer['bank_account_emailaddress'];

    if ($updatePostData['email'] == '') {
        $updatePostData['email'] = $customer['emailaddress'];
    }

    $updatePostData['telefon'] = $customer['phoneareacode'].$customer['phonenumber'];
    
    $updatePostData['mobile'] = $customer['mobilephone'];

    $updatePostData['inhaber'] = $customer['bank_account_holder_lastname'];
    $updatePostData['inhaber_adresse'] = $customer['street'].' '.$customer['streetno'];

    $updatePostData['inhaber_plz'] = $customer['zipcode'];
    $updatePostData['inhaber_ort'] = $customer['city'];

    $updatePostData['bank'] = $customer['bank_account_bankname'];

    $updatePostData['BIC'] = $customer['bank_account_bic_new'];

    $updatePostData['IBAN'] = $customer['bank_account_iban'];

    $updatePostData['blz'] = $customer['bank_account_bic'];
    $updatePostData['kontonr'] = $customer['bank_account_number'];

    $updatePostData['einzug'] = empty($updatePostData['IBAN']) ? '' : '1';


    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'changecontract'
    );
    
    $url .= '&'.utf8_decode(http_build_query($updatePostData));

    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        //CURLOPT_URL             => $PurtelUrl1,
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $result = trim($result);
    $result = explode(';', $result);

    if ('+OK' !== $result[0]) {
        var_dump('Failed to update sip', $result, $userId);

        continue;
    }






    echo "done: ".$userId."\n";

    //die();
}
























/////////////////////////////////////////////// TEST set sipusername ////////////////////////////////////////////////////////////////////////7
die();

require __DIR__.'/../_conf/database.inc';
require_once __DIR__.'/../vendor/wisotel/configuration/Configuration.php';

$customerQuery = $db->query("SELECT * FROM `customers` WHERE `id` = 5513");

if ($customerQuery->num_rows !== 1) {
    die('kunde nicht gefunden');
}

$customer = $customerQuery->fetch_assoc();

$network = [
    'id' => null,
    'id_string' => null,
    'name' => null,
    'partner' => null,
    'network_ready' => null,
    'area_code' => null,
    'fix_cost' => null,
    'var_cost' => null,
    'var_percent' => null,
    'no_stat' => null,
];

$networkQuery = $db->query("SELECT * FROM `networks` WHERE `id` = ".$customer['network_id']);

if ($networkQuery->num_rows === 1) {
    $network = $networkQuery->fetch_assoc();
}


$purtelMasterAccount = [
    'purtel_login' => null,
];

$purtelMasterAccountQuery = $db->query("SELECT * FROM `purtel_account` WHERE `cust_id` = ".$customer['id']." AND `master` = 1");

if ($purtelMasterAccountQuery->num_rows === 1) {
    $purtelMasterAccount = $purtelMasterAccountQuery->fetch_assoc();
}




$postData = \Wisotel\Configuration\Configuration::get('purtelAccountValues');

$postData['anrede'] = $customer['title'] == 'Herr' ? '1' : '2';
$postData['vorname'] = $customer['firstname'];
$postData['nachname'] = $customer['lastname'];
$postData['adresse'] = $customer['street'];

$houseNrMatch = [];

preg_match('/(\d+)(.*)/', $customer['streetno'], $houseNrMatch);

$postData['haus_nr'] = '';

if (isset($houseNrMatch[1])) {
    $postData['haus_nr'] = $houseNrMatch[1];
}
if (isset($houseNrMatch[2])) {
    $postData['zusatz'] = $houseNrMatch[2];
}

$postData['plz'] = $customer['zipcode'];
$postData['ort'] = $customer['city'];
$postData['version'] = $customer['version'];
$postData['purtel_use_version'] = $customer['purtel_use_version'];

$postData['kundennummer_extern'] = $customer['clientid'];

if ($customer['purtel_use_version'] == 1 && $customer['version'] > 0) {
    $postData['kundennummer_extern'] += '_'.$customer['version'];
}

$postData['geburtsdatum'] = $customer['birthday'];

if ($postData['geburtsdatum'] != '') {
   $birthday = explode('.', $postData['geburtsdatum']);
   $postData['geburtsdatum'] = $birthday[2].$birthday[1].$birthday[0];
}

$postData['email'] = $postData['anrede'] == '' ? $customer['emailaddress'] : $customer['bank_account_emailaddress'];

$postData['ortsnetz'] = $network['area_code'];

if (isset($purtelMasterAccount['purtel_login']) && !empty($purtelMasterAccount['purtel_login'])) {
    $postData['konto_von'] = $purtelMasterAccount['purtel_login'];
}

$postData['ip_eingeschraenkt'] = 2;




$username = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$password = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');



/* 
// CREATE-CONTRACT

$url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
    urlencode($username),
    urlencode($password),
    'createcontract'
);

$url .= '&'.utf8_decode(http_build_query($postData));


$curl = curl_init($url);

curl_setopt_array($curl, array(
    //CURLOPT_URL             => $PurtelUrl1,
    CURLOPT_HEADER          => false,
    CURLOPT_RETURNTRANSFER  => true,
    CURLOPT_SSL_VERIFYPEER  => false,
));


$result = curl_exec($curl);

curl_close($curl);

var_dump($result);
// +OK;produkt=4578;benutzername=673603;passwort=9d205bf0802c;produkt_bezeichnung=Leistungsfrei;produkt_zuruecksetzen=;poolnummer=;
*/





$updatePostData = [];
$updatePostData['kundennummer_extern'] = $customer['clientid'];
$updatePostData['sipusername'] = 'COMIN128xxxxxx@comin.be-converged.com';
$updatePostData['anschluss'] = '673603';

// UPDATE CONTRACT

$url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
    urlencode($username),
    urlencode($password),
    'changecontract'
);
/*
$url .= '&'.utf8_decode(http_build_query($updatePostData));


$curl = curl_init($url);

curl_setopt_array($curl, array(
    //CURLOPT_URL             => $PurtelUrl1,
    CURLOPT_HEADER          => false,
    CURLOPT_RETURNTRANSFER  => true,
    CURLOPT_SSL_VERIFYPEER  => false,
));


$result = curl_exec($curl);

curl_close($curl);

var_dump($result);
// -ERR 042 Parameter nicht erlaubt f�r Mandant 100260 [sipusername=COMIN128xxxxxx@comin.be-converged.com]

*/

















/*
'purtelAccountValues' => [
    'land' => 49,
    'zeitzone' => 1,
    'waehrung' => 'Euro',
    'produkt' => 4578,
    'nolimit' => 1,
    'kredit' => 10000,
    'postversand' => 0,
    'rechnung_per_mail' => 2,
    'evn' => 1,
],
*/

/*



purtel2['anrede'] = $("[name=CustomerTitle]").val();
if (purtel2['anrede'] == 'Herr') {
  purtel2['anrede'] = '1';
} else {
  purtel2['anrede'] = '2';
}
purtel2['vorname']              = encodeURIComponent($("[name=CustomerFirstname]").val());
purtel2['nachname']             = encodeURIComponent($("[name=CustomerLastname]").val());
purtel2['adresse']              = encodeURIComponent($("[name=CustomerStreet]").val());
str_part                        = encodeURIComponent($(":input[name=CustomerStreetno]").val()).match(/(\d+)(.*)/);    
purtel2['haus_nr']              = str_part[1];
purtel2['zusatz']               = str_part[2];
purtel2['plz']                  = encodeURIComponent($("[name=CustomerZipcode]").val());
purtel2['ort']                  = encodeURIComponent($("[name=CustomerCity]").val());
purtel2['firma']                = encodeURIComponent($("[name=CustomerCompany]").val());
purtel2['kundennummer_extern']  = purtel['kundennummer_extern'];
birthday                        = $("[name=CustomerBirthday]").val().split('.');
purtel2['geburtsdatum']         = $("[name=CustomerBirthday]").val();
if (purtel2['geburtsdatum'] != '') {
   birthday   = purtel2['geburtsdatum'].split('.');
   purtel2['geburtsdatum']      = birthday[2] + birthday[1] + birthday[0];
}
purtel2['email']                = encodeURIComponent($("[name=CustomerBankAccountEmailaddress]").val());
if (purtel2['email'] == '') {
  purtel2['email']                = encodeURIComponent($("[name=CustomerEmailaddress]").val());
}
purtel2['ortsnetz']             = encodeURIComponent($("[name=area_code]").val());
purtel2['telefon']              = encodeURIComponent($("[name=CustomerPhoneareacode]").val()+$(":input[name=CustomerPhonenumber]").val());
purtel2['mobile']               = encodeURIComponent($("[name=CustomerMobilephone]").val());
purtel2['inhaber']              = encodeURIComponent($.trim($(":input[name=CustomerBankAccountHolderLastname]").val()));
purtel2['inhaber_adresse']      = encodeURIComponent($("[name=CustomerStreet]").val()+' '+$(":input[name=CustomerStreetno]").val());
purtel2['inhaber_plz']          = encodeURIComponent($("[name=CustomerZipcode]").val());
purtel2['inhaber_ort']          = encodeURIComponent($("[name=CustomerCity]").val());
purtel2['bank']                 = encodeURIComponent($("[name=CustomerBankAccountBankname]").val());
purtel2['BIC']                  = encodeURIComponent($.trim($("[name=CustomerBankAccountBicNew]").val()));
purtel2['IBAN']                 = encodeURIComponent($.trim($("[name=CustomerBankAccountIban]").val()));
purtel2['blz']                  = encodeURIComponent($("[name=CustomerBankAccountBic]").val());
purtel2['kontonr']              = encodeURIComponent($("[name=CustomerBankAccountNumber]").val());
purtel2['einzug']               = '1';
if (!purtel2['IBAN'] || /^\s*$/.test(purtel2['IBAN']) ) {
  purtel2['einzug'] = '';
} else {
  purtel2['einzug'] = '1';
}

PurtelIndex                     = $("[name=PurtelLogin]").val();
purtel3['CustomerId']           = encodeURIComponent($("#CustomerId").val());
purtel3['PurtelPorting']        = encodeURIComponent($("[name=PurtelPorting]").val());
purtel3['PurtelNummer']         = encodeURIComponent($("[name=PurtelNummer]").val());
purtel3['PurtelTyp']            = encodeURIComponent($("[name=PurtelTyp]").val());
if (PurtelIndex) {
  purtel3['PurtelLogin']        = encodeURIComponent($("[name=PurtelPurtelLogin_"+PurtelIndex+"]").val());  
  purtel3['PurtelPassword']     = encodeURIComponent($("[name=PurtelPurtelPassword_"+PurtelIndex+"]").val());
  if (purtelMaster == '') {
    purtel3['PurtelMaster']     = 1;
  } else {
    purtel3['PurtelMaster']     = 0;
  }
} else {
  purtel3['PurtelLogin']        = '';
  purtel3['PurtelPassword']     = '';
  if (purtelMaster == '') {
    purtel3['PurtelMaster']     = 1;
  } else {
    purtel3['PurtelMaster']     = 0;
  }
 }
    
purtel3['purtelMaster']         = encodeURIComponent(purtelMaster);

PurtelParams                    = JSON.stringify(purtel);
Purtel2Params                   = JSON.stringify(purtel2);
Purtel3Params                   = JSON.stringify(purtel3);
*/






/*
//$purtelLogin = urldecode($_POST['PurtelLogin']);
$Purtel = json_decode($_POST['Purtel'], true);
$Purtel2 = json_decode($_POST['Purtel2'], true);

require_once $StartPath.'/vendor/wisotel/configuration/Configuration.php';

$username = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$password = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

$action = 'createcontract';
$action2 = 'changecontract';
$status = 0;
$status2 = 0;
$purtelPassword = '';
$PurtelUrl = '';
$PurtelUrl2 = '';

// Purtelkunde anlegen
if (!$purtelLogin) {
    $PurtelUrl = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($username),
        urlencode($password),
        urlencode($action)
    );

    foreach ($Purtel as $key => $value) {
        $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
    }

    $curl = curl_init($PurtelUrl);
    
    curl_setopt_array($curl, array(
        CURLOPT_URL             => $PurtelUrl,
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));

    $result = curl_exec($curl);

    curl_close($curl);

    $resultArray = str_getcsv($result, ';');

    if($resultArray[0] == '+OK') {
        $status = 1;
    
        foreach($resultArray as $resultKey => $resultValue) {
            if(strpos($resultValue, 'benutzername=') !== false) {
                $purtelLogin = substr($resultValue, 13);
            } else if(strpos($resultValue, 'passwort=') !== false) {
                $purtelPassword = substr($resultValue, 9);
            }
        }
    }
} else {
    $status = 2;
}

if ($purtelLogin) {
    $Purtel2['anschluss'] = $purtelLogin;
    $PurtelUrl2  = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($username),
        urlencode($password),
        urlencode($action2)
    );

    foreach ($Purtel2 as $key => $value) {
        if ($value) {
            $PurtelUrl2 .= "&$key=".urlencode(utf8_decode($value));
        }
    }

    $curl = curl_init($PurtelUrl2);

    curl_setopt_array($curl, array(
        CURLOPT_URL             => $PurtelUrl2,
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));

    $result = curl_exec($curl);

    curl_close($curl);

    $resultArray = str_getcsv($result, ';');

    if($resultArray[0] == '+OK') {
        $status2 = 1;
    }
} else {
    $status2 = 2;
}

$response = array(
    'purtelLogin' => $purtelLogin,
    'purtelPassword' => $purtelPassword,
    'PurtelUrl' => $PurtelUrl2,
    'status' => $status,
    'status2' => $status2,
);

print_r(json_encode($response));

?>
*/

