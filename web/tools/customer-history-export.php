<?php

require __DIR__.'/../_conf/database.inc';

$handle = fopen('php://output', 'w');

$q = $db->query("
    SELECT ch.*, c.`clientid`, n.`id_string`, che.`event` 
    FROM `customer_history` ch 
    INNER JOIN `customers` c ON c.id = ch.customer_id 
    INNER JOIN `networks` n ON n.id = c.network_id 
    INNER JOIN `customer_history_events` che ON che.id = ch.event_id 
    WHERE ch.`created_at` BETWEEN '2018-01-01' AND '2018-12-31'");

$outputColumns = [
    'zkv-Kundennummer' => 'clientid',
    'Netz' => 'id_string',
    'Aktion' => 'event',
    'eingetragen durch' => 'created_by',
    'eingetragen am' => 'created_at',
    'Notiz' => 'note',
];
/*$outputColumns = [
    'clientid' => 'zkv-Kundennummer',
    'id_string' => 'Netz',
    'event' => 'Aktion',
    'created_by' => 'eingetragen durch',
    'created_at' => 'eingetragen am',
    'note' => 'Notiz',
];*/

// output headline
fputcsv($handle, array_keys($outputColumns), ';');

while ($data = $q->fetch_assoc()) {
    $output = [];

    foreach ($outputColumns as $key) {
        $output[] = $data[$key];
    }

    fputcsv($handle, $output, ';');
}

fclose($handle);
