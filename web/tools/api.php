<?php
/****************************************
 *
 * WKV (Wisotel Kundenverwaltung API)
 *
 * Programmierung: Jürgen Lehmann (IT-Systemlösungen)
 * 16.01.2014
 * KH Legat Wisotel
 *
*****************************************/
  require_once('config.inc');
  require_once($StartPath.'/_conf/database.inc');
  $Error = true;
  if (key_exists('func', $_GET)) {
    switch ($_GET['func']) {
      case 'neukunde':
        if (key_exists('clientid', $_GET)) {
          $sql = "SELECT count(*) as count FROM customers WHERE clientid = '".$_GET['clientid']."'";
          $db_cust_count  = $db->query($sql);
          $db_cust_rownum = $db_cust_count->fetch_assoc();
          $db_cust_count->free();
          if ($db_cust_rownum['count']) {
            $sql_string_head = "UPDATE customers SET ";
            $sql_string_foot = " WHERE clientid = '".urldecode($_GET['clientid'])."'";
            $Error = false;
          } else {
            $sql_string_head = "INSERT INTO customers SET ";
            $sql_string_foot = "";
          }
          $var_set = '';
          foreach ($_GET as $key => $value) {
            switch ($key) {  
              case 'add_charge_terminal':
              case 'add_customer_on_fb':
              case 'add_ip_address':
              case 'add_ip_address_cost':
              case 'add_phone_lines':
              case 'add_phone_lines_cost':
              case 'add_phone_nos':
              case 'add_port':
              case 'add_satellite':
              case 'application':
              case 'application_text':
              case 'bank_account_bankname':
              case 'bank_account_bic':
              case 'bank_account_bic_new':
              case 'bank_account_holder_firstname':
              case 'bank_account_holder_lastname':
              case 'bank_account_iban':
              case 'bank_account_number':
              case 'birthday':
              case 'call_data_record':
              case 'cancel_purtel':
              case 'cancel_purtel_for_date':
              case 'cancel_tal':
              case 'card_id':
              case 'city':
              case 'clientid':
              case 'clienttype':
              case 'comment':
              case 'company':
              case 'connect_type':
              case 'connection_activation_date':
              case 'connection_address_equal':
              case 'connection_city':
              case 'connection_fee_height':
              case 'connection_fee_paid':
              case 'connection_inactive_date':
              case 'connection_installation':
              case 'connection_street':
              case 'connection_streetno':
              case 'connection_zipcode':
              case 'contract':
              case 'contract_acknowledge':
              case 'contract_change_to':
              case 'contract_comment':
              case 'contract_id':
              case 'contract_received_date':
              case 'contract_sent_date':
              case 'contract_text':
              case 'contract_version':
              case 'credit_rating_date':
              case 'credit_rating_link':
              case 'credit_rating_ok':
              case 'customer_connect_info_date':
              case 'customer_connect_info_from':
              case 'customer_connect_info_how':
              case 'direct_debit_allowed':
              case 'district':
              case 'DPBO':
              case 'dslam_arranged_date':
              case 'dslam_card_no':
              case 'dslam_location':
              case 'dslam_port':
              case 'dtag_line':
              case 'dtagbluckage_fullfiled':
              case 'dtagbluckage_fullfiled_date':
              case 'dtagbluckage_sended':
              case 'dtagbluckage_sended_date':
              case 'emailaddress':
              case 'eu_flat':
              case 'exp_date_int':
              case 'exp_date_phone':
              case 'fax':
              case 'fb_vorab':
              case 'final_purtelproduct_date':
              case 'firewall_router':
              case 'firmware_version':
              case 'firstname':
              case 'flat_eu_network':
              case 'flat_eu_network_cost':
              case 'flat_german_network':
              case 'flat_german_network_cost':
              case 'fordering_costs':
              case 'german_mobile':
              case 'german_mobile_price':
              case 'german_network':
              case 'german_network_price':
              case 'gf_branch':
              case 'gf_cabling':
              case 'gf_cabling_cost':
              case 'gutschrift':
              case 'gutschrift_till':
              case 'hd_plus_card':
              case 'higher_availability':
              case 'higher_uplink_one':
              case 'higher_uplink_two':
              case 'history':
              case 'house_connection':
              case 'id':
              case 'implementation':
              case 'implementation_text':
              case 'international_calls_price':
              case 'ip_address':
              case 'ip_address_inet':
              case 'ip_address_phone':
              case 'ip_range_id':
              case 'isdn_backup':
              case 'isdn_backup2':
              case 'Kommando_query':
              case 'kvz_addition':
              case 'kvz_name':
              case 'kvz_standort':
              case 'kvz_toggle_no':
              case 'lapse_notice_date_inet':
              case 'lapse_notice_date_tel':
              case 'lastname':
              case 'line_identifier':
              case 'little_bar':
              case 'location_id':
              case 'lsa_kvz_partition_id':
              case 'lsa_pin':
              case 'mac_address':
              case 'media_converter':
              case 'mobile_flat':
              case 'mobilephone':
              case 'moved':
              case 'nb_canceled_date':
              case 'nb_canceled_date_phone':
              case 'network':
              case 'network_id':
              case 'new_clientid_permission':
              case 'new_number_date':
              case 'new_number_from':
              case 'new_numbers_text':
              case 'new_or_ported_phonenumbers':
              case 'no_eze':
              case 'no_ping':
              case 'notice_period_internet_int':
              case 'notice_period_internet_type':
              case 'notice_period_phone_int':
              case 'notice_period_phone_type':
              case 'old_contract_active':
              case 'old_provider_comment':
              case 'oldcontract_address_equal':
              case 'oldcontract_city':
              case 'oldcontract_comment':
              case 'oldcontract_exists':
              case 'oldcontract_firstname':
              case 'oldcontract_internet_client_cancelled':
              case 'oldcontract_internet_clientno':
              case 'oldcontract_internet_provider':
              case 'oldcontract_lastname':
              case 'oldcontract_phone_client_cancelled':
              case 'oldcontract_phone_clientno':
              case 'oldcontract_phone_provider':
              case 'oldcontract_phone_type':
              case 'oldcontract_street':
              case 'oldcontract_streetno':
              case 'oldcontract_title':
              case 'oldcontract_zipcode':
              case 'paper_bill':
              case 'password':
              case 'patch_date':
              case 'payment_performance':
              case 'phone_adapter':
              case 'phone_comment':
              case 'phone_number_added':
              case 'phone_number_block':
              case 'phoneareacode':
              case 'phoneentry_done_date':
              case 'phoneentry_done_from':
              case 'phonenumber':
              case 'portdefault_comment':
              case 'porting':
              case 'porting_text':
              case 'pppoe_config_date':
              case 'productname':
              case 'prospect_supply_status':
              case 'purtel_bluckage_fullfiled_date':
              case 'purtel_bluckage_sended_date':
              case 'purtel_customer_reg_date':
              case 'purtel_data_record_id':
              case 'purtel_edit_done':
              case 'purtel_edit_done_from':
              case 'purtel_login':
              case 'purtel_passwort':
              case 'purtel_product':
              case 'purtelproduct_advanced':
              case 'rDNS_count':
              case 'rDNS_installation':
              case 'reached_downstream':
              case 'reached_upstream':
              case 'recommended_product':
              case 'recruited_by':
              case 'reg_answer_date':
              case 'registration_date':
              case 'remote_login':
              case 'remote_password':
              case 'routing_active':
              case 'routing_active_date':
              case 'routing_deleted':
              case 'routing_wish_date':
              case 'second_tal':
              case 'Service':
              case 'service_level':
              case 'service_technician':
              case 'sip_accounts':
              case 'sofortdsl':
              case 'special_conditions_from':
              case 'special_conditions_text':
              case 'special_conditions_till':
              case 'special_termination_internet':
              case 'special_termination_phone':
              case 'Spectrumprofile':
              case 'stati_oldcontract_comment':
              case 'stati_port_confirm_date':
              case 'status':
              case 'stnz_fb':
              case 'street':
              case 'streetno':
              case 'subnetmask':
              case 'tal_assigned_phoneno':
              case 'tal_cancel_ack_date':
              case 'tal_cancel_date':
              case 'tal_canceled_for_date':
              case 'tal_canceled_from':
              case 'tal_dtag_assignment_no':
              case 'tal_dtag_revisor':
              case 'tal_lended_fritzbox':
              case 'tal_order':
              case 'tal_order_ack_date':
              case 'tal_order_date':
              case 'tal_order_work':
              case 'tal_orderd_from':
              case 'tal_ordered_for_date':
              case 'tal_product':
              case 'tal_releasing_operator':
              case 'talorder':
              case 'talorder_text':
              case 'techdata_comment':
              case 'techdata_status':
              case 'telefonbuch_eintrag':
              case 'terminal_ready':
              case 'terminal_ready_from':
              case 'terminal_sended_date':
              case 'terminal_sended_from':
              case 'terminal_serialno':
              case 'terminal_type':
              case 'terminal_type_exists':
              case 'title':
              case 'tv_service':
              case 'tvs':
              case 'tvs_date':
              case 'tvs_from':
              case 'tvs_to':
              case 'ventelo_confirmation_date':
              case 'ventelo_port_letter_date':
              case 'ventelo_port_letter_from':
              case 'ventelo_port_letter_how':
              case 'ventelo_port_wish_date':
              case 'ventelo_purtel_enter_date':
              case 'ventelo_purtel_enter_from':
              case 'version':
              case 'vlan_ID':
              case 'wisocontract_canceled_date':
              case 'wisotelno':
              case 'zipcode':
                $var_set .= "$key = '".urldecode($value)."',";
                break;
            }
          }
          if ($Error) {
          $var_set = substr($var_set, 0, -1);
          $sql = $sql_string_head.$var_set.$sql_string_foot;
          $db_save_result = $db->query($sql);
          if ($db_save_result) {
            echo "OK: Kunde eingefuegt";
          } else {
            echo "ERROR: Kunde konnte nicht eingefuegt werden";
          }
        } else {
          echo "ERROR: Kunde schon vorhanden";
        }
      } else {
          echo "ERROR: 'clientid' fehlt";
        }
        break;
      
      case 'kundechange':
        if (key_exists('clientid', $_GET)) {
          $sql = "SELECT count(*) as count FROM customers WHERE clientid = '".$_GET['clientid']."'";
          $db_cust_count  = $db->query($sql);
          $db_cust_rownum = $db_cust_count->fetch_assoc();
          $db_cust_count->free();
          if ($db_cust_rownum['count']) {
            $sql_string_head = "UPDATE customers SET ";
            $sql_string_foot = " WHERE clientid = '".urldecode($_GET['clientid'])."'";
          } else {
            $sql_string_head = "INSERT INTO customers SET ";
            $sql_string_foot = "";
            $Error = false;
          }
          $var_set = '';
          foreach ($_GET as $key => $value) {
            switch ($key) {
                case 'add_charge_terminal':
                case 'add_customer_on_fb':
                case 'add_ip_address':
                case 'add_ip_address_cost':
                case 'add_phone_lines':
                case 'add_phone_lines_cost':
                case 'add_phone_nos':
                case 'add_port':
                case 'add_satellite':
                case 'application':
                case 'application_text':
                case 'bank_account_bankname':
                case 'bank_account_bic':
                case 'bank_account_bic_new':
                case 'bank_account_holder_firstname':
                case 'bank_account_holder_lastname':
                case 'bank_account_iban':
                case 'bank_account_number':
                case 'birthday':
                case 'call_data_record':
                case 'cancel_purtel':
                case 'cancel_purtel_for_date':
                case 'cancel_tal':
                case 'card_id':
                case 'city':
                case 'clientid':
                case 'clienttype':
                case 'comment':
                case 'company':
                case 'connect_type':
                case 'connection_activation_date':
                case 'connection_address_equal':
                case 'connection_city':
                case 'connection_fee_height':
                case 'connection_fee_paid':
                case 'connection_inactive_date':
                case 'connection_installation':
                case 'connection_street':
                case 'connection_streetno':
                case 'connection_zipcode':
                case 'contract':
                case 'contract_acknowledge':
                case 'contract_change_to':
                case 'contract_comment':
                case 'contract_id':
                case 'contract_received_date':
                case 'contract_sent_date':
                case 'contract_text':
                case 'contract_version':
                case 'credit_rating_date':
                case 'credit_rating_link':
                case 'credit_rating_ok':
                case 'customer_connect_info_date':
                case 'customer_connect_info_from':
                case 'customer_connect_info_how':
                case 'direct_debit_allowed':
                case 'district':
                case 'DPBO':
                case 'dslam_arranged_date':
                case 'dslam_card_no':
                case 'dslam_location':
                case 'dslam_port':
                case 'dtag_line':
                case 'dtagbluckage_fullfiled':
                case 'dtagbluckage_fullfiled_date':
                case 'dtagbluckage_sended':
                case 'dtagbluckage_sended_date':
                case 'emailaddress':
                case 'eu_flat':
                case 'exp_date_int':
                case 'exp_date_phone':
                case 'fax':
                case 'fb_vorab':
                case 'final_purtelproduct_date':
                case 'firewall_router':
                case 'firmware_version':
                case 'firstname':
                case 'flat_eu_network':
                case 'flat_eu_network_cost':
                case 'flat_german_network':
                case 'flat_german_network_cost':
                case 'fordering_costs':
                case 'german_mobile':
                case 'german_mobile_price':
                case 'german_network':
                case 'german_network_price':
                case 'gf_branch':
                case 'gf_cabling':
                case 'gf_cabling_cost':
                case 'gutschrift':
                case 'gutschrift_till':
                case 'hd_plus_card':
                case 'higher_availability':
                case 'higher_uplink_one':
                case 'higher_uplink_two':
                case 'history':
                case 'house_connection':
                case 'id':
                case 'implementation':
                case 'implementation_text':
                case 'international_calls_price':
                case 'ip_address':
                case 'ip_address_inet':
                case 'ip_address_phone':
                case 'ip_range_id':
                case 'isdn_backup':
                case 'isdn_backup2':
                case 'Kommando_query':
                case 'kvz_addition':
                case 'kvz_name':
                case 'kvz_standort':
                case 'kvz_toggle_no':
                case 'lapse_notice_date_inet':
                case 'lapse_notice_date_tel':
                case 'lastname':
                case 'line_identifier':
                case 'little_bar':
                case 'location_id':
                case 'lsa_kvz_partition_id':
                case 'lsa_pin':
                case 'mac_address':
                case 'media_converter':
                case 'mobile_flat':
                case 'mobilephone':
                case 'moved':
                case 'nb_canceled_date':
                case 'nb_canceled_date_phone':
                case 'network':
                case 'network_id':
                case 'new_clientid_permission':
                case 'new_number_date':
                case 'new_number_from':
                case 'new_numbers_text':
                case 'new_or_ported_phonenumbers':
                case 'no_eze':
                case 'no_ping':
                case 'notice_period_internet_int':
                case 'notice_period_internet_type':
                case 'notice_period_phone_int':
                case 'notice_period_phone_type':
                case 'old_contract_active':
                case 'old_provider_comment':
                case 'oldcontract_address_equal':
                case 'oldcontract_city':
                case 'oldcontract_comment':
                case 'oldcontract_exists':
                case 'oldcontract_firstname':
                case 'oldcontract_internet_client_cancelled':
                case 'oldcontract_internet_clientno':
                case 'oldcontract_internet_provider':
                case 'oldcontract_lastname':
                case 'oldcontract_phone_client_cancelled':
                case 'oldcontract_phone_clientno':
                case 'oldcontract_phone_provider':
                case 'oldcontract_phone_type':
                case 'oldcontract_street':
                case 'oldcontract_streetno':
                case 'oldcontract_title':
                case 'oldcontract_zipcode':
                case 'paper_bill':
                case 'password':
                case 'patch_date':
                case 'payment_performance':
                case 'phone_adapter':
                case 'phone_comment':
                case 'phone_number_added':
                case 'phone_number_block':
                case 'phoneareacode':
                case 'phoneentry_done_date':
                case 'phoneentry_done_from':
                case 'phonenumber':
                case 'portdefault_comment':
                case 'porting':
                case 'porting_text':
                case 'pppoe_config_date':
                case 'productname':
                case 'prospect_supply_status':
                case 'purtel_bluckage_fullfiled_date':
                case 'purtel_bluckage_sended_date':
                case 'purtel_customer_reg_date':
                case 'purtel_data_record_id':
                case 'purtel_edit_done':
                case 'purtel_edit_done_from':
                case 'purtel_login':
                case 'purtel_passwort':
                case 'purtel_product':
                case 'purtelproduct_advanced':
                case 'rDNS_count':
                case 'rDNS_installation':
                case 'reached_downstream':
                case 'reached_upstream':
                case 'recommended_product':
                case 'recruited_by':
                case 'reg_answer_date':
                case 'registration_date':
                case 'remote_login':
                case 'remote_password':
                case 'routing_active':
                case 'routing_active_date':
                case 'routing_deleted':
                case 'routing_wish_date':
                case 'second_tal':
                case 'Service':
                case 'service_level':
                case 'service_technician':
                case 'sip_accounts':
                case 'sofortdsl':
                case 'special_conditions_from':
                case 'special_conditions_text':
                case 'special_conditions_till':
                case 'special_termination_internet':
                case 'special_termination_phone':
                case 'Spectrumprofile':
                case 'stati_oldcontract_comment':
                case 'stati_port_confirm_date':
                case 'status':
                case 'stnz_fb':
                case 'street':
                case 'streetno':
                case 'subnetmask':
                case 'tal_assigned_phoneno':
                case 'tal_cancel_ack_date':
                case 'tal_cancel_date':
                case 'tal_canceled_for_date':
                case 'tal_canceled_from':
                case 'tal_dtag_assignment_no':
                case 'tal_dtag_revisor':
                case 'tal_lended_fritzbox':
                case 'tal_order':
                case 'tal_order_ack_date':
                case 'tal_order_date':
                case 'tal_order_work':
                case 'tal_orderd_from':
                case 'tal_ordered_for_date':
                case 'tal_product':
                case 'tal_releasing_operator':
                case 'talorder':
                case 'talorder_text':
                case 'techdata_comment':
                case 'techdata_status':
                case 'telefonbuch_eintrag':
                case 'terminal_ready':
                case 'terminal_ready_from':
                case 'terminal_sended_date':
                case 'terminal_sended_from':
                case 'terminal_serialno':
                case 'terminal_type':
                case 'terminal_type_exists':
                case 'title':
                case 'tv_service':
                case 'tvs':
                case 'tvs_date':
                case 'tvs_from':
                case 'tvs_to':
                case 'ventelo_confirmation_date':
                case 'ventelo_port_letter_date':
                case 'ventelo_port_letter_from':
                case 'ventelo_port_letter_how':
                case 'ventelo_port_wish_date':
                case 'ventelo_purtel_enter_date':
                case 'ventelo_purtel_enter_from':
                case 'version':
                case 'vlan_ID':
                case 'wisocontract_canceled_date':
                case 'wisotelno':
                case 'zipcode':
                  $var_set .= "$key = '".urldecode($value)."',";
                  break;
              }
            }
            if ($Error) {
              $var_set = substr($var_set, 0, -1);
              $sql = $sql_string_head.$var_set.$sql_string_foot;
              $db_save_result = $db->query($sql);
              if ($db_save_result) {
               echo "OK:"; 
               echo "Kunde geaendert";
              } else {
                #echo "NOK<br />\n"; 
                echo "ERROR:Kunde konnte nicht geaendert werden";
              }
            } else {
              # echo "NOK<br />\n"; 
              echo "ERROR:Kunde nicht vorhanden";
            }
          } else {
            #echo "NOK<br />\n"; 
          echo "ERROR:'clientid' fehlt";
        }
        break;

      case 'get':
        if (key_exists('clientid', $_GET)) {
          $clientid = "'".$_GET['clientid']."'";
          $sql = "SELECT count(*) as count FROM customers WHERE clientid = '".$_GET['clientid']."'";
          $db_cust_count  = $db->query($sql);
          $db_cust_rownum = $db_cust_count->fetch_assoc();
          $db_cust_count->free();
          if ($db_cust_rownum['count']) {
            $Error = true;
            $sql_string_foot = " WHERE clientid = '".urldecode($_GET['clientid'])."'";
            $db_cust = $db->query("SELECT * FROM customers $sql_string_foot");
            $Row = $db_cust->fetch_assoc();
            $db_cust->free();
            $lastname = $Row["lastname"];
            $connection_street = $Row["connection_street"];
            $connection_streetno = $Row["connection_streetno"];
            $connection_city = $Row["connection_city"];
            $connection_zipcode = $Row["connection_zipcode"];
            $stati_port_confirm_date = $Row["stati_port_confirm_date"];
            $tal_order_date = $Row["tal_order_date"];
            $tal_order = $Row["tal_order"];
            $tal_order_ack_date = $Row["tal_order_ack_date"];
            $tal_order_work = $Row["tal_order_work"];
            $tal_ordered_for_date = $Row["tal_ordered_for_date"];
            $tal_cancel_date = $Row["tal_cancel_date"];
            $tal_cancel_ack_date = $Row["tal_cancel_ack_date"];
            $stati_oldcontract_comment = $Row["stati_oldcontract_comment"];
            $dtag_line = $Row["dtag_line"];
                  $tvs_date = $Row["tvs_date"];
                  $connection_activation_date = $Row["connection_activation_date"];
            # echo "Status = $Error <br />\nclient found $clientid<br />\n";
            # echo "Result : <br />\n";
            echo  "Lastname = $lastname \n";
            echo "connection_street = $connection_street\n";
            echo "connection_streetno = $connection_streetno\n";
            echo "connection_city = $connection_city\n";
            echo "connection_zipcode = $connection_zipcode\n";
            echo "stati_port_confirm_date = $stati_port_confirm_date\n";
            echo "tal_order_date = $tal_order_date\n";
            echo "tal_order = $tal_order\n";
            echo "tal_order_ack_date = $tal_order_ack_date\n";
            echo "tal_order_work = $tal_order_work\n";
            echo "tal_ordered_for_date = $tal_ordered_for_date\n";
            echo "tal_cancel_date = $tal_cancel_date\n";
            echo "tal_cancel_ack_date = $tal_cancel_ack_date\n";
                  echo "tvs_date = $tvs_date\n";
                  echo "connection_activation_date = $connection_activation_date\n";
            echo "Dtag_line = $dtag_line\n";
            echo "stati_oldcontract_comment = $stati_oldcontract_comment\n\n\n";

            #echo "Lastname ; connection_street ; connection_streetno ; connection_city ; connection_zipcode ; stati_port_confirm_date ; tal_order_date ; tal_order ; tal_order_ack_date ;  tal_order_work ; tal_ordered_for_date ; tal_cancel_date ;  tal_cancel_ack_date ; dtag_line ; stati_oldcontract_comment<br />\n " ;

            #echo "$lastname ; $connection_street ; $connection_streetno ; $connection_city ; $connection_zipcode ; $stati_port_confirm_date ; $tal_order_date ; $tal_order ; $tal_order_ack_date ; $tal_order_work ; $tal_ordered_for_date ; $tal_cancel_date ; $tal_cancel_ack_date ; $dtag_line ; $stati_oldcontract_comment <br />\n" ;


            #  echo "Result = $result1 Row = $Row Firstname = $lastname ";

          } else {
            $Error = false;
            #echo "NO"."\n"; 
            echo "ERROR:client not found $clientid\n";
          }
        }
        break;
      default:
      
        #echo "NOK \n"; 
        echo "ERROR:Keine gueltige Funktion";
    }
  } else {
      #echo "NOK \n"; 
    echo "ERROR:Keine Funktion angegeben";
  }
  $db->close();
?>
