<?php

$file = '/tmp/ip_ranges.csv';

$content = file_get_contents($file);

if (!is_string($content) && !empty($content)) {
    throw new InvalidArgumentException('invalid file');
}

$delimiter = null;

foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
    if (false !== strpos($content, $delimiter_)) {
        $delimiter = $delimiter_;
        break;
    }
}

if (null == $delimiter) {
    throw new InvalidArgumentException('can not use file');
}

$csv = array_map(function ($line) {
    return str_getcsv($line, ';');
}, explode($delimiter, $content));

$header = array_shift($csv);



$db = new mysqli('localhost', 'wkv', 'wkv', 'wkvdev');


$data = [];

$dataQ = $db->query("SELECT * FROM `ip_ranges`");

while ($x = $dataQ->fetch_assoc()) {
    $data[$x['id']] = $x;
}
//var_dump(count(array_keys(end($data))));
$idKey = array_search('id', $header);
//var_dump($idKey);

$querys = [];

foreach ($csv as $key => $array) {
    if (91 < $key) {
        break;
    }

    foreach ($array as $k => $v) {
        if (3 === $k) {
            continue;
        }

        if (empty($data[$array[$idKey]][$header[$k]]) && null !== $data[$array[$idKey]][$header[$k]]) {
            $data[$array[$idKey]][$header[$k]] = null;
            // update ip_ranges set `$header[$k]` = NULL
            $querys[] = "UPDATE `ip_ranges` SET `".$header[$k]."` = NULL WHERE `id` = ".$array[$idKey];
        }

        if ("NULL" === $v || empty($v)) {
            $v = null;
        }

        if (0 === strpos($v, '255255255')) {
            $v = str_replace('255255255', '255.255.255.', $v);
        }

        if (0 === strpos($v, '46237221')) {
            $v = str_replace('46237221', '46.237.221.', $v);
        }

        if ($v !== $data[$array[$idKey]][$header[$k]]) {
            

            if (10 === $k && 'Router' === $v) {
                // Hier noch mal KHL fragen, was sollte dann hier geändert werden?
            } else {
                var_dump($v, $data[$array[$idKey]][$header[$k]]);
                echo "\n\n";

                $v = empty($v) ? 'NULL' : "'".$v."'";

                $querys[] = "UPDATE `ip_ranges` SET `".$header[$k]."` = ".$v." WHERE `id` = ".$array[$idKey];
            }
            // Router
        }
    }
}

var_dump($querys);

foreach ($querys as $query) {
    //var_dump($db->query($query));
}