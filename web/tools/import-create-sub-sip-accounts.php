<?php

/*
 * Todo
 *
 * use area_code from purtel_account
 */


require __DIR__.'/../_conf/database.inc';
require_once __DIR__.'/../vendor/wisotel/configuration/Configuration.php';

$purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');


$purtelMasterAccounts = [];

//$query = $db->query("SELECT * FROM `purtel_account` ORDER BY `purtel_login` ASC");
$query = $db->query("SELECT * FROM `purtel_account` WHERE `cust_id` BETWEEN 5802 AND 6246 ORDER BY `purtel_login` ASC");

while ($data = $query->fetch_assoc()) {
    if ($data['master'] === '1') {
        $purtelMasterAccounts[] = $data['purtel_login'];
    }
}

$purtelAccounts = [];

foreach ($purtelMasterAccounts as $purtelLogin) {
    $q = $db->query("SELECT `purtel_account`.*, `customers`.`customer_id` 
        FROM `purtel_account` 
        LEFT JOIN `customers` ON `customers`.`id` = `purtel_account`.`cust_id` 
        WHERE `purtel_account`.`purtel_login` = '".$purtelLogin."'");

    if ($q->num_rows < 2) {
        continue; // has only 1 account
    }

    while ($d = $q->fetch_assoc()) {
        if ($d['master'] === '1' || empty($d['nummer']) || empty($d['customer_id'])) {
            continue;
        }

        $purtelAccounts[] = $d;
    }
}





$skip = [
];

foreach ($purtelAccounts as $account) {
    usleep(100000);

    if (in_array($account['id'], $skip)) {
        continue;
    }

    echo "Purtel-Account-Id: ".$account['id']." / customer-id: ".$account['cust_id']."\n";

    $userId = $account['cust_id'];

    $customerQuery = $db->query("SELECT * FROM `customers` WHERE `id` = ".$userId);

    if ($customerQuery->num_rows !== 1) {
        die('kunde nicht gefunden');
    }

    $customer = $customerQuery->fetch_assoc();

    $networkQuery = $db->query("SELECT * FROM `networks` WHERE `id` = ".$customer['network_id']);

    if ($networkQuery->num_rows !== 1) {
        die('network nicht gefunden');
    }

    $network = $networkQuery->fetch_assoc();

    $purtelMasterAccount = $account['purtel_login'];

    $postData = \Wisotel\Configuration\Configuration::get('purtelAccountValues');

    $postData['skip_timeout'] = 1;

    $postData['anrede'] = $customer['title'] == 'Herr' ? '1' : '2';
    $postData['vorname'] = $customer['firstname'];
    $postData['nachname'] = $customer['lastname'];
    $postData['adresse'] = $customer['street'];

    $houseNrMatch = [];

    preg_match('/(\d+)(.*)/', $customer['streetno'], $houseNrMatch);

    $postData['haus_nr'] = '';

    if (isset($houseNrMatch[1])) {
        $postData['haus_nr'] = $houseNrMatch[1];
    }
    if (isset($houseNrMatch[2])) {
        $postData['zusatz'] = $houseNrMatch[2];
    }

    $postData['plz'] = $customer['zipcode'];
    $postData['ort'] = $customer['city'];
    $postData['version'] = $customer['version'];
    $postData['purtel_use_version'] = $customer['purtel_use_version'];

    $postData['kundennummer_extern'] = $customer['clientid'];

    if ($customer['purtel_use_version'] == 1 && $customer['version'] > 0) {
        $postData['kundennummer_extern'] += '_'.$customer['version'];
    }

    $postData['geburtsdatum'] = $customer['birthday'];

    if ($postData['geburtsdatum'] != '') {
       $birthday = explode('.', $postData['geburtsdatum']);
       $postData['geburtsdatum'] = $birthday[2].$birthday[1].$birthday[0];
    }

    $postData['email'] = $postData['anrede'] == '' ? $customer['emailaddress'] : $customer['bank_account_emailaddress'];

    $postData['ortsnetz'] = $network['area_code'];

    $postData['ip_eingeschraenkt'] = 2;


    // create sip

    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'createcontract'
    );

    $url .= '&'.utf8_decode(http_build_query($postData));


    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        //CURLOPT_URL             => $PurtelUrl1,
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $result = trim($result);
    $result = explode(';', $result);

    if ('+OK' !== $result[0]) {
        var_dump('Failed to create sip', $result, $userId);

        continue;
    }


    // set username and password to account

    $password = str_replace('passwort=', '', $result[3]);
    $username = str_replace('benutzername=', '', $result[2]);

    $db->query("UPDATE `purtel_account` 
        SET `purtel_login` = '".$username."', `purtel_password` = '".$password."', `master` = 0 
        WHERE `id` = ".$account['id']);

    if (!empty($db->error)) {
         var_dump('Failed to set password to master', $result, $userId);

        continue;
    }

    


    // update account

    $updatePostData = $postData;

    unset($updatePostData['skip_timeout']);

    $updatePostData['konto_von'] = $account['purtel_login'];

    $updatePostData['anschluss'] = $username;

    $updatePostData['firma'] = $customer['company'];

    $updatePostData['email'] = $customer['bank_account_emailaddress'];

    if ($updatePostData['email'] == '') {
        $updatePostData['email'] = $customer['emailaddress'];
    }

    $updatePostData['telefon'] = $customer['phoneareacode'].$customer['phonenumber'];
    
    $updatePostData['mobile'] = $customer['mobilephone'];

    $updatePostData['inhaber'] = $customer['bank_account_holder_lastname'];
    $updatePostData['inhaber_adresse'] = $customer['street'].' '.$customer['streetno'];

    $updatePostData['inhaber_plz'] = $customer['zipcode'];
    $updatePostData['inhaber_ort'] = $customer['city'];

    $updatePostData['bank'] = $customer['bank_account_bankname'];

    $updatePostData['BIC'] = $customer['bank_account_bic_new'];

    $updatePostData['IBAN'] = $customer['bank_account_iban'];

    $updatePostData['blz'] = $customer['bank_account_bic'];
    $updatePostData['kontonr'] = $customer['bank_account_number'];

    $updatePostData['einzug'] = empty($updatePostData['IBAN']) ? '' : '1';


    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'changecontract'
    );
    
    $url .= '&'.utf8_decode(http_build_query($updatePostData));

    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        //CURLOPT_URL             => $PurtelUrl1,
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $result = trim($result);
    $result = explode(';', $result);

    if ('+OK' !== $result[0]) {
        var_dump('Failed to update sip', $result, $userId);

        continue;
    }
}






















