<?php

/**
 * Read contents from csv-file
 *
 * @param string $filename
 *
 * @throws InvalidArgumentException
 *
 * @return array
 */
function readCsvFile($filename)
{
    if (!file_exists($filename) || !is_readable($filename)) {
        throw new InvalidArgumentException('Can not open file for reading');
    }

    $content = file_get_contents($filename);

    if (!is_string($content) && !empty($content)) {
        throw new InvalidArgumentException('invalid file');
    }

    $delimiter = null;

    foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
        if (false !== strpos($content, $delimiter_)) {
            $delimiter = $delimiter_;
            break;
        }
    }

    if (null == $delimiter) {
        throw new InvalidArgumentException('can not use file');
    }

    return array_map(function ($line) {
        return str_getcsv($line, ';');
    }, explode($delimiter, $content));
}


$filesToCompare = [
    '/tmp/CL38_stammdaten_steffi.csv',
    '/tmp/CL38_stammdaten_tanja.csv',
];

$files = [];
$headlines = [];

foreach ($filesToCompare as $key => $file) {
    $file = readCsvFile($file);
    $headlines[$key] = array_shift($file);

    if (count(end($file)) !== count($headlines[$key])) {
        array_pop($file);
    }

    $files[$key] = $file;
}



$foundCustomers = [];


foreach ($files as $key => $file) {
    foreach ($file as $lineNum => $line) {
        if (!isset($foundCustomers[$line[0]])) {
            $foundCustomers[$line[0]] = [
                $key => 0,
            ];
        } else if (!isset($foundCustomers[$line[0]][$key])) {
            $foundCustomers[$line[0]][$key] = 0;
        }

        $foundCustomers[$line[0]][$key]++;
    }
}

foreach ($foundCustomers as $kundennummer => $filesArray) {
    if (count($filesArray) !== count($files)) {
        var_dump($kundennummer, $filesArray);
        echo "\n\n";
        continue;
    }

    $last = null;

    foreach ($filesArray as $fileKey => $count) {
        if (null === $last) {
            $last = $count;
            continue;
        }

        if ($count !== $last) {
            var_dump($kundennummer, $filesArray);
            echo "\n\n";
        }
    }
}

die();





// echo "\033[1;31mLog output\033[0m\n"; // red
// echo "\033[1;36mCompare headlines\033[0m\n"; // blue

// compare headlines

echo "\033[1;35mCompare headlines\033[0m\n";

$x = 1;
$of = count($headlines);

foreach ($headlines as $key => $headline) {
    echo "\t\033[1;36mFile ".$x++."/".$of."\033[0m\n";

    foreach ($headlines as $key2 => $headline2) {
        if ($key === $key2) {
            continue;
        }

        $diff = array_diff($headline, $headline2);

        if (!empty($diff)) {
            var_dump("headlines not same", $filesToCompare[$key], $filesToCompare[$key2]);
        }
    }
}

// compare line count

echo "\033[1;35mCompare lines count\033[0m\n";

$x = 1;
$of = count($headlines);

foreach ($files as $key => $file) {
    echo "\t\033[1;36mFile ".$x++."/".$of."\033[0m\n";

    foreach ($files as $key2 => $file2) {
        if ($key === $key2) {
            continue;
        }

        if (count($file) !== count($file2)) {
            var_dump("line count not same", $filesToCompare[$key], $filesToCompare[$key2]);
        }
    }
}

$diffArray = [];
$count = 0;
foreach ($files[0] as $lineNum => $lineArray) {
    if (empty($lineArray[47])) {
        continue;
    }
$count++;
    foreach ($lineArray as $columnNum => $value) {
        $value = trim($value);
        $compValue = trim($files[1][$lineNum][$columnNum]);

        if ($value !== $compValue) {
            if (empty($value) && empty($compValue)) {
                continue;
            }

            if (!isset($diffArray[$lineNum])) {
                $diffArray[$lineNum] = [];
            }

            $diffArray[$lineNum][] = [
                'column' => $columnNum,
                'value' => $value,
                'compValue' => $compValue,
            ];
        }
    }
}
var_dump($count);
//var_dump($diffArray);
//$csvOut = [];

$file = fopen('/tmp/diff.csv', 'w');
fputcsv($file, ['Zeile', 'Spalte', 'Wert (Steffi)', 'Wert (Tanja)'], ';');

foreach ($diffArray as $lineNum => $diff) {
    foreach ($diff as $array) {
        array_unshift($array, $lineNum);

        $array['column'] = $headlines[0][$array['column']];
        fputcsv($file, $array, ';');
        //$csvOut = $array;
    }
}
fclose($file);



/*
// compare KUNDENNUMMER

echo "\033[1;35mCompare KUNDENNUMMER\033[0m\n";

$dataGroupedByKundennummer = [];

$x = 1;
$of = count($headlines);

foreach ($files as $key => $file) {
    echo "\t\033[1;36mFile ".$x++."/".$of."\033[0m\n";

    $dataGroupedByKundennummer[$key] = [];

    foreach ($file as $line) {
        $kundennummer = $line[0];

        if (!isset($dataGroupedByKundennummer[$key][$kundennummer])) {
            $dataGroupedByKundennummer[$key][$kundennummer] = [];
        }

        $dataGroupedByKundennummer[$key][$kundennummer][] = $line;
    }
}

// compare data

echo "\033[1;35mCompare data\033[0m\n";

foreach ($dataGroupedByKundennummer[0] as $kundennummer => $dataLines) {
    $amountDataLines = count($dataLines);

    foreach ($dataGroupedByKundennummer as $fileKey => $dataArray) {
        if (0 === $fileKey) {
            continue;
        }

        $dataLines2 = $dataArray[$kundennummer];

        if ($amountDataLines !== count($dataLines2)) {
            var_dump("not same amount of data for kundennummer (".$kundennummer.")", $filesToCompare[0], $filesToCompare[$fileKey]);
        }

        foreach ($dataLines as $cKey => $cData) {
            $diff = array_diff($cData, $dataLines2[$cKey]);
            $diff2 = array_diff($dataLines2[$cKey], $cData);

            if (!empty($diff)) {
                echo "in ".$filesToCompare[0]." gefunden, aber nicht in ".$filesToCompare[$fileKey]." :: kundennummer (".$kundennummer.")\n";

                foreach ($diff as $pos => $value) {
                    echo $headlines[0][$pos]." :: ".$value."\n";
                }
            }
            if (!empty($diff2)) {
                echo "in ".$filesToCompare[$fileKey]." gefunden, aber nicht in ".$filesToCompare[0]." :: kundennummer (".$kundennummer.")\n";

                foreach ($diff2 as $pos => $value) {
                    echo $headlines[0][$pos]." :: ".$value."\n";
                }
            }
        }
    }
}

echo "\033[1;32mReached end\033[0m\n\n";
*/