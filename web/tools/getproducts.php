<?php
require_once('../config.inc');
require_once($StartPath.'/_conf/database.inc');
require_once $StartPath.'/vendor/wisotel/configuration/Configuration.php';

$username = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$password = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
$action1 = 'getproduktinfo';
$action2 = 'getcrossselling';
$parameter  = array();

// get Purtel product    
echo "----------Start Prutel Product-------------------<br>";
$purtel = array();
$PurtelUrl  = 'https://ipcom.purtel.com/index.php?super_username=' . urlencode($username) . '&super_passwort=' . urlencode($password) . '&action=' . urlencode($action1);
foreach ($parameter as $key => $value) {
    $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
}
$curl = curl_init ($PurtelUrl);
$curl_options = array (
    CURLOPT_URL             => $PurtelUrl,
    CURLOPT_HEADER          => false,
    CURLOPT_RETURNTRANSFER  => true,
    CURLOPT_SSL_VERIFYPEER  => false,
);
curl_setopt_array($curl, $curl_options);
$result = curl_exec($curl);
$lines = explode ("\n", $result);
array_pop($lines);
$headline_csv = strtolower(array_shift ($lines));
$headline = str_getcsv($headline_csv, ';','"');
$i = 0;
while ($line = array_shift ($lines)) {
    $temp = array();
    $purt = str_getcsv($line, ';','"');
    foreach ($headline as $ind => $head) {
        if ($head == 'gueltig_ab' or $head == 'gueltig_bis') {
            if ($purt[$ind]) {
                list($y, $m, $d, $h, $n, $s) = sscanf($purt[$ind], "%04d%02d%02d%02d%02d%02d");
                $purt[$ind] = "$d.$m.$y $h:$n:$s";   
            }
        }
        if ($head == 'id')  $head = 'purtel_id';
        $temp[$head] = $purt[$ind];
    }
    $purtel[$i++] = $temp;
}
curl_close($curl);
$i = 1;
foreach ($purtel as $temp) {
    $var_set = '';
    $sql = "INSERT INTO purtel_products SET ";
    $csv = array();
    foreach ($temp as $key => $value) {
        if ($value) {
            $var_set .= "$key = '".utf8_encode($value)."',\n";
        }
    }
    if ($var_set) {
        $var_set = substr($var_set, 0, -2);
        $sql .= $var_set;
        $db_save_result = $db->query($sql);
        echo "$i - $db_save_result - ".$db->error."<br>$sql<hr>";
        $i++;
    }     
}

// get Purtel crossselling    
echo "----------Start Prutel Crossselling-------------------<br>";
$purtel = array();
$PurtelUrl  = 'https://ipcom.purtel.com/index.php?super_username=' . urlencode($username) . '&super_passwort=' . urlencode($password) . '&action=' . urlencode($action2);
foreach ($parameter as $key => $value) {
    $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
}
$curl = curl_init ($PurtelUrl);
$curl_options = array (
    CURLOPT_URL             => $PurtelUrl,
    CURLOPT_HEADER          => false,
    CURLOPT_RETURNTRANSFER  => true,
    CURLOPT_SSL_VERIFYPEER  => false,
);
curl_setopt_array($curl, $curl_options);
$result = curl_exec($curl);
$lines = explode ("\n", $result);
array_pop($lines);
$headline_csv = strtolower(array_shift ($lines));
$headline = str_getcsv($headline_csv, ';','"');
$i = 0;
while ($line = array_shift ($lines)) {
    $temp = array();
    $purt = str_getcsv($line, ';','"');
    foreach ($headline as $ind => $head) {
        if ($head == 'gueltig_ab' or $head == 'gueltig_bis') {
            if ($purt[$ind]) {
                list($y, $m, $d, $h, $n, $s) = sscanf($purt[$ind], "%04d%02d%02d%02d%02d%02d");
                $purt[$ind] = "$d.$m.$y $h:$n:$s";   
            }
        }
        if ($head == 'id')  $head = 'purtel_id';
        $temp[$head] = $purt[$ind];
    }
    $purtel[$i++] = $temp;
}
curl_close($curl);

$i = 1;
foreach ($purtel as $temp) {
    $var_set = '';
    $sql = "INSERT INTO purtel_crossselling SET ";
    $csv = array();
    foreach ($temp as $key => $value) {
        if ($value) {
            $var_set .= "$key = '".utf8_encode($value)."',\n";
        }
    }
    if ($var_set) {
        $var_set = substr($var_set, 0, -2);
        $sql .= $var_set;
        $db_save_result = $db->query($sql);
        echo "$i - $db_save_result - ".$db->error."<br>$sql<hr>";
        $i++;
    }     
}
$db->close();
?>
