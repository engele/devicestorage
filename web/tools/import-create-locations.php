<?php

require __DIR__.'/../_conf/database.inc';

$q = $db->query("SELECT * FROM `locations` WHERE `name` = 'Cluster 28'");

$d = $q->fetch_assoc();
/*
+------------------------+--------------+------+-----+---------+----------------+
| Field                  | Type         | Null | Key | Default | Extra          |
+------------------------+--------------+------+-----+---------+----------------+
| network_id             | int(11)      | NO   |     | NULL    |                |
| name                   | varchar(128) | NO   |     | NULL    |                |
| kvz_prefix             | varchar(64)  | NO   |     | NULL    |                |
| cid_suffix_start       | int(11)      | NO   |     | NULL    |                |
| cid_suffix_end         | int(11)      | NO   |     | NULL    |                |
| vlan_pppoe             | varchar(4)   | YES  |     | NULL    |                |
| vlan_iptv              | varchar(4)   | YES  |     | NULL    |                |
| vlan_acs               | varchar(4)   | YES  |     | NULL    |                |
| vectoring              | tinyint(1)   | YES  |     | 1       |                |
| logon_port             | varchar(23)  | YES  |     | NULL    |                |
| dslam_ip_adress        | varchar(32)  | YES  |     | NULL    |                |
| logon_id               | varchar(32)  | YES  |     | NULL    |                |
| logon_pw               | varchar(32)  | YES  |     | NULL    |                |
| dslam_conf             | tinyint(1)   | YES  |     | 1       |                |
| dslam_type             | varchar(32)  | YES  |     | ALU     |                |
| vlan_pppoe_local       | varchar(4)   | YES  |     | 7       |                |
| vlan_iptv_local        | varchar(4)   | YES  |     | 8       |                |
| vlan_acs_local         | varchar(4)   | YES  |     | 9       |                |
| acs_type               | varchar(8)   | YES  |     | zeag    |                |
| dslam_ip_real          | varchar(32)  | YES  |     | NULL    |                |
| dslam_router           | varchar(32)  | YES  |     | NULL    |                |
| netz                   | varchar(4)   | YES  |     | /24     |                |
| vlan_dcn               | varchar(4)   | YES  |     | NULL    |                |
| prozessor              | varchar(8)   | YES  |     | NULL    |                |
| dslam_software_release | varchar(100) | YES  |     | NULL    |                |
| pppoe_type             | varchar(32)  | YES  |     | NULL    |                |
+------------------------+--------------+------+-----+---------+----------------+

*/
$clusters = [
    '1',
    '2',
    '3',
    '5',
    '6',
    '7',
    '8',
    '14',
    '16',
    '17',
    '20',
    '21',
    '24',
    '25',
    '26',
    '27',
    '28',
    '29',
    '30',
    '31',
    '32',
    '33',
    '34',
    '35',
    '36',
    '37',
    '38',
    'Rupptertswies',
    'Kösching',
]; 

foreach ($clusters as $cluster) {
    $name = 'Cluster '.$cluster;
    $query = "INSERT INTO `locations` 
    (`network_id`, `name`, `kvz_prefix`, `cid_suffix_start`, `cid_suffix_end`, 
        `vlan_pppoe`, `vlan_iptv`, `vlan_acs`, `vectoring`, `logon_port`, `dslam_ip_adress`, 
        `logon_id`, `logon_pw`, `dslam_conf`, `dslam_type`, `vlan_pppoe_local`, `vlan_iptv_local`, 
        `vlan_acs_local`, `acs_type`, `dslam_ip_real`, `dslam_router`, `netz`, `vlan_dcn`, `prozessor`, 
        `dslam_software_release`, `pppoe_type`) 
    VALUES ('".$d['network_id']."', '".$name."', '".$name."', '".$d['cid_suffix_start']."', '".$d['cid_suffix_end']."', '".$d['vlan_pppoe']."', '".$d['vlan_iptv']."', '".$d['vlan_acs']."', '".$d['vectoring']."', '".$d['logon_port']."', '".$d['dslam_ip_adress']."', '".$d['logon_id']."', '".$d['logon_pw']."', '".$d['dslam_conf']."', '".$d['dslam_type']."', '".$d['vlan_pppoe_local']."', '".$d['vlan_iptv_local']."', '".$d['vlan_acs_local']."', '".$d['acs_type']."', '".$d['dslam_ip_real']."', '".$d['dslam_router']."', '".$d['netz']."', '".$d['vlan_dcn']."', '".$d['prozessor']."', '".$d['dslam_software_release']."', '".$d['pppoe_type']."')";
    

    $db->query($query);

    if (!empty($db->error)) {
        var_dump($db->error, $cluster);
        echo $query."\n";
    }

    $id = $db->insert_id;

    foreach ($d as $column => $value) {
        if (empty($value)) {
            $db->query("UPDATE `locations` SET `".$column."` = NULL WHERE `id` = ".$id);

            if (!empty($db->error)) {
                var_dump($db->error, $cluster);
            }
        }
    }
}

