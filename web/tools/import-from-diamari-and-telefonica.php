<?php

/**
 * Import customer-data from Dimari and Telefonica
 */

/**
 * Read contents from csv-file
 *
 * @param string $filename
 *
 * @throws InvalidArgumentException
 *
 * @return array
 */
function readCsvFile($filename)
{
    if (!file_exists($filename) || !is_readable($filename)) {
        throw new InvalidArgumentException('Can not open file for reading');
    }

    $content = file_get_contents($filename);

    if (!is_string($content) && !empty($content)) {
        throw new InvalidArgumentException('invalid file');
    }

    $delimiter = null;

    foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
        if (false !== strpos($content, $delimiter_)) {
            $delimiter = $delimiter_;
            break;
        }
    }

    if (null == $delimiter) {
        throw new InvalidArgumentException('can not use file');
    }

    return array_map(function ($line) {
        return str_getcsv($line, ';');
    }, explode($delimiter, $content));
}

$dimariCsv = readCsvFile('/tmp/dimari.csv');
$telefonicaCsv = readCsvFile('/tmp/telefonica.csv');

$dimariHeader = array_shift($dimariCsv);
$dimariHeaderReversed = [];
$telefonicaHeader = array_shift($telefonicaCsv);
$telefonicaHeaderReversed = [];

foreach ($dimariHeader as $column => $name) {
    $dimariHeaderReversed[$name] = $column;
}
foreach ($telefonicaHeader as $column => $name) {
    $telefonicaHeaderReversed[$name] = $column;
}

if (count(end($telefonicaCsv)) !== count($telefonicaHeader)) {
    array_pop($telefonicaCsv);
}
if (count(end($dimariCsv)) !== count($dimariHeader)) {
    array_pop($dimariCsv);
}

//var_dump($dimariHeader, $telefonicaHeader);

// find data from dimari within telefonica

/**
 * Find lines with specific column content in telefonica-data
 * 
 * @return array|null
 */
$findInTelefonica = function ($matching) use (&$telefonicaCsv, &$telefonicaHeaderReversed)
{
    $matches = null;

    foreach ($matching as $column => $matchingValue) {
        if (null === $matches) {
            $matches = [];

            foreach ($telefonicaCsv as $key => $line) {
                if ($line[$telefonicaHeaderReversed[$column]] === $matchingValue) {
                    $matches[$key] = $line;
                }
            }
        } elseif (count($matches) > 0) {
            foreach ($matches as $key => $line) {
                if ($line[$telefonicaHeaderReversed[$column]] !== $matchingValue) {
                    unset($matches[$key]);
                }
            }
        } else {
            return null;
        }
    }

    return count($matches) > 0 ? $matches : null;
};
/*
$originalTelefonicaCsv = $telefonicaCsv;

$dimariWithoutTelefonica = [];

$foundTelefonicaForDimariCustomer = [];

foreach ($dimariCsv as $key => $line) {
    // hier die dimari-daten importieren.
    // achtung, dass auch die doppelten (wegen rufnummer) importiert werden

    if (isset($foundTelefonicaForDimariCustomer[$line[$dimariHeaderReversed['KUNDENNUMMER']]])) {
        continue; // multiple dimari entries
    } else {

    }

    $matchedLines = $findInTelefonica([
        'street' => $line[$dimariHeaderReversed['Straße']],
        'housenumber' => $line[$dimariHeaderReversed['HAUSNR']],
        'lastname' => $line[$dimariHeaderReversed['NAME']],
        'firstname' => $line[$dimariHeaderReversed['VORNAME']],
        'zipcode' => $line[$dimariHeaderReversed['PLZ']],
    ]);
    //$asd = preg_replace('/^0/', '', $line[$dimariHeaderReversed['SUBSCRIBER_ID']]);
    //$asd = preg_replace('/[^0-9]/', '', $asd);

    //$matchedLines = $findInTelefonica([
    //    'rn' => $asd,
    //]);

    if (!empty($matchedLines)) {
        $foundTelefonicaForDimariCustomer[$line[$dimariHeaderReversed['KUNDENNUMMER']]] = true;

        foreach ($matchedLines as $key => $data) {
            unset($telefonicaCsv[$key]);

            // hier geht es weiter mit dem import der voip-daten aus dem telefonica-export
        }
    } else {
        $asd = preg_replace('/^0/', '', $line[$dimariHeaderReversed['SUBSCRIBER_ID']]);
        $asd = preg_replace('/[^0-9]/', '', $asd);

        $matchedLines = $findInTelefonica([
            'rn' => $asd,
        ]);

        if (!empty($matchedLines)) {
            // etwas gefunden, dass zur telefonnr. passt
            $rn = end($matchedLines);
            $line[] = $rn[2];
        } else {
            // definitiv nichts gefunden
            $line[] = null;
        }

        $dimariWithoutTelefonica[$key] = $line;
    }
}

var_dump(count($telefonicaCsv)); // telefonica without dimari
var_dump(count($dimariWithoutTelefonica));

// create info files for zagler

$file = fopen('/tmp/Eintraege-aus-Telefonica-Export-die-nicht-im-Dimari-Export-gefunden-wurden.csv', 'a+');
fputcsv($file, $telefonicaHeader, ';');
foreach ($telefonicaCsv as $line) {
    fputcsv($file, $line, ';');
}
fclose($file);

$file = fopen('/tmp/Eintraege-aus-Dimari-Export-die-nicht-im-Telefonica-Export-gefunden-wurden.csv', 'a+');
$dimariHeader[] = 'moegliche rn (aus Telefonica-Export)';
fputcsv($file, $dimariHeader, ';');
foreach ($dimariWithoutTelefonica as $line) {
    fputcsv($file, $line, ';');
}
fclose($file);
*/


require __DIR__.'/../_conf/database.inc';

$locations = [
    'Cluster 21' => null,
    'Cluster 1' => null,
    'Cluster 2' => null,
    'Cluster 20' => null,
    'Cluster 7' => null,
    'Cluster 36' => null,
    'Cluster 3' => null,
    'Cluster 27' => null,
    'Cluster 33' => null,
    'Cluster 24' => null,
    'Cluster 35' => null,
    'Cluster 14' => null,
    'Cluster 38' => null,
    'Cluster 31' => null,
    'Cluster 5' => null,
    'Cluster 0' => null,
    'Cluster 29' => null,
    'Cluster 30' => null,
    'Cluster 25' => null,
    'Cluster 28' => null,
    'Cluster 34' => null,
    'Cluster 6' => null,
    'Cluster 23' => null,
    'Cluster 32' => null,
    'Cluster 26' => null,
    'cluster 29' => null,
    'Cluster Großmehring' => null,
    'Cluster 11' => null,
    'Cluster 37' => null,
    'Cluster 17' => null,
    'cluster 32' => null,
    'Cluster Kösching' => null,
    'CLuster 35' => null,
    'Cluster 16' => null,
    'Cluster 8' => null,
];

foreach ($locations as $name => $null) {
    $db->query(sprintf('INSERT INTO `locations` (`%s`, `%s`, `%s`, `%s`, `%s`) VALUES ("%s", "%s", "%s", "%s", "%s")',
        'network_id',
        'name',
        'kvz_prefix',
        'cid_suffix_start',
        'cid_suffix_end',
        '35',
        $name,
        $name,
        '1',
        '9999'
    ));

    if (!empty($db->error)) {
        throw new Exception($db->error);
    }

    $locations[$name] = $db->insert_id;
}




$foundTelefonicaForDimariCustomer = [];
$countClientId = 1;

foreach ($dimariCsv as $key => $line) {
    // hier die dimari-daten importieren.
    // achtung, dass auch die doppelten (wegen rufnummer) importiert werden

    if (isset($foundTelefonicaForDimariCustomer[$line[$dimariHeaderReversed['KUNDENNUMMER']]])) {
        // add only purtel

        $id = $foundTelefonicaForDimariCustomer[$line[$dimariHeaderReversed['KUNDENNUMMER']]];
    } else {
        // insert customer

        // clientid
        $clientId = '50000.17.'.str_pad($countClientId++, 4, "0", STR_PAD_LEFT);

        // connection_activation_date      VERTRAGSAKTIVIERUNG
        $connectionActivationDate = $line[$dimariHeaderReversed['VERTRAGSAKTIVIERUNG']];

        // network
        $networkId = 35;

        // customer_id      KUNDENNUMMER
        $customerId = $line[$dimariHeaderReversed['KUNDENNUMMER']];

        // contract_id      VERTRAGSNUMMER
        $contractId = $line[$dimariHeaderReversed['VERTRAGSNUMMER']];

        // bank_account_holder_lastname         KTOINHABER (Achtung - vor-und zuname)
        $bankAccountHolderLastname = $line[$dimariHeaderReversed['KTOINHABER']];

        // bank_account_bic      BIC
        $bankAccountBic = $line[$dimariHeaderReversed['BIC']];

        // bank_account_iban         IBAN
        $bankAccountIban = $line[$dimariHeaderReversed['IBAN']];

        // bank_account_emailaddress        Emailadr.
        $bankAccountEmailaddress = $line[$dimariHeaderReversed['Emailadr.']];

        // wisocontract_cancel_date      VERTRAGSDEAKTIVIERUNG
        $wisocontractCancelDate = $line[$dimariHeaderReversed['VERTRAGSDEAKTIVIERUNG']];

        // wisocontract_switchoff_finish         VERTRAGSDEAKTIVIERUNG
        $wisocontractSwitchoffFinish = $line[$dimariHeaderReversed['VERTRAGSDEAKTIVIERUNG']];

        // connection_street        Straße
        $connectionStreet = $line[$dimariHeaderReversed['Straße']];

        // connection_streetno      HAUSNR+Hausnr.zus.
        $connectionStreetNo = $line[$dimariHeaderReversed['HAUSNR']].$line[$dimariHeaderReversed['Hausnr.zus.']];

        // connection_city       ORT
        $connectionCity = $line[$dimariHeaderReversed['ORT']];

        // connection_zipcode        PLZ
        $connectionZipcode = $line[$dimariHeaderReversed['PLZ']];

        // title        ANREDE
        $title = $line[$dimariHeaderReversed['ANREDE']];

        // firstname        VORNAME
        $firstname = $line[$dimariHeaderReversed['VORNAME']];

        // lastname         NAME
        $lastname = $line[$dimariHeaderReversed['NAME']];

        // street        Straße
        $street = $line[$dimariHeaderReversed['Straße']];

        // streetno      HAUSNR+Hausnr.zus.
        $streetno = $line[$dimariHeaderReversed['HAUSNR']].$line[$dimariHeaderReversed['Hausnr.zus.']];

        // city      ORT
        $city = $line[$dimariHeaderReversed['ORT']];

        // zipcode       PLZ
        $zipcode = $line[$dimariHeaderReversed['PLZ']];

        // district         ORTSTEIL
        $district = 'Ingolstadt';

        // birthday         GEBURTSDATUM
        $birthday = $line[$dimariHeaderReversed['GEBURTSDATUM']];

        // emailaddress         Emailadr.
        $emailaddress = $line[$dimariHeaderReversed['Emailadr.']];

        // stati_port_confirm_date      PORTIERUNGSTERMIN
        $statiPortConfirmDate = $line[$dimariHeaderReversed['PORTIERUNGSTERMIN']];

        // company      FIRMA
        $company = $line[$dimariHeaderReversed['FIRMA']];

        // contract_version         VERTRAGSUNTERSCHRIFT
        $contractVersion = $line[$dimariHeaderReversed['VERTRAGSUNTERSCHRIFT']];

        // comment         ABRECHNUNGSGRUPPE + COV_ID
        $comment = sprintf("ABRECHNUNGSGRUPPE: %s\nCOV_ID: %s",
            $line[$dimariHeaderReversed['ABRECHNUNGSGRUPPE']],
            $line[$dimariHeaderReversed['COV_ID']]
        );

        // no_eze       Lastschriftzahler (0 = 'no')
        $noEze = '0' == $line[$dimariHeaderReversed['ABRECHNUNGSGRUPPE']] ? 'no' : '';

        $creditRatingOk = '';
        $creditRatingDate = '';

        if ('no' != $noEze) {
            $creditRatingOk = 'successful';
            $creditRatingDate = '01.01.2000';
        }

        // clienttype       KUNDENGRUPPE (commercial - non_commercial)
        $clienttype = 'non_commercial';


        $purtelEditDone = '';
        $wisocontractCanceledDate = '';

        switch ($line[$dimariHeaderReversed['STATUS']]) {
            case 'Aktiv':
                // purtel_edit_done        YES
                $purtelEditDone = 'yes';

                break;

            case 'Gekündigt':
                // wisocontract_canceled_date      1.1.2000
                $wisocontractCanceledDate = '01.01.2000';

                break;

            case 'Teilbereitstellung':
            case 'Testkunde':
            case 'Willkommensbrief':
            case 'Auftragsbestätigung':
                break;
        }


        $locationId = trim($line[$dimariHeaderReversed['ORTSTEIL']]);

        if (!empty($locationId)) {
            $locationId = $locations[
                $locationId
            ];

            if (!is_numeric($locationId)) {
                var_dump( trim($line[$dimariHeaderReversed['ORTSTEIL']]) );
                throw new Exception('location');
            }
        }

        $query = sprintf('INSERT INTO `customers` 
            (`%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, 
                `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, 
                `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, 
                `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`, `%s`) 
            VALUES 
            ("%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", 
                "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", 
                "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", 
                "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s")',

            'clientid',
            'connection_activation_date',
            'network_id',
            'customer_id',
            'contract_id',
            'bank_account_holder_lastname',
            'bank_account_bic_new',
            'bank_account_iban',
            'bank_account_emailaddress',
            'wisocontract_cancel_date',
            'wisocontract_switchoff_finish',
            'connection_street',
            'connection_streetno',
            'connection_city',
            'connection_zipcode',
            'title',
            'firstname',
            'lastname',
            'street',
            'streetno',
            'city',
            'zipcode',
            'district',
            'birthday',
            'emailaddress',
            'stati_port_confirm_date',
            'company',
            'contract_version',
            'comment',
            'no_eze',
            'credit_rating_ok',
            'credit_rating_date',
            'clienttype',
            'purtel_edit_done',
            'wisocontract_canceled_date',
            'application_text',
            'application',
            'prospect_supply_status',
            'tal_order_date',
            'tal_dtag_assignment_no',
            'customer_connect_info_date',
            'tal_order_ack_date',
            'dslam_arranged_date',
            'terminal_ready',
            'patch_date',
            'location_id',
            'contract_sent_date',
            'contract_received_date',

            $clientId,
            $connectionActivationDate,
            $networkId,
            $customerId,
            $contractId,
            $bankAccountHolderLastname,
            $bankAccountBic,
            $bankAccountIban,
            $bankAccountEmailaddress,
            $wisocontractCancelDate,
            $wisocontractSwitchoffFinish,
            $connectionStreet,
            $connectionStreetNo,
            $connectionCity,
            $connectionZipcode,
            $title,
            $firstname,
            $lastname,
            $street,
            $streetno,
            $city,
            $zipcode,
            $district,
            $birthday,
            $emailaddress,
            $statiPortConfirmDate,
            $company,
            $contractVersion,
            $comment,
            $noEze,
            $creditRatingOk,
            $creditRatingDate,
            $clienttype,
            $purtelEditDone,
            $wisocontractCanceledDate,
            'with_supply',
            'green',
            'with_supply',
            'N/E da GF',
            'N/E da GF',
            'N/E da GF',
            'N/E da GF',
            '01.01.2000',
            '01.01.2000',
            '01.01.2000',
            $locationId,
            '01.01.2000',
            '01.01.2000'
        );
        
        $db->query($query);

        if (!empty($db->error)) {
            throw new Exception($db->error);
        }

        // id
        $id = $db->insert_id;

        $foundTelefonicaForDimariCustomer[$line[$dimariHeaderReversed['KUNDENNUMMER']]] = $id; // save for other entries
    }

    $nummer = preg_replace('/[^0-9]/', '', $line[$dimariHeaderReversed['SUBSCRIBER_ID']]);
    $purtelLogin = null;

    $rn = preg_replace('/^0/', '', $nummer);

    $matchedLines = $findInTelefonica([
        'rn' => $rn,
    ]);

    if (!empty($matchedLines)) {
        $telefonicaData = end($matchedLines); // ist immer 1

        $purtelLogin = $telefonicaData[$telefonicaHeaderReversed['rn']];
    }

    $query = sprintf('INSERT INTO `purtel_account` (`purtel_login`, `nummer`, `typ`, `cust_id`) VALUES (%s, %s, %s, %s)',
        null === $purtelLogin ? 'NULL' : '"'.$purtelLogin.'"',
        '"'.$nummer.'"',
        '1',
        '"'.$id.'"'
    );

    $db->query($query);

    if (!empty($db->error)) {
        throw new Exception($db->error);
    }
}


?>
