<?php

require __DIR__.'/../_conf/database.inc';

$q = $db->query("SELECT * FROM `purtel_account` ORDER BY `cust_id` ASC, `purtel_login` ASC");

$data = []; 

while ($d = $q->fetch_assoc()) {
    if (!isset($data[$d['purtel_login']])) {
        $data[$d['purtel_login']] = [];
    }

    $data[$d['purtel_login']][] = $d;
}


echo 'ikv_customer_id;purtel_sip_account;phonenumber';
echo "\n";

foreach ($data as $purtelLogin => $d) {
    foreach ($d as $account) {
        if (empty($account['nummer'])) {
            continue;
        }

        echo sprintf('%s;%s;"%s"',
            $account['cust_id'],
            $account['purtel_login'],
            $account['nummer']
        );

        echo "\n";
    }
}