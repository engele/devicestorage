<?php

require __DIR__.'/../../../_conf/database.inc';
require_once __DIR__.'/../../../vendor/wisotel/configuration/Configuration.php';

/**
 * Get buchungen for all accounts by kundennummer_extern
 */
function getBuchungen($clientId)
{
    $purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
    $purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

    $kundennummer_extern = $clientId;

    $parameter = [
        'kundennummer_extern' => $kundennummer_extern,
        'von_datum' => '20180101',
        'bis_datum' => '20180420',
        //'erweitert' => '2',
    ];

    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'getbuchungen'
    );
    
    $url .= '&'.utf8_decode(http_build_query($parameter));

    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $content = trim($result);
    
    $delimiter = null;

    foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
        if (false !== strpos($content, $delimiter_)) {
            $delimiter = $delimiter_;
            break;
        }
    }

    if (null == $delimiter) {
        throw new Exception('keine Buchungen gefunden');
    }

    $result = array_map(function ($line) {
        $data = str_getcsv($line, ';');

        foreach ($data as $key => $value) {
            if (is_scalar($value)) {
                if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                    $data[$key] = substr($value, 1, -1);
                } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                    $data[$key] = substr($value, 1, -1);
                }
            }
        }

        return $data;
    }, explode($delimiter, $content));

    return $result;
}

$csvHeadline = null;
$csvData = [];
$csvHandle = fopen('php://output', 'w');

$customersQ = $db->query("SELECT DISTINCT(c.`clientid`) as clientid, c.`customer_id` as dimariId, l.`name` as cluster 
    FROM `customers` c 
    LEFT JOIN `locations` l ON l.`id` = c.`location_id` 
    WHERE c.`clientid` LIKE '50000.17.%'
");

while ($customer = $customersQ->fetch_assoc()) {
    try {
        $buchungen = getBuchungen($customer['clientid']);

        if (null === $csvHeadline) {
            $csvHeadline = $buchungen[0];
        }

        unset($buchungen[0]);

        foreach ($buchungen as $buchung) {
            $buchung[3] = utf8_encode($buchung[3]);
            $buchung[4] = (float) $buchung[4] / 100;
            $buchung[5] = (float) $buchung[5] / 100;
            $buchung[7] = preg_replace('/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/', '$3.$2.$1-$4:$5:$6', $buchung[7]);

            $buchung[] = $customer['dimariId'];
            $buchung[] = $customer['cluster'];

            fputcsv($csvHandle, $buchung, ';');
        }
    } catch (Exception $e) {
        fputcsv($csvHandle, [
            $e->getMessage(),
            'ikv: '.$customer['clientid'],
            'dimari: '.$customer['dimariId'],
        ], ';');
    }
}

$csvHeadline[] = 'Dimari-Kundennummer';

fputcsv($csvHandle, $csvHeadline, ';');

fclose($csvHandle);
