<?php

require __DIR__.'/../../../_conf/database.inc';
require_once __DIR__.'/../../../vendor/wisotel/configuration/Configuration.php';
require __DIR__.'/../Customer.php';
require __DIR__.'/../CsvReader.php';
require __DIR__.'/../PurtelSepa.php';

const SQL_CUSTOMER = "SELECT * FROM customers WHERE %s";

$purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

$cust = array();
$csvfile = isset($argv[1]) ? $argv[1] : '';
$where = '';
$purtelSepa = new PurtelSepa($db);
$clientid = NULL;
if(file_exists($csvfile)) {
    $csv = new CsvReader($csvfile);
    foreach ($csv->data as $value){
        echo "-------------------\n";
        $clientid = empty ($value[$csv->headlineReversed['Kontonummer IKV']]) ? NULL : $value[$csv->headlineReversed['Kontonummer IKV']];
        $iban = empty ($value[$csv->headlineReversed['IBAN']]) ? NULL : $value[$csv->headlineReversed['IBAN']];
        $kontoinhaber = empty ($value[$csv->headlineReversed['Name 1']]) ? NULL : $value[$csv->headlineReversed['Name 1']];
        $mandatenid = empty ($value[$csv->headlineReversed['Mandatsreferenz']]) ? NULL : $value[$csv->headlineReversed['Mandatsreferenz']];
        $debitor = empty ($value[$csv->headlineReversed['Debitor']]) ? NULL : $value[$csv->headlineReversed['Debitor']];

        $sql = sprintf (SQL_CUSTOMER, "id = '$clientid'");

        $purtelSepa->initSepa($db, $purtelSuperUsername, $purtelSuperPassword, $sql, $iban, $mandatenid, $debitor);
        echo "- ".$mandatenid."\n";

        try {
            $purtelSepa->setSepa();     
        } catch (\UnexpectedValueException $e) {
            fwrite (STDERR, $e->getMessage()."\n"); 
        }
    }
}

/*
if(file_exists($csvfile)) {
    $csv = new CsvReader($csvfile);
    foreach ($csv->data as $temp){
        $pur->initSepa($purtelSuperUsername, $purtelSuperPassword, $db, 'SELECT * FROM customers');
        try {
            $pur->setSepa();     
        } catch (\UnexpectedValueException $e) {
            fwrite (STDERR, $e->getMessage()."\n"); 
        }
    }
} else {
    fwrite (STDERR, "Warning - csv file '$csvfile' not found\n");
    //$customersQuery = $db->query("SELECT * FROM `customers` WHERE `id` BETWEEN 7588 AND 7967");
    //$customersQuery = $db->query("SELECT * FROM `customers` WHERE `id` IN(7674,7749,7752,7858,7938)");
    //$customersQuery = $db->query("SELECT * FROM `customers` WHERE `clientid` LIKE '50000.17.%' " );
    //$customersQuery = $db->query("SELECT * FROM `customers` WHERE `clientid` IN ('50000.17.0040','50000.17.0041','50000.17.0042','50000.17.0043','50000.17.0044','50000.17.0597','50000.17.0569','50000.17.0874')" );
    //$customersQuery = $db->query("SELECT * FROM `customers` WHERE `clientid` IN ('50000.17.0597','50000.17.0569','50000.17.0874')" );
}
*/

/*
while ($customer = $customersQuery->fetch_assoc()) {
    unset($resultLine);
    unset($resultHeader);
    unset($resultStatus);
    
    // get sepa

    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s&art=1&kundennummer_extern=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'createsepa',
        urlencode($customer['clientid'])
    );

    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $resultLine = explode("\n", $result);
    $resultHeader = str_getcsv(array_shift($resultLine),';');
    $resultStatus = $resultHeader[0];
    
    if (substr($resultStatus, 0, 4) == '-ERR') {
        fwrite(STDERR, $customer['clientid'].': Error - '.$resultStatus."\n");
        continue;
    }
    
    if ($resultLine[0]) {
        if (substr($resultLine[0], 0, 4) == '-ERR') {
            $bicAlt = $bic = $customer['bank_account_bic_new'];
            
            if (!isset($banklist[$bic])) {
                if (substr($bic, -3) == 'XXX'){
                    $bicAlt = substr($bic, 0, strlen($bic) - 3);
                    if (!isset($banklist[$bicAlt])) {
                        $bankname = "Bankname nicht gefunden";    
                    } else {
                        $bankname = $banklist[$bicAlt];   
                    }  
                }
            } else {
                $bankname = $banklist[$bic];
            }
            fwrite(STDERR, $customer['clientid'].": Info - Insert Bankname: $bic ($bicAlt): $bankname\n");
            
            // insert bankname

            $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s&art=1&kundennummer_extern=%s&bank=%s',
                urlencode($purtelSuperUsername),
                urlencode($purtelSuperPassword),
                'changecontract',
                urlencode($customer['clientid']),
                urlencode($bankname)
            );

            $curl = curl_init($url);

            curl_setopt_array($curl, array(
                CURLOPT_HEADER          => false,
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_SSL_VERIFYPEER  => false,
            ));


            $result = curl_exec($curl);

            curl_close($curl);
            if (substr($result, 0, 4) == '-ERR') {
                fwrite(STDERR, $customer['clientid'].": Error - Inserting Bankname: $result\n");
                continue;
            }

            unset($resultLine);
            unset($resultHeader);
            unset($resultStatus);
            // get sepa again

            $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s&art=1&kundennummer_extern=%s',
                urlencode($purtelSuperUsername),
                urlencode($purtelSuperPassword),
                'createsepa',
                urlencode($customer['clientid'])
            );

            $curl = curl_init($url);

            curl_setopt_array($curl, array(
                CURLOPT_HEADER          => false,
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_SSL_VERIFYPEER  => false,
            ));


            $result = curl_exec($curl);

            curl_close($curl);

            $resultLine = explode("\n", $result);
            $resultHeader = str_getcsv(array_shift($resultLine),';');
            $resultStatus = $resultHeader[0];
       }

        $resultData = str_getcsv(array_shift($resultLine),';');
        foreach ($resultHeader as $key => $value){
            $header[$value] = isset($resultData[$key]) ? $resultData[$key] : '';
        }
        $id_version = strstr ($header['kundennummer_extern'], '-');
        if ($id_version) {
            echo $customer['clientid'].': SEPA Created Madant-ID:'.$header['kundennummer_extern']."\n";   
        } else {
            fwrite(STDERR, $customer['clientid'].': Warning - SEPA Exists '.$header['kundennummer_extern']." ".$header['create_status']."\n");
            continue;
        }
    }

    unset($resultLine);
    unset($resultHeader);
    unset($resultStatus);
    
    // create and activate sepa

    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s&art=1&erzeugen=1&aktivieren=1&kundennummer_extern=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'createsepa',
        urlencode($customer['clientid'])
    );

    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $resultLine = explode("\n", $result);
    $resultHeader = str_getcsv(array_shift($resultLine),';');
    $resultStatus = $resultHeader[0];
    
    if (substr($resultStatus, 0, 4) == '-ERR') {
        fwrite(STDERR, $customer['clientid'].': Error - '.$resultStatus."\n");
        continue;
    }
}
*/
?>
