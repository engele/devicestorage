<?php

require __DIR__.'/../../../_conf/database.inc';

$timeZone = new \DateTimeZone('Europe/Berlin');
$today = new \DateTime('now', $timeZone);

$q = $db->query("SELECT * FROM customer_products WHERE propperly_copyed_to_customer = 1");

while ($d = $q->fetch_assoc()) {
    if (empty($d['deactivation_date'])) {
        continue;
    }

    $deactivationDate = new \DateTime($d['deactivation_date'], $timeZone);

    if ($deactivationDate > $today) {
        continue; // is still active
    }

    //var_dump($d);
    //SELECT `id` FROM `products` WHERE id IN (%s) AND type = 'Tarif'

    $allProductsForCustomerQ = $db->query("SELECT cp.*, p.`type` 
        FROM `customer_products` cp 
        LEFT JOIN `products` p 
        ON cp.`product_id` = p.`id` 
        WHERE cp.`customer_id` = ".$d['customer_id']." AND cp.`propperly_copyed_to_customer` != 1");

    $otherMainProducts = [];

    while ($cpd = $allProductsForCustomerQ->fetch_assoc()) {
        if ('Tarif' !== $cpd['type']) {
            continue;
        }

        $otherMainProducts[] = $cpd;
    }

    if (empty($otherMainProducts)) {
        var_dump($today, $deactivationDate);
        die(var_dump('has no other main products', $d));
    }

    if (count($otherMainProducts) > 1) {
        die(var_dump('has to many other main products', $d));
    }
}


/*
$c = [];

$q = $db->query("SELECT cp.*, c.`purtel_product`, p.`id` as customerPurtelProductId 
    FROM customer_products cp 
    LEFT JOIN customers c ON c.`id` = cp.`customer_id` 
    LEFT JOIN products p ON p.`identifier` = REPLACE(c.`purtel_product`, 'Dimari-', '') 
    WHERE cp.`propperly_copyed_to_customer` = 1");

while ($d = $q->fetch_assoc()) {
    if (!isset($c[$d['customer_id']])) {
        $c[$d['customer_id']] = [];
    }

    $c[$d['customer_id']][] = $d;
}

foreach ($c as $customerId => $array) {
    if (count($array) < 2) {
        unset($c[$customerId]);
        continue;
    }

    $customerCurrentActiveProduct = $array[0]['customerPurtelProductId'];

    foreach ($array as $customerProduct) {
        if ($customerProduct['product_id'] !== $customerCurrentActiveProduct) {

        }
    }

    //$x = $db->query()
    //var_dump($array);
}
var_dump($c);
*/
