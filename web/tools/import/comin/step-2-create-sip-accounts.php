<?php

require __DIR__.'/../../../_conf/database.inc';
require_once __DIR__.'/../../../vendor/wisotel/configuration/Configuration.php';
require __DIR__.'/../PurtelAccount.php';
require __DIR__.'/../Customer.php';

$purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

$skipUsers = [
];

//$customersQuery = $db->query("SELECT * FROM `customers` WHERE `id` BETWEEN 8175 AND 8406");
$customersQuery = $db->query("SELECT * FROM `customers` WHERE `id` BETWEEN 16126 AND 16431");
//$customersQuery = $db->query("SELECT * FROM `customers` WHERE `id` IN(16376)");

while ($customer = $customersQuery->fetch_assoc()) {
    $customerObject = new Customer();
    $customerObject->id = $customer['id'];
    
    echo "start: ".$customerObject->id."\n";

    if (in_array($customerObject->id, $skipUsers)) {
        echo "skipped\n\n";
        continue;
    }

    $purtelMasterAccount = null;
    $purtelSubAccounts = [];

    $purtelAccountsQuery = $db->query("SELECT * FROM `purtel_account` WHERE `cust_id` = ".$customer['id']);

    if ($purtelAccountsQuery->num_rows > 0) {
        // has imported purtel-accounts
        while ($purtelAccount = $purtelAccountsQuery->fetch_assoc()) {
            $purtelAccount = PurtelAccount::createFromArray($purtelAccount);
            $purtelAccount->isNew = false; // important!
            $purtelAccount->customer = $customerObject;

            if (1 === (int) $purtelAccount->master) {
                if (null !== $purtelMasterAccount) {
                    throw new \UnexpectedValueException(sprintf('customer (%s) has multiple master purtel-accounts (%s) and (%s)',
                        $customerObject->id,
                        $purtelMasterAccount->id,
                        $purtelAccount->id
                    ));
                }

                $purtelMasterAccount = $purtelAccount;

                continue; // don't append master accout to $purtelSubAccounts
            }

            $purtelSubAccounts[] = $purtelAccount;
        }
    } else {
        // has no purtel-accounts
        $purtelAccount = new PurtelAccount();
        $purtelAccount->customer = $customerObject;
        $purtelAccount->master = 1;

        $purtelMasterAccount = $purtelAccount;
    }

    // get other required data
    $networkQuery = $db->query("SELECT * FROM `networks` WHERE `id` = ".$customer['network_id']);

    if ($networkQuery->num_rows !== 1) {
        die('network nicht gefunden');
    }

    $network = $networkQuery->fetch_assoc();

    // create master account

    $postData = \Wisotel\Configuration\Configuration::get('purtelAccountValues');

    $postData['anrede'] = $customer['title'] == 'Herr' ? '1' : '2';
    $postData['vorname'] = $customer['firstname'];
    $postData['nachname'] = $customer['lastname'];
    $postData['adresse'] = $customer['street'];

    $houseNrMatch = [];

    preg_match('/(\d+)(.*)/', $customer['streetno'], $houseNrMatch);

    $postData['haus_nr'] = '';

    if (isset($houseNrMatch[1])) {
        $postData['haus_nr'] = $houseNrMatch[1];
    }
    if (isset($houseNrMatch[2])) {
        $postData['zusatz'] = $houseNrMatch[2];
    }

    $postData['plz'] = $customer['zipcode'];
    $postData['ort'] = $customer['city'];
    $postData['version'] = $customer['version'];
    $postData['purtel_use_version'] = $customer['purtel_use_version'];

    $postData['kundennummer_extern'] = $customer['clientid'];

    if ($customer['purtel_use_version'] == 1 && $customer['version'] > 0) {
        $postData['kundennummer_extern'] += '_'.$customer['version'];
    }

    $postData['geburtsdatum'] = $customer['birthday'];

    if ($postData['geburtsdatum'] != '') {
       $birthday = explode('.', $postData['geburtsdatum']);
       $postData['geburtsdatum'] = $birthday[2].$birthday[1].$birthday[0];
    }

    $postData['email'] = $postData['anrede'] == '' ? $customer['emailaddress'] : $customer['bank_account_emailaddress'];

    $postData['ortsnetz'] = $network['area_code'];

    $postData['ip_eingeschraenkt'] = 2;


    // create sip

    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'createcontract'
    );

    $url .= '&'.utf8_decode(http_build_query(array_merge($postData, ['skip_timeout' => 1])));

    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        //CURLOPT_URL             => $PurtelUrl1,
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $result = trim($result);
    $result = explode(';', $result);

    if ('+OK' !== $result[0]) {
        var_dump('Failed to create sip', $result, $customerObject->id);
        echo $url."\n\n";

        continue;
    }

    // set username and password to master account

    $username = str_replace('benutzername=', '', $result[2]);
    $password = str_replace('passwort=', '', $result[3]);

    $purtelMasterAccount->purtelLogin = $username;
    $purtelMasterAccount->purtelPassword = $password;

    if ($purtelMasterAccount->isNew) {
        $db->query($purtelMasterAccount->buildInsertQuery());

        if (!empty($db->error)) {
             var_dump('Failed insert master', $result, $customerObject->id);

            continue;
        }

        $purtelMasterAccount->id = $db->insert_id;
        $purtelMasterAccount->isNew = false;
    } else {
        $db->query("UPDATE `purtel_account` SET `purtel_login` = '".$username."', `purtel_password` = '".$password."' WHERE `id` = ".$purtelMasterAccount->id);

        if (!empty($db->error)) {
            var_dump('Failed to set password to master', $result, $customerObject->id);

            continue;
        }
    }

    // update account

    $updatePostData = $postData;

    $updatePostData['anschluss'] = $username;

    $updatePostData['firma'] = $customer['company'];

    $updatePostData['email'] = $customer['bank_account_emailaddress'];

    if ($updatePostData['email'] == '') {
        $updatePostData['email'] = $customer['emailaddress'];
    }

    $updatePostData['telefon'] = $customer['phoneareacode'].$customer['phonenumber'];
    
    $updatePostData['mobile'] = $customer['mobilephone'];

    $updatePostData['inhaber'] = $customer['bank_account_holder_lastname'];
    $updatePostData['inhaber_adresse'] = $customer['street'].' '.$customer['streetno'];

    $updatePostData['inhaber_plz'] = $customer['zipcode'];
    $updatePostData['inhaber_ort'] = $customer['city'];

    $updatePostData['bank'] = $customer['bank_account_bankname'];

    $updatePostData['BIC'] = $customer['bank_account_bic_new'];

    $updatePostData['IBAN'] = $customer['bank_account_iban'];

    $updatePostData['blz'] = $customer['bank_account_bic'];
    $updatePostData['kontonr'] = $customer['bank_account_number'];

    $updatePostData['einzug'] = empty($updatePostData['IBAN']) ? '' : '1';


    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'changecontract'
    );
    
    $url .= '&'.utf8_decode(http_build_query($updatePostData));

    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        //CURLOPT_URL             => $PurtelUrl1,
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $result = trim($result);
    $result = explode(';', $result);

    if ('+OK' !== $result[0]) {
        var_dump('Failed to update sip', $result, $customerObject->id);

        continue;
    }

    echo "done (master): ".$customerObject->id."\n";



    // create sub-sip accounts
    foreach ($purtelSubAccounts as $purtelAccount) {
        echo "Purtel-Account-Id: ".$purtelAccount->id." / customer-id: ".$customerObject->id."\n";

        $postData = \Wisotel\Configuration\Configuration::get('purtelAccountValues');

        $postData['skip_timeout'] = 1;

        $postData['anrede'] = $customer['title'] == 'Herr' ? '1' : '2';
        $postData['vorname'] = $customer['firstname'];
        $postData['nachname'] = $customer['lastname'];
        $postData['adresse'] = $customer['street'];

        $houseNrMatch = [];

        preg_match('/(\d+)(.*)/', $customer['streetno'], $houseNrMatch);

        $postData['haus_nr'] = '';

        if (isset($houseNrMatch[1])) {
            $postData['haus_nr'] = $houseNrMatch[1];
        }
        if (isset($houseNrMatch[2])) {
            $postData['zusatz'] = $houseNrMatch[2];
        }

        $postData['plz'] = $customer['zipcode'];
        $postData['ort'] = $customer['city'];
        $postData['version'] = $customer['version'];
        $postData['purtel_use_version'] = $customer['purtel_use_version'];

        $postData['kundennummer_extern'] = $customer['clientid'];

        if ($customer['purtel_use_version'] == 1 && $customer['version'] > 0) {
            $postData['kundennummer_extern'] += '_'.$customer['version'];
        }

        $postData['geburtsdatum'] = $customer['birthday'];

        if ($postData['geburtsdatum'] != '') {
           $birthday = explode('.', $postData['geburtsdatum']);
           $postData['geburtsdatum'] = $birthday[2].$birthday[1].$birthday[0];
        }

        $postData['email'] = $postData['anrede'] == '' ? $customer['emailaddress'] : $customer['bank_account_emailaddress'];

        $postData['ortsnetz'] = $network['area_code'];

        $postData['ip_eingeschraenkt'] = 2;


        // create sip

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($purtelSuperUsername),
            urlencode($purtelSuperPassword),
            'createcontract'
        );

        $url .= '&'.utf8_decode(http_build_query($postData));


        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            //CURLOPT_URL             => $PurtelUrl1,
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $result = trim($result);
        $result = explode(';', $result);

        if ('+OK' !== $result[0]) {
            var_dump('Failed to create sip', $result, $userId);

            continue;
        }

        // set username and password to account

        $username = str_replace('benutzername=', '', $result[2]);
        $password = str_replace('passwort=', '', $result[3]);

        $db->query("UPDATE `purtel_account` 
            SET `purtel_login` = '".$username."', `purtel_password` = '".$password."' 
            WHERE `id` = ".$purtelAccount->id);

        if (!empty($db->error)) {
             var_dump('Failed to set password to master', $result, $userId);

            continue;
        }

        $purtelAccount->purtelLogin = $username;
        $purtelAccount->purtelPassword = $password;

        // update account

        $updatePostData = $postData;

        unset($updatePostData['skip_timeout']);

        $updatePostData['konto_von'] = $purtelMasterAccount->purtelLogin;

        $updatePostData['anschluss'] = $username;

        $updatePostData['firma'] = $customer['company'];

        $updatePostData['email'] = $customer['bank_account_emailaddress'];

        if ($updatePostData['email'] == '') {
            $updatePostData['email'] = $customer['emailaddress'];
        }

        $updatePostData['telefon'] = $customer['phoneareacode'].$customer['phonenumber'];
        
        $updatePostData['mobile'] = $customer['mobilephone'];

        $updatePostData['inhaber'] = $customer['bank_account_holder_lastname'];
        $updatePostData['inhaber_adresse'] = $customer['street'].' '.$customer['streetno'];

        $updatePostData['inhaber_plz'] = $customer['zipcode'];
        $updatePostData['inhaber_ort'] = $customer['city'];

        $updatePostData['bank'] = $customer['bank_account_bankname'];

        $updatePostData['BIC'] = $customer['bank_account_bic_new'];

        $updatePostData['IBAN'] = $customer['bank_account_iban'];

        $updatePostData['blz'] = $customer['bank_account_bic'];
        $updatePostData['kontonr'] = $customer['bank_account_number'];

        $updatePostData['einzug'] = empty($updatePostData['IBAN']) ? '' : '1';


        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($purtelSuperUsername),
            urlencode($purtelSuperPassword),
            'changecontract'
        );
        
        $url .= '&'.utf8_decode(http_build_query($updatePostData));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            //CURLOPT_URL             => $PurtelUrl1,
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $result = trim($result);
        $result = explode(';', $result);

        if ('+OK' !== $result[0]) {
            var_dump('Failed to update sip', $result, $userId);

            continue;
        }

        echo "done (sub): account-id: ".$purtelAccount->id."\n";
    }
}
