<?php

/**
 * Import customers from dimari
 * 
 * Step 5 = create radius
 * Step 6 = create acs
 * 
 * (Step 8 = rufnummern zuweisen)
 * 
 * Important Notices:
 * 
 *  - Customers without phonenumber will be imported without any purtel-accounts!
 * 
 * @todo
 *  - the global variable "$mapping" can be removed because it doesn't realy makes sense to use it
 * 
 * Manche Kunden haben fälschlicherweiße mehrfach die gleiche Rufnummer als subscriber_id.
 * Das muss noch geprüft werden!
 */

require __DIR__.'/../CsvReader.php';
require __DIR__.'/../Customer.php';
require __DIR__.'/../Product.php';
require __DIR__.'/../CustomerProduct.php';
require __DIR__.'/../PurtelAccount.php';
require __DIR__.'/../Location.php';
require __DIR__.'/../Mapper.php';
require __DIR__.'/../Exception/DoNoImportCustomerException.php';
require __DIR__.'/../Exception/CustomerAlreadyExistsInDatabaseException.php';
require __DIR__.'/../../../_conf/database.inc';

// import customers into this network
define('NETWORK_ID', 35);
define('NETZ', 50000);
define('IP_RANGE', 4);
// import customers into this year
define('CLIENT_ID_YEAR', 17);

// if IS_SIMULATION is true no data will be written to database
define('IS_SIMULATION', true);

// if true, only customers with certain statues will be imported
define('IGNORE_STATUS', false);

// if true, no matching of phonenumbers with telefonica-csv is done and any number gets imported
define('IGNORE_MATCH_IN_TELEFONICA', false);

// load telefonica file
$telefonicaCsv = new CsvReader('/tmp/telefonica.csv');
// load dimari export file
//$dimariCsv = new CsvReader('/tmp/kundenstamm_cluster_1_26_27_34.csv');
$dimariCsv = new CsvReader('/tmp/Restkunden.csv');
//$dimariCsv = new CsvReader('/tmp/CL_38_offene_Portierungen.csv');
// load extension for dimari file
// holds addition phonenumbers and statuses to customers
/*$dimariCsv->extend(
    new CsvReader('/tmp/Cluster-1-26-27-34_rufnummern-nicht-in-telefonica-gefunden.csv'),
    'Dimari Kundenummer',
    'KUNDENNUMMER',
    'not-in-telefonica-but-also-to-import'
);
$dimariCsv->extend(
    new CsvReader('/tmp/Aktualisierung-Cluster-1-26-27-34_rufnummern-nicht-in-telefonica-gefunden-3.csv'),
    'Dimari Kundenummer',
    'KUNDENNUMMER',
    'not-in-telefonica-but-also-to-import-2'
);*/
$dimariCsv->extend(
    new CsvReader('/tmp/Restkunden-rn-nicht-in-telefonica-trotzdem-portieren.csv'),
    'Dimari-Kundenummer',
    'KUNDENNUMMER',
    'not-in-telefonica-but-also-to-import'
);

class Notifications
{
    public static $globallyOn = false;
    public static $globallyOff = false;
    public static $notFoundInTelefonica = true;
    public static $unableToGetAreaCode = true;
    public static $connectionActivationDate = true;
    public static $contractId = true;
    public static $wisocontractCancelDate = true;
    public static $wisocontractSwitchoffFinish = true;
    public static $statiPortConfirmDate = false;
    public static $contractVersion = true;
    public static $noCwmp = false;
    public static $lineIdentifier = true;
    public static $pppoePin = true;
    public static $noPppoePin = false;
    public static $invalidPppoePin = true;
    public static $canNotExtractPasswordFromPppoePin = true;
    public static $commentMandatsreferenz = true;
    public static $commentSecondLeitung = false;
    public static $setExternalCustomerId = true;
    public static $doNoImportCustomerException = true;
    public static $invalidPortingDate = true;

    public static function isActive($element)
    {
        if (self::$globallyOn) {
            return true;
        }

        if (self::$globallyOff) {
            return false;
        }

        return self::${$element};
    }
}

// map some internal-names to csv-headlines
// use as 'internalName' => 'csv-headline',
$mapping = [
    'customerId' => 'KUNDENNUMMER',
    'product.identifier' => 'PRODUCT_ID',
    'product.name' => 'PRODUKTBESCHREIBUNG',
    'product.category' => 'KATEGORIE',
    'product.priceNet' => 'Preis netto',
    'product.activationDate' => 'Prod.Aktivierung',
    'product.deactivationDate' => 'Prod.Deaktivierung',
];

// to simplify things, group the csv entries by customer-id
$dataGroupedByCustomerId = [];
$customerIdColumn = $dimariCsv->headlineReversed[$mapping['customerId']];

foreach ($dimariCsv->data as $key => $line) {
    $customerId = trim($line[$customerIdColumn]);

    if (!isset($dataGroupedByCustomerId[$customerId])) {
        $dataGroupedByCustomerId[$customerId] = [];
    }

    $dataGroupedByCustomerId[$customerId][$key] = & $dimariCsv->data[$key]; // use reference 
}

// all products that customers have
$knownProducts = [];
// all locations that customers have
$knownLocations = [];









/**
 * Get value for $columnHeadlineName.
 * Verifies that all lines have same value for $columnHeadlineName-colum
 * 
 * @param array $lines
 * @param CsvReader $csv
 * @param string $columnHeadlineName
 * 
 * @throws \UnexpectedValueException
 * 
 * @return string
 */
function simpleDataMapping(array $lines, CsvReader $csv, $columnHeadlineName) {
    $value = null;

    // check if all lines have same value
    foreach ($lines as $line) {
        $value_ = trim($line[$csv->headlineReversed[$columnHeadlineName]]);

        if (null === $value) {
            $value = $value_;
        } elseif ($value !== $value_) {
            throw new \UnexpectedValueException(sprintf('customer has different "%s" (%s) and (%s)',
                $columnHeadlineName,
                $value,
                $value_
            ));
        }
    }

    return $value;
};

/**
 * Get not empty value for $columnHeadlineName.
 * 
 * @param array $lines
 * @param CsvReader $csv
 * @param string $columnHeadlineName
 * 
 * @return array
 */
function getNotEmptyValues(array $lines, CsvReader $csv, $columnHeadlineName) {
    $values = [];

    // check if all lines have same value
    foreach ($lines as $line) {
        $value = trim($line[$csv->headlineReversed[$columnHeadlineName]]);

        if (!empty($value)) {
            $values[] = $value;
        }
    }

    return $values;
};

/**
 * Get not empty value for $columnHeadlineName.
 * If values are not empty they get compared
 * 
 * @param array $lines
 * @param CsvReader $csv
 * @param string $columnHeadlineName
 * 
 * @throws \UnexpectedValueException
 * 
 * @return string
 */
function getComparedNotEmptyValue(array $lines, CsvReader $csv, $columnHeadlineName) {
    $value = null;

    foreach ($lines as $line) {
        $value_ = trim($line[$csv->headlineReversed[$columnHeadlineName]]);

        if (empty($value_)) {
            continue;
        }

        if (null === $value) {
            $value = $value_;

            continue;
        }

        if ($value !== $value_) {
            throw new \UnexpectedValueException(sprintf('customer has different "%s" (%s) and (%s)',
                $columnHeadlineName,
                $value,
                $value_
            ));
        }
    }

    return $value;
};

// set mapper methods
// this is used to filter/verify data for object
// each mapper method must return an array which is then used as arguments for call_uer_func_array()
$toCustomerMapper = new Mapper();

/**
 * Mapper for customer.products - all products, main, cross-selling
 */
$toCustomerMapper->toProducts(function (array $lines) use (&$mapping, $dimariCsv, &$knownProducts, $db) {
    $productColumn = $dimariCsv->headlineReversed[$mapping['product.identifier']];
    $customerProducts = [];

    foreach ($lines as $key => $line) {
        if (count($line) !== count($dimariCsv->headline)) {
            var_dump($key, $line);
        }
        $productId = trim($line[$productColumn]);

        if (empty($productId)) {
            continue;
        }

        $productPriceNetColumn = $dimariCsv->headlineReversed[$mapping['product.priceNet']];

        $priceNet = trim($line[$productPriceNetColumn]);
        $priceNet = empty($priceNet) ? null : (float) str_replace(",", ".", $priceNet);

        if (isset($knownProducts[$productId])) {
            // already know this product
            $product = $knownProducts[$productId];
        } else {
            // see if we imported this product some time before
            $productQuery = $db->query("SELECT * FROM `products` WHERE `identifier` = '".$productId."'");

            if ($productQuery->num_rows > 0) {
                // already have product in database
                $product = Product::createFromArray($productQuery->fetch_assoc());
                $product->isNew = false;
            } else {
                // is new product
                $productNameColumn = $dimariCsv->headlineReversed[$mapping['product.name']];
                $productCategoryColumn = $dimariCsv->headlineReversed[$mapping['product.category']];

                $product = new Product();
                $product->identifier = $productId;
                $product->name = trim($line[$productNameColumn]);

                $itemCategory = trim($line[$productCategoryColumn]);
                $itemCategory = empty($itemCategory) ? null : $itemCategory;

                $product->itemCategory = $itemCategory;

                $product->defaultPriceNet = $priceNet;
            }

            $knownProducts[$productId] = $product; // add to known products
        }

        $productActivationDateColumn = $dimariCsv->headlineReversed[$mapping['product.activationDate']];
        $productDeactivationDateColumn = $dimariCsv->headlineReversed[$mapping['product.deactivationDate']];

        $activationDate = trim($line[$productActivationDateColumn]);

        try {
            $activationDate = empty($activationDate) ? null : new \DateTime($activationDate);
        } catch (\Exception $exception) {
            $activationDate = null;
        }

        $deactivationDate = trim($line[$productDeactivationDateColumn]);

        try {
            $deactivationDate = empty($deactivationDate) ? null : new \DateTime($deactivationDate);
        } catch (\Exception $exception) {
            $deactivationDate = null;
        }

        $customerProduct = new customerProduct();
        $customerProduct->product = $product;
        $customerProduct->priceNet = $priceNet;
        $customerProduct->activationDate = $activationDate;
        $customerProduct->deactivationDate = $deactivationDate;

        $customerProducts[] = $customerProduct;
    }

    return [$customerProducts];
});

/**
 * Mapper for customer.purtelAccounts
 */
$toCustomerMapper->toPurtelAccounts(function (array $lines) use (&$mapping, $dimariCsv, $telefonicaCsv) {
    //$ignoreCustomerIfNoNumberMatchedInTelefonica = null;
    $customerPurtelAccounts = [];

    foreach ($lines as $line) {
        $customerIdColumn = $dimariCsv->headlineReversed[$mapping['customerId']];
        $customerId = trim($line[$customerIdColumn]);

        $subscriberId = trim($line[$dimariCsv->headlineReversed['SUBSCRIBER_ID']]);

        // replace +49 with 0
        $subscriberId = preg_replace('/^\+49/', '0', $subscriberId);

        $telefonicaRn = null;

        if (!empty($subscriberId)) {
            $telefonicaRn = $subscriberId;
        } else {
            // no $subscriberId -> no need for a purtel-account
            continue;
        }

        $purtelAccount = new PurtelAccount();

        $portingDate = trim($line[$dimariCsv->headlineReversed['PORTIERUNGSTERMIN']]);

        if (!empty($portingDate)) {
            try {
                $purtelAccount->portingDate = new \DateTime($portingDate);
            } catch (\Exception $exception) {
                if (Notifications::isActive('invalidPortingDate')) {
                    trigger_error(sprintf('invalid "PORTIERUNGSTERMIN" (%s) for customer %s',
                        $portingDate,
                        $customerId
                    ), E_USER_NOTICE);
                }
            }
        }

        $telefonicaRnOrginal = $telefonicaRn;
        $telefonicaRn = preg_replace('/[^0-9]/', '', $telefonicaRn);
        $telefonicaRn = preg_replace('/^0/', '', $telefonicaRn);

        if (IGNORE_MATCH_IN_TELEFONICA) {
            // comparison with telefonica file is disabled, therefore use this number anyway
            $purtelAccount->nummer = preg_replace('/[^0-9]/', '', $telefonicaRnOrginal);
        } else {
            // try to find this number in telefonica csv
            $matchedLinesInTelefonicaCsv = $telefonicaCsv->findLines([
                'rn' => $telefonicaRn,
            ]);

            if (!empty($matchedLinesInTelefonicaCsv)) { // has exactly 1 item
                $purtelAccount->nummer = preg_replace('/[^0-9]/', '', $telefonicaRnOrginal);
            } else {
                if (null !== $purtelAccount->portingDate && $purtelAccount->portingDate > new \DateTime('now')) {
                    // porting date is in future, therefore this phonenumber can not be found in telefonica-csv
                    $purtelAccount->nummer = preg_replace('/[^0-9]/', '', $telefonicaRnOrginal);
                } else {
                    foreach (['not-in-telefonica-but-also-to-import', 'not-in-telefonica-but-also-to-import-2'] as $extensionNamespace) {
                        if (isset($line[$extensionNamespace])) {
                            $extensionLines = $line[$extensionNamespace]->findLines([
                                'Rufnummer' => $telefonicaRnOrginal,
                            ]);

                            if (count($extensionLines) > 0) {
                                if (count($extensionLines) > 1) {
                                    // warning must only be one - only last will be used!
                                    trigger_error(sprintf('extension file (%s) has multiple entries for phonenumber (%s) to customer (%s) - hast found will be used',
                                        $extensionNamespace,
                                        $telefonicaRnOrginal,
                                        $customerId
                                    ), E_USER_NOTICE);
                                }

                                // if the below (part with 'zu tun') is not used, accept all numbers when found
                                $purtelAccount->nummer = preg_replace('/[^0-9]/', '', $telefonicaRnOrginal);

                                /*
                                kept, maybe it's needed again

                                $extensionLine = end($extensionLines);
                                $todo = trim($extensionLine[$line[$extensionNamespace]->getCsvReader()->headlineReversed['zu tun']]);

                                if ('komplett anlegen und aktivieren' === $todo) {
                                    // keep this number
                                    $purtelAccount->nummer = preg_replace('/[^0-9]/', '', $telefonicaRnOrginal);

                                    break; // one extension sayed this number is needed - we simply trust this one
                                }*/
                            }
                        }
                    }

                    if (empty($purtelAccount->nummer)) {
                        // still no number found

                        if (Notifications::isActive('notFoundInTelefonica')) {
                            trigger_error(sprintf('number not imported! customers (%s) phonenumber (%s) not found in telefonica and "PORTIERUNGSTERMIN" (%s) not in future and nothing relevant found in extension files - searched by %s',
                                $customerId,
                                $telefonicaRnOrginal,
                                $portingDate,
                                $telefonicaRn
                            ), E_USER_NOTICE);
                        }

                        // do not import purtel account without phonenumber
                        continue;
                    }
                }
            }
        }

        $areaCode = explode('-', $telefonicaRnOrginal);

        if (count($areaCode) === 2) {
            $purtelAccount->areaCode = preg_replace('/[^0-9]/', '', $areaCode[0]);
        } else {
            $areaCode = explode(' ', $telefonicaRnOrginal);

            if (count($areaCode) === 3) {
                $purtelAccount->areaCode = $areaCode[0].$areaCode[1];
            } else {
                if (Notifications::isActive('unableToGetAreaCode')) {
                    trigger_error(sprintf('unable to get area-code from customers (%s) phonenumber (%s)',
                        $customerId,
                        $telefonicaRnOrginal
                    ), E_USER_NOTICE);
                }
            }

            // keep going - its not too much of a problem without an area-code
        }

        // use a simple data mapping to add some other informations
        $simpleDataMapping = [
            'Auskunft' => 'phoneBookInformation',
            'Digitale Medien' => 'phoneBookDigitalMedia',
            'Inverssuche' => 'phoneBookInverseSearch',
            'EVN' => 'itemizedBill',
            'anonymisieren' => 'itemizedBillAnonymized',
            'reduziert' => 'itemizedBillShorted',
        ];

        foreach ($simpleDataMapping as $dataKey => $objectParameter) {
            $value = trim($line[$dimariCsv->headlineReversed[$dataKey]]);

            if (!empty($value) && '0' !== $value) {
                $purtelAccount->{$objectParameter} = $value;
            }
        }

        // set first account as "master"
        if (empty($customerPurtelAccounts)) {
            $purtelAccount->master = 1;
        }

        $customerPurtelAccounts[] = $purtelAccount;
    }

    /*if (null !== $ignoreCustomerIfNoNumberMatchedInTelefonica && empty($customerPurtelAccounts)) {
        throw new DoNoImportCustomerException(sprintf('no number found in telefonica and the extension file (%s) sayes "ignorieren"',
            $ignoreCustomerIfNoNumberMatchedInTelefonica
        ));
    }*/

    return [$customerPurtelAccounts];
});

/**
 * Mapper for customer.locationId
 */
$toCustomerMapper->toLocation(function (array $lines) use (&$mapping, $dimariCsv, &$knownLocations, $db) {
    $locationName = null;

    // check if all lines have same location
    foreach ($lines as $line) {
        $customerIdColumn = $dimariCsv->headlineReversed[$mapping['customerId']];
        $customerId = trim($line[$customerIdColumn]);

        $locationName_ = trim($line[$dimariCsv->headlineReversed['ORTSTEIL']]);

        if (null === $locationName) {
            $locationName = $locationName_;
        } elseif ($locationName !== $locationName_) {
            throw new \UnexpectedValueException(sprintf('customer (%s) has different locations (%s) and (%s)',
                $customerId,
                $locationName,
                $locationName_
            ));
        }
    }

    if (empty($locationName)) { // empty gets not only null, but also ""
        // no location found
        throw new \UnexpectedValueException(sprintf('no location found for customer (%s)', $customerId));
    }

    if (isset($knownLocations[$locationName])) {
        // already know this location
        return [ $knownLocations[$locationName] ];
    }

    // try to find location in database
    $query = $db->query("SELECT * FROM `locations` WHERE `name` = '".$locationName."'");

    if ($query->num_rows > 0) {
        // already have location in database?
        $location = Location::createFromArray($query->fetch_assoc());
        $location->isNew = false;

        $knownLocations[$locationName] = $location;

        return [ $location ];
    }

    // it is a totaly new location
    $location = new Location();
    $location->networkId = NETWORK_ID;
    $location->name = $locationName;
    $location->kvzPrefix = $locationName;
    $location->cidSuffixStart = 1;
    $location->cidSuffixEnd = 9999;

    $knownLocations[$locationName] = $location;

    return [ $location ];
});

/**
 * Mapper for customer.customer_id
 */
$toCustomerMapper->toExternalCustomerId(function (array $lines) use (&$mapping, $dimariCsv, $db) {
    $line = current($lines);
    $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

    // see if a customer with same customer_id already exists in database
    $findCustomerInDbQuery = $db->query("SELECT `id`, `clientid` FROM `customers` WHERE `customer_id` = ".$customerId);

    if ($findCustomerInDbQuery->num_rows > 0) {
        // a customer with same customer_id already exists in database!
        $customerDataFromDb = $findCustomerInDbQuery->fetch_assoc();

        throw new CustomerAlreadyExistsInDatabaseException(sprintf('Data for KUNDENNUMMER = %s already exists in database - %s (%s)',
            $customerId,
            $customerDataFromDb['clientid'],
            $customerDataFromDb['id']
        ));
    }

    return [ $customerId ];
});

/**
 * Mapper for customer.connection_activation_date
 */
$toCustomerMapper->toConnectionActivationDate(function (array $lines) use ($dimariCsv) {
    try {
        $connectionActivationDate = simpleDataMapping($lines, $dimariCsv, 'VERTRAGSAKTIVIERUNG');
    } catch (\UnexpectedValueException $exception) {
        $line = current($lines);
        $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

        if (Notifications::isActive('connectionActivationDate')) {
            trigger_error(sprintf('%s - customer (%s) -- %s',
                $exception->getMessage(),
                $customerId,
                'connection_activation_date'
            ), E_USER_NOTICE);
        }
    
        $connectionActivationDate = trim($line[$dimariCsv->headlineReversed['VERTRAGSAKTIVIERUNG']]);
    }

    if (empty($connectionActivationDate)) {
        return [ null ];
    }

    return [
        new \DateTime($connectionActivationDate),
    ];
});

/**
 * Mapper for customer.contract_id
 */
$toCustomerMapper->toContractId(function (array $lines) use ($dimariCsv) {
    try {
        $contractId = simpleDataMapping($lines, $dimariCsv, 'VERTRAGSNUMMER');
    } catch (\UnexpectedValueException $exception) {
        $line = current($lines);
        $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

        if (Notifications::isActive('contractId')) {
            trigger_error(sprintf('%s - customer (%s) -- %s',
                $exception->getMessage(),
                $customerId,
                'contract_id'
            ), E_USER_NOTICE);
        }
    
        $contractId = trim($line[$dimariCsv->headlineReversed['VERTRAGSNUMMER']]);
    }

    return [
        $contractId,
    ];
});

/**
 * Mapper for customer.bank_account_holder_lastname
 */
$toCustomerMapper->toBankAccountHolderLastname(function (array $lines) use ($dimariCsv) {
    return [
        simpleDataMapping($lines, $dimariCsv, 'KTOINHABER'),
    ];
});

/**
 * Mapper for customer.bank_account_bic
 */
$toCustomerMapper->toBankAccountBic(function (array $lines) use ($dimariCsv) {
    return [
        simpleDataMapping($lines, $dimariCsv, 'BIC'),
    ];
});

/**
 * Mapper for customer.bank_account_iban
 */
$toCustomerMapper->toBankAccountIban(function (array $lines) use ($dimariCsv) {
    return [
        simpleDataMapping($lines, $dimariCsv, 'IBAN'),
    ];
});

/**
 * Mapper for customer.bank_account_emailaddress
 */
$toCustomerMapper->toBankAccountEmailaddress(function (array $lines) use ($dimariCsv) {
    return [
        simpleDataMapping($lines, $dimariCsv, 'Emailadr.'),
    ];
});

/**
 * Mapper for customer.wisocontract_cancel_date
 */
$toCustomerMapper->toWisocontractCancelDate(function (array $lines) use ($dimariCsv) {
    try {
        $wisocontractCancelDate = simpleDataMapping($lines, $dimariCsv, 'VERTRAGSDEAKTIVIERUNG');
    } catch (\UnexpectedValueException $exception) {
        $line = current($lines);
        $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

        if (Notifications::isActive('wisocontractCancelDate')) {
            trigger_error(sprintf('%s - customer (%s) -- %s',
                $exception->getMessage(),
                $customerId,
                'wisocontract_cancel_date'
            ), E_USER_NOTICE);
        }
    
        $wisocontractCancelDate = trim($line[$dimariCsv->headlineReversed['VERTRAGSDEAKTIVIERUNG']]);
    }

    if (empty($wisocontractCancelDate)) {
        return [ null ];
    }

    return [
        new \DateTime($wisocontractCancelDate),
    ];
});

/**
 * Mapper for customer.wisocontract_switchoff_finish
 */
$toCustomerMapper->toWisocontractSwitchoffFinish(function (array $lines) use ($dimariCsv) {
    try {
        $wisocontractSwitchoffFinish = simpleDataMapping($lines, $dimariCsv, 'VERTRAGSDEAKTIVIERUNG');
    } catch (\UnexpectedValueException $exception) {
        $line = current($lines);
        $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

        if (Notifications::isActive('wisocontractSwitchoffFinish')) {
            trigger_error(sprintf('%s - customer (%s) -- %s',
                $exception->getMessage(),
                $customerId,
                'wisocontract_switchoff_finish'
            ), E_USER_NOTICE);
        }
    
        $wisocontractSwitchoffFinish = trim($line[$dimariCsv->headlineReversed['VERTRAGSDEAKTIVIERUNG']]);
    }

    if (empty($wisocontractSwitchoffFinish)) {
        return [ null ];
    }

    return [
        new \DateTime($wisocontractSwitchoffFinish),
    ];
});

/**
 * Mapper for customer.connection_street
 */
$toCustomerMapper->toConnectionStreet(function (array $lines) use ($dimariCsv) {
    return [
        simpleDataMapping($lines, $dimariCsv, 'Straße'),
    ];
});

/**
 * Mapper for customer.connection_streetno
 */
$toCustomerMapper->toConnectionStreetNo(function (array $lines) use ($dimariCsv) {
    return [
        simpleDataMapping($lines, $dimariCsv, 'HAUSNR') . simpleDataMapping($lines, $dimariCsv, 'Hausnr.zus.'),
    ];
});

/**
 * Mapper for customer.connection_city
 */
$toCustomerMapper->toConnectionCity(function (array $lines) use ($dimariCsv) {
    return [
        simpleDataMapping($lines, $dimariCsv, 'ORT'),
    ];
});

/**
 * Mapper for customer.connection_zipcode
 */
$toCustomerMapper->toConnectionZipcode(function (array $lines) use ($dimariCsv) {
    return [
        simpleDataMapping($lines, $dimariCsv, 'PLZ'),
    ];
});

/**
 * Mapper for customer.title
 */
$toCustomerMapper->toTitle(function (array $lines) use ($dimariCsv) {
    return [
        simpleDataMapping($lines, $dimariCsv, 'ANREDE'),
    ];
});

/**
 * Mapper for customer.firstname
 */
$toCustomerMapper->toFirstname(function (array $lines) use ($dimariCsv) {
    return [
        simpleDataMapping($lines, $dimariCsv, 'VORNAME'),
    ];
});

/**
 * Mapper for customer.lastname
 */
$toCustomerMapper->toLastname(function (array $lines) use ($dimariCsv) {
    return [
        simpleDataMapping($lines, $dimariCsv, 'NAME'),
    ];
});

/**
 * Mapper for customer.street
 */
$toCustomerMapper->toStreet(function (array $lines) use ($dimariCsv) {
    return [
        simpleDataMapping($lines, $dimariCsv, 'Straße'),
    ];
});

/**
 * Mapper for customer.streetno
 */
$toCustomerMapper->toStreetno(function (array $lines) use ($dimariCsv) {
    return [
        simpleDataMapping($lines, $dimariCsv, 'HAUSNR') . simpleDataMapping($lines, $dimariCsv, 'Hausnr.zus.'),
    ];
});

/**
 * Mapper for customer.city
 */
$toCustomerMapper->toCity(function (array $lines) use ($dimariCsv) {
    return [
        simpleDataMapping($lines, $dimariCsv, 'ORT'),
    ];
});

/**
 * Mapper for customer.zipcode
 */
$toCustomerMapper->toZipcode(function (array $lines) use ($dimariCsv) {
    return [
        simpleDataMapping($lines, $dimariCsv, 'PLZ'),
    ];
});

/**
 * Mapper for customer.birthday
 */
$toCustomerMapper->toBirthday(function (array $lines) use ($dimariCsv) {
    return [
        simpleDataMapping($lines, $dimariCsv, 'GEBURTSDATUM'),
    ];
});

/**
 * Mapper for customer.emailaddress
 */
$toCustomerMapper->toEmailaddress(function (array $lines) use ($dimariCsv) {
    return [
        simpleDataMapping($lines, $dimariCsv, 'Emailadr.'),
    ];
});

/**
 * Mapper for customer.stati_port_confirm_date
 */
$toCustomerMapper->toStatiPortConfirmDate(function (array $lines) use ($dimariCsv) {
    try {
        $statiPortConfirmDate = simpleDataMapping($lines, $dimariCsv, 'PORTIERUNGSTERMIN');
    } catch (\UnexpectedValueException $exception) {
        $line = current($lines);
        $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

        if (Notifications::isActive('statiPortConfirmDate')) {
            trigger_error(sprintf('%s - customer (%s) -- %s',
                $exception->getMessage(),
                $customerId,
                'contract_version'
            ), E_USER_NOTICE);
        }
    
        $statiPortConfirmDate = trim($line[$dimariCsv->headlineReversed['PORTIERUNGSTERMIN']]);
    }

    if (empty($statiPortConfirmDate)) {
        return [ null ];
    }

    return [
        new \DateTime($statiPortConfirmDate),
    ];
});

/**
 * Mapper for customer.company
 */
$toCustomerMapper->toCompany(function (array $lines) use ($dimariCsv) {
    return [
        simpleDataMapping($lines, $dimariCsv, 'FIRMA'),
    ];
});

/**
 * Mapper for customer.contract_version
 */
$toCustomerMapper->toContractVersion(function (array $lines) use ($dimariCsv) {
    try {
        $contractVersion = simpleDataMapping($lines, $dimariCsv, 'VERTRAGSUNTERSCHRIFT');
    } catch (\UnexpectedValueException $exception) {
        $line = current($lines);
        $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

        if (Notifications::isActive('contractVersion')) {
            trigger_error(sprintf('%s - customer (%s) -- %s',
                $exception->getMessage(),
                $customerId,
                'contract_version'
            ), E_USER_NOTICE);
        }
    
        $contractVersion = trim($line[$dimariCsv->headlineReversed['VERTRAGSUNTERSCHRIFT']]);
    }

    return [
        $contractVersion,
    ];
});

/**
 * Mapper for customer.no_eze
 */
$toCustomerMapper->toNoEze(function (array $lines) use ($dimariCsv) {
    return [
        '0' == simpleDataMapping($lines, $dimariCsv, 'Lastschriftzahler') ? 'no' : '',
    ];
});

/**
 * Mapper for customer.credit_rating_ok
 */
$toCustomerMapper->toCreditRatingOk(function (array $lines) use ($dimariCsv) {
    $creditRatingOk = null;

    if ('1' == simpleDataMapping($lines, $dimariCsv, 'Lastschriftzahler')) {
        $creditRatingOk = 'successful';
    }

    return [
        $creditRatingOk,
    ];
});

/**
 * Mapper for customer.credit_rating_date
 */
$toCustomerMapper->toCreditRatingDate(function (array $lines) use ($dimariCsv) {
    $creditRatingOk = null;

    if ('1' == simpleDataMapping($lines, $dimariCsv, 'Lastschriftzahler')) {
        $creditRatingOk = new \DateTime('01.01.2000');
    }

    return [
        $creditRatingOk,
    ];
});

/**
 * Mapper for customer.clienttype
 */
$toCustomerMapper->toClienttype(function (array $lines) use ($dimariCsv) {
    $clienttype = simpleDataMapping($lines, $dimariCsv, 'KUNDENGRUPPE');

    switch ($clienttype) {
        case 'Geschäftskunde':
            return [ 'commercial' ];

        case 'Privatkunde':
        case 'keine':
            return [ 'non_commercial' ];
    }

    $line = current($lines);
    $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

    throw new \UnexpectedValueException(sprintf('unexpected value for "KUNDENGRUPPE" (%s) - customer (%s)',
        $clienttype,
        $customerId
    ));
});

/**
 * Mapper for customer.purtel_edit_done
 */
$toCustomerMapper->toPurtelEditDone(function (array $lines) use ($dimariCsv) {
    $status = simpleDataMapping($lines, $dimariCsv, 'STATUS');

    if ('Aktiv' === $status || 'Teilbereitstellung' === $status) {
        return [ 'yes' ];
    }

    return [ null ];
});

/**
 * Mapper for customer.wisocontractCanceledDate
 */
$toCustomerMapper->toWisocontractCanceledDate(function (array $lines) use ($dimariCsv) {
    $status = simpleDataMapping($lines, $dimariCsv, 'STATUS');

    if ('Gekündigt' === $status) {
        return [ new \DateTime('01.01.2000') ]; // maybe use "VERTRAGSDEAKTIVIERUNG" date if given (could be in the future)
    }

    return [ null ];
});


/**
 * Mapper for customer.mac_address
 */
$toCustomerMapper->toMacAddress(function (array $lines) use ($dimariCsv) {
    $cwmp = null;

    foreach ($lines as $line) {
        $cwmp_ = trim($line[$dimariCsv->headlineReversed['Fritz Box Seriennr']]);

        if (empty($cwmp_)) {
            continue;
        }

        $matchMac = null;

        preg_match('/([0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2})|([0-9a-f]{12})/i', $cwmp_, $matchMac);

        if (isset($matchMac[0])) {
            $cwmp = preg_replace('/[^0-9a-f]/i', '', $matchMac[0]);

            break; // first match is ok
        }
    }

    if (null === $cwmp) {
        $line = current($lines);
        $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

        if (Notifications::isActive('noCwmp')) {
            trigger_error(sprintf('customer (%s) has no cwmp',
                $customerId
            ), E_USER_NOTICE);
        }
    }

    return [ $cwmp ];
});

/**
 * Mapper for customer.line_identifier
 */
$toCustomerMapper->toLineIdentifier(function (array $lines) use ($dimariCsv) {
    $lineIdentifier = null;

    foreach ($lines as $line) {
        $lineIdentifier_ = trim($line[$dimariCsv->headlineReversed['Line ID']]);

        if (empty($lineIdentifier_)) {
            continue;
        }

        if (null === $lineIdentifier) {
            $lineIdentifier = $lineIdentifier_;

            continue;
        }

        if ($lineIdentifier !== $lineIdentifier_) {
            $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

            if (Notifications::isActive('lineIdentifier')) {
                trigger_error(sprintf('customer (%s) has different "Line ID" (%s) (%s) - non imported',
                    $customerId,
                    $lineIdentifier,
                    $lineIdentifier_
                ), E_USER_NOTICE);
            }

            return [ null ];
        }
    }

    if (null === $lineIdentifier) {
        return [ null ];
    }

    return [ $lineIdentifier ];
});


/**
 * Mapper for customer.pppoe_pin
 */
$toCustomerMapper->toPppoePin(function (array $lines) use ($dimariCsv) {
    $pppoePin = null;

    foreach ($lines as $line) {
        $pppoePin_ = trim($line[$dimariCsv->headlineReversed['PIN']]);

        if (empty($pppoePin_)) {
            continue;
        }

        if (null === $pppoePin) {
            $pppoePin = $pppoePin_;

            continue;
        }

        if ($pppoePin !== $pppoePin_) {
            $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

            if (Notifications::isActive('pppoePin')) {
                trigger_error(sprintf('customer (%s) has different "PIN" (%s) (%s) - non imported',
                    $customerId,
                    $pppoePin,
                    $pppoePin_
                ), E_USER_NOTICE);
            }

            return [ null ];
        }
    }

    if (empty($pppoePin)) {
        $line = current($lines);
        $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

        if (Notifications::isActive('noPppoePin')) {
            trigger_error(sprintf('customer (%s) has no "PIN"',
                $customerId
            ), E_USER_NOTICE);
        }

        return [ null ];
    }

    if (1 !== preg_match('/^\d+$/', $pppoePin) || strlen($pppoePin) !== 15) {
        $line = current($lines);
        $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

        if (Notifications::isActive('invalidPppoePin')) {
            trigger_error(sprintf('customer (%s) has an invalid ppppoe-pin',
                $customerId
            ), E_USER_NOTICE);
        }
    }

    return [ $pppoePin ];
});

/**
 * Mapper for customer.password
 */
$toCustomerMapper->toPassword(function (array $lines) use ($dimariCsv) {
    $pppoePin = null;

    foreach ($lines as $line) {
        $pppoePin_ = trim($line[$dimariCsv->headlineReversed['PIN']]);

        if (empty($pppoePin_)) {
            continue;
        }

        if (null === $pppoePin) {
            $pppoePin = $pppoePin_;

            continue;
        }

        if ($pppoePin !== $pppoePin_) {
            $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

            if (Notifications::isActive('pppoePin')) {
                trigger_error(sprintf('customer (%s) has different "PIN" (%s) (%s) - non imported',
                    $customerId,
                    $pppoePin,
                    $pppoePin_
                ), E_USER_NOTICE);
            }

            return [ null ];
        }
    }

    if (empty($pppoePin)) {
        $line = current($lines);
        $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);
        
        if (Notifications::isActive('noPppoePin')) {
            trigger_error(sprintf('customer (%s) has no "PIN"',
                $customerId
            ), E_USER_NOTICE);
        }

        return [ null ];
    }

    if (1 !== preg_match('/^\d+$/', $pppoePin) || strlen($pppoePin) !== 15) {
        $line = current($lines);
        $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

        if (Notifications::isActive('canNotExtractPasswordFromPppoePin')) {
            trigger_error(sprintf('can not extract password from pppoe-pin for customer (%s) because ppppoe-pin is not valid',
                $customerId
            ), E_USER_NOTICE);
        }

        return [ null ];
    }

    return [ substr($pppoePin, -6) ];
});

/**
 * Mapper for customer.comment
 */
$toCustomerMapper->toComment(function (array $lines) use ($dimariCsv) {
    $comment = sprintf("ABRECHNUNGSGRUPPE: %s\nCOV_ID: %s\n",
        simpleDataMapping($lines, $dimariCsv, 'ABRECHNUNGSGRUPPE'),
        implode('; ', getNotEmptyValues($lines, $dimariCsv, 'COV_ID'))
    );

    try {
        $mandatsreferenz = getComparedNotEmptyValue($lines, $dimariCsv, 'Mandatsreferenz');

        if (!empty($mandatsreferenz)) {
            $comment .= "Mandatsreferenz: ".$mandatsreferenz."\n";
        }
    } catch (\UnexpectedValueException $exception) {
        $line = current($lines);
        $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

        if (Notifications::isActive('commentMandatsreferenz')) {
            trigger_error(sprintf('%s - customer (%s) -- %s',
                $exception->getMessage(),
                $customerId,
                'comment'
            ), E_USER_NOTICE);
        }
    }

    $webBenutzername = simpleDataMapping($lines, $dimariCsv, 'Web-Benutzername');

    if (!empty($webBenutzername) && 'keine' !== $webBenutzername) {
        $comment .= "Web-Benutzername: ".$webBenutzername."\n";
    }

    try {
        $secondLeitung = getComparedNotEmptyValue($lines, $dimariCsv, '2.Ltg');

        if (!empty($secondLeitung)) {
            $comment .= "2.Ltg: ".$secondLeitung."\n";
        }
    } catch (\UnexpectedValueException $exception) {
        $line = current($lines);
        $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

        if (Notifications::isActive('commentSecondLeitung')) {
            trigger_error(sprintf('%s - customer (%s) -- %s',
                $exception->getMessage(),
                $customerId,
                'comment'
            ), E_USER_NOTICE);
        }
    }

    $columns = [
        'TEL_RUFNR',
        'TEL_ANREDE',
        'TEL_FIRMA',
        'TEL_NAME',
        'TEL_VORNAME',
        'TEL_Straße',
        'TEL_HAUSNR',
        'TEL_Hausnr.zus.',
        'TEL_PLZ',
        'TEL_ORT',
        'INSTAL_ANREDE',
        'INSTAL_FIRMA',
        'INSTAL_NAME',
        'INSTAL_VORNAME',
        'INSTAL_Straße',
        'INSTAL_HAUSNR',
        'INSTAL_Hausnr.zus.',
        'INSTAL_PLZ',
        'INSTAL_ORT',
        'Land1',
        'Land2',
        'ACS',
        'CPRODUCTID',
    ];

    foreach ($columns as $column) {
        $values = getNotEmptyValues($lines, $dimariCsv, $column);

        if (!empty($values)) {
            $comment .= $column.": ".implode('; ', $values)."\n";
        }
    }

    return [ $comment ];
});

/**
 * Mapper for customer.paper_bill
 */
$toCustomerMapper->toPaperBill(function (array $lines) use ($dimariCsv) {
    $paperBill = simpleDataMapping($lines, $dimariCsv, 'Papierrg');

    if (!empty($paperBill) && '0' !== $paperBill) {
        return [ 'yes' ];
    }

    return [ null ];
});
























$customers = [];

// map customer csv lines to a single customer object
foreach ($dataGroupedByCustomerId as $customerId => $lines) {
    if (!IGNORE_STATUS) {
        // skip data because of status?
        try {
            $status = simpleDataMapping($lines, $dimariCsv, 'STATUS');
        } catch (\UnexpectedValueException $exception) {
            throw new \UnexpectedValueException(sprintf('%s - customer (%s)',
                $exception->getMessage(),
                $customerId
            ));
        }

        switch (strtolower($status)) {
            case 'teilbereitstellung':
            case 'aktiv':
                break;

            case 'gekündigt':
                try {
                    $terminationDate = simpleDataMapping($lines, $dimariCsv, 'VERTRAGSDEAKTIVIERUNG');
                } catch (\UnexpectedValueException $exception) {
                    throw new \UnexpectedValueException(sprintf('%s - customer (%s)',
                        $exception->getMessage(),
                        $customerId
                    ));
                }

                if (empty($terminationDate)) {
                    echo sprintf("skipped customer (%s) because status = '%s' and no value for 'VERTRAGSDEAKTIVIERUNG'\n",
                        $customerId,
                        $status
                    );

                    continue 2;
                }

                try {
                    $terminationDate = new \DateTime($terminationDate);
                } catch (\Exception $exception) {
                    echo sprintf("skipped customer (%s) because status = '%s' and invalid value (%s) for 'VERTRAGSDEAKTIVIERUNG'\n",
                        $customerId,
                        $status,
                        $terminationDate
                    );

                    continue 2;
                }

                if ($terminationDate < new \DateTime()) {
                    echo sprintf("skipped customer (%s) because status = '%s' and value (%s) for 'VERTRAGSDEAKTIVIERUNG' in the past\n",
                        $customerId,
                        $status,
                        $terminationDate->format('d.m.Y')
                    );

                    continue 2;
                }

                break;

            case 'testkunde':
            case 'willkommensbrief':
            case 'auftragsbestätigung':
                echo sprintf("skipped customer (%s) because status = '%s'\n",
                    $customerId,
                    $status
                );

                continue 2;

            default:
                die(var_dump($status));
        }
    }



    $customer = new Customer();
    $toCustomerMapper->setObject($customer);

    try {
        $toCustomerMapper->setExternalCustomerId($lines);
    } catch (CustomerAlreadyExistsInDatabaseException $exception) {
        $customer->isNew = false;

        if (Notifications::isActive('setExternalCustomerId')) {
            trigger_error($exception->getMessage(), E_USER_NOTICE);
        }
    }

    $toCustomerMapper->setProducts($lines);
    $toCustomerMapper->setPurtelAccounts($lines);

    try {
        $toCustomerMapper->setLocation($lines);
    } catch (\UnexpectedValueException $exception) {
        echo sprintf("skipped customer (%s) - %s\n",
            $customerId,
            $exception->getMessage()
        );

        continue;
    }

    try {
        // do all the simple data mapping
        $toCustomerMapper->setConnectionActivationDate($lines);
        $toCustomerMapper->setContractId($lines);
        $toCustomerMapper->setBankAccountHolderLastname($lines);
        $toCustomerMapper->setBankAccountBic($lines);
        $toCustomerMapper->setBankAccountIban($lines);
        $toCustomerMapper->setBankAccountEmailaddress($lines);
        $toCustomerMapper->setWisocontractCancelDate($lines);
        $toCustomerMapper->setWisocontractSwitchoffFinish($lines);
        $toCustomerMapper->setConnectionStreet($lines);
        $toCustomerMapper->setConnectionStreetNo($lines);
        $toCustomerMapper->setConnectionCity($lines);
        $toCustomerMapper->setConnectionZipcode($lines);
        $toCustomerMapper->setTitle($lines);
        $toCustomerMapper->setFirstname($lines);
        $toCustomerMapper->setLastname($lines);
        $toCustomerMapper->setStreet($lines);
        $toCustomerMapper->setStreetno($lines);
        $toCustomerMapper->setCity($lines);
        $toCustomerMapper->setZipcode($lines);
        $toCustomerMapper->setBirthday($lines);
        $toCustomerMapper->setEmailaddress($lines);
        $toCustomerMapper->setStatiPortConfirmDate($lines);
        $toCustomerMapper->setCompany($lines);
        $toCustomerMapper->setContractVersion($lines);
        $toCustomerMapper->setNoEze($lines);
        $toCustomerMapper->setCreditRatingOk($lines);
        $toCustomerMapper->setCreditRatingDate($lines);
        $toCustomerMapper->setClienttype($lines);
        $toCustomerMapper->setPurtelEditDone($lines);
        $toCustomerMapper->setWisocontractCanceledDate($lines);
        $toCustomerMapper->setMacAddress($lines);
        $toCustomerMapper->setLineIdentifier($lines);
        $toCustomerMapper->setPppoePin($lines);
        $toCustomerMapper->setPassword($lines);
        $toCustomerMapper->setPaperBill($lines);
        $toCustomerMapper->setComment($lines);
    } catch (\UnexpectedValueException $exception) {
        throw new \UnexpectedValueException(sprintf('%s - customer (%s)',
            $exception->getMessage(),
            $customerId
        ));
    }

    $customer->district = 'Ingolstadt';
    $customer->networkId = NETWORK_ID;
    $customer->applicationText = 'with_supply';
    $customer->application = 'green';
    $customer->prospectSupplyStatus = 'with_supply';
    $customer->talOrderDate = 'N/E da GF';
    $customer->talDtagAssignmentNo = 'N/E da GF';
    $customer->customerConnectInfoDate = 'N/E da GF';
    $customer->talOrderAckDate = 'N/E da GF';
    $customer->dslamArrangedDate = new \DateTime('01.01.2000');
    $customer->terminalReady = new \DateTime('01.01.2000');
    $customer->patchDate = new \DateTime('01.01.2000');
    $customer->contractSentDate = new \DateTime('01.01.2000');
    $customer->contractReceivedDate = new \DateTime('01.01.2000');

    if (defined('IP_RANGE')) {
        $customer->ipRangeId = IP_RANGE;
    }

    // set customer.telefonbuch_eintrag
    foreach ($customer->purtelAccounts as $purtelAccount) {
        if (!empty($purtelAccount->phoneBookInformation) || !empty($purtelAccount->phoneBookDigitalMedia) || !empty($purtelAccount->phoneBookInverseSearch)) {
            $customer->telefonbuchEintrag = 'Ja';

            break;
        }
    }

    $customers[] = $customer;
}





// import new products
foreach ($knownProducts as $product) {
    if (!$product->isNew) {
        continue;
    }

    if (!IS_SIMULATION) {
        $db->query($product->buildInsertQuery());

        if (!empty($db->error)) {
            throw new Exception($db->error);
        }

        $product->id = $db->insert_id;
    } else {
        $product->id = 0;
    }
}

// import new locations
foreach ($knownLocations as $location) {
    if (!$location->isNew) {
        continue;
    }



    // REMOVE NEXT 2 LINES !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //$location->id = 99;
    //continue;


    die(var_dump("is new location", $location));
    // save to database and set insert_id to $location->id
}

$clientIdStartingString = NETZ.'.'.CLIENT_ID_YEAR.'.';
$lastClientId = 0;

$lastClientIdQuery = $db->query("SELECT `clientid` FROM `customers` 
    WHERE `clientid` LIKE '".$clientIdStartingString."%' ORDER BY `clientid` DESC LIMIT 1"
);

if (1 === $lastClientIdQuery->num_rows) {
    $lastClientId = $lastClientIdQuery->fetch_row();
    $lastClientId = $lastClientId[0];
    $lastClientId = explode('.', $lastClientId);
    $lastClientId = (int) end($lastClientId);
}

$firstCustomerId = null;
$lastCustomerId = null;

// import customers
foreach ($customers as $customer) {
    if (!$customer->isNew) {
        continue;
    }

    echo sprintf("importing %s\n", $customer->externalCustomerId);

    // save to database and set insert_id to $customer->id

    $customer->clientId = $clientIdStartingString.str_pad(++$lastClientId, 4, "0", STR_PAD_LEFT);

    // find main product
    $mainProductQuery = $db->query(sprintf("SELECT `id` FROM `products` WHERE id IN (%s) AND type = 'Tarif'",
        implode(', ', array_map(function ($value) {
            return sprintf("'%s'", $value->product->id);
        }, $customer->products))
    ));

    if ($mainProductQuery->num_rows < 1) {
        trigger_error(sprintf('ATTENTION !!! no main product found for customer (%s)',
            $customer->externalCustomerId
        ), E_USER_NOTICE);
    } else {
        // set main product & purtel-product
        $mainProductIds = array_map(function ($array) {
            return $array[0];
        }, $mainProductQuery->fetch_all());

        $foundMainProduct = [];

        foreach ($customer->products as $customerProduct) {
            if (in_array($customerProduct->product->id, $mainProductIds) ) {
                if (null === $customerProduct->deactivationDate || $customerProduct->deactivationDate > new \DateTime()) {
                    $foundMainProduct[] = $customerProduct;
                }
            }
        }

        if (count($foundMainProduct) === 1) {
            $foundMainProduct = $foundMainProduct[0];
            $foundMainProduct->propperlyCopyedToCustomer = 1;

            $customer->setPurtelProduct('Dimari-'.$foundMainProduct->product->identifier); // prepend "Dimari-"
            $customer->setProductname('Dimari-'.$foundMainProduct->product->identifier); // prepend "Dimari-"
        } else {
            trigger_error(sprintf('Invalid amount of main products found (%s) for customer (%s) - non set - products (%s)',
                count($foundMainProduct),
                $customer->externalCustomerId,
                implode(', ', array_map(function ($value) {
                    return sprintf("%s", $value->product->name);
                }, $foundMainProduct))
            ), E_USER_NOTICE);
        }
    }

    if (!IS_SIMULATION) {
        $db->query($customer->buildInsertQuery());

        if (!empty($db->error)) {
            throw new Exception($db->error);
        }

        $customer->id = $db->insert_id;
    } else {
        $customer->id = 0;
    }

    echo sprintf("\tikv-id: %s\n", $customer->id);

    if (null === $firstCustomerId) {
        $firstCustomerId = $customer->id;
    }

    if (!IS_SIMULATION) {
        if (null !== $lastCustomerId) {
            if ($lastCustomerId + 1 !== $customer->id) {
                trigger_error(sprintf('ATTENTION !!! there was an unexpected skip between last insert_id (%s) and this one (%s) - Make sure to remember this for your next steps',
                    $lastCustomerId,
                    $customer->id
                ), E_USER_NOTICE);
            }
        }
    }

    $lastCustomerId = $customer->id;

    // save products
    if (!empty($customer->products)) {
        foreach ($customer->products as $product) {
            $product->customer = $customer;

            if (!IS_SIMULATION) {
                $db->query($product->buildInsertQuery());

                if (!empty($db->error)) {
                    throw new Exception($db->error);
                }

                $product->id = $db->insert_id;
            } else {
                $product->id = 0;
            }
        }
    }

    // save purtel accounts
    if (!empty($customer->purtelAccounts)) {
        foreach ($customer->purtelAccounts as $purtelAccount) {
            $purtelAccount->customer = $customer;

            if (!IS_SIMULATION) {
                $db->query($purtelAccount->buildInsertQuery());

                if (!empty($db->error)) {
                    throw new Exception($db->error);
                }

                $purtelAccount->id = $db->insert_id;
            } else {
                $purtelAccount->id = 0;
            }
        }
    }
}

echo sprintf("first imported customerId (%s) - last (%s)\n",
    $firstCustomerId,
    $lastCustomerId
);
