<?php

require_once __DIR__.'/../../../vendor/wisotel/configuration/Configuration.php';

$purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

$kundennummer_extern = $clientId;

$parameter = [
    'periode' => '201804',
    'erweitert' => 9
];

$url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
    urlencode($purtelSuperUsername),
    urlencode($purtelSuperPassword),
    'stammdatenexport'
);

$url .= '&'.utf8_decode(http_build_query($parameter));

$curl = curl_init($url);

curl_setopt_array($curl, array(
    CURLOPT_HEADER          => false,
    CURLOPT_RETURNTRANSFER  => true,
    CURLOPT_SSL_VERIFYPEER  => false,
));


$result = curl_exec($curl);

curl_close($curl);

$content = trim($result);

echo $content;
