<?php

// spezialfall kmu

require __DIR__.'/../../../_conf/database.inc';
require __DIR__.'/../CsvReader.php';

$kmuCsv = new \CsvReader('/tmp/Rufnummern_20180821-2.csv');

$q = $db->query("SELECT p.*, c.`clientid` 
    FROM `purtel_account` p 
    INNER JOIN `customers` c ON c.`id` = p.`cust_id` 
    WHERE p.`cust_id` IN(8566, 8566, 8566, 8566, 8566, 8567, 8567, 6279, 6279, 6279, 7558, 8570, 8570, 8570, 8570, 8570, 8570, 8572, 9824, 8573, 8576, 8576, 8576, 8576, 8577, 8577, 8577, 8577, 8577, 8578, 8578, 8579, 8580, 8580, 8581, 8581, 8581, 8581, 8581, 8581, 8582, 6280, 6280, 6280, 8584, 8588, 8588, 8588, 8588, 8588, 8588, 8588, 8588, 8588, 8588, 7241, 5495, 5495, 5495, 8590, 8591, 8591, 8591, 8591, 8591, 8591, 8591, 8591, 8592, 8594, 8595, 8596, 8597, 8597, 8597, 8597, 8597, 8597, 7556, 7556, 7556, 8599, 7937, 7937, 7937, 7937, 7937, 7937, 7937, 7937, 8420, 8420, 8420, 8420, 8420, 8420, 8420, 8601, 8602, 8603, 8603, 8603, 8603, 8603, 8603, 8603, 8603, 8603, 8604, 8604, 8604, 8604, 8604, 8604, 8604, 8604, 8604, 5500, 5500, 5500, 5500, 5500, 5500, 5500, 5500, 5500, 8605, 8605, 8605, 8606, 8607, 8609, 8610, 8611, 8612, 8612, 8086, 8613, 8613, 8613, 8613, 8613, 8613, 8613, 8613, 8613, 8614, 5501, 5769, 5769, 8616, 8616, 8616, 8618, 8618, 8618, 8619, 8620, 8621, 8622, 8622, 8622, 8623, 8624, 8624, 8624, 8625, 8625, 8625, 8625, 8625, 8625, 8625, 8625, 8625, 8626, 8627, 8627, 8627, 8627, 8627, 8627, 8627, 8627, 8627, 8569, 8569, 8569, 8569, 8569, 8569, 8569, 5505, 8628, 8628, 8628, 8628, 8628, 8628, 8628, 8628, 8628, 8630, 8631, 8631, 8631, 8632, 8633, 8633, 8633, 7559, 7559, 7559, 7559, 7559, 7559, 7559, 7559, 7559, 8634, 8634, 8634, 8421, 8421, 8421, 8421, 8421, 8635, 8636, 8636, 8636, 8011, 8637, 8638, 8639, 7560, 8082, 8640, 8640, 8640, 8640, 8640, 8640, 8640, 8640, 8640, 8641, 8642, 8643, 8643, 8644, 8644, 8644, 8644, 8644, 8644, 8644, 8644, 8646, 8646, 8646, 8646, 8646, 8646, 8646, 8646, 8646, 8647, 8647, 8647, 8648, 8648, 8648, 8649, 8650, 8652, 8653, 8654, 8655, 8655, 8655, 8655, 8655, 8655, 8655, 8655, 8655, 8655, 8655, 8656, 8656, 8656, 8656, 8656, 8656, 8656, 8656, 8039, 8039, 8039, 8657, 8657, 8657, 8657, 8657, 8657, 8658, 8658, 8658, 8658, 8084, 8659, 8659, 8659, 8659, 8659, 8659, 8659)");


// must be a positiv integer (or 0) as string
//define('WBCI_VORAB_ID_PREFIX', '26');
$wbciIds = [
    '11.09.2018' => '30',
    '12.09.2018' => '31',
    '13.09.2018' => '32',
    '14.09.2018' => '33',
    '17.09.2018' => '34',
    '18.09.2018' => '35',
    '19.09.2018' => '36',
    '20.09.2018' => '37',
    '21.09.2018' => '38',
];

// date - format tt.mm.YYYY
define('PORTING_DATE', '05.09.2018');


$ignoreCustomer = [
];


$sortedByCustomerLogin = [];

while ($d = $q->fetch_assoc()) {
    if (in_array($d['cust_id'], $ignoreCustomer)) {
        continue;
    }

    $customer = $d['cust_id'];

    if (!isset($sortedByCustomerLogin[$customer])) {
        $sortedByCustomerLogin[$customer] = [];
    }

    $sortedByCustomerLogin[$customer][] = $d;
}


$outArray = [
    'anschlussnummer' => null,
    'Kundennummer_extern' => null,
    'ONKZ' => null,
    'MSN 1' => null,
    'MSN 2' => null,
    'MSN 3' => null,
    'MSN 4' => null,
    'MSN 5' => null,
    'MSN 6' => null,
    'MSN 7' => null,
    'MSN 8' => null,
    'MSN 9' => null,
    'MSN 10' => null,
    'Kopfnummer' => null,
    'Abfragestelle' => null,
    'Block' => null,
    'PKI_AB' => 'D061',
    'PKI_AUF' => 'D027',
    'Termin' => PORTING_DATE,
    'WBCI Vorab_ID' => 'DEU.COMIN.VMAS', // + 000001
    'already ported D027' => null,
];


$counter = 4;

$csvHandle = fopen('php://output', 'w');

fputcsv($csvHandle, array_keys($outArray), ';');

foreach ($sortedByCustomerLogin as $customerId => $data) {
    $purtelMasterAccount = null;
    $externalCustomerId = null;
    //$onkz_ = null;
    $phonenumbers = [];
    $xx = 0;

    foreach ($data as $purtelAccount) {
        if ($purtelAccount['master'] === '1') {
            $purtelMasterAccount = $purtelAccount['purtel_login'];
            $externalCustomerId = $purtelAccount['clientid'];
        }

        if (empty($purtelAccount['nummer'])) {
            continue;
        }

        $onkzLength = strlen($purtelAccount['area_code']);

        $onkz = $purtelAccount['area_code'];

        if (!empty($purtelAccount['nummernblock'])) {
            $onkz .= '_'.$xx++;
        }

        if (!isset($phonenumbers[$onkz])) {
            $phonenumbers[$onkz] = [];
        }

        $phonenumbers[$onkz][] = [
            'number' => substr($purtelAccount['nummer'], $onkzLength),
            'block' => $purtelAccount['nummernblock'],
        ];
    }

    if (null === $purtelMasterAccount) {
        var_dump($customerId);
        die("no master");
    }

    if (empty($externalCustomerId)) {
        var_dump($customerId);
        die("no clientid");
    }

    if (empty($phonenumbers)) {
        continue;
    }

    foreach ($phonenumbers as $onkz => $numbers) {
        if (count($numbers) > 10) {
            //var_dump("too many phonenumbers", $customerId);
            //die("too many phonenumbers");
            continue 2;
        }
    }

    foreach ($phonenumbers as $onkz => $numbers) {
        if (false !== $strPos = strpos($onkz, '_')) {
            $onkz = substr($onkz, 0, $strPos);
        }

        $outData = $outArray;
        $outData['anschlussnummer'] = $purtelMasterAccount;
        $outData['Kundennummer_extern'] = $externalCustomerId;
        $outData['ONKZ'] = $onkz;

        $wkbciVorabId = null;
        $numberNotInCsv = false;

        $alreadyPorted = [];
        $numbersAdded = 0;

        $indexCount = 1;

        foreach ($numbers as $index => $numberData) {
// auch das feld "ported"!

            $serachNumber = $onkz.$numberData['number'];
            $serachNumber = preg_replace('/^0/', '49', $serachNumber);

            $numberInCsv = $kmuCsv->findLines([
                'a_rn' => $serachNumber,
            ]);

            if (null === $numberInCsv) {
                $numberNotInCsv = true;

                continue;
            }

            $numbersAdded++;

            if (count($numberInCsv) !== 1) {
                die(var_dump( "darf nicht sein", $numberInCsv ));
            }

            $outData['MSN '.($indexCount++)] = $numberData['number'];

            if (!empty($numberData['block'])) {
                $outData['Block'] = $numberData['block'];
            }

            $numberInCsv = end($numberInCsv);

            try {
                $termin = new \DateTime($numberInCsv[$kmuCsv->headlineReversed['Portierungsdatum']]);
            } catch (\Exception $exception) {
                die(var_dump( 'ungueltiges datum fuer portierungstermin', $numberInCsv ));
            }

            $outData['Termin'] = $termin->format('d.m.Y');

            if (!isset($wbciIds[$termin->format('d.m.Y')])) {
                die("falsches datum - ".$termin->format('d.m.Y'));
            }

            $wbciId = $wbciIds[$termin->format('d.m.Y')];
            
            if (null === $wkbciVorabId) {
                $wkbciVorabId = $outData['WBCI Vorab_ID'].$wbciId.str_pad((string) ++$counter, 6 - strlen($wbciId), '0', STR_PAD_LEFT);
            } else {
                if (1 !== preg_match('/^'.str_replace('.', '\\.', $outData['WBCI Vorab_ID']).$wbciId.'/', $wkbciVorabId)) {
                    die("wkbciVorabId unterschiedlich");
                }
            }

            // keine ahnung wie ich das auf die liste mit schreiben soll,
            // da es für jede nummer ist, auf der liste aber die nummern zusammengefasst
            // in einer spalte stehen
            // already ported D027
            if ('TRUE' == $numberInCsv[$kmuCsv->headlineReversed['already ported D027']]) {
                $alreadyPorted[] = $numberData['number'];
            }
        }

        if ($numbersAdded > 10) {
            die(var_dump("too many phonenumbers", $customerId));
        }

        if (null === $wkbciVorabId) {
            if ($numberNotInCsv) {
                continue;
            }

            die("wkbciVorabId leer. darf nicht sein");
        }

        $outData['WBCI Vorab_ID'] = $wkbciVorabId;

        $outData['already ported D027'] = implode(', ', $alreadyPorted);

        fputcsv($csvHandle, $outData, ';');
    }
}

fclose($csvHandle);
