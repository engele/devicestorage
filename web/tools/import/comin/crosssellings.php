<?php

define('DEFAULT_ACTIVATION_DATE', '2018-04-01');

$oneTimeProducts = [
    '10016',
    '10024',
    '10026',
    '10037',
    '10040',
    '10044',
    '10045',
    '10046',
    '10047',
    '10048',
    '10054',
    '10059',
    '10060',
    '10061',
    '10077',
    '10078',
    '10083',
    '10180',
    '10182',
    '10201',
    '10202',
    '10204',
    '10208',
    '10210',
    '10212',
    '10213',
    '10214',
    '10226',
    '10230',
    '10266',
    '10268',
    '10270',
    '10272',
    '10275',
    '10276',
    '10295',
    '10300',
    '10313',
    '10314',
    '10315',
    '10316',
    '10317',
    '10318',
    '10328',
    '10343',
    '10041',
    '10076',
    '10051',
    '10298',
    '10209',
    '10205',
];

$ignoredProducts = [
    '10006',
    '1002',
    '10257',
    '10311',
    '10038',
    '1001',
    '10228',
    '10187',
    '10211',
    '10206',
    '10207',
    '10062',
    '10053',
    '10203',
    '10183',
    '10186',
    '10260',
    '10191',
    '10199',
    '10193',
    '10305',
    '10346',
    '10127',
    '10261',
    '10304',
    '10312',
    '10194',
    '10004',
    '10229',
    '10042',
    '10034',
    '10192',
    '10058',
    '10283',
];

$danielSpecial = [
    '14240' => 'sven 01.04.2018',
    '14380' => 'sven 01.04.2018',
    '14683' => 'sven 01.04.2018',
    '14847' => 'sven 01.04.2018',
    '14866' => 'sven 01.04.2018',
    '15418' => 'sven 01.04.2018, wichtig ohne GS Teilbereitstellung',
    '15431' => 'sven 01.04.2018',
    '15496' => 'sven 01.04.2018',
    '15527' => 'sven 01.04.2018',
    '15866' => 'sven 01.04.2018',
    '16271' => 'sven 01.04.2018',
    '16408' => 'sven 01.04.2018, wichtig ohne Aktion Testwochen',
    '16514' => 'sven 01.04.2018',
    '16729' => 'sven 01.04.2018, wichtig ohne Aktion Testwochen',
    '16760' => 'sven 01.04.2018',
    '16972' => 'sven 01.04.2018',
    '17074' => 'sven 01.04.2018',
    '17108' => 'sven 01.04.2018, wichtig ohne Aktion WoWi',
    '17142' => 'sven 01.04.2018',
    '17143' => 'sven 01.04.2018',
    '17371' => 'sven 01.04.2018',
    '17899' => 'sven 01.04.2018, wichtig ohne GS Teilbereitstellung',
    '18562' => 'sven 01.04.2018',
    '18640' => 'sven 01.04.2018',
    '18710' => '"im purtel korrigiert und aktiviert zum 01.04.2018 mit Crossselling',
    '18859' => 'sven 01.04.2018',
    '19066' => '"im purtel korrigiert und aktiviert zum 01.04.2018 mit Crossselling',
    '19122' => 'sven 01.04.2018',
    '19164' => 'sven 01.04.2018',
    '19210' => 'sven 01.04.2018',
    '19259' => 'sven 01.04.2018',
    '19317' => 'sven 01.04.2018',
    '19333' => 'sven 01.04.2018',
    '19540' => 'sven 01.04.2018',
    '19732' => '"im purtel korrigiert und aktiviert zum 01.04.2018 mit Crossselling',
    '20027' => '"im purtel korrigiert und aktiviert zum 01.04.2018 mit Crossselling',
    '20028' => '"im purtel korrigiert und aktiviert zum 01.04.2018 mit Crossselling',
    '20129' => 'sven 01.04.2018',
    '20207' => '"im purtel korrigiert und aktiviert zum 01.04.2018 mit Crossselling',
    '20371' => '"im purtel korrigiert und aktiviert zum 01.04.2018 mit Crossselling',
];

$skipCustomer = [
    '5508',
];


$csvColumns = [
    'kundennummer' => '100260',
    'rufnummer' => null,
    'datum' => null,
    'produkt_id' => null,
    'einrichtung' => null,
    'grundgebuehr' => null,
    'laufzeit' => null,
    'aktiv' => 1,
    'menge' => 1,
    'anteilig' => 0,
    'verrechnen' => 0,
    'deaktivieren' => 0,
    'einrichtung_nettobasiert' => 0,
    'einrichtung_bruttobasiert' => 0,
    'basisentgelt_nettobasiert' => null,
    'basisentgelt_bruttobasiert' => null,
    'Produktname' => null,
    'Dimari-Kundennummer' => null,
    'ikv-Kundennummer' => null,
    'Cluster' => null,
];

require __DIR__.'/../../../_conf/database.inc';

$csvOut = [];

$customerProducts = $db->query("SELECT cp.*, c.`customer_id` as dimariId, c.`clientid` as ikvId, l.`name` as cluster 
    FROM `customer_products` cp 
    LEFT JOIN `customers` c ON c.`id` = cp.`customer_id` 
    LEFT JOIN `locations` l ON c.`location_id` = l.`id`
");

while ($customerProduct = $customerProducts->fetch_assoc()) {
    if ('1' === $customerProduct['propperly_copyed_to_customer']) {
        // main products are already done -> skip
        continue;
    }

    if (in_array($customerProduct['customer_id'], $skipCustomer)) {
        trigger_error(sprintf('skipped - customer (%s)',
            $customerProduct['customer_id']
        ), E_USER_NOTICE);

        continue;
    }

    $productQ = $db->query("SELECT * FROM `products` WHERE `id` = ".$customerProduct['product_id']);

    if (1 !== $productQ->num_rows) {
        die("invalid amount of products (".$productQ->num_rows.") - product (".$customerProduct['product_id'].") - ".$customerProduct['customer_id']);
    }

    $product = $productQ->fetch_assoc();

    if (in_array($product['identifier'], $ignoredProducts)) {
        trigger_error(sprintf('skipped - customers (%s) product (%s) is ignored',
            $customerProduct['customer_id'],
            $product['identifier']
        ), E_USER_NOTICE);

        continue;
    }

    if (in_array($product['identifier'], $oneTimeProducts)) {
        trigger_error(sprintf('skipped - customers (%s) product (%s) is one time product',
            $customerProduct['customer_id'],
            $product['identifier']
        ), E_USER_NOTICE);

        continue;
    }

    if (isset($danielSpecial[$customerProduct['dimariId']])) {
        switch ($danielSpecial[$customerProduct['dimariId']]) {
            case '"im purtel korrigiert und aktiviert zum 01.04.2018 mit Crossselling':
                trigger_error(sprintf('skipped - customers (%s) product (%s) was ignored by danielSpecial (already done)',
                    $customerProduct['customer_id'],
                    $product['identifier']
                ), E_USER_NOTICE);

                continue 2;

            case 'sven 01.04.2018, wichtig ohne GS Teilbereitstellung':
                if ('10057' === $product['identifier']) {
                    trigger_error(sprintf('skipped - customers (%s) product (%s) was ignored by danielSpecial (GS Teilbereitstellung)',
                        $customerProduct['customer_id'],
                        $product['identifier']
                    ), E_USER_NOTICE);

                    continue 2;
                }

                break;
                
            //case 'sven 01.04.2018, wichtig ohne Aktion Testwochen': // 10207 - already in ignoredProducts
            //case 'sven 01.04.2018, wichtig ohne Aktion WoWi': // 10305 - already in ignoredProducts
            
            case 'sven 01.04.2018':
                $customerProduct['activation_date'] = '2018-04-01';
                break;
        }
    }

    if (empty($product['purtel_product_id'])) {
        echo "no purtel-product-id for product (".$customerProduct['product_id'].") - ".$customerProduct['customer_id']."\n";
        //die();
        continue; // can not use without purtel-product-id
    }

    $purtelMasterQuery = $db->query("SELECT * FROM `purtel_account` WHERE `master` = 1 AND `cust_id` = ".$customerProduct['customer_id']);

    if (1 !== $purtelMasterQuery->num_rows) {
        echo 'invalid amount of purtel master accounts ('.$purtelMasterQuery->num_rows.') - '.$customerProduct['customer_id']."\n";
        //die();
        continue;
    }

    $purtelMasterAccount = $purtelMasterQuery->fetch_assoc();

    if (empty($purtelMasterAccount['purtel_login'])) {
        trigger_error(sprintf('skipped - customers (%s) purtel-account has no purtel_login (may be was status "offen")',
            $customerProduct['customer_id']
        ), E_USER_NOTICE);

        continue;
    }

    $date = null;
    $laufzeitMonate = 0;

    if (empty($customerProduct['activation_date'])) {
        $customerProduct['activation_date'] = DEFAULT_ACTIVATION_DATE;
    }

    $activationDate = new \DateTime($customerProduct['activation_date']);

    if (!empty($customerProduct['deactivation_date'])) {
        $deactivationDate = new \DateTime($customerProduct['deactivation_date']);

        if ($deactivationDate <= new \DateTime()) {
            trigger_error(sprintf('skipped - customers (%s) date of deactivation is in past (%s)',
                $customerProduct['customer_id'],
                $customerProduct['deactivation_date']
            ), E_USER_NOTICE);

            continue; // skip because already outdated
        }

        $laufzeit = $activationDate->diff($deactivationDate);
        $laufzeitMonate = ($laufzeit->y * 12) + $laufzeit->m;

        if ($laufzeit->d > 0) {
            $laufzeitMonate++;
        }
    }

    $date = str_replace('-', '', $customerProduct['activation_date']);

    $priceNet = null;
    $priceGross = null;

    if (empty($customerProduct['price_net'])) {
        if (empty($product['default_price_gross'])) {
            if (empty($product['default_price_net'])) {
                $priceNet = 0;
                $priceGross = 0;
            } else {
                $priceNet = (float) $product['default_price_net'];
                $priceNet *= 100;
                $priceNet = round($priceNet, 0);
            }
        } else {
            $priceGross = (float) $product['default_price_gross'];
            $priceGross *= 100;
            $priceGross = round($priceGross, 0);
        }
    } else {
        $priceNet = (float) $customerProduct['price_net'];
        $priceNet *= 100;
        $priceNet = round($priceNet, 0);
    }

    // existiert überhaupt das passende cross-selling-produkt in der ikv?
    $optionQ = $db->query("SELECT * FROM `optionen` WHERE `option` = 'Dimari-".$product['purtel_product_id']."'");

    if ($optionQ->num_rows !== 1) {
        //echo "missing option - Dimari-".$product['purtel_product_id']."\n";
        //var_dump($product, $customerProduct['customer_id']);
        echo "X-".$customerProduct['id']."-".$customerProduct['customer_id'].";".$product['identifier'].";\"".$product['name']."\";".$product['purtel_product_id']."\n";
        continue;
    }

    $csvOut[] = array_merge($csvColumns, [
        'rufnummer' => $purtelMasterAccount['purtel_login'],
        'datum' => $date,
        'produkt_id' => $product['purtel_product_id'],
        'basisentgelt_nettobasiert' => $priceNet,
        'basisentgelt_bruttobasiert' => $priceGross,
        'laufzeit' => $laufzeitMonate,
        'deaktivieren' => $laufzeitMonate > 0 ? 1 : 0,
        'Produktname' => $product['name'],
        'Dimari-Kundennummer' => $customerProduct['dimariId'],
        'ikv-Kundennummer' => $customerProduct['ikvId'],
        'Cluster' => $customerProduct['cluster'],
    ]);

    /*echo sprintf("id (%s) set propperly_copyed_to_customer to 1\n",
        $customerProduct['id']
    );*/

    /*$db->query("INSERT INTO `customer_options` 
        SET (`cust_id`, `opt_id`, `price_net`, `price_gross`, `activation_date`, `deactivation_date`) 
        VALUES (".$customerProduct['customer_id'].", 'Dimari-".$product['identifier']."')");
    */

    echo "set propperly_copyed_to_customer, move to customer_options - customer_products_id = ".$customerProduct['id']."\n";
}

echo "\n\n\n\n\n";

$csvHandle = fopen('php://output', 'w');

fputcsv($csvHandle, array_keys($csvColumns), ';');

foreach ($csvOut as $key => $array) {
    fputcsv($csvHandle, $array, ';');
}

fclose($csvHandle);

