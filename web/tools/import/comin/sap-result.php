<?php

require __DIR__.'/../CsvReader.php';

$importCsv = new CsvReader('/tmp/import.csv');
$exportCsv = new CsvReader('/tmp/export.csv');

// group by iban
// $importGroupedByIban & $exportGroupedByIban
foreach (['import', 'export'] as $type) {
    ${$type.'GroupedByIban'} = [];

    foreach (${$type.'Csv'}->data as $key => $line) {
        $iban = trim($line[${$type.'Csv'}->headlineReversed['IBAN']]);

        if (!isset(${$type.'GroupedByIban'}[$iban])) {
            ${$type.'GroupedByIban'}[$iban] = [];
        } else {
            trigger_error(sprintf('found iban multiple times in %s (%s)',
                $type,
                $iban
            ), E_USER_NOTICE);
        }

        ${$type.'GroupedByIban'}[$iban][$key] = & ${$type.'Csv'}->data[$key];
    }
}

foreach ($exportGroupedByIban as $iban => $lines) {
    if (!isset($importGroupedByIban[$iban])) {
        trigger_error(sprintf('iban (%s) not in import',
            $iban
        ), E_USER_NOTICE);

        continue;
    }

    if (count($lines) !== count($importGroupedByIban[$iban])) {
        var_dump('andere anzahl ibans export zu import', $iban, count($lines), count($importGroupedByIban[$iban]));

        die();
    }
}

foreach ($importGroupedByIban as $iban => $lines) {
    if (!isset($exportGroupedByIban[$iban])) {
        trigger_error(sprintf('iban (%s) not in export',
            $iban
        ), E_USER_NOTICE);

        continue;
    }

    if (count($lines) !== count($exportGroupedByIban[$iban])) {
        var_dump('andere anzahl ibans import zu export', $iban, count($lines), count($exportGroupedByIban[$iban]));

        die();
    }
}






foreach ($exportGroupedByIban as $iban => $lines) {
    $linesInImport = $importCsv->findLines(['IBAN' => $iban]);

    if (count($linesInImport) !== count($lines)) {
        var_dump('andere anzahl bei suche von export iban in import', $iban, count($lines), count($linesInImport));
        die();
    }

    ksort($linesInImport);
    ksort($lines);

    /*if (count($linesInImport) > 1) {
        var_dump($linesInImport, $lines);
        die();
    }*/

    foreach ($lines as $key => $line) {
        $relatedImportLine = array_shift($linesInImport);

        $exportGroupedByIban[$iban][$key]['relatedImportLine'] = $relatedImportLine;
    }

    if (!empty($linesInImport)) {
        var_dump('not empty', $linesInImport);
        die();
    }
}




$columnsOut = [
    'Kontonummer IKV' => null,
    //'Anrede' => null,
    'Name 1' => null,
    'Name 2' => null,
    //'Suchbegriff' => null,
    //'Strasse / HsNr.' => null,
    //'PLZ' => null,
    //'Stadt' => null,
    //'Abweichender Kontoinhaber' => null,
    'IBAN' => null,
    //'Zahlweg' => null,
    'Debitor' => null,
    'Angelegt am' => null,
    'Mandatsreferenz' => null,
];
//Alte Kontonummer  Debitor Name 1  Name 2  Angelegt am IBAN    Mandatsreferenz Kontoinhaber

$csvHandle = fopen('php://output', 'w');

fputcsv($csvHandle, array_keys($columnsOut), ';');

foreach ($exportGroupedByIban as $iban => $lines) {
    //if (count($lines) > 1) die(var_dump($lines));

    foreach ($lines as $key => $line) {
        fputcsv($csvHandle, [
            'Kontonummer IKV' => $line['relatedImportLine'][$importCsv->headlineReversed['Kontonummer IKV']],
            //'Anrede' => $line[$exportCsv->headlineReversed['Anrede']],
            'Name 1' => $line[$exportCsv->headlineReversed['Name 1']],
            'Name 2' => $line[$exportCsv->headlineReversed['Name 2']],
            //'Suchbegriff' => $line[$exportCsv->headlineReversed['Suchbegriff']],
            //'Strasse / HsNr.' => $line[$exportCsv->headlineReversed['Strasse / HsNr.']],
            //'PLZ' => $line[$exportCsv->headlineReversed['PLZ']],
            //'Stadt' => $line[$exportCsv->headlineReversed['Stadt']],
            //'Abweichender Kontoinhaber' => $line[$exportCsv->headlineReversed['Abweichender Kontoinhaber']],
            'IBAN' => $line[$exportCsv->headlineReversed['IBAN']],
            //'Zahlweg' => $line[$exportCsv->headlineReversed['Zahlweg']],
            'Debitor' => $line[$exportCsv->headlineReversed['Debitor']],
            'Angelegt am' => $line[$exportCsv->headlineReversed['Angelegt am']],
            'Mandatsreferenz' => $line[$exportCsv->headlineReversed['Mandatsreferenz']],
        ], ';');
    }
}

fclose($csvHandle);
