<?php

require __DIR__.'/../../../_conf/database.inc';

/*
$q = $db->query("SELECT p.*, c.`clientid` 
    FROM `purtel_account` p 
    INNER JOIN `customers` c ON c.`id` = p.`cust_id` 
    WHERE (p.`cust_id` BETWEEN 5802 AND 6246) OR (c.`customer_id` IN (12209, 12705, 12834, 13429, 13605, 14899, 15156, 18952))");
*/
/*$q = $db->query("SELECT p.*, c.`clientid` 
    FROM `purtel_account` p 
    INNER JOIN `customers` c ON c.`id` = p.`cust_id` 
    WHERE p.`cust_id` BETWEEN 6296 AND 7552 AND c.`location_id` = 25");
*/
$q = $db->query("SELECT p.*, c.`clientid` 
    FROM `purtel_account` p 
    INNER JOIN `customers` c ON c.`id` = p.`cust_id` 
    WHERE p.`cust_id` BETWEEN 16126 AND 16431");


// must be a positiv integer (or 0) as string
define('WBCI_VORAB_ID_PREFIX', '26');

// date - format tt.mm.YYYY
define('PORTING_DATE', '05.09.2018');


$ignoreCustomer = [
];


$sortedByCustomerLogin = [];

while ($d = $q->fetch_assoc()) {
    if (in_array($d['cust_id'], $ignoreCustomer)) {
        continue;
    }

    $customer = $d['cust_id'];

    if (!isset($sortedByCustomerLogin[$customer])) {
        $sortedByCustomerLogin[$customer] = [];
    }

    $sortedByCustomerLogin[$customer][] = $d;
}


$outArray = [
    'anschlussnummer' => null,
    'Kundennummer_extern' => null,
    'ONKZ' => null,
    'MSN 1' => null,
    'MSN 2' => null,
    'MSN 3' => null,
    'MSN 4' => null,
    'MSN 5' => null,
    'MSN 6' => null,
    'MSN 7' => null,
    'MSN 8' => null,
    'MSN 9' => null,
    'MSN 10' => null,
    'Kopfnummer' => null,
    'Abfragestelle' => null,
    'Block' => null,
    'PKI_AB' => 'D061',
    'PKI_AUF' => 'D027',
    'Termin' => PORTING_DATE,
    'WBCI Vorab_ID' => 'DEU.COMIN.VMAS', // + 000001
];


$counter = 4;

$csvHandle = fopen('php://output', 'w');

fputcsv($csvHandle, array_keys($outArray), ';');

foreach ($sortedByCustomerLogin as $customerId => $data) {
    $purtelMasterAccount = null;
    $externalCustomerId = null;
    //$onkz_ = null;
    $phonenumbers = [];

    foreach ($data as $purtelAccount) {
        if ($purtelAccount['master'] === '1') {
            $purtelMasterAccount = $purtelAccount['purtel_login'];
            $externalCustomerId = $purtelAccount['clientid'];
        }

        if (empty($purtelAccount['nummer'])) {
            continue;
        }

        $onkzLength = strlen($purtelAccount['area_code']);

        $onkz = $purtelAccount['area_code'];

        /*if (null !== $onkz_ && $onkz_ !== $onkz) {
            var_dump($customerId);
            die('not same onkz');
        }

        $onkz_ = $onkz;*/
        if (!isset($phonenumbers[$onkz])) {
            $phonenumbers[$onkz] = [];
        }

        $phonenumbers[$onkz][] = substr($purtelAccount['nummer'], $onkzLength);
    }

    if (null === $purtelMasterAccount) {
        var_dump($customerId);
        die("no master");
    }

    if (empty($externalCustomerId)) {
        var_dump($customerId);
        die("no clientid");
    }

    if (empty($phonenumbers)) {
        continue;
    }

    foreach ($phonenumbers as $onkz => $numbers) {
        if (count($numbers) > 10) {
            var_dump("too many phonenumbers", $customerId);
            die("too many phonenumbers");
            continue 2;
        }
    }

    foreach ($phonenumbers as $onkz => $numbers) {
        $outData = $outArray;
        $outData['anschlussnummer'] = $purtelMasterAccount;
        $outData['Kundennummer_extern'] = $externalCustomerId;
        $outData['ONKZ'] = $onkz;
        $outData['WBCI Vorab_ID'] .= WBCI_VORAB_ID_PREFIX.str_pad((string) ++$counter, 6 - strlen(WBCI_VORAB_ID_PREFIX), '0', STR_PAD_LEFT);

        foreach ($numbers as $index => $number) {
            $outData['MSN '.($index + 1)] = $number;
        }

        fputcsv($csvHandle, $outData, ';');
    }
}

fclose($csvHandle);
