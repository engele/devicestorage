<?php

/*
 * Korrigiert die falschen Haupt-Produkte und Buchungen
 */

require __DIR__.'/../../../_conf/database.inc';
require_once __DIR__.'/../../../vendor/wisotel/configuration/Configuration.php';

$productsFromPurtel = [
    '4578' => ['name' => 'Leistungsfrei', 'price' => '0'],
    '4582' => ['name' => 'Fon / 24 Monate', 'price' => '1495'],
    '4794' => ['name' => 'Web 100 / 24 Monate', 'price' => '2495'],
    '4796' => ['name' => 'HD-TV & Fon / 24 Monate', 'price' => '2495'],
    '4798' => ['name' => 'Komfort 100 / 24 Monate', 'price' => '2995'],
    '4842' => ['name' => 'HD-TV / 24 Monate', 'price' => '995'],
    '4870' => ['name' => 'HD-TV & Web 100 / 24 Monate', 'price' => '3495'],
    '4876' => ['name' => 'Premium 100 / 24 Monate', 'price' => '3995'],
    '4979' => ['name' => 'Fon', 'price' => '1495'],
    '4981' => ['name' => 'HD-TV & Web 100', 'price' => '3495'],
    '4983' => ['name' => 'Web 100', 'price' => '2495'],
    '4985' => ['name' => 'Komfort 100', 'price' => '2995'],
    '4987' => ['name' => 'Premium 100', 'price' => '3995'],
    '4989' => ['name' => 'HD-TV & Fon', 'price' => '2495'],
    '4991' => ['name' => 'HD-TV', 'price' => '995'],
    '5130' => ['name' => 'Business I3 / 36 Monate', 'price' => '5343'],
    '5132' => ['name' => 'Business S1-AA/2', 'price' => '2368'],
    '5142' => ['name' => 'Glasfaser Premium 100', 'price' => '4990'],
    '5144' => ['name' => 'Glasfaser Komfort 100', 'price' => '3990'],
    '5146' => ['name' => 'Glasfaser Web', 'price' => '3490'],
    '5150' => ['name' => 'Glasfaser Fon', 'price' => '2190'],
    '5152' => ['name' => 'Glasfaser HD-TV', 'price' => '1390'],
    '5154' => ['name' => 'Glasfaser COM-FIX GeWo ', 'price' => '0'],
    '5156' => ['name' => 'Glasfaser Premium 50', 'price' => '3990'],
    '5158' => ['name' => 'Glasfaser Premium 100 / 24 Monate', 'price' => '4990'],
    '5160' => ['name' => 'Glasfaser Premium 50 / 24 Monate', 'price' => '3990'],
    '5162' => ['name' => 'Glasfaser Komfort 100 / 24 Monate', 'price' => '3990'],
    '5164' => ['name' => 'Glasfaser Komfort 50', 'price' => '2990'],
    '5166' => ['name' => 'Glasfaser Komfort 50 / 24 Monate', 'price' => '2990'],
    '5168' => ['name' => 'Glasfaser Web 100', 'price' => '2990'],
    '5170' => ['name' => 'Glasfaser Web 100 / 24 Monate', 'price' => '2990'],
    '5172' => ['name' => 'Glasfaser Web 50', 'price' => '2489'],
    '5174' => ['name' => 'Glasfaser Web 50 / 24 Monate', 'price' => '2489'],
    '5258' => ['name' => 'Glasfaser Fon / 24 Monate', 'price' => '1990'],
    '5260' => ['name' => 'Glasfaser Fon und HD-TV', 'price' => '2990'],
    '5262' => ['name' => 'Glasfaser Fon und HD-TV / 24 Monate', 'price' => '2990'],
    '5264' => ['name' => 'Glasfaser HD-TV / 24 Monate', 'price' => '1390'],
    '5266' => ['name' => 'Glasfaser Web 50 und HD-TV / 24 Monate', 'price' => '3490'],
    '5268' => ['name' => 'Glasfaser Web 25', 'price' => '1990'],
    '5270' => ['name' => 'Glasfaser Web 25 / 24 Monate', 'price' => '1990'],
    '5272' => ['name' => 'Glasfaser Komfort 25 / 24 Monate', 'price' => '2489'],
    '5274' => ['name' => 'Glasfaser Premium 25 / 24 Monate', 'price' => '3490'],
    '5360' => ['name' => 'Business S1-MGA/2', 'price' => '2368'],
    '5362' => ['name' => 'Business S2-AA/5 ', 'price' => '4748'],
    '5364' => ['name' => 'Business S3-AA/10 ', 'price' => '11888'],
    '5366' => ['name' => 'Business S4 AA/30 ', 'price' => '11888'],
    '5444' => ['name' => 'Premium 100 / 24 Monate + LP1', 'price' => '3995'],
    '5494' => ['name' => 'Glasfaser Komfort 50 + LP 1', 'price' => '2990'],
    '5496' => ['name' => 'Glasfaser COM-FIX GeWo + LP 1', 'price' => '0'],
    '5498' => ['name' => 'Glasfaser COM-FIX GeWo + LP 1 & LP 2', 'price' => '0'],
    '5500' => ['name' => 'Glasfaser COM-FIX GeWo + LP 2', 'price' => '0'],
    '5502' => ['name' => 'Glasfaser Fon und HD-TV + LP 1', 'price' => '2990'],
    '5504' => ['name' => 'Glasfaser Fon + LP 1', 'price' => '2190'],
    '5506' => ['name' => 'Glasfaser Komfort 100 + LP 1', 'price' => '3990'],
    '5508' => ['name' => 'Glasfaser Komfort 100 + LP 2', 'price' => '3990'],
    '5510' => ['name' => 'Glasfaser Komfort 25 + LP 1', 'price' => '2489'],
    '5512' => ['name' => 'Glasfaser Komfort 50 + LP 2', 'price' => '2990'],
    '5514' => ['name' => 'Glasfaser Premium 100 + LP 1', 'price' => '4990'],
    '5516' => ['name' => 'Glasfaser Premium 50 + LP 1', 'price' => '3990'],
    '5518' => ['name' => 'Glasfaser Premium 50 + LP 2', 'price' => '3990'],
    '5520' => ['name' => 'Premium 100 + LP1', 'price' => '3990'],
    '5524' => ['name' => 'Komfort 100 + LP 1', 'price' => '2990'],
    '5526' => ['name' => 'Komfort 100 + LP 2', 'price' => '2990'],
    '5528' => ['name' => 'Komfort 100 / 24 Monate + LP 1', 'price' => '2990'],
    '5530' => ['name' => 'Komfort 100 / 24 Monate + LP 2', 'price' => '2990'],
    '5664' => ['name' => 'Business I2 / 36 Monate', 'price' => '4153'],
    '5666' => ['name' => 'Business I1 / 36 Monate', 'price' => '2963'],
];

/**
 * Get products for all accounts by kundennummer_extern
 */
function getCurrentPurtelProducts($clientId)
{
    $purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
    $purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

    $kundennummer_extern = $clientId;

    $parameter = [
        'kundennummer_extern' => $kundennummer_extern,
        'erweitert' => 1
    ];

    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'getaccounts'
    );
    
    $url .= '&'.utf8_decode(http_build_query($parameter));

    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $content = trim($result);
    
    $delimiter = null;

    foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
        if (false !== strpos($content, $delimiter_)) {
            $delimiter = $delimiter_;
            break;
        }
    }

    if (null == $delimiter) {
        var_dump($content);
        trigger_error(sprintf("\033[1;31mdelemiter not found - customer (%s)\033[0m",
            $clientId
        ), E_USER_NOTICE);

        return null;
    }

    $result = array_map(function ($line) {
        $data = str_getcsv($line, ';');

        foreach ($data as $key => $value) {
            if (is_scalar($value)) {
                if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                    $data[$key] = substr($value, 1, -1);
                } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                    $data[$key] = substr($value, 1, -1);
                }
            }
        }

        return $data;
    }, explode($delimiter, $content));

    unset($result[0]);

    return $result;
}

/**
 * Get buchungen for all accounts by kundennummer_extern
 */
function getBuchungen($clientId)
{
    $purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
    $purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

    $kundennummer_extern = $clientId;

    $parameter = [
        'kundennummer_extern' => $kundennummer_extern,
        'von_datum' => '20180101',
        'bis_datum' => '20180416',
        //'erweitert' => '2',
    ];

    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'getbuchungen'
    );
    
    $url .= '&'.utf8_decode(http_build_query($parameter));

    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $content = trim($result);
    
    $delimiter = null;

    foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
        if (false !== strpos($content, $delimiter_)) {
            $delimiter = $delimiter_;
            break;
        }
    }

    if (null == $delimiter) {
        var_dump($content);
        trigger_error(sprintf("\033[1;31mdelemiter not found - customer (%s)\033[0m",
            $clientId
        ), E_USER_NOTICE);

        return null;
    }

    $result = array_map(function ($line) {
        $data = str_getcsv($line, ';');

        foreach ($data as $key => $value) {
            if (is_scalar($value)) {
                if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                    $data[$key] = substr($value, 1, -1);
                } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                    $data[$key] = substr($value, 1, -1);
                }
            }
        }

        return $data;
    }, explode($delimiter, $content));

    unset($result[0]);

    return $result;
}

/**
 * update product at purtel
 */
function updateProductAtPurtel($clientId, $productId, $customerId)
{
    global $db;

    $purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
    $purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

    $updatePostData = [
        'wechseln' => 2,
        'buchen' => 0,
    ];

    $updatePostData['produkt'] = $productId;

    $updatePostData['kundennummer_extern'] = $clientId;

    // Optionale Angabe einen Anschlusses.
    // Ist dieser Wert gefüllt, wirkt sich die Änderung nur auf diesen Anschluss aus.
    // Die anderen Anschlüsse der externen Kundennummer bleiben unberührt.
    //$updatePostData['anschluss'] = $purtelMaster['purtel_login'];

    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'changeprodukt'
    );
    
    $url .= '&'.utf8_decode(http_build_query($updatePostData));

    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $result = trim($result);
    $result = explode(';', $result);

    if ('+OK' !== $result[0]) {
        var_dump('Failed to update product', $result, $clientid);

        return;
    }

    $changeContractData = [
        'gesperrt' => 0,
        'kundennummer_extern' => $updatePostData['kundennummer_extern'],
    ];

    //////////
    $query = $db->query("SELECT `purtel_login` FROM `purtel_account` WHERE `cust_id` = ".$customerId);

    while ($purtelAccount = $query->fetch_assoc()) {
        echo sprintf("\tactivate %s\n", $purtelAccount['purtel_login']);

        $changeContractData['anschluss'] = $purtelAccount['purtel_login'];

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($purtelSuperUsername),
            urlencode($purtelSuperPassword),
            'changecontract'
        );
        
        $url .= '&'.utf8_decode(http_build_query($changeContractData));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $result = trim($result);
        $result = explode(';', $result);

        if ('+OK' !== $result[0]) {
            var_dump('Failed to activate product', $result, $clientId);
        }
    }
}

/**
 * update buchung at purtel
 */
function updateBuchung($verwendungszweck, $customerId, $buchungsId, $price, $datum)
{
    global $db;

    $purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
    $purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

    $query = $db->query("SELECT `purtel_login` FROM `purtel_account` WHERE `master` = 1 AND `cust_id` = ".$customerId);

    if ($query->num_rows !== 1) {
        die(var_dump($query));
    }

    $purtelAccount = $query->fetch_assoc();

    $updatePostData = [
        'anschluss' => $purtelAccount['purtel_login'],
        'verwendungszweck' => $verwendungszweck,
        'betrag_soll' => $price,
        'aendern' => 1,
        'id' => $buchungsId,
        'datum' => $datum,
    ];

    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'setbuchung'
    );
    
    $url .= '&'.utf8_decode(http_build_query($updatePostData));

    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));

    $result = curl_exec($curl);

    curl_close($curl);

    $result = trim($result);
    $result = explode(';', $result);

    if ('+OK' !== $result[0]) {
        var_dump('Failed to update buchung', $result, $customerId);

        return;
    }
}

/**
 * lösche buchung at purtel
 */
function deleteBuchung($customerId, $buchungsId, $verwendungszweck)
{
    global $db;

    $purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
    $purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

    $query = $db->query("SELECT `purtel_login` FROM `purtel_account` WHERE `master` = 1 AND `cust_id` = ".$customerId);

    if ($query->num_rows !== 1) {
        die(var_dump($query));
    }

    $purtelAccount = $query->fetch_assoc();

    $updatePostData = [
        'anschluss' => $purtelAccount['purtel_login'],
        'verwendungszweck' => $verwendungszweck,
        'loeschen' => 1,
        'id' => $buchungsId,
        //'betrag_brutto_netto' => 'netto',
    ];

    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'setbuchung'
    );
    
    $url .= '&'.utf8_decode(http_build_query($updatePostData));

    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));

    $result = curl_exec($curl);

    curl_close($curl);

    $result = trim($result);
    $result = explode(';', $result);

    if ('+OK' !== $result[0]) {
        var_dump('Failed to delete buchung', $result, $customerId);

        return;
    }
}


$timeZone = new \DateTimeZone('Europe/Berlin');
$today = new \DateTime('now', $timeZone);

// hole alle Hauptprodukte (+ notwendige Kundendaten), die momentan aktiviert sein sollten
$q = $db->query("SELECT cp.*, p.`type`, p.`name`, p.`purtel_product_id`, c.`customer_id` as dimari, c.`clientid`, p.`identifier`
    FROM `customer_products` cp 
    LEFT JOIN `products` p ON p.`id` = cp.`product_id` 
    LEFT JOIN `customers` c ON c.`id` = cp.`customer_id` 
    WHERE p.`type` = 'Tarif' AND (cp.`deactivation_date` IS NULL OR cp.`deactivation_date` > NOW()) AND (cp.`activation_date` IS NULL OR cp.`activation_date` < NOW())");

$dataByCustomer = [];

// gruppiere sql-ergebnisse nach customer.id
while ($d = $q->fetch_assoc()) {
    if (!isset($dataByCustomer[$d['customer_id']])) {
        $dataByCustomer[$d['customer_id']] = [];
    }

    $dataByCustomer[$d['customer_id']][] = $d;
}

$multipleProducts = [];

foreach ($dataByCustomer as $customerId => $produktsHeShouldHave) {
    if (count($produktsHeShouldHave) > 1) {
        // kunde hat mehr als ein Produkt
        // -> das verarbeiten wir erst später
        $multipleProducts[$customerId] = $produktsHeShouldHave;

        continue;
    }

    //echo "hat nur 1\n";

    $theProduktHeShouldHave = $produktsHeShouldHave[0];

    // hole aktuelles produkt des kunden aus ikv (das Falsche)
    $customersCurrentIkvProductQ = $db->query("SELECT c.`purtel_product`, p.`name`, p.`purtel_product_id` 
        FROM `customers` c 
        LEFT JOIN `products` p ON p.`identifier` = REPLACE(c.`purtel_product`, 'Dimari-', '') 
        WHERE c.`id` = ".$customerId);

    if ($customersCurrentIkvProductQ->num_rows !== 1) {
        // komischerweise gibt es diesen Kunden mehr oder weniger als ein mal
        die(var_dump("invalid amount of customers", $customerId, $customersCurrentIkvProductQ));
    }

    $customersCurrentIkvProduct = $customersCurrentIkvProductQ->fetch_assoc();
    $customersCurrentIkvProductIdentifier = preg_replace('/^Dimari-/', '', $customersCurrentIkvProduct['purtel_product']);

    if ($theProduktHeShouldHave['identifier'] === $customersCurrentIkvProductIdentifier) {
        // das Produkt, dass er haben sollte, ist das gleiche wie das, dass er tatsächlich (in der ikv) hat
        // nichts mehr zu tun

        // sicherheitshaber noch mal richtig setzten, falls mehrere datensätze mit der gleichen
        // product_id vorhanden sind und doch welche bereits deactiviert sein sollten
        $db->query("UPDATE `customer_products` 
            SET `propperly_copyed_to_customer` = 0 
            WHERE `propperly_copyed_to_customer` = 1 AND `customer_id` = ".$theProduktHeShouldHave['customer_id']);

        if (!empty($db->error)) {
            die(var_dump($db->error));
        }

        $db->query("UPDATE `customer_products` 
            SET `propperly_copyed_to_customer` = 1 
            WHERE `id` = ".$theProduktHeShouldHave['id']." 
            AND `customer_id` = ".$theProduktHeShouldHave['customer_id']);

        if (!empty($db->error)) {
            die(var_dump($db->error));
        }

        continue; // alles stimmt
    }

    // Kunde hat ein anderes Haupt-Produkt, als das, dass er haben sollte ..

    // prüfen wir zuerst, ob der Datensatz des Kunden bereits von jemand anderem manuell verändert wurde.
    // dazu müssen wir herausfinden welches "falsche" Produkt er momentan haben müsste
    // und dann vergleichen, ob er das auch tatsächlich noch hat

    // welches falsche müsste er gerade haben?
    // hole alle Haupt-Produkte, die dem Kunden breits übertragen wurden
    $q2 = $db->query("SELECT cp.*, p.`type`, p.`name`, c.`customer_id` as dimari, c.`clientid`, p.`identifier`
        FROM `customer_products` cp 
        LEFT JOIN `products` p ON p.`id` = cp.`product_id` 
        LEFT JOIN `customers` c ON c.`id` = cp.`customer_id` 
        WHERE p.`type` = 'Tarif' AND cp.`propperly_copyed_to_customer` = 1 AND cp.`customer_id` = ".$customerId);

    if ($q2->num_rows < 1) {
        die(var_dump("der Kunde hat komischerweise noch nie ein hautprodukt bekommen", $customerId));
    }

    $found = false;
    $expectedProducts = [];

    // Hat er das, dass er hätte haben müssen?

    // Vergleiche das Haupt-Produkt, dass er momentan hat,
    // mit denen, die ihm übertragen wurden.

    // Wenn der Kunde nicht manuell geändert wurde,
    // dann muss eines der übertragenen Produkte das gleiche wie sein momentanes Haupt-Produkt sein
    while ($d2 = $q2->fetch_assoc()) {        
        if (preg_replace('/^Dimari-/', '', $d2['identifier']) === $customersCurrentIkvProductIdentifier) {
            $found = true;

            break;
        }

        $expectedProducts[] = $d2['name'];
    }

    if ($found) {
        // ja, das erwartete falsche produkt ist immer noch beim kunden hinterlegt
        echo sprintf("Kunde (%s) Produkt (%s) nach (%s) aendern\n",
            $theProduktHeShouldHave['dimari'],
            $customersCurrentIkvProduct['name'],
            $theProduktHeShouldHave['name']
        );

        // es sollte noch geprüft werden, ob bei purtel auch noch das gleiche "falsche" Haupt-Produkt aktiv ist
            // wenn ja, darf dieser datensatz geändert werden
            // wenn nein,
                // sollte geprüft werden, ob bei purtel bereits das echte richtige produkt aktiv ist
                    // falls ja, dann das produkt in ikv übernehmen
                    // falls nein, anzeigen und nichts mehr tun

        $currentPurtelProducts = getCurrentPurtelProducts($theProduktHeShouldHave['clientid']);

        $currentPurtelProduct = null;

        foreach ($currentPurtelProducts as $array) {
            if (null === $currentPurtelProduct) {
                $currentPurtelProduct = $array[4];
                continue;
            }

            if ($currentPurtelProduct !== $array[4]) {
                trigger_error(sprintf('not same product for all accounts (%s) - %s & %s - customer (%s)',
                    $array[1],
                    $currentPurtelProduct,
                    $array[4],
                    $theProduktHeShouldHave['dimari']
                ), E_USER_NOTICE);

                continue 2;
            }
        }

        if ($customersCurrentIkvProduct['purtel_product_id'] !== $currentPurtelProduct) {
            if ($theProduktHeShouldHave['purtel_product_id'] === $currentPurtelProduct) {
                echo sprintf("Der Kunde (%s) hat ein anderes Produkt bei Purtel, aber das ist genau das, dass er haben sollte\n",
                    $theProduktHeShouldHave['dimari']
                );

                // - update produkt in ikv
                $db->query("UPDATE `customers` SET 
                    `productname` = 'Dimari-".$theProduktHeShouldHave['identifier']."', 
                    `purtel_product` = 'Dimari-".$theProduktHeShouldHave['identifier']."' 
                    WHERE `id` = ".$theProduktHeShouldHave['customer_id']);

                if (!empty($db->error)) {
                    die(var_dump($db->error));
                }
                // - update customer_products
                $db->query("UPDATE `customer_products` 
                    SET `propperly_copyed_to_customer` = 0 
                    WHERE `propperly_copyed_to_customer` = 1 AND `customer_id` = ".$theProduktHeShouldHave['customer_id']);

                if (!empty($db->error)) {
                    die(var_dump($db->error));
                }

                $db->query("UPDATE `customer_products` 
                    SET `propperly_copyed_to_customer` = 1 
                    WHERE `id` = ".$theProduktHeShouldHave['id']." 
                    AND `customer_id` = ".$theProduktHeShouldHave['customer_id']);

                if (!empty($db->error)) {
                    die(var_dump($db->error));
                }

                echo "done\n";
            } else {
                if ('4578' !== $currentPurtelProduct) { // fehler für leistungsfrei momentan ausblenden, da cluster 20 usw. nicht fertig
                    trigger_error(sprintf('customer (%s) has different product at purtel - purtel (%s) - ikv (%s) - actuall correct product (%s - %s)',
                        $theProduktHeShouldHave['dimari'],
                        $currentPurtelProduct,
                        $customersCurrentIkvProduct['purtel_product_id'],
                        $theProduktHeShouldHave['purtel_product_id'],
                        $theProduktHeShouldHave['name']
                    ), E_USER_NOTICE);
                } else {
                    echo "lf-produkt .. ignorieren..\n";

                    // setze das richtige produkt bei purtel
                    // script muss ein zweites mal laufen um auch die customer_products und customers zu korrigieren
                    /*updateProductAtPurtel(
                        $theProduktHeShouldHave['clientid'],
                        $theProduktHeShouldHave['purtel_product_id'],
                        $theProduktHeShouldHave['customer_id']
                    );*/
                }

                continue;
            }
        } else {
            // else bedeutet, dass auch bei purtel das gleiche "falsche" produkt aktiv ist.

            // wenn das neue "korrekte" Produkt die gleiche purtel_product_id hat,
            // dann muss nur in der ikv geändert werden

            if ($customersCurrentIkvProduct['purtel_product_id'] === $theProduktHeShouldHave['purtel_product_id']) {
                echo sprintf("Kunde (%s) produkt nur in ikv ändern - neues produkt hat selbe id wie altes\n",
                    $theProduktHeShouldHave['dimari']
                );

                // - update produkt in ikv
                $db->query("UPDATE `customers` SET 
                    `productname` = 'Dimari-".$theProduktHeShouldHave['identifier']."', 
                    `purtel_product` = 'Dimari-".$theProduktHeShouldHave['identifier']."' 
                    WHERE `id` = ".$theProduktHeShouldHave['customer_id']);

                if (!empty($db->error)) {
                    die(var_dump($db->error));
                }
                // - update customer_products
                $db->query("UPDATE `customer_products` 
                    SET `propperly_copyed_to_customer` = 0 
                    WHERE `propperly_copyed_to_customer` = 1 AND `customer_id` = ".$theProduktHeShouldHave['customer_id']);

                if (!empty($db->error)) {
                    die(var_dump($db->error));
                }

                $db->query("UPDATE `customer_products` 
                    SET `propperly_copyed_to_customer` = 1 
                    WHERE `id` = ".$theProduktHeShouldHave['id']." 
                    AND `customer_id` = ".$theProduktHeShouldHave['customer_id']);

                if (!empty($db->error)) {
                    die(var_dump($db->error));
                }

                echo "done\n";
            } else {
                echo sprintf("Kunde (%s) produkt auch bei purtel ändern\n",
                    $theProduktHeShouldHave['dimari']
                );

                // finde passende buchung
                if (!isset($productsFromPurtel[$customersCurrentIkvProduct['purtel_product_id']])) {
                    die("can not find name of product");
                }

                $purtelProduct = $productsFromPurtel[$customersCurrentIkvProduct['purtel_product_id']];
                $productNameAtPurtel = $purtelProduct['name'];
                $buchungen = getBuchungen($theProduktHeShouldHave['clientid']);
                
                if (!empty($buchungen)) {
                    $foundBuchung = null;

                    foreach ($buchungen as $buchung) {
                        if ('Basisentgelt '.$productNameAtPurtel === $buchung[3]) {
                            $foundBuchung = true;

                            // - update buchung
                            updateBuchung(
                                'Basisentgelt '.$productsFromPurtel[$theProduktHeShouldHave['purtel_product_id']]['name'],
                                $theProduktHeShouldHave['customer_id'],
                                $buchung[0],
                                $productsFromPurtel[$theProduktHeShouldHave['purtel_product_id']]['price'],
                                $buchung[7]
                            );
                        } elseif ($productNameAtPurtel.' Endabrechnung wegen Tarifwechsel' === $buchung[3]) {
                            deleteBuchung(
                                $theProduktHeShouldHave['customer_id'],
                                $buchung[0],
                                $productNameAtPurtel.' Endabrechnung wegen Tarifwechsel'
                            );
                        }
                    }

                    if (null === $foundBuchung) {
                        foreach ($buchungen as $buchung) {
                            echo "\033[1;33m - ".$buchung[3]."\033[0m\n";
                        }
                        
                        echo "\033[1;31mBuchung nicht gefunden!!!\033[0m\n";

                        echo "\033[0;33maborting? - waiting ...\033[0m";

                        for ($i = 5; $i > 0; $i--) {
                            echo "\033[1;33m ".$i.",\033[0m";
                            sleep(1);
                        }

                        echo "\n";
                    }
                }

                // 7 => datum
                // 1 => id
                //verwendungszweck
echo "\n";
var_dump('current ikv and purtel', $customersCurrentIkvProduct['purtel_product_id']);
var_dump('theProduktHeShouldHave', $theProduktHeShouldHave);

                // - update produkt bei purtel
                updateProductAtPurtel(
                    $theProduktHeShouldHave['clientid'],
                    $theProduktHeShouldHave['purtel_product_id'],
                    $theProduktHeShouldHave['customer_id']
                );
                
                // - update produkt in ikv
                $db->query("UPDATE `customers` SET 
                    `productname` = 'Dimari-".$theProduktHeShouldHave['identifier']."', 
                    `purtel_product` = 'Dimari-".$theProduktHeShouldHave['identifier']."' 
                    WHERE `id` = ".$theProduktHeShouldHave['customer_id']);

                if (!empty($db->error)) {
                    die(var_dump($db->error));
                }
                // - update customer_products
                $db->query("UPDATE `customer_products` 
                    SET `propperly_copyed_to_customer` = 0 
                    WHERE `propperly_copyed_to_customer` = 1 AND `customer_id` = ".$theProduktHeShouldHave['customer_id']);

                if (!empty($db->error)) {
                    die(var_dump($db->error));
                }

                $db->query("UPDATE `customer_products` 
                    SET `propperly_copyed_to_customer` = 1 
                    WHERE `id` = ".$theProduktHeShouldHave['id']." 
                    AND `customer_id` = ".$theProduktHeShouldHave['customer_id']);

                if (!empty($db->error)) {
                    die(var_dump($db->error));
                }


                echo "\n\n\n\n\n";
                echo "\033[1;34msleeping for\033[0m";

                for ($i = 5; $i > 0; $i--) {
                    echo "\033[1;34m ".$i.",\033[0m";
                    sleep(1);
                }

                echo "\n";
            }
        }
    } else {
        // ne, kunde hat total anderes produkt
        // hier war wohl bereits jemand von Hand tätig
        trigger_error(sprintf('Kunde (%s) was changed - has product (%s) - expected one of (%s)',
            $theProduktHeShouldHave['dimari'],
            $customersCurrentIkvProduct['name'],
            implode(' , ', $expectedProducts)
        ), E_USER_NOTICE);

        // korrigiere customer_products, so dass wenigstens die cross-sellings generiert werden können.
        // info mit möglichen falschen produkten sollte an comin gehen
        // - update customer_products
        /*$db->query("UPDATE `customer_products` 
            SET `propperly_copyed_to_customer` = 0 
            WHERE `propperly_copyed_to_customer` = 1 AND `customer_id` = ".$theProduktHeShouldHave['customer_id']);

        if (!empty($db->error)) {
            die(var_dump($db->error));
        }

        $db->query("UPDATE `customer_products` 
            SET `propperly_copyed_to_customer` = 1 
            WHERE `id` = ".$theProduktHeShouldHave['id']." 
            AND `customer_id` = ".$theProduktHeShouldHave['customer_id']);

        if (!empty($db->error)) {
            die(var_dump($db->error));
        }*/
    }
}




// spezialfälle
foreach ($multipleProducts as $customerId => $produktsHeShouldHave) {
    echo $customerId." :\n";

    foreach ($produktsHeShouldHave as $a) {
        echo "\t".$a['dimari']." / ".$a['clientid']." - ".$a['name']."\n";

        /*if ($a['propperly_copyed_to_customer'] == '0') {
            echo "\tDELETE FROM customer_products WHERE `id` = ".$a['id']."\n";
            //$db->query("DELETE FROM customer_products WHERE `id` = ".$a['id']);
        }*/
    }
}

