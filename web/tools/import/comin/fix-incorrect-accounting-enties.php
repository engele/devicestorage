<?php

/*
 * Korrigiere falsche Buchungen (Cluster 33)
 */

require __DIR__.'/../../../_conf/database.inc';
require_once __DIR__.'/../../../vendor/wisotel/configuration/Configuration.php';

/**
 * Get buchungen for all accounts by kundennummer_extern
 */
function getBuchungen($clientId)
{
    $purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
    $purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

    $kundennummer_extern = $clientId;

    $now = new \DateTime();

    $parameter = [
        'kundennummer_extern' => $kundennummer_extern,
        'von_datum' => '20180101',
        'bis_datum' => $now->format('Ymd'),
        //'erweitert' => '2',
    ];

    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'getbuchungen'
    );
    
    $url .= '&'.utf8_decode(http_build_query($parameter));

    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $content = trim($result);
    
    $delimiter = null;

    foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
        if (false !== strpos($content, $delimiter_)) {
            $delimiter = $delimiter_;
            break;
        }
    }

    if (null == $delimiter) {
        var_dump($content);
        trigger_error(sprintf("\033[1;31mdelemiter not found - customer (%s)\033[0m",
            $clientId
        ), E_USER_NOTICE);

        return null;
    }

    $result = array_map(function ($line) {
        $data = str_getcsv($line, ';');

        foreach ($data as $key => $value) {
            if (is_scalar($value)) {
                if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                    $data[$key] = substr($value, 1, -1);
                } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                    $data[$key] = substr($value, 1, -1);
                }
            }
        }

        return $data;
    }, explode($delimiter, $content));

    unset($result[0]);

    return $result;
}

$q = $db->query("SELECT c.* FROM `customers` c WHERE c.`location_id` = 24 AND c.`clientid` LIKE '50000.17.%'");

while ($customer = $q->fetch_assoc()) {
   $buchungen = getBuchungen($customer['clientid']);

   var_dump($buchungen);
   die();
}
