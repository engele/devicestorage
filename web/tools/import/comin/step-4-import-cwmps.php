<?php

/**
 * Import CWMPs to customers
 */

require __DIR__.'/../CsvReader.php';
require __DIR__.'/../Customer.php';
require __DIR__.'/../Mapper.php';
require __DIR__.'/../Exception/CustomerAlreadyExistsInDatabaseException.php';
require __DIR__.'/../../../_conf/database.inc';

// load dimari export file
$dimariCsv = new CsvReader('/tmp/CWMP_CL3_Mitarbeiter1_neu.csv');

class Notifications
{
    public static $globallyOn = false;
    public static $globallyOff = false;
    public static $noCwmp = true;
    public static $setExternalCustomerId = false;
    public static $customerNotWithinLastImportedData = true;

    public static function isActive($element)
    {
        if (self::$globallyOn) {
            return true;
        }

        if (self::$globallyOff) {
            return false;
        }

        return self::${$element};
    }
}

// to simplify things, group the csv entries by customer-id
$dataGroupedByCustomerId = [];
$customerIdColumn = $dimariCsv->headlineReversed['KUNDENNUMMER'];

foreach ($dimariCsv->data as $key => $line) {
    $customerId = trim($line[$customerIdColumn]);

    if (!isset($dataGroupedByCustomerId[$customerId])) {
        $dataGroupedByCustomerId[$customerId] = [];
    }

    $dataGroupedByCustomerId[$customerId][$key] = & $dimariCsv->data[$key]; // use reference 
}

// set mapper methods
// this is used to filter/verify data for object
// each mapper method must return an array which is then used as arguments for call_uer_func_array()
$toCustomerMapper = new Mapper();

/**
 * Mapper for customer.customer_id
 */
$toCustomerMapper->toExternalCustomerId(function (array $lines) use ($dimariCsv, $db) {
    $line = current($lines);
    $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

    // see if a customer with same customer_id already exists in database
    $findCustomerInDbQuery = $db->query("SELECT `id`, `clientid` FROM `customers` WHERE `customer_id` = ".$customerId);

    if ($findCustomerInDbQuery->num_rows > 0) {
        // a customer with same customer_id already exists in database!
        $customerDataFromDb = $findCustomerInDbQuery->fetch_assoc();

        throw new CustomerAlreadyExistsInDatabaseException(sprintf('Data for KUNDENNUMMER = %s already exists in database - %s (%s)',
            $customerId,
            $customerDataFromDb['clientid'],
            $customerDataFromDb['id']
        ), $customerDataFromDb['id']);
    }

    return [ $customerId ];
});

/**
 * Mapper for customer.mac_address
 */
$toCustomerMapper->toMacAddress(function (array $lines) use ($dimariCsv) {
    $cwmp = null;

    foreach ($lines as $line) {
        //$cwmp_ = trim($line[$dimariCsv->headlineReversed['Fritz Box Seriennr']]);
        $cwmp_ = trim($line[$dimariCsv->headlineReversed['CWMP ID']]);

        if (empty($cwmp_)) {
            continue;
        }

        $matchMac = null;

        preg_match('/([0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2})|([0-9a-f]{12})/i', $cwmp_, $matchMac);

        if (isset($matchMac[0])) {
            $cwmp = preg_replace('/[^0-9a-f]/i', '', $matchMac[0]);

            break; // first match is ok
        }
    }

    if (null === $cwmp) {
        $line = current($lines);
        $customerId = trim($line[$dimariCsv->headlineReversed['KUNDENNUMMER']]);

        if (Notifications::isActive('noCwmp')) {
            trigger_error(sprintf('customer (%s) has no cwmp',
                $customerId
            ), E_USER_NOTICE);
        }
    }

    return [ $cwmp ];
});






















$customers = [];

// map customer csv lines to a single customer object
foreach ($dataGroupedByCustomerId as $customerId => $lines) {
    $customer = new Customer();
    $toCustomerMapper->setObject($customer);

    try {
        $toCustomerMapper->setExternalCustomerId($lines);
    } catch (CustomerAlreadyExistsInDatabaseException $exception) {
        $customer->isNew = false;
        $customer->id = $exception->getCode();

        if (Notifications::isActive('setExternalCustomerId')) {
            trigger_error($exception->getMessage(), E_USER_NOTICE);
        }
    }

    try {
        // do all the simple data mapping
        $toCustomerMapper->setMacAddress($lines);
    } catch (\UnexpectedValueException $exception) {
        throw new \UnexpectedValueException(sprintf('%s - customer (%s)',
            $exception->getMessage(),
            $customerId
        ));
    }

    $customers[] = $customer;
}


// import customers
foreach ($customers as $customer) {
    if ($customer->isNew) {
        // don't import new customers
        trigger_error(sprintf('Can not import data for (%s) because customer does not exist in database',
            $customer->externalCustomerId
        ), E_USER_NOTICE);

        continue;
    }

    if ($customer->id < 8680 || $customer->id > 8916) {
        if (Notifications::isActive('customerNotWithinLastImportedData')) {
            trigger_error(sprintf('Customer (%s) is not within last imported data',
                $customer->id
            ), E_USER_NOTICE);
        }

        continue; // only allow to add data to customers from last import
    }

    echo sprintf("importing data for %s\n", $customer->id);

    if (!empty($customer->macAddress)) {
        $db->query(sprintf("UPDATE `customers` SET `mac_address` = '%s' WHERE `id` = %s",
            $customer->macAddress,
            $customer->id
        ));

        if (!empty($db->error)) {
            trigger_error($db->error, E_USER_NOTICE);
        }
    }
}

echo "done\n";
