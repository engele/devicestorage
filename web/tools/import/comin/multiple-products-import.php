<?php

require __DIR__.'/../CsvReader.php';
require __DIR__.'/../../../_conf/database.inc';

$csv = new CsvReader('/tmp/Multiple-Produkte-1.csv');

// Dimari-Kundennummer  ikv-Kundennummer    interne-id  ist Hauptprodukt    Preis (netto)   Aktivierungsdatum   Purtel-Produkt-id   Crossselling    Cp-anzahl

// trim($line[$csv->headlineReversed[$columnHeadlineName]]);

$dataByCustomerId = [];

foreach ($csv->data as $key => $line) {
    $customerId = trim($line[$csv->headlineReversed['interne-id']]);

    if (isset($dataByCustomerId[$customerId])) {
        die(var_dump('doppelt', $customerId));
    }

    $dataByCustomerId[$customerId] = $line;
}

$useProductForCustomer = [
    /*'8680' => '43',
    '8179' => '43',
    '8235' => '43',
    '8208' => '83',*/

    '5142' => '43',
    '5152' => '83',
];

$skipUntil = 9325;

foreach ($dataByCustomerId as $customerId => $line) {
    if (!empty($skipUntil)) {

        if ($customerId !== $skipUntil) {
            echo "skipped - ".$customerId."\n";
            continue;
        } else {
            $skipUntil = null;
        }
    }

    $purtelProductId = trim($line[$csv->headlineReversed['Purtel-Produkt-id']]);

    $productQ = $db->query("SELECT * FROM `products` WHERE `purtel_product_id` = ".$purtelProductId);

    if ($productQ->num_rows !== 1) {
        if (isset($useProductForCustomer[$purtelProductId])) {
            $productQ = $db->query("SELECT * FROM `products` WHERE `purtel_product_id` = ".$purtelProductId." AND `id` = ".$useProductForCustomer[$purtelProductId]);
        } else {
            while ($p = $productQ->fetch_assoc()) {
                var_dump($p);
            }
            die(var_dump('invalid amount products', $productQ->num_rows, $purtelProductId, $customerId));
        }
    }

    $product = $productQ->fetch_assoc();

    echo $customerId."\n";
    //$q = $db->query("SELECT * FROM customer_products WHERE customer_id = ".$customerId);
    $q = $db->query("SELECT cp.*, p.`type`, p.`name`, p.`purtel_product_id`, c.`customer_id` as dimari, c.`clientid`, p.`identifier`
        FROM `customer_products` cp 
        LEFT JOIN `products` p ON p.`id` = cp.`product_id` 
        LEFT JOIN `customers` c ON c.`id` = cp.`customer_id` 
        WHERE cp.`customer_id` = ".$customerId." AND p.`type` = 'Tarif' AND (cp.`deactivation_date` IS NULL OR cp.`deactivation_date` > NOW()) AND (cp.`activation_date` IS NULL OR cp.`activation_date` < NOW())");


    while ($d = $q->fetch_assoc()) {
        var_dump($d);

        echo "UPDATE `customer_products` SET `deactivation_date` = '2018-04-16' WHERE `id` = ".$d['id']."\n";

        $db->query("UPDATE `customer_products` SET `deactivation_date` = '2018-04-16' WHERE `id` = ".$d['id']);

        if (!empty($db->error)) {
            die(var_dump($db->error));
        }
    }
    echo "\n";

    $price = empty($product['default_price_net']) ? 'NULL' : $product['default_price_net'];

    echo "INSERT INTO `customer_products` (`product_id`, `customer_id`, `price_net`, `activation_date`, `deactivation_date`, `propperly_copyed_to_customer`) 
        VALUES (".$product['id'].", ".$customerId.", ".$price.", '2018-04-16', NULL, 0)\n";

    $db->query("INSERT INTO `customer_products` (`product_id`, `customer_id`, `price_net`, `activation_date`, `deactivation_date`, `propperly_copyed_to_customer`) 
        VALUES (".$product['id'].", ".$customerId.", ".$price.", '2018-04-16', NULL, 0)");

    if (!empty($db->error)) {
        die(var_dump($db->error));
    }
}
