<?php

// require __DIR__.'/../../../_conf/database.inc';
require_once __DIR__.'/../../../vendor/wisotel/configuration/Configuration.php';

/**
 * Get buchungen for all accounts by kundennummer_extern
 */
function getBuchungen($clientId)
{
    $purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
    $purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

    $kundennummer_extern = $clientId;

    $parameter = [
        //'kundennummer_extern' => $kundennummer_extern,
        'von_datum' => '20100101',
        'bis_datum' => '20180703',
        'erweitert' => '6',
    ];

    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'getbuchungen'
    );
    
    $url .= '&'.utf8_decode(http_build_query($parameter));

    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $content = trim($result);
    
    $delimiter = null;

    foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
        if (false !== strpos($content, $delimiter_)) {
            $delimiter = $delimiter_;
            break;
        }
    }

    if (null == $delimiter) {
        throw new Exception('keine Buchungen gefunden');
    }

    $result = array_map(function ($line) {
        $data = str_getcsv($line, ';');

        foreach ($data as $key => $value) {
            if (is_scalar($value)) {
                if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                    $data[$key] = substr($value, 1, -1);
                } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                    $data[$key] = substr($value, 1, -1);
                }
            }
        }

        return $data;
    }, explode($delimiter, $content));

    return $result;
}

/**
 * update buchung at purtel
 * 
 * @return boolean
 */
function updateBuchung($buchung)
{
    $updatePostData = [
        //'betrag_soll_bruttobasiert' => $buchung[26],
        //'betrag_haben_bruttobasiert' => $buchung[27],
        'anschluss' => $buchung[2],
        'verwendungszweck' => $buchung[3],
        'aendern' => 1,
        'id' => $buchung[0],
        'datum' => $buchung[7],
        //'sap_buchungskonto' => $buchung[23],
    ];

    if (isset($buchung[26])) {
        $updatePostData['betrag_soll_bruttobasiert'] = $buchung[26];
    } else {
        $updatePostData['betrag_haben_bruttobasiert'] = $buchung[27];
    }

    /*if (isset($buchung[5])) {
        $updatePostData['betrag_haben'] = $buchung[5];
    } else {
        $updatePostData['betrag_soll'] = $buchung[4];
    }*/

    $purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
    $purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'setbuchung'
    );
    
    $url .= '&'.utf8_decode(http_build_query($updatePostData));

    //echo "transmitted data to update buchung:\n\t".http_build_query($updatePostData)."\n";

    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));

    $result = curl_exec($curl);

    curl_close($curl);

    $result = trim($result);
    $result = explode(';', $result);

    if ('+OK' === $result[0]) {
        return true;
    }

    var_dump('Failed to update buchung', $result);

    return false;
}

/*
array(28) {
    [0]=>
    string(2) "id"
    [1]=>
    string(7) "mandant"
    [2]=>
    string(9) "anschluss"
    [3]=>
    string(16) "verwendungszweck"
    [4]=>
    string(11) "betrag_soll"
    [5]=>
    string(12) "betrag_haben"
    [6]=>
    string(8) "erledigt"
    [7]=>
    string(5) "datum"
    [8]=>
    string(8) "renummer"
    [9]=>
    string(19) "kundennummer_extern"
    [10]=>
    string(11) "mitarbeiter"
    [11]=>
    string(6) "steuer"
    [12]=>
    string(3) "art"
    [13]=>
    string(10) "unterkonto"
    [14]=>
    string(10) "userfield1"
    [15]=>
    string(10) "userfield2"
    [16]=>
    string(10) "userfield3"
    [17]=>
    string(29) "kundennummer_extern_anschluss"
    [18]=>
    string(9) "kundenart"
    [19]=>
    string(10) "entgelttyp"
    [20]=>
    string(8) "reseller"
    [21]=>
    string(20) "betrag_soll_reseller"
    [22]=>
    string(21) "betrag_haben_reseller"
    [23]=>
    string(17) "sap_buchungskonto"
    [24]=>
    string(24) "betrag_soll_nettobasiert"
    [25]=>
    string(25) "betrag_haben_nettobasiert"
    [26]=>
    string(25) "betrag_soll_bruttobasiert"
    [27]=>
    string(26) "betrag_haben_bruttobasiert"
}
*/



$allBuchungen = getBuchungen(null);
die(var_dump($allBuchungen));
$headline = array_shift($buchungen);

foreach ($allBuchungen as $buchung) {
    if (!empty($buchung[6])) {
        continue;
    }

    $clientId = $buchung[9];
    $purtelAccount = $buchung[2];
    $verwendungszweck = $buchung[3];

    if (!empty($buchung[26]) || !empty($buchung[26])) {
        var_dump($clientId, $purtelAccount, $verwendungszweck);
        echo "betrag_soll_bruttobasiert - betrag_haben_bruttobasiert eines davon ist bereits befüllt - continue\n";
        continue;
    }

    if (!empty($buchung[4])) { // betrag_soll
        //var_dump($buchung[4]);
        $buchung[26] = round($buchung[4], 0); // betrag_soll_bruttobasiert
        unset($buchung[27]);
    } elseif (!empty($buchung[5])) { // betrag_haben
        //var_dump($buchung[5]);
        $buchung[27] = round($buchung[5], 0); // betrag_haben_bruttobasiert
        unset($buchung[26]);
    } else {
        var_dump($clientId, $purtelAccount, $verwendungszweck);
        echo "tatsächlich 0 euro ?? - continue\n";
        continue;
    }

    if (!updateBuchung($buchung)) {
        var_dump('update', $clientId, $purtelAccount, $verwendungszweck);
        echo "failed - continue\n";
        continue;
    } else {
        echo "update - ok!\n";
    }
}

echo "all done\n";


/*
$alreadySeenClientId = [];

foreach ($purtelAccounts as $clientId) {
    echo "staring - ".$clientId."\n";

    if (isset($alreadySeenClientId[$clientId])) {
        echo "already seen client-id - ".$clientId." - continue\n";
        continue;
    }

    $alreadySeenClientId[$clientId] = true;

    $buchungen = getBuchungen($clientId);
    
    unset($buchungen[0]);

    foreach ($buchungen as $buchung) {
        // Todo - If erledigt? -> continue



        if (!empty($buchung[26]) || !empty($buchung[26])) {
            var_dump($buchung);
            echo "betrag_soll_bruttobasiert - betrag_haben_bruttobasiert eines davon ist bereits befüllt - continue\n";
            continue;
        }

        if (!empty($buchung[4])) { // betrag_soll
            var_dump($buchung[4]);
            $buchung[26] = round($buchung[4], 0); // betrag_soll_bruttobasiert
            unset($buchung[27]);
        } elseif (!empty($buchung[5])) { // betrag_haben
            var_dump($buchung[5]);
            $buchung[27] = round($buchung[5], 0); // betrag_haben_bruttobasiert
            unset($buchung[26]);
        } else {
            var_dump($buchung);
            echo "tatsächlich 0 euro ?? - continue\n";
            continue;
        }

        if (!updateBuchung($buchung)) {
            var_dump($buchung);
            echo "failed - continue\n";
            continue;
        }
    }
}

echo "all done\n";

*/