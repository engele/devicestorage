<?php
/*
$timeZone = new \DateTimeZone('Europe/Berlin');

$startAt = new \DateTime('2018-04-05 00:10:00', $timeZone);
$now = new \DateTime('now', $timeZone);

echo sprintf("starting at: %s - Now is: %s\n",
    $startAt->format('d.m.Y H:i:s'),
    $now->format('d.m.Y H:i:s')
);

while (($now = new \DateTime('now', $timeZone)) < $startAt) {
    echo sprintf("sleeping... %s - Now is: %s\n", $startAt->format('d.m.Y H:i:s'), $now->format('d.m.Y H:i:s'));
    sleep(600);
}
*/


define('WECHSELN', 2);
define('BUCHEN', 0);
define('ACTIVATE_ACCOUNTS', true);

require __DIR__.'/../../../_conf/database.inc';
require_once __DIR__.'/../../../vendor/wisotel/configuration/Configuration.php';

$purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

//$q = $db->query("SELECT * FROM `customers` WHERE `id` BETWEEN 6296 AND 7552 AND `location_id` = 25");
//$q = $db->query("SELECT * FROM `customers` WHERE `id` BETWEEN 6296 AND 7552");
$q = $db->query("SELECT * FROM `customers` WHERE `id` BETWEEN 16126 AND 16431");
//$q = $db->query("SELECT * FROM `customers` WHERE `clientid` LIKE '50000.17.%' AND `location_id` IN(24, 3, 17, 18, 25, 29)");

$ignoreCustomer = [
];

$onlyThisProducts = [
];

while ($customer = $q->fetch_assoc()) {
    if (in_array($customer['clientid'], $ignoreCustomer) || in_array($customer['id'], $ignoreCustomer)) {
        echo "skipped ".$customer['id']."\n";

        continue;
    }

    $purtelMaster = null;
    $purtelSubAccounts = [];

    $purtelAccountsQuery = $db->query("SELECT * FROM `purtel_account` WHERE `cust_id` = '".$customer['id']."'");

    while ($purtelAccount = $purtelAccountsQuery->fetch_assoc()) {
        if ('1' === $purtelAccount['master']) {
            if (null !== $purtelMaster) {
                trigger_error(sprintf('customer (%s) has multiple purtel master accounts - skipped customer',
                    $customer['id']
                ), E_USER_NOTICE);

                continue 2;
            }

            $purtelMaster = $purtelAccount;

            continue; // don't add master to $purtelSubAccounts
        }

        $purtelSubAccounts[] = $purtelAccount;
    }

    if (null === $purtelMaster) {
        trigger_error(sprintf('customer (%s) has no purtel master account - skipped customer',
            $customer['id']
        ), E_USER_NOTICE);

        continue;
    }

    if (empty($customer['purtel_product']) || 0 !== strpos($customer['purtel_product'], 'Dimari-')) {
        trigger_error(sprintf('customer (%s) has no product - skipped customer',
            $customer['id']
        ), E_USER_NOTICE);

        continue;
    }

    $productIdentifier = explode('Dimari-', $customer['purtel_product']);
    $productIdentifier = end($productIdentifier);

    $productQ = $db->query("SELECT * FROM `products` WHERE `identifier` = '".$productIdentifier."'");

    if ($productQ->num_rows > 1) {
        trigger_error(sprintf('this product (%s) exists multiple times in `products`-table - skipped customer (%s)',
            $productIdentifier,
            $customer['id']
        ), E_USER_NOTICE);

        continue;
    }

    $product = $productQ->fetch_assoc();

    if (empty($product['purtel_product_id'])) {
        trigger_error(sprintf('product (%s) has no `purtel_product_id` - skipped customer (%s)',
            $productIdentifier,
            $customer['id']
        ), E_USER_NOTICE);

        continue;
    }

    if (!empty($onlyThisProducts) && !in_array($product['purtel_product_id'], $onlyThisProducts)) {
        echo "skipped ".$customer['id']." -- because of product\n";

        continue;
    }

    echo "Kunde: ".$customer['id']."\n";

////////////////////////////////////////////////////////////////////////////////////////
/*    $kundennummer_extern = $customer['clientid'];

    if ($customer['purtel_use_version'] == 1 && $customer['version'] > 0) {
        $kundennummer_extern += '_'.$customer['version'];
    }

    $parameter = [
        'kundennummer_extern' => $kundennummer_extern,
        'erweitert' => 1
    ];

    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'getaccounts'
    );
    
    $url .= '&'.utf8_decode(http_build_query($parameter));

    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $content = trim($result);
    
    $delimiter = null;

    foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
        if (false !== strpos($content, $delimiter_)) {
            $delimiter = $delimiter_;
            break;
        }
    }

    if (null == $delimiter) {
        var_dump($content);
        trigger_error(sprintf('delemiter not found - customer (%s)',
            $customer['id']
        ), E_USER_NOTICE);

        continue;
    }

    $result = array_map(function ($line) {
        $data = str_getcsv($line, ';');

        foreach ($data as $key => $value) {
            if (is_scalar($value)) {
                if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                    $data[$key] = substr($value, 1, -1);
                } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                    $data[$key] = substr($value, 1, -1);
                }
            }
        }

        return $data;
    }, explode($delimiter, $content));

    unset($result[0]);

    $currentProduct = null;

    foreach ($result as $array) {
        if (null === $currentProduct) {
            $currentProduct = $array[4];
            continue;
        }

        if ($currentProduct !== $array[4]) {
            trigger_error(sprintf('not same product for all accounts (%s) - %s & %s - customer (%s)',
                $array[1],
                $currentProduct,
                $array[4],
                $customer['id']
            ), E_USER_NOTICE);

            continue 2;
        }
    }

    if ($product['purtel_product_id'] !== $currentProduct) {
        trigger_error(sprintf('customer (%s) has different product at purtel - purtel (%s) - ikv (%s)',
            $customer['id'],
            $currentProduct,
            $product['purtel_product_id']
        ), E_USER_NOTICE);

        continue;
    }
*/
    //var_dump($result);
//////////////////////////////////////////////////////////////////////////////////////7
//continue;
    // update product

    $updatePostData = [
        'wechseln' => WECHSELN,
        'buchen' => BUCHEN,
    ];

    $updatePostData['produkt'] = $product['purtel_product_id'];

    $updatePostData['kundennummer_extern'] = $customer['clientid'];

    if ($customer['purtel_use_version'] == 1 && $customer['version'] > 0) {
        $updatePostData['kundennummer_extern'] += '_'.$customer['version'];
    }

    // Optionale Angabe einen Anschlusses.
    // Ist dieser Wert gefüllt, wirkt sich die Änderung nur auf diesen Anschluss aus.
    // Die anderen Anschlüsse der externen Kundennummer bleiben unberührt.
    //$updatePostData['anschluss'] = $purtelMaster['purtel_login'];

    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'changeprodukt'
    );
    
    $url .= '&'.utf8_decode(http_build_query($updatePostData));

    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $result = trim($result);
    $result = explode(';', $result);

    if ('+OK' !== $result[0]) {
        var_dump('Failed to update sip', $result, $customer['id']);

        continue;
    }

    if (ACTIVATE_ACCOUNTS) {
        $changeContractData = [
            'gesperrt' => 0,
            'kundennummer_extern' => $updatePostData['kundennummer_extern'],
        ];

        foreach (array_merge($purtelSubAccounts, [$purtelMaster]) as $purtelAccount) {
            echo sprintf("\tactivate %s\n", $purtelAccount['purtel_login']);

            $changeContractData['anschluss'] = $purtelAccount['purtel_login'];

            $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
                urlencode($purtelSuperUsername),
                urlencode($purtelSuperPassword),
                'changecontract'
            );
            
            $url .= '&'.utf8_decode(http_build_query($changeContractData));

            $curl = curl_init($url);

            curl_setopt_array($curl, array(
                CURLOPT_HEADER          => false,
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_SSL_VERIFYPEER  => false,
            ));


            $result = curl_exec($curl);

            curl_close($curl);

            $result = trim($result);
            $result = explode(';', $result);

            if ('+OK' !== $result[0]) {
                var_dump('Failed to update sip', $result, $customer['id']);
            }
        }
    }
}
