<?php

$a = [
    '13897',
    '14048',
    '14083',
    '14203',
    '14307',
    '14376',
    '16065',
    '16490',
    '16536',
    '16617',
    '16699',
    '16718',
    '16764',
    '16833',
    '16902',
    '16962',
    '16963',
    '16984',
    '16994',
    '17000',
    '17006',
    '17011',
    '17024',
    '17035',
    '17056',
    '17057',
    '17059',
    '17060',
    '17061',
    '17063',
    '17064',
    '17065',
    '17066',
    '17067',
    '17068',
    '17069',
    '17070',
    '17071',
    '17072',
    '17073',
    '17074',
    '17075',
    '17076',
    '17077',
    '17078',
    '17079',
    '17080',
    '17081',
    '17082',
    '17083',
    '17084',
    '17085',
    '17086',
    '17087',
    '17088',
    '17089',
    '17090',
    '17091',
    '17092',
    '17093',
    '17094',
    '17096',
    '17097',
    '17098',
    '17099',
    '17100',
    '17101',
    '17102',
    '17103',
    '17104',
    '17105',
    '17106',
    '17107',
    '17108',
    '17109',
    '17110',
    '17111',
    '17112',
    '17113',
    '17114',
    '17115',
    '17116',
    '17117',
    '17118',
    '17119',
    '17120',
    '17121',
    '17122',
    '17123',
    '17124',
    '17125',
    '17126',
    '17127',
    '17128',
    '17129',
    '17130',
    '17131',
    '17132',
    '17134',
    '17135',
    '17136',
    '17137',
    '17138',
    '17139',
    '17140',
    '17141',
    '17142',
    '17143',
    '17144',
    '17146',
    '17147',
    '17148',
    '17149',
    '17150',
    '17151',
    '17152',
    '17153',
    '17154',
    '17155',
    '17156',
    '17157',
    '17158',
    '17159',
    '17160',
    '17161',
    '17162',
    '17163',
    '17164',
    '17165',
    '17166',
    '17167',
    '17168',
    '17169',
    '17170',
    '17171',
    '17172',
    '17173',
    '17174',
    '17175',
    '17176',
    '17177',
    '17178',
    '17179',
    '17180',
    '17181',
    '17182',
    '17183',
    '17184',
    '17185',
    '17186',
    '17187',
    '17188',
    '17189',
    '17191',
    '17192',
    '17193',
    '17194',
    '17195',
    '17196',
    '17197',
    '17198',
    '17199',
    '17200',
    '17201',
    '17202',
    '17203',
    '17204',
    '17205',
    '17206',
    '17207',
    '17208',
    '17209',
    '17210',
    '17212',
    '17213',
    '17214',
    '17216',
    '17217',
    '17218',
    '17219',
    '17220',
    '17221',
    '17222',
    '17223',
    '17224',
    '17225',
    '17226',
    '17227',
    '17228',
    '17229',
    '17230',
    '17231',
    '17232',
    '17233',
    '17234',
    '17235',
    '17236',
    '17237',
    '17238',
    '17239',
    '17240',
    '17241',
    '17242',
    '17243',
    '17244',
    '17245',
    '17246',
    '17247',
    '17248',
    '17249',
    '17250',
    '17251',
    '17252',
    '17253',
    '17254',
    '17255',
    '17256',
    '17257',
    '17258',
    '17259',
];

require __DIR__.'/../CsvReader.php';

$debitorenGesamtCsv = new CsvReader('/tmp/debitoren-gesamt.csv');

$csvHandle = fopen('php://output', 'w');

foreach ($a as $customerId) {
    $linesIndebitorenGesamt = $debitorenGesamtCsv->findLines(['Alte Kontonummer' => $customerId]);

    if (null === $linesIndebitorenGesamt) {
        continue;
    }

    if (count($linesIndebitorenGesamt) > 1) {
        echo "alte kontonummer (".$customerId.") ".count($linesIndebitorenGesamt)." mal in debitorenGesamtCsv gefunden - skipping\n";

        continue;
    }

    fputcsv($csvHandle, current($linesIndebitorenGesamt), ';', '"');
}

fclose($csvHandle);
