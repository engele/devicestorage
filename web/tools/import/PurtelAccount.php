<?php

/**
 * 
 */
class PurtelAccount
{
    /**
     * Do not set unless you are realy sure you know what you are doing!
     * 
     * @var integer
     */
    public $id;

    /**
     * @var boolean
     */
    public $isNew = true;

    /**
     * @var string
     */
    public $purtelLogin;

    /**
     * @var string
     */
    public $purtelPassword;

    /**
     * @var integer
     */
    public $master;

    /**
     * @var integer
     */
    public $porting;

    /**
     * @var string
     */
    public $nummer;

    /**
     * @var \DateTime
     */
    public $portingDate;

    /**
     * @var string
     */
    public $nummernblock;

    /**
     * @var string
     */
    public $areaCode;

    /**
     * @var integer
     */
    public $typ = 1;

    /**
     * @var Customer
     */
    public $customer;

    /**
     * @var integer
     */
    public $phoneBookInformation;

    /**
     * @var integer
     */
    public $phoneBookDigitalMedia;

    /**
     * @var integer
     */
    public $phoneBookInverseSearch;

    /**
     * @var integer
     */
    public $itemizedBill;

    /**
     * @var integer
     */
    public $itemizedBillAnonymized;

    /**
     * @var integer
     */
    public $itemizedBillShorted;

    /**
     * Create PurtelAccount-object from array
     * 
     * @param array $data
     * 
     * @return PurtelAccount
     */
    public static function createFromArray($data)
    {
        $mapping = [
            'id' => 'id',
            'purtel_login' => 'purtelLogin',
            'purtel_password' => 'purtelPassword',
            'master' => 'master',
            'porting' => 'porting',
            'nummer' => 'nummer',
            'nummernblock' => 'nummernblock',
            'area_code' => 'areaCode',
            'typ' => 'typ',
            'cust_id' => 'customer',
            'phone_book_information' => 'phoneBookInformation',
            'phone_book_digital_media' => 'phoneBookDigitalMedia',
            'phone_book_inverse_search' => 'phoneBookInverseSearch',
            'itemized_bill' => 'itemizedBill',
            'itemized_bill_anonymized' => 'itemizedBillAnonymized',
            'itemized_bill_shorted' => 'itemizedBillShorted',
        ];

        $purtelAccount = new self();

        foreach ($mapping as $dataKey => $objectParameter) {
            if (!isset($data[$dataKey])) {
                continue;
            }

            $purtelAccount->{$objectParameter} = $data[$dataKey];
        }
        
        return $purtelAccount;
    }

    /**
     * Build insert sql query
     * 
     * @return string
     */
    public function buildInsertQuery()
    {
        $mapping = [
            'purtel_login' => $this->purtelLogin,
            'purtel_password' => $this->purtelPassword,
            'master' => $this->master,
            'porting' => $this->porting,
            'nummer' => $this->nummer,
            'nummernblock' => $this->nummernblock,
            'area_code' => $this->areaCode,
            'typ' => $this->typ,
            'cust_id' => $this->customer->id,
            'phone_book_information' => $this->phoneBookInformation,
            'phone_book_digital_media' => $this->phoneBookDigitalMedia,
            'phone_book_inverse_search' => $this->phoneBookInverseSearch,
            'itemized_bill' => $this->itemizedBill,
            'itemized_bill_anonymized' => $this->itemizedBillAnonymized,
            'itemized_bill_shorted' => $this->itemizedBillShorted,
        ];

        return sprintf('INSERT INTO `purtel_account` (%s) VALUES (%s)',
            // colum names
            implode(',', array_map(function ($value) {
                return sprintf('`%s`', $value);
            }, array_keys($mapping))),
            
            // values
            implode(',', array_map(function ($value) {
                if (null === $value) {
                    return 'NULL';
                }

                if (empty($value)) {
                    return "''";
                }

                if ($value instanceOf \DateTime) {
                    return sprintf("'%s'", $value->format('d.m.Y'));
                }

                return sprintf("'%s'", $value);
            }, $mapping))
        );
    }
}
