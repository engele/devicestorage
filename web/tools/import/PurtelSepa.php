<?php
/**
 *  Class to set and Activate Sepa Mandat in Purtel
 */
class PurtelSepa
{
    /**
    * sql 
    */
    const SQL_BIC_BANKNAME = 'SELECT * FROM bic_bankname';
    const SQL_BLZ_BANKNAME = 'SELECT * FROM blz_bankname';

    const SQL_UPDATE_MANDANTENID = 'UPDATE customers SET bank_account_mandantenid = "%s" WHERE id = "%s"';
    const SQL_UPDATE_DEBITOR = 'UPDATE customers SET bank_account_debitor = "%s" WHERE id = "%s"';
    const SQL_UPDATE_BIC = 'UPDATE customers SET bank_account_bic_new = "%s" WHERE id = "%s"';
    const SQL_UPDATE_BANKNAME = 'UPDATE customers SET bank_account_bankname = "%s" WHERE id = "%s"';
    const SQL_UPDATE_IBAN = 'UPDATE customers SET bank_account_iban = "%s" WHERE id = "%s"';
    
    /**
    * used purtel url
    */
    const PURTEL_SEPA = 'https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s&art=1&kundennummer_extern=%s';
    const PURTEL_SEPA_OPTIONS = "&erzeugen=1&aktivieren=1";
    const PURTEL_CONTRACT = 'https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s&art=1&kundennummer_extern=%s&bank=%s';

    /**
    *  purtel actions
    */
    const CREATESEPA = 'createsepa';
    const CHANGECONTRACT = 'changecontract';
    /**
    * @var string    
    */
    protected $purtelSuperUsername = NULL;

    /**
    * @var string    
    */
    protected $purtelSuperPassword = NULL;

    /**
    * @var object    
    */
    protected $db = NULL;

    /**
    * @var string    
    */
    protected $sql = NULL;
    
    /**
    * @var string    
    */
    protected $debitor = NULL;
     
    /**
    * @var string    
    */
    protected $mandantenid = NULL;
    
    /**
    * @var string    
    */
    protected $bic = NULL;
    
    /**
    * @var string    
    */
    protected $iban = NULL;
    
    /**
    * @var string    
    */
    protected $bankname = NULL;
    
    /**
    * @var array    
    */
    protected $biclist = NULL;
    
    /**
    * @var array    
    */
    protected $blzlist = NULL;
    
    /**
    * @var array    
    */
    protected $blzbiclist = NULL;
    
    /**
    * Constructor Sepa Data
    * 
    * @parameter string $purtelSuperUsername
    * @parameter string $purtelSuperPassword
    * @parameter object $db
    * @parameter string $sql
    * @parameter string $mandantsid
    * @parameter string $debitor
    * 
    * @return void
    */
    public function __construct ($db = NULL, $purtelSuperUsername = NULL, $purtelSuperPassword = NULL, $sql = NULL, $iban = NULL, $mandantenid = NULL, $debitor = NULL, $bic = NULL, $bankname = NULL)
    {
        $bicQuery = $db->query(self::SQL_BIC_BANKNAME);
        while ($temp = $bicQuery->fetch_assoc()) {
            if (!isset($this->biclist[$temp['bic']])) {
                $this->biclist[$temp['bic']] = $temp['bankname'];
            }
        }
        $bicQuery->free();
 
        $blzQuery = $db->query(self::SQL_BLZ_BANKNAME);
        while ($temp = $blzQuery->fetch_assoc()) {
            if (!isset($this->blzlist[$temp['blz']])) {
                $this->blzlist[$temp['blz']] = $temp['bankname'];
                $this->blzbiclist[$temp['blz']] = $temp['bic'];
            }
        }
        $blzQuery->free();

        $this->initSepa($db, $purtelSuperUsername, $purtelSuperPassword, $sql, $iban, $mandantenid, $debitor, $bic, $bankname);
    }
    /**
    * Initialize Sepa Data
    * 
    * @parameter string $purtelSuperUsername
    * @parameter string $purtelSuperPassword
    * @parameter object $db
    * @parameter string $sql
    * @parameter string $mandantenId
    * @parameter string $debitor
    * @parameter string $bic
    * @parameter string $bankname
    * 
    * @return void
    */
    public function initSepa ($db = NULL, $purtelSuperUsername = NULL, $purtelSuperPassword = NULL, $sql = NULL, $iban = NULL, $mandantenid = NULL, $debitor = NULL, $bic = NULL, $bankname = NULL)
    {
        $this->purtelSuperUsername = empty($purtelSuperUsername) ? NULL : $purtelSuperUsername;
        $this->purtelSuperPassword = empty($purtelSuperPassword) ? NULL : $purtelSuperPassword;
        $this->db = empty($db) ? NULL : $db;
        $this->sql = empty($sql) ? NULL: $sql;
        $this->iban = empty($iban) ? NULL: $iban;
        $this->mandantenid = empty($mandantenid) ? NULL : $mandantenid;
        $this->debitor = empty($debitor) ? NULL : $debitor;
        $this->bic = empty($bic) ? NULL: $bic;
        $this->bankname = empty($bankname) ? NULL: $bankname;
        $this->getBank();
    }

    /**
    * get Customer Parameter and set and activate Sepa
    *     
    */
    public function setSepa ()
    {
        $db = $this->db;
        if (empty($this->sql)) {
            throw new \UnexpectedValueException("Error - SQL Statment missing");
            return;
        }
        $customersQuery = $this->db->query($this->sql);
        if ($customersQuery === FALSE) {
            throw new \UnexpectedValueException("Error - ".$this->db->error);
        }
        if ($customersQuery->num_rows == 0) {
            throw new \UnexpectedValueException("Error - no customer in ikv ".$this->sql);    
        }
        while ($customer = $customersQuery->fetch_assoc()) {
            if ($this->mandantenid != $customer['bank_account_mandantenid']) {
                if (empty ($this->mandantenid)) {
                    $this->mandantenid = $customer['bank_account_mandantenid']; 
                } 
                $dbup_return = $db->query(sprintf(self::SQL_UPDATE_MANDANTENID, $this->mandantenid, $customer['id']));
                if ($dbup_return === FALSE) {
                    fwrite (STDERR, "Error - update mandantenid for ".$customer['clientid'].' - '.$this->db->error."\n");    
                }
            }
            if ($this->debitor != $customer['bank_account_debitor']) {
                if (empty ($this->debitor)) {
                    $this->debitor = $customer['bank_account_debitor']; 
                } 
                $dbup_return = $db->query(sprintf(self::SQL_UPDATE_DEBITOR, $this->debitor, $customer['id']));
                if ($dbup_return === FALSE) {
                    fwrite (STDERR, "Error - update debitor for ".$customer['clientid'].' - '.$this->db->error."\n");    
                }
            }
            if ($this->bic != $customer['bank_account_bic_new']) {
                if (empty ($this->bic)) {
                    $this->bic = $customer['bank_account_bic_new']; 
                } 
                $dbup_return = $db->query(sprintf(self::SQL_UPDATE_BIC, $this->bic, $customer['id']));
                if ($dbup_return === FALSE) {
                    fwrite (STDERR, "Error - update bic for ".$customer['clientid'].' - '.$this->db->error."\n");    
                }
            }
            if ($this->bankname != $customer['bank_account_bankname']) {
                if (empty ($this->bankname)) {
                    $this->bankname = $customer['bank_account_bankname']; 
                } 
                $dbup_return = $db->query(sprintf(self::SQL_UPDATE_BANKNAME, $this->bankname, $customer['id']));
                if ($dbup_return === FALSE) {
                    fwrite (STDERR, "Error - update bankname for ".$customer['clientid'].' - '.$this->db->error."\n");    
                }
            }
            if ($this->iban != $customer['bank_account_iban']) {
                if (empty ($this->iban)) {
                    $this->iban = $customer['bank_account_iban']; 
                } elseif (empty($customer['bank_account_iban']) && $this->iban) {
                    $dbup_return = $db->query(sprintf(self::SQL_UPDATE_IBAN, $this->iban, $customer['id']));
                    if ($dbup_return === FALSE) {
                        fwrite (STDERR, "Error - update iban for ".$customer['clientid'].' - '.$this->db->error."\n");    
                    }
                } else {
                    fwrite(STDERR, $customer['clientid'].": Info - iban in ikv and csv is different - csv:".$this->iban." ikv:".$customer['bank_account_iban']."\n");
                }
            }

            /*try {
                $this->setPurtel($customer);
            } catch (\UnexpectedValueException $e) {
                fwrite (STDERR, $e->getMessage()."\n"); 
            }*/
        }
    }

    /**
    * set the SEPA on Purtelplattform
    * 
    * @parameter array $customer
    * 
    * @return void
    */
    protected function setPurtel ($customer)
    { 
        // get sepa

        $url = sprintf(self::PURTEL_SEPA,
            urlencode($this->purtelSuperUsername),
            urlencode($this->purtelSuperPassword),
            self::CREATESEPA,
            urlencode($customer['clientid'])
        );

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $resultLine = explode("\n", $result);
        $resultHeader = str_getcsv(array_shift($resultLine),';');
        $resultStatus = $resultHeader[0];
    
        if (substr($resultStatus, 0, 4) == '-ERR') {
            throw new \UnexpectedValueException($customer['clientid'].': Error - '.$resultStatus);
        }
    
        if ($resultLine[0]) {
            if (substr($resultLine[0], 0, 4) == '-ERR') {
                $this->getBank();
                fwrite(STDERR, $customer['clientid'].": Info - Insert Bankname: ".$this->bankname." (".$this->bic.") \n");
            
                // insert bankname

                $url = sprintf(self::PURTEL_CONTRACT,
                    urlencode($this->purtelSuperUsername),
                    urlencode($this->purtelSuperPassword),
                    self::CHANGECONTRACT,
                    urlencode($customer['clientid']),
                    urlencode($this->bankname)
                );

                $curl = curl_init($url);

                curl_setopt_array($curl, array(
                    CURLOPT_HEADER          => false,
                    CURLOPT_RETURNTRANSFER  => true,
                    CURLOPT_SSL_VERIFYPEER  => false,
                ));

                $result = curl_exec($curl);

                curl_close($curl);
                if (substr($result, 0, 4) == '-ERR') {
                    throw new \UnexpectedValueException($customer['clientid'].": Error - Inserting Bankname: ".$result);
                }

                unset($resultLine);
                unset($resultHeader);
                unset($resultStatus);
                // get sepa again

                $url = sprintf(self::PURTEL_SEPA,
                    urlencode($this->purtelSuperUsername),
                    urlencode($this->purtelSuperPassword),
                    self::CREATESEPA,
                    urlencode($customer['clientid'])
                );

                $curl = curl_init($url);

                curl_setopt_array($curl, array(
                    CURLOPT_HEADER          => false,
                    CURLOPT_RETURNTRANSFER  => true,
                    CURLOPT_SSL_VERIFYPEER  => false,
                ));

                $result = curl_exec($curl);

                curl_close($curl);

                $resultLine = explode("\n", $result);
                $resultHeader = str_getcsv(array_shift($resultLine),';');
                $resultStatus = $resultHeader[0];
            }
            $resultData = str_getcsv(array_shift($resultLine),';');
            foreach ($resultHeader as $key => $value){
                $header[$value] = isset($resultData[$key]) ? $resultData[$key] : '';
            }
            $id_version = strstr ($header['kundennummer_extern'], '-');
            if ($id_version) {
                echo $customer['clientid'].': SEPA Created Madant-ID:'.$header['kundennummer_extern']."\n";   
            } else {
                throw new \UnexpectedValueException($customer['clientid'].': Warning - SEPA Exists '.$header['kundennummer_extern']." ".$header['create_status']);
            }
        }

        unset($resultLine);
        unset($resultHeader);
        unset($resultStatus);
    
        // create and activate sepa
        $options = self::PURTEL_SEPA_OPTIONS;
        if (!empty($this->mandantenid)) {
            $options .= '&mandantsid='. urlencode($this->mandantenid);    
        }
        
        $url = sprintf(self::PURTEL_SEPA.$options,
            urlencode($this->purtelSuperUsername),
            urlencode($this->purtelSuperPassword),
            self::CREATESEPA,
            urlencode($customer['clientid'])
        );

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));

        $result = curl_exec($curl);

        curl_close($curl);

        $resultLine = explode("\n", $result);
        $resultHeader = str_getcsv(array_shift($resultLine),';');
        $resultStatus = $resultHeader[0];
    
        if (substr($resultStatus, 0, 4) == '-ERR') {
            throw new \UnexpectedValueException($customer['clientid'].': Error - '.$resultStatus);
        }
    }
    
    /**
    * get bankname from iban and  bic
    * 
    * @return viod
    */
    protected function getBank ()
    {
        if (!empty ($this->iban)) {
            // country code == DE
            if (substr($this->iban, 0, 2) == 'DE') {
                $blz = substr($this->iban, 4, 8);
                if (isset ($this->blzlist[$blz])) {
                    $this->bankname = empty($this->bankname) ?  $this->bankname = $this->blzlist[$blz] : $this->bankname; 
                }    
                if (isset ($this->blzbiclist[$blz])) {
                    $this->bic = empty($this->bic) ?  $this->bic = $this->blzbiclist[$blz] : $this->bic; 
                }    
            } elseif (empty($this->bankname) && !empty($this->bic)) {
                if (isset($this->biclist[$this->bic])) { 
                    $this->bankname = empty($this->bankname) ?  $this->biclist[$this->bic] : $this->bankname;
                }    
            }
        } elseif (empty($this->bankname) && !empty($this->bic)) {
            if (isset($this->biclist[$this->bic])) { 
                $this->bankname = empty($this->bankname) ?  $this->biclist[$this->bic] : $this->bankname;
            }    
        }
        return;
    }
}  
?>
