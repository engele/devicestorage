<?php

/**
 *
 */
class GroupedExtensionData
{
    /**
     * @var string
     */
    public $groupingValue;

    /**
     * @var array
     */
    public $data;

    /**
     * This GroupedExtensionData belongs to this CsvReader
     * 
     * @var CsvReader
     */
    protected $csvReader;

    /**
     * Constructor
     * 
     * @param string $groupingValue
     * @param array $groupedData
     * @param CsvReader $extensionCsv
     */
    public function __construct($groupingValue, array $data, CsvReader $csvReader)
    {
        $this->groupingValue = $groupingValue;
        $this->data = $data;
        $this->csvReader = $csvReader;
    }

    /**
     * Get CsvReader
     * 
     * @return CsvReader
     */
    public function getCsvReader()
    {
        return $this->csvReader;
    }

    /**
     * Find lines with specific column content in csv data
     * 
     * @param array $matching
     * 
     * @return array|null
     */
    public function findLines($matching)
    {
        $matches = null;

        foreach ($matching as $column => $matchingValue) {
            if (null === $matches) {
                $matches = [];

                foreach ($this->data as $key => $line) {
                    if ($line[$this->csvReader->headlineReversed[$column]] === $matchingValue) {
                        $matches[$key] = $line;
                    }
                }
            } elseif (count($matches) > 0) {
                foreach ($matches as $key => $line) {
                    if ($line[$this->csvReader->headlineReversed[$column]] !== $matchingValue) {
                        unset($matches[$key]);
                    }
                }
            } else {
                return null;
            }
        }

        return count($matches) > 0 ? $matches : null;
    }
}
