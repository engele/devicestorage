<?php

/**
 * Used to collect all data to customer
 */
class Customer
{
    /**
     * Do not set unless you are realy sure you know what you are doing!
     * 
     * @var integer
     */
    public $id;
    
    /**
     * @var boolean
     */
    public $isNew = true;

    /**
     * @var string
     */
    public $purtelProduct;

    /**
     * Main product
     * 
     * @var string
     */
    public $productname;

    /**
     * @var array
     */
    public $products;

    /**
     * @var array
     */
    public $purtelAccounts;

    /**
     * @var Location
     */
    public $location;

    /**
     * customer_id
     * 
     * @var string
     */
    public $externalCustomerId;

    /**
     * @var \DateTime
     */
    public $connectionActivationDate;

    /**
     * @var string
     */
    public $contractId;

    /**
     * @var string
     */
    public $district;

    /**
     * @var string
     */
    public $bankAccountHolderLastname;

    /**
     * @var string
     */
    public $bankAccountBic;

    /**
     * @var string
     */
    public $bankAccountIban;

    /**
     * @var string
     */
    public $bankAccountEmailaddress;

    /**
     * @var \DateTime
     */
    public $wisocontractCancelDate;

    /**
     * @var \DateTime
     */
    public $wisocontractSwitchoffFinish;

    /**
     * @var string
     */
    public $connectionStreet;

    /**
     * @var string
     */
    public $connectionStreetNo;

    /**
     * @var string
     */
    public $connectionCity;

    /**
     * @var string
     */
    public $connectionZipcode;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $firstname;

    /**
     * @var string
     */
    public $lastname;

    /**
     * @var string
     */
    public $street;

    /**
     * @var string
     */
    public $streetno;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $zipcode;

    /**
     * @var \DateTime
     */
    public $birthday;

    /**
     * @var string
     */
    public $emailaddress;

    /**
     * @var \DateTime
     */
    public $statiPortConfirmDate;

    /**
     * @var string
     */
    public $company;

    /**
     * @var \DateTime
     */
    public $contractVersion;

    /**
     * @var string
     */
    public $noEze;

    /**
     * @var string
     */
    public $creditRatingOk;

    /**
     * @var \DateTime
     */
    public $creditRatingDate;

    /**
     * @var string
     */
    public $clienttype;

    /**
     * @var string
     */
    public $purtelEditDone;

    /**
     * @var \DateTime
     */
    public $wisocontractCanceledDate;

    /**
     * @var string
     */
    public $macAddress;

    /**
     * @var string
     */
    public $lineIdentifier;

    /**
     * @var string
     */
    public $pppoePin;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $paperBill;

    /**
     * @var string
     */
    public $telefonbuchEintrag;

    /**
     * @var string
     */
    public $comment;

    public $networkId;
    public $applicationText;
    public $application;
    public $prospectSupplyStatus;
    public $talOrderDate;
    public $talDtagAssignmentNo;
    public $customerConnectInfoDate;
    public $talOrderAckDate;

    /**
     * @var \DateTime
     */
    public $dslamArrangedDate;

    /**
     * @var \DateTime
     */
    public $terminalReady;

    /**
     * @var \DateTime
     */
    public $patchDate;

    /**
     * @var \DateTime
     */
    public $contractSentDate;

    /**
     * @var \DateTime
     */
    public $contractReceivedDate;

    /**
     * @var string
     */
    public $clientId;

    /**
     * @var integer
     */
    public $ipRangeId;

    /**
     * All products, main, cross-selling
     */
    public function setProducts(array $products)
    {
        $this->products = $products;
    }

    /**
     *
     */
    public function setPurtelProduct($product)
    {
        $this->purtelProduct = $product;
    }

    /**
     * 
     */
    public function setProductname($productname)
    {
        $this->productname = $productname;
    }

    /**
     * 
     */
    public function setPurtelAccounts(array $purtelAccounts)
    {
        $this->purtelAccounts = $purtelAccounts;
    }

    /**
     * 
     */
    public function setLocation(Location $location)
    {
        $this->location = $location;
    }

    /**
     * 
     */
    public function setExternalCustomerId($externalCustomerId)
    {
        $this->externalCustomerId = $externalCustomerId;
    }

    /**
     * 
     */
    public function setConnectionActivationDate(\DateTime $connectionActivationDate = null)
    {
        $this->connectionActivationDate = $connectionActivationDate;
    }

    /**
     * 
     */
    public function setContractId($contractId)
    {
        $this->contractId = $contractId;
    }

    /**
     * 
     */
    public function setBankAccountHolderLastname($bankAccountHolderLastname)
    {
        $this->bankAccountHolderLastname = $bankAccountHolderLastname;
    }

    /**
     * 
     */
    public function setBankAccountBic($bankAccountBic)
    {
        $this->bankAccountBic = $bankAccountBic;
    }

    /**
     * 
     */
    public function setBankAccountIban($bankAccountIban)
    {
        $this->bankAccountIban = $bankAccountIban;
    }

    /**
     * 
     */
    public function setBankAccountEmailaddress($bankAccountEmailaddress)
    {
        $this->bankAccountEmailaddress = $bankAccountEmailaddress;
    }

    /**
     * 
     */
    public function setWisocontractCancelDate(\DateTime $wisocontractCancelDate = null)
    {
        $this->wisocontractCancelDate = $wisocontractCancelDate;
    }

    /**
     * 
     */
    public function setWisocontractSwitchoffFinish(\DateTime $wisocontractSwitchoffFinish = null)
    {
        $this->wisocontractSwitchoffFinish = $wisocontractSwitchoffFinish;
    }

    /**
     * 
     */
    public function setConnectionStreet($connectionStreet)
    {
        $this->connectionStreet = $connectionStreet;
    }

    /**
     * 
     */
    public function setConnectionStreetNo($connectionStreetNo)
    {
        $this->connectionStreetNo = $connectionStreetNo;
    }

    /**
     * 
     */
    public function setConnectionCity($connectionCity)
    {
        $this->connectionCity = $connectionCity;
    }

    /**
     * 
     */
    public function setConnectionZipcode($connectionZipcode)
    {
        $this->connectionZipcode = $connectionZipcode;
    }

    /**
     * 
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * 
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * 
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * 
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * 
     */
    public function setStreetno($streetno)
    {
        $this->streetno = $streetno;
    }

    /**
     * 
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * 
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * 
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * 
     */
    public function setEmailaddress($emailaddress)
    {
        $this->emailaddress = $emailaddress;
    }

    /**
     * 
     */
    public function setStatiPortConfirmDate(\DateTime $statiPortConfirmDate = null)
    {
        $this->statiPortConfirmDate = $statiPortConfirmDate;
    }

    /**
     * 
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * 
     */
    public function setContractVersion($contractVersion)
    {
        $this->contractVersion = $contractVersion;
    }

    /**
     * 
     */
    public function setNoEze($noEze)
    {
        $this->noEze = $noEze;
    }

    /**
     * 
     */
    public function setCreditRatingOk($creditRatingOk)
    {
        $this->creditRatingOk = $creditRatingOk;
    }

    /**
     * 
     */
    public function setCreditRatingDate(\DateTime $creditRatingDate = null)
    {
        $this->creditRatingDate = $creditRatingDate;
    }

    /**
     * 
     */
    public function setClienttype($clienttype)
    {
        $this->clienttype = $clienttype;
    }

    /**
     * 
     */
    public function setPurtelEditDone($purtelEditDone)
    {
        $this->purtelEditDone = $purtelEditDone;
    }

    /**
     * 
     */
    public function setWisocontractCanceledDate(\DateTime $wisocontractCanceledDate = null)
    {
        $this->wisocontractCanceledDate = $wisocontractCanceledDate;
    }

    /**
     * 
     */
    public function setMacAddress($macAddress)
    {
        $this->macAddress = $macAddress;
    }

    /**
     * 
     */
    public function setLineIdentifier($lineIdentifier)
    {
        $this->lineIdentifier = $lineIdentifier;
    }

    /**
     * 
     */
    public function setPppoePin($pppoePin)
    {
        $this->pppoePin = $pppoePin;
    }

    /**
     * 
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * 
     */
    public function setPaperBill($paperBill)
    {
        $this->paperBill = $paperBill;
    }

    /**
     * 
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * 
     */
    public function setIpRangeId($ipRangeId)
    {
        $this->ipRangeId = $ipRangeId;
    }

    /**
     * Build insert sql query
     * 
     * @return string
     */
    public function buildInsertQuery()
    {
        $mapping = [
            'clientid' => $this->clientId,
            'connection_activation_date' => $this->connectionActivationDate,
            'network_id' => $this->networkId,
            'customer_id' => $this->externalCustomerId,
            'contract_id' => $this->contractId,
            'bank_account_holder_lastname' => $this->bankAccountHolderLastname,
            'bank_account_bic_new' => $this->bankAccountBic,
            'bank_account_iban' => $this->bankAccountIban,
            'bank_account_emailaddress' => $this->bankAccountEmailaddress,
            'wisocontract_cancel_date' => $this->wisocontractCancelDate,
            'wisocontract_switchoff_finish' => $this->wisocontractSwitchoffFinish,
            'connection_street' => $this->connectionStreet,
            'connection_streetno' => $this->connectionStreetNo,
            'connection_city' => $this->connectionCity,
            'connection_zipcode' => $this->connectionZipcode,
            'title' => $this->title,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'street' => $this->street,
            'streetno' => $this->streetno,
            'city' => $this->city,
            'zipcode' => $this->zipcode,
            'district' => $this->district,
            'birthday' => $this->birthday,
            'emailaddress' => $this->emailaddress,
            'stati_port_confirm_date' => $this->statiPortConfirmDate,
            'company' => $this->company,
            'contract_version' => $this->contractVersion,
            'comment' => $this->comment,
            'no_eze' => $this->noEze,
            'credit_rating_ok' => $this->creditRatingOk,
            'credit_rating_date' => $this->creditRatingDate,
            'clienttype' => $this->clienttype,
            'purtel_edit_done' => $this->purtelEditDone,
            'wisocontract_canceled_date' => $this->wisocontractCanceledDate,
            'application_text' => $this->applicationText,
            'application' => $this->application,
            'prospect_supply_status' => $this->prospectSupplyStatus,
            'tal_order_date' => $this->talOrderDate,
            'tal_dtag_assignment_no' => $this->talDtagAssignmentNo,
            'customer_connect_info_date' => $this->customerConnectInfoDate,
            'tal_order_ack_date' => $this->talOrderAckDate,
            'dslam_arranged_date' => $this->dslamArrangedDate,
            'terminal_ready' => $this->terminalReady,
            'patch_date' => $this->patchDate,
            'location_id' => $this->location->id,
            'contract_sent_date' => $this->contractSentDate,
            'contract_received_date' => $this->contractReceivedDate,
            'mac_address' => $this->macAddress, 
            'line_identifier' => $this->lineIdentifier, 
            'pppoe_pin' => $this->pppoePin, 
            'paper_bill' => $this->paperBill, 
            'telefonbuch_eintrag' => $this->telefonbuchEintrag,
            'password' => $this->password,
            'productname' => $this->productname,
            'purtel_product' => $this->purtelProduct,
            'ip_range_id' => $this->ipRangeId,
        ];

        // can not be null
        foreach (['stati_port_confirm_date', 'wisocontract_cancel_date', 'wisocontract_switchoff_finish', 'wisocontract_canceled_date', 'telefonbuch_eintrag', 'purtel_edit_done', 'credit_rating_date', 'dslam_arranged_date'] as $key) {
            if (null === $mapping[$key]) {
                $mapping[$key] = '';
            }
        }

        return sprintf('INSERT INTO `customers` (%s) VALUES (%s)',
            // colum names
            implode(',', array_map(function ($value) {
                return sprintf('`%s`', $value);
            }, array_keys($mapping))),
            
            // values
            implode(',', array_map(function ($value) {
                if (null === $value) {
                    return 'NULL';
                }

                if (empty($value)) {
                    return "''";
                }

                if ($value instanceOf \DateTime) {
                    return sprintf("'%s'", $value->format('d.m.Y'));
                }

                return sprintf("'%s'", $value);
            }, $mapping))
        );
    }
}
