<?php

/**
 *
 */
class Product
{
    /**
     * Do not set unless you are realy sure you know what you are doing!
     * 
     * @var integer
     */
    public $id;

    /**
     * @var boolean
     */
    public $isNew = true;

    /**
     * @var string
     */
    public $identifier;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $itemCategory;

    /**
     * @var float|null
     */
    public $defaultPriceNet;

    /**
     * @var integer
     */
    public $active = 1;

    /**
     * Create Product-object from array
     * 
     * @param array $data
     * 
     * @return Product
     */
    public static function createFromArray($data)
    {
        $mapping = [
            'id' => 'id',
            'identifier' => 'identifier',
            'name' => 'name',
            'item_category' => 'itemCategory',
            'default_price_net' => 'defaultPriceNet',
        ];

        $product = new self();

        foreach ($mapping as $dataKey => $objectParameter) {
            if (!isset($data[$dataKey])) {
                continue;
            }

            $product->{$objectParameter} = $data[$dataKey];
        }
        
        return $product;
    }

    /**
     * Build insert sql query
     * 
     * @return string
     */
    public function buildInsertQuery()
    {
        $mapping = [
            'active' => $this->active,
            'identifier' => $this->identifier,
            'name' => $this->name,
            'item_category' => $this->itemCategory,
            'default_price_net' => $this->defaultPriceNet,
        ];

        return sprintf('INSERT INTO `products` (%s) VALUES (%s)',
            // colum names
            implode(',', array_map(function ($value) {
                return sprintf('`%s`', $value);
            }, array_keys($mapping))),
            
            // values
            implode(',', array_map(function ($value) {
                if (null === $value) {
                    return 'NULL';
                }

                if (empty($value)) {
                    return "''";
                }

                if ($value instanceOf \DateTime) {
                    return sprintf("'%s'", $value->format('Y-m-d'));
                }

                return sprintf("'%s'", $value);
            }, $mapping))
        );
    }
}
