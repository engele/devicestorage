<?php

/**
 * Runs a mapping function before actually setting data
 */
class Mapper
{
    /**
     * Set data to this object
     */
    public $object;

    /**
     * Mapping methods called befor actually setting data
     * 
     * @var array
     */
    protected $mappingMethods = [];

    /**
     * Set object to set mapping & data for
     * 
     * @param object $object
     * 
     * @return Mapper
     */
    public function setObject($object)
    {
        $this->object = $object;

        return $this;
    }

    /**
     * Get data object
     * 
     * @return object
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @throws InvalidArgumentException|BadMethodCallException - and execptions comming from object while setting data
     * 
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        // set mapping method
        if (0 === strpos($method, 'to')) {
            if (count($arguments) !== 1) {
                throw new \InvalidArgumentException(sprintf('the mapper "to" methods must have exactly 1 argument - called method (%s)',
                    $method
                ));
            }

            $callback = $arguments[0];
            $methodIdentifier = substr($method, 2);

            if (!is_callable($callback)) {
                throw new \InvalidArgumentException('the argument to the mapper method must be a callable');
            }

            $this->mappingMethods[$methodIdentifier] = $callback;

            return $this;
        }

        // set actual data
        if (0 === strpos($method, 'set')) {
            $methodIdentifier = substr($method, 3);

            if (isset($this->mappingMethods[$methodIdentifier])) {
                // run mapping method
                $arguments = call_user_func_array($this->mappingMethods[$methodIdentifier], $arguments);

                if (!is_array($arguments)) {
                    throw new \InvalidArgumentException(sprintf('the mapper method for %s must return an array',
                        $methodIdentifier
                    ));
                }
            }

            // actually setting data to object
            return call_user_func_array([$this->object, $method], $arguments);
        }

        throw new \BadMethodCallException('unsupported method');
    }
}
