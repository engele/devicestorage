<?php

/**
 *
 */
class Location
{
    /**
     * Do not set unless you are realy sure you know what you are doing!
     * 
     * @var integer
     */
    public $id;

    /**
     * @var boolean
     */
    public $isNew = true;
    
    /**
     * @var integer
     */
    public $networkId;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $kvzPrefix;

    /**
     * @var integer
     */
    public $cidSuffixStart;

    /**
     * @var integer
     */
    public $cidSuffixEnd;

    /**
     * Create Location-object from array
     * 
     * @param array
     * 
     * @return Locatiom
     */
    public static function createFromArray(array $array)
    {
        $location = new self();

        $location->id = $array['id'];
        $location->networkId = $array['network_id'];
        $location->name = $array['name'];
        $location->kvzPrefix = $array['kvz_prefix'];
        $location->cidSuffixStart = $array['cid_suffix_start'];
        $location->cidSuffixEnd = $array['cid_suffix_end'];

        return $location;
    }
}
