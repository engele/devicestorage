<?php

require_once __DIR__.'/GroupedExtensionData.php';

/**
 * 
 */
class CsvReader
{
    /**
     * Csv contents as array
     *
     * @var array
     */
    public $data;

    /**
     * First line of csv
     * 
     * @var array
     */
    public $headline;

    /**
     * $headline but with values as keys and keys as value
     * 
     * @var array
     */
    public $headlineReversed;

    /**
     * @var boolean
     */
    protected $useFirstLineAsHeadline;

    /**
     * @var string
     */
    protected $filename;

    /**
     * Constructor
     * 
     * Read contents from csv-file
     *
     * @param string $filename
     * @param boolean $useFirstLineAsHeadline
     *
     * @throws InvalidArgumentException
     */
    public function __construct($filename, $useFirstLineAsHeadline = true, $stripQuotes = true)
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            throw new InvalidArgumentException('Can not open file for reading');
        }

        $file = fopen($filename, 'r');
        $lineCounter = 0;

        while (!feof($file)) {
            $lineCounter++;

            $data = fgetcsv($file, 0, ';', '"');

            if (!is_array($data)) {
                //trigger_error(sprintf('invalid csv-line at line %d', $lineCounter), E_USER_NOTICE);

                continue;
            }

            if (!$stripQuotes) {
                $this->data[] = $data;

                continue;
            }

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            $this->data[] = $data;
        }

        fclose($file);

        /*$content = file_get_contents($filename);

        if (!is_string($content) && !empty($content)) {
            throw new InvalidArgumentException('invalid file');
        }

        $this->useFirstLineAsHeadline = $useFirstLineAsHeadline;
        $this->filename = $filename;

        $delimiter = null;

        foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
            if (false !== strpos($content, $delimiter_)) {
                $delimiter = $delimiter_;
                break;
            }
        }

        if (null == $delimiter) {
            throw new InvalidArgumentException('can not use file');
        }

        $this->data = array_map(function ($line) use ($stripQuotes) {
            if (!$stripQuotes) {
                return str_getcsv($line, ';');
            }

            $data = str_getcsv($line, ';');

            foreach ($data as $key => $value) {
                if (is_scalar($value)) {
                    if ("'" === substr($value, 0, 1) && "'" === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    } elseif ('"' === substr($value, 0, 1) && '"' === substr($value, -1)) {
                        $data[$key] = substr($value, 1, -1);
                    }
                }
            }

            return $data;
        }, explode($delimiter, $content));*/


        if ($useFirstLineAsHeadline) {
            $this->headline = array_shift($this->data);

            $this->headlineReversed = array_combine($this->headline, array_keys($this->headline));

            if (count($this->headline) !== count($this->headlineReversed)) {
                // likely some columns use the same name

                foreach ($this->headline as $key => $value) {
                    if ($key !== $this->headlineReversed[$value]) {
                        throw new \Exception(sprintf('column "%s" exists multiple times', $value));
                    }
                }
            }
        }

        // strip last element if empty
        // use current bevor end because
        // end() sets internal array pointer to last element
        if (count(current($this->data)) !== count(end($this->data))) {
            array_pop($this->data);
        }
    }

    /**
     * Find lines with specific column content in csv data
     * 
     * @param array $matching
     * 
     * @return array|null
     */
    public function findLines($matching)
    {
        $matches = null;

        foreach ($matching as $column => $matchingValue) {
            if (null === $matches) {
                $matches = [];

                foreach ($this->data as $key => $line) {
                    if ($line[$this->headlineReversed[$column]] === $matchingValue) {
                        $matches[$key] = $line;
                    }
                }
            } elseif (count($matches) > 0) {
                foreach ($matches as $key => $line) {
                    if ($line[$this->headlineReversed[$column]] !== $matchingValue) {
                        unset($matches[$key]);
                    }
                }
            } else {
                return null;
            }
        }

        return count($matches) > 0 ? $matches : null;
    }

    /**
     * Get key for column
     * 
     * @param string $column
     * 
     * @throws \InvalidArgumentException
     * 
     * @return string|integer
     */
    public function getKeyForColumn($column)
    {
        if ($this->useFirstLineAsHeadline) {
            return $this->headlineReversed[$column];
        }

        if (!is_int($column)) {
            throw new \InvalidArgumentException(sprintf('You chose not to use first line as headline and therefore column must be an integer but (%s) was given',
                gettype($column)
            ));
        }

        return $column;
    }

    /**
     * Get grouped data
     * 
     * @param string|integer $columnKey
     * 
     * @return array
     */
    public function getDataGroupedBy($columnKey)
    {
        $groupedData = [];

        foreach ($this->data as $key => $line) {
            $groupingValue = trim($line[$columnKey]);

            if (!isset($groupedData[$groupingValue])) {
                $groupedData[$groupingValue] = [];
            }

            $groupedData[$groupingValue][$key] = & $this->data[$key]; // use reference 
        }

        return $groupedData;
    }

    /**
     * Extend "base"-csv with columns from extension file
     * 
     * @param CsvReader $extensionCsv
     * @param string $mergeExtensionColumn
     * @param string $mergeBaseColumn
     * @param string|integer|null $namespace - (optional) extension becomes available under this namespace
     * 
     * @throws \UnexpectedValueException
     * 
     * @return CsvReader
     */
    public function extend(CsvReader $extensionCsv, $mergeExtensionColumn, $mergeBaseColumn, $namespace = null)
    {
        $namespace = null !== $namespace ? $namespace : 'extension.'.$extensionCsv->getFilename();

        // ensure all lines have same column count
        foreach ($this->data as $key => $lines) {
            $this->data[$key][$namespace] = null;
        }

        // find grouping column key
        $groupingColumnKey = $extensionCsv->getKeyForColumn($mergeExtensionColumn);

        // append extension-data to relevant base-csv lines
        // use grouped data for simplicity
        foreach ($extensionCsv->getDataGroupedBy($groupingColumnKey) as $groupingValue => $lines) {
            $groupedExtensionData = new GroupedExtensionData($groupingValue, $lines, $extensionCsv);

            $relevantBaseCsvLines = $this->findLines([
                $mergeBaseColumn => (string) $groupingValue,
            ]);

            if (empty($relevantBaseCsvLines)) {
                throw new \UnexpectedValueException(sprintf('No relevant data in base csv found for %s searched by %s',
                    $groupingValue,
                    $mergeBaseColumn
                ));
            }

            foreach ($relevantBaseCsvLines as $key => $baseCsvLines) {
                $this->data[$key][$namespace] = $groupedExtensionData;
            }
        }

        if ($this->useFirstLineAsHeadline) {
            $this->headlineReversed[$namespace] = $namespace;
            $this->headline[$namespace] = $namespace;
        }

        return $this;
    }

    /**
     * Get use first line as headline
     * 
     * @return boolean
     */
    public function getUseFirstLineAsHeadline()
    {
        return $this->useFirstLineAsHeadline;
    }

    /**
     * Get filename
     * 
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }
}
