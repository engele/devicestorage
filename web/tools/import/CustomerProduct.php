<?php

/**
 *
 */
class CustomerProduct
{
    /**
     * @var Product
     */
    public $product;

    /**
     * @var float
     */
    public $priceNet;

    /**
     * @var \DateTime
     */
    public $activationDate;

    /**
     * @var \DateTime
     */
    public $deactivationDate;

    /**
     * @var Customer
     */
    public $customer;

    /**
     * @var integer
     */
    public $propperlyCopyedToCustomer = 0;

    /**
     * Build insert sql query
     * 
     * @return string
     */
    public function buildInsertQuery()
    {
        $mapping = [
            'product_id' => $this->product->id,
            'customer_id' => $this->customer->id,
            'price_net' => $this->priceNet,
            'activation_date' => $this->activationDate,
            'deactivation_date' => $this->deactivationDate,
            'propperly_copyed_to_customer' => $this->propperlyCopyedToCustomer,
        ];

        return sprintf('INSERT INTO `customer_products` (%s) VALUES (%s)',
            // colum names
            implode(',', array_map(function ($value) {
                return sprintf('`%s`', $value);
            }, array_keys($mapping))),
            
            // values
            implode(',', array_map(function ($value) {
                if (null === $value) {
                    return 'NULL';
                }

                if (empty($value)) {
                    return "''";
                }

                if ($value instanceOf \DateTime) {
                    return sprintf("'%s'", $value->format('Y-m-d'));
                }

                return sprintf("'%s'", $value);
            }, $mapping))
        );
    }
}
