<?php

require __DIR__.'/../_conf/database.inc';

$query = $db->query("SELECT * FROM `purtel_account` WHERE `purtel_login` IS NULL");

$byCustomer = [];

while ($d = $query->fetch_assoc()) {
    if (!isset($byCustomer[$d['cust_id']])) {
        $byCustomer[$d['cust_id']] = [];
    }

    $byCustomer[$d['cust_id']][] = $d;
}


foreach ($byCustomer as $customerId => $data) {
    $requireMaster = true;

    foreach ($data as $acc) {
        if (1 == $acc['master']) {
            $requireMaster = false;
        }
    }

    if ($requireMaster) {
        $acc = $data[0];

        $db->query("UPDATE purtel_account SET master = 1 WHERE id = ".$acc['id']);
    }
}
