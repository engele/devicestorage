<?php

require __DIR__.'/../_conf/database.inc';
require_once __DIR__.'/../vendor/wisotel/configuration/Configuration.php';

$purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

$q = $db->query("SELECT * FROM `customers` WHERE `id` BETWEEN 5382 AND 5505");

$ignoreCustomer = [
    '50000.17.0120',
    '50000.17.0124',
    '5382',
];

$onlyThisProducts = [
];

while ($customer = $q->fetch_assoc()) {
    sleep(1);

    if (in_array($customer['clientid'], $ignoreCustomer) || in_array($customer['id'], $ignoreCustomer)) {
        echo "skipped ".$customer['id']."\n";

        continue;
    }

    $purtelQ = $db->query("SELECT * FROM `purtel_account` WHERE `cust_id` = '".$customer['id']."' AND `master` = 1");

    if ($purtelQ->num_rows > 1) {
        var_dump('unpassende anzahl purtel accounts', $customer['id']);

        continue;
    }

    $purtelMaster = $purtelQ->fetch_assoc();


    if (empty($customer['purtel_product']) || 0 !== strpos($customer['purtel_product'], 'Dimari-')) {
        var_dump('no product', $customer['id']);

        continue;
    }

    $productIdentifier = explode('Dimari-', $customer['purtel_product']);
    $productIdentifier = end($productIdentifier);

    $productQ = $db->query("SELECT * FROM `products` WHERE `identifier` = '".$productIdentifier."'");

    if ($productQ->num_rows > 1) {
        var_dump('unpassende anzahl produkte', $customer['id'], $productIdentifier);

        continue;
    }

    $product = $productQ->fetch_assoc();

    if (empty($product['purtel_product_id'])) {
        var_dump('no purtel_product_id', $customer['id'], $productIdentifier);

        continue;
    }

    if (!empty($onlyThisProducts) && !in_array($product['purtel_product_id'], $onlyThisProducts)) {
        continue;
    }

    echo "Kunde: ".$customer['id']."\n";

    //var_dump($purtelMaster['purtel_login']);
    //continue;


    // update account

    $updatePostData = [
        //'wechseln' => 2,
        //'buchen' => 1,
    ];

    //$updatePostData['produkt'] = $product['purtel_product_id'];

    $updatePostData['gesperrt'] = 0;

    $updatePostData['kundennummer_extern'] = $customer['clientid'];

    if ($customer['purtel_use_version'] == 1 && $customer['version'] > 0) {
        $updatePostData['kundennummer_extern'] += '_'.$customer['version'];
    }

    $purtelAccountQ = $db->query("SELECT `purtel_login` FROM `purtel_account` WHERE `cust_id` = ".$customer['id']);

    while ($purtelAccount = $purtelAccountQ->fetch_assoc()) {
        $updatePostData['anschluss'] = $purtelAccount['purtel_login'];

        $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
            urlencode($purtelSuperUsername),
            urlencode($purtelSuperPassword),
            'changecontract'
        );
        
        $url .= '&'.utf8_decode(http_build_query($updatePostData));

        $curl = curl_init($url);

        curl_setopt_array($curl, array(
            //CURLOPT_URL             => $PurtelUrl1,
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));


        $result = curl_exec($curl);

        curl_close($curl);

        $result = trim($result);
        $result = explode(';', $result);

        if ('+OK' !== $result[0]) {
            var_dump('Failed to update sip', $result, $customer['id']);
        }
    }

    //die();
}


