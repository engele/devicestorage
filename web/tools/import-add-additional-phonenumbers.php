<?php

/**
 * Read contents from csv-file
 *
 * @param string $filename
 *
 * @throws InvalidArgumentException
 *
 * @return array
 */
function readCsvFile($filename)
{
    if (!file_exists($filename) || !is_readable($filename)) {
        throw new InvalidArgumentException('Can not open file for reading');
    }

    $content = file_get_contents($filename);

    if (!is_string($content) && !empty($content)) {
        throw new InvalidArgumentException('invalid file');
    }

    $delimiter = null;

    foreach (["\r\n", "\n\r", "\n", "\r"] as $delimiter_) {
        if (false !== strpos($content, $delimiter_)) {
            $delimiter = $delimiter_;
            break;
        }
    }

    if (null == $delimiter) {
        throw new InvalidArgumentException('can not use file');
    }

    return array_map(function ($line) {
        return str_getcsv($line, ';');
    }, explode($delimiter, $content));
}

$dimariCsv = readCsvFile('/tmp/nicht-importierte-rufnummern-da-nur-subcriber-id-und-nicht-in-telefonica-liste-zurueck.csv');

$dimariHeader = array_shift($dimariCsv);
$dimariHeaderReversed = [];

foreach ($dimariHeader as $column => $name) {
    $dimariHeaderReversed[$name] = $column;
}

// strip last empty line from csv-data
if (count(end($dimariCsv)) !== count($dimariHeader)) {
    array_pop($dimariCsv);
}


require __DIR__.'/../_conf/database.inc';


class PurtelAccount
{
    public $purtelLogin;
    public $purtelPassword;
    public $master;
    public $porting;
    public $nummer;
    public $nummernblock;
    public $areaCode;
    public $typ = 1;
    public $customerId;
    public $phoneBookInformation;
    public $phoneBookDigitalMedia;
    public $phoneBookInverseSearch;
    public $itemizedBill;
    public $itemizedBillAnonymized;
    public $itemizedBillShorted;
}


foreach ($dimariCsv as $key => $line) {
    $customerId = $line[1];

    $q = $db->query("SELECT * FROM purtel_account WHERE cust_id = ".$customerId);

    $requireMaster = false;
    $setMaster = false;

    if ($q->num_rows < 1) {
        $requireMaster = true;
    }

    for ($i = 2; $i < 6; $i++) {
        $rn = trim($line[$i]);

        if (empty($rn)) {
            continue;
        }

        ///////////
        $purtelAccount = new PurtelAccount();

        if (!$setMaster && $requireMaster) {
            $purtelAccount->master = 1;
            $setMaster = true;
        }

        $purtelAccount->nummer = preg_replace('/[^0-9]/', '', $rn);

        $areaCode = explode('-', $rn);

        if (count($areaCode) === 2) {
            $purtelAccount->areaCode = $areaCode[0];
        }

        //////////////////
        $query = sprintf('INSERT INTO `purtel_account` (`purtel_login`, `master`, `nummer`, `area_code`, `typ`, `cust_id`, 
            `phone_book_information`, `phone_book_digital_media`, `phone_book_inverse_search`, `itemized_bill`, 
            `itemized_bill_anonymized`, `itemized_bill_shorted`) 
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
            null === $purtelAccount->purtelLogin ? 'NULL' : '"'.$purtelAccount->purtelLogin.'"',
            1 == $purtelAccount->master ? '1' : 'NULL',
            '"'.$purtelAccount->nummer.'"',
            null === $purtelAccount->areaCode ? 'NULL' : '"'.$purtelAccount->areaCode.'"',
            $purtelAccount->typ,
            '"'.$customerId.'"',
            null === $purtelAccount->phoneBookInformation ? 'NULL' : $purtelAccount->phoneBookInformation,
            null === $purtelAccount->phoneBookDigitalMedia ? 'NULL' : $purtelAccount->phoneBookDigitalMedia,
            null === $purtelAccount->phoneBookInverseSearch ? 'NULL' : $purtelAccount->phoneBookInverseSearch,
            null === $purtelAccount->itemizedBill ? 'NULL' : $purtelAccount->itemizedBill,
            null === $purtelAccount->itemizedBillAnonymized ? 'NULL' : $purtelAccount->itemizedBillAnonymized,
            null === $purtelAccount->itemizedBillShorted ? 'NULL' : $purtelAccount->itemizedBillShorted
        );

        $db->query($query);

        if (!empty($db->error)) {
            throw new Exception($db->error);
        }
    }
}
