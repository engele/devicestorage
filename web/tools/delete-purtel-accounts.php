<?php


require __DIR__.'/../_conf/database.inc';
require_once __DIR__.'/../vendor/wisotel/configuration/Configuration.php';

$purtelSuperUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$purtelSuperPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');


$query = $db->query("SELECT * FROM `customers` WHERE `id` BETWEEN 5802 AND 6246");

while ($d = $query->fetch_assoc()) {
    $postData = [
        'kundennummer_extern' => $d['clientid'],
    ];

    $url = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelSuperUsername),
        urlencode($purtelSuperPassword),
        'cancelcontract'
    );

    $url .= '&'.utf8_decode(http_build_query($postData));


    $curl = curl_init($url);

    curl_setopt_array($curl, array(
        //CURLOPT_URL             => $PurtelUrl1,
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $result = trim($result);
    $result = explode(';', $result);

    if ('+OK' !== $result[0]) {
        var_dump('Failed to create sip', $result, $userId);
        echo $url."\n\n";

        continue;
    }
}

