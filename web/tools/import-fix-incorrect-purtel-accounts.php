<?php

/*
nummer darf nur 1x existieren
purtel_login darf nur 1x existieren
*/


require __DIR__.'/../_conf/database.inc';

$query = $db->query("SELECT pa.* FROM `purtel_account` pa INNER JOIN `customers` c ON c.`id` = pa.`cust_id` ORDER BY pa.`id` ASC");


$accounts = [];
$duplicates = []; // doppelte purtel_login
$delete = [];
$emptyPurtelLogin = [];

while ($data = $query->fetch_assoc()) {
    if (empty($data['purtel_login']) && empty($data['nummer']) && empty($data['nummernblock'])) {
        $delete[] = $data;

        continue;
    }

    if (empty($data['purtel_login']) && empty($data['nummer'])) {
        var_dump($data);
        die('empty');
    }

    if (empty($data['purtel_login'])) {
        if (!isset($emptyPurtelLogin[$data['cust_id']])) {
            $emptyPurtelLogin[$data['cust_id']] = [];
        }

        $emptyPurtelLogin[$data['cust_id']][] = $data;

        continue;
    }

    if (!isset($accounts[$data['purtel_login']])) {
        $accounts[$data['purtel_login']] = $data;
    } else {
        $duplicates[] = [
            'data' => $data,
            'duplicateOf' => $accounts[$data['purtel_login']],
        ];
    }
}

$duplicatesWithoutNumber = [];
$duplicatesWithNumber = [];

foreach ($duplicates as $duplicate) {
    if (empty($duplicate['data']['nummer'])) {
        // doppelter sip-account aber OHNE rufnummer

        $diff = array_diff($duplicate['data'], $duplicate['duplicateOf']);

        unset($diff['id']); // ignore this elements

        if (empty($diff)) {
            // tatsächlich alles doppelt. Kann gelöscht werden
            $delete[] = $duplicate['data'];

            continue;
        }

        // das hier erstmal von Hand machen
        $duplicate['diff'] = $diff;
        $duplicatesWithoutNumber[] = $duplicate;
    } else {
        // doppelter sip-account mit rufnummer
        // das hier erstmal von Hand machen
        $duplicatesWithNumber[] = $duplicate;
    }
}

// löschen überflüssige
foreach ($delete as $data) {
    $db->query("DELETE FROM `purtel_account` WHERE `id` = ".$data['id']);
}

// setzte "master" = 1 am ersten account - Nur, wenn kein anderes account der master ist!
foreach ($accounts as $data) {
    $q = $db->query("SELECT `id` FROM `purtel_account` WHERE `cust_id` = ".$data['cust_id']." AND `master` = 1 LIMIT 1");

    if ($q->num_rows < 1) {
        $db->query("UPDATE `purtel_account` SET `master` = 1 WHERE `id` = ".$data['id']);
    }
}

echo "Doppelte ohne Nummern:\n";
var_dump($duplicatesWithoutNumber);

echo "\n\n";

echo "Doppelte mit Nummern:\n";
var_dump($duplicatesWithNumber);


//var_dump($duplicates);
//var_dump($delete);

/*
foreach ($duplicates as $data) {
    if (empty($data['nummer'])) {
        // merge data (not number)
    } else {
        // check if an other account hast this number too
        
        $foundIn = [];
        
        if (isset($accounts[$data['purtel_login']])) {
            if ($data['nummer'] === $accounts[$data['purtel_login']]['nummer']) {
                $foundIn[] = $accounts[$data['purtel_login']];
            }
        }

        if (isset($emptyPurtelLogin[$data['cust_id']])) {
            foreach ($emptyPurtelLogin[$data['cust_id']] as $accountData) {
                if ($data['nummer'] === $accountData['nummer']) {
                    $foundIn[] = $accountData;
                }
            }
        }

        $countFoundIn = count($foundIn);

        if (1 === $countFoundIn) {
            // merge data (not number)
            //var_dump($data['nummer']);

            // compare purtel_login's
            var_dump($data['purtel_login'] === $countFoundIn[0]['purtel_login']);
        } else if ($countFoundIn > 1) {
            // hmm.. here is more to do
            var_dump($data['nummer']);
            die('here is more to do');
        } else {
            // not found
            // no other account has this number
            // check if there is an account with this number as purtel_login
            $possiblePurtelLogin = preg_replace('/[^0-9]/', '', $data['nummer']);
            $possiblePurtelLogin = preg_replace('/^0/', '', $possiblePurtelLogin);

            if (isset($accounts[$possiblePurtelLogin])) {
                // found an account with this number as purtel_login
                if (!empty($accounts[$possiblePurtelLogin]['nummer'])) {
                    var_dump($accounts[$possiblePurtelLogin], $data['nummer']);
                    die(var_dump('hmm.. this account already has a number..'));
                }

                // merge number and data to account
                //var_dump($data['nummer']);
            } else {
                // not event this number as purtel_login was found, therefore we need to keep this account
            }

            
        }
    }
}
*/
