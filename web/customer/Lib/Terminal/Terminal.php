<?php

/**
 *
 */

namespace customer\Lib\Terminal;

/**
 *
 */
class Terminal
{
    /**
     * @var string
     */
    private $remoteUser;

    /**
     * @var string
     */
    private $remotePassword;

    /**
     * @var string
     */
    private $remotePort;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $macAddress;

    /**
     * @var string
     */
    private $serialnumber;

    /**
     * @var \customer\Lib\Customer\Customer
     */
    private $customer;

    /**
     * @var \customer\Lib\IpAddress\IpAddress
     */
    private $ipAddress;

    /**
     * Don't know what this stands for
     * 
     * @var string
     */
    private $tvId;

    /**
     * Don't know what this stands for
     * 
     * @var string
     */
    private $checkId;

    /**
     * Activation date
     * 
     * @var \DateTime
     * 
     * @ORM\Column(type="date", nullable=true)
     */
    private $activationDate;

    /**
     * Alias for __construct
     * 
     * @return Terminal
     */
    public static function newInstance()
    {
        return new self();
    }

    /**
     * Set check-id
     * 
     * @param string $checkId
     * 
     * @return Terminal
     */
    public function setCheckId($checkId)
    {
        $this->checkId = $checkId;

        return $this;
    }

    /**
     * Set tv-id
     * 
     * @param string $tvId
     * 
     * @return Terminal
     */
    public function setTvId($tvId)
    {
        $this->tvId = $tvId;

        return $this;
    }

    /**
     * Set ipAddress
     * 
     * @param \customer\Lib\IpAddress\IpAddress|null $ipAddress
     * 
     * @return Terminal
     */
    public function setIpAddress(\customer\Lib\IpAddress\IpAddress $ipAddress = null)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Set customer
     * 
     * @param \customer\Lib\Customer\Customer $customer
     * 
     * @return Terminal
     */
    public function setCustomer(\customer\Lib\Customer\Customer $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Set serialnumber
     * 
     * @param string $serialnumber
     * 
     * @return Terminal
     */
    public function setSerialnumber($serialnumber)
    {
        $this->serialnumber = $serialnumber;

        return $this;
    }

    /**
     * Set mac-address
     * 
     * @param string $macAddress
     * 
     * @return Terminal
     */
    public function setMacAddress($macAddress)
    {
        $this->macAddress = $macAddress;

        return $this;
    }

    /**
     * Set type
     * 
     * @param string $type
     * 
     * @return Terminal
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set remote-port
     * 
     * @param string $remotePort
     * 
     * @throws \Exception
     * 
     * @return Terminal
     */
    public function setRemotePort($remotePort)
    {
        if (null !== $remotePort && !is_numeric($remotePort)) {
            throw new \Exception('Invalid value for remotePort');
        }

        $this->remotePort = null === $remotePort ? null : (int) $remotePort;

        return $this;
    }

    /**
     * Set remote-password
     * 
     * @param string $remotePassword
     * 
     * @return Terminal
     */
    public function setRemotePassword($remotePassword)
    {
        $this->remotePassword = $remotePassword;

        return $this;
    }

    /**
     * Set remote-user
     * 
     * @param string $remoteUser
     * 
     * @return Terminal
     */
    public function setRemoteUser($remoteUser)
    {
        $this->remoteUser = $remoteUser;

        return $this;
    }

    /**
     * Set activationDate.
     *
     * @param \DateTime|null $activationDate
     *
     * @return CustomerOption
     */
    public function setActivationDate($activationDate = null)
    {
        if (isset($activationDate)) {
            $activationDate = date('Y-m-d', strtotime($activationDate));
        }
        $this->activationDate = $activationDate;

        return $this;
    }

    /**
     * Get check-id
     * 
     * @return string
     */
    public function getCheckId()
    {
        return $this->checkId;
    }

    /**
     * Get tv-id
     * 
     * @return string
     */
    public function getTvId()
    {
        return $this->tvId;
    }

    /**
     * Get ipAddress
     * 
     * @return \customer\Lib\IpAddress\IpAddress
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Get customer
     * 
     * @return string
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Get serialnumber
     * 
     * @return string
     */
    public function getSerialnumber()
    {
        return $this->serialnumber;
    }

    /**
     * Get mac-address
     * 
     * @return string
     */
    public function getMacAddress()
    {
        return $this->macAddress;
    }

    /**
     * Get type
     * 
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get remote-port
     * 
     * @return string
     */
    public function getRemotePort()
    {
        return $this->remotePort;
    }

    /**
     * Get remote-password
     * 
     * @return string
     */
    public function getRemotePassword()
    {
        return $this->remotePassword;
    }

    /**
     * Get remote-user
     * 
     * @return string
     */
    public function getRemoteUser()
    {
        return $this->remoteUser;
    }

    /**
     * Get activationDate.
     *
     * @return \DateTime|null
     */
    public function getActivationDate()
    {
        return $this->activationDate;
    }

    /**
     * Get activationDateFormat.
     *
     * @return \DateTime|null
     */
    public function getActivationDateFormat()
    {
        if (isset($this->activationDate)) {
            $this->activationDate = date('d.m.Y', strtotime($this->activationDate));
        }
        return $this->activationDate;
    }

}

?>
