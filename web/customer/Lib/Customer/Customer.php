<?php
/**
 * 
 */

namespace customer\Lib\Customer;

require_once __DIR__.'/../IpAddress/IpAddress.php';
require_once __DIR__.'/../IpAddress/IpAddressCollection.php';
require_once __DIR__.'/../IpAddress/DummyIpRange.php';
require_once __DIR__.'/History/History.php';
require_once __DIR__.'/History/EventList.php';
require_once __DIR__.'/../../../administration/vendor/propertyreader/propertyreader/PropertyReader.php';

use \customer\Lib\IpAddress\IpAddress;
use \customer\Lib\IpAddress\IpAddressCollection;
use \customer\Lib\IpAddress\IpRangeCollection;
use \customer\Lib\IpAddress\DummyIpRange;
use \customer\Lib\Terminal\Terminal;
use \customer\Lib\Customer\History\History;
use \customer\Lib\Customer\History\EventList as HistoryEventList;
use PropertyReader\PropertyReader;

/**
 * Currently only ipAddresses!
 */
class Customer
{
	/**
	 * Customer Id
	 * 
	 * @var integer
	 */
	private $id;

	/**
	 * Collection of ipAddresses
	 * 
	 * @var IpAddressCollection
	 */
	private $ipAddresses;

    /**
     * Collection of terminals
     * 
     * @var \ArrayIterator
     */
    private $terminals;

    /**
     * History-Collection of Customer
     * 
     * @var \ArrayIterator
     */
    private $history;

	/**
	 * Instance of mysqli
	 * 
	 * @var \mysqli
	 */
	private $db;

	/**
	 * $sql from _conf/database.inc
	 * 
	 * @var array
	 */
	private $sql;

    /**
     * 
     */
    protected $technicalData;

	/**
	 * Constructor
	 * 
	 * @param \mysqli $db
	 * @param array   $sql
	 */
	public function __construct(\mysqli $db, array $sql)
	{
		$this->db = $db;
		$this->sql = $sql;

        $this->terminals = new \ArrayIterator();
        $this->history = new \ArrayIterator();
	}

    /**
     * Set technical data
     * 
     * @param \AppBundle\Entity\Customer\TechnicalData $technicalData
     * 
     * @return Customer
     */
    public function setTechnicalData(\AppBundle\Entity\Customer\TechnicalData $technicalData) : Customer
    {
        $this->technicalData = $technicalData;

        return $this;
    }

    /**
     * Get id
     * 
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * Fetch data from database and return prefilled customer object (Currently only ipAddresses!)
	 * 
	 * @todo
     *  - technicalData
	 * 
	 * @param \mysqli           $db
	 * @param array             $sql
	 * @param integer           $customerId
	 * @param IpRangeCollection $ipRangeCollection
	 * 
	 * @throws \InvalidArgumentException - if $customerId not integer
	 * 
	 * @return Customer
	 */
	public static function fetchInstance(\mysqli $db, array $sql, $customerId, IpRangeCollection $ipRangeCollection)
	{
		if (!is_int($customerId)) {
			throw new \InvalidArgumentException('CustomerId must be an integer.');
		}

        $customer = new Customer($db, $sql);

        // Get ipAddresses
		$ipAddresses = [];

		$statement = $db->prepare($sql['select_customers_ip_addresses']);
        $statement->bind_param('i', $customerId);
        $statement->execute();

        $dataResult = $statement->get_result();

        while ($data = $dataResult->fetch_assoc()) {
        	$ipAddresses[] = IpAddress::newInstance()
				->setIpAddress($data['ip_address'])
                ->setIpRange(
                	$ipRangeCollection->findRangeById($data['ip_range_id'])
                )
                ->setSubnetmask($data['subnetmask'])
                ->setGateway($data['gateway'])
                ->setReverseDns($data['rdns'])
			;
        }
       	
        $dataResult->free();
        $statement->close();

        $customer->setIpAddresses(
        	new IpAddressCollection($ipAddresses)
        );

        // Get terminals
        $terminals = $customer->getTerminals();

        $statement = $db->prepare($sql['select_customers_terminals']);
        $statement->bind_param('i', $customerId);
        $statement->execute();

        $dataResult = $statement->get_result();

        while ($data = $dataResult->fetch_assoc()) {
            $terminalIpAddress = $data['ip_address'];

            if (!empty($terminalIpAddress)) {
                $terminalIpAddress = $customer->getIpAddresses()->findByAddress($terminalIpAddress);

                if (null == $terminalIpAddress) { // Ip wasn't within additional ipAddresses
                    // therefore it must be the former ip_address
                    $terminalIpAddress = $ipRangeCollection->findIpAddressObjectByAddress($data['ip_address']);
                }
            }

            $terminals->append(Terminal::newInstance()
                ->setCustomer($customer)
                ->setRemoteUser($data['remote_user'])
                ->setRemotePassword($data['remote_password'])
                ->setRemotePort($data['remote_port'])
                ->setType($data['type'])
                ->setMacAddress($data['mac_address'])
                ->setSerialnumber($data['serialnumber'])
                ->setIpAddress($terminalIpAddress)
                ->setTvId($data['tv_id'])
                ->setCheckId($data['check_id'])
                ->setActivationDate($data['activation_date'])
            );
        }
        
        $dataResult->free();
        $statement->close();

        // Get history
        $historyEventList = new HistoryEventList($db, $sql);

        $statement = $db->prepare($sql['select_history_by_customer']);
        $statement->bind_param('i', $customerId);
        $statement->execute();

        $dataResult = $statement->get_result();


        while ($data = $dataResult->fetch_assoc()) {
            $history = History::newInstance()
                ->setCreatedBy($data['created_by'])
                ->setCreatedByFullname($data['fullname'])
                ->setCreatedAt(new \DateTime($data['created_at']))
                ->setNote($data['note'])
                ->setEvent($historyEventList->getEventById($data['event_id']))
            ;

            $id = & PropertyReader::newInstance()->read($history, 'id');
            $id = $data['id'];

            $customer->addHistory($history);
        }
        
        $dataResult->free();
        $statement->close();

        return $customer;
	}

	/**
	 * Set id
	 * 
	 * @param integer $id
	 * 
	 * @throws \BadMethodCallException
	 * 
	 * @return Customer
	 */
	public function setId($id)
	{
		if (null !== $this->id) {
			throw new \BadMethodCallException('CustomerId can not be overridden.');
		}

		$this->id = $id;

		return $this;
	}

	/**
	 * Set ipAddress collection
	 * 
	 * @param IpAddressCollection $ipAddressCollection
	 * 
	 * @return Customer
	 */
	public function setIpAddresses(IpAddressCollection $ipAddressCollection)
	{
		$this->ipAddresses = $ipAddressCollection;

		return $this;
	}

    /**
     * Set terminal collection
     * 
     * @param \ArrayIterator $terminals
     * 
     * @return Customer
     */
    public function setTerminals(\ArrayIterator $terminals)
    {
        foreach ($terminals as $terminal) {
            if (!($terminal instanceof Terminal)) {
                throw new \Exception('Invalid element within $terminals. '
                    .'All elements must be an instance of \\customer\\Lib\\Terminal\\Terminal');
            }
        }

        $this->terminals = $terminals;

        return $this;
    }

	/**
	 * Add ipAddress to ipAddress collection
	 * 
	 * @param IpAddress $ipAddress
	 * 
	 * @return Customer
	 */
	public function addIpAddress(IpAddress $ipAddress)
	{
		$ipAddresses = $this->ipAddresses->toArray();

		$ipAddresses[] = $ipAddress;

		$this->ipAddresses = new IpAddressCollection($ipAddresses);

		return $this;
	}

    /**
     * Add terminal to terminal collection
     * 
     * @param Terminal $terminal
     * 
     * @return Customer
     */
    public function addTerminal(Terminal $terminal)
    {
        $terminal->setCustomer($this); // Set customer to terminal

        $this->terminals->append($terminal);

        return $this;
    }

	/**
	 * Get ipAddresse collection
	 * 
	 * @return IpAddressCollection
	 */
	public function getIpAddresses()
	{
		return $this->ipAddresses;
	}

    /**
     * Get terminal collection
     * 
     * @return \ArrayIterator
     */
    public function getTerminals()
    {
        return $this->terminals;
    }

    /**
     * Get history collection
     * 
     * @return \ArrayIterator
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * Get history by id
     * 
     * @param integer $id
     * 
     * @return History|null
     */
    public function getHistoryById($id)
    {
        foreach ($this->history as $history) {
            if ($id === $history->getId()) {
                return $history;
            }
        }

        return null;
    }

    /**
     * Add History to history collection
     * 
     * @param History $history
     * 
     * @return Customer
     */
    public function addHistory(History $history)
    {
        $history->setCustomer($this);

        $this->history->append($history);

        return $this;
    }

	/**
	 * Save to database (Currently only ipAddresses!)
	 * 
	 * @todo
	 * 		Add exceptions
	 * 
	 * @throws \Exception - if insert query failes
	 * 
	 * @return Customer
	 */
	public function persist()
	{
        if (null == $this->id) {
            throw new \Exception('Can not persist customers data without a customerId');
        }

        /**
         * IpAddresses
         */
		$statement = $this->db->prepare($this->sql['delete_customers_ip_addresses']);
        $statement->bind_param('d', $this->id);
        $statement->execute();
		
		$valuesPlaceholder = "(%s, %s, %s, %s, %s, %d, %d),";
		$values = "";

		foreach ($this->ipAddresses as $ipAddress) {
			$gateway = $ipAddress->getGateway();
			$gateway = null !== $gateway ? "'".$gateway."'" : "NULL";

			$vlanId = $ipAddress->getVlanId();
			$vlanId = null !== $vlanId ? "'".$vlanId."'" : "NULL";

			$reverseDns = $ipAddress->getReverseDns();
			$reverseDns = null !== $reverseDns ? "'".$reverseDns."'" : "NULL";

			$values .= sprintf(
				$valuesPlaceholder,
				"'".$ipAddress->getIpAddress()."'",
				"'".$ipAddress->getSubnetmask()."'",
				$gateway,
				$vlanId,
				$reverseDns,
				$ipAddress->getIpRange()->getId(),
				$this->id
			);
		}

		if (!empty($values)) {
			$values = substr($values, 0, -1);

			$result = $this->db->query(sprintf(
				$this->sql['insert_customers_ip_addresses'],
				$values
			));

			if (!$result) {
				throw new \Exception('Failed to persist customer. - '.$this->db->error.' -- '.sprintf(
				    $this->sql['insert_customers_ip_addresses'],
				    $values
                ));
			}
		}

        /**
         * Terminals
         */
        $statement = $this->db->prepare($this->sql['delete_customers_terminals']);
        $statement->bind_param('d', $this->id);
        $statement->execute();

        $valuesPlaceholder = "(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %d),";
        $values = "";

        foreach ($this->terminals as $terminal) {
            $stringOrNullValues = [
                $terminal->getIpAddress() instanceof IpAddress ? $terminal->getIpAddress()->getIpAddress() : null,
                $terminal->getType(),
                $terminal->getMacAddress(),
                $terminal->getSerialnumber(),
                $terminal->getRemoteUser(),
                $terminal->getRemotePassword(),
                $terminal->getRemotePort(),
                $terminal->getTvId(),
                $terminal->getCheckId(),
                $terminal->getActivationDate(),
            ];

            foreach ($stringOrNullValues as $key => $value) {
                $stringOrNullValues[$key] = null !== $value ? "'".$value."'" : "NULL";
            }

            $stringOrNullValues[] = $this->id;

            $values .= vsprintf($valuesPlaceholder, $stringOrNullValues);
        }

        if (!empty($values)) {
            $values = substr($values, 0, -1);

            $result = $this->db->query(sprintf(
                $this->sql['insert_customers_terminals'],
                $values
            ));

            if (!$result) {
                throw new \Exception('Failed to persist customer. - '.$this->db->error.' -- '.sprintf(
                    $this->sql['insert_customers_terminals'],
                    $values
                ));
            }
        }

        /**
         * History
         */        
        $statement = $this->db->prepare($this->sql['insert_history']);

        foreach ($this->history as $key => $history) {
            if (null == $history->getId()) { // insert
                $statement->bind_param('iisss', 
                    $this->id,
                    $history->getEvent()->getId(),
                    $history->getCreatedBy(),
                    $history->getCreatedAt()->format('Y-m-d H:i:s'),
                    $history->getNote()
                );

                if (!$statement->execute()) {
                    throw new \Exception('Failed to persist customer. - '.$statement->error);
                }

                $id = & PropertyReader::newInstance()->read($history, 'id');
                $id = $statement->insert_id;
            }
        }

		return $this;
	}
}

?>
