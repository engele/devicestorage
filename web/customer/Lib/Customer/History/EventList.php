<?php

/*
 *
 */

namespace customer\Lib\Customer\History;

require_once __DIR__.'/../../../../administration/vendor/propertyreader/propertyreader/PropertyReader.php';
require_once __DIR__.'/Event.php';

use PropertyReader\PropertyReader;

/**
 *
 */
class EventList
{
    /**
     * List of events
     * 
     * @var array
     */
    protected static $eventList;

    /**
     * Constructor
     * 
     * @param \mysqli $db
     * @param array   $sql
     */
    public function __construct(\mysqli $db = null, $sql = null)
    {
        if (null == self::$eventList) {
            if (!($db instanceOf \mysqli)) {
                throw new \InvalidArgumentException('required argument $db must be instance of \\mysqli');
            }

            if (!is_array($sql) || !isset($sql['select_history_events'])) {
                throw new \InvalidArgumentException('required argument $sql must be typeOf array and requires key "select_history_events"');
            }

            $objectToDatabasePropertyMappings = array(
                // objectPropertyName => databasePropertyName
                'id' => 'id',
                'event' => 'event',
                'group' => 'group_id',
            );

            $statement = $db->prepare($sql['select_history_events']);
            $statement->execute();

            $dataResult = $statement->get_result();

            while ($data = $dataResult->fetch_assoc()) {
                $event = new Event();

                foreach ($objectToDatabasePropertyMappings as $objectPropertyName => $databasePropertyName) {
                    $property = & PropertyReader::newInstance()->read($event, $objectPropertyName);
                    $property = $data[$databasePropertyName];
                }

                self::$eventList[$event->getId()] = $event;
            }
            
            $dataResult->free();
            $statement->close();
        }
    }

    /**
     * Get event list
     * 
     * @return array
     */
    public function getEventList()
    {
        return self::$eventList;
    }

    /**
     * Get Event by id
     * 
     * @return Event|null
     */
    public function getEventById($id)
    {
        if (isset(self::$eventList[$id])) {
            return self::$eventList[$id];
        }

        return null;
    }

    /**
     * Get event list as assoziative array ordered by group (groups as keys).
     * Ungrouped events will be in "ungrouped"
     * 
     * @return array
     */
    public function getGroupedEventList()
    {
        $eventList = [
            'ungrouped' => [],
        ];

        foreach (self::$eventList as $event) {
            $group = $event->getGroup();
            $group = null === $group ? 'ungrouped' : $group;

            if (!isset($eventList[$group])) {
                $eventList[$group] = [];
            }

            $eventList[$group][] = $event;
        }

        return $eventList;
    }
}

?>