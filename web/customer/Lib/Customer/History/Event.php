<?php

/*
 *
 */

namespace customer\Lib\Customer\History;

/**
 *
 */
class Event
{
    /**
     * Id of Event
     * 
     * @var array
     */
    private $id;

    /**
     * Name of Event
     * 
     * @var string
     */
    private $event;

    /**
     * Event belongs to this group
     * 
     * @var string|null
     */
    private $group;

    /**
     * Get id
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get event
     * 
     * @return string
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Get group
     * 
     * @return string|null
     */
    public function getGroup()
    {
        return $this->group;
    }
}

?>