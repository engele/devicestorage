<?php

/*
 *
 */

namespace customer\Lib\Customer\History;

require_once __DIR__.'/Event.php';

use \customer\Lib\Customer\Customer;

/**
 * Customers History
 */
class History
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var Event
     */
    protected $event;

    /**
     * @var string
     */
    protected $note;

    /**
     * User-Account
     * 
     * @var string
     */
    protected $createdBy;

    /**
     * User-Account Fullname
     * 
     * @var string
     */
    protected $createdByFullname;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var Customer
     */
    protected $customer;

    /**
     * Alias for constructor
     * 
     * @return History
     */
    public static function newInstance()
    {
        return new self();
    }

    /**
     * Get customer
     * 
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Get created at
     * 
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get created by
     * 
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Get created by fullname
     * 
     * @return string
     */
    public function getCreatedByFullname()
    {
        return $this->createdByFullname;
    }

    /**
     * Get note
     * 
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Get event
     * 
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Get id
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created at
     * 
     * @param \DateTime $datetime
     * 
     * @return History
     */
    public function setCreatedAt(\DateTime $datetime)
    {
        $this->createdAt = $datetime;

        return $this;
    }

    /**
     * Set created by
     * 
     * @param int $userId
     * 
     * @return History
     */
    public function setCreatedBy($userId)
    {
        $this->createdBy = $userId;

        return $this;
    }

    /**
     * Set created by fullname
     * 
     * @param int $userId
     * 
     * @return History
     */
    public function setCreatedByFullname($fullname)
    {
        $this->createdByFullname = $fullname;

        return $this;
    }

    /**
     * Set note
     * 
     * @param string $note
     * 
     * @return History
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Set event
     * 
     * @param Event $event
     * 
     * @return History
     */
    public function setEvent(Event $event)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Set customer
     * 
     * @param Customer $customer
     * 
     * @return History
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;

        return $this;
    }
}

?>