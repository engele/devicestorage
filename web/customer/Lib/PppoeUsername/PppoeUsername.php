<?php

require_once __DIR__.'/../../../vendor/wisotel/configuration/Configuration.php';

/**
 * 
 */
class PppoeUsername
{
    /**
     * Create PPPoE-Username from PPPoE-PIN and customer-client-id
     * 
     * @param string|null $pppoePin
     * @param string $customerClientId
     *
     * @return string
     */
    public static function create($pppoePin, $customerClientId)
    {
        $pppoeUsername = $customerClientId;

        if (!empty($pppoePin)) {
            $firstMark = substr($pppoePin, 0, 3);

            $pppoeUsername = $firstMark.'/'.substr($pppoePin, 3, 6);
        }

        $realm = self::getRealm($pppoeUsername, $customerClientId);

        if (null !== $realm) {
            return $pppoeUsername.'@'.$realm;
        }

        return $pppoeUsername;
    }

    /**
     * Get realm
     * 
     * @param string $pppoeUsername
     * @param string $customerClientId
     * 
     * @return string
     */
    public static function getRealm($pppoeUsername, $customerClientId)
    {
        if (!empty($pppoeUsername) && !empty($customerClientId)) {
            if ($pppoeUsername !== $customerClientId) {
                // only use old if it is certaily not equal
                return \Wisotel\Configuration\Configuration::get('purtelSipRealmOld');
            }
        }

        return \Wisotel\Configuration\Configuration::get('purtelSipRealm');
    }
}
