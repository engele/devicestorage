<?php
/**
 *
 */

namespace customer\Lib\IpAddress;

/**
 *
 */
class IpAddress
{
	/**
	 * IPv4 dotted decimal notation
	 * 
	 * @var string
	 */
	private $ipAddress;

	/**
	 * Subnetmask of ip-range
	 * 
	 * @var string
	 */
	private $subnetmask;

	/**
	 * VlanId of ip-range
	 * 
	 * @var integer|null
	 */
	private $vlanId;

	/**
	 * IpRange of this ipAddress
	 * 
	 * @var IpRange
	 */
	private $ipRange;

	/**
	 * Gateway of ip-range
	 * 
	 * @var string
	 */
	private $gateway;

	/**
	 * reverseDNS of ip-range
	 * 
	 * @var string
	 */
	private $reverseDns;

	/**
	 * Constructor
	 */
	public function __construct()
	{
	}

	/**
	 * Alias for constructor
	 * 
	 * @return IpAddress
	 */
	public static function newInstance()
	{
		return new self;
	}

	/**
	 * Set ipAddress
	 * 
	 * @param string $ipAddress - IPv4 dotted decimal notation
	 * 
	 * @throws \InvalidArgumentException
	 * 
	 * @return IpAddress
	 */
	public function setIpAddress($ipAddress)
	{
		$ipAddress = filter_var($ipAddress, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);

		if (!$ipAddress) {
			throw new \InvalidArgumentException('Invalid value for ipAddress');
		}

		$this->ipAddress = $ipAddress;

		return $this;
	}

	/**
	 * Set subnetmask
	 * 
	 * @param string $subnetmask - IPv4 dotted decimal notation
	 * 
	 * @throws \InvalidArgumentException
	 * 
	 * @return IpAddress
	 */
	public function setSubnetmask($subnetmask)
	{
		$subnetmask = filter_var($subnetmask, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);

		if (!$subnetmask) {
			throw new \InvalidArgumentException('Invalid value for subnetmask');
		}

		$this->subnetmask = $subnetmask;

		return $this;
	}

	/**
	 * Set VlanId
	 * 
	 * @param mixed|null - should be integer! (but will not be checked)
	 * 
	 * @return IpAddress
	 */
	public function setVlanId($vlanId)
	{
		$this->vlanId = $vlanId;

		return $this;
	}

	/**
	 * Set ipRange
	 * 
	 * @param IpRangeInterface $ipRange
	 * 
	 * @throws \InvalidArgumentException - if ipAddress not withing $ipRange
	 * 
	 * @return IpAddress
	 */
	public function setIpRange(IpRangeInterface $ipRange)
	{
		if ($ipRange->hasIpAddress($this)) {
			$this->ipRange = $ipRange;

			return $this;
		}

		throw new \InvalidArgumentException('IpRange can not be set for ipAddress because this ipAddress is not within given ipRange.');
	}

	/**
	 * Set gateway
	 * 
	 * @param string $gateway - IPv4 dotted decimal notation
	 * 
	 * @throws \InvalidArgumentException
	 * 
	 * @return IpAddress
	 */
	public function setGateway($gateway = null)
	{
		if (null !== $gateway) {
			$gateway = filter_var($gateway, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);

			if (!$gateway) {
				throw new \InvalidArgumentException('Invalid value for gateway');
			}
		}

		$this->gateway = $gateway;

		return $this;
	}

	/**
	 * Set reverseDNS
	 * 
	 * @param string $reverseDns
	 * 
	 * @return IpAddress
	 */
	public function setReverseDns($reverseDns)
	{
		$this->reverseDns = $reverseDns;

		return $this;
	}

	/**
	 * Get ipAddress
	 * 
	 * @throws \BadMethodCallException - if wasn`t set before getting
	 * 
	 * @return string
	 */
	public function getIpAddress()
	{
		if (null === $this->ipAddress) {
			throw new \BadMethodCallException('IpAddress not set');
		}

		return $this->ipAddress;
	}

	/**
	 * Get subnetmask
	 * 
	 * @throws \BadMethodCallException - if wasn`t set before getting
	 * 
	 * @return string - IPv4 dotted decimal notation
	 */
	public function getSubnetmask()
	{
		if (null === $this->subnetmask) {
			throw new \BadMethodCallException('Subnetmask not set');
		}

		return $this->subnetmask;
	}

	/**
	 * Get VlanId
	 * 
	 * @return mixed|null - should be integer (but wasn't actually checked) or null
	 */
	public function getVlanId()
	{
		return $this->vlanId;
	}

	/**
	 * Get ipRange
	 * 
	 * @throws \BadMethodCallException - if wasn`t set before getting
	 * 
	 * @return IpRange
	 */
	public function getIpRange()
	{
		if (null === $this->ipRange) {
			throw new \BadMethodCallException('IpRange not set');
		}

		return $this->ipRange;
	}

	/**
	 * Get gateway
	 * 
	 * @return string - IPv4 dotted decimal notation
	 */
	public function getGateway()
	{
		return $this->gateway;
	}

	/**
	 * Get reverseDNS
	 * 
	 * @return string
	 */
	public function getReverseDns()
	{
		return $this->reverseDns;
	}

	/**
	 * Convert object to string
	 * 
	 * @return strin
	 */
	public function __toString()
	{
		return $this->getIpAddress();
	}

	/**
	 * Convert object to long integer
	 * 
	 * @return integer
	 */
	public function toLong()
	{
		return ip2long($this->getIpAddress());
	}
}

?>