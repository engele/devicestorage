<?php
/**
 *
 */

namespace customer\Lib\IpAddress;

require_once __DIR__.'/IpRange.php';

/**
 *
 */
class IpRangeCollection implements \Iterator
{
	/**
	 * Id of network
	 * 
	 * @var integer
	 */
	private $networkId;

	/**
	 * Collection of ip-ranges from within networkId
	 * Same as $ipRangesIds but uses "startIp-endIp" as keys
	 * 
	 * @var array
	 */
	private $ipRanges;

	/**
	 * Collection of ip-ranges from within networkId.
	 * Same as $ipRanges but uses rangeId as keys
	 * 
	 * @var array
	 */
	private $ipRangesIds;

	/**
	 * Instance of mysqli
	 * 
	 * @var \mysqli
	 */
	private $db;

	/**
	 * $sql from _conf/database.inc
	 * 
	 * @var array
	 */
	private $sql;

	/**
	 * Constructor
	 * 
	 * @param \mysqli $db
	 * @param array   $sql - From _conf/database.inc
	 */
	public function __construct(\mysqli $db, array $sql)
	{
		$this->db = $db;
		$this->sql = $sql;
        $this->ipRanges = [];
        $this->ipRangesIds = [];
	}

	/**
	 * Fetch ip-ranges (addresses, name, subnetmask, gateway, vlan) from database
	 * 
	 * @param integer $networkId
	 * 
	 * @return IpRangeCollection
	 */
	public function fetchByNetworkId($networkId)
	{
		$this->setNetworkId($networkId);

		$statement = $this->db->prepare($this->sql['ip_ranges']);
        $statement->bind_param('d', $this->networkId);
        $statement->execute();

        $dataResult = $statement->get_result();

        while ($data = $dataResult->fetch_assoc()) {
        	$this->add(new IpRange($this, $this->db, $this->sql, $data));
        }
       	
        $dataResult->free();
        $statement->close();

        return $this;
	}

	/**
	 * Set networkId
	 * 
	 * @param integer $networkId
	 * 
	 * @throws \BadMethodCallException - if tryed to override networkId
	 * @throws \InvalidArgumentException - if networkId not integer
	 * 
	 * @return IpRangeCollection
	 */
	public function setNetworkId($networkId)
	{
		if (null !== $this->networkId) {
			throw new \BadMethodCallException('NetworkId was already set. Overriding is not allowed.');
		}

		if (!is_int($networkId)) {
    		throw new \InvalidArgumentException('Invalid networkId. Must be integer.');
    	}

		$this->networkId = $networkId;

		return $this;
	}

	/**
	 * Add ipRange to collection
	 * 
	 * @param IpRange $ipRange
	 * 
	 * @throws \InvalidArgumentException - if tryed to override ipRange
	 * 
	 * @return IpRangeCollection
	 */
	public function add(IpRange $ipRange)
	{
		try {
			$startIpAddress = $ipRange->getStartIpAddress();
			$endIpAddress = $ipRange->getEndIpAddress();
		} catch (\BadMethodCallException $e) {
			throw new \InvalidArgumentException('IpRange incomplete ('.$e->getMessage().').');
		}

		$identifier = $startIpAddress.'-'.$endIpAddress;

		if (isset($this->ipRanges[$identifier])) {
			throw new \InvalidArgumentException('IpRange already exists. Overriding is not allowed.');
		}

		$this->ipRanges[$identifier] = $ipRange;
		$this->ipRangesIds[$ipRange->getId()] = $ipRange;

		return $this;
	}

	/**
	 * Remove ipRange from collection
	 * 
	 * @param mixed $identifier - should look like: "startIp-endIp" (no spaces)
	 * 
	 * @return IpRangeCollection
	 */
	public function removeByIdentifier($identifier)
	{
		if (isset($this->ipRanges[$identifier])) {
			unset($this->ipRangesIds[$this->ipRanges[$identifier]->getId()]); // remove first
			unset($this->ipRanges[$identifier]);
		}

		return $this;
	}

	/**
	 * Remove ipRange from collection
	 * 
	 * @param mixed $rangeId
	 * 
	 * @return IpRangeCollection
	 */
	public function removeById($rangeId)
	{
		if (isset($this->ipRangesIds[$rangeId])) {
			$startIpAddress = $this->ipRangesIds[$rangeId]->getStartIpAddress();
			$endIpAddress = $this->ipRangesIds[$rangeId]->getEndIpAddress();
			$identifier = $startIpAddress.'-'.$endIpAddress;

			unset($this->ipRanges[$identifier]);
			unset($this->ipRangesIds[$rangeId]);
		}

		return $this;
	}

	/**
	 * Finds ipRange with corresponding id within this collection
	 * 
	 * @param mixed $rangeId
	 * 
	 * @param IpRange|null
	 */
	public function findRangeById($rangeId)
	{
		if (isset($this->ipRangesIds[$rangeId])) {
			return $this->ipRangesIds[$rangeId];
		}

		return null;
	}

	/**
	 * Get networkId
	 * 
	 * @return integer|null
	 */
	public function getNetworkId()
	{
		return $this->networkId;
	}

    /**
     * Find ipAddress-object by (string) ip-address
     * 
     * @param string $ipAddress
     * 
     * @return IpAddress|null
     */
    public function findIpAddressObjectByAddress($ipAddress)
    {
        $ipAddress = IpAddress::newInstance()->setIpAddress($ipAddress);

        foreach ($this->ipRanges as $ipRange) {
            if ($ipRange->hasIpAddress($ipAddress)) {
                return $ipAddress
                    ->setSubnetmask($ipRange->getSubnetmask())
                    ->setGateway($ipRange->getGateway())
                    ->setIpRange($ipRange)
                ;
            }
        }

        return null;
    }

	/**
	 * Convert this object to array
	 * 
	 * @return array
	 */
	public function toArray()
	{
		return $this->ipRangesIds;
	}

	/**
	 * Iterator method
	 * 
	 * @see http://php.net/manual/de/class.iterator.php
	 * 
	 * @return boolean
	 */
	public function valid()
	{
		return null !== key($this->ipRanges);
	}

	/**
	 * Iterator method
	 * 
	 * @see http://php.net/manual/de/class.iterator.php
	 * 
	 * @return void
	 */
	public function rewind()
	{
		reset($this->ipRanges);
	}

	/**
	 * Iterator method
	 * 
	 * @see http://php.net/manual/de/class.iterator.php
	 * 
	 * @return void
	 */
	public function next()
	{
		next($this->ipRanges);
	}

	/**
	 * Iterator method
	 * 
	 * @see http://php.net/manual/de/class.iterator.php
	 * 
	 * @return mixed
	 */
	public function current()
	{
		return current($this->ipRanges);
	}

	/**
	 * Iterator method
	 * 
	 * @see http://php.net/manual/de/class.iterator.php
	 * 
	 * @return mixed
	 */
	public function key()
	{
		return key($this->ipRanges);
	}
}

?>