<?php
/**
 * 
 */

namespace customer\Lib\IpAddress;

require_once __DIR__.'/IpRangeInterface.php';

/**
 * 
 */
class DummyIpRange implements IpRangeInterface
{
	/**
	 * Id of ip-range
	 * 
	 * @var integer
	 */
	private $id;

	/**
	 * Constructor
	 */
	public function __cunstruct()
	{
	}

	/**
	 * Alias for constructor
	 * 
	 * @return DummyIpRange
	 */
	public static function newInstance()
	{
		return new self;
	}

	/**
	 * Set id
	 * 
	 * @param mixed - should be integer! (but will not be checked)
	 * 
	 * @return IpRange
	 */
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * Get id
	 * 
	 * @throws \BadMethodCallException - if wasn`t set before getting
	 * 
	 * @return mixed - should be integer! (but wasn't actually checked)
	 */
	public function getId()
	{
		if (null === $this->id) {
			throw new \BadMethodCallException('Id not set');
		}

		return $this->id;
	}

	/**
	 * Is ipAddress within this range?
	 * 
	 * @param IpAddress $ipAddress
	 * 
	 * @return boolean
	 */
	public function hasIpAddress(IpAddress $ipAddress)
	{
		return true;
	}
}

?>
