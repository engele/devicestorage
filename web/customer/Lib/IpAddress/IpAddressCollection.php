<?php
/**
 *
 */

namespace customer\Lib\IpAddress;

/**
 * No adding or removing of idAddresses allowed
 */
class IpAddressCollection implements \Iterator
{
	/**
	 * Number of elements
	 * 
	 * @var integer
	 */
	public $count;

	/**
	 * Collection of ipAddresses
	 * 
	 * @var array
	 */
	private $ipAddresses;

	/**
	 * Constructor
	 * 
	 * @param array $ipAddresses - optional
	 */
	public function __construct(array $ipAddresses = [])
	{
		$keys = [];

		foreach ($ipAddresses as $ipAddress) {
			$keys[] = $ipAddress->getIpAddress();
		}

		$this->ipAddresses = array_combine($keys, $ipAddresses);
		$this->count = count($this->ipAddresses);
	}

	/**
	 * Is ipAddress in this collection?
	 * 
	 * @param IpAddress $ipAddress
	 * 
	 * @return boolean
	 */
	public function hasIpAddress(IpAddress $ipAddress)
	{
		/*
		 * Compare complete IpAddress object
		 * 
		 * if (false !== array_search($ipAddress, $this->toArray())) {
		 *     return true;
		 * }
		 * 
		 * return false;
		 */

		// Good enough for now
		return isset($this->ipAddresses[$ipAddress->getIpAddress()]);
	}

    /**
     * If address is in collection, return ipAddress-object.
     * If not, return null
     * 
     * @param string $ipAddress
     * 
     * @return IpAddress|null
     */
    public function findByAddress($ipAddress)
    {
        if (isset($this->ipAddresses[$ipAddress])) {
            return $this->ipAddresses[$ipAddress];
        }

        return null;
    }

	/**
	 * Get array of this collection
	 * 
	 * @return array
	 */
	public function toArray()
	{
		return $this->ipAddresses;
	}

	/**
	 * Iterator method
	 * 
	 * @see http://php.net/manual/de/class.iterator.php
	 * 
	 * @return boolean
	 */
	public function valid()
	{
		return null !== key($this->ipAddresses);
	}

	/**
	 * Iterator method
	 * 
	 * @see http://php.net/manual/de/class.iterator.php
	 * 
	 * @return void
	 */
	public function rewind()
	{
		reset($this->ipAddresses);
	}

	/**
	 * Iterator method
	 * 
	 * @see http://php.net/manual/de/class.iterator.php
	 * 
	 * @return void
	 */
	public function next()
	{
		next($this->ipAddresses);
	}

	/**
	 * Iterator method
	 * 
	 * @see http://php.net/manual/de/class.iterator.php
	 * 
	 * @return mixed
	 */
	public function current()
	{
		return current($this->ipAddresses);
	}

	/**
	 * Iterator method
	 * 
	 * @see http://php.net/manual/de/class.iterator.php
	 * 
	 * @return mixed
	 */
	public function key()
	{
		return key($this->ipAddresses);
	}
}

?>