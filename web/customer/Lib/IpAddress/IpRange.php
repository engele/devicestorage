<?php
/**
 *
 */

namespace customer\Lib\IpAddress;

require_once __DIR__.'/IpRangeInterface.php';
require_once __DIR__.'/IpAddress.php';
require_once __DIR__.'/IpAddressCollection.php';

/**
 * @todo
 * 		verifySubnetting()
 * 		calculateSubnetmask()
 */
class IpRange implements IpRangeInterface
{
	/**
	 * Collection of ipAddresses
	 * 
	 * No adding or removing of idAddresses allowed because it would acutally change the range and therefore wouldn't be this object anymore.
	 * 
	 * @var IpAddressCollection
	 */
	private $ipAddresses;

	/**
	 * Collection of available ipAddresses
	 * 
	 * @var IpAddressCollection
	 */
	private $availableIpAddresses;

	/**
	 * Collection of taken ipAddresses
	 * 
	 * @var IpAddressCollection
	 */
	private $takenIpAddresses;

	/**
	 * Id of ip-range
	 * 
	 * @var integer
	 */
	private $id;

	/**
	 * Name of ip-range
	 * 
	 * @var string
	 */
	private $name;

	/**
	 * Subnetmask of ip-range
	 * 
	 * @var string
	 */
	private $subnetmask;

    /**
     * Gateway of ip-range
     * 
     * @var string
     */
    private $gateway;

	/**
	 * VlanId of ip-range
	 * 
	 * @var integer|null
	 */
	private $vlanId;

	/**
	 * Start IpAddress of ip-range - IPv4 dotted decimal notation
	 * 
	 * @var string
	 */
	private $startIpAddress;

	/**
	 * End IpAddress of ip-range - IPv4 dotted decimal notation
	 * 
	 * @var string
	 */
	private $endIpAddress;

	/**
	 * Instance of parent IpRangeCollection
	 * 
	 * @var IpRangeCollection
	 */
	private $network;

	/**
	 * Instance of mysqli
	 * 
	 * @var \mysqli
	 */
	private $db;

	/**
	 * $sql from _conf/database.inc
	 * 
	 * @var array
	 */
	private $sql;

	/**
	 * Constructor
	 * 
	 * @param IpRangeCollection $network
	 * @param \mysqli           $db
	 * @param array             $sql - From _conf/database.inc
	 * @param array             $data - optional - Containing values for id, name, subnet, gateway, vlanId, startIp, endIp
	 * 
	 * @throws \InvalidArgumentException - only if $data is incomplete
	 */
	public function __construct(IpRangeCollection $network, \mysqli $db, array $sql, array $data = null)
	{
		$this->db = $db;
		$this->sql = $sql;
		$this->network = $network;

		if (null !== $data) {
			$expectedKeys = ['id', 'name', 'subnetmask', 'gateway', 'vlan_ID', 'start_address', 'end_address'];
			$missingKeys = array_diff($expectedKeys, array_keys($data));

			if (!empty($missingKeys)) {
				throw new \InvalidArgumentException(
					'$data does not contain all required elements. Missing keys are: '.implode(', ', $missingKeys)
				);
			}

			$this
				->setId($data['id'])
				->setName($data['name'])
                ->setSubnetmask($data['subnetmask'])
				->setGateway($data['gateway'])
				->setVlanId($data['vlan_ID'])
				->setStartIpAddress($data['start_address'])
				->setEndIpAddress($data['end_address'])
			;
		}
	}

	/**
	 * Compare computed subnetmask against $this->subnetmask
	 * 
	 * @todo
	 */
	public function verifySubnetting()
	{
	}

	/**
	 * Calculate subnetmask from startIp and endIp
	 * 
	 * @todo
	 */
	public function calculateSubnetmask()
	{
	}

	/**
	 * Create ipAddresses for this range
	 * 
	 * Load ipAddresses only if really used - to save memory
	 * 
	 * @todo
	 * 		At this current stage it will be assumed that the values for
	 * 		startIpAddress and endIpAddress are correct set!
	 * 
	 * @throws \InvalidArgumentException
	 * 
	 * @return void
	 */
	private function loadIpAddresses()
	{
		try {
			$startIpAddress2long = ip2long($this->getStartIpAddress());
			$endIpAddress2long = ip2long($this->getEndIpAddress());
		} catch (\BadMethodCallException $e) {
			throw new \InvalidArgumentException('IpRange incomplete ('.$e->getMessage().').');
		}

		if ($endIpAddress2long < $startIpAddress2long) {
			throw new \InvalidArgumentException('IpRange wrong. EndIpAddress is smaller than startIpAddress.');
		}

		$this->ipAddresses = $this->fetchAllIpAddresses($startIpAddress2long, $endIpAddress2long);
		$this->takenIpAddresses = $this->fetchTakenIpAddresses($startIpAddress2long, $endIpAddress2long);
		$this->availableIpAddresses = $this->fetchAvailableIpAddresses();
	}

	/**
	 * Fetch all ipAddresses for this range
	 * 
	 * @param integer $startIpAddress2long
	 * @param integer $endIpAddress2long
	 * 
	 * @return IpAddressCollection
	 */
	private function fetchAllIpAddresses($startIpAddress2long, $endIpAddress2long)
	{
		$ipAddresses = [];

		$ipAddress = $startIpAddress2long;

		while ($ipAddress <= $endIpAddress2long) {
			$ipAddresses[] = IpAddress::newInstance()
				->setIpAddress(long2ip($ipAddress))
                ->setSubnetmask($this->subnetmask)
				->setGateway($this->gateway)
				->setIpRange($this)
			;

			$ipAddress++;
		}

		return new IpAddressCollection($ipAddresses);
	}

	/**
	 * Fetch taken ipAddresses for this range
	 * 
	 * @param integer $startIpAddress2long
	 * @param integer $endIpAddress2long
	 * 
	 * @return IpAddressCollection
	 */
	private function fetchTakenIpAddresses($startIpAddress2long, $endIpAddress2long)
	{
		$ipAddresses = [];

		// Get taken from `customers`
		$statement = $this->db->prepare($this->sql['ip_addresses']);
        $statement->bind_param('dd', $startIpAddress2long, $endIpAddress2long);
        $statement->execute();

        $ipAddressesResult = $statement->get_result();

        while ($ipAddress = $ipAddressesResult->fetch_assoc()) {
        	$ipAddresses[] = IpAddress::newInstance()
        		->setIpAddress($ipAddress['ip_address'])
                ->setSubnetmask($this->subnetmask)
        		->setGateway($this->gateway)
        		->setIpRange($this)
        	;
        }
       	
        $ipAddressesResult->free();
        $statement->close();

        // Get taken from `ip_addresses`
        $statement = $this->db->prepare($this->sql['select_taken_ip_addresses_by_range']);
        $statement->bind_param('d', $this->id);
        $statement->execute();

        $ipAddressesResult = $statement->get_result();

        while ($ipAddress = $ipAddressesResult->fetch_assoc()) {
        	$ipAddresses[] = IpAddress::newInstance()
        		->setIpAddress($ipAddress['ip_address'])
                ->setSubnetmask($this->subnetmask)
        		->setGateway($this->gateway)
        		->setIpRange($this)
        	;
        }
       	
        $ipAddressesResult->free();
        $statement->close();

        // Get defective from `defective_ip_addresses`
        $statement = $this->db->prepare($this->sql['select_defective_ip_addresses']);
        $statement->bind_param('d', $this->id);
        $statement->execute();

        $ipAddressesResult = $statement->get_result();

        while ($ipAddress = $ipAddressesResult->fetch_assoc()) {
            $ipAddresses[] = IpAddress::newInstance()
                ->setIpAddress($ipAddress['value'])
                ->setSubnetmask($this->subnetmask)
                ->setGateway($this->gateway)
                ->setIpRange($this)
            ;
        }
        
        $ipAddressesResult->free();
        $statement->close();

        return new IpAddressCollection($ipAddresses);
	}

	/**
	 * Fetch available ipAddresses for this range
	 * 
	 * @return IpAddressCollection
	 */
	private function fetchAvailableIpAddresses()
	{
		return new IpAddressCollection(array_diff(
			$this->ipAddresses->toArray(),
			$this->takenIpAddresses->toArray()
		));
	}

	/**
	 * Get all ipAddresses
	 * 
	 * @return IpAddressCollection
	 */
	public function getAllIpAddresses()
	{
		if (null === $this->ipAddresses) {
			$this->loadIpAddresses();
		}

		return $this->ipAddresses;
	}

	/**
	 * Get available ipAddresses
	 * 
	 * @return IpAddressCollection
	 */
	public function getAvailableIpAddresses()
	{
		if (null === $this->availableIpAddresses) {
			$this->loadIpAddresses();
		}

		return $this->availableIpAddresses;
	}

	/**
	 * Get taken ipAddresses
	 * 
	 * @return IpAddressCollection
	 */
	public function getTakenIpAddresses()
	{
		if (null === $this->takenIpAddresses) {
			$this->loadIpAddresses();
		}

		return $this->takenIpAddresses;
	}

	/**
	 * Is ipAddress within this range?
	 * 
	 * @param IpAddress $ipAddress
	 * 
	 * @return boolean
	 */
	public function hasIpAddress(IpAddress $ipAddress)
	{
		/*
		 * Do not use:
		 * 		return $this->getAllIpAddresses()->hasIpAddress($ipAddress);
		 * 
		 * It will cause a memory leak!
		 *
		 * This is what will happen:
		 *
		 * IpRange::fetch***IpAddresses() calls
		 * IpAddress::setIpRange(IpRange) calls
		 * IpRange::hasIpAddress(IpAddress) calls
		 * IpRange::fetch***IpAddresses() again
		 */

		$startIpAddress2long = ip2long($this->getStartIpAddress());
		$endIpAddress2long = ip2long($this->getEndIpAddress());
		$ip2long = ip2long($ipAddress->getIpAddress());

		if ($ip2long >= $startIpAddress2long && $ip2long <= $endIpAddress2long) {
			return true;
		}

		return false;
	}

	/**
	 * Moves ipAddress from available to taken
	 * 
	 * @param IpAddress $ipAddress
	 * 
	 * @throws \InvalidArgumentException - if not in range
	 * 
	 * @return IpRange
	 */
	public function takeIpAddress(IpAddress $ipAddress)
	{
		if (!$this->hasIpAddress($ipAddress)) {
			throw new \InvalidArgumentException('The ipAddress you are trying to take is not within this range.');
		}

		if ($this->getAvailableIpAddresses()->hasIpAddress($ipAddress)) {
			// remove from available
			$availableIpAddresses = $this->getAvailableIpAddresses()->toArray();
			unset($availableIpAddresses[$ipAddress->getIpAddress()]);
			$this->availableIpAddresses = new IpAddressCollection($availableIpAddresses);

			// add to taken
			$takenIpAddresses = $this->getTakenIpAddresses()->toArray();
			$takenIpAddresses[] = $ipAddress;
			$this->takenIpAddresses = new IpAddressCollection($takenIpAddresses);
		}

		// Not in available? Then it must be in taken already

		return $this;
	}

	/**
	 * Moves ipAddress from taken to available
	 * 
	 * @param IpAddress $ipAddress
	 * 
	 * @throws \InvalidArgumentException - if not in range
	 * 
	 * @return IpRange
	 */
	public function releaseIpAddress(IpAddress $ipAddress)
	{
		if (!$this->hasIpAddress($ipAddress)) {
			throw new \InvalidArgumentException('The ipAddress you are trying to take is not within this range.');
		}

		if ($this->getTakenIpAddresses()->hasIpAddress($ipAddress)) {
			// remove from taken
			$takenIpAddresses = $this->getTakenIpAddresses()->toArray();
			unset($takenIpAddresses[$ipAddress->getIpAddress()]);
			$this->takenIpAddresses = new IpAddressCollection($takenIpAddresses);

			// add to available
			$availableIpAddresses = $this->getAvailableIpAddresses()->toArray();
			$availableIpAddresses[] = $ipAddress;
			$this->availableIpAddresses = new IpAddressCollection($availableIpAddresses);
		}

		// Not in taken? Then it must be in available already

		return $this;
	}

	/**
	 * Set id
	 * 
	 * @param mixed - should be integer! (but will not be checked)
	 * 
	 * @return IpRange
	 */
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * Set name
	 * 
	 * @param string $name
	 * 
	 * @throws \InvalidArgumentException
	 * 
	 * @return IpRange
	 */
	public function setName($name)
	{
		if (!is_string($name) || empty($name)) {
			throw new \InvalidArgumentException('Name must be a string and must not be empty');
		}

		$this->name = $name;

		return $this;
	}

	/**
	 * Set subnetmask
	 * 
	 * @param string $subnetmask - IPv4 dotted decimal notation
	 * 
	 * @throws \InvalidArgumentException
	 * 
	 * @return IpRange
	 */
	public function setSubnetmask($subnetmask)
	{
		$subnetmask = filter_var($subnetmask, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);

		if (!$subnetmask) {
			throw new \InvalidArgumentException('Invalid value for subnetmask');
		}

		$this->subnetmask = $subnetmask;

		return $this;
	}

    /**
     * Set gateway
     * 
     * @param string $gateway - IPv4 dotted decimal notation
     * 
     * @throws \InvalidArgumentException
     * 
     * @return IpRange
     */
    public function setGateway($gateway)
    {
        if (null !== $gateway) {
            $gateway = filter_var($gateway, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);

            if (!$gateway) {
                throw new \InvalidArgumentException('Invalid value for gateway');
            }
        }

        $this->gateway = $gateway;

        return $this;
    }

	/**
	 * Set VlanId
	 * 
	 * @param mixed|null - should be integer! (but will not be checked)
	 * 
	 * @return IpRange
	 */
	public function setVlanId($vlanId)
	{
		$this->vlanId = $vlanId;

		return $this;
	}

	/**
	 * Set startIpAddress
	 * 
	 * @param string $ipAddress - IPv4 dotted decimal notation
	 * 
	 * @throws \InvalidArgumentException
	 * 
	 * @return IpRange
	 */
	public function setStartIpAddress($ipAddress)
	{
		$ipAddress = filter_var($ipAddress, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);

		if (!$ipAddress) {
			throw new \InvalidArgumentException('Invalid value for ipAddress');
		}

		$this->startIpAddress = $ipAddress;

		return $this;
	}

	/**
	 * Set endIpAddress
	 * 
	 * @param string $ipAddress - IPv4 dotted decimal notation
	 * 
	 * @throws \InvalidArgumentException
	 * 
	 * @return IpRange
	 */
	public function setEndIpAddress($ipAddress)
	{
		$ipAddress = filter_var($ipAddress, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);

		if (!$ipAddress) {
			throw new \InvalidArgumentException('Invalid value for ipAddress');
		}

		$this->endIpAddress = $ipAddress;

		return $this;
	}

	/**
	 * Get id
	 * 
	 * @throws \BadMethodCallException - if wasn`t set before getting
	 * 
	 * @return mixed - should be integer! (but wasn't actually checked)
	 */
	public function getId()
	{
		if (null === $this->id) {
			throw new \BadMethodCallException('Id not set');
		}

		return $this->id;
	}

	/**
	 * Get name
	 * 
	 * @throws \BadMethodCallException - if wasn`t set before getting
	 * 
	 * @return string
	 */
	public function getName()
	{
		if (null === $this->name) {
			throw new \BadMethodCallException('Name not set');
		}

		return $this->name;
	}

	/**
	 * Get subnetmask
	 * 
	 * @throws \BadMethodCallException - if wasn`t set before getting
	 * 
	 * @return string - IPv4 dotted decimal notation
	 */
	public function getSubnetmask()
	{
		if (null === $this->subnetmask) {
			throw new \BadMethodCallException('Subnetmask not set');
		}

		return $this->subnetmask;
	}

    /**
     * Get gateway
     * 
     * @return string - IPv4 dotted decimal notation
     */
    public function getGateway()
    {
        return $this->gateway;
    }

	/**
	 * Get VlanId
	 * 
	 * @return mixed|null - should be integer (but wasn't actually checked) or null
	 */
	public function getVlanId()
	{
		return $this->vlanId;
	}

	/**
	 * Get startIpAddress
	 * 
	 * @throws \BadMethodCallException - if wasn`t set before getting
	 * 
	 * @return string - IPv4 dotted decimal notation
	 */
	public function getStartIpAddress()
	{
		if (null === $this->startIpAddress) {
			throw new \BadMethodCallException('StartIpAddress not set');
		}

		return $this->startIpAddress;
	}

	/**
	 * Get endIpAddress
	 * 
	 * @throws \BadMethodCallException - if wasn`t set before getting
	 * 
	 * @return string - IPv4 dotted decimal notation
	 */
	public function getEndIpAddress()
	{
		if (null === $this->endIpAddress) {
			throw new \BadMethodCallException('EndIpAddress not set');
		}

		return $this->endIpAddress;
	}

	/**
	 * Return serialized array
	 * 
     * @return string
     */
    public function serialize()
    {
        return serialize([
            'id' => $this->id,
            'name' => $this->name,
            'subnetmask' => $this->subnetmask,
            'gateway' => $this->gateway,
            'vlanId' => $this->vlanId,
            'startIpAddress' => $this->startIpAddress,
            'endIpAddress' => $this->endIpAddress,
            'networkId' => $this->network->getNetworkId(),
        ]);
    }
}

?>