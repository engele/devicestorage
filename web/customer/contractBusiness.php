<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_CONTRACT');

$disable = 'readonly="readonly"';
$rw = false;   

if ($authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_CONTRACT')) {
    $disable = '';   
    $rw = true;   
}

?>

<h3 class="CustContractBusiness">Vertragsdaten (Geschäftskunden)</h3>
<div class="CustContractBusiness form">
  <span>
    <label for="CustomerClientid07"><?php echo \Wisotel\Configuration\Configuration::get('companyName'); ?> Nr.</label><br />
    <input id="CustomerClientid07" type="text" value="<?php setInputValue ($customer, 'clientid'); ?>" maxlength="64" readonly="readonly" name="CustomerClientid">    
  </span>
  <span>
    <label for="CustomerContractId07">Vertrags Nr.</label><br />
    <input id="CustomerContractId07" type="text" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'contract_id'); ?>" maxlength="64" name="CustomerContractId">    
  </span>
  <span>
    <label for="CustomerCreditRatingDate07">Boni-prüfung am</label><br />
    <input id="CustomerCreditRatingDate07" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'credit_rating_date'); ?>" name="CustomerCreditRatingDate">
  </span>
  <span>
    <label for="CustomerCreditRatingOk07">Boni-prüfung Status</label><br />
    <select id="CustomerCreditRatingOk07" name="CustomerCreditRatingOk" <?php echo $disable; ?> >
      <?php setOption ($option_credit_rating_ok, $customer, 'credit_rating_ok'); ?>
    </select>
  </span>
  <?php
  $externalCustomerIdFieldName = 'Externe Kundenkennung';
  
  if (null !== $exCustField = \Wisotel\Configuration\Configuration::get('externalCustomerIdFieldName')) {
    $externalCustomerIdFieldName = $exCustField;
  }
  ?>
  <span class="double">
    <label for="CustomerCustomerId07"><?php echo $externalCustomerIdFieldName; ?></label><br />
    <input id="CustomerCustomerId07" type="text" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'customer_id'); ?>" maxlength="64" name="CustomerCustomerId" />    
  </span>
  <span>
    <label for="CustomerCustomerElectrisity07">Stromkunde</label><br />
    <select id="CustomerCustomerElectrisity07" name="CustomerCustomerElectrisity" <?php echo $disable; ?>>
      <?php setOption ($option_electrisity, $customer, 'customer_electrisity'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerCustomerElectrisityEnddate07">Stromvertrag endet am</label><br />
    <input id="CustomerCustomerElectrisityEnddate07" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'customer_electrisity_enddate'); ?>"  maxlength="11"  name="CustomerCustomerElectrisityEnddate" />
  </span>
  
  <span class="double">
    <label for="CustomerNetworkId07">Netz</label><br />
    <select id="CustomerNetworkId07" name="CustomerNetworkId" <?php echo $disable; ?>>
      <?php setOption ($networks, $customer, 'network_id'); ?>
    </select>
  </span>
  <span class="double"><br /><h4>Produktdetails</h4></span>
  
  <span class="double">
    <label for="CustomerClienttype07">Kundentyp *</label><br />
    <select id="CustomerClienttype07" name="CustomerClienttype" disabled="disabled">
      <?php setOption ($option_clienttype, $customer, 'clienttype'); ?>
    </select>
  </span>
  <span class="double">
    <label for="CustomerProductname07">Produktname *</label><br />
    <select id="CustomerProductname07" name="CustomerProductname" <?php echo $disable; ?>>
        <option value=""></option>

        <?php
        $groupedMainProducts = [];

        foreach ($mainProducts as $mainProduct) {
            $category = null !== $mainProduct->getCategory() ? $mainProduct->getCategory()->getId() : 'uncategorized';
            $groupedMainProducts[$category][] = $mainProduct;
        }

        foreach ($groupedMainProducts as $categoryId => $mainProducts_) {
            $categoryHasActiveProducts = false;

            foreach ($mainProducts_ as $mainProduct) {
                if ($mainProduct->getActive()) {
                    $categoryHasActiveProducts = true;

                    break;
                }
            }

            if (!$categoryHasActiveProducts) {
                $customerProductNameCategory = null !== $customer['productname']->getCategory() ? $customer['productname']->getCategory()->getId() : 'uncategorized';

                if ($customerProductNameCategory !== $categoryId) {
                    continue;
                }
            }

            $categoryName = null !== $mainProducts_[0]->getCategory() ? $mainProducts_[0]->getCategory()->getName() : 'unkategorisiert';
            
            echo sprintf('<optgroup label="%s">', $categoryName);

            foreach ($mainProducts_ as $mainProduct) {
                $isSelected = $mainProduct->getIdentifier() == $customer['productname']->getIdentifier();
                $isActive = $mainProduct->getActive();

                if (!$isSelected && !$isActive) {
                    continue;
                }

                $priceGross = $mainProduct->getPriceGross();

                echo sprintf('<option value="%s"%s%s>[%s] %s%s</option>',
                    $mainProduct->getIdentifier(),
                    $isActive ? '' : ' disabled="disabled"',
                    $isSelected ? ' selected="selected"' : '',
                    $mainProduct->getIdentifier(),
                    $mainProduct->getName(),
                    empty($priceGross) ? '' : ' (Preis: '.$priceGross.'€)'
                );
            }

            echo '</optgroup>';
        }
        ?>
    </select>
  </span>
  
  <span>
    <label for="CustomerRegistrationDate07">Interessent seit</label><br />
    <input id="CustomerRegistrationDate07" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'registration_date'); ?>" maxlength="11" name="CustomerRegistrationDate">
  </span>
  <span>
    <label for="CustomerConnectionWishDate07"><b>Anschalt-Wunschtermin</b></label><br />
    <input id="CustomerConnectionWishDate07" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'connection_wish_date'); ?>" maxlength="11" name="CustomerConnectionWishDate">
  </span>
  <span>
    <label for="CustomerServiceLevel07">Service Level *</label><br />
    <select id="CustomerServiceLevel07" name="CustomerServiceLevel" <?php echo $disable; ?>>
      <?php setOption ($option_servicelevel, $customer, 'service_level'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerServiceLevelSend07">SLA versendet am</label><br />
    <input id="CustomerServiceLevelSend07" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'service_level_send'); ?>" maxlength="11" name="CustomerServiceLevelSend">
  </span>
  
  <span>
    <label for="CustomerContractSentDate07">Vertrag vers. am</label><br />
    <input id="CustomerContractSentDate07" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'contract_sent_date'); ?>" name="CustomerContractSentDate">
  </span>
  <span>
    <label for="CustomerContractVersion07">Vertrag unterschr. am</label><br />
    <input id="CustomerContractVersion07" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'contract_version'); ?>" maxlength="11" name="CustomerContractVersion">
  </span>
  <span>
    <label for="CustomerAddPhoneLines07">Zusätzlicher Gesprächskanal *</label><br />
    <input id="CustomerAddPhoneLines07" type="text" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'add_phone_lines'); ?>" maxlength="10" name="CustomerAddPhoneLines">
  </span>
  <span>
    <label for="CustomerAddPhoneNos07">Zusätzliche Tel.-Nr.</label><br />
    <input id="CustomerAddPhoneNos07" type="text" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'add_phone_nos'); ?>" maxlength="10" name="CustomerAddPhoneNos">
  </span>
  
  <span>
    <label for="CustomerContractReceivedDate07">Vertrag empf. am</label><br />
    <input id="CustomerContractReceivedDate07" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'contract_received_date'); ?>" name="CustomerContractReceivedDate">
  </span>
  <span>
    <label for="CustomerContractAcknowledge07">Vert.Best. zugesandt am</label><br />
    <input id="CustomerContractAcknowledge07" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'contract_acknowledge'); ?>" maxlength="10" name="CustomerContractAcknowledge">
  </span>
  <span class="double">
    <label for="CustomerPurtelProduct07">Purtel-Produkt</label><br />
    <select id="CustomerPurtelProduct07" name="CustomerPurtelProduct" <?php echo $disable; ?>>
        <option value=""></option>

        <?php
        foreach ($groupedMainProducts as $categoryId => $mainProducts_) {
            $categoryHasActiveProducts = false;

            foreach ($mainProducts_ as $mainProduct) {
                if ($mainProduct->getActive()) {
                    $categoryHasActiveProducts = true;

                    break;
                }
            }

            if (!$categoryHasActiveProducts) {
                $customerProductNameCategory = null !== $customer['purtel_product']->getCategory() ? $customer['purtel_product']->getCategory()->getId() : 'uncategorized';

                if ($customerProductNameCategory !== $categoryId) {
                    continue;
                }
            }

            $categoryName = null !== $mainProducts_[0]->getCategory() ? $mainProducts_[0]->getCategory()->getName() : 'unkategorisiert';
            
            echo sprintf('<optgroup label="%s">', $categoryName);

            foreach ($mainProducts_ as $mainProduct) {
                $isSelected = $mainProduct->getIdentifier() == $customer['purtel_product']->getIdentifier();
                $isActive = $mainProduct->getActive();

                if (!$isSelected && !$isActive) {
                    continue;
                }

                $priceGross = $mainProduct->getPriceGross();

                echo sprintf('<option value="%s"%s%s>[%s] %s%s</option>',
                    $mainProduct->getIdentifier(),
                    $isActive ? '' : ' disabled="disabled"',
                    $isSelected ? ' selected="selected"' : '',
                    $mainProduct->getIdentifier(),
                    $mainProduct->getName(),
                    empty($priceGross) ? '' : ' (Preis: '.$priceGross.'€)'
                );
            }

            echo '</optgroup>';
        }
        ?>
    </select>
  </span>
  
  <span>
    <label for="CustomerHouseConnection07">Kosten Hausanschluss</label><br />
    <input id="CustomerHouseConnection07" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'house_connection'); ?>" type="text" name="CustomerHouseConnection">
  </span>
  <span>
    <label for="CustomerRecruitedBy07">Geworben von</label><br />
    <input id="CustomerRecruitedBy07" type="text" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'recruited_by'); ?>" maxlength="64" name="CustomerRecruitedBy">
  </span>
  <span>
    <label for="CustomerPurtelproductAdvanced07">Purtel-Produkt vorab</label><br />
    <select id="CustomerPurtelproductAdvanced07" name="CustomerPurtelproductAdvanced" <?php echo $disable; ?>>
      <?php setOption ($option_purtelproduct_advanced, $customer, 'purtelproduct_advanced'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerFinalPurtelproductDate07">Wechsel zum Endprodukt</label><br />
    <input id="CustomerFinalPurtelproductDate07" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'final_purtelproduct_date'); ?>" maxlength="11" name="CustomerFinalPurtelproductDate">
  </span>
 
  <span>
    <label for="CustomerGfCabling07">Hausint. GF-Verkabelung in m</label><br />
    <input id="CustomerGfCabling07" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'gf_cabling'); ?>" type="text" name="CustomerGfCabling">
  </span>
  <span>
    <label for="CustomerGfCablingCost07">Kosten für die GF-Verkabelung</label><br />
    <input id="CustomerGfCablingCost07" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'gf_cabling_cost'); ?>" type="text" name="CustomerGfCablingCost">
  </span>
  <span>
    <label for="CustomerSofortdsl07">Highspeed Internet sofort</label><br />
    <select id="CustomerSofortdsl07" name="CustomerSofortdsl" <?php echo $disable; ?>>
      <?php setOption ($option_sofortdsl, $customer, 'sofortdsl'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerSipAccounts07">SIP-Accounts</label><br />
    <select id="CustomerSipAccounts07" name="CustomerSipAccounts" <?php echo $disable; ?>>
      <?php setOption ($option_sip_accounts, $customer, 'sip_accounts'); ?>
    </select>
  </span>

  <span>
    <label for="CustomerFirewallRouter07">Firewall Router</label><br />
    <input id="CustomerFirewallRouter07" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'firewall_router'); ?>" type="text" name="CustomerFirewallRouter">
  </span>
  <span>
    <label for="CustomerStnzFb07">Fritzbox 7390</label><br />
    <input id="CustomerStnzFb07" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'stnz_fb'); ?>" type="text" name="CustomerStnzFb">
  </span>
  <span class="double">
    <label for="CustomerTvService07">TV- Dienst</label><br />
    <?php
    $disableTvDienst = $disable;

    if (!empty($disable) && $authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_TECH_DATA_TECHNICAL')) {
        $disableTvDienst = null;
    }
    ?>
    <select id="CustomerTvService07" name="CustomerTvService" <?php echo $disableTvDienst; ?>>
      <?php setOption ($option_tv_service, $customer, 'tv_service'); ?>
    </select>
  </span>
 
  <span class="double">
    <label for="CustomerMediaConverter07">Medien-Konverter</label><br />
    <select id="CustomerMediaConverter07" name="CustomerMediaConverter" <?php echo $disable; ?>>
      <?php setOption ($option_media_converter, $customer, 'media_converter'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerHdPlusCard07">HD-Plus-Karten</label><br />
    <select id="CustomerHdPlusCard07" name="CustomerHdPlusCard" <?php echo $disable; ?>>
      <?php setOption ($option_hd_plus_card, $customer, 'hd_plus_card'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerAddSatellite07">weitere Satelliten</label><br />
    <select id="CustomerAddSatellite07" name="CustomerAddSatellite" <?php echo $disable; ?>>
      <?php setOption ($option_add_satellite, $customer, 'add_satellite'); ?>
    </select>
  </span>

  <span>
    <label for="CustomerConnectionActivationDate07">Anschluss aktiv seit</label><br />
    <input id="CustomerConnectionActivationDate07" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'connection_activation_date'); ?>" name="CustomerConnectionActivationDate">
  </span>
  <span>
    <label for="CustomerContractChangeTo07">Vertragsänderung zum</label><br />
    <input id="CustomerContractChangeTo07" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'contract_change_to'); ?>" maxlength="11" name="CustomerContractChangeTo">
  </span>
  <span>
    <label for="CustomerEuFlat07">EU-Flat</label><br />
    <select id="CustomerEuFlat07" name="CustomerEuFlat" <?php echo $disable; ?>>
      <?php setOption ($option_eu_flat, $customer, 'eu_flat'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerMobileFlat07">Mobilfunk-Flat</label><br />
    <select id="CustomerMobileFlat07" name="CustomerMobileFlat" <?php echo $disable; ?>>
      <?php setOption ($option_mobile_flat, $customer, 'mobile_flat'); ?>
    </select>
  </span>

  <span class="doublebox">
  
    <span>
      <label for="CustomerPurtelEditDone07">Aktivschaltung erledigt</label><br />
      <select id="CustomerPurtelEditDone07" name="CustomerPurtelEditDone" <?php echo $disable; ?>>
        <?php setOption ($option_purtel_edit_done, $customer, 'purtel_edit_done'); ?>
      </select>
    </span>
    <span>
      <label for="CustomerPurtelEditDoneFrom07">von</label><br />  
      <select id="CustomerPurtelEditDoneFrom07" name="CustomerPurtelEditDoneFrom" <?php echo $disable; ?>>
        <?php setOption ($option_purtel_edit_done_from, $customer, 'purtel_edit_done_from'); ?>
      </select>
    </span>
  
  <span>
    Nächster Kündigungstermin<br />
    <?php echo $contract_next_cancle_date; ?>
  </span>
    <span>
      <label for="CustomerWisocontractCanceledDate07"><?php echo \Wisotel\Configuration\Configuration::get('companyName'); ?> Vertrag gekündigt zum</label><br />
      <input id="CustomerWisocontractCanceledDate07" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wisocontract_canceled_date'); ?>" maxlength="11" name="CustomerWisocontractCanceledDate">
    </span>

    <span>
      <label for="CustomerCancelTal07">Tal kündigen</label><br />
      <select id="CustomerCancelTal07" name="CustomerCancelTal" <?php echo $disable; ?>>
        <?php setOption ($option_cancel_tal, $customer, 'cancel_tal'); ?>
      </select>
    </span>
    <span>
      <label for="CustomerTalCancelAckDate07">Tal-Kdg. bestätigt zum</label><br />
      <input id="CustomerTalCancelAckDate07" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'tal_cancel_ack_date'); ?>" name="CustomerTalCancelAckDate">
    </span>
  </span>
  <span class="double">
    <label for="CustomerContractComment07">Kommentar (Vertragsdetails)</label><br />
    <textarea id="CustomerContractComment07" name="CustomerContractComment" <?php echo $disable; ?>><?php setInputValue ($customer, 'contract_comment'); ?></textarea>
  </span>

  <span class="double"><br /><h4>Sonstiges</h4></span>
  <span class="double"><br /><h4>Telefonbucheintrag</h4></span>

  <span>
    <label for="CustomerPaperBill07">Papierrechnung</label><br />
    <select id="CustomerPaperBill07" name="CustomerPaperBill" <?php echo $disable; ?>>
      <?php setOption ($option_paper_bill, $customer, 'paper_bill'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerForderingCosts07">Versandkostenpauschale</label><br />
    <select id="CustomerForderingCosts07" name="CustomerForderingCosts" <?php echo $disable; ?>>
      <?php setOption ($option_fordering_costs, $customer, 'fordering_costs'); ?>
    </select>
  </span>
  <span class="double">
    <label for="CustomerTelefonbuchEintrag07">Kunde wünscht Telefonbucheintrag</label><br />  
    <select id="CustomerTelefonbuchEintrag07" name="CustomerTelefonbuchEintrag" <?php echo $disable; ?>>
      <?php setOption ($option_telefonbuch_eintrag, $customer, 'telefonbuch_eintrag'); ?>
    </select>
  </span>
  
  <span>
    <label for="CustomerGfBranch07">GF Hausanschluss</label><br />
    <select id="CustomerGfBranch07" name="CustomerGfBranch" <?php echo $disable; ?>>
      <?php setOption ($option_gf_branch, $customer, 'gf_branch'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerConnectionInstallation07">Einrichtung Anschluss</label><br />
    <select id="CustomerConnectionInstallation07" name="CustomerConnectionInstallation" <?php echo $disable; ?>>
      <?php setOption ($option_connection_installation, $customer, 'connection_installation'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerPhoneentryDoneDate07">Formular erstellt am</label><br />
    <input id="CustomerPhoneentryDoneDate07" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'phoneentry_done_date'); ?>" maxlength="11" name="CustomerPhoneentryDoneDate">
  </span>
  <span>
    <label for="CustomerPhoneentryDoneFrom07">von</label><br />
    <select id="CustomerPhoneentryDoneFrom07" name="CustomerPhoneentryDoneFrom" <?php echo $disable; ?>>
      <?php setOption ($option_phoneentry_done_from, $customer, 'phoneentry_done_from'); ?>
    </select>
  </span>
  
  <span>
    <label for="CustomerAddChargeTerminal07">Zuzahlung Endgerät</label><br />
    <select id="CustomerAddChargeTerminal07" name="CustomerAddChargeTerminal" <?php echo $disable; ?>>
      <?php setOption ($option_add_charge_terminal, $customer, 'add_charge_terminal'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerTerminalType07">Endgerät Typ</label><br />
    <select id="CustomerTerminalType07" name="CustomerTerminalType" <?php echo $disable; ?>>
      <?php setOption ($option_terminal_type, $customer, 'terminal_type'); ?>
    </select>
  </span>
  <span class="double"><br /><h4>Zusatzoptionen</h4></span>

  <span>
    <label for="CustomerServiceTechnician07">Servicetechniker</label><br />
    <select id="CustomerServiceTechnician07" name="CustomerServiceTechnician" <?php echo $disable; ?>>
      <?php setOption ($option_service_technician, $customer, 'service_technician'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerAddPort07">Zusätzl. Port.</label><br />
    <select id="CustomerAddPort07" name="CustomerAddPort" <?php echo $disable; ?>>
      <?php setOption ($option_add_port, $customer, 'add_port'); ?>
    </select>
  </span>
  <span class="double">
      <label for="CustomerHigherAvailability07">Höhere Verfügbarkeit/ schnellere Entstörung</label><br />
      <select id="CustomerHigherAvailability07" name="CustomerHigherAvailability" <?php echo $disable; ?>>
        <?php setOption ($option_higher_availability, $customer, 'higher_availability'); ?>
      </select>
  </span>
  
  <span>
    <label for="CustomerAddCustomerOnFb07">Zusätzl. Mandant auf FB</label><br />
    <select id="CustomerAddCustomerOnFb07" name="CustomerAddCustomerOnFb" <?php echo $disable; ?>>
      <?php setOption ($option_add_customer_on_fb, $customer, 'add_customer_on_fb'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerSpecialConditionsFrom07">Sonderkonditionen ab</label><br />
    <input id="CustomerSpecialConditionsFrom07" type="text" class="datepicker" <?php echo $disable; ?> name="CustomerSpecialConditionsFrom" value="<?php setInputValue ($customer, 'special_conditions_from'); ?>" >
  </span>
  <span class="double">
    <label for="CustomerHigherUplinkOne07">Höherer Uplink bis 7,5 Mbit/s</label><br />
    <select id="CustomerHigherUplinkOne07" name="CustomerHigherUplinkOne" <?php echo $disable; ?>>
      <?php setOption ($option_higher_uplink_one, $customer, 'higher_uplink_one'); ?>
    </select>
  </span>
  
  <span>
    <label for="CustomerSpecialConditionsTill07">Sonderkonditionen bis</label><br />
    <input id="CustomerSpecialConditionsTill07" type="text" class="datepicker" <?php echo $disable; ?> name="CustomerSpecialConditionsTill" value="<?php setInputValue ($customer, 'special_conditions_till'); ?>">
  </span>
  <span>
    <label for="CustomerSecondTal07">Bestellung 2. Tal</label><br />
    <select id="CustomerSecondTal07" name="CustomerSecondTal" <?php echo $disable; ?>>
      <?php setOption ($option_second_tal, $customer, 'second_tal'); ?>
    </select>
  </span>
  <span class="double">
    <label for="CustomerHigherUplinkTwo07">Höherer Uplink bis 12 Mbit/s</label><br />
    <select id="CustomerHigherUplinkTwo07" name="CustomerHigherUplinkTwo" <?php echo $disable; ?>>
      <?php setOption ($option_higher_uplink_two, $customer, 'higher_uplink_two'); ?>
    </select>
  </span>

  <span class="double">
    <label for="CustomerSpecialConditionsText07">Sonderkonditionen Text</label><br />
    <textarea id="CustomerSpecialConditionsText07" name="CustomerSpecialConditionsText" <?php echo $disable; ?>><?php setInputValue ($customer, 'special_conditions_text'); ?></textarea>
  </span>

  <span class="doublebox">
    <span class="double"><br /><h4>IP-Adressen</h4></span>
    <span>
      <label for="CustomerIpAddressInet07">Feste IP-Adr. (Internet)</label><br />
      <select id="CustomerIpAddressInet07" name="CustomerIpAddressInet" <?php echo $disable; ?>>
        <?php setOption ($option_ip_address_inet, $customer, 'ip_address_inet'); ?>
      </select>
    </span>
    <span>
      <label for="CustomerIpAddressPhone07">Feste IP-Adr. (Telefon)</label><br />
      <select id="CustomerIpAddressPhone07" name="CustomerIpAddressPhone" <?php echo $disable; ?>>
        <?php setOption ($option_ip_address_phone, $customer, 'ip_address_phone'); ?>
      </select>
    </span>
    <span>
      <label for="CustomerAddIpAddress07">Anzahl zusätzl. IP-Adr.</label><br />
      <input id="CustomerAddIpAddress07" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'add_ip_address'); ?>" name="CustomerAddIpAddress" type="text">
    </span>
    <span>
      <label for="CustomerAddIpAddressCost07">Preis zusätzl. IP-Adr.</label><br />
      <select id="CustomerAddIpAddressCost07" name="CustomerAddIpAddressCost" <?php echo $disable; ?>>
        <?php setOption ($option_add_ip_address_cost, $customer, 'add_ip_address_cost'); ?>
      </select>
    </span>
  </span>

  <span>
    <label for="CustomerGutschrift07">Gutschrift</label><br />
    <select id="CustomerGutschrift07" name="CustomerGutschrift" <?php echo $disable; ?>>
      <?php setOption ($option_gutschrift, $customer, 'gutschrift'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerGutschriftTill07">Gutschrift bis</label><br />
    <input id="CustomerGutschriftTill07" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'gutschrift_till'); ?>" maxlength="11" name="CustomerGutschriftTill">
  </span>
  <span>
    <label for="CustomerRDNSInstallation07">Einr. IP-Adr.bereich Preis</label><br />
    <input id="CustomerRDNSInstallation07" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'rDNS_installation'); ?>" type="text" name="CustomerRDNSInstallation">
  </span>
  <span>
    <label for="CustomerRDNSCount07">Anzahl IP-Adressen</label><br />
    <input id="CustomerRDNSCount07" maxlength="5" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'rDNS_count'); ?>" type="text" name="CustomerRDNSCount">
  </span>

  <span class="four"><br /><h4>Telefonanschluss</h4></span>
 
  <span class="double">
    <label for="CustomerPhoneNumberBlock07">Einrichtung Rufnummernblock/blöcke Preis</label><br />
    <input id="CustomerPhoneNumberBlock07" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'phone_number_block'); ?>" type="text" name="CustomerPhoneNumberBlock">
  </span>
  <span>
    <label for="CustomerAddPhoneLines07">zusätzl. Tel.kanäle</label><br />
    <select id="CustomerAddPhoneLines07" name="CustomerAddPhoneLines" <?php echo $disable; ?>>
      <?php setOption ($option_add_phone_lines_gf, $customer, 'add_phone_lines'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerAddPhoneLinesCost07">Preis je Kanal</label><br />
    <input name="CustomerAddPhoneLinesCost07" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'add_phone_lines_cost'); ?>" id="CustomerAddPhoneLinesCost" type="text">
  </span>
 
  <span class="double">
    <label for="CustomerPhoneComment07">Kommentar Telefonanlagen</label><br />
    <textarea id="CustomerPhoneComment07" name="CustomerPhoneComment" <?php echo $disable; ?>><?php setInputValue ($customer, 'phone_comment'); ?></textarea>
  </span>
  <span class="doublebox">
    <span>
      <label for="CustomerGermanNetwork07">Gespräche dt. Festnetz</label><br />
      <select id="CustomerGermanNetwork07" name="CustomerGermanNetwork" <?php echo $disable; ?>>
        <?php setOption ($option_german_network, $customer, 'german_network'); ?>
      </select>
    </span>
    <span>
      <label for="CustomerGermanNetworkPrice07">Preis pro Minute</label><br />
      <input name="CustomerGermanNetworkPrice07" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'german_network_price'); ?>" id="CustomerGermanNetworkPrice" type="text">
    </span>
    <span>
      <label for="CustomerGermanMobile07">Gespräche dt. Mobilnetz</label><br />
      <select id="CustomerGermanMobile07" name="CustomerGermanMobile" <?php echo $disable; ?>>
        <?php setOption ($option_german_mobile, $customer, 'german_mobile'); ?>
      </select>
    </span>
    <span>
      <label for="CustomerGermanMobilePrice07">Preis pro Minute</label><br />
      <input id="CustomerGermanMobilePrice07" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'german_mobile_price'); ?>" type="text" name="CustomerGermanMobilePrice">
    </span>
    <span>
      <label for="CustomerFlatGermanNetwork07">Business Flat Dt., Kanäle</label><br />
        <select id="CustomerFlatGermanNetwork07" name="CustomerFlatGermanNetwork" <?php echo $disable; ?>>
          <?php setOption ($option_flat_german_network, $customer, 'flat_german_network'); ?>
        </select>
    </span>
    <span>
      <label for="CustomerFlatGermanNetworkCost07">Preis pro Kanal</label><br />
      <input id="CustomerFlatGermanNetworkCost07" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'flat_german_network_cost'); ?>" type="text" name="CustomerFlatGermanNetworkCost">
    </span>
  </span>
 
  <span class="double">
    <label for="CustomerPhoneAdapter07">Telefonadapter</label><br />
    <input id="CustomerPhoneAdapter07" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'phone_adapter'); ?>" type="text" name="CustomerPhoneAdapter">
  </span>
  <span>
    <label for="CustomerFlatEuNetwork07">Business Flat EU, Kanäle</label><br />
    <select id="CustomerFlatEuNetwork07" name="CustomerFlatEuNetwork" <?php echo $disable; ?>>
      <?php setOption ($option_flat_eu_network, $customer, 'flat_eu_network'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerFlatEuNetworkCost07">Preis pro Kanal</label>
    <input  id="CustomerFlatEuNetworkCost" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'flat_eu_network_cost'); ?>" type="text" name="CustomerFlatEuNetworkCost">
  </span>
 
  <span class="double">
  </span>
  <span>
    <label for="CustomerIsdnBackup07">ISDN Backup einmalig</label><br />
    <input id="CustomerIsdnBackup07" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'isdn_backup'); ?>" type="text" name="CustomerIsdnBackup">
  </span>
  <span>
    <label for="CustomerIsdnBackup207">ISDN Backup monatl.</label><br />
    <input id="CustomerIsdnBackup207" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'isdn_backup2'); ?>" type="text" name="CustomerIsdnBackup2">
  </span>
</div>

