<?php
$StartPath = urldecode($_POST['StartPath']);
$SelPath = urldecode($_POST['SelPath']);
$CustomerId = urldecode($_POST['CustomerId']);
$StdPdf = urldecode($_POST['StdPdf']);
$To = urldecode($_POST['To']);
$Subject = urldecode($_POST['Subject']);
$Body = urldecode($_POST['Body']);
$AttachFiles = json_decode($_POST['AttachFiles'], true);

require_once $StartPath.'/_inc/phpmailer/class.phpmailer.php';
require_once $StartPath.'/_inc/phpmailer/class.smtp.php';
require_once $StartPath.'/vendor/wisotel/configuration/Configuration.php';

$dir = $StartPath.'/'.$SelPath.'/_files/_mails/'.$CustomerId;
$mailServer = \Wisotel\Configuration\Configuration::get('mailServer');
$mailHead = [];
$mailLine = [];
$mailList = [];
$mailCsv  = false;

$mailAddresses = explode(';', $To);
$mailBcc = explode(';', $mailServer['bcc']);

$mail = new PHPMailer();
$mail->CharSet = $mailServer['CharSet'];
$mail->Host = $mailServer['smtpHost'];
$mail->Port = $mailServer['smtpPort'];
$mail->SMTPAuth = $mailServer['smtpAuth'];
$mail->Username = $mailServer['smtpUser'];
$mail->Password = $mailServer['smtpPasswd'];
$mail->From = $mailServer['from'];
$mail->FromName = $mailServer['fromName'];
$mail->Subject = $Subject;
$mail->Body = $Body;

if (isset($mailServer['smtpSecure'])) {
    $mail->SMTPSecure = $mailServer['smtpSecure'];
}

ini_set('auto_detect_line_endings', true);
foreach ($AttachFiles as $AttachFile) {
    $fext = pathinfo ($AttachFile, PATHINFO_EXTENSION);
    $att = $dir.'/'.$AttachFile;
    if ($fext == 'csv') {
        $i = 0;
        $hcsv = fopen ($att, 'r');
        $mailHead = fgetcsv($hcsv, 0, ';');
        $mailLine = fgetcsv($hcsv, 0, ';');
        while ($mailLine = fgetcsv($hcsv, 0, ';') ) {
            foreach ($mailHead as $key => $value) {
                $mailList[$i][trim($value)] = $mailLine[$key];
            }
            $i++;
        }
        fclose($hcsv);
        $mailCsv = true;
        
    }  else {
        $mail->AddAttachment($att);
    }
}

$mail->SetLanguage("de", $StartPath.'/_inc/phpmailer');
$mail->IsSMTP();
$mail->IsHTML($isHTML);
// $mail->AddAddress("juergen.lehmann@wisotel.com");
$companyName = \Wisotel\Configuration\Configuration::get('companyName');

switch ($StdPdf) {
    case "1":
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/Willkommen_SWB.pdf');

        break;

    case "2":
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/Auftrag_Privatkunde_BADEN-NET.pdf');
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/Baden_Net_AGBs.pdf');
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/Besondere_Geschäftsbedingungen_für_Baden.Net-TV.pdf');
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/Ergaenzende-Geschäftsbedingungen-Privatkunden.pdf');
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/Flyer Baden.Net.pdf');

        break;

    case "3":
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/SEPA-Mandat_Baden.net.pdf');

        break;

    case "4":
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/SEPA-Mandat_Baden.net.pdf');

        break;

    case "5":
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/Telefonbucheintrag_Formular_leer_ausfüllbar.pdf');

        break;

    case "6":
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/Baden_Net_AGBs.pdf');
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/Besondere_Geschäftsbedingungen_für_Baden.Net-TV.pdf');
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/Ergaenzende-Geschäftsbedingungen-Privatkunden.pdf');

        break;

    case "7":
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/Besondere_Geschäftsbedingungen_für_Baden.Net-TV.pdf');

        break;

    case "8":
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/'.$companyName.'_DaheimInternet_Produktflyer_DSL.pdf');
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/'.$companyName.'_Formulare_AGBs_mit_Widerruf.pdf');
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/'.$companyName.'_Formulare_DaheimInternet_DSL_Online.pdf');

        break;

    case "9":
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/'.$companyName.'_DaheimInternet_Produktflyer_Glasfaser.pdf');
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/'.$companyName.'_Formulare_AGBs_mit_Widerruf.pdf');
        $mail->AddAttachment($StartPath.'/'.$SelPath.'/_files/'.$companyName.'_Formulare_DaheimInternet_Glasfaser_Online.pdf');

        break;
}

$status = 0;

if ($mailCsv) {
    foreach ($mailList as $mailCust) {
        $mail->clearAddresses();
        if ($mailCust['Kunden.E-Mail Adresse'] != '') {
            $mail->AddAddress($mailCust['Kunden.E-Mail Adresse']);    
            if (!$mail->Send()) {
                $status = 1;
            }
        }
    }
} else {
    foreach ($mailAddresses as $mailAddress) {
        $mail->AddAddress(trim($mailAddress));
    }
    foreach ($mailBcc as $bcc) {
        $mail->AddBCC(trim($bcc));
    }
    if (!$mail->Send()) {
        $status = 1;
    }
}

$response = array (
    'status' => $status,
    'error' => $mail->ErrorInfo,
);

print_r(json_encode($response));
