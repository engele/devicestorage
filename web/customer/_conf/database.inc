<?php
$sql = array (
    // Kundenliste
    'customers' => "
        SELECT * FROM customers ORDER BY clientid
    ",
    // Kundensuche
    'customer' => "
          SELECT * FROM customers
          WHERE id = ?
    ",
    // ClientId
    'max_clientid' => "
          SELECT MAX(clientid) FROM customers
          WHERE clientid LIKE ?
          ORDER BY clientid
    ",
    // Version
    'max_version' => "
          SELECT MAX(version) FROM customers
          WHERE clientid = ?
    ",
    // E-Mail Vorlagen
    'email_temp' => "
          SELECT * FROM mail_templates
          ORDER BY group_id, id
    ",
    // E-Mail Gruppem
    'email_group' => "
        SELECT * FROM mail_group
    ",
    // Anschluss Stadt
    'connection_city' => "
        SELECT * FROM connection_city
        ORDER BY `order`
    ",
     // Anschluss Teilort
    'connection_district' => "
        SELECT * FROM connection_district
        ORDER BY `order`
    ",

    // Netzwerk id_string
    /*'networks_id' => "
        SELECT * FROM networks
        WHERE id = ?
    ",*/
    'networks_id' => "
        SELECT n.* FROM networks n 
        INNER JOIN customers c ON c.network_id = n.id 
        WHERE c.id = ?
    ",
    // Netze
    'networks' => "
        SELECT * FROM networks
        ORDER BY id_string
    ",
    // IP-Block
    'ip_ranges' => "
        SELECT ipr.id, ipr.start_address, ipr.end_address, ipr.name, ipr.subnetmask, ipr.gateway, ipr.vlan_ID
        FROM networks AS net, ip_ranges_networks AS iprn, ip_ranges AS ipr
        WHERE net.id = iprn.network_id 
        AND iprn.ip_range_id = ipr.id
        AND net.id = ?
    ",
    // IP-Addresses
    'ip_addresses' => "
        SELECT ip_address FROM customers 
        WHERE INET_ATON(ip_address) BETWEEN ? AND ?
    ",
    // locations
    'locations' => "
        SELECT id, name, kvz_prefix, cid_suffix_start, cid_suffix_end, dslam_ip_adress, logon_port, vectoring, vlan_pppoe, vlan_acs, vlan_iptv, logon_id, logon_pw, dslam_conf, dslam_type, acs_type FROM locations
        WHERE network_id = ?
    ",
    // lsa_kvz_partitions
    'lsa_kvz_partitions' => "
        SELECT * FROM lsa_kvz_partitions
        WHERE location_id = ?
    ",
    // lsa_pins
    'lsa_pins' => "
        SELECT lsa_pin FROM customers
        WHERE lsa_kvz_partition_id = ?
    ",
    // defective_lsa_pins
    'defective_lsa_pins' => "
        SELECT value FROM defective_lsa_pins
        WHERE lsa_kvz_partition_id = ?
    ",
    // cards
    'cards' => "
        SELECT c.id, c.port_amount, c.line as line, c.name AS cardname, c.card_type_id, t.name AS cardtype FROM cards AS c, card_types AS t
        WHERE c.card_type_id = t.id
        AND c.location_id = ?
    ",
    /*// dslam_ports
    'dslam_ports' => "
        SELECT dslam_port FROM customers
        WHERE card_id = ?
    ",*/
    // defective_lsa_pins
    'defective_card_ports' => "
        SELECT value FROM defective_card_ports
        WHERE card_id = ?
    ",
    // product
    'produkte' => "
        SELECT * FROM produkte
        WHERE produkt = ?
    ",
    // product select
    'produkte_sel' => "
        SELECT * FROM produkte ORDER BY group_id
    ",

    'prod_group' => "
        SELECT * FROM prod_group
    ",
    
    // Produktoptionen
    'options' => "
        SELECT * FROM optionen 
        WHERE active != 0 
        AND active IS NOT NULL
    ",
    
    'cust_options' => "
        SELECT * FROM customer_options AS cust LEFT JOIN optionen AS opt ON cust.opt_id = opt.option
        WHERE cust_id = ?
    ",

    // Purtel account
    /*'purtel_account' => "
        SELECT * FROM purtel_account
        WHERE cust_id = ?
        ORDER BY master DESC, purtel_login
    ",*/

    // WBCI Endkundenvertragspartner
    'wbci_ekp' => "
        SELECT * FROM wbci_ekp
    ",

    // WBCI Geschäftsfall
    'wbci_gf' => "
        SELECT * FROM wbci_gf
    ",
    // multible IP's
    'delete_customers_ip_addresses' => "
        DELETE FROM `ip_addresses` 
        WHERE `customer_id` = ?
    ",

    'insert_customers_ip_addresses' => "
        INSERT INTO `ip_addresses` (`ip_address`, `subnetmask`, `gateway`, `vlan_id`, `rdns`, `ip_range_id`, `customer_id`)
        VALUES %s
    ",

    'select_customers_ip_addresses' => "
        SELECT * FROM `ip_addresses` WHERE `customer_id` = ?
    ",

    'select_taken_ip_addresses_by_range' => "
        SELECT `ip_address` FROM `ip_addresses` WHERE `ip_range_id` = ?
    ",
    
    'select_defective_ip_addresses' => "
        SELECT `value` FROM `defective_ip_addresses` 
        WHERE `ip_range_id` = ?
    ",

    // Multible Terminals
    'select_customers_terminals' => "
        SELECT * FROM `customer_terminals` 
        WHERE `customer_id` = ?
    ",

    'delete_customers_terminals' => "
        DELETE FROM `customer_terminals` WHERE `customer_id` = ?
    ",

    'insert_customers_terminals' => "
        INSERT INTO `customer_terminals` 
        (`ip_address`, `type`, `mac_address`, `serialnumber`, `remote_user`, 
        `remote_password`, `remote_port`, `tv_id`, `check_id`, `activation_date`, `customer_id`)
        VALUES %s
    ",

    // Strukturierter Verlauf
    'select_history_events' => "
        SELECT * FROM `customer_history_events` ORDER BY `order_number`
    ",

    /*'select_history_by_customer' => "
        SELECT ch.*, kvu.`fullname` FROM `customer_history` ch 
        LEFT JOIN `kv_user` kvu ON ch.`created_by` = kvu.`account`
        WHERE `customer_id` = ?
        ORDER BY created_at  DESC
    ",*/
    'select_history_by_customer' => "
        SELECT ch.*, u.`full_name` as fullname FROM `customer_history` ch 
        LEFT JOIN `app_users` u ON ch.`created_by` = u.`username`
        WHERE ch.`customer_id` = ?
        ORDER BY ch.created_at  DESC
    ",

    'insert_history' => "
        INSERT INTO `customer_history`
        (`customer_id`, `event_id`, `created_by`, `created_at`, `note`)
        VALUES (?, ?, ?, ?, ?)
    ",

    'update_history' => "
        UPDATE `customer_history` SET `event_id` = ?, `note` = ? 
        WHERE `id` = ?
    ",
  );

$GLOBALS['sql'] = & $sql;
?>
