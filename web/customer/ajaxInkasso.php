<?php
$StartPath = urldecode($_POST['StartPath']);
$SelPath   = urldecode($_POST['SelPath']);
$StartURL  = urldecode($_POST['StartURL']);
$custid    = urldecode($_POST['CustID']);
$Inkasso   = json_decode ($_POST['Inkasso'], true);
  
session_start();
require_once ($StartPath.'/_conf/database.inc');
require_once $StartPath.'/vendor/wisotel/configuration/Configuration.php';

$username = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$password = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
$action    = 'ksp_set_auftrag';
$status    = 0;
$PurtelUrl = '';

$PurtelUrl  = 'https://ipcom.purtel.com/index.php?super_username=' . urlencode($username) . '&super_passwort=' . urlencode($password) . '&action=' . urlencode($action);
// $PurtelUrl .= "&test_email=juergen.lehmann@wisotel.com";
foreach ($Inkasso as $key => $value) {
    $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
}

/*
ob_start();
var_dump($PurtelUrl, $Inkasso);
file_put_contents(__DIR__."/inkasso.txt", ob_get_clean());
ob_end_clean();
die("error");
*/

$curl = curl_init ($PurtelUrl);
$curl_options = array (
    CURLOPT_URL             => $PurtelUrl,
    CURLOPT_HEADER          => false,
    CURLOPT_RETURNTRANSFER  => true,
    CURLOPT_SSL_VERIFYPEER  => false,
);
curl_setopt_array($curl, $curl_options);
$result = curl_exec($curl);
if (preg_match ('/^\+OK/', $result)) {
    $status = 1;
    $hist_id = 43;
    $note = $db->real_escape_string("Inkasso eingestellt am: ".date('d.m.Y')."<br>Rechnungsnr: ".$Inkasso['rechnung']);
    $sqlh = "INSERT INTO customer_history SET customer_id = ".$custid.", event_id = $hist_id, created_by = '".$_SESSION['login']['user']."', created_at = NOW(), note = '$note';\n";
    $db_save_result = $db->query($sqlh);
}
$response = array (
    'status'    => $status,
    'error_mes' => $result
);
print_r (json_encode ($response));
?>
