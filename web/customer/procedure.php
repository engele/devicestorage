<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_PROCEDURE');

?>

<h3 class="CustProcedure">Ablauf</h3>
<div class="CustProcedure form">
  <span class="double"><h4>Vertrag</h4></span>
  <span class="double"><h4>TAL</h4></span>
  <span class="double">
    <label for="CustomerContractSentDate05">Vertrag vers. am</label><br>
    <input id="CustomerContractSentDate05" type="text" maxlength="11" readonly="readonly" name="CustomerContractSentDate" value="<?php setInputValue ($customer, 'contract_sent_date'); ?>">
  </span>
  <span>
    <label for="CustomerTalOrderDate05">TAL-Bestelldatum</label><br>
    <input id="CustomerTalOrderDate05" type="text" maxlength="11" readonly="readonly" name="CustomerTalOrderDate" value="<?php setInputValue ($customer, 'tal_order_date'); ?>">    
  </span>
  <span>
    <label for="CustomerTalOrderedForDate05">TAL bestellt zum</label><br>
    <input id="CustomerTalOrderedForDate05" type="text" maxlength="11" readonly="readonly" name="CustomerTalOrderedForDate" value="<?php setInputValue ($customer, 'tal_ordered_for_date'); ?>">    
  </span>
  <span class="double">
    <label for="CustomerContractVersion05">Vertrag unterschr. am</label><br>
    <input id="CustomerContractVersion05" type="text" maxlength="11" readonly="readonly" name="CustomerContractVersion" value="<?php setInputValue ($customer, 'contract_version'); ?>">
  </span>
  <span>
    <label for="CustomerTalOrderAckDate05">TAL bestätigt zum</label><br>
    <input id="CustomerTalOrderAckDate05" type="text" maxlength="11" readonly="readonly" name="CustomerTalOrderAckDate" value="<?php setInputValue ($customer, 'tal_order_ack_date'); ?>">    
  </span>
  <span>
    <label for="CustomerTalOrderWork05">Arbeiten beim Kunden</label><br>
    <input id="CustomerTalOrderWork05" type="text" maxlength="5" readonly="readonly" name="CustomerTalOrderWork" value="<?php setInputValue ($customer, 'tal_order_work'); ?>">
  </span>
  <span class="double">
    <label for="CustomerContractReceivedDate05">Vertrag empf. am</label><br>
    <input id="CustomerContractReceivedDate05" type="text" maxlength="11" readonly="readonly" name="CustomerContractReceivedDate" value="<?php setInputValue ($customer, 'contract_received_date'); ?>">
  </span>
  <span class="double">
    <label for="CustomerCustomerConnectInfoDate05">Kunde über Anschlussdatum informiert am</label><br>
    <input id="CustomerCustomerConnectInfoDate05" type="text" maxlength="11" readonly="readonly" name="CustomerCustomerConnectInfoDate" value="<?php setInputValue ($customer, 'customer_connect_info_date'); ?>">    
  </span>
  <span class="double">
    <label for="CustomerProductname05">Produktname</label><br>
    <input id="CustomerProductname05" type="text" maxlength="64" readonly="readonly" name="CustomerProductname" value="<?php echo $customer['productname']->getIdentifier(); ?>">
  </span>
  <span>
    <label for="CustomerTalLetterToOperator05">Kündigung an NB am</label><br>
    <input id="CustomerTalLetterToOperator05" type="text" readonly="readonly" name="CustomerTalLetterToOperator" value="<?php setInputValue ($customer, 'tal_letter_to_operator'); ?>">  
  </span>
  <span>
    <label for="CustomerTalLetterToOperatorFrom05">gekündigt von</label><br>
    <input id="CustomerTalLetterToOperatorFrom05" type="text" readonly="readonly" name="CustomerTalLetterToOperatorFrom" value="<?php setInputValue ($customer, 'tal_letter_to_operator_from'); ?>">
  </span>
  <span class="double">
    <label for="CustomerPurtelProduct05">Purtel-Produkt</label><br>
    <input id="CustomerPurtelProduct05" type="text" maxlength="64" readonly="readonly" name="CustomerPurtelProduct" value="<?php echo $customer['purtel_product']->getIdentifier(); ?>">
  </span>
  <span class="double"><br><h4>Logistik</h4></span>
  <span class="double">
    <label for="CustomerPurtelproductAdvanced05">Purtel-Produkt vorab</label><br>
    <input id="CustomerPurtelproductAdvanced05" type="text" readonly="readonly" name="CustomerPurtelproductAdvanced" value="<?php setInputValue ($customer, 'purtelproduct_advanced'); ?>">
  </span>
  <span>
    <label for="CustomerFbVorab05">Fritzbox vorab</label><br>
    <input id="CustomerFbVorab05" type="text" maxlength="5" readonly="readonly" name="CustomerFbVorab" value="<?php setInputValue ($customer, 'fb_vorab'); ?>">    
  </span>
  <span>
    <label for="CustomerTerminalType05">Endgerät Typ</label><br>
    <input id="CustomerTerminalType05" type="text" maxlength="8" readonly="readonly" name="CustomerTerminalType"  value="<?php setInputValue ($customer, 'terminal_type'); ?>">    
  </span>
  <span class="double">
    <label for="CustomerFinalPurtelproductDate05">Wechsel zum Endprodukt</label><br>
    <input id="CustomerFinalPurtelproductDate05" type="text" maxlength="11" readonly="readonly" name="CustomerFinalPurtelproductDate" value="<?php setInputValue ($customer, 'final_purtelproduct_date'); ?>">
  </span>
  <span>
    <label for="CustomerTerminalReady05">FB konfiguriert am</label><br>
    <input id="CustomerTerminalReady05" type="text" maxlength="11" readonly="readonly" name="CustomerTerminalReady" name="CustomerTerminalReady" value="<?php setInputValue ($customer, 'terminal_ready'); ?>">    
  </span>
  <span>
    <label for="CustomerTerminalSendedDate05">Endgerät versendet am</label><br>
    <input id="CustomerTerminalSendedDate05" type="text" maxlength="11" readonly="readonly" name="CustomerTerminalSendedDate" value="<?php setInputValue ($customer, 'terminal_sended_date'); ?>">    
  </span>
  <span class="double"><br><h4>Portierung</h4></span>
  <span class="double"><br><h4>Netztechnik</h4></span>
  <span class="double">
    <label for="CustomerOldcontractExists05">Altvertrag vorhanden</label><br>
    <input id="CustomerOldcontractExists05" type="text" maxlength="25" readonly="readonly" name="CustomerOldcontractExists" value="<?php setInputValue ($customer, 'oldcontract_exists'); ?>"> 
  </span>
  <span>
    <label for="CustomerPatchDate05">Gepatched am</label><br>
    <input id="CustomerPatchDate05" type="text" maxlength="11" readonly="readonly" name="CustomerPatchDate" value="<?php setInputValue ($customer, 'patch_date'); ?>">    
  </span>
  <span>
    <label for="CustomerPppoeConfigDate05">PPPoE Konfig am</label><br>
    <input id="CustomerPppoeConfigDate05" type="text" maxlength="11" readonly="readonly" name="CustomerPppoeConfigDate" value="<?php setInputValue ($customer, 'pppoe_config_date'); ?>">    
  </span>
  <span class="double">
    <label for="CustomerAddPhoneNos05">Zusätzliche Tel.-Nr.</label><br>
    <input id="CustomerAddPhoneNos05" type="text" maxlength="5" readonly="readonly" name="CustomerAddPhoneNos" value="<?php setInputValue ($customer, 'add_phone_nos'); ?>">  
  </span>
  <span class="double">
    <label for="CustomerDslamArrangedDate05">Am DSLAM/ CMTS eingerichtet am</label><br>
    <input id="CustomerDslamArrangedDate05" type="text" maxlength="11" readonly="readonly" name="CustomerDslamArrangedDate" value="<?php setInputValue ($customer, 'dslam_arranged_date'); ?>">    
  </span>
  <span class="double">
    <label for="CustomerPurtelCustomerRegDate05">Kunde bei Purtel angelegt am</label><br>
    <input id="CustomerPurtelCustomerRegDate05" type="text" maxlength="64" readonly="readonly" name="CustomerPurtelCustomerRegDate" value="<?php setInputValue ($customer, 'purtel_customer_reg_date'); ?>">
  </span>
  <span>
    <label for="CustomerSofortdsl05">DSL sofort</label><br>
    <input id="CustomerSofortdsl05" type="text" readonly="readonly" name="CustomerSofortdsl" value="<?php setInputValue ($customer, 'sofortdsl'); ?>">    
  </span>
  <span>
    <label for="CustomerSoforttel05">Telefon sofort</label><br>
    <input id="CustomerSoforttel05" type="text" readonly="readonly" name="CustomerSoforttel" value="<?php setInputValue ($customer, 'soforttel'); ?>">    
  </span>
  <span class="double">
    <label for="CustomerVenteloPortLetterDate05">Portierung versendet am</label><br>
    <input id="CustomerVenteloPortLetterDate05" type="text" maxlength="11" readonly="readonly" name="CustomerVenteloPortLetterDate" value="<?php setInputValue ($customer, 'ventelo_port_letter_date'); ?>">
  </span>
  <span class="double"><br><h4>Aktivierung</h4></span>
  <span class="double">
    <label for="CustomerVenteloConfirmationDate05">Portierung erhalten am</label><br>
    <input id="CustomerVenteloConfirmationDate05" type="text" maxlength="11" readonly="readonly" name="CustomerVenteloConfirmationDate" value="<?php setInputValue ($customer, 'ventelo_confirmation_date'); ?>">
  </span>
  <span class="double">
    <label for="CustomerConnectionActivationDate05">Anschluss aktiv seit</label><br>
    <input id="CustomerConnectionActivationDate05" type="text" maxlength="11" readonly="readonly" name="CustomerConnectionActivationDate" value="<?php setInputValue ($customer, 'connection_activation_date'); ?>">    
  </span>
  <span class="double">
    <label for="CustomerVenteloPurtelEnterDate05">Port. eingestellt am</label><br>
    <input id="CustomerVenteloPurtelEnterDate05" type="text" maxlength="11" readonly="readonly" name="CustomerVenteloPurtelEnterDate" value="<?php setInputValue ($customer, 'ventelo_purtel_enter_date'); ?>">  
  </span>
  <span>
    <label for="CustomerReachedDownstream05">VLAN_ID</label><br>
    <input id="CustomerReachedDownstream05" type="text" value="" maxlength="32" readonly="readonly" name="CustomerReachedDownstream" value="<?php setInputValue ($customer, 'reached_downstream'); ?>">    
  </span>
  <span>
    <label for="CustomerReachedUpstream05">Messung Upstream</label><br>
    <input id="CustomerReachedUpstream05" type="text" value="" maxlength="32" readonly="readonly" name="CustomerReachedUpstream" value="<?php setInputValue ($customer, 'reached_upstream'); ?>">
  </span>
  <span class="double">
    <label for="CustomerStatiPortConfirmDate05">Bestätigtes Portdat.</label><br>
    <input id="CustomerStatiPortConfirmDate05" type="text" maxlength="11" readonly="readonly" name="CustomerStatiPortConfirmDate" value="<?php setInputValue ($customer, 'stati_port_confirm_date'); ?>">
  </span>
  <span>
    <label for="CustomerPurtelEditDone05">Aktivschaltung erledigt</label><br>
    <input id="CustomerPurtelEditDone05" type="text" value="" maxlength="5" readonly="readonly" name="CustomerPurtelEditDone" value="<?php setInputValue ($customer, 'purtel_edit_done'); ?>">
  </span>
  <span>
    <label for="CustomerPurtelEditDoneFrom05">von</label><br>
    <input id="CustomerPurtelEditDoneFrom05" type="text" value="" maxlength="10" readonly="readonly" name="CustomerPurtelEditDoneFrom" value="<?php setInputValue ($customer, 'purtel_edit_done_from'); ?>">    
  </span>
  <span class="double">
    <label for="CustomerPortdefaultComment05">Rückmeldung Purtel</label><br>
    <textarea id="CustomerPortdefaultComment05" readonly="readonly" name="CustomerPortdefaultComment"><?php setInputValue ($customer, 'portdefault_comment'); ?></textarea>  
  </span>
  <span class="doublebox">
  <span>
    <label for="CustomerDtagbluckageSendedDate05">DTAG StöMe gesend. am</label><br>
    <input id="CustomerDtagbluckageSendedDate05" type="text" value="" maxlength="11" readonly="readonly" name="CustomerDtagbluckageSendedDate" value="<?php setInputValue ($customer, 'dtagbluckage_sended_date'); ?>">    
  </span>
  <span>
    <label for="CustomerDtagbluckageFullfiledDate05">DTAG StöMe behoben am</label><br>
    <input id="CustomerDtagbluckageFullfiledDate05" type="text" value="" maxlength="11" readonly="readonly" name="CustomerDtagbluckageFullfiledDate" value="<?php setInputValue ($customer, 'dtagbluckage_fullfiled_date'); ?>">    
  </span>
    <label for="CustomerTelefonbuchEintrag05">Kunde wünscht Telefonbucheintrag</label><br>
    <input id="CustomerTelefonbuchEintrag05" type="text" value="" maxlength="5" readonly="readonly" name="CustomerTelefonbuchEintrag" value="<?php setInputValue ($customer, 'telefonbuch_eintrag'); ?>">  
  </span>
</div>
