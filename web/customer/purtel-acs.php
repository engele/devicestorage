<?php

/**
 * Automaticly add or remove acs_id from/to customer after adding or deleting cpe from acs
 */

require_once __DIR__.'/../_conf/database.inc';

$purtelUrl = urldecode($_GET['purtel-url']);
$response = file_get_contents($purtelUrl);

switch ($_GET['action']) {
    /**
     * ACS CPE löschen
     */
    case 'acs_cpe_loeschen':
        $matches = [];

        if (1 === preg_match('/^\+OK Die Hardware mit der Seriennummer: ([a-zA-Z0-9]+) von Mandant: (\d+) wurde gel.scht!$/', $response, $matches)) {
            echo sprintf('<p>%s</p><br />', $response);

            $parsedGet = [];

            parse_str(parse_url($purtelUrl, PHP_URL_QUERY), $parsedGet);

            $statement = $db->prepare("SELECT `id` FROM `customers` WHERE `acs_id` = ?");
            $statement->bind_param('i', $parsedGet['acs_id']);
            $statement->execute();

            $response = $statement->get_result();

            if ($response->num_rows === 1) {
                $customer = $response->fetch_assoc();
                $response->free();
                $statement->close();

                $statement = $db->prepare("UPDATE `customers` SET `acs_id` = NULL WHERE `id` = ?");
                $statement->bind_param('i', $customer['id']);

                if ($statement->execute()) {
                    echo sprintf('<span style="color: green;">ACS Hardware-Id (%d) wurde automatisch beim Kunden (%s) gelöscht.<br />'
                        .'(Ps: Kundendatenseite muss neu laden werden - F5)</span>',
                        $parsedGet['acs_id'],
                        $customer['id']
                    );
                } else {
                    echo '<span style="color: red;">ACS Hardware-Id konnte nicht automatisch vom Kunden gelöscht werden!!!<br />'
                        .'Bitte machen Sie dies manuell!</span>';
                }

                $statement->close();
            } else {
                exit(sprintf('Failure! %d Kunden gefunden, deren ACS-Id "%s" lautet.',
                    $response->num_rows,
                    $parsedGet['acs_id']
                ));
            }
        } else {
            exit(sprintf('Unexpected response from purtel. Response: %s', $response));
        }

        break;

    /**
     * ACS CPE anlegen
     */
    case 'acs_cpe_neu':
        $matches = [];

        if (1 === preg_match('/^\+OK;id=(\d+)$/', $response, $matches)) {
            $matches[1] = (int)$matches[1];

            echo sprintf('<p>%s</p><br />', $response);

            $parsedGet = [];

            parse_str(parse_url($purtelUrl, PHP_URL_QUERY), $parsedGet);

            $param = $parsedGet['cwmp'].'%';

            $statement = $db->prepare("SELECT `id` FROM `customers` WHERE `mac_address` LIKE ?");
            $statement->bind_param('s', $param);
            $statement->execute();
            $response = $statement->get_result();

            if ($response->num_rows === 1) {
                $customer = $response->fetch_assoc();
                $response->free();
                $statement->close();

                $statement = $db->prepare("UPDATE `customers` SET `acs_id` = ? WHERE `id` = ?");
                $statement->bind_param('ii', $matches[1], $customer['id']);

                if ($statement->execute()) {
                    echo sprintf('<span style="color: green;">ACS Hardware-Id (%d) wurde automatisch beim Kunden (%s) gespeichert.<br />'
                        .'(Ps: Kundendatenseite muss neu laden werden - F5)</span>',
                        $matches[1],
                        $customer['id']
                    );
                } else {
                    echo '<span style="color: red;">ACS Hardware-Id konnte nicht automatisch beim Kunden gespeichert werden!!!<br />'
                        .'Bitte machen Sie dies manuell!</span>';
                }

                $statement->close();
            } else {
                exit(sprintf('Failure! %d Kunden gefunden, deren Mac-Adresse mit "%s" beginnt.',
                    $response->num_rows,
                    $parsedGet['cwmp']
                ));
            }
        } else {
            exit(sprintf('Unexpected response from purtel. Response: %s', $response));
        }

        break;

    /**
     * Default
     */
    default:
        exit('I\'ve got nothing to do..');
}

?>
