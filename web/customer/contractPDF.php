<?php
  require_once '../config.inc';
  require_once '../_conf/database.inc';
  require_once '_conf/database.inc';
  require_once '../_inc/tcpdf/tcpdf.php';
  require_once '../_inc/fpdi/fpdi.php';

  $option_notice_period_type = array (
    '-n-'                   => '',
    'months'                => 'Monat(e)',
    'weeks'                 => 'Wochen',
    'weeks till months end' => 'Wo. zum Monatsende',
    'days'                  => 'Tage',
  );
  $date = date('Y-m-d');
  // get customers data
  if (true) {
    if (key_exists('id', $_GET)) $cust_id = $_GET['id']; else $cust_id = 0;
    $db_customer = $db->prepare($sql['customer']);
    $db_customer->bind_param('d', $cust_id);
    $db_customer->execute();
    $db_customer_result = $db_customer->get_result();
    $customer = $db_customer_result->fetch_assoc();
    $db_customer_result->close();

    $db_options = $db->prepare($sql['cust_options']);
    $db_options->bind_param('d', $cust_id);
    $db_options->execute();
    $db_options_result = $db_options->get_result();
    $options = array();
    foreach ($db_options_result as $db_option) array_push($options, $db_option['opt_id']);
    $db_options_result->close();

    $ported_phones = explode ("\r\n", $customer['new_or_ported_phonenumbers']);
    // $pdf = new TCPDF("P","mm","A4", true, "UTF-8",false);
    $pdf      = new FPDI();
    $pdfCount = $pdf->setSourceFile('_files/Auftrag.pdf');
    $tempId   = $pdf->importPage($pdfCount);
    $size     = $pdf->getTemplateSize($tempId);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor(\Wisotel\Configuration\Configuration::get('companyName'));

    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false); // kann genutzt werden, um Seitenzahlen etc. einzupflegen
    $pdf->SetFont('helvetica', '', 10); // Schriftart festlegen
    // add a page
    $pdf->AddPage('P', array($size['w'],$size['h']));
    $pdf->useTemplate($tempId);

    /**************************************
    * Meine persönlichen Daten
    */
    // Vorname
    $pdf->SetXY(14, 26.1, 1);
    $pdf->Write(5, $customer['firstname'], '', 0, 'L');
    // Nachname
    $pdf->SetXY(14, 34.3, 1);
    $pdf->Write(5, $customer['lastname'], '', 0, 'L');
    // Straße
    $pdf->SetXY(14, 42.5, 1);
    $pdf->Write(5, $customer['street'], '', 0, 'L');
    // Hausnr
    $pdf->SetXY(82.5, 42.5, 1);
    $pdf->Write(5, $customer['streetno'], '', 0, 'L');
    // PLZ
    $pdf->SetXY(14, 50.8, 1);
    $pdf->Write(5, $customer['zipcode'], '', 0, 'L');
    // Ort
    $pdf->SetXY(38.5, 50.8, 1);
    $pdf->Write(5, $customer['city'], '', 0, 'L');
    /*********************/
    // Geburtsdatum
    $pdf->SetXY(102.5, 26.1, 1);
    $pdf->Write(5, $customer['birthday'], '', 0, 'L');
    // Telefon
    $pdf->SetXY(102.5, 34.3, 1);
    $pdf->Write(5, $customer['phoneareacode'].' '.$customer['phonenumber'], '', 0, 'L');
    // Mobilnummer
    $pdf->SetXY(102.5, 42.5, 1);
    $pdf->Write(5, $customer['mobilephone'], '', 0, 'L');
    // E-Mail
    $pdf->SetXY(102.5, 50.8, 1);
    $pdf->Write(5, $customer['emailaddress'], '', 0, 'L');
    /**************************************
    * Produktauswahl
    */
    // Bemerkungen
    $txt = $customer['contract_comment'];
    $pdf->MultiCell(92.7, 13, $txt, 0, 'L', 0, 0, 102.5, 154.8, 1, 0, 0, 1, 15, 'T', 0);
    /**************************************
    * Mein derzeitiger Anschluss
    */
    // Telefonanbieter
    $pdf->SetXY(14, 177.8, 1);
    $pdf->Write(5, $customer['oldcontract_phone_provider'], '', 0, 'L');
    // Vertragsinhaber
    $pdf->SetXY(56, 177.8, 1);
    $pdf->Write(5, $customer['oldcontract_firstname'].' '.$customer['oldcontract_lastname'], '', 0, 'L');
    // Kunden-Nr.
    $pdf->SetXY(102.5, 177.8, 1);
    $pdf->Write(5, $customer['oldcontract_phone_clientno'], '', 0, 'L');
    // Kündigungstermin
    $pdf->SetXY(132.5, 177.8, 1);
    $pdf->Write(5, $customer['exp_date_phone'], '', 0, 'L');
    // Kündigungsfrist
    $pdf->SetXY(163, 177.8, 1);
    $pdf->Write(5, $customer['notice_period_phone_int'].' '.$option_notice_period_type[$customer['notice_period_phone_type']], '', 0, 'L');
    // Rufnummer 1
    if (key_exists('0', $ported_phones)){
      $pdf->SetXY(38.5, 190.5, 1);
      $pdf->Write(5, $ported_phones[0], '', 0, 'L');
    }
    // Rufnummer 2
    if (key_exists('1', $ported_phones)){
      $pdf->SetXY(70.5, 190.5, 1);
      $pdf->Write(5, $ported_phones[1], '', 0, 'L');
    }
    // Rufnummer 3
    if (key_exists('2', $ported_phones)){
      $pdf->SetXY(102.5, 190.5, 1);
      $pdf->Write(5, $ported_phones[2], '', 0, 'L');
    }
    // Sonstiges
    //$pdf->SetXY(132.5, 190.5, 1);
    //$pdf->Write(5, 'Sonstiges', '', 0, 'L');
    // Mein derzeitiger Internet-Anbieter
    $pdf->SetXY(14, 204.3, 1);
    $pdf->Write(5, $customer['oldcontract_internet_provider'], '', 0, 'L');
    // Vertragsinhaber
    $pdf->SetXY(56, 204.3, 1);
    $pdf->Write(5, $customer['oldcontract_firstname'].' '.$customer['oldcontract_lastname'], '', 0, 'L');
    // Kunden-Nr.
    $pdf->SetXY(102.5, 204.3, 1);
    $pdf->Write(5, $customer['oldcontract_internet_clientno'], '', 0, 'L');
    // Frühester Kündigungstermin
    $pdf->SetXY(132.5, 204.3, 1);
    $pdf->Write(5, $customer['exp_date_int'], '', 0, 'L');
    // Kündigungsfrist
    $pdf->SetXY(163, 204.3, 1);
    $pdf->Write(5, $customer['notice_period_internet_int'].' '.$option_notice_period_type[$customer['notice_period_internet_type']], '', 0, 'L');
    /**************************************
    * Einzugsermächtigung 
    */
    // Kontoinhaber
    $pdf->SetXY(147.5, 241, 1);
    $pdf->Write(5, $customer['bank_account_holder_lastname'], '', 0, 'L');
    // Kreditinstitut
    $pdf->SetXY(14, 249, 1);
    $pdf->Write(5, $customer['bank_account_bankname'], '', 0, 'L');
    // IBAN
    $pdf->SetFont('courierb', '', 10); // Schriftart festlegen
    $pdf->SetXY(14, 241.3, 1);
    $pdf->Cell(80, 5, $customer['bank_account_iban'], 0, 0, 'L', 0, '', 4);
    // BIC
    $pdf->SetXY(102, 241.3, 1);
    $pdf->Cell(34, 5, $customer['bank_account_bic_new'], 0, 0, 'L', 0, '', 4);
    /**************************************
    * Anschlußtyp 
    */
    $pdf->SetFont('helvetica', '', 10); // Schriftart festlegen
    switch ($customer['connect_type']) {
      // DSL
      case 'DSL':
        $pdf->SetXY(13.3, 14.5, 1);
        $pdf->Write(0, 'X', '', 0, 'L');
        break;
      // Glasfaser
      case  'Glasfaser':
        $pdf->SetXY(31.3, 14.5, 1);
        $pdf->Write(0, 'X', '', 0, 'L');
        break;
      // Breitbandkabel
      case 'Breitbandkabel':
        $pdf->SetXY(60.7, 14.5, 1);
        $pdf->Write(0, 'X', '', 0, 'L');
        break;
    }
    /**************************************
    * Produktauswahl
    */

    switch ($customer['productname']) {
      // Highspeed 10 MBit/S
      case 'CBPA0010H':
        $pdf->SetXY(15.7, 74.3, 1);
        $pdf->Write(0, 'X', '', 0, 'L');
        break;
      // Highspeed 20 MBit/S
      case 'CBPA0020H':
        $pdf->SetXY(15.7, 81.4, 1);
        $pdf->Write(0, 'X', '', 0, 'L');
        break;
      // Highspeed 30 MBit/S
      case 'CBPA0030H':
        $pdf->SetXY(15.7, 88.6, 1);
        $pdf->Write(0, 'X', '', 0, 'L');
        break;
      // Highspeed 40 MBit/S
      case 'CBPA0040H':
        $pdf->SetXY(15.7, 95.6, 1);
        $pdf->Write(0, 'X', '', 0, 'L');
        break;
      // Highspeed 50 MBit/S
      case 'CBPA0050H':
        $pdf->SetXY(15.7, 102.8, 1);
        $pdf->Write(0, 'X', '', 0, 'L');
        break;
      // Highspeed 75 MBit/S
      case 'CBPA0075H':
        $pdf->SetXY(15.7, 109.9, 1);
        $pdf->Write(0, 'X', '', 0, 'L');
        break;
      // Highspeed 100 MBit/S
      case 'CBPA0100H':
        $pdf->SetXY(15.7, 117.1, 1);
        $pdf->Write(0, 'X', '', 0, 'L');
        break;
    }
    
    // Einrichtung des Anschlusses
    if ($customer['connection_installation'] == 'Ja') {
      $pdf->SetXY(15.7, 126.4, 1);
      $pdf->Write(0, 'X', '', 0, 'L');
    }
    // Versandkostenpauschale
    if ($customer['fordering_costs'] == 'Ja') {
      $pdf->SetXY(15.7, 134, 1);
      $pdf->Write(0, 'X', '', 0, 'L');
    }
    /**************************************
    * Zusatzoptionen
    */
    foreach ($options as $option) {
      switch ($option) {
        // Zusätzlicher Telefonkanal
        case 'CTTKPKDFP':
          $pdf->SetXY(103.8, 74.4, 1);
          $pdf->Write(0, 'X', '', 0, 'L');
          break;
        // EU-Flatrate
        case 'CTAEUFFL':
          $pdf->SetXY(103.8, 81.5, 1);
          $pdf->Write(0, 'X', '', 0, 'L');
          break;
        // Mobilfunk-Flatrate
        case 'CTDMFLXX':
          $pdf->SetXY(103.8, 88.7, 1);
          $pdf->Write(0, 'X', '', 0, 'L');
          break;
        // Papierrechnung
        case 'CZPPRXXX':
          $pdf->SetXY(103.8, 95.7, 1);
          $pdf->Write(0, 'X', '', 0, 'L');
          break;
        // Höherwertiges Endgerät 1
        case 'CNFBPHXXT':
          $pdf->SetXY(103.8, 102.9, 1);
          $pdf->Write(0, 'X', '', 0, 'L');
          $pdf->SetXY(148.3, 101.5, 1);
          $pdf->Write(0, 'X', '', 0, 'L');
          break;
        // Höherwertiges Endgerät 2
        case 'CNFBPEXXT':
          $pdf->SetXY(103.8, 102.9, 1);
          $pdf->Write(0, 'X', '', 0, 'L');
          $pdf->SetXY(169.2, 101.5, 1);
          $pdf->Write(0, 'X', '', 0, 'L');
          break;
        // Servicetechniker 
        case 'CSST15XX':
          $pdf->SetXY(103.8, 109.9, 1);
          $pdf->Write(0, 'X', '', 0, 'L');
          break;
        // Highspeed-Internet SOFORT
        case 'CBPADSXX':
          $pdf->SetXY(103.8, 117, 1);
          $pdf->Write(0, 'X', '', 0, 'L');
          break;
        // TV-Dienst
        case 'CFTV1XXX':
          $pdf->SetXY(103.8, 124.3, 1);
          $pdf->Write(0, 'X', '', 0, 'L');
          break;
        // Jeder weitere Satellit
        case 'CFTV2XXX':
          $pdf->SetXY(103.8, 128, 1);
          $pdf->Write(0, 'X', '', 0, 'L');
          break;
        // HD+ Karte
        case 'CFTVHD1X':
          $pdf->SetXY(103.8, 134, 1);
          $pdf->Write(0, 'X', '', 0, 'L');
          break;
        // Glasfaser-Hausanschluss
        case 'CHAGFIND':
          $pdf->SetXY(103.8, 141.5, 1);
          $pdf->Write(0, 'X', '', 0, 'L');
          break;
      }
    }
    /**************************************
    * Mein derzeitiger Anschluss
    */
    switch ($customer['oldcontract_phone_type']) {
      // Analog
      case 'Analog':
        $pdf->SetXY(14.6, 186.5, 1);
        $pdf->Write(0, 'X', '', 0, 'L');
        break;
      // ISDN
      case 'ISDN':
        $pdf->SetXY(14.6, 191.1, 1);
        $pdf->Write(0, 'X', '', 0, 'L');
        break;
      // ISDN Anlage
      case 'ISDN-Anlage':
        $pdf->SetXY(14.6, 195.5, 1);
        $pdf->Write(0, 'X', '', 0, 'L');
        break;
    }
    
    // Ich möchte folgende Rufnummer/n mitnehmen
    if ($customer['oldcontract_porting'] == 'Ja') {
      $pdf->SetXY(38.2, 186, 1);
      $pdf->Write(0, 'X', '', 0, 'L');
    }
    // Ich habe zur Zeit keinen Anschluss
    if ($customer['oldcontract_exists'] == 'Nein') {
      $pdf->SetXY(102.1, 186, 1);
      $pdf->Write(0, 'X', '', 0, 'L');
    }
    

    /***********************************/  
    $pdf->Output($date.'_'.$customer['lastname'].'_'.$customer['firstname'].'.pdf', 'D');
  }
  $db->close();
?>
