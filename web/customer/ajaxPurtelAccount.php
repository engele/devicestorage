<?php

$debugLog = false;

if ($debugLog) {
    ob_start();
}

$StartPath = urldecode($_POST['StartPath']);
$SelPath = urldecode($_POST['SelPath']);
$StartURL = urldecode($_POST['StartURL']);

require_once $StartPath.'/_conf/database.inc';
require_once $StartPath.'/'.$SelPath.'/_conf/database.inc';
require_once $StartPath.'/vendor/wisotel/configuration/Configuration.php';

$Purtel = array_merge(
    \Wisotel\Configuration\Configuration::get('purtelAccountValues'),
    json_decode($_POST['Purtel'], true)
);

$Purtel2 = json_decode($_POST['Purtel2'], true);

if (JSON_ERROR_NONE !== json_last_error()) {
    $response = array(
        'purtelLogin' => null,
        'purtelPassword' => null,
        'purtelMaster' => null,
        'purtelPorting' => null,
        'purtelNummer' => null,
        'purtelTyp' => null,
        'purtelInsid' => null,
        'PurtelUrl' => null,
        'result' => null,
        'status' => 0,
        'status2' => 0,
        'status3' => 0
    );

    print_r(json_encode($response));
    exit;
}

$Purtel3 = json_decode($_POST['Purtel3'], true);

if (JSON_ERROR_NONE !== json_last_error()) {
    $response = array(
        'purtelLogin' => null,
        'purtelPassword' => null,
        'purtelMaster' => null,
        'purtelPorting' => null,
        'purtelNummer' => null,
        'purtelTyp' => null,
        'purtelInsid' => null,
        'PurtelUrl' => null,
        'result' => null,
        'status' => 0,
        'status2' => 0,
        'status3' => 0
    );

    print_r(json_encode($response));
    exit;
}

if ($debugLog) {
    echo $_POST['Purtel2']."\n\n";
    var_dump('post-Purtel2', $_POST['Purtel2'], json_decode($_POST['Purtel2'], true));
    var_dump('post-Purtel3', $_POST['Purtel3'], json_decode($_POST['Purtel3'], true));
}

$username = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$password = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

$puraccount = array();
$action = 'getaccounts';
$action1 = 'createcontract';
$action2 = 'changecontract';
$status = 0;
$status2 = 0;
$status3 = 0;
$purtelPassword = '';
$PurtelUrl = '';
$PurtelUrl1 = '';
$PurtelUrl2 = '';

// Produktnummer von Masteraccount
if ($Purtel3['purtelMaster']) {
    $parameter = array(
        'kundennummer_extern' => $Purtel['kundennummer_extern'],
        'erweitert' => 1,
    );

    $PurtelUrl = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($username),
        urlencode($password),
        urlencode($action)
    );

    foreach ($parameter as $key => $value) {
        $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
    }

    $curl = curl_init ($PurtelUrl);

    curl_setopt_array($curl, array(
        CURLOPT_URL             => $PurtelUrl,
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));

    $result = curl_exec($curl);

    curl_close($curl);

    $lines = explode ("\n", $result);

    array_pop($lines);

    $headline_csv = array_shift($lines);
    $headline = str_getcsv($headline_csv, ';');

    while ($line = array_shift ($lines)) {
        $purt = str_getcsv($line, ';');

        foreach ($headline as $ind => $head) {
            $temp[$head] = $purt[$ind];
        }

        $puraccount[] = $temp;
    }

    $Purtel['produkt'] = $puraccount[0]['produkt'];
}

// Purtelkunde anlegen
if (!$Purtel3['PurtelLogin']) {
    $PurtelUrl1 = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($username),
        urlencode($password),
        urlencode($action1)
    );

    $Purtel['skip_timeout'] = 1;
    
    foreach ($Purtel as $key => $value) {
        $PurtelUrl1 .= "&$key=".urlencode(utf8_decode($value));
    }

    $curl = curl_init($PurtelUrl1);

    curl_setopt_array($curl, array(
        CURLOPT_URL             => $PurtelUrl1,
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));

    $result_safe = $PurtelUrl1;
    $result = curl_exec($curl);

    if ($debugLog) {
        var_dump('Hauptaccount anlegen result:', $result);
    }
    
    curl_close($curl);

    $resultArray = str_getcsv($result, ';');

    if ($resultArray[0] == '+OK') {
        $status = 1;

        foreach ($resultArray as $resultKey => $resultValue) {
            if (strpos($resultValue, 'benutzername=') !== false) {
                $Purtel3['PurtelLogin'] = substr($resultValue, 13);
            } else if (strpos($resultValue, 'passwort=') !== false) {
                $Purtel3['PurtelPassword'] = substr($resultValue, 9);
            }
        }
    } else {
        $result_safe = $result;
    }
} else {
    $status = 2;
}

//  Ergänzungen hinzufügen
if ($status == 1) {
    $Purtel2['anschluss'] = $Purtel3['PurtelLogin'];

    $PurtelUrl2 = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($username),
        urlencode($password),
        urlencode($action2)
    );

    foreach ($Purtel2 as $key => $value) {
        if ($value) {
            $PurtelUrl2 .= "&$key=".urlencode(utf8_decode($value));
        }
    }

    if ($debugLog) {
        var_dump('Subaccount anlegen url:', $PurtelUrl2);
    }

    $curl = curl_init ($PurtelUrl2);

    curl_setopt_array($curl, array(
        CURLOPT_URL             => $PurtelUrl2,
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));

    $result = curl_exec($curl);

    if ($debugLog) {
        var_dump('Subaccount anlegen result:', $result);
    }

    curl_close($curl);

    $resultArray = str_getcsv($result, ';');

    if ($resultArray[0] == '+OK') {
        $status2 = 1;
    }
} elseif ($status == 2) {
    $status2 = 2;
}

$purtel_insid = null;

if ($status2 != 0) {
    $porting = 0;

    if (!empty($Purtel3['PurtelPorting']) && is_numeric($Purtel3['PurtelPorting'])) {
        $porting = $Purtel3['PurtelPorting'];
    }

    $sql_insert  = "INSERT INTO purtel_account SET \n"
        ."purtel_login = '".$Purtel3['PurtelLogin']."',\n"
        ."purtel_password = '".$Purtel3['PurtelPassword']."',\n"
        ."master = '".$Purtel3['PurtelMaster']."',\n"
        ."porting = '".$porting."',\n"
        ."nummer = '".$Purtel3['PurtelNummer']."',\n"
        ."typ = '".$Purtel3['PurtelTyp']."',\n"
        ."cust_id = '".$Purtel3['CustomerId']."'\n";

    $db_save_result = $db->query($sql_insert);

    if (!empty($db->error)) {
        if ($debugLog) {
            var_dump('DB error:', $db->error);
        } else {
            trigger_error($db->error, E_USER_ERROR);
        }
    }

    $purtel_insid = $db->insert_id;
    $status3 = 1;
}

$response = array(
    'purtelLogin' => $Purtel3['PurtelLogin'],
    'purtelPassword' => $Purtel3['PurtelPassword'],
    'purtelMaster' => $Purtel3['PurtelMaster'],
    'purtelPorting' => $Purtel3['PurtelPorting'],
    'purtelNummer' => $Purtel3['PurtelNummer'],
    'purtelTyp' => $Purtel3['PurtelTyp'],
    'purtelInsid' => $purtel_insid,
    'PurtelUrl' => $PurtelUrl1,
    'result' => utf8_encode($result_safe),
    'status' => $status,
    'status2' => $status2,
    'status3' => $status3
);

if ($debugLog) {
    var_dump('Response:', $response);

    file_put_contents(__FILE__.'.debug.log', ob_get_clean());

    ob_end_clean();
}

print_r(json_encode($response));

?>
