<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_CONNECTION');

$disable = 'readonly="readonly"';
$rw = false;   

if ($authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_CONNECTION')) {
    $disable = '';   
    $rw = true;   
}

?>

<h3 class="CustConnection">Anschluss</h3>
<div class="CustConnection form">
  <span class="four">
  <?php if ($rw) { ?>
    <button onclick="copyMasterAddressToConnection()" type="button">Adresse aus Kundendaten einfügen</button>
  <?php } ?>
  </span>
  <span class="one_half">
    <label for="CustomerConnectionStreet03">Straße *</label><br>
    <input id="CustomerConnectionStreet03" type="text" maxlength="64" name="CustomerConnectionStreet" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'connection_street'); ?>">
  </span>
  <span class="half">
    <label for="CustomerConnectionStreetno03">Hausnummer *</label><br>
    <input id="CustomerConnectionStreetno03" type="text" maxlength="8" name="CustomerConnectionStreetno" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'connection_streetno'); ?>">
  </span>
  <span>
    <label for="CustomerConnectionZipcode03">PLZ *</label><br>
    <input id="CustomerConnectionZipcode03" type="text" maxlength="8" name="CustomerConnectionZipcode" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'connection_zipcode'); ?>">
  </span>
  <span>
    <label for="CustomerConnectionCity03">Stadt *</label><br>
    <select id="CustomerConnectionCity03" name="CustomerConnectionCity" <?php echo $disable; ?>>
      <?php setOption ($option_connection_city, $customer, 'connection_city'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerDistrict03">Ortsteil *</label><br>
    <select id="CustomerDistrict03" name="CustomerDistrict" <?php echo $disable; ?>>
      <?php setOption ($option_district, $customer, 'district'); ?>
    </select>
  </span>
  <span class="double">
    <label for="CustomerNetworkId03">Netz</label><br>
    <select id="CustomerNetworkId03" name="CustomerNetworkId" <?php echo $disable; ?>>
      <?php setOption ($networks, $customer, 'network_id'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerProspectSupplyStatus03">Versorgbarkeit</label>
    <select id="CustomerProspectSupplyStatus03" name="CustomerProspectSupplyStatus" <?php echo $disable; ?>>
      <?php setOption ($option_prospect_supply_status, $customer, 'prospect_supply_status'); ?>
    </select>
  </span>
</div>
