<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_PROVIDER_CHANGE');

$disable = 'readonly="readonly"';
$rw = false;   

if ($authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_PROVIDER_CHANGE')) {
    $disable = '';   
    $rw = true;   
}

?>
<h3 class="CustVentelo">Ventelo</h3>
<div class="CustVentelo form">
    <span class="double"><br /><?php if($rw) {?><button onclick="fillPortingdata()" type="button">Kein Altvertrag, neue Rufnr. oder nur Datenltg.</button><?php } ?></span>
    <span class="double"></span>
    
    <span>
        <label for="CustomerVenteloPortLetterDate16">Portierung versendet am</label>
        <input type="text" id="CustomerVenteloPortLetterDate16" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'ventelo_port_letter_date'); ?>" maxlength="11" class="datepicker" name="CustomerVenteloPortLetterDate">
    </span>
    <span>
        <label for="CustomerVenteloPortLetterHow16">per</label><br />
        <select id="CustomerVenteloPortLetterHow16" name="CustomerVenteloPortLetterHow" <?php echo $disable; ?>>
        <?php setOption ($option_ventelo_port_letter_how, $customer, 'ventelo_port_letter_how'); ?>
        </select>
    </span>
    <span class="double">
        <label for="CustomerVenteloPortLetterFrom16">Portierung versendet von</label><br />
        <select id="CustomerVenteloPortLetterFrom16" name="CustomerVenteloPortLetterFrom" <?php echo $disable; ?>>
        <?php setOption ($option_ventelo_port_letter_from, $customer, 'ventelo_port_letter_from'); ?>
        </select>
    </span>

    <span class="double">
        <label for="CustomerVenteloConfirmationDate16">Portierung erhalten am</label><br />
        <input type="text" id="CustomerVenteloConfirmationDate16" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'ventelo_confirmation_date'); ?>" class="datepicker" name="CustomerVenteloConfirmationDate">
    </span>
    <span>
        <label for="CustomerVenteloPurtelEnterDate16">Port. eingestellt am</label><br />
        <input type="text" id="CustomerVenteloPurtelEnterDate16" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'ventelo_purtel_enter_date'); ?>" class="datepicker" name="CustomerVenteloPurtelEnterDate">
    </span>
    <span>
        <label for="CustomerVenteloPurtelEnterFrom16">Port. hochgeladen von</label><br />
        <select id="CustomerVenteloPurtelEnterFrom16" name="CustomerVenteloPurtelEnterFrom" <?php echo $disable; ?>>
        <?php setOption ($option_ventelo_purtel_enter_from, $customer, 'ventelo_purtel_enter_from'); ?>
        </select>
    </span>

    <span class="doublebox">
        <span class="double">
            <label for="CustomerVenteloPortWishDate16">Portierungswunschdatum</label><br />
            <input type="text" id="CustomerVenteloPortWishDate16" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'ventelo_port_wish_date'); ?>" class="datepicker" name="CustomerVenteloPortWishDate">
        </span>
        <span>
            <label class="" for="CustomerStatiPortConfirmDate16">Bestätigtes Portdat.</label><br />
            <input type="text" id="CustomerStatiPortConfirmDate16" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'stati_port_confirm_date'); ?>" class="datepicker" name="CustomerStatiPortConfirmDate">
        </span>
        <span>
            <label for="CustomerPhoneNumberAdded16">Nur RN Aufschaltung</label><br />
            <select id="CustomerPhoneNumberAdded16" name="CustomerPhoneNumberAdded" <?php echo $disable; ?>>
            <?php setOption ($option_phone_number_added, $customer, 'phone_number_added'); ?>
            </select>
        </span>
    </span>
    <span class="double">
        <label for="CustomerPortdefaultComment16">Rückmeldung Anbieterwechsel</label>
        <textarea id="CustomerPortdefaultComment16" name="CustomerPortdefaultComment" <?php echo $disable; ?>><?php setInputValue ($customer, 'portdefault_comment'); ?></textarea>
    </span>
</div>