<?php
//------------------------------------------------------------------------------
require_once ($StartPath.'/_conf/database.inc');
require_once ($StartPath.'/_conf/function.inc');
require_once ($StartPath.'/'.$SelPath.'/_conf/database.inc');
require_once ($StartPath.'/'.$SelPath.'/_conf/form.inc');

require_once ($StartPath.'/'.$SelPath.'/Lib/Terminal/Terminal.php');
require_once ($StartPath.'/'.$SelPath.'/Lib/IpAddress/IpAddress.php');
require_once ($StartPath.'/'.$SelPath.'/Lib/IpAddress/IpRangeCollection.php');
require_once ($StartPath.'/'.$SelPath.'/Lib/IpAddress/IpAddressCollection.php');
require_once ($StartPath.'/'.$SelPath.'/Lib/IpAddress/DummyIpRange.php');
require_once ($StartPath.'/'.$SelPath.'/Lib/Customer/Customer.php');
require_once ($StartPath.'/'.$SelPath.'/Lib/Customer/History/History.php');
require_once ($StartPath.'/'.$SelPath.'/Lib/Customer/History/EventList.php');
require_once ($StartPath.'/'.$SelPath.'/Lib/PppoeUsername/PppoeUsername.php');

use AppBundle\Form\Type\OtrsAdapter\OtrsTicketCreateType;
use AppBundle\src\OtrsAdapter\OtrsAdapter;
use customer\Lib\Terminal\Terminal;
use customer\Lib\IpAddress\IpAddress;
use customer\Lib\IpAddress\IpRangeCollection;
use customer\Lib\IpAddress\IpAddressCollection;
use customer\Lib\IpAddress\DummyIpRange;
use customer\Lib\Customer\Customer;
use customer\Lib\Customer\History\History;
use customer\Lib\Customer\History\EventList as HistoryEventList;
use AppBundle\Entity\Location\CardPort;
use AppBundle\Util\PppoePasswordGenerator;
use AppBundle\Entity\Customer\CustomerVlanProfile;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Util\LineIdentifierGenerator;
use AppBundle\Util\KvzSchaltnummerGenerator;
use AppBundle\Entity\Location\Card;

$historyEventList     = new HistoryEventList($db, $sql);
$customer             = array();
$locations            = array();
$locationsdb          = array();
$cards                = array();
$cardsdb              = array();
$dslam_ports          = array();
$lsa_kvz_partitions   = array();
$lsa_pins             = array();
//$ip_ranges            = array();
//$ip_addresses         = array();
$products             = array();
$purtelNewAccount     = array();
$purtel_account       = array();
$purtel_option        = array();
$lsa_kvz_partitionsdb = array();
$purtel_master        = '';
$purtel_masterpw      = '';
$ip_subnetmask        = '';
$ip_vlanid            = '';
$ip_DPBO              = 0;

// connection_city from Database
$db_city = $db->query($sql['connection_city']);
$citydb = $db_city->fetch_all(MYSQLI_ASSOC);
$db_city->close();
  
$option_connection_city = array ('-n-' => '');
foreach ($citydb as $temp) {
    $option_connection_city[$temp['city']] = $temp['city'];
    $cityArray[$temp['id']] = $temp['city'];
}
// district form database
$option_district = array('-n-' => '');
$db_district = $db->query($sql['connection_district']);
$districtdb = $db_district->fetch_all(MYSQLI_ASSOC);
/*$city_id = 0;
foreach ($districtdb as $temp) {
    if ($temp['city_id'] != $city_id) {
        $city_id = $temp['city_id'];
        $option_district[sprintf('--%02d', $city_id)] = $cityArray[$city_id];
    }
    $option_district[$temp['district']] = $temp['district'];
    $connectionZipcode[$temp['district']] = $temp['zipcode'];
}*/
$groupedDistricts = [];
foreach ($districtdb as $temp) {
    if (!isset($groupedDistricts[$temp['city_id']])) {
        $groupedDistricts[$temp['city_id']] = [];
    }

    $groupedDistricts[$temp['city_id']][$temp['district']] = $temp['district'];
    $connectionZipcode[$temp['district']] = $temp['zipcode'];
}
foreach ($groupedDistricts as $tempKey => $temp) {
    $city_id = $tempKey;
    $option_district[sprintf('--%02d', $city_id)] = $cityArray[$city_id];
    $option_district += $temp;
}
unset($groupedDistricts);
$db_district->close();

// getnetwork from Database
$db_networks = $db->query($sql['networks']);
$networksdb = $db_networks->fetch_all(MYSQLI_ASSOC);
$db_networks->close();
  
$networks = array ('-n-' => '');
foreach ($networksdb as $network) {
    $networks[$network['id']] = $network['name']." (".$network['id_string'].")";
    $area_code[$network['id']]  = $network['area_code'];
}

if (isset($_POST['save'])) {
    // Create an empty Customer object
    $customer_addSomethingToThisNameToMakeItUnique = new Customer($db, $sql);
    $customer_addSomethingToThisNameToMakeItUnique->setId((int) $_POST['CustomerId']);

    // Application Status
    if (isset ($_POST['CustomerProspectSupplyStatus'])) {
        $_POST['CustomerApplicationText'] = $_POST['CustomerProspectSupplyStatus'];
        switch ($_POST['CustomerProspectSupplyStatus']) {
            case 'with_supply':
                $_POST['CustomerApplication'] = 'green';
                break;
            case 'without_supply':
                $_POST['CustomerApplication'] = 'red';
                break;
            case 'supply_unknown':
                $_POST['CustomerApplication'] = 'yellow';
                break;
            case 'not_now_supply':
                $_POST['CustomerApplication'] = 'yellow';
                break;
            default:
                $_POST['CustomerApplication'] = 'red';
        } 
    } 
    // Contract Status
    if ($_POST['CustomerApplication'] != 'green') {
        $_POST['CustomerContract']      = 'gray';
        $_POST['CustomerContractText']  = 'application_not_done';
    } else {
        if (isset($_POST['CustomerContractSentDate']) && $_POST['CustomerContractReceivedDate']) {
            $_POST['CustomerContract']      = 'green';
            $_POST['CustomerContractText']  = 'contract_received###<i>'.$_POST['CustomerContractReceivedDate'].'</i>';
        } elseif (isset($_POST['CustomerContractSentDate'])) {
            $_POST['CustomerContract']      = 'yellow';
            $_POST['CustomerContractText']  = 'waiting_for_contract';
        } else {
            $_POST['CustomerContract']      = 'yellow';
            $_POST['CustomerContractText']  = 'contract_not_sent';
        }
      
        if (!isset($_POST['CustomerWisocontractCanceledDate']) || empty($_POST['CustomerWisocontractCanceledDate'])) {
            $status_cancontract = 'green';
            $_POST['CustomerContractText'] .= ', contract_not_cancelled';
        } else {
            $status_cancontract = 'red';
            $_POST['CustomerContractText'] .= ', contract_cancelled_on###<i>'.$_POST['CustomerWisocontractCanceledDate'].'</i>';
        }
     
        if (isset($_POST['CustomerCreditRatingOk']) && $_POST['CustomerCreditRatingOk'] == 'successful') {
            $status_credit = 'green';
            $_POST['CustomerContractText'] .= ', credit_rating_ok';
        } elseif (isset($_POST['CustomerCreditRatingOk']) && $_POST['CustomerCreditRatingOk'] == 'outstanding') {
            $status_credit = 'yellow';
            $_POST['CustomerContractText'] .= ', credit_rating_outstanding';
        } elseif (isset($_POST['CustomerCreditRatingOk']) && $_POST['CustomerCreditRatingOk'] == 'in_process') {
            $status_credit = 'yellow';
            $_POST['CustomerContractText'] .= ', credit_rating_in_process';
        } else {
            $status_credit = 'red';
            $_POST['CustomerContractText'] .= ', credit_rating_not_ok';
        }

        if ($_POST['CustomerNetworkId']) {
            $status_network_id = 'green';
        } else {
            $status_network_id = 'yellow';
            $_POST['CustomerContractText'] .= ', network_id_not_ok';
        }

        if (isset($_POST['CustomerLocationId']) && !empty($_POST['CustomerLocationId'])) {
            $status_dslam_location = 'green';
        } else {
            $status_dslam_location = 'yellow';
            $_POST['CustomerContractText'] .= ', dslam_location_missing';
        }

        if ($status_credit == "green" && $_POST['CustomerContract'] == "green" && $status_network_id == "green" && $status_dslam_location == "green" && $status_cancontract == "green") {
            $_POST['CustomerContract'] = "green";
        } elseif ($status_credit == "yellow" or $_POST['CustomerContract'] == "yellow" or $status_network_id == "yellow" or $status_dslam_location == "yellow" or $status_cancontract == "yellow") {
            $_POST['CustomerContract'] = "yellow";
        }
        if ($status_credit == "red" or $_POST['CustomerContract'] == "red" or $status_network_id == "red" or $status_cancontract == "red") {
            $_POST['CustomerContract'] = "red";
        }
    }
    // Porting Status
    if ($_POST['CustomerContract'] != 'green' || !isset ($_POST['CustomerId'])) {
        $_POST['CustomerPorting']      = 'gray';
        $_POST['CustomerPortingText']  = 'contract_not_done';
    } elseif ($_POST['CustomerWbciBestaetigtAm'] && $_POST['CustomerWbciEingestelltAm']) {
        $_POST['CustomerPorting']      = 'green';
        $_POST['CustomerPortingText']  = 'porting_confirmed###<i>'.$_POST['CustomerWbciBestaetigtAm'].'</i> (WBCI)';
    } elseif ($_POST['CustomerStatiPortConfirmDate'] && $_POST['CustomerVenteloPurtelEnterDate']) {
        $_POST['CustomerPorting']      = 'green';
        $_POST['CustomerPortingText']  = 'porting_confirmed###<i>'.$_POST['CustomerStatiPortConfirmDate'].'</i>';
    } elseif ($_POST['CustomerVenteloPurtelEnterDate']) {
        $_POST['CustomerPorting']      = 'yellow';
        $_POST['CustomerPortingText']  = 'purtel_letter_uploaded###<i>'.$_POST['CustomerVenteloPurtelEnterDate'].'</i>';
    } else {
        $_POST['CustomerPorting']      = 'yellow';
        $_POST['CustomerPortingText']  = 'purtel_letter_not_uploaded';
    }
    // TAL Order Status
    if ($_POST['CustomerContract'] != 'green') {
        $_POST['CustomerTalorder'] = 'gray';
        $_POST['CustomerTalorderText'] = 'talorder_not_done';
    } elseif ($_POST['CustomerTalOrderDate'] && $_POST['CustomerTalOrderAckDate']) {
        $_POST['CustomerTalorder'] = 'green';
        $_POST['CustomerTalorderText'] = 'tal_order_confirmed###<i>'.$_POST['CustomerTalOrderAckDate'].'</i>';
    } elseif ($_POST['CustomerTalOrderDate']) {
        $_POST['CustomerTalorder'] = 'yellow';
        $_POST['CustomerTalorderText'] = 'tal_ordered###<i>'.$_POST['CustomerTalOrderDate'].'</i>';
    } else {
        $_POST['CustomerTalorder'] = 'yellow';
        $_POST['CustomerTalorderText'] = 'tal_not_ordered';
    }
    // Implementation Status
    if ($_POST['CustomerPorting'] != 'green' && (!isset($_POST['CustomerTalOrder']) || $_POST['CustomerTalOrder'] != 'green')) {
        $_POST['CustomerImplementation'] = 'gray';
        $_POST['CustomerImplementationText'] = 'talorder_and_porting_not_done'; 
    } elseif (!isset($_POST['CustomerTalorder']) || $_POST['CustomerTalorder'] != 'green') {
        $_POST['CustomerImplementation'] = 'gray';
        $_POST['CustomerImplementationText'] = 'talorder_not_done'; 
    } elseif ($_POST['CustomerPorting'] != 'green') {
        $_POST['CustomerImplementation'] = 'gray';
        $_POST['CustomerImplementationText'] = 'porting_not_done'; 
    } else {
        if ($_POST['CustomerPppoeConfigDate']) {
            $status_pppoe = 'green';
            $_POST['CustomerImplementationText'] = 'pppoe_done###<i>'.$_POST['CustomerPppoeConfigDate'].'</i>';
        } else {
            $status_pppoe = 'yellow';
            $_POST['CustomerImplementationText'] = 'pppoe_not_done';
        }
      
        if ($_POST['CustomerDslamArrangedDate']) {
            $status_dslam = 'green';
            $_POST['CustomerImplementationText'] .= 'DSLAM_done_date###<i>'.$_POST['CustomerDslamArrangedDate'].'</i>';
        } else {
            $status_dslam = 'yellow';
            $_POST['CustomerImplementationText'] .= 'DSLAM_not_done';
        }
      
        if ($_POST['CustomerTerminalReady']) {
            $status_terminal = 'green';
            $_POST['CustomerImplementationText'] .= 'terminal_ready###<i>'.$_POST['CustomerTerminalReady'].'</i>';
        } else {
            $status_terminal = 'yellow';
            $_POST['CustomerImplementationText'] .= 'terminal_not_ready';
        }
      
        if ($_POST['CustomerPatchDate']) {
            $status_patch = 'green';
            $_POST['CustomerImplementationText'] .= 'patch_done###<i>'.$_POST['CustomerPatchDate'].'</i>';
        } else {
            $status_patch = 'yellow';
            $_POST['CustomerImplementationText'] .= 'patch_not_done';
        }
      
        if ($_POST['CustomerConnectionActivationDate']) {
            $status_connection = 'green';
            $_POST['CustomerImplementationText'] .= 'connection_activated###<i>'.$_POST['CustomerConnectionActivationDate'].'</i>';
        } else {
            $status_connection = 'yellow';
            $_POST['CustomerImplementationText'] .= 'connection_not_activated';
        }
        if ($status_pppoe == "green" && $status_terminal == "green" && $status_patch == "green" && $status_connection == "green" &&$status_dslam == "green") {
            $_POST['CustomerImplementation'] = "green";
        } elseif ($status_pppoe == "yellow" or $status_terminal == "yellow" or $status_patch == "yellow" or $status_connection == "yellow" or $status_dslam == "yellow") {
            $_POST['CustomerImplementation'] = "yellow";
        }
        if ($status_pppoe == "red" or $status_terminal == "red" or $status_patch == "red" or $status_connection == "red" or $status_dslam == "red") {
            $_POST['CustomerImplementation'] = "red";
        }
    }

    $historyEventNotes = [
        'CustomerContractOnlineDate' => [
            'eventId' => 6,
            'note' => 'Vertrag elektronisch erfaßt am: %s',
        ],
        'CustomerWbciEingestelltAm' => [
            'eventId' => 12,
            'note' => 'WBCI eingestellt am: %s',
        ],
        'CustomerPhoneentryDoneDate' => [
            'eventId' => 13,
            'note' => 'Telefonnummer(n) im Telefonbuch eingtragen am: %s',
        ],
        'CustomerTalOrderAckDate' => [
            'eventId' => 15,
            'note' => 'TAL bestellt,geändert, gekündigt am: %s',
        ],
        'CustomerTvsDate' => [
            'eventId' => 16,
            'note' => 'TAL Terminverschiebung am: %s',
        ],
        'CustomerTalCancelAckDate' => [
            'eventId' => 21,
            'note' => 'TAL gekündigt am: %s',
        ],
        'CustomerDslamArrangedDate' => [
            'eventId' => 27,
            'note' => 'Kunde eingerichtet am: %s',
        ],
        'CustomerPatchDate' => [
            'eventId' => 28,
            'note' => 'Kunde gepatched am: %s',
        ],
        'CustomerTerminalReady' => [
            'eventId' => 29,
            'note' => 'Endgerät eingerichtet am: %s',
        ],
        'CustomerTerminalSendedDate' => [
            'eventId' => 30,
            'note' => 'Endgerät versendet am: %s',
        ],
        'CustomerCommissioningDate' => [
            'eventId' => 65,
            'note' => 'Kunde inbetriebgenommen am: %s',
        ],
        'CustomerConnectionActivationDate' => [
            'eventId' => 31,
            'note' => 'Kunde aktivgeschaltet am: %s',
        ],
        'CustomerNewNumberDate' => [
            'eventId' => 40,
            'note' => 'Rufnummer bestellt am: %s',
        ],
    ];

    $addHistoryAtEvent = []; // progress auto-insert

    $post = $request->request;

    foreach ($historyEventNotes as $postKey => $eventNote) {
        $postValue = $post->get($postKey);

        if (!empty($postValue)) {
            try {
                // test if it is a valid date
                $date = new \DateTime($postValue);

                $eventNote['note'] = sprintf($eventNote['note'], $postValue);

                $addHistoryAtEvent[$postKey] = $eventNote;
            } catch (\Exception $e) {
                // nothing to do
            }
        }
    }

    if ($post->has('CustomerRegAnswerDate')) {
        $addHistoryAtEvent['CustomerContractReceivedDate'] = [
            'eventId' => 5,
            'note' => 'Vertrag erfaßt am: '.$post->get('CustomerRegAnswerDate'),
        ];
    }

    $username = $this->get('security.token_storage')->getToken()->getUser()->getUsername();
    $now = new DateTime('now');

    foreach ($addHistoryAtEvent as $eventTrigger => $historyData) {
        if (false !== strpos($_POST['changed_fields'], $eventTrigger)) {
            $customer_addSomethingToThisNameToMakeItUnique->addHistory(
                History::newInstance()
                    ->setCreatedBy($username)
                    ->setCreatedAt($now)
                    ->setNote($historyData['note'])
                    ->setEvent(
                        $historyEventList->getEventById($historyData['eventId'])
                    )
            );
        }
    }

    if (false !== strpos($_POST['changed_fields'], 'CustomerVenteloPortLetterDate') 
        && 'N/E' !== $_POST['CustomerVenteloPortLetterDate']
        && !empty($_POST['CustomerVenteloPortLetterDate'])) {
        
        $customer_addSomethingToThisNameToMakeItUnique->addHistory(
            History::newInstance()
                ->setCreatedBy($username)
                ->setCreatedAt($now)
                ->setNote('Anbieterwechsel beauftragt am: '.$_POST['CustomerVenteloPortLetterDate'])
                ->setEvent(
                    $historyEventList->getEventById(7)
                )
        );
    }
    if (false !== strpos($_POST['changed_fields'], 'CustomerVenteloPurtelEnterDate') 
        && 'N/E' !== $_POST['CustomerVenteloPurtelEnterDate']
        && !empty($_POST['CustomerVenteloPurtelEnterDate'])) {

        $customer_addSomethingToThisNameToMakeItUnique->addHistory(
            History::newInstance()
                ->setCreatedBy($username)
                ->setCreatedAt($now)
                ->setNote('Anbieterwechsel-Fax versendet am: '.$_POST['CustomerVenteloPurtelEnterDate'])
                ->setEvent(
                    $historyEventList->getEventById(8)
                )
        );
    }
    if (false !== strpos($_POST['changed_fields'], 'CustomerPhoneNumberAdded') && !empty($_POST['CustomerPhoneNumberAdded'])) {
        $customer_addSomethingToThisNameToMakeItUnique->addHistory(
            History::newInstance()
                ->setCreatedBy($username)
                ->setCreatedAt($now)
                ->setNote('Rufnummern aufgeschaltet!')
                ->setEvent(
                    $historyEventList->getEventById(33)
                )
        );
    }

    $_POST['changed_fields'] .= "CustomerApplication|CustomerApplicationText|CustomerContract|CustomerContractText|CustomerPorting|CustomerPortingText|CustomerTalorder|CustomerTalorderText|CustomerImplementation|CustomerImplementationText|";

    // correct integer NULL values
    if (isset ($_POST['CustomerCardId'])                 and $_POST['CustomerCardId'] == '')                 $_POST['CustomerCardId']                = 0;
    if (isset ($_POST['CustomerDPBO'])                   and $_POST['CustomerDPBO'] == '')                   $_POST['CustomerDPBO']                  = 0;
    if (isset ($_POST['CustomerNoPing'])                 and $_POST['CustomerNoPing'] == '')                 $_POST['CustomerNoPing']                = 0;
    if (isset ($_POST['CustomerNewClientidPermission'])  and $_POST['CustomerNewClientidPermission'] == '')  $_POST['CustomerNewClientidPermission'] = 0;
    if (isset ($_POST['CustomerPurtelDataRecordId'])     and $_POST['CustomerPurtelDataRecordId'] == '')     $_POST['CustomerPurtelDataRecordId']    = 0;
    if (isset ($_POST['CustomerService'])                and $_POST['CustomerService'] == '')                $_POST['CustomerService']               = 0;
    if (isset ($_POST['CustomerSpectrumprofile'])        and $_POST['CustomerSpectrumprofile'] == '')        $_POST['CustomerSpectrumprofile']       = 0;
    if (isset ($_POST['CustomerIpRangeId'])              and $_POST['CustomerIpRangeId'] == '')              unset($_POST['CustomerIpRangeId']);
    if (isset ($_POST['CustomerLsaKvzPartitionId'])      and $_POST['CustomerLsaKvzPartitionId'] == '')      unset($_POST['CustomerLsaKvzPartitionId']);
    if (isset ($_POST['CustomerNetworkId'])              and $_POST['CustomerNetworkId'] == '') {
        $_POST['CustomerNetworkId']         = 0;
        $_POST['CustomerIpRangeId']         = 0;
        $_POST['CustomerLocationId']        = 0;
        $_POST['CustomerLsaKvzPartitionId'] = 0;
        $_POST['CustomerIpAddress']         = '';
    }
    if (isset ($_POST['CustomerLocationId'])             and $_POST['CustomerLocationId'] == '') {
        $_POST['CustomerLocationId']        = 0;
        $_POST['CustomerLsaKvzPartitionId'] = 0;
    }

    if (!isset($_POST['CustomerPassword']) || $_POST['CustomerPassword'] == '') {
        // only allow to set PPPoE-password when customer gets created first time.
        if (empty($request->get('CustomerId'))) {
            $_POST['CustomerPassword'] = PppoePasswordGenerator::generatePassword();

            if (!isset($_POST['CustomerRemotePassword']) || $_POST['CustomerRemotePassword'] == '') {
                $_POST['CustomerRemotePassword'] = generateRemotePassword($_POST['CustomerClientid'], $_POST['CustomerPassword']);
                $_POST['changed_fields'] .= "CustomerRemotePassword|";
            }

            $_POST['changed_fields'] .= "CustomerPassword|";
        }
    }

    // set connection zipcode
    if (isset($_POST['CustomerDistrict']) && !empty($_POST['CustomerDistrict'])) {
        $_POST['CustomerConnectionZipcode'] = $connectionZipcode[$_POST['CustomerDistrict']];
        $_POST['changed_fields'] .= "CustomerConnectionZipcode|";
    }
    
    $changed_fields = substr($_POST['changed_fields'], 0, -1);
    $changed_field_array = array_unique (explode ('|', $changed_fields));
    $var_set = '';
    $opt_change = array();;
    $customer_saved = '';

    $canNotBeNullColumns = [
        'card_id' => 0,
        'cancel_purtel_for_date' => '',
        'stati_port_confirm_date' => '',
        'wisocontract_cancel_date' => '',
        'wisocontract_switchoff_finish' => '',
        'wisocontract_canceled_date' => '',
        'telefonbuch_eintrag' => '',
        'purtel_edit_done' => '',
        'credit_rating_date' => '',
        'dslam_arranged_date' => '',
        'customer_connect_info_date' => '',
        'customer_connect_info_from' => '',
        'customer_connect_info_how' => '',
        'exp_date_int' => '',
        'exp_date_phone' => '',
        'final_purtelproduct_date' => '',
        'gf_branch' => '',
        'ip_range_id' => 0,
        'kvz_addition' => '',
        'location_id' => 0,
        'lsa_kvz_partition_id' => 0,
        'nb_canceled_date' => '',
        'new_clientid_permission' => 0,
        'new_number_date' => '',
        'oldcontract_city' => '',
        'oldcontract_street' => '',
        'oldcontract_streetno' => '',
        'oldcontract_zipcode' => '',
        'patch_date' => '',
        'phoneentry_done_date' => '',
        'phoneentry_done_from' => '',
        'pppoe_config_date' => '',
        'prospect_supply_status' => '',
        'purtel_data_record_id' => 0,
        'purtel_edit_done_from' => '',
        'purtelproduct_advanced' => '',
        'reg_answer_date' => '',
        'registration_date' => '',
        'routing_active_date' => '',
        'service_level' => '',
        'sofortdsl' => '',
        'tal_cancel_ack_date' => '',
        'tal_cancel_date' => '',
        'tal_canceled_for_date' => '',
        'tal_order_ack_date' => '',
        'tal_order_date' => '',
        'tal_order_work' => '',
        'tal_orderd_from' => '',
        'tal_ordered_for_date' => '',
        'terminal_ready_from' => '',
        'terminal_sended_date' => '',
        'tv_service' => '',
        'ventelo_confirmation_date' => '',
        'ventelo_port_letter_date' => '',
        'ventelo_port_letter_from' => '',
        'ventelo_port_letter_how' => '',
        'ventelo_port_wish_date' => '',
        'ventelo_purtel_enter_date' => '',
        'wisocontract_cancel_input_date' => '',
        'wisocontract_cancel_input_ack' => '',
        'wisocontract_cancel_account_date' => '',
        'wisocontract_cancel_account_finish' => '',
        'wisocontract_cancel_ack' => '',
        'wisocontract_canceled_from' => '',
        'wisocontract_move' => '',
        'get_hardware_date' => '',
        'pay_hardware_date' => '',
        'wisocontract_switchoff' => '',
        'exp_done_phone' => '',
        'exp_done_int' => '',
        'tech_free_for_date' => '',
        'tech_free_date' => '',
        'cancel_purtel_date' => '',
        'cancel_tel_date' => '',
        'cancel_tel_for_date' => '',
        'vectoring' => '',
    ];

    foreach ($changed_field_array as $key) {
        if (strpos($key, 'CustomerOption') === 0) {
            list($db_key, $prod_opt) = explode ('_', $key);
            if ($db_key == 'CustomerOptionActivationDate' or $db_key == 'CustomerOptionDeactivationDate') {
                $_POST[$key] = $_POST[$key] ? date('Y-m-d', strtotime($_POST[$key])) : null;
           }
            $opt_change[$prod_opt][$save_option_fields[$db_key]] = $db->real_escape_string($_POST[$key]);
            if ($db_key == 'CustomerOptionPriceNet') {
                $db_key = 'CustomerOptionPriceGross';
                $key = $db_key.'_'.$prod_opt;
                $opt_change[$prod_opt][$save_option_fields[$db_key]] = $db->real_escape_string($_POST[$key]);
            } elseif ($db_key == 'CustomerOptionPriceGross') {
                $db_key = 'CustomerOptionPriceNet';
                $key = $db_key.'_'.$prod_opt;
                $opt_change[$prod_opt][$save_option_fields[$db_key]] = $db->real_escape_string($_POST[$key]);
            }
        } elseif (isset ($_POST[$key]) && substr($key,0,6) != 'Purtel') {
            if (!isset($save_fields[$key])) {
                continue;
            }

            if (isset($canNotBeNullColumns[$save_fields[$key]])) {
                if (null === $_POST[$key] || '' === $_POST[$key]) {
                    $_POST[$key] = $canNotBeNullColumns[$save_fields[$key]];
                }

                unset($canNotBeNullColumns[$save_fields[$key]]);
            }

            $var_set .= $save_fields[$key]." = '".$db->real_escape_string($_POST[$key])."',\n";
            switch ($key) {
                case 'CustomerNetworkId':
                    $var_set .= "new_clientid_permission = '1',\n";
                    $var_set .= "location_id = '0',\n";
                    $var_set .= "lsa_kvz_partition_id = '0',\n";
                    $var_set .= "lsa_pin = '',\n";
                    $var_set .= "ip_range_id = '0',\n";
                    $var_set .= "ip_address = '',\n";
                    $var_set .= "subnetmask = '',\n";
                    $var_set .= "card_id = '0',\n";
                    $var_set .= "dslam_port = '',\n";
                    $var_set .= "line_identifier = '',\n";
                    $var_set .= "kvz_toggle_no = '',\n";
                    $var_set .= "vlan_ID = '',\n";
                    $var_set .= "DPBO = '0',\n";
                    $var_set .= "no_ping = '0',\n";
                    $var_set .= "Kommando_query = '',\n";
                    unset($canNotBeNullColumns['new_clientid_permission']);
                    unset($canNotBeNullColumns['location_id']);
                    unset($canNotBeNullColumns['lsa_kvz_partition_id']);
                    unset($canNotBeNullColumns['lsa_pin']);
                    unset($canNotBeNullColumns['ip_range_id']);
                    unset($canNotBeNullColumns['ip_address']);
                    unset($canNotBeNullColumns['subnetmask']);
                    unset($canNotBeNullColumns['card_id']);
                    unset($canNotBeNullColumns['dslam_port']);
                    unset($canNotBeNullColumns['line_identifier']);
                    unset($canNotBeNullColumns['kvz_toggle_no']);
                    unset($canNotBeNullColumns['vlan_ID']);
                    unset($canNotBeNullColumns['DPBO']);
                    unset($canNotBeNullColumns['no_ping']);
                    unset($canNotBeNullColumns['Kommando_query']);
                    break;

                case 'CustomerLocationId':
                    $var_set .= "card_id = '0',\n";
                    $var_set .= "dslam_port = '',\n";
                    $var_set .= "line_identifier = '',\n";
                    // spectrum profile
                    break;

                case 'CustomerLsaPin':
                    /** 
                     * @todo remove usage of $_POST['kvz_prefix'] and use the customers lsa_kvz_partition-values instead
                     */
                    $var_set .= "kvz_toggle_no = '".KvzSchaltnummerGenerator::generate($_POST[$key], $_POST['kvz_prefix'])."',\n";
                    unset($canNotBeNullColumns['kvz_toggle_no']);
                    break;

                case 'CustomerCardId':
                    $lineIdentifier = '';
                    
                    if (is_numeric($_POST['CustomerCardId']) && $_POST['CustomerCardId'] > 0) {
                        $card = $this->getDoctrine()->getRepository(Card::class)->findOneById($_POST['CustomerCardId']);

                        if ($card instanceof Card) {
                            $lineIdentifier = LineIdentifierGenerator::generate($card, $_POST['CustomerDslamPort']);   
                        }
                    }

                    $var_set .= "line_identifier = '".$lineIdentifier."',\n";
                    $var_set .= "dslam_port = '',\n";
                    unset($canNotBeNullColumns['line_identifier']);
                    // alte karte hat den gleichen spectrum profile wert, dann lassen, sonst leeren
                    break;

                case 'CustomerDslamPort':
                    /** 
                     * @todo remove usage of $_POST['card_line'] and use the customers card instead
                     */
                    $var_set .= "line_identifier = '".LineIdentifierGenerator::generateByStrings($_POST['card_line'], $_POST['CustomerDslamPort'])."',\n";
                    unset($canNotBeNullColumns['line_identifier']);
                    // hat kartenport.connectionType einen anderen spectrum profile wert, dann leeren, sonst lassen
                    break;

                case 'CustomerIpRangeId':
                    $var_set .= "subnetmask = '".$_POST['ip_subnetmask']."',\n";
                    $var_set .= "DPBO = '".$_POST['ip_DPBO']."',\n";
                    $var_set .= "vlan_ID = '".$_POST['ip_vlanid']."',\n";
                    $var_set .= "reached_downstream = '".$_POST['ip_vlanid']."',\n";
                    unset($canNotBeNullColumns['subnetmask']);
                    unset($canNotBeNullColumns['DPBO']);
                    unset($canNotBeNullColumns['vlan_ID']);
                    unset($canNotBeNullColumns['reached_downstream']);
                    break;

                case 'CustomerProductname':
                    $var_set .= "Service = '".$_POST['CustomerProductname']."',\n";
                    if (strpos($_POST['CustomerProductname'], 'BGE')) {
                        $var_set .= "service_level = 'GK.EMQ',\n";
                    } elseif (strpos($_POST['CustomerProductname'], 'BGS')) {
                        $var_set .= "service_level = 'GK.SMQ',\n";
                    } elseif (strpos($_POST['CustomerProductname'], 'BGA')) {
                        $var_set .= "service_level = 'GK.GKQ',\n";
                    } elseif ($_POST['CustomerClienttype'] == 'commercial') { 
                        $var_set .= "service_level = 'GK.PKQ',\n";
                    } else {
                        $var_set .= "service_level = 'PK.PKQ',\n";
                    }
                    unset($canNotBeNullColumns['Service']);
                    unset($canNotBeNullColumns['service_level']);
                    break;
            }
        }
    }

    // modify product options
    if (isset($_POST['CustomerOptions'])) {
        foreach ($_POST['CustomerOptions'] as $temp) {
            $sql_add = "INSERT INTO customer_options SET cust_id = '".$_POST['CustomerId']."', opt_id = '".$temp."'";
            $db_save_result = $db->query($sql_add);
        }
    }
    
    foreach ($opt_change as $opt_id => $temp) {
        $opt_set = '';
        $opt_delete = false;
        foreach ($temp as $key => $value) {
            if ($value) {
               $opt_set  .= $key." = '".$value."',\n"; 
            } else {
               $opt_set  .= $key." = NULL,\n"; 
            }
            
            if ($key == 'delete') $opt_delete = true; 
        }
        $opt_set = substr($opt_set, 0, -2);
        if ($opt_delete) {
            $sql_delete = "DELETE FROM customer_options  WHERE cust_id = '".$_POST['CustomerId']."' AND opt_id = '".$opt_id."'";
            $db_save_result = $db->query($sql_delete);
        } else {
            $sql_update = "UPDATE customer_options  SET ".$opt_set." WHERE cust_id = '".$_POST['CustomerId']."' AND opt_id = '".$opt_id."'";
            $db_save_result = $db->query($sql_update);
        }
    }
    /*************
     * tal_letter_to_operator = '{$_POST['CustomerTalLetterToOperator']}',
     * tal_letter_to_operator_from = '{$_POST['CustomerTalLetterToOperatorFrom']}',
     ************/

    if ($var_set) {
        $var_set = substr($var_set, 0, -2);
        if ($_POST['CustomerId']) {
            $sql_save ="UPDATE customers SET ".$var_set." WHERE id ='".$_POST['CustomerId']."'";
            $db_save_result = $db->query($sql_save);
            if ($db_save_result) {
                $customer_saved = '&cust_saved=saved';
            } else {
                $logger->error('customer was not saved', ['mysql_error' => $db->error]);

                $customer_saved = '&cust_saved=not_saved';
            }
        } else {
            // fetch clientid for this new prospective customer
            $interest_clientid  = '00000%';

            $db_max_clientid = $db->prepare($sql['max_clientid']);
            $db_max_clientid->bind_param('s', $interest_clientid);
            $db_max_clientid->execute();
            $db_max_clientid_result = $db_max_clientid->get_result();
        
            $max_clientid = $db_max_clientid_result->fetch_row();

            $db_max_clientid_result->free_result();
            $db_max_clientid->close();

            $new_clientid = '0001';

            if (null !== $max_clientid[0]) {
                $max_clientid_arr = explode('.', $max_clientid[0]);
                $new_clientid = $max_clientid_arr[2] + 1;
            }

            $new_clientid = "00000.".date('y').".".sprintf ("%04d", $new_clientid);

            // to ensure all columns that can not be null get a default value
            if (!empty($canNotBeNullColumns)) {
                $var_set .= ', ';

                foreach ($canNotBeNullColumns as $key => $value) {
                    $var_set .= $key." = '".$value."', ";
                }

                $var_set = substr($var_set, 0, -2);
            }

            $db_save_result = $db->query("INSERT INTO customers SET ".$var_set.", clientid = '".$new_clientid."'");
            
            if ($db_save_result) {
                $customer_saved = '&cust_saved=saved';
            } else {
                $logger->error('customer was not saved', ['mysql_error' => $db->error]);

                $customer_saved = '&cust_saved=not_saved';
            }

            $_POST['CustomerId'] = $db->insert_id;
        }
    }

    // Customer History edit event
    if (isset($_POST['edit-history-item-id'])) {
        if (null !== $historyEventList->getEventById($_POST['edit-history-item-event'])) {
            $query = $db->prepare($sql['update_history']);
            $query->bind_param('isi', $_POST['edit-history-item-event'], $_POST['edit-history-item-text'], $_POST['edit-history-item-id']);
            $query->execute();
        } else {
            $request->getSession()->getFlashBag()->add('error', 'Der Verlaufseintrag konnte nicht gespeichert werden.');
        }
    }

    // Customers History add event
    if (isset($_POST['customer_history'])) {
        try {
            // due to the fact that the 'customer_history'-fields will always be send
            // we can only check for the other if (at least) one is not empty

            $requiredFields = array(
                'event' => null,
                'note' => null,
            );

            $oneIsSet = false;
            $oneIsNotSet = null;

            foreach ($requiredFields as $fieldName => $null) {
                if (!isset($_POST['customer_history'][$fieldName]) || empty($_POST['customer_history'][$fieldName])) {
                    $oneIsNotSet = $fieldName;

                    continue;
                }

                if (isset($_POST['customer_history'][$fieldName]) && !empty($_POST['customer_history'][$fieldName])) {
                    $oneIsSet = true;
                }
            }

            if ($oneIsSet && null !== $oneIsNotSet) {
                throw new InvalidArgumentException('Missing field ('.$oneIsNotSet.')');
            }

            $event = $historyEventList->getEventById((int) $_POST['customer_history']['event']);

            if (null == $event) { // confirm genuineness of posted event (id)
                throw new InvalidArgumentException('');
            }

            $customer_addSomethingToThisNameToMakeItUnique->addHistory(
                History::newInstance()
                    ->setCreatedBy($this->get('security.token_storage')->getToken()->getUser()->getUsername())
                    ->setCreatedAt(new DateTime('now'))
                    ->setNote($_POST['customer_history']['note'])
                    ->setEvent($event)
            );
        } catch (InvalidArgumentException $e) {
            if (false !== strpos($e->getMessage(), 'Missing field')) {
                $request->getSession()->getFlashBag()->add('error', sprintf('Es wurden nicht alle Felder ausgefüllt.<br />%s',
                    $e->getMessage()
                ));

                if (isset($_POST['customer_history']['note']) && !empty($_POST['customer_history']['note'])) {
                    $request->getSession()->getFlashBag()->set('customer.createHistory.missingFields', $_POST['customer_history']['note']);
                }
            }
        }
    }

    // Array of customers additional ipAddresses (additional only!)
    $customerIpAddresses = [];

    // Loop through posted ipAddresses
    if (isset($_POST['customer-ip-address'])) {
        foreach ($_POST['customer-ip-address'] as $key => $value) {
            // Set default values for gateway and reverseDns
            if (empty($_POST['customer-gateway'][$key])) {
                $_POST['customer-gateway'][$key] = null;
            }
            if (empty($_POST['customer-rdns'][$key])) {
                $_POST['customer-rdns'][$key] = null;
            }

            try {
                // Create IpAddress object from posted data
                $customerIpAddresses[] = IpAddress::newInstance()
                    ->setIpAddress($_POST['customer-ip-address'][$key])
                    ->setIpRange(DummyIpRange::newInstance()->setId(
                        $_POST['customer-ip-range-id'][$key]
                    ))
                    ->setSubnetmask($_POST['customer-subnetmask'][$key])
                    ->setGateway($_POST['customer-gateway'][$key])
                    ->setReverseDns($_POST['customer-rdns'][$key])
                ;
            } catch (InvalidArgumentException $e) {
                // If one of the posted values was invalid, 
                // this exception will be thrown and no 
                // IpAddress object will be created.
                // 
                // @todo 
                //    Show an error to user.
            }
        }
    }

    $customerIpAddresses = new IpAddressCollection($customerIpAddresses);

    // Array of customers additional terminals (additional only!)
    $customerTerminals = new \ArrayIterator();

    // Loop through posted terminals
    if (isset($_POST['customer-terminal-type'])) {
        foreach ($_POST['customer-terminal-type'] as $key => $value) {
            // Set default values
            $possibleDefaults = [
                'customer-terminal-ip',
                'customer-terminal-type',
                'customer-terminal-macaddress',
                'customer-terminal-serialnumber',
                'customer-terminal-remote-user',
                'customer-terminal-remote-password',
                'customer-terminal-remote-port',
                'customer-terminal-activation-date',
                'customer-terminal-tv-id',
                'customer-terminal-check-id',
            ];

            // For possible defaults: set empty strings to null
            foreach ($possibleDefaults as $postName) {
                if (empty($_POST[$postName][$key])) {
                    $_POST[$postName][$key] = null;
                }
            }

            $terminalIpAddress = $_POST['customer-terminal-ip'][$key];

            if (null !== $terminalIpAddress) {
                $terminalIpAddress = $customerIpAddresses->findByAddress($terminalIpAddress);

                if (null == $terminalIpAddress) { // Ip wasn't within additional ipAddresses
                    // therefore it must be the former ip_address
                    $terminalIpAddress = IpAddress::newInstance()->setIpAddress($_POST['CustomerIpAddress']);
                }
            }

            $customerTerminals->append(Terminal::newInstance()
                ->setRemoteUser($_POST['customer-terminal-remote-user'][$key])
                ->setRemotePassword($_POST['customer-terminal-remote-password'][$key])
                ->setRemotePort($_POST['customer-terminal-remote-port'][$key])
                ->setType($_POST['customer-terminal-type'][$key])
                ->setMacAddress($_POST['customer-terminal-macaddress'][$key])
                ->setSerialnumber($_POST['customer-terminal-serialnumber'][$key])
                ->setIpAddress($terminalIpAddress)
                ->setTvId($_POST['customer-terminal-tv-id'][$key])
                ->setCheckId($_POST['customer-terminal-check-id'][$key])
                ->setActivationDate($_POST['customer-terminal-activation-date'][$key])
            );
        }
    }

    $em = $doctrine->getManager();

    // customer technical data
    $customerTechnicalData = $doctrine->getRepository(\AppBundle\Entity\Customer\TechnicalData::class)
        ->findOneByCustomerId($customer_addSomethingToThisNameToMakeItUnique->getId());

    if (null === $customerTechnicalData) {
        $customerTechnicalData = new \AppBundle\Entity\Customer\TechnicalData();
        $customerTechnicalData->setCustomerId($customer_addSomethingToThisNameToMakeItUnique->getId());
    }

    if (!empty($request->get('technical_data_spectrum_profile_vdsl'))) {
        $customersVdslSpectrumProfileId = (int) $request->get('technical_data_spectrum_profile_vdsl');

        $spectrumProfile = $doctrine->getRepository(\AppBundle\Entity\SpectrumProfile::class)->findOneById($customersVdslSpectrumProfileId);

        if (!$customerTechnicalData->hasSpectrumProfile($spectrumProfile)) {
            // if customer has other spectrumprofiles for vdsl, they have to be removed
            $vdslCardType = $doctrine->getRepository(\AppBundle\Entity\Location\CardType::class)->findOneByName('VDSL');
            $customersVdslSpectrumProfiles = $customerTechnicalData->getAllSpectrumProfilesByCardType($vdslCardType);

            if (!empty($customersVdslSpectrumProfiles)) {
                foreach ($customersVdslSpectrumProfiles as $customersVdslSpectrumProfile) {
                    $customerTechnicalData->removeSpectrumProfile($customersVdslSpectrumProfile);
                }
            }

            $customerTechnicalData->addSpectrumProfile($spectrumProfile);

            $em->persist($customerTechnicalData);
        }
    } else {
        // has spectrum profiles for vdsl?
        $vdslCardType = $doctrine->getRepository(\AppBundle\Entity\Location\CardType::class)->findOneByName('VDSL');
        $customersVdslSpectrumProfiles = $customerTechnicalData->getAllSpectrumProfilesByCardType($vdslCardType);

        if (!empty($customersVdslSpectrumProfiles)) {
            foreach ($customersVdslSpectrumProfiles as $customersVdslSpectrumProfile) {
                $customerTechnicalData->removeSpectrumProfile($customersVdslSpectrumProfile);
            }

            $em->persist($customerTechnicalData);
        }
    }

    if (null !== $request->request->get('technical_data_rf_overlay_signal_strength')) {
        $rfOverlaySignalStrength = $request->request->get('technical_data_rf_overlay_signal_strength');

        if ('' === $rfOverlaySignalStrength) {
            $rfOverlaySignalStrength = null;
        }

        $customerTechnicalData->setRfOverlaySignalStrength($rfOverlaySignalStrength);

        $em->persist($customerTechnicalData);
    }
    
    $em->flush();


    try {
        // Set customerId, ipAddresses and terminal to Customer object and save data to database.
        $customer_addSomethingToThisNameToMakeItUnique
            ->setIpAddresses($customerIpAddresses)
            ->setTerminals($customerTerminals)
            ->persist()
        ;
    } catch (Exception $e) {
        // This exception will be thrown if the sql-query failed.
        // It will include the mysql-error as well as the query itself.
        // 
        // @todo
        //    Find a besser way than aborting the script.
        if ('Can not persist customers data without a customerId' !== $e->getMessage()) { // workaround to allow adding of interessenten
            die($e->getMessage());
        }
    }

    #echo "<hr>$sql_save<hr>".$db->error;
    $wkvurl = $StartURL."/index.php?menu=customer&func=vertrag&id=".$_POST['CustomerId'].'&area='.$_POST['area'].$customer_saved;
    //header("Location: $wkvurl");
    throw new \AppBundle\Exception\RedirectException($wkvurl); // throw this exception instead of header()
} elseif (isset($_POST['delete'])) {
    $sql_delete = "DELETE FROM customers WHERE id = ".$_POST['CustomerId'];
    $db_delete_result = $db->query($sql_delete);

    $wkvurl = $StartURL."/index.php";
    //header("Location: $wkvurl");
    throw new \AppBundle\Exception\RedirectException($wkvurl); // throw this exception instead of header()
} elseif (isset($_POST['version'])) {
    $db_customer_version = $db->prepare($sql['max_version']);
    $db_customer_version->bind_param('s', $_POST['CustomerClientid']);
    $db_customer_version->execute();
    $db_customer_version_result = $db_customer_version->get_result();
    $max_version_res = $db_customer_version_result->fetch_row();
    $max_version = $max_version_res[0];
    $max_version++;
    $db_customer_version_result->free_result();
    $db_customer_version->close();

    $db_customer = $db->prepare($sql['customer']);
    $db_customer->bind_param('d', $_POST['CustomerId']);
    $db_customer->execute();
    $db_customer_result = $db_customer->get_result();
    $customer = $db_customer_result->fetch_assoc();
    $db_customer_result->close();
    
    unset ($customer['id']);
    $customer['version'] = $max_version;

    $var_set = '';
    foreach ($customer AS $key => $value) {
        if (isset($value)) {
            $value = str_replace ("'", "\'", $value);
            $var_set .= "$key = '$value',\n";
        }
    }
    if ($var_set) {
        $var_set = substr($var_set, 0, -2);
        $sql_save ="INSERT INTO customers SET ".$var_set;
        $db_save_result = $db->query($sql_save);

        if ($db_save_result) {
            $customerId = $db->insert_id;

            $query = $db->prepare("SELECT * FROM `customer_history` WHERE `customer_id` = ?");
            
            $query->bind_param('d', $_POST['CustomerId']);
            $query->execute();

            $result = $query->get_result();

            if ($result->num_rows > 0) {
                $insertQuery = "INSERT INTO `customer_history` VALUES ";
                $insertValues = [];

                while ($customerHistory = $result->fetch_assoc()) {
                    foreach ($customerHistory as $key => $value) {
                        if ('id' === $key || null === $value) {
                            $customerHistory[$key] = "NULL";

                            continue;
                        }

                        // use new customer id
                        if ('customer_id' === $key) {
                            $value = $customerId;
                        }

                        $customerHistory[$key] = "'".$value."'";
                    }

                    $insertValues[] = '('.implode(', ', $customerHistory).')';
                }

                $result->close();

                $insertQuery .= implode(', ', $insertValues);

                $db->query($insertQuery);
            }
        }
    }

    if (isset($customerId)) {
        $request->getSession()->getFlashBag()->add('success', 'Kunde gesichert.');

        $wkvurl = $StartURL."/index.php?menu=customer&func=vertrag&id=".$customerId;
    } else {
        $request->getSession()->getFlashBag()->add('error', 'Kunde konnte nicht gesichert werden.');

        $wkvurl = $StartURL."/index.php";
    }

    throw new \AppBundle\Exception\RedirectException($wkvurl); // throw this exception instead of header()
} elseif (isset($_POST['copy'])) {
    $interest_clientid  = '00000%';
    $fields2Copy = [
        'bank_account_bankname',
        'bank_account_bic',
        'bank_account_bic_new',
        'bank_account_city',
        'bank_account_emailaddress',
        'bank_account_holder_firstname',
        'bank_account_holder_lastname',
        'bank_account_iban',
        'bank_account_number',
        'bank_account_street',
        'bank_account_streetno',
        'bank_account_zipcode',
        'birthday',
        'call_data_record',
        'city',
        'clientid',
        'clienttype',
        'comment',
        'company',
        'connection_address_equal',
        'connection_city',
        'connection_installation',
        'connection_street',
        'connection_streetno',
        'connection_zipcode',
        'contract',
        'contract_acknowledge', 
        'contract_change_to',
        'contract_comment',
        'contract_id',
        'contract_text',
        'contract_version',
        'credit_rating_date',
        'credit_rating_link',
        'credit_rating_ok',
        'cudaid',
        'customer_connect_info_date',
        'customer_connect_info_from',
        'customer_connect_info_how',
        'customer_electrisity',
        'customer_electrisity_enddate',
        'customer_id',
        'direct_debit_allowed',
        'district',
        'emailaddress',
        'fax',
        'final_purtelproduct_date',
        'firstname', 
        'inkasso_bankgebuehren', 
        'inkasso_datum_letzte_mahnung',
        'inkasso_datum_ruecklastschrift',
        'inkasso_grund_ruecklastschrift',
        'inkasso_hauptforderung',
        'inkasso_mahngebuehren', 
        'inkasso_rechnung',
        'lastname',
        'mobilephone',
        'paper_bill',
        'phoneareacode',
        'phonenumber',
        'productname', 
        'purtel_product', 
        'purtel_product_id', 
        'purtelproduct_advanced', 
        'recommended_product', 
        'service_level',
        'street', 
        'streetno', 
        'title',
        'wisotelno',
        'zipcode',
        'new_city',
        'new_zipcode',
        'new_street',
        'new_streetno',
        'get_hardware_date',
        'pay_hardware_date',
        'move_registration_ack',
        'add_phone_nos',
        'gf_branch',
        'service_level_send',
        'tv_service',
        'network_id',
        'location_id',
        'ip_range_id',
    ];
    $db_max_clientid = $db->prepare($sql['max_clientid']);
    $db_max_clientid->bind_param('s', $interest_clientid);
    $db_max_clientid->execute();
    $db_max_clientid_result = $db_max_clientid->get_result();

    $max_clientid = $db_max_clientid_result->fetch_row();
    $db_max_clientid_result->free_result();
    $db_max_clientid->close();
    $max_clientid_arr = explode('.', $max_clientid[0]);
    $new_clientid = $max_clientid_arr[2] + 1;
    $new_clientid = "00000.".date('y').".".sprintf ("%04d", $new_clientid);

    $db_customer = $db->prepare($sql['customer']);
    $db_customer->bind_param('d', $_POST['CustomerId']);
    $db_customer->execute();
    $db_customer_result = $db_customer->get_result();

    if (1 !== $db_customer_result->num_rows) {
        throw new NotFoundHttpException('Customer not found');
    }

    $customer = $db_customer_result->fetch_assoc();
    $db_customer_result->close();
    
    unset ($customer['id']);
    $customer['clientid'] = $new_clientid;

    $var_set = '';
    foreach ($customer AS $key => $value) {
        if (isset($value) and in_array($key, $fields2Copy)) {
            $value = str_replace ("'", "\'", $value);
            $var_set .= "$key = '$value',\n";
        }
    }
    if ($var_set) {
        $var_set = substr($var_set, 0, -2);
        $sql_save ="INSERT INTO customers SET ".$var_set;
        $db_save_result = $db->query($sql_save);
        $customerId = $db->insert_id;
    }

    //header("Location: ".$StartURL."/index.php?menu=customer&func=vertrag&id=".$customerId);
    throw new \AppBundle\Exception\RedirectException($StartURL."/index.php?menu=customer&func=vertrag&id=".$customerId); // throw this exception instead of header()
} else {
    $dom_id = "Customer"; 
    if (isset($_GET['id'])) {
        $this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_SEARCH');

        // Get Customer
        $db_customer = $db->prepare($sql['customer']);
        $db_customer->bind_param('d', $_GET['id']);
        $db_customer->execute();
        $db_customer_result = $db_customer->get_result();

        if ($db_customer_result->num_rows !== 1) {
            throw new NotFoundHttpException('Customer not found');
        }

        $customer = $db_customer_result->fetch_assoc();
        $db_customer_result->close();

        // load product-object for productname
        $customer['productname'] = $this->getDoctrine()->getRepository('AppBundle\Entity\Product\MainProduct')->
            findOneByIdentifier($customer['productname']);

        if (!($customer['productname'] instanceof \AppBundle\Entity\Product\MainProduct)) {
            $customer['productname'] = new \AppBundle\Entity\Product\MainProduct();
        }

        // load product-object for purtel_product
        $customer['purtel_product'] = $this->getDoctrine()->getRepository('AppBundle\Entity\Product\MainProduct')->
            findOneByIdentifier($customer['purtel_product']);

        if (!($customer['purtel_product'] instanceof \AppBundle\Entity\Product\MainProduct)) {
            $customer['purtel_product'] = new \AppBundle\Entity\Product\MainProduct();
        }

        // load product-object for Service
        $customer['Service'] = $this->getDoctrine()->getRepository('AppBundle\Entity\Product\MainProduct')->
            findOneByIdentifier($customer['Service']);

        if (!($customer['Service'] instanceof \AppBundle\Entity\Product\MainProduct)) {
            $customer['Service'] = new \AppBundle\Entity\Product\MainProduct();
        }

        // load card-object for card_id
        $customer['card_id'] = $this->getDoctrine()->getRepository('AppBundle\Entity\Location\Card')->
            findOneById($customer['card_id']);

        if (!($customer['card_id'] instanceof \AppBundle\Entity\Location\Card)) {
            $customer['card_id'] = new \AppBundle\Entity\Location\Card();
        }

        // load card-port-object for dslam_port
        $customer['dslam_port'] = $this->getDoctrine()->getRepository('AppBundle\Entity\Location\CardPort')->
            findOneByCardAndNumber($customer['card_id'], $customer['dslam_port']);

        if (!($customer['dslam_port'] instanceof \AppBundle\Entity\Location\CardPort)) {
            $customer['dslam_port'] = new \AppBundle\Entity\Location\CardPort();
        }

        // load customer vlan profiles
        $customer['vlanProfiles'] = $doctrine->getRepository(CustomerVlanProfile::class)->findByCustomerId($customer['id']);

        // customer technical data
        $customerTechnicalData = $doctrine->getRepository(\AppBundle\Entity\Customer\TechnicalData::class)->findOneByCustomerId($customer['id']);

        if (null === $customerTechnicalData) {
            $customerTechnicalData = new \AppBundle\Entity\Customer\TechnicalData();
        }

        $customer['technicalData'] = $customerTechnicalData;

        // Purtel Account
        $purtel_option['-n-'] = '';
        //$db_purtel_account = $db->prepare($sql['purtel_account']);
        //$db_purtel_account->bind_param('i', $customer['id']);
        //$db_purtel_account->execute();
        //$db_purtel_account_result = $db_purtel_account->get_result();
        //$purtel_account_db = $db_purtel_account_result->fetch_all(MYSQLI_ASSOC);
        //foreach ($purtel_account_db AS $temp) {
        $purtel_accounts = $this->getDoctrine()->getRepository('AppBundle\Entity\PurtelAccount')
            ->findByCustomerId($customer['id'], ['master' => 'DESC', 'purtelLogin' => 'ASC']);
    
        foreach ($purtel_accounts as $key => $account) {
            $temp = $account->toArray();
            $purtel_account[$temp['id']] = $temp;
            $purtel_option[$temp['id']] = $temp['purtel_login'];

            if ($temp['master']) {
                $purtel_master = $temp['purtel_login'];
                $purtel_masterpw = $temp['purtel_password'];
            }  
        }

        $purtel_option =  array_unique($purtel_option);

        foreach ($option_purtel_admin_choice_no_submit as $key => $value) {    
            $newkey = str_replace ('#kunde#', $purtel_master, $key);
            $option_purtel_admin_choice_no_submit[$newkey] = $option_purtel_admin_choice_no_submit[$key];
            if ($newkey != $key) {
                unset ($option_purtel_admin_choice_no_submit[$key]);
            }
        }

        $db_ip_ranges = $db->prepare($sql['ip_ranges']);
        $db_ip_ranges->bind_param('d', $customer['network_id']);
        $db_ip_ranges->execute();
        $db_ip_ranges_result = $db_ip_ranges->get_result();
        $ip_ranges = $db_ip_ranges_result->fetch_all(MYSQLI_BOTH);
        $db_ip_ranges->close();

        $ip_addresses   = array();
        $used_ip        = array();
        $own_ip         = array ($customer['ip_address']);

        foreach ($ip_ranges as $value) {
            if ($value['id'] == $customer['ip_range_id']) {
                $startint = ip2long ($value['start_address']);
                $endint   = ip2long ($value['end_address']);
                for ($ip = $startint; $ip <= $endint; $ip++) {
                    array_push ($ip_addresses, long2ip($ip));
                }
                $db_ip_addresses = $db->prepare($sql['ip_addresses']);
                $db_ip_addresses->bind_param('dd', $startint, $endint);
                $db_ip_addresses->execute();
                $db_ip_addresses_result = $db_ip_addresses->get_result();
                while ($ip_result = $db_ip_addresses_result->fetch_assoc()) {
                    array_push ($used_ip, $ip_result['ip_address']);
                }
                $db_ip_addresses->close();
                $used_ip = array_diff ($used_ip, $own_ip);
                $ip_addresses = array_diff ($ip_addresses, $used_ip);
                $ip_subnetmask = $value['subnetmask'];
            }
        }

        try {
            // Get prossible ipRanges for this customer based on network_id
            $ipRanges = new IpRangeCollection($db, $sql);
            $ipRanges->fetchByNetworkId($customer['network_id']);
        } catch (InvalidArgumentException $exception) {
            if (false !== strpos($exception->getMessage(), 'Invalid networkId')) {
                // somehow network_id was removed from customer
                // autodelete values that can not exist without network_id
                $result = $db->query("UPDATE `customers` SET 
                    `network_id` = '0', 
                    `location_id` = '0', 
                    `lsa_kvz_partition_id` = '0', 
                    `lsa_pin` = '', 
                    `ip_range_id` = '0', 
                    `ip_address` = NULL, 
                    `subnetmask` = '', 
                    `card_id` = '0', 
                    `dslam_port` = '', 
                    `line_identifier` = ''
                    WHERE `id` = ".$customer['id']);

                if (false == $result || '00000' != $db->errno) {
                    throw new \Exception($db->error, $db->errno);
                }

                $request->getSession()->getFlashBag()->add('warning', sprintf(
                    'Achtung!<br />
                    Das Netz dieses Kunden wurde gelöscht!<br /><br />
                    Folgende Daten wurden automatisch von diesem Kunden gelöscht:<br />
                    Standort-Id: %s<br />
                    LSA-KVz-Id: %s<br />
                    LSA-Pin: %s<br />
                    IP-Block-Id: %s<br />
                    IP-Adresse: %s<br />
                    Subnetmaske: %s<br />
                    DSLAM-Karte-Id: %s<br />
                    Kartenport: %s<br />
                    Line Identifier: %s<br />',
                    $customer['location_id'],
                    $customer['lsa_kvz_partition_id'],
                    $customer['lsa_pin'],
                    $customer['ip_range_id'],
                    $customer['ip_address'],
                    $customer['subnetmask'],
                    $customer['card_id']->getId(),
                    $customer['dslam_port']->getId(),
                    $customer['line_identifier']
                ));

                $wkvurl = $StartURL."/index.php?menu=customer&func=vertrag&id=".$customer['id'];

                throw new \AppBundle\Exception\RedirectException($wkvurl); // throw this exception instead of header()
            }
            
            throw $exception;
        }

        // Get customer's data (currently it is only for ipAddresses)
        $customer_addSomethingToThisNameToMakeItUnique = Customer::fetchInstance($db, $sql, $customer['id'], $ipRanges);

        // Emty IpAddressCollection in case customer wasn't assigned to an ipRange yet
        $availableIpAddresses = new IpAddressCollection();

        if (isset($customer['ip_range_id']) && !empty($customer['ip_range_id'])) {
            // Get ipRange for this customer based on his ip_range_id
            $customerIpRange = $ipRanges->findRangeById($customer['ip_range_id']);

            // Set $ip_subnetmask
            $ip_subnetmask = $customerIpRange->getSubnetmask();

            if (!empty($customer['ip_address'])) {
                // Set customers ipAddresses (the one comming from `customers`)
                try {
                    $customerIpAddresses = IpAddress::newInstance()
                        ->setIpAddress($customer['ip_address'])
                        ->setIpRange($customerIpRange)
                        ->setSubnetmask($customerIpRange->getSubnetmask())
                        ->setGateway($customerIpRange->getGateway())
                    ;
                } catch (InvalidArgumentException $exception) {
                    if (false === stripos($exception->getMessage(), 'ipAddress is not within given ipRange')) {
                        throw $exception;
                    }

                    // Autodelete incorrect ipaddress
                    $db->query("UPDATE `customers` SET `ip_address` = NULL WHERE `id` = ".$customer['id']);

                    $request->getSession()->getFlashBag()->add('warning', sprintf(
                        'Achtung!<br />IP-Adresse und IP-Block stimmen nicht überein!<br />Die IP-Adresse (<u>%s</u>) wurde automatisch von diesem Kunden gelöscht!',
                        $customer['ip_address']
                    ));
                }
            }
        }

        $db_locations = $db->prepare($sql['locations']);
        $db_locations->bind_param('d', $customer['network_id']);
        $db_locations->execute();
        $db_locations_result = $db_locations->get_result();
        $locationsdb = $db_locations_result->fetch_all(MYSQLI_BOTH);
        $db_locations->close();

        $locations = array ('-n-' => '');
        foreach ($locationsdb as $location) {
            $locations[$location['id']] = $location['name'];
        }

        $db_lsa_kvz_partitions = $db->prepare($sql['lsa_kvz_partitions']);
        $db_lsa_kvz_partitions->bind_param('d', $customer['location_id']);
        $db_lsa_kvz_partitions->execute();
        $db_lsa_kvz_partitions_result = $db_lsa_kvz_partitions->get_result();
        $lsa_kvz_partitionsdb = $db_lsa_kvz_partitions_result->fetch_all(MYSQLI_BOTH);
        $db_lsa_kvz_partitions->close();

        $lsa_kvz_partitions = array ('-n-' => '');

        $lsa_pins   = array();
        $used_pins  = array();
        $own_pin    = array ($customer['lsa_pin']);
        foreach ($lsa_kvz_partitionsdb as $value) {
            $lsa_kvz_partitions[$value['id']] = $value['kvz_name'];
            if ($value['id'] == $customer['lsa_kvz_partition_id']) {
                $ip_vlanid  = $value['default_VLAN'];
                $ip_DPBO    = $value['dpbo'];
                for ($i = 1; $i <= $value['pin_amount']; $i++) {
                    array_push ($lsa_pins, $i);
                }
                $db_lsa_pins = $db->prepare($sql['lsa_pins']);
                $db_lsa_pins->bind_param('d', $customer['lsa_kvz_partition_id']);
                $db_lsa_pins->execute();
                $db_lsa_pins_result = $db_lsa_pins->get_result();
                while ($used_pin = $db_lsa_pins_result->fetch_assoc()) {
                    array_push ($used_pins, $used_pin['lsa_pin']);
                }
                $db_lsa_pins->close();
        
                $db_lsa_pins = $db->prepare($sql['defective_lsa_pins']);
                $db_lsa_pins->bind_param('d', $customer['lsa_kvz_partition_id']);
                $db_lsa_pins->execute();
                $db_lsa_pins_result = $db_lsa_pins->get_result();
                while ($used_pin = $db_lsa_pins_result->fetch_assoc()) {
                    array_push ($used_pins, $used_pin['value']);
                }
                $db_lsa_pins->close();

                $used_pins  = array_diff ($used_pins, $own_pin);
                $lsa_pins   = array_diff ($lsa_pins, $used_pins);
            }
        }

        $db_cards = $db->prepare($sql['cards']);
        $db_cards->bind_param('d', $customer['location_id']);
        $db_cards->execute();
        $db_cards_result = $db_cards->get_result();
        $cardsdb = $db_cards_result->fetch_all(MYSQLI_BOTH);
        $db_cards->close();
    
        $cards        = array('' => '');
        $dslam_ports  = array('-n-' => '');
        //$used_ports   = array('-n-' => '');
        //$own_port     = array $customer['dslam_port']);

        $allowedCardType = null;

        // has customer been assigned to a card yet?
        if (!empty($customer['productname']->getId())) {
            // does customers-product has an assoziation to card-typ?
            if ($customer['productname']->getCardType() instanceof \AppBundle\Entity\Location\CardType) {
                $allowedCardType = $customer['productname']->getCardType()->getId();
            }
        }

        foreach ($cardsdb as $value) {
            if (null === $allowedCardType || $allowedCardType == $value['card_type_id']) {
                $cards[$value['id']] = $value['cardname']."(".$value['cardtype'].")";
            }

            /*if ($value['id'] == $customer['card_id']->getId()) {
                for ($i = 1; $i <= $value['port_amount']; $i++) {
                    array_push ($dslam_ports, $i);
                }

                $db_dslam_ports = $db->prepare($sql['dslam_ports']);
                $db_dslam_ports->bind_param('d', $customer['card_id']->getId());
                $db_dslam_ports->execute();
                $db_dslam_ports_result = $db_dslam_ports->get_result();
                while ($used_port = $db_dslam_ports_result->fetch_assoc()) {
                    array_push ($used_ports, $used_port['dslam_port']);
                }
                $db_dslam_ports->close();

                $db_dslam_ports = $db->prepare($sql['defective_card_ports']);
                $db_dslam_ports->bind_param('d', $customer['card_id']->getId());
                $db_dslam_ports->execute();
                $db_dslam_ports_result = $db_dslam_ports->get_result();
                while ($used_port = $db_dslam_ports_result->fetch_assoc()) {
                    array_push ($used_ports, $used_port['value']);
                }
                $db_dslam_ports->close();

                $used_ports = array_diff ($used_ports, $own_port);
                $dslam_ports = array_diff ($dslam_ports, $used_ports);
            }*/
        }

        $dbConnection = $this->getDoctrine()->getManager()->getConnection();

        if (null !== $customer['dslam_port']->getId()) {
            // get ports for a certain card, except the ones that are connected to another card-port or a customer
            // if selectedCardPortId was given, get this values too
            $stmt = $dbConnection->prepare("SELECT cp.`id`, cp.`number`, cp.`color` 
                FROM `card_ports` cp 
                LEFT JOIN `customers` cu ON cu.`card_id` = cp.`card_id`
                WHERE cp.`card_id` = :cardId AND cp.`card_port_id` IS NULL AND (cp.`id` = :selectedCardPortId OR cu.`dslam_port` IS NULL OR cu.`dslam_port` != cp.`number`) ORDER BY cp.`number` ASC");

            $stmt->bindValue(':selectedCardPortId', $customer['dslam_port']->getId());
        } else {
            // get ports for a certain card, except the ones that are connected to another card-port or a customer
            $stmt = $dbConnection->prepare("SELECT cp.`id`, cp.`number`, cp.`color` 
                FROM `card_ports` cp 
                LEFT JOIN `customers` cu ON cu.`card_id` = cp.`card_id`
                WHERE cp.`card_id` = :cardId AND cp.`card_port_id` IS NULL AND (cu.`dslam_port` IS NULL OR cu.`dslam_port` = '' OR cu.`dslam_port` != cp.`number`) ORDER BY cp.`number` ASC");
        }

        $stmt->bindValue(':cardId', $customer['card_id']->getId());
        $stmt->execute();
        $cardPorts = $stmt->fetchAll();

        $cardPortIds = array_column($cardPorts, 'number');
        $cardPorts = array_combine($cardPortIds, $cardPorts); // set number as array key
        unset($cardPortIds);

        $db_dslam_ports = $db->prepare($sql['defective_card_ports']);
        $customerCardId = $customer['card_id']->getId(); // to avoid Only variables should be passed by reference
        $db_dslam_ports->bind_param('d', $customerCardId);
        $db_dslam_ports->execute();
        $db_dslam_ports_result = $db_dslam_ports->get_result();

        while ($used_port = $db_dslam_ports_result->fetch_assoc()) {
            unset($cardPorts[$used_port['value']]);
        }

        $db_dslam_ports->close();

        //  Berechnet Kündigungstermin
        $contract_next_cancle_date  = '';
        $one_year                   = 24*60*60*365;
        $two_years                  = 2 * $one_year;
        $temp_date                  = time();
        $conn_act_date_int          = strtotime($customer['connection_activation_date']);
    
        if ($conn_act_date_int) {
            $temp_contract  = $temp_date - $conn_act_date_int;
        
            if ($temp_contract < $two_years) {
                $contract_next_cancle_int = strtotime('+2 years -1 day', $conn_act_date_int);
        
                if ($contract_next_cancle_int < strtotime ('+3 month', $temp_date)) {
                    $contract_next_cancle_int = strtotime ('+1 year', $contract_next_cancle_int);
                }
        
                $contract_next_cancle_date = date('d.m.Y', $contract_next_cancle_int);
            } else {
                $contract_next_cancle_int = strtotime('+1 year  -1 day', strtotime(date('d.m.',$conn_act_date_int).date('Y', $temp_date)));

                if ('p' === strtolower(substr($customer['productname']->getIdentifier(), 2, 1))) {
                    // non_commercial
                    if ($contract_next_cancle_int < strtotime ('+1 month', $temp_date)) {
                        $contract_next_cancle_int = strtotime ('+1 year', $contract_next_cancle_int);
                    }
                } else {
                    // commercial
                    if ($contract_next_cancle_int < strtotime ('+3 month', $temp_date)) {
                        $contract_next_cancle_int = strtotime ('+1 year', $contract_next_cancle_int);
                    }
                }
        
                $contract_next_cancle_date  =  date('d.m.Y', $contract_next_cancle_int);
            }
        }

        // Berechnet Sonderkündigungstermin
        $contract_extra_cancel_date     = '';
        $wisocontract_cancel_input_int  = strtotime($customer['wisocontract_cancel_input_date']);
        $wisocontract_cancel_int        = strtotime($customer['wisocontract_cancel_date']);
        if ($wisocontract_cancel_input_int && $customer['wisocontract_cancel_extra'] == 'yes') {
            $contract_extra_cancel_int = strtotime('+3 month', $wisocontract_cancel_input_int);
            $contract_extra_cancel_date = date('d.m.Y', $contract_extra_cancel_int);  
        }
        
        $abrogate_red = '';
        if (!$customer['wisocontract_canceled_date']) {
            if ($wisocontract_cancel_int && $wisocontract_cancel_int < $contract_next_cancle_int) {
                $abrogate_red = 'red';
            }
            if ($customer['wisocontract_cancel_extra'] == 'yes' &&  $wisocontract_cancel_int >= $contract_extra_cancel_int) {
                $abrogate_red = '';
            }
        } 

        $headline = "ID ".$customer['clientid'];
        if ($customer['version']) {
            $headline .= " <span class='text red'>[Version: ".$customer['version']."]</span> ";
        }
        if ($customer['company'] != '') {
            $headline .= " ".$customer['company'];
        }
        $headline .= " - ".$customer['lastname'].", ".$customer['firstname']." (".$option_clienttype_short[$customer['clienttype']].")";
        if (isset($_GET['cust_saved'])) {
            if ($_GET['cust_saved'] == 'saved') {
                $headline .= '<br /><span class="green">(Kundendaten wurden gesichert)</span>';
            } else {
                $headline .= '<br /><span class="red">(Kundendaten konnten nicht gesichert werden)</span>';
            }
        }
    } else {
        $interest_clientid  = '00000%';
    
        $db_max_clientid = $db->prepare($sql['max_clientid']);
        $db_max_clientid->bind_param('s', $interest_clientid);
        $db_max_clientid->execute();
        $db_max_clientid_result = $db_max_clientid->get_result();
    
        $max_clientid = $db_max_clientid_result->fetch_row();

        $db_max_clientid_result->free_result();
        $db_max_clientid->close();

        $new_clientid = '0001';

        if (null !== $max_clientid[0]) {
            $max_clientid_arr = explode('.', $max_clientid[0]);
            $new_clientid = $max_clientid_arr[2] + 1;
        }

        $new_clientid = "00000.".date('y').".".sprintf ("%04d", $new_clientid);

        $customer['clientid'] = $new_clientid;
   
        if (isset($_GET['func'])) {
            if ($_GET['func'] == 'vertrag') {
                $headline = 'Neuer Vertrag';
                $dom_id   = "NewCustomer";
            } else {
                $this->denyAccessUnlessGranted('ROLE_VIEW_POTENTIAL_CUSTOMER');

                $headline = 'Neuer Interessent';
                $dom_id   = "Interest";
            }
        } else {
            $this->denyAccessUnlessGranted('ROLE_VIEW_POTENTIAL_CUSTOMER');

            $headline = 'Neuer Interessent';
            $dom_id   = "Interest";
        }
    }

    if (isset($_GET['area'])) {
        $area = $_GET['area']; 
    } else {
        $area = '';
    }

    // Mail Templates
    $db_mailtemp = $db->query( $sql['email_temp']);
    $mail_temp   = $db_mailtemp->fetch_all(MYSQLI_ASSOC);
    $db_mailtemp->close();

    // Mail Group
    $db_mailgroup = $db->query( $sql['email_group']);
    $mail_group   = $db_mailgroup->fetch_all(MYSQLI_ASSOC);
    $db_mailgroup->close();
    
    // Produkte
    $service = null;
    
    if (isset($customer['Service']) && is_object($customer['Service'])) {
        $service = $customer['Service']->getIdentifier();
    }

    $db_products = $db->prepare($sql['produkte']);
    $db_products->bind_param('s', $service);
    $db_products->execute();
    $db_products_result = $db_products->get_result();
    $products = $db_products_result->fetch_all(MYSQLI_ASSOC);
    $db_products->close();

    // Produktgruppen
    $db_prod_group = $db->query($sql['prod_group']);
    $prod_group_db   = $db_prod_group->fetch_all(MYSQLI_ASSOC);
    foreach ($prod_group_db as $temp) $prog_group[$temp['id']] = $temp['groupname'];
    $db_prod_group->close();
    
    /*
    // Produkte
    $product_sel_option = array('-n-' => '');
    $service_sel_option = array('-n-' => '');
    $db_productsel = $db->query($sql['produkte_sel']);
    $product_sel   = $db_productsel->fetch_all(MYSQLI_ASSOC);
    $group_id = 0;
    foreach ($product_sel as $temp) {
        if ($temp['produkt_bezeichnung']) {
            if ($temp['group_id'] != $group_id) {
                $group_id = $temp['group_id'];
                $service_sel_option[sprintf('--%02d', $group_id)] = $prog_group[$group_id];
                $product_sel_option[sprintf('--%02d', $group_id)] = $prog_group[$group_id];
            }
            $service_sel_option[$temp['produkt']]  = $temp['produkt_bezeichnung'];
            $product_sel_option[$temp['produkt']]  = '['.$temp['produkt'].'] '.$temp['produkt_bezeichnung'];
            if ($temp['Preis_Brutto']) {
                $product_sel_option[$temp['produkt']] .= ' (Preis: '.$temp['Preis_Brutto'].'€)';
            }
            $productname[$temp['produkt']]  =  '['.$temp['produkt'].'] '.$temp['produkt_bezeichnung'];
        }
    }
    $db_productsel->close();
    */

    $doctrine = $this->getDoctrine();

    // has customer been assigned to a card yet?
    if (isset($customer['card_id']) && is_object($customer['card_id']) && !empty($customer['card_id']->getId())) {
        $cardTypesByCardId = array_column($cardsdb, 'card_type_id', 'id');
        $cardTypeId = $cardTypesByCardId[$customer['card_id']->getId()];

        // select allowed products for to cardType
        $mainProducts = $doctrine->getRepository('AppBundle\Entity\Product\MainProduct')->findAllowedForCardType($cardTypeId);

        // already has a product?
        if (!empty($customer['productname']->getId())) {
            // does customers-product has an assoziation to card-typ?
            if ($customer['productname']->getCardType() instanceof \AppBundle\Entity\Location\CardType) {
                // is his product allowed in combination with the assigned card?
                if ($cardTypeId !== $customer['productname']->getCardType()->getId()) {
                    $request->getSession()->getFlashBag()->add('customer.error', sprintf(
                        'Kunde hat ein falsches Produkt oder eine falsche Karte.<br />Er hat ein %s-Produkt, '
                        .'jedoch sind bei der eingestellten Karte nur %s-Produkte möglich.',
                        $customer['productname']->getCardType()->getName(),
                        $doctrine->getRepository('AppBundle\Entity\Location\CardType')->findOneById($cardTypeId)->getName()
                    ));

                    // Add current customers product to mainProducts although it's technically not allowed.
                    // If we would't do this here, the currently choosen product would not be shown
                    $mainProducts[] = $customer['productname'];

                    // Add current customers card to cards although it's technically not allowed.
                    // If we would't do this here, the currently choosen card would not be shown
                    $cards[$customer['card_id']->getId()] = sprintf('%s (%s)',
                        $customer['card_id']->getName(),
                        $customer['card_id']->getCardType()->getName()
                    );
                }
            } elseif (!$customer['productname']->getActive()) {
                $mainProducts[] = $customer['productname'];
            }
        }

        // already has a (purtel-)product?
        if (!empty($customer['purtel_product']->getId())) {
            // does customers-(purtel-)product has an assoziation to card-typ?
            if ($customer['purtel_product']->getCardType() instanceof \AppBundle\Entity\Location\CardType) {
                // is his (purtel-)product allowed in combination with the assigned card?
                if ($cardTypeId !== $customer['purtel_product']->getCardType()->getId()) {
                    $request->getSession()->getFlashBag()->add('customer.error', sprintf(
                        'Kunde hat ein falsches Purtel-Produkt oder eine falsche Karte.<br />Er hat ein %s-Produkt, '
                        .'jedoch sind bei der eingestellten Karte nur %s-Produkte möglich.',
                        $customer['purtel_product']->getCardType()->getName(),
                        $doctrine->getRepository('AppBundle\Entity\Location\CardType')->findOneById($cardTypeId)->getName()
                    ));

                    // Add current customers product to mainProducts although it's technically not allowed.
                    // If we would't do this here, the currently choosen product would not be shown
                    
                    // Only add purtel_product to mainProducts if it is not the same as productname
                    // because it it is then it was already added above
                    if ($customer['purtel_product']->getId() != $customer['productname']->getId()) {
                        $mainProducts[] = $customer['purtel_product'];

                        // Add current customers card to cards although it's technically not allowed.
                        // If we would't do this here, the currently choosen card would not be shown
                        $cards[$customer['card_id']->getId()] = sprintf('%s (%s)',
                            $customer['card_id']->getName(),
                            $customer['card_id']->getCardType()->getName()
                        );
                    }
                }
            } elseif (!$customer['purtel_product']->getActive()) {
                // Only add purtel_product to mainProducts if it is not the same as productname
                // because it it is then it was already added above
                if ($customer['purtel_product']->getId() != $customer['productname']->getId()) {
                    $mainProducts[] = $customer['purtel_product'];
                }
            }
        }

        // already has a (service-)product?
        if (!empty($customer['Service']->getId())) {
            // does customers-(purtel-)product has an assoziation to card-typ?
            if ($customer['Service']->getCardType() instanceof \AppBundle\Entity\Location\CardType) {
                // is his (purtel-)product allowed in combination with the assigned card?
                if ($cardTypeId !== $customer['Service']->getCardType()->getId()) {
                    $request->getSession()->getFlashBag()->add('customer.error', sprintf(
                        'Das Service-Produkt des Kunden passt nicht zur Karte.<br />Er hat ein %s-Produkt, '
                        .'jedoch sind bei der eingestellten Karte nur %s-Produkte möglich.',
                        $customer['Service']->getCardType()->getName(),
                        $doctrine->getRepository('AppBundle\Entity\Location\CardType')->findOneById($cardTypeId)->getName()
                    ));

                    // Add current customers product to mainProducts although it's technically not allowed.
                    // If we would't do this here, the currently choosen product would not be shown
                    
                    // Only add Service to mainProducts if it is not the same as productname or purtel_product
                    // because it it is then it was already added above
                    if ($customer['Service']->getId() != $customer['productname']->getId()
                        && $customer['Service']->getId() != $customer['purtel_product']->getId()) {
                        $mainProducts[] = $customer['Service'];

                        // Add current customers card to cards although it's technically not allowed.
                        // If we would't do this here, the currently choosen card would not be shown
                        $cards[$customer['card_id']->getId()] = sprintf('%s (%s)',
                            $customer['card_id']->getName(),
                            $customer['card_id']->getCardType()->getName()
                        );
                    }
                }
            } elseif (!$customer['Service']->getActive()) {
                // Only add Service to mainProducts if it is not the same as productname or purtel_product
                // because it it is then it was already added above
                if ($customer['Service']->getId() != $customer['productname']->getId()
                    && $customer['Service']->getId() != $customer['purtel_product']->getId()) {
                    
                    $mainProducts[] = $customer['Service'];
                }
            }
        }
    } else {
        $mainProducts = $doctrine->getRepository('AppBundle\Entity\Product\MainProduct')->findAll();
    }

    // Produktoptionen
    $options = [];
    $db_options = $db->query( $sql['options']);
    $options_db = $db_options->fetch_all(MYSQLI_ASSOC);
    foreach ($options_db as $temp) {
        $options[$temp['option']] = $temp;
    }
    $db_options->close();

    $db_cust_options = $db->prepare($sql['cust_options']);
    $db_cust_options->bind_param('i', $customer['id']);
    $db_cust_options->execute();
    $db_cust_options_result = $db_cust_options->get_result();
    $cust_options_db = $db_cust_options_result->fetch_all(MYSQLI_ASSOC);
    $cust_options = array();
    foreach ($cust_options_db AS $temp) {
        $cust_options[$temp['opt_id']] = $temp;
        if (!isset($cust_options[$temp['opt_id']]['price_gross'])) $cust_options[$temp['opt_id']]['price_gross'] = $cust_options[$temp['opt_id']]['Preis_Brutto'];
        if (!isset($cust_options[$temp['opt_id']]['price_net'])) $cust_options[$temp['opt_id']]['price_net'] = $cust_options[$temp['opt_id']]['Preis_Netto'];
    }
    $db_cust_options->close();
        
    // WBCI Endkundenvertragspartner
    $wbci_ekpauf_id = null;
    $wbci_ekpauf = null;
    $wbci_ekp_option = array('-n-' => '');
    $db_wbci_ekp = $db->query($sql['wbci_ekp']);
    $wbci_ekp     = $db_wbci_ekp->fetch_all(MYSQLI_ASSOC);
    foreach ($wbci_ekp as $temp) {
        if ($temp['carriercode'] == 'DEU.COMIN') {
            $wbci_ekpauf_id = $temp['id'];
            $wbci_ekpauf    = $temp['carriercode'].' - '.$temp['bezeichnung'];
        }
        $wbci_ekp_option[$temp['id']] = $temp['carriercode'].' - '.$temp['bezeichnung'];
    }
    $db_wbci_ekp->close();
    
    // WBCI Geschäftsfall
    $wbci_gf_option = array('-n-' => '');
    $db_wbci_gf = $db->query($sql['wbci_gf']);
    $wbci_gf     = $db_wbci_gf->fetch_all(MYSQLI_ASSOC);
    foreach ($wbci_gf as $temp) {
        $wbci_gf_option[$temp['gf']] = '['.$temp['gf'].'] '.$temp['gf_beschreibung'];
    }
    $db_wbci_gf->close();
    
    /*
    $area_code    = '';
    $network_name = '';
    foreach ($networks as $value) {
        if (isset ($customer['network_id']) and $customer['network_id'] == $value['id']) {
            $area_code    = $value['area_code'];
            $network_name = ' (Netz: '.$value['name'].')';
        }
    }
    */
    $kvz_prefix = '';
    foreach ($locationsdb as $value) {
        if (isset($customer['location_id']) and $customer['location_id'] == $value['id']) {
            $kvz_prefix = $value['kvz_prefix'];
        }
    }

    $authorizationChecker = $this->get('security.authorization_checker');

    ob_start();

    $IncCustStatus = '';

    if (isset($customer_addSomethingToThisNameToMakeItUnique)) {
        # get status id 01
        require_once ($StartPath.'/customer/status.php');
        $IncCustStatus = ob_get_contents();
        ob_clean();
    }

    $IncCustProcedure = '';

    if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_PROCEDURE') && isset($customer_addSomethingToThisNameToMakeItUnique)) {
        # get procedure id 05
        require_once ($StartPath.'/customer/procedure.php');
        $IncCustProcedure = ob_get_contents();
        ob_clean();
    }

    $IncCustMasterData = '';

    if (isset($customer_addSomethingToThisNameToMakeItUnique)) {
        if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_MASTER_DATA')) {
            # get master_data id 02
            require_once ($StartPath.'/customer/masterData.php');
            $IncCustMasterData = ob_get_contents();
            ob_clean();
        }
    } elseif (isset($_GET['func']) && 'vertrag' === $_GET['func']) {
        # get master_data id 02
        require_once ($StartPath.'/customer/masterData.php');
        $IncCustMasterData = ob_get_contents();
        ob_clean();
    } else {
        if ($authorizationChecker->isGranted('ROLE_VIEW_POTENTIAL_CUSTOMER')) {
            # get master_data id 02
            require_once ($StartPath.'/customer/masterData.php');
            $IncCustMasterData = ob_get_contents();
            ob_clean();
        }
    }

    $IncCustConnection = '';
  
    if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_CONNECTION')) {
        # get connection id 03
        require_once ($StartPath.'/customer/connection.php');
        $IncCustConnection = ob_get_contents();
        ob_clean();
    }

    $IncCustAccounting = '';

    if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_ACCOUNTING') && isset($customer_addSomethingToThisNameToMakeItUnique)) {
        # get accounting id 04
        require_once ($StartPath.'/customer/accounting.php');
        $IncCustAccounting = ob_get_contents();
        ob_clean();
    }

    $IncCustInkasso = '';

    if ($authorizationChecker->isGranted('ROLE_CREATE_DEBT_COLLECTION_ORDER') && isset($customer_addSomethingToThisNameToMakeItUnique)) {
        # get accounting id 16
        require_once ($StartPath.'/customer/inkasso.php');
        $IncCustInkasso = ob_get_contents();
        ob_clean();
    }

    $IncCustContractPrivate = '';
    $IncCustContractBusiness = '';
    $IncCustContractOptions = '';
    $IncCustOldContract = '';

    if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_CONTRACT') && isset($customer_addSomethingToThisNameToMakeItUnique)) {
        # get contract private id 06
        require_once ($StartPath.'/customer/contractPrivate.php');
        $IncCustContractPrivate = ob_get_contents();
        ob_clean();

        # get contract business id 07
        require_once ($StartPath.'/customer/contractBusiness.php');
        $IncCustContractBusiness = ob_get_contents();
        ob_clean();

        # get contract options 16
        require_once ($StartPath.'/customer/contractOptions.php');
        $IncCustContractOptions = ob_get_contents();
        ob_clean();

        # get old contract id 08
        require_once ($StartPath.'/customer/oldContract.php');
        $IncCustOldContract = ob_get_contents();
        ob_clean();
    }

    $IncCustTal = '';

    if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_TAL') && isset($customer_addSomethingToThisNameToMakeItUnique)) {
        # get tal id 09
        require_once ($StartPath.'/customer/tal.php');
        $IncCustTal = ob_get_contents();
        ob_clean();
    }

    $IncCustPurtel = '';

    if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_PURTEL') && isset($customer_addSomethingToThisNameToMakeItUnique)) {
        # get porting id 13
        require_once ($StartPath.'/customer/purtel.php');
        $IncCustPurtel = ob_get_contents();
        ob_clean();
    }

    $IncCustPurtelAccount = '';

    if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_PURTEL_ACCOUNT') && isset($customer_addSomethingToThisNameToMakeItUnique)) {
        # get porting id 14
        require_once ($StartPath.'/customer/purtelAccount.php');
        $IncCustPurtelAccount = ob_get_contents();
        ob_clean();
    }

    $IncCustTechData = '';

    # get tech data id 10
    if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_TECH_DATA') && isset($customer_addSomethingToThisNameToMakeItUnique)) {
        require_once ($StartPath.'/customer/techData.php');
        $IncCustTechData = ob_get_contents();
        ob_clean();
    }

    $IncCustTechConf = '';

    if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_TECH_CONFIGURATION') && isset($customer_addSomethingToThisNameToMakeItUnique)) {
        # get tech configuration
        require_once ($StartPath.'/customer/techConf.php');
        $IncCustTechConf = ob_get_contents();
        ob_clean();
    }

    $IncCustProgress = '';

    if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_PROGRESS') && isset($customer_addSomethingToThisNameToMakeItUnique)) {
        # get procedure id 12
        require_once ($StartPath.'/customer/progress.php');
        $IncCustProgress = ob_get_contents();
        ob_clean();
    }

    $IncCustConnectingRequest = '';

    if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_CONNECTION_REQUEST') && isset($customer_addSomethingToThisNameToMakeItUnique)) {
        # get connection request id 11
        require_once ($StartPath.'/customer/connectingRequest.php');
        $IncCustConnectingRequest = ob_get_contents();
        ob_clean();
    }

    $IncCustAbrogate = '';

    if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_ABROGATE') && isset($customer_addSomethingToThisNameToMakeItUnique)) {
        # get abrogate id 14
        require_once ($StartPath.'/customer/abrogate.php');
        $IncCustAbrogate = ob_get_contents();
        ob_clean();
    }

    $IncCustWbci = '';
    $IncCustVentelo = '';

    if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_PROVIDER_CHANGE') && isset($customer_addSomethingToThisNameToMakeItUnique)) {
        # get abrogate id 15
        require_once ($StartPath.'/customer/wbci.php');
        $IncCustWbci = ob_get_contents();
        ob_clean();

        # get abrogate id 15
        require_once ($StartPath.'/customer/ventelo.php');
        $IncCustVentelo = ob_get_contents();
        ob_clean();
    }

    ob_end_clean();
    $cust_id = $StartURL.'/customer/contractPDF.php?id='.$customer['id'];
?>

<h1><?php echo $headline; ?></h1>

<?php
$userNotificationsColorization = [
    'customer.error' => 'red',
    'customer.warning' => 'orange',
    'customer.success' => 'green',
];

foreach ($userNotificationsColorization as $notificationType => $notificationColorCode) {
    $notificationMessages = $request->getSession()->getFlashBag()->get($notificationType);

    if (!empty($notificationMessages)) {
        foreach ($notificationMessages as $notificationMessage) {
            echo sprintf('<span class="text" style="font-size: 20px; font-weight: bold; color: %s">%s</span><br /><br />',
                $notificationColorCode,
                $notificationMessage
            );
        }
    }
}

$formUrl = $StartURL.'/index.php?menu=customer';

if (!empty($request->query->get('id'))) {
    $formUrl .= '&id='.$request->query->get('id');
}

?>

<form id="custForm" action="<?php echo $formUrl; ?>" method="post" enctype="multipart/form-data">
<div id="<?php echo $dom_id; ?>" class="box">

<?php 
    echo $IncCustProcedure;
    echo $IncCustMasterData;
    echo $IncCustConnection;
    echo $IncCustAccounting;
    echo $IncCustContractPrivate;
    echo $IncCustContractBusiness;
    echo $IncCustContractOptions;
    echo $IncCustOldContract;
    echo $IncCustTal;
    echo $IncCustPurtel;

    if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_VLAN') && isset($customer_addSomethingToThisNameToMakeItUnique)) {
        echo $this->renderView('customer/vlan/overview.html.php', [
            'customer' => \AppBundle\Entity\Customer::createFromArray($customer, $doctrine),
            'globalVlanProfiles' => $doctrine->getRepository(\AppBundle\Entity\VlanProfile::class)->findAll(),
        ]);
    }

    echo $IncCustTechData;
    echo $IncCustTechConf;
    echo $IncCustProgress;
    echo $IncCustConnectingRequest;
    echo $IncCustAbrogate;
    echo $IncCustWbci;
    echo $IncCustVentelo;
    echo $IncCustStatus;
    echo $IncCustInkasso;
?>

    <div id="otrs-ticket" style="display: none; text-align: left"  class="">
        <?php

        $otrsAdapter = $this->get(OtrsAdapter::class);

        echo "<input name='otrs-ajaxcontroller-url' id='otrs-ajaxcontroller-url' type='hidden' value='".$otrsAdapter->getOtrsAjaxControllerUrl()."'>";
        
        echo $this->renderView(
            'otrs-bridge/create-ticket.html.twig',
            ['form' => $this->get('form.factory')->create(OtrsTicketCreateType::class)->createView()]
        );

        ?>
    </div>


    <h3 class="CustSave"></h3>
<div class="CustSave form">
<?php
    echo "<input name='CustomerId' id='CustomerId' type='hidden' value='".$customer['id']."'>";
    echo "<input name='CustomerVersion' id='CustomerVersion' type='hidden' value='".(isset($customer['version']) ? $customer['version'] : null)."'>";
    echo "<input name='CustomerPurtelMaster' id='CustomerPurtelMaster' type='hidden' value='".$purtel_master."'>";
    echo "<input name='area' id='area' type='hidden' value='".$area."'>";
    echo "<input name='area_code' id='area_code' type='hidden' value='".(isset($customer['network_id']) && isset($area_code[$customer['network_id']]) ? $area_code[$customer['network_id']] : null)."'>";
    echo "<input name='kvz_prefix' id='kvz_prefix' type='hidden' value='".$kvz_prefix."'>";

    $customersCardlineIdentifierPrefix = '';

    if (isset($customer['card_id']) && is_object($customer['card_id']) && !empty($customer['card_id']->getId())) {
        $customersCardlineIdentifierPrefix = $customer['card_id']->getLineIdentifierPrefix();
    }

    echo "<input name='card_line' id='card_line' type='hidden' value='".$customersCardlineIdentifierPrefix."'>";
    echo "<input name='ip_subnetmask' id='ip_subnetmask' type='hidden' value='".$ip_subnetmask."'>";
    echo "<input name='ip_vlanid' id='ip_vlanid' type='hidden' value='".$ip_vlanid."'>";
    echo "<input name='ip_DPBO' id='ip_DPBO' type='hidden' value='".$ip_DPBO."'>";
?>
    <input name='none' id='command' type='hidden' value='yes'>
    <input name='changed_fields' id='changed_fields' type='hidden' value='|'>
    <button name="save" type="submit" id="submit-button-save"><img alt="" src="<?php echo $StartURL.'/_img/Yes.png'; ?>" height="12px"> Speichern</button>&nbsp;&nbsp;&nbsp;&nbsp;
    <button name="reset" onclick="custReset()" type="reset"><img alt="" src="<?php echo $StartURL.'/_img/Erase.png'; ?>" height="12px"> Zurücksetzen</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <button name="delete" onclick="custDelete()" type="button"><img alt="" src="<?php echo $StartURL.'/_img/delete.png'; ?>" height="12px"> Löschen</button>&nbsp;&nbsp;&nbsp;&nbsp;
    <button name="EGW" onclick="ticketForm('<?php echo $this->container->getParameter('ticketSystemType'); ?>')" type="button"><img alt="" src="<?php echo $StartURL.'/_img/Disaster.png'; ?>" height="12px"> Ticket erzeugen</button>
</div>
<?php echo $IncCustPurtelAccount; ?>
<h3 class="CustVersion"></h3>
<div class="CustVersion form">
    <button name="version" onclick="custVersion()" type="button"><img alt="" src="<?php echo $StartURL.'/_img/Save.png'; ?>" height="12px"> Sichern (Bestandskunde)</button>&nbsp;&nbsp;&nbsp;&nbsp;
    <button name="copy" onclick="custCopy()" type="button"><img alt="" src="<?php echo $StartURL.'/_img/Copy.png'; ?>" height="12px"> Kopie (Neuer Vertrag)</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <button name="PDF" onclick="window.location.href='<?php echo $cust_id ?>'" type="button"><img alt="" src="<?php echo $StartURL.'/_img/New document.png'; ?>" height="12px"> Auftrag PDF</button>&nbsp;&nbsp;&nbsp;&nbsp;
    <button name="EGW" onclick="ticketForm('<?php echo $this->container->getParameter('ticketSystemType'); ?>')" type="button"><img alt="" src="<?php echo $StartURL.'/_img/Disaster.png'; ?>" height="12px"> Ticket erzeugen</button>
</div>
</div>
</form>
<div id="dialog"></div>
<?php } $db->close();?>
