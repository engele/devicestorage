function copyMasterAddressToConnection() 
{
    $("[name=CustomerConnectionStreet]").data('oldval', $("[name=CustomerConnectionStreet]").val());
    $("[name=CustomerConnectionStreet]").css('background-color', '#F7F9B6');
    $("[name=CustomerConnectionStreet]").css('border-color', '#089606');
    $("[name=CustomerConnectionStreet]").addClass('changed');
    $("[name=CustomerConnectionStreet]").val($("[name=CustomerStreet]").val());
    $("[name=CustomerConnectionStreet]").data('newval', $("[name=CustomerStreet]").val());

    $("[name=CustomerConnectionStreetno]").data('oldval', $("[name=CustomerConnectionStreetno]").val());
    $("[name=CustomerConnectionStreetno]").css('background-color', '#F7F9B6');
    $("[name=CustomerConnectionStreetno]").css('border-color', '#089606');
    $("[name=CustomerConnectionStreetno]").addClass('changed');
    $("[name=CustomerConnectionStreetno]").val($("[name=CustomerStreetno]").val());
    $("[name=CustomerConnectionStreetno]").data('newval', $("[name=CustomerStreetno]").val());

    $("[name=CustomerConnectionZipcode]").data('oldval', $("[name=CustomerConnectionZipcode]").val());
    $("[name=CustomerConnectionZipcode]").css('background-color', '#F7F9B6');
    $("[name=CustomerConnectionZipcode]").css('border-color', '#089606');
    $("[name=CustomerConnectionZipcode]").addClass('changed');
    $("[name=CustomerConnectionZipcode]").val($("[name=CustomerZipcode]").val());
    $("[name=CustomerConnectionZipcode]").data('newval',$("[name=CustomerZipcode]").val());

    $("[name=CustomerConnectionCity]").data('oldval', $("[name=CustomerConnectionCity]").val());
    $("[name=CustomerConnectionCity]").css('background-color', '#F7F9B6');
    $("[name=CustomerConnectionCity]").css('border-color', '#089606');
    $("[name=CustomerConnectionCity]").addClass('changed');
    $("[name=CustomerConnectionCity]").val($("[name=CustomerCity]").val());
    $("[name=CustomerConnectionCity]").data('newval', $("[name=CustomerCity]").val());
}

function copyConnectionToMasterAddress()
{
    $("[name=CustomerStreet]").data('oldval', $("[name=CustomerStreet]").val());
    $("[name=CustomerStreet]").css('background-color', '#F7F9B6');
    $("[name=CustomerStreet]").css('border-color', '#089606');
    $("[name=CustomerStreet]").addClass('changed');
    $("[name=CustomerStreet]").val($("[name=CustomerConnectionStreet]").val());
    $("[name=CustomerStreet]").data('newval', $("[name=CustomerConnectionStreet]").val());

    $("[name=CustomerStreetno]").data('oldval', $("[name=CustomerStreetno]").val());
    $("[name=CustomerStreetno]").css('background-color', '#F7F9B6');
    $("[name=CustomerStreetno]").css('border-color', '#089606');
    $("[name=CustomerStreetno]").addClass('changed');
    $("[name=CustomerStreetno]").val($("[name=CustomerConnectionStreetno]").val());
    $("[name=CustomerStreetno]").data('newval', $("[name=CustomerConnectionStreetno]").val());

    $("[name=CustomerZipcode]").data('oldval', $("[name=CustomerZipcode]").val());
    $("[name=CustomerZipcode]").css('background-color', '#F7F9B6');
    $("[name=CustomerZipcode]").css('border-color', '#089606');
    $("[name=CustomerZipcode]").addClass('changed');
    $("[name=CustomerZipcode]").val($("[name=CustomerConnectionZipcode]").val());
    $("[name=CustomerZipcode]").data('newval', $("[name=CustomerConnectionZipcode]").val());

    $("[name=CustomerCity]").data('oldval', $("[name=CustomerCity]").val());
    $("[name=CustomerCity]").css('background-color', '#F7F9B6');
    $("[name=CustomerCity]").css('border-color', '#089606');
    $("[name=CustomerCity]").addClass('changed');
    $("[name=CustomerCity]").val($("[name=CustomerConnectionCity]").val());
    $("[name=CustomerCity]").data('newval', $("[name=CustomerConnectionCity]").val());
}

function copyMasterdataToOldcontract()
{
    $("[name=CustomerOldcontractTitle]").data('oldval', $("[name=CustomerOldcontractTitle]").val());
    $("[name=CustomerOldcontractTitle]").css('background-color', '#F7F9B6');
    $("[name=CustomerOldcontractTitle]").css('border-color', '#089606');
    $("[name=CustomerOldcontractTitle]").addClass('changed');
    $("[name=CustomerOldcontractTitle]").val($("[name=CustomerTitle]").val());
    $("[name=CustomerOldcontractTitle]").data('newval', $("[name=CustomerTitle]").val());

    $("[name=CustomerOldcontractLastname]").data('oldval', $("[name=CustomerOldcontractLastname]").val());
    $("[name=CustomerOldcontractLastname]").css('background-color', '#F7F9B6');
    $("[name=CustomerOldcontractLastname]").css('border-color', '#089606');
    $("[name=CustomerOldcontractLastname]").addClass('changed');
    $("[name=CustomerOldcontractLastname]").val($("[name=CustomerLastname]").val());
    $("[name=CustomerOldcontractLastname]").data('newval', $("[name=CustomerLastname]").val());

    $("[name=CustomerOldcontractFirstname]").data('oldval', $("[name=CustomerOldcontractFirstname]").val());
    $("[name=CustomerOldcontractFirstname]").css('background-color', '#F7F9B6');
    $("[name=CustomerOldcontractFirstname]").css('border-color', '#089606');
    $("[name=CustomerOldcontractFirstname]").addClass('changed');
    $("[name=CustomerOldcontractFirstname]").val($("[name=CustomerFirstname]").val());
    $("[name=CustomerOldcontractFirstname]").data('newval', $("[name=CustomerFirstname]").val());

    $("[name=CustomerOldcontractStreet]").data('oldval', $("[name=CustomerOldcontractStreet]").val());
    $("[name=CustomerOldcontractStreet]").css('background-color', '#F7F9B6');
    $("[name=CustomerOldcontractStreet]").css('border-color', '#089606');
    $("[name=CustomerOldcontractStreet]").addClass('changed');
    $("[name=CustomerOldcontractStreet]").val($("[name=CustomerStreet]").val());
    $("[name=CustomerOldcontractStreet]").data('newval', $("[name=CustomerStreet]").val());

    $("[name=CustomerOldcontractStreetno]").data('oldval', $("[name=CustomerOldcontractStreetno]").val());
    $("[name=CustomerOldcontractStreetno]").css('background-color', '#F7F9B6');
    $("[name=CustomerOldcontractStreetno]").css('border-color', '#089606');
    $("[name=CustomerOldcontractStreetno]").addClass('changed');
    $("[name=CustomerOldcontractStreetno]").val($("[name=CustomerStreetno]").val());
    $("[name=CustomerOldcontractStreetno]").data('newval', $("[name=CustomerStreetno]").val());

    $("[name=CustomerOldcontractZipcode]").data('oldval', $("[name=CustomerOldcontractZipcode]").val());
    $("[name=CustomerOldcontractZipcode]").css('background-color', '#F7F9B6');
    $("[name=CustomerOldcontractZipcode]").css('border-color', '#089606');
    $("[name=CustomerOldcontractZipcode]").addClass('changed');
    $("[name=CustomerOldcontractZipcode]").val($("[name=CustomerZipcode]").val());
    $("[name=CustomerOldcontractZipcode]").data('newval', $("[name=CustomerZipcode]").val());

    $("[name=CustomerOldcontractCity]").data('oldval', $("[name=CustomerOldcontractCity]").val());
    $("[name=CustomerOldcontractCity]").css('background-color', '#F7F9B6');
    $("[name=CustomerOldcontractCity]").css('border-color', '#089606');
    $("[name=CustomerOldcontractCity]").addClass('changed');
    $("[name=CustomerOldcontractCity]").val($("[name=CustomerCity]").val());   
    $("[name=CustomerOldcontractCity]").data('newval', $("[name=CustomerCity]").val());
}

function fillOldContractInetData()
{
    $("[name=CustomerOldcontractInternetProvider]").data('oldval', $("[name=CustomerOldcontractInternetProvider]").val());
    $("[name=CustomerOldcontractInternetProvider]").css('background-color', '#F7F9B6');
    $("[name=CustomerOldcontractInternetProvider]").css('border-color', '#089606');
    $("[name=CustomerOldcontractInternetProvider]").addClass('changed');
    $("[name=CustomerOldcontractInternetProvider]").val("Kein Vertrag");
    $("[name=CustomerOldcontractInternetProvider]").data('newval', "Kein Vertrag");

    $("[name=CustomerOldcontractInternetClientno]").data('oldval', $("[name=CustomerOldcontractInternetClientno]").val());
    $("[name=CustomerOldcontractInternetClientno]").css('background-color', '#F7F9B6');
    $("[name=CustomerOldcontractInternetClientno]").css('border-color', '#089606');
    $("[name=CustomerOldcontractInternetClientno]").addClass('changed');
    $("[name=CustomerOldcontractInternetClientno]").val("Kein Vertrag");
    $("[name=CustomerOldcontractInternetClientno]").data('newval', "Kein Vertrag");

    $("[name=CustomerExpDateInt]").data('oldval', $("[name=CustomerExpDateInt]").val());
    $("[name=CustomerExpDateInt]").css('background-color', '#F7F9B6');
    $("[name=CustomerExpDateInt]").css('border-color', '#089606');
    $("[name=CustomerExpDateInt]").addClass('changed');
    $("[name=CustomerExpDateInt]").val("Kein Vertrag");
    $("[name=CustomerExpDateInt]").data('newval', "Kein Vertrag");

    $("[name=CustomerNoticePeriodInternetInt]").data('oldval', $("[name=CustomerNoticePeriodInternetInt]").val());
    $("[name=CustomerNoticePeriodInternetInt]").css('background-color', '#F7F9B6');
    $("[name=CustomerNoticePeriodInternetInt]").css('border-color', '#089606');
    $("[name=CustomerNoticePeriodInternetInt]").addClass('changed');
    $("[name=CustomerNoticePeriodInternetInt]").val("N/E");
    $("[name=CustomerNoticePeriodInternetInt]").data('newval', "N/E");
}

function copyOldContractInetData()
{
    $("[name=CustomerOldcontractInternetProvider]").data('oldval', $("[name=CustomerOldcontractInternetProvider]").val());
    $("[name=CustomerOldcontractInternetProvider]").css('background-color', '#F7F9B6');
    $("[name=CustomerOldcontractInternetProvider]").css('border-color', '#089606');
    $("[name=CustomerOldcontractInternetProvider]").addClass('changed');
    $("[name=CustomerOldcontractInternetProvider]").val($("[name=CustomerOldcontractPhoneProvider]").val());
    $("[name=CustomerOldcontractInternetProvider]").data('newval', $("[name=CustomerOldcontractPhoneProvider]").val());

    $("[name=CustomerOldcontractInternetClientno]").data('oldval', $("[name=CustomerOldcontractInternetClientno]").val());
    $("[name=CustomerOldcontractInternetClientno]").css('background-color', '#F7F9B6');
    $("[name=CustomerOldcontractInternetClientno]").css('border-color', '#089606');
    $("[name=CustomerOldcontractInternetClientno]").addClass('changed');
    $("[name=CustomerOldcontractInternetClientno]").val($("[name=CustomerOldcontractPhoneClientno]").val());
    $("[name=CustomerOldcontractInternetClientno]").data('newval', $("[name=CustomerOldcontractPhoneClientno]").val());

    $("[name=CustomerExpDateInt]").data('oldval', $("[name=CustomerExpDateInt]").val());
    $("[name=CustomerExpDateInt]").css('background-color', '#F7F9B6');
    $("[name=CustomerExpDateInt]").css('border-color', '#089606');
    $("[name=CustomerExpDateInt]").addClass('changed');
    $("[name=CustomerExpDateInt]").val($("[name=CustomerExpDatePhone]").val());
    $("[name=CustomerExpDateInt]").data('newval', $("[name=CustomerExpDatePhone]").val());

    $("[name=CustomerNoticePeriodInternetInt]").data('oldval', $("[name=CustomerNoticePeriodInternetInt]").val());
    $("[name=CustomerNoticePeriodInternetInt]").css('background-color', '#F7F9B6');
    $("[name=CustomerNoticePeriodInternetInt]").css('border-color', '#089606');
    $("[name=CustomerNoticePeriodInternetInt]").addClass('changed');
    $("[name=CustomerNoticePeriodInternetInt]").val($("[name=CustomerNoticePeriodPhoneInt]").val());
    $("[name=CustomerNoticePeriodInternetInt]").data('newval', $("[name=CustomerNoticePeriodPhoneInt]").val());

    $("[name=CustomerNoticePeriodInternetType]").data('oldval', $("[name=CustomerNoticePeriodInternetType]").val());
    $("[name=CustomerNoticePeriodInternetType]").css('background-color', '#F7F9B6');
    $("[name=CustomerNoticePeriodInternetType]").css('border-color', '#089606');
    $("[name=CustomerNoticePeriodInternetType]").addClass('changed');
    $("[name=CustomerNoticePeriodInternetType]").val($("[name=CustomerNoticePeriodPhoneType]").val()); 
    $("[name=CustomerNoticePeriodInternetType]").data('newval', $("[name=CustomerNoticePeriodPhoneType]").val());
}

function copyOldContractPhoneData()
{
    $("[name=CustomerOldcontractPhoneProvider]").data('oldval', $("[name=CustomerOldcontractPhoneProvider]").val());
    $("[name=CustomerOldcontractPhoneProvider]").css('background-color', '#F7F9B6');
    $("[name=CustomerOldcontractPhoneProvider]").css('border-color', '#089606');
    $("[name=CustomerOldcontractPhoneProvider]").addClass('changed');
    $("[name=CustomerOldcontractPhoneProvider]").val($("[name=CustomerOldcontractInternetProvider]").val());
    $("[name=CustomerOldcontractPhoneProvider]").data('newval', $("[name=CustomerOldcontractInternetProvider]").val());

    $("[name=CustomerOldcontractPhoneClientno]").data('oldval', $("[name=CustomerOldcontractPhoneClientno]").val());
    $("[name=CustomerOldcontractPhoneClientno]").css('background-color', '#F7F9B6');
    $("[name=CustomerOldcontractPhoneClientno]").css('border-color', '#089606');
    $("[name=CustomerOldcontractPhoneClientno]").addClass('changed');
    $("[name=CustomerOldcontractPhoneClientno]").val($("[name=CustomerOldcontractInternetClientno]").val());
    $("[name=CustomerOldcontractPhoneClientno]").data('newval', $("[name=CustomerOldcontractInternetClientno]").val());

    $("[name=CustomerExpDatePhone]").data('oldval', $("[name=CustomerExpDatePhone]").val());
    $("[name=CustomerExpDatePhone]").css('background-color', '#F7F9B6');
    $("[name=CustomerExpDatePhone]").css('border-color', '#089606');
    $("[name=CustomerExpDatePhone]").addClass('changed');
    $("[name=CustomerExpDatePhone]").val($("[name=CustomerExpDateInt]").val());
    $("[name=CustomerExpDatePhone]").data('newval', $("[name=CustomerExpDateInt]").val());

    $("[name=CustomerNoticePeriodPhoneInt]").data('oldval', $("[name=CustomerNoticePeriodPhoneInt]").val());
    $("[name=CustomerNoticePeriodPhoneInt]").css('background-color', '#F7F9B6');
    $("[name=CustomerNoticePeriodPhoneInt]").css('border-color', '#089606');
    $("[name=CustomerNoticePeriodPhoneInt]").addClass('changed');
    $("[name=CustomerNoticePeriodPhoneInt]").val($("[name=CustomerNoticePeriodInternetInt]").val());
    $("[name=CustomerNoticePeriodPhoneInt]").data('newval', $("[name=CustomerNoticePeriodInternetInt]").val());

    $("[name=CustomerNoticePeriodPhoneType]").data('oldval', $("[name=CustomerNoticePeriodPhoneType]").val());
    $("[name=CustomerNoticePeriodPhoneType]").css('background-color', '#F7F9B6');
    $("[name=CustomerNoticePeriodPhoneType]").css('border-color', '#089606');
    $("[name=CustomerNoticePeriodPhoneType]").addClass('changed');
    $("[name=CustomerNoticePeriodPhoneType]").val($("[name=CustomerNoticePeriodInternetType]").val()); 
    $("[name=CustomerNoticePeriodPhoneType]").data('newval', $("[name=CustomerNoticePeriodInternetType]").val());
}

function fillOldContractPhoneData()
{
    $("[name=CustomerOldcontractPhoneProvider]").data('oldval', $("[name=CustomerOldcontractPhoneProvider]").val());
    $("[name=CustomerOldcontractPhoneProvider]").css('background-color', '#F7F9B6');
    $("[name=CustomerOldcontractPhoneProvider]").css('border-color', '#089606');
    $("[name=CustomerOldcontractPhoneProvider]").addClass('changed');
    $("[name=CustomerOldcontractPhoneProvider]").val("Kein Vertrag");
    $("[name=CustomerOldcontractPhoneProvider]").data('newval', "Kein Vertrag");

    $("[name=CustomerOldcontractPhoneClientno]").data('oldval', $("[name=CustomerOldcontractPhoneClientno]").val());
    $("[name=CustomerOldcontractPhoneClientno]").css('background-color', '#F7F9B6');
    $("[name=CustomerOldcontractPhoneClientno]").css('border-color', '#089606');
    $("[name=CustomerOldcontractPhoneClientno]").addClass('changed');
    $("[name=CustomerOldcontractPhoneClientno]").val("Kein Vertrag");
    $("[name=CustomerOldcontractPhoneClientno]").data('newval', "Kein Vertrag");

    $("[name=CustomerExpDatePhone]").data('oldval', $("[name=CustomerExpDatePhone]").val());
    $("[name=CustomerExpDatePhone]").css('background-color', '#F7F9B6');
    $("[name=CustomerExpDatePhone]").css('border-color', '#089606');
    $("[name=CustomerExpDatePhone]").addClass('changed');
    $("[name=CustomerExpDatePhone]").val("Kein Vertrag");
    $("[name=CustomerExpDatePhone]").data('newval', "Kein Vertrag");

    $("[name=CustomerNoticePeriodPhoneInt]").data('oldval', $("[name=CustomerNoticePeriodPhoneInt]").val());
    $("[name=CustomerNoticePeriodPhoneInt]").css('background-color', '#F7F9B6');
    $("[name=CustomerNoticePeriodPhoneInt]").css('border-color', '#089606');
    $("[name=CustomerNoticePeriodPhoneInt]").addClass('changed');
    $("[name=CustomerNoticePeriodPhoneInt]").val("N/E");
    $("[name=CustomerNoticePeriodPhoneInt]").data('newval', "N/E");
}

function fillTechdata()
{
    $("[name=CustomerPppoeConfigDate]").data('oldval', $("[name=CustomerPppoeConfigDate]").val());
    $("[name=CustomerPppoeConfigDate]").css('background-color', '#F7F9B6');
    $("[name=CustomerPppoeConfigDate]").css('border-color', '#089606');
    $("[name=CustomerPppoeConfigDate]").addClass('changed');
    $("[name=CustomerPppoeConfigDate]").val("N/E da GF");
    $("[name=CustomerPppoeConfigDate]").data('newval', "N/E da GF");

    $("[name=CustomerPatchDate]").data('oldval', $("[name=CustomerPatchDate]").val());
    $("[name=CustomerPatchDate]").css('background-color', '#F7F9B6');
    $("[name=CustomerPatchDate]").css('border-color', '#089606');
    $("[name=CustomerPatchDate]").addClass('changed');
    $("[name=CustomerPatchDate]").val("N/E da GF");
    $("[name=CustomerPatchDate]").data('newval', "N/E da GF");

    $("[name=CustomerlsaKvzPartitionId]").data('oldval', $("[name=CustomerlsaKvzPartitionId]").val());
    $("[name=CustomerlsaKvzPartitionId]").css('background-color', '#F7F9B6');
    $("[name=CustomerlsaKvzPartitionId]").css('border-color', '#089606');
    $("[name=CustomerlsaKvzPartitionId]").addClass('changed');
    $("[name=CustomerlsaKvzPartitionId]").val("N/E da GF");
    $("[name=CustomerlsaKvzPartitionId]").data('newval', "N/E da GF");

    $("[name=CustomerKvzAddition]").data('oldval', $("[name=CustomerKvzAddition]").val());
    $("[name=CustomerKvzAddition]").css('background-color', '#F7F9B6');
    $("[name=CustomerKvzAddition]").css('border-color', '#089606');
    $("[name=CustomerKvzAddition]").addClass('changed');
    $("[name=CustomerKvzAddition]").val("GPON");
    $("[name=CustomerKvzAddition]").data('newval', "GPON");
}

function fillPortingdata()
{
    $("[name=CustomerVenteloPurtelEnterDate]").data('oldval', $("[name=CustomerVenteloPurtelEnterDate]").val());
    $("[name=CustomerVenteloPurtelEnterDate]").css('background-color', '#F7F9B6');
    $("[name=CustomerVenteloPurtelEnterDate]").css('border-color', '#089606');
    $("[name=CustomerVenteloPurtelEnterDate]").addClass('changed');
    $("[name=CustomerVenteloPurtelEnterDate]").val("N/E");
    $("[name=CustomerVenteloPurtelEnterDate]").data('newval', "N/E");

    $("[name=CustomerStatiPortConfirmDate]").data('oldval', $("[name=CustomerStatiPortConfirmDate]").val());
    $("[name=CustomerStatiPortConfirmDate]").css('background-color', '#F7F9B6');
    $("[name=CustomerStatiPortConfirmDate]").css('border-color', '#089606');
    $("[name=CustomerStatiPortConfirmDate]").addClass('changed');
    $("[name=CustomerStatiPortConfirmDate]").val("N/E");
    $("[name=CustomerStatiPortConfirmDate]").data('newval', "N/E");

    $("[name=CustomerVenteloPortLetterDate]").data('oldval', $("[name=CustomerVenteloPortLetterDate]").val());
    $("[name=CustomerVenteloPortLetterDate]").css('background-color', '#F7F9B6');
    $("[name=CustomerVenteloPortLetterDate]").css('border-color', '#089606');
    $("[name=CustomerVenteloPortLetterDate]").addClass('changed');
    $("[name=CustomerVenteloPortLetterDate]").val("N/E");
    $("[name=CustomerVenteloPortLetterDate]").data('newval', "N/E");

    $("[name=CustomerVenteloConfirmationDate]").data('oldval', $("[name=CustomerVenteloConfirmationDate]").val());
    $("[name=CustomerVenteloConfirmationDate]").css('background-color', '#F7F9B6');
    $("[name=CustomerVenteloConfirmationDate]").css('border-color', '#089606');
    $("[name=CustomerVenteloConfirmationDate]").addClass('changed');
    $("[name=CustomerVenteloConfirmationDate]").val("N/E");
    $("[name=CustomerVenteloConfirmationDate]").data('newval', "N/E");

    $("[name=CustomerVenteloPortWishDate]").data('oldval', $("[name=CustomerVenteloPortWishDate]").val());
    $("[name=CustomerVenteloPortWishDate]").css('background-color', '#F7F9B6');
    $("[name=CustomerVenteloPortWishDate]").css('border-color', '#089606');
    $("[name=CustomerVenteloPortWishDate]").addClass('changed');
    $("[name=CustomerVenteloPortWishDate]").val("N/E");
    $("[name=CustomerVenteloPortWishDate]").data('newval', "N/E");
}

function fillTaldata()
{
    $("[name=CustomerTalOrderDate]").data('oldval', $("[name=CustomerTalOrderDate]").val());
    $("[name=CustomerTalOrderDate]").css('background-color', '#F7F9B6');
    $("[name=CustomerTalOrderDate]").css('border-color', '#089606');
    $("[name=CustomerTalOrderDate]").addClass('changed');
    $("[name=CustomerTalOrderDate]").val("N/E da GF");
    $("[name=CustomerTalOrderDate]").data('newval', "N/E da GF");

    $("[name=CustomerTalDtagAssignmentNo]").data('oldval', $("[name=CustomerTalDtagAssignmentNo]").val());
    $("[name=CustomerTalDtagAssignmentNo]").css('background-color', '#F7F9B6');
    $("[name=CustomerTalDtagAssignmentNo]").css('border-color', '#089606');
    $("[name=CustomerTalDtagAssignmentNo]").addClass('changed');
    $("[name=CustomerTalDtagAssignmentNo]").val("N/E da GF");
    $("[name=CustomerTalDtagAssignmentNo]").data('newval', "N/E da GF");

    $("[name=CustomerCustomerConnectInfoDate]").data('oldval', $("[name=CustomerCustomerConnectInfoDate]").val());
    $("[name=CustomerCustomerConnectInfoDate]").css('background-color', '#F7F9B6');
    $("[name=CustomerCustomerConnectInfoDate]").css('border-color', '#089606');
    $("[name=CustomerCustomerConnectInfoDate]").addClass('changed');
    $("[name=CustomerCustomerConnectInfoDate]").val("N/E da GF");
    $("[name=CustomerCustomerConnectInfoDate]").data('newval', "N/E da GF");
}

function custReset()
{
    $(".changed").css('background-color', '#FFFFFF');
    $(".changed").css('border-color', '#C0C0C0');
    $(".changed").removeClass('changed');
}

function custSubmit()
{
    $("#custForm").submit();
}

function custDelete()
{
    version = '';
    if ($("#CustomerVersion").val() != '') {version = " (Version: "+$("#CustomerVersion").val()+") ";}
    htmlstr = "Wollen Sie den Kunden <br /><h3><i>"+$(":input[name=CustomerFirstname]").val()+" "+$(":input[name=CustomerLastname]").val()+" ("+$(":input[name=CustomerClientid]").val()+")"+version+"</i></h3>wirklich löschen?";
    $("#dialog").html(htmlstr);
    $("#dialog").dialog({
        resizable: true,
        modal: true,
        title: 'Kundendaten löschen',
        position: {my: 'top', at: 'top', of: '#Container'},
        height: 300,
        width: 500,
        buttons: {
            'Ja': function ()
            {
                $(this).dialog('close');
                callDelete(true);
            },
            'Nein': function ()
            {
                $(this).dialog('close');
                callDelete(false);
            }
        }
    });
}

function callDelete(save)
{
    if (save){
        $("#command").attr('name', 'delete');
        $("#custForm").submit();
    }
}

function custVersion()
{
    htmlstr = "Wollen Sie den Kunden <br /><h3><i>"+$(":input[name=CustomerFirstname]").val()+" "+$(":input[name=CustomerLastname]").val()+" ("+$(":input[name=CustomerClientid]").val()+")</i></h3>wirklich sichern?";
    $("#dialog").html(htmlstr);
    $("#dialog").dialog({
        resizable: true,
        modal: true,
        title: 'Kunde sichern',
        position: {my: 'top', at: 'top', of: '#Container'},
        height: 300,
        width: 500,
        buttons: {
            'Ja': function () {
                $(this).dialog('close');
                callVersion(true);
            },
            'Nein': function () {
                $(this).dialog('close');
                callVersion(false);
            }
        }
    });
}

function callVersion(save)
{
    if (save){
        $("#command").attr('name', 'version');
        $("#custForm").submit();
    }
}

function custCopy()
{
    htmlstr = "Wollen Sie den Kunden <br /><h3><i>"+$(":input[name=CustomerFirstname]").val()+" "+$(":input[name=CustomerLastname]").val()+" ("+$(":input[name=CustomerClientid]").val()+")</i></h3>wirklich kopieren<br />um einen neuen Vertrag zu erstellen?";
    $("#dialog").html(htmlstr);
    $("#dialog").dialog({
        resizable: true,
        modal: true,
        title: 'Kunde kopieren',
        position: {my: 'top', at: 'top', of: '#Container'},
        height: 300,
        width: 500,
        buttons: {
            'Ja': function () {
                $(this).dialog('close');
                callCopy(true);
            },
            'Nein': function () {
                (this).dialog('close');
                callCopy(false);
            }
        }
    });
}

function callCopy(save) {
    if (save){
        $("#command").attr('name', 'copy');
        $("#custForm").submit();
    }
}

function custMailFileLoad()
{
    $('#MailFiles').uploadifive('upload');
}

function custMailSend()
{
    htmlstr = "Wollen Sie die E-Mail senden?";
    $("#dialog").html(htmlstr);
    $("#dialog").dialog({
        resizable: true,
        modal: true,
        title: 'E-Mail senden',
        position: {my: 'top', at: 'top', of: '#Container'},
        height: 300,
        width: 500,
        buttons: {
            'Ja': function () {
                $(this).dialog('close');
                callMailSend(true);
            },
            'Nein': function () {
                $(this).dialog('close');
                callMailSend(false);
            }
        }
  });
}

function callMailSend(save)
{
    if (save){
        var StartPath   = $("#StartPath").val();
        var SelPath     = $("#SelPath").val();
        var StartURL    = $("#StartURL").val();
        var CustomerId  = $(":input[name=CustomerId]").val();
        var To          = $(":input[name=MailAddress]").val();
        var Subject     = encodeURIComponent($(":input[name=MailSubject]").val());
        var Body        = encodeURIComponent($(":input[name=MailBody]").val());
        var Attachment  = [];

        $("#MailFileQueue .complete .filename").each (function () {
            Attachment.push($(this).text());
        });

        AttachFiles = JSON.stringify(Attachment);
        StdPdf = 0;

        switch ($("#MailSubject").val()) {
            case "Vertragsunterlagen für DSL DaheimInternet von "+COMPANY_NAME+" Energie AG":
                StdPdf = 8;
                break;

            case "Vertragsunterlagen für Glasfaser DaheimInternet von "+COMPANY_NAME+" Energie AG":
                StdPdf = 9;
                break;

            case "Vertragseingangsbestätigung Daheiminternet BADEN.NET":
                StdPdf = 1;
                break;

            case "Auftragsformular und AGB BADEN.NET":
                StdPdf = 2;
                break;

            case "Fehlendes SEPA Lastschriftmandat BADEN.NET":
                StdPdf = 3;
                break;

            case "Zahlung per Rechnung nicht möglich":
                StdPdf = 4;
                break;

            case "Daten für Telefonbucheintrag":
                StdPdf = 5;
                break;

            case "Willkommen bei BADEN.NET":
                StdPdf = 6;
                break;

            case "Willkommen bei BADEN.NET IP-TV (Buchung nachträglich)":
                StdPdf = 7;
                break;
        }


        $.ajax({
            type: "POST",
            url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxMailSend.php",
            data: "CustomerId="+CustomerId+"&StartPath="+StartPath+"&SelPath="+SelPath+"&To="+To+"&Subject="+Subject+"&Body="+Body+"&AttachFiles="+AttachFiles+"&StdPdf="+StdPdf,
            success: function(data){ajaxMailSend(data)}
        });
    }
}

function custMailReset() 
{
    $(".MailForm").hide();
}

function ajaxMailSend(data) 
{
    var response    = $.parseJSON(data);
    var status      = response.status;
    var error       = response.error;
  
    if (status == 1) {
        alert ('E-Mail konnte nicht gesendet werden: '+error);
    }
    $(".MailForm").hide();
}

function custChange2customer()
{
    htmlstr = "Wollen Sie den Interessenten <br /><h3><i>"+$(":input[name=CustomerFirstname]").val()+" "+$(":input[name=CustomerLastname]").val()+" ("+$(":input[name=CustomerClientid]").val()+")</i></h3>in einen Kunden umwandeln?";
    $("#dialog").html(htmlstr);
    $("#dialog").dialog({
        resizable: true,
        modal: true,
        title: 'Interessenten zu Kunden wandeln',
        position: {my: 'top', at: 'top', of: '#Container'},
        height: 300,
        width: 500,
        buttons: {
            'Ja': function () {
                $(this).dialog('close');
                callChange2Customer(true);
            },
            'Nein': function () {
                $(this).dialog('close');
                callChange2Customer(false);
            }
        }
    });
}

function callChange2Customer(save)
{
    if (save){
        var StartPath = $("#StartPath").val();
        var SelPath = $("#SelPath").val();
        var StartURL = $("#StartURL").val();
        var CustomerId = $(":input[name=CustomerId]").val();
        $.ajax({
            type: "POST",
            url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxChange2Customer.php",
            data: "CustomerId="+CustomerId,
            success: function(data){ajaxChange2CustomerAnswer(data)}
        }); 
    }
}

function ajaxChange2CustomerAnswer(data)
{
    var response    = $.parseJSON(data);

    var status      = response.status;
    var returnStr   = response.returnStr;
    var clientId    = response.clientId;
    var clientIdOld = $(":input[name=CustomerClientid]").val();
  
    if (status) {
        $(":input[name=CustomerClientid]").val(clientId);
        $('#Content > h1').text($('#Content > h1').text().replace(clientIdOld, clientId));
        $('#change2customer').hide();
    }
    $('#Content > h1').append(returnStr);
}

function ajaxConnectingRequest()
{
    var connect_request_replace = $("#connect_request_replace").val();
    var connect_letter_replace = $("#connect_letter_replace").val();
    var umlaut = $("#umlaut").val();
    var StartPath = $("#StartPath").val();
    var SelPath = $("#SelPath").val();
    var StartURL = $("#StartURL").val();
    $.ajax({
        type: "POST",
        url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxConnectingRequest.php",
        data: "connect_request_replace="+connect_request_replace+"&connect_letter_replace="+connect_letter_replace+"&connect_letter_stick_replace="+"&umlaut="+umlaut+"&StartPath="+StartPath+"&StartURL="+StartURL,
        success: function(data){ajaxConnectingRequestAnswer(data)}
    }); 
}

function ajaxConnectingRequestAnswer(data)
{
    var response    = $.parseJSON(data);
    var urlLetter1  = response.urlLetter1;
    var urlLetter2  = response.urlLetter2;
    var urlLetter3  = response.urlLetter3;
  
    $('#connectingLetter1').attr('href', urlLetter1);
    $('#connectingLetter1').css('background-color', '#EAEAEA');
    $('#connectingLetter2').attr('href', urlLetter2);
    $('#connectingLetter2').css('background-color', '#EAEAEA');
    $('#connectingLetter3').attr('href', urlLetter3);
    $('#connectingLetter3').css('background-color', '#EAEAEA');
}

function ajaxNetwork()
{
    var StartPath = $("#StartPath").val();
    var SelPath   = $("#SelPath").val();
    var StartURL  = $("#StartURL").val();
    var NetworkId = $(":input[name=CustomerNetworkId]").val();
    $.ajax({
        type: "POST",
        url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxNetwork.php",
        data: "NetworkId="+NetworkId+"&StartPath="+StartPath+"&SelPath="+SelPath+"&StartURL="+StartURL,
        success: function(data){ajaxNetworkAnswer(data)}
    }); 
}

function ajaxNetworkAnswer(data)
{
    var response    = $.parseJSON(data);
    $(":input[name=CustomerLocationId]").empty();
    $(":input[name=CustomerLocationId]").append($("<option></potion>").attr("value", "").text(""));
    $.each (response.Test, function (value, text) {
        $(":input[name=CustomerLocationId]").append($("<option></potion>").attr("value", value).text(text));
    });
}

function custInkassoSend()
{
    htmlstr = "Wollen Sie für den Kunden <br /><h3><i>"+$(":input[name=CustomerFirstname]").val()+" "+$(":input[name=CustomerLastname]").val()+" ("+$(":input[name=CustomerClientid]").val()+")</i></h3>einen Inkassoauftrag senden?";
    $("#dialog").html(htmlstr);
    $("#dialog").dialog({
        resizable: true,
        modal: true,
        title: 'Inkassoauftrag senden',
        position: {my: 'top', at: 'top', of: '#Container'},
        height: 300,
        width: 500,
        buttons: {
            'Ja': function () {
                $(this).dialog('close');
                ajaxInkassoSend(true);
            },
            'Nein': function () {
                $(this).dialog('close');
                ajaxInkassoSend(false);
            }
    }
  });
}

function ajaxInkassoSend(save) 
{
    if (save){
        var StartPath = $("#StartPath").val();
        var SelPath   = $("#SelPath").val();
        var StartURL  = $("#StartURL").val();
        var CustID    = $("#CustomerId").val();
        var temp, InkassoParams;

        var inkasso   = {};
        inkasso['anschluss']              = $("[name=CustomerInkassoaccount]").val();
        inkasso['kundennummer_extern']    = $("[name=CustomerClientid]").val();
        inkasso['rechnung']               = encodeURIComponent($("[name=CustomerInkassoRechnung]").val());
        inkasso['hauptforderung']         = $("[name=CustomerInkassoHauptforderung]").val() * 100;
        inkasso['bankgebuehren']          = $("[name=CustomerInkassoBankgebuehren]").val() * 100;
        inkasso['mahngebuehren']          = $("[name=CustomerInkassoMahngebuehren]").val() * 100;
        inkasso['datum_letzte_mahnung']   = encodeURIComponent($("[name=CustomerInkassoDatumLetzteMahnung]").val());
        inkasso['datum_ruecklastschrift'] = encodeURIComponent($("[name=CustomerInkassoDatumRuelastschrift]").val());
        inkasso['grund_ruecklastschrift'] = encodeURIComponent($("[name=CustomerInkassoGrundRuelastschrift]").val().replace('\n', ' '));
        if (inkasso['datum_letzte_mahnung'] != '') {
            temp = inkasso['datum_letzte_mahnung'].split('.');
            inkasso['datum_letzte_mahnung'] = temp[2] + temp[1] + temp[0];
        }
        if (inkasso['datum_ruecklastschrift'] != '') {
            temp = inkasso['datum_ruecklastschrift'].split('.');
            inkasso['datum_ruecklastschrift'] = temp[2] + temp[1] + temp[0];
        }
        InkassoParams = JSON.stringify(inkasso);
        $("#CustomerInkassoSend").css('background-color', '#F78C8C');
        $.ajax({
            type: "POST",
            url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxInkasso.php",
            data: "StartPath="+StartPath+"&SelPath="+SelPath+"&StartURL="+StartURL+"&CustID="+CustID+"&Inkasso="+InkassoParams,
            success: function(data){ajaxInkassoSendAnswer(data)}
        }); 
    }
}

function ajaxInkassoSendAnswer(data) 
{
    var response        = $.parseJSON(data);
    var error_mes       = response.error_mes;
    var status          = response.status;

    $("#CustomerInkassoSend").css('background-color', '#EAEAEA');
    if (!status) {
        alert (error_mes);
    }
}

function purtelAccount(custId, purtelMaster, purtelId, purtelLogin) 
{
    if (purtelLogin != '') {
        htmlstr = "Wollen Sie für den Kunden <br /><h3><i>"+$(":input[name=CustomerFirstname]").val()+" "+$(":input[name=CustomerLastname]").val()+" ("+$(":input[name=CustomerClientid]").val()+" | "+purtelLogin+")</i></h3>das Purtelkundenkonto ändern?"
    } else {
        htmlstr = "Wollen Sie für den Kunden <br /><h3><i>"+$(":input[name=CustomerFirstname]").val()+" "+$(":input[name=CustomerLastname]").val()+" ("+$(":input[name=CustomerClientid]").val()+")</i></h3>ein Purtelkundenkonto anlegen?"
    }
    $("#dialog").html(htmlstr);
    $("#dialog").dialog({
        resizable: true,
        modal: true,
        title: 'Purtelkundenkonto erstellen',
        position: {my: 'top', at: 'top', of: '#Container'},
        height: 300,
        width: 500,
        buttons: {
            'Ja': function () {
                $(this).dialog('close');
                ajaxPurtelAccount(true, custId, purtelMaster, purtelId, purtelLogin);
            },
            'Nein': function () {
                $(this).dialog('close');
                ajaxPurtelAccount(false, custId, purtelMaster, purtelId, purtelLogin);
            }
        }
    });
}

function ajaxPurtelAccount(save, custId, purtelMaster, purtelId, purtelLogin) 
{
    if (save){
        var StartPath = $("#StartPath").val();
        var SelPath = $("#SelPath").val();
        var StartURL = $("#StartURL").val();
        var birthday;
        var str_part;
        var PurtelIndex;
        var purtel = {};
        var purtel2 = {};
        var purtel3 = {};
    
        purtel['anrede'] = $("[name=CustomerTitle]").val();
        if (purtel['anrede'] == 'Herr') {
            purtel['anrede'] = '1';
        } else {
            purtel['anrede'] = '2';
        }
        purtel['vorname'] = encodeURIComponent($("[name=CustomerFirstname]").val());
        purtel['nachname'] = encodeURIComponent($("[name=CustomerLastname]").val());
        purtel['adresse'] = encodeURIComponent($("[name=CustomerStreet]").val());
        purtel['plz'] = encodeURIComponent($("[name=CustomerZipcode]").val());
        purtel['ort'] = encodeURIComponent($("[name=CustomerCity]").val());
        purtel['version'] = $("[name=CustomerVersion]").val();
        purtel['purtel_use_version'] = $("[name=CustomerPurtelUseVersion]").val();
        purtel['haus_nr'] = encodeURIComponent($(":input[name=CustomerStreetno]").val());
        purtel['zusatz'] = encodeURIComponent($(":input[name=CustomerStreetnoAddition]").val());

        // To ensure compatibility to old times before the field "StreetnoAddition" existed.
        // Back then where housenumber and addition only in the field "Streetno".
        if ("" == purtel['zusatz']) {
            str_part = encodeURIComponent($(":input[name=CustomerStreetno]").val()).match(/(\d+)(.*)/);

            if ("" != str_part[2]) {
                purtel['haus_nr'] = str_part[1];
                purtel['zusatz'] = str_part[2];
            }
        }
        
        if (purtel['purtel_use_version'] == 1 && purtel['version'] > 0) {
            purtel['kundennummer_extern'] = $("[name=CustomerClientid]").val()+'_'+purtel['version'];
        } else {
            purtel['kundennummer_extern'] = $("[name=CustomerClientid]").val();
        }

        birthday = $("[name=CustomerBirthday]").val().split('.');
        purtel['geburtsdatum'] = $("[name=CustomerBirthday]").val();

        if (purtel['geburtsdatum'] != '') {
            birthday   = purtel['geburtsdatum'].split('.');
            purtel['geburtsdatum']      = birthday[2] + birthday[1] + birthday[0];
        }

        purtel['email'] = encodeURIComponent($("[name=CustomerBankAccountEmailaddress]").val());
        
        if (purtel['anrede'] == '') {
            purtel['email'] = encodeURIComponent($("[name=CustomerEmailaddress]").val());
        }

        purtel['ortsnetz'] = encodeURIComponent($("[name=area_code]").val());
        
        if (purtelMaster != '') {
            purtel['konto_von'] = purtelMaster;  
        }

        purtel2['anrede'] = $("[name=CustomerTitle]").val();
        if (purtel2['anrede'] == 'Herr') {
            purtel2['anrede'] = '1';
        } else {
            purtel2['anrede'] = '2';
        }
        purtel2['vorname'] = encodeURIComponent($("[name=CustomerFirstname]").val());
        purtel2['nachname'] = encodeURIComponent($("[name=CustomerLastname]").val());
        //purtel2['adresse'] = encodeURIComponent($("[name=CustomerStreet]").val());
        purtel2['haus_nr'] = purtel['haus_nr'];
        purtel2['zusatz'] = purtel['zusatz'];
        purtel2['plz'] = encodeURIComponent($("[name=CustomerZipcode]").val());
        purtel2['ort'] = encodeURIComponent($("[name=CustomerCity]").val());
        purtel2['firma'] = encodeURIComponent($("[name=CustomerCompany]").val());
        purtel2['kundennummer_extern'] = purtel['kundennummer_extern'];
        birthday = $("[name=CustomerBirthday]").val().split('.');
        purtel2['geburtsdatum'] = $("[name=CustomerBirthday]").val();
        
        if (purtel2['geburtsdatum'] != '') {
            birthday = purtel2['geburtsdatum'].split('.');
            purtel2['geburtsdatum'] = birthday[2] + birthday[1] + birthday[0];
        }

        purtel2['email'] = encodeURIComponent($("[name=CustomerBankAccountEmailaddress]").val());
        
        if (purtel2['email'] == '') {
            purtel2['email'] = encodeURIComponent($("[name=CustomerEmailaddress]").val());
        }

        purtel2['ortsnetz'] = encodeURIComponent($("[name=area_code]").val());
        purtel2['telefon'] = encodeURIComponent($("[name=CustomerPhoneareacode]").val()+$(":input[name=CustomerPhonenumber]").val());
        purtel2['mobile'] = encodeURIComponent($("[name=CustomerMobilephone]").val());
        purtel2['inhaber'] = encodeURIComponent($.trim($(":input[name=CustomerBankAccountHolderLastname]").val()));
        purtel2['inhaber_adresse'] = encodeURIComponent($("[name=CustomerStreet]").val()+' '+$(":input[name=CustomerStreetno]").val());
        purtel2['inhaber_plz'] = encodeURIComponent($("[name=CustomerZipcode]").val());
        purtel2['inhaber_ort'] = encodeURIComponent($("[name=CustomerCity]").val());
        purtel2['bank'] = encodeURIComponent($("[name=CustomerBankAccountBankname]").val());
        purtel2['BIC'] = encodeURIComponent($.trim($("[name=CustomerBankAccountBicNew]").val()));
        purtel2['IBAN'] = encodeURIComponent($.trim($("[name=CustomerBankAccountIban]").val()));
        purtel2['blz'] = encodeURIComponent($("[name=CustomerBankAccountBic]").val());
        purtel2['kontonr'] = encodeURIComponent($("[name=CustomerBankAccountNumber]").val());
        purtel2['einzug'] = '1';

        if (!purtel2['IBAN'] || /^\s*$/.test(purtel2['IBAN']) ) {
            purtel2['einzug'] = '';
        }

        PurtelIndex = $("[name=PurtelLogin]").val();
        purtel3['CustomerId'] = encodeURIComponent($("#CustomerId").val());
        purtel3['PurtelPorting'] = encodeURIComponent($("[name=PurtelPorting]").val());
        purtel3['PurtelNummer'] = encodeURIComponent($("[name=PurtelNummer]").val());
        purtel3['PurtelTyp'] = encodeURIComponent($("[name=PurtelTyp]").val());
        purtel2['userfield1'] = encodeURIComponent($("[name=CustomerCustomerId]").val());
        purtel2['userfield2'] = encodeURIComponent($("[name=CustomerExternalID2]").val());
        purtel2['userfield4'] = encodeURIComponent($("[name=CustomerBankAccountMandantenid]").val());

        if (PurtelIndex) {
            purtel3['PurtelLogin'] = encodeURIComponent($("[name=PurtelPurtelLogin_"+PurtelIndex+"]").val());  
            purtel3['PurtelPassword'] = encodeURIComponent($("[name=PurtelPurtelPassword_"+PurtelIndex+"]").val());
            
            if (purtelMaster == '') {
                purtel3['PurtelMaster'] = 1;
            } else {
                purtel3['PurtelMaster'] = 0;
            }
        } else {
            purtel3['PurtelLogin'] = '';
            purtel3['PurtelPassword'] = '';
            
            if (purtelMaster == '') {
                purtel3['PurtelMaster'] = 1;
            } else {
                purtel3['PurtelMaster'] = 0;
            }
        }
        
        purtel3['purtelMaster'] = encodeURIComponent(purtelMaster);

        PurtelParams = JSON.stringify(purtel);
        Purtel2Params = JSON.stringify(purtel2);
        Purtel3Params = JSON.stringify(purtel3);
        
        $("#PurtelAccount").css('background-color', '#F78C8C');
        
        $.ajax({
            type: "POST",
            url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxPurtelAccount.php",
            data: "StartPath="+StartPath+"&SelPath="+SelPath+"&StartURL="+StartURL+"&Purtel="+PurtelParams+"&Purtel2="+Purtel2Params+"&Purtel3="+Purtel3Params,
            success: function(data){ajaxPurtelAccountAnswer(data)}
        }); 
    }
}

function ajaxPurtelAccountAnswer(data) 
{
    var response        = $.parseJSON(data);
    var result          = response.result;
    var status          = response.status;
    var status2         = response.status2;
    var status3         = response.status3;
    var purtelLogin     = response.purtelLogin;
    var purtelPassword  = response.purtelPassword;
    var purtelMaster    = response.purtelMaster;
    var purtelPorting   = response.purtelPorting;
    var purtelNummer    = response.purtelNummer;
    var purtelTyp       = response.purtelTyp;
    var purtelInsid     = response.purtelInsid;

    var OptionMaster    = $("[name=PurtelMaster]").html();
    var OptionPorting   = $("[name=PurtelPorting]").html();
    var OptionTyp       = $("[name=PurtelTyp]").html();
    var PurtelHTML;
  
    $("#PurtelAccount").css('background-color', '#EAEAEA');
    if (status == 0) {
        alert ('Kunde konnte nicht bei Purtel angelegt werden! '+result);
    } else {
        $("#command").attr('name', 'save');
        $("#custForm").submit();
    }
    if (status2 == 0) {
        alert ('Purtelergänzungen konnten nicht angelegt werden!'+result);
    }

    if (status3 == 0) {
        alert ('Purtelaccount konnten nicht gespeichert werden!');
    }
}

function purtelPhone(custId) 
{
    var custClientid  = $("[name=CustomerClientid]").val();
    var version       = $("[name=CustomerVersion]").val();
    var use_version   = $("[name=CustomerPurtelUseVersion]").val();

    if (use_version == 1 && version > 0) {
        custClientid = custClientid+'_'+version;
    }
    htmlstr = "Wollen Sie für den Kunden <br /><h3><i>"+$(":input[name=CustomerFirstname]").val()+" "+$(":input[name=CustomerLastname]").val()+" ("+$(":input[name=CustomerClientid]").val()+")</i></h3>die Telefonnummern zuweisen.";
    $("#dialog").html(htmlstr);
    $("#dialog").dialog({
        resizable: true,
        modal: true,
        title: 'Purtel Telefonnummern zuweisen',
        position: {my: 'top', at: 'top', of: '#Container'},
        height: 300,
        width: 500,
        buttons: {
            'Ja': function () {
                $(this).dialog('close');
                ajaxPurtelPhone(true, custId, custClientid);
            },
            'Nein': function () {
                $(this).dialog('close');
                ajaxPurtelPhone(false, custId, custClientid);
            }
        }
    });
}

function ajaxPurtelPhone(save, custId, custClientid)
{
    if (save){
        var StartPath   = $("#StartPath").val();
        var SelPath     = $("#SelPath").val();
        var StartURL    = $("#StartURL").val();

        $("#PurtelPhone").css('background-color', '#F78C8C');
        $.ajax({
            type: "POST",
            url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxPurtelPhone.php",
            data: "StartPath="+StartPath+"&SelPath="+SelPath+"&StartURL="+StartURL+"&CustId="+custId+"&CustClientid="+custClientid,
            success: function(data){ajaxPurtelPhoneAnswer(data)}
        }); 
    }
}

function ajaxPurtelPhoneAnswer(data)
{
    var response  = $.parseJSON(data);
    $("#PurtelPhone").css('background-color', '#EAEAEA');
    $("#command").attr('name', 'save');
    $("#custForm").submit();
}

function purtelChange()
{
    htmlstr = "Wollen Sie für den Kunden <br /><h3><i>"+$(":input[name=CustomerFirstname]").val()+" "+$(":input[name=CustomerLastname]").val()+" ("+$(":input[name=CustomerClientid]").val()+")</i></h3>die Purteldaten ändern.";
    $("#dialog").html(htmlstr);
    $("#dialog").dialog({
        resizable: true,
        modal: true,
        title: 'Purteldaten ändern',
        position: {my: 'top', at: 'top', of: '#Container'},
        height: 300,
        width: 500,
        buttons: {
            'Ja': function () {
                $(this).dialog('close');
                ajaxPurtelChange(true);
            },
            'Nein': function () {
                $(this).dialog('close');
                ajaxPurtelChange(false);
            }
        }
    });
}

function ajaxPurtelChange(save)
{
    if (save){
        var StartPath   = $("#StartPath").val();
        var SelPath     = $("#SelPath").val();
        var StartURL    = $("#StartURL").val();
        var name;
        var value;
        var changes     = false;
        var purtel      = {};

        $("#PurtelChange").css('background-color', '#F78C8C');
        $(".CustPurtelList .changed").each(function(index) {
            name          = $(this).attr('name');
            if ($(this).hasClass('SelPurtelMove')) {
                value       = $('option:selected',this).text();
            } else {
                value       = $(this).val();
            }
            purtel[name]  = value;
            changes       = true;
        });
        if (changes) {
            PurtelParams  = JSON.stringify(purtel);
            $.ajax({
                type: "POST",
                url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxPurtelChange.php",
                data: "StartPath="+StartPath+"&SelPath="+SelPath+"&StartURL="+StartURL+"&Purtel="+PurtelParams,
                success: function(data){ajaxPurtelChangeAnswer(data)}
            });
        } else {
            $("#PurtelChange").css('background-color', '#EAEAEA');
        }

        return false;
    }
}

function ajaxPurtelChangeAnswer(data) 
{
    var response  = $.parseJSON(data);
    if (!response.status) {
        alert ("es ist ein Fehler beim speichern der Purteldaten aufgetreten !\n" + response.sql);
    }
    $("#PurtelChange").css('background-color', '#EAEAEA');
    $("#command").attr('name', 'save');
    $("#custForm").submit();
}

function changePurtelContract() 
{
    var htmlstr = "Wollen Sie für den Kunden <br /><h3><i>"+$(":input[name=CustomerFirstname]").val()+" "+$(":input[name=CustomerLastname]").val()+" ("+$(":input[name=CustomerClientid]").val()+")</i></h3>die Stammdaten des Purtelkundenkontos ändern?";
    $("#dialog").html(htmlstr);
    $("#dialog").dialog({
        resizable: true,
        modal: true,
        title: 'Purtelkundenkonto ändern',
        position: {my: 'top', at: 'top', of: '#Container'},
        height: 300,
        width: 500,
        buttons: {
            'Ja': function () {
                $(this).dialog('close');
                ajaxChangePurtelContract(true);
            },
            'Nein': function () {
                $(this).dialog('close');
                ajaxChangePurtelContract(false);
            }
        }
    });
}

function ajaxChangePurtelContract(save)
{
    if (save){
        var StartPath = $("#StartPath").val();
        var SelPath = $("#SelPath").val();
        var StartURL = $("#StartURL").val();
        var PurtelLogin = $("[name=CustomerPurtelMaster]").val();
        var birthday;
        var str_part;
        var purtel = {};
        var purtelmaster = {};

        purtel['anrede'] = $("[name=CustomerTitle]").val();
        if (purtel['anrede'] == 'Herr') {
            purtel['anrede'] = '1';
        } else {
            purtel['anrede'] = '2';
        }
        purtel['vorname'] = encodeURIComponent($("[name=CustomerFirstname]").val());
        purtel['nachname'] = encodeURIComponent($("[name=CustomerLastname]").val());
        purtel['adresse'] = encodeURIComponent($("[name=CustomerStreet]").val());
        purtel['haus_nr'] = encodeURIComponent($(":input[name=CustomerStreetno]").val());
        purtel['zusatz'] = encodeURIComponent($(":input[name=CustomerStreetnoAddition]").val());

        // To ensure compatibility to old times before the field "StreetnoAddition" existed.
        // Back then where housenumber and addition only in the field "Streetno".
        if ("" == purtel['zusatz']) {
            str_part = encodeURIComponent($(":input[name=CustomerStreetno]").val()).match(/(\d+)(.*)/);

            if ("" != str_part[2]) {
                purtel['haus_nr'] = str_part[1];
                purtel['zusatz'] = str_part[2];
            }
        }

        purtel['plz'] = encodeURIComponent($("[name=CustomerZipcode]").val());
        purtel['ort'] = encodeURIComponent($("[name=CustomerCity]").val());
        purtel['firma'] = encodeURIComponent($("[name=CustomerCompany]").val());
        purtel['version'] = $("[name=CustomerVersion]").val();
        purtel['purtel_use_version'] = $("[name=CustomerPurtelUseVersion]").val();
        
        if (purtel['purtel_use_version'] == 1 && purtel['version'] > 0) {
            purtel['kundennummer_extern'] = $("[name=CustomerClientid]").val()+'_'+purtel['version'];
        } else {
            purtel['kundennummer_extern'] = $("[name=CustomerClientid]").val();
        }

        purtel['geburtsdatum'] = $("[name=CustomerBirthday]").val();
        
        if (purtel['geburtsdatum'] != '') {
            birthday = purtel['geburtsdatum'].split('.');
            purtel['geburtsdatum'] = birthday[2] + birthday[1] + birthday[0];
        }

        purtel['email'] = encodeURIComponent($("[name=CustomerBankAccountEmailaddress]").val());
        
        if (purtel['email'] == '') {
            purtel['email'] = encodeURIComponent($("[name=CustomerEmailaddress]").val());
        }

        purtel['ortsnetz'] = encodeURIComponent($("[name=area_code]").val());
        purtel['telefon'] = encodeURIComponent($("[name=CustomerPhoneareacode]").val()+$(":input[name=CustomerPhonenumber]").val());
        purtel['mobile'] = encodeURIComponent($("[name=CustomerMobilephone]").val());
        purtel['inhaber'] = encodeURIComponent($.trim($(":input[name=CustomerBankAccountHolderLastname]").val()));
        purtel['inhaber_adresse'] = encodeURIComponent($("[name=CustomerStreet]").val()+' '+$(":input[name=CustomerStreetno]").val());
        purtel['inhaber_plz'] = encodeURIComponent($("[name=CustomerZipcode]").val());
        purtel['inhaber_ort'] = encodeURIComponent($("[name=CustomerCity]").val());
        purtel['bank'] = encodeURIComponent($("[name=CustomerBankAccountBankname]").val());
        purtel['BIC'] = encodeURIComponent($.trim($("[name=CustomerBankAccountBicNew]").val()));
        purtel['IBAN'] = encodeURIComponent($.trim($("[name=CustomerBankAccountIban]").val()));
        purtel['blz'] = encodeURIComponent($("[name=CustomerBankAccountBic]").val());
        purtel['kontonr'] = encodeURIComponent($("[name=CustomerBankAccountNumber]").val());
        purtel['einzug'] = '1';

        if (!purtel['IBAN'] || /^\s*$/.test(purtel['IBAN']) ) {
            purtel['einzug'] = '';
        }

        purtelmaster['kundennummer_extern'] = purtel['kundennummer_extern'];
        purtelmaster['userfield1'] = encodeURIComponent($("[name=CustomerCustomerId]").val());
        purtelmaster['userfield2'] = encodeURIComponent($("[name=CustomerExternalID2]").val());
        purtelmaster['userfield4'] = encodeURIComponent($("[name=CustomerBankAccountMandantenid]").val());

        PurtelParams = JSON.stringify(purtel);
        PurtelMaster = JSON.stringify(purtelmaster);

        $("#PurtelContract").css('background-color', '#F78C8C');
        
        $.ajax({
            type: "POST",
            url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxChangePurtelContract.php",
            data: "StartPath="+StartPath+"&SelPath="+SelPath+"&StartURL="+StartURL+"&Purtel="+PurtelParams+"&PurtelMaster="+PurtelMaster+"&PurtelLogin="+PurtelLogin,
            success: function(data){ajaxChangePurtelContractAnswer(data)}
        });
    }
}

function ajaxChangePurtelContractAnswer(data) 
{
    var response = $.parseJSON(data);
    var status = response.status;
    var purtelUrl = response.purtelUrl;
    var result = response.result;
  
    $("#PurtelContract").css('background-color', '#EAEAEA');

    if (status == 0) {
        alert ('Kunde konnte nicht bei Purtel geändert werden! '+result);
    }
}

function resetPurtelAccount() 
{
    htmlstr = "Wollen Sie für den Kunden <br /><h3><i>"+$(":input[name=CustomerFirstname]").val()+" "+$(":input[name=CustomerLastname]").val()+" ("+$(":input[name=CustomerClientid]").val()+")</i></h3>die Purtelkundenkonten zurücksetzten?";
    $("#dialog").html(htmlstr);
    $("#dialog").dialog({
        resizable: true,
        modal: true,
        title: 'Purtelkundenkonto erstellen',
        position: {my: 'top', at: 'top', of: '#Container'},
        height: 300,
        width: 500,
        buttons: {
            'Ja': function () {
                $(this).dialog('close');
                ajaxResetPurtelAccount(true);
            },
            'Nein': function () {
                $(this).dialog('close');
                ajaxResetPurtelAccount(false);
            }
        }
    });
}

function ajaxResetPurtelAccount(save)
{
    if (save){
        var StartPath           = $("#StartPath").val();
        var SelPath             = $("#SelPath").val();
        var StartURL            = $("#StartURL").val();
        var CustomerVersion     = $("[name=CustomerVersion]").val();
        var CustomerUseVersion  = $("[name=CustomerPurtelUseVersion]").val();
        var CustomerExtern      = $(":input[name=CustomerClientid]").val();

        if (CustomerUseVersion == 1 && CustomerVersion > 0) {
            CustomerExtern = CustomerExtern+'_'+CustomerVersion;
        }    
        $("#PurtelReset").css('background-color', '#F78C8C');
        $.ajax({
            type: "POST",
            url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxResetPurtelAccount.php",
            data: "StartPath="+StartPath+"&SelPath="+SelPath+"&StartURL="+StartURL+"&PurtelCustomerExtern="+CustomerExtern,
            success: function(data){ajaxResetPurtelAccountAnswer(data)}
        }); 
    }
}

function ajaxResetPurtelAccountAnswer(data)
{
    var response        = $.parseJSON(data);
    var status          = response.status;
    $("#PurtelReset").css('background-color', '#EAEAEA');
    if (status == 0) {
        alert ('Kundenkonten konnten nicht zurückgesetzt werden!');
    }
}

function createPurtelRadius() 
{
    htmlstr = "Wollen Sie für den Kunden <br /><h3><i>"+$(":input[name=CustomerFirstname]").val()+" "+$(":input[name=CustomerLastname]").val()+" ("+$(":input[name=CustomerClientid]").val()+")</i></h3>einen Radiusaccount erstellen?";
    $("#dialog").html(htmlstr);
    $("#dialog").dialog({
        resizable: true,
        modal: true,
        title: 'Radiusaccount erstellen',
        position: {my: 'top', at: 'top', of: '#Container'},
        height: 300,
        width: 500,
        buttons: {
            'Ja': function () {
                $(this).dialog('close');
                ajaxCreatePurtelRadius(true);
            },
            'Nein': function () {
                $(this).dialog('close');
                ajaxCreatePurtelRadius(false);
            }
        }
    });
}

function ajaxCreatePurtelRadius(save)
{
    if (save){
        var StartPath               = $("#StartPath").val();
        var SelPath                 = $("#SelPath").val();
        var StartURL                = $("#StartURL").val();
        var CustomerPurtelMaster    = $("[name=CustomerPurtelMaster]").val();
        var CustomerId              = $("#CustomerId").val();
        var RadiusUser              = $("#CustomerUser10").val();
        var RadiusPassword          = $("#CustomerPassword10").val();
        
        if (CustomerPurtelMaster) {
            $.ajax({
                type: "POST",
                url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxPurtelRadius.php",
                data: "StartPath="+StartPath+"&SelPath="+SelPath+"&StartURL="+StartURL+"&RadiusCommand=create_radius&PurtelMaster="+CustomerPurtelMaster+"&CustId="+CustomerId+"&RadiusUser="+RadiusUser+"&RadiusPassword="+RadiusPassword,
                success: function(data){ajaxCreatePurtelRadiusAnswer(data)}
            }); 
        }
    }
}

function ajaxCreatePurtelRadiusAnswer(data)
{
    var response = $.parseJSON(data);
    var status = response.status;
    var mes = response.mes;
    if (status == 0) {
        alert ('Radius Account konnte nicht angelegt werden!\n' + mes);
    } else {
        alert('Radius Eintrag erfolgreich erstellt\n' + mes);
    }
}

function updatePurtelRadius() 
{
    htmlstr = "Wollen Sie für den Kunden <br /><h3><i>"+$(":input[name=CustomerFirstname]").val()+" "+$(":input[name=CustomerLastname]").val()+" ("+$(":input[name=CustomerClientid]").val()+")</i></h3>das Passwort ändern?";
    $("#dialog").html(htmlstr);
    $("#dialog").dialog({
        resizable: true,
        modal: true,
        title: 'Radius Passwort ändern',
        position: {my: 'top', at: 'top', of: '#Container'},
        height: 300,
        width: 500,
        buttons: {
            'Ja': function () {
                $(this).dialog('close');
                ajaxUpdatePurtelRadius(true);
            },
            'Nein': function () {
                $(this).dialog('close');
                ajaxUpdatePurtelRadius(false);
            }
        }
    });
}

function ajaxUpdatePurtelRadius(save)
{
    if (save){
        var StartPath               = $("#StartPath").val();
        var SelPath                 = $("#SelPath").val();
        var StartURL                = $("#StartURL").val();
        var CustomerPurtelMaster    = $("[name=CustomerPurtelMaster]").val();
        var CustomerId              = $("#CustomerId").val();
        var RadiusUser              = $("#CustomerUser10").val();
        var RadiusPassword          = $("#CustomerPassword10").val();

        if (CustomerPurtelMaster) {
            $.ajax({
                type: "POST",
                url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxPurtelRadius.php",
                data: "StartPath="+StartPath+"&SelPath="+SelPath+"&StartURL="+StartURL+"&RadiusCommand=update_radius&PurtelMaster="+CustomerPurtelMaster+"&CustId="+CustomerId+"&RadiusUser="+RadiusUser+"&RadiusPassword="+RadiusPassword,
                success: function(data){ajaxUpdatePurtelRadiusAnswer(data)}
            }); 
        }
    }
}

function ajaxUpdatePurtelRadiusAnswer(data)
{
    var response = $.parseJSON(data);
    var status = response.status;
    var mes = response.mes;
    if (status == 0) {
        alert ('Passwort konnte nicht geändert werden!\n' + mes);
    } else {
        alert('Radius Eintrag erfolgreich geändert');
    }
}

function deletePurtelRadius() 
{
    htmlstr = "Wollen Sie für den Kunden <br /><h3><i>"+$(":input[name=CustomerFirstname]").val()+" "+$(":input[name=CustomerLastname]").val()+" ("+$(":input[name=CustomerClientid]").val()+")</i></h3>den Radiusaccount löschen?";
    $("#dialog").html(htmlstr);
    $("#dialog").dialog({
        resizable: true,
        modal: true,
        title: 'Radiusaccount löschen',
        position: {my: 'top', at: 'top', of: '#Container'},
        height: 300,
        width: 500,
        buttons: {
            'Ja': function () {
                $(this).dialog('close');
                ajaxDeletePurtelRadius(true);
            },
            'Nein': function () {
                $(this).dialog('close');
                ajaxDeletePurtelRadius(false);
            }
        }
    });
}

function ajaxDeletePurtelRadius(save)
{
    if (save){
        var StartPath               = $("#StartPath").val();
        var SelPath                 = $("#SelPath").val();
        var StartURL                = $("#StartURL").val();
        var CustomerPurtelMaster    = $("[name=CustomerPurtelMaster]").val();
        var CustomerId              = $("#CustomerId").val();
        var RadiusUser              = $("#CustomerUser10").val();
        var RadiusPassword          = $("#CustomerPassword10").val();

        if (CustomerPurtelMaster) {
            $.ajax({
                type: "POST",
                url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxPurtelRadius.php",
                data: "StartPath="+StartPath+"&SelPath="+SelPath+"&StartURL="+StartURL+"&RadiusCommand=cancel_radius&PurtelMaster="+CustomerPurtelMaster+"&CustId="+CustomerId+"&RadiusUser="+RadiusUser+"&RadiusPassword="+RadiusPassword,
                success: function(data){ajaxDeletePurtelRadiusAnswer(data)}
            }); 
        }
    }
}

function ajaxDeletePurtelRadiusAnswer(data)
{
    var response = $.parseJSON(data);
    var status = response.status;
    var mes = response.mes;
    if (status == 0) {
        alert ('Raduisaccount konnte nicht gelöscht werden!\n' + mes);
    } else {
        alert('Radius Eintrag erfolgreich gelöscht');
    }
}

function ticketForm(ticketSystemType) {
    if (ticketSystemType === 'otrs') {
        openModalOtrsForm();
    } else {
        callGroupware();
    }
}

function openModalOtrsForm() {
    $("#otrs-ticket").dialog({
        resizable: true,
        modal: true,
        title: 'OTRS-Ticket',
        position: {my: 'top', at: 'top', of: '#Container'},
        height: 560,
        width: 700,
        buttons: {
            'Ticket erzeugen': function () {
                $(this).dialog('close');
                ajaxOtrsSaveTicket();
            },
        }
    });

}

function ajaxOtrsSaveTicket()
{
    var otrsAjaxcontrollerUrl = $("#otrs-ajaxcontroller-url").val();
    var postData;
    postData = {};
    postData['internal_customerId'] = $("#CustomerId").val();
    postData['otrs_customerId']          = $("#otrs_ticket_create_otrs_customerId").val();
    postData['subject']             = $("#otrs_ticket_create_subject").val();
    postData['body']                = $("#otrs_ticket_create_body").val();
    postData['queueId']             = $("#otrs_ticket_create_queueId").val();
    postData['ownerId']             = $("#otrs_ticket_create_ownerId").val();
    postData['responsibleId']       = $("#otrs_ticket_create_responsibleId").val();
    //postData['_token']              = $("#otrs_ticket_create__token").val();

    $.ajax({
        type: "POST",
        url: decodeURIComponent(otrsAjaxcontrollerUrl),
        data: postData ,
        success: function(data){
            response = $.parseJSON(data)
            if (response.error === true) {
                alert('Es ist leider ein Fehler beim Erstellen des Tickets aufgetreten! \n\nFehler-Details:\n"' + response.data + '"');
            } else {
                alert('Ticket erfolgreich angelegt! \n\nOTRS-Ticket-Nr: ' + response.data + '');
            }
        },
        error: function(xhr, ajaxOptions, thrownError){ alert('Fehler: ' + xhr +ajaxOptions + thrownError) }
    });
}

function callGroupware() 
{
    htmlstr = "Wollen Sie für den Kunden <br /><h3><i>"+$(":input[name=CustomerFirstname]").val()
        + " "+$(":input[name=CustomerLastname]").val()
        + " ("+$(":input[name=CustomerClientid]").val()+")</i></h3>ein Ticket erzeugen?<br>"
        + "Bitte vergessen Sie nicht, das erzeugte Basisticket innerhalb von<br><b>30 Minuten</b> einer Verfolgungsqueue zuzuordnen, da es sonst automatisch gelöscht wird!";

    $("#dialog").html(htmlstr);
    
    $("#dialog").dialog({
        resizable: true,
        modal: true,
        title: 'Basisticket',
        position: {my: 'top', at: 'top', of: '#Container'},
        height: 300,
        width: 500,
        buttons: {
            'Ja': function () {
                $(this).dialog('close');
                ajaxGroupware(true);
            },
            'Nein': function () {
                $(this).dialog('close');
                ajaxGroupware(false);
            }
        }
    });
}

function ajaxGroupware(save)
{
    if (save){
        var StartPath = $("#StartPath").val();
        var SelPath   = $("#SelPath").val();
        var StartURL  = $("#StartURL").val();
        var custHead  = {};
        var cust      = {};
    
        custHead['id']          = encodeURIComponent($("[name=CustomerId]").val());  
        custHead['clientid']    = encodeURIComponent($("[name=CustomerClientid]").val());  
        custHead['name']        = encodeURIComponent($("[name=CustomerFirstname]").val())+' '+encodeURIComponent($("[name=CustomerLastname]").val());

        cust['Netze']           = encodeURIComponent($("#CustomerNetworkId03 option:selected").val());
        cust['Kunden-Nr.']      = encodeURIComponent($("[name=CustomerClientid]").val());
        cust['Firma']           = encodeURIComponent($("[name=CustomerCompany]").val());
        cust['Kontaktperson']   = encodeURIComponent($("[name=CustomerFirstname]").val())+' '+encodeURIComponent($("[name=CustomerLastname]").val());
        cust['Telefonnummer']   = encodeURIComponent($("[name=CustomerPhoneareacode]").val()+$(":input[name=CustomerPhonenumber]").val());
        cust['Email']           = encodeURIComponent($("[name=CustomerEmailaddress]").val());
        cust['Adresse PLZ']     = encodeURIComponent($("[name=CustomerConnectionZipcode]").val());
        cust['Adresse ORT']     = encodeURIComponent($("[name=CustomerConnectionCity]").val());
        cust['Adresse']         = encodeURIComponent($("[name=CustomerConnectionStreet]").val()+' '+$(":input[name=CustomerConnectionStreetno]").val());
        cust['Mobiltelefon']    = encodeURIComponent($("[name=CustomerMobilephone]").val());

        custHeadParams          = JSON.stringify(custHead);
        custParams              = JSON.stringify(cust);

        winID = window.open('', '_blank');
        $.ajax({
            type: "POST",
            url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxGroupware.php",
            data: "Cust="+custParams+"&CustHead="+custHeadParams,
            success: function(data){ajaxGroupwareAnswer(data, winID)},
            error: function(xhr, ajaxOptions, thrownError){ajaxGroupwareError(xhr, ajaxOptions, thrownError)}
        });
    }
}

function ajaxGroupwareAnswer(data, winID)
{
    try {
        var response = $.parseJSON(data);
    } catch (e) {
        var response = data;
    }

    var url = response.url;
    
    if (url) {
        location.reload(true);
        winID.location.assign(url);
    } else {
        alert ('Es ist kein Groupware Benutzer vorhanden!');
    }
}

function ajaxGroupwareError(xhr, ajaxOptions, thrownError)
{
    htmlstr = "Groupware Antwort<br>"+xhr.status+"<br>"+thrownError;
    $("#dialog").html(htmlstr);
    $("#dialog").dialog({
        resizable: true,
        modal: true,
        title: 'Störungsticket',
        position: {my: 'top', at: 'top', of: '#Container'},
        height: 300,
        width: 500,
        buttons: {
            'OK': function () {
                $(this).dialog('close');
            },
        }
    });
}

function callPing(ip)
{
    var StartPath = $("#StartPath").val();
    var SelPath = $("#SelPath").val();
    var StartURL = $("#StartURL").val();
    var district = $(":input[name=CustomerDistrict]").val();
    var noping = $(":input[name=CustomerNoPing]").val();
    var achtung = '';
    if (ip == undefined || ip == '') {
        ip = $(":input[name=CustomerIpAddress]").val();
    }
    if (district === "Nonnenberg" ) achtung = " => Kunde ist am Nonnenberg IP Adresse überprüfen !!";

    if (ip != '') {
        if (noping != 1) {
            $("#statusval").html("<strong>Status:</strong> pinging..."+achtung);
            $(".fbox").removeClass("gray");
            $(".fbox").removeClass("red");
            $(".fbox").removeClass("yellow");
            $(".fbox").removeClass("green");
            $(".fbox").addClass("yellow");
            $.ajax({
                type: "POST",
                url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxPing.php",
                data: "ip="+ip,
                success: function(data){ajaxPingAnswer(data)}
            }); 
        } else {
            $("#statusval").empty();
            $(".fbox").removeClass("gray");
            $(".fbox").removeClass("red");
            $(".fbox").removeClass("yellow");
            $(".fbox").removeClass("green");
            $(".fbox").addClass("gray");
        }
    }
}

function ajaxPingAnswer(data)
{
    var response  = $.parseJSON(data);
    var ret       = response.ret;
    var district  = $(":input[name=CustomerDistrict]").val();
    var achtung   = '';  
    if (district === "Nonnenberg" ) achtung = " => Kunde ist am Nonnenberg IP Adresse überprüfen !!";
  
    if (ret === 0) {
        $("#statusval").empty();
        $("#statusval").html("<strong>Status:</strong> up"+achtung);
        $(".fbox").removeClass("gray");
        $(".fbox").removeClass("red");
        $(".fbox").removeClass("yellow");
        $(".fbox").removeClass("green");
        $(".fbox").addClass("green");
    } else {
        $("#statusval").empty();
        $("#statusval").html("<strong>Status:</strong> down"+achtung);
        $(".fbox").removeClass("gray");
        $(".fbox").removeClass("red");
        $(".fbox").removeClass("yellow");
        $(".fbox").removeClass("green");
        $(".fbox").addClass("red");
    }
}

function callPurtelRadiusStatus()
{
    var StartPath               = $("#StartPath").val();
    var SelPath                 = $("#SelPath").val();
    var StartURL                = $("#StartURL").val();
    var CustomerPurtelMaster    = $("[name=CustomerPurtelMaster]").val();
    var CustomerId              = $("[name=CustomerClientid]").val();
    var RadiusUser              = $("#CustomerUser10").val();
    var RadiusPassword          = $("#CustomerPassword10").val();

    $.ajax({
        type: "POST",
        url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxPurtelRadiusStatus.php",
        data: "StartPath="+StartPath+"&SelPath="+SelPath+"&StartURL="+StartURL+"&CustomerPurtelMaster="+CustomerPurtelMaster+"&CustId="+CustomerId+"&RadiusUser="+RadiusUser+"&RadiusPassword="+RadiusPassword,
        success: function(data){ajaxPurtelRadiusStatusAnswer(data)}
    }); 
}

function ajaxPurtelRadiusStatusAnswer(data)
{
    var response = $.parseJSON(data);
    var status = response.status;
    var mes = response.mes;
    var ip = response.ip;
    if (status == false) {
        $(".radiustext").html("<strong>Radius Fehler:</strong> "+mes);
        $(".radius").removeClass("gray");
        $(".radius").removeClass("red");
        $(".radius").removeClass("yellow");
        $(".radius").removeClass("green");
        $(".radius").addClass("red");
    } else {
        $(".radiustext").html("<strong>Radius IP-Adresse:</strong> "+ip+"  <span id='statusval'></span>");
        callPing(ip);
    }
}

function acsManage(action, type)
{
    var acs = {};

    acs['clientId']   = $("#CustomerClientid10").val();
    acs['macAddress'] = $("#CustomerMacAddress10").val();
    acs['version']    = $("#CustomerVersion").val();
    acs['area_code']  = $("#area_code").val();
    acs['firstname']  = $(":input[name=CustomerFirstname]").val();
    acs['lastname']   = $(":input[name=CustomerLastname]").val();
  
    switch (action) {
        case 'activate':
            actiontxt = 'die ACS-Registrierung durchführen';
            break;
        case 'modify':
            actiontxt = 'die ACS-Registrierung ändern';
            break;
        case 'deactivate':
            actiontxt = 'die ACS-Registrierung deaktivieren';
            break;
        case 'reactivate':
            actiontxt = 'die ACS-Registrierung aktivieren';
            break;
        case 'delete':
            actiontxt = 'die ACS-Registrierung löschen';
            break;
        default:
            actiontxt = 'keine Aktion durchführen';
            break;
    }
  
    htmlstr = "Wollen Sie für den Kunden <br /><h3><i>"+acs['firstname']+" ";
    htmlstr += acs['lastname']+" ("+acs['clientId'];
    htmlstr += ")</i></h3>"+actiontxt+"<hr />";
    htmlstr += "Version: "+acs['version']+"<br />";
    htmlstr += "MAC-Adresse: "+acs['macAddress']+"<br />";
    htmlstr += "Vorwahl: "+acs['area_code'];
  
    $("#dialog").html(htmlstr);
    $("#dialog").dialog({
        resizable: true,
        modal: true,
        title: 'Purtelkundenkonto erstellen',
        position: {my: 'top', at: 'top', of: '#Container'},
        height: 300,
        width: 500,
        buttons: {
            'Ja': function () {
                $(this).dialog('close');
                ajaxAcsManage(true, action, type);
            },
            'Nein': function () {
                $(this).dialog('close');
                ajaxAcsManage(false, action, type);
            }
        }
    });
}

function ajaxAcsManage(save, action, type)
{
    if (save) {
        var StartPath = $("#StartPath").val();
        var SelPath   = $("#SelPath").val();
        var StartURL  = $("#StartURL").val();
        var acs       = {};

        acs['custId']     = $("#CustomerId").val();
        acs['clientId']   = $("#CustomerClientid10").val();
        acs['macAddress'] = $("#CustomerMacAddress10").val();
        acs['password']   = encodeURIComponent($("#CustomerPassword10").val());
        acs['version']    = $("#CustomerVersion").val();
        acs['area_code']  = $("#area_code").val();

        acsParams     = JSON.stringify(acs);
        $.ajax({
            type:     "POST",
            url:      decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxAcs.php",
            data:     "StartPath="+StartPath+"&SelPath="+SelPath+"&StartURL="+StartURL+"&action="+action+"&type="+type+"&acs="+acsParams,
            success:  function(data){ajaxAcsManageResult(data)}
        });
    }
}

function ajaxAcsManageResult(data)
{
    var response  = $.parseJSON(data);
    var status    = response.status;
    var info      = response.info;
  
    if (status == 0) {
        $("#dialog").html(info);
        $("#dialog").dialog({
            resizable: true,
            modal: true,
            title: 'ACS Management',
            position: {my: 'top', at: 'top', of: '#Container'},
            height: 300,
            width: 500,
            buttons: {
                'OK': function () {
                    $(this).dialog('close');
                },
            }
        });
    } else {
        alert (info);  
    }
}

function SwitchProcedure()
{
    $("#area").attr('value', 'SwitchProcedure');
    $(".CustProcedure").show();
    $(".CustStatus").hide();
    $(".MailForm").hide();
    $(".CustMasterData").hide();
    $(".CustConnection").hide();
    $(".CustAccounting").hide();
    $(".CustInkasso").hide();
    $(".CustContractPrivate").hide();
    $(".CustContractBusiness").hide();
    $(".CustContractOptions").hide();
    $(".CustContractOptionsNew").hide();
    $(".CustOldContract").hide();
    $(".CustTal").hide();
    $(".CustWbci").hide();
    $(".CustVentelo").hide();
    $(".CustPurtel").hide();
    $(".CustPurtelAccount").hide();
    $(".CustVlan").hide();
    $(".CustTechData").hide();
    $(".CustTechConf").hide();
    $(".CustProgress").hide();
    $(".CustProgressSave").hide();
    $(".CustConnectingRequest").hide();
    $(".CustAbrogate").hide();
    $(".CustSave").hide();
    $(".CustVersion").hide();
}

function SwitchStatus()
{
    callPing();
    callPurtelRadiusStatus();
    $("#area").attr('value', 'SwitchStatus');
    $(".CustProcedure").hide();
    $(".CustStatus").show();
    $(".MailForm").hide();
    $(".CustMasterData").hide();
    $(".CustConnection").hide();
    $(".CustAccounting").hide();
    $(".CustInkasso").hide();
    $(".CustContractPrivate").hide();
    $(".CustContractBusiness").hide();
    $(".CustContractOptions").hide();
    $(".CustContractOptionsNew").hide();
    $(".CustOldContract").hide();
    $(".CustTal").hide();
    $(".CustWbci").hide();
    $(".CustVentelo").hide();
    $(".CustPurtel").hide();
    $(".CustPurtelAccount").hide();
    $(".CustVlan").hide();
    $(".CustTechData").hide();
    $(".CustTechConf").hide();
    $(".CustProgress").hide();
    $(".CustProgressSave").hide();
    $(".CustConnectingRequest").hide();
    $(".CustAbrogate").hide();
    $(".CustSave").hide();
    $(".CustVersion").show();
}

function SwitchMasterData()
{
    $("#area").attr('value', 'SwitchMasterData');
    $(".CustProcedure").hide();
    $(".CustStatus").hide();
    $(".MailForm").hide();
    $(".CustMasterData").show();
    $(".CustConnection").show();
    $(".CustAccounting").show();
    $(".CustInkasso").show();
    $(".CustContractPrivate").hide();
    $(".CustContractBusiness").hide();
    $(".CustContractOptions").hide();
    $(".CustContractOptionsNew").hide();
    $(".CustOldContract").hide();
    $(".CustTal").hide();
    $(".CustWbci").hide();
    $(".CustVentelo").hide();
    $(".CustPurtel").hide();
    $(".CustPurtelAccount").hide();
    $(".CustVlan").hide();
    $(".CustTechData").hide();
    $(".CustTechConf").hide();
    $(".CustProgress").hide();
    $(".CustProgressSave").hide();
    $(".CustConnectingRequest").hide();
    $(".CustAbrogate").hide();
    $(".CustSave").show();
    $(".CustVersion").hide();
}

function SwitchContract()
{
    $("#area").attr('value', 'SwitchContract');
    $(".CustProcedure").hide();
    $(".CustStatus").hide();
    $(".MailForm").hide();
    $(".CustMasterData").hide();
    $(".CustConnection").hide();
    $(".CustAccounting").hide();
    $(".CustInkasso").hide();
    if ($(":input[name=CustomerClienttype]").val() == 'commercial') {
        $(".CustContractPrivate").hide();
        $(".CustContractBusiness").show();
        $(".CustContractOptions").show();
        $(".CustContractOptionsNew").show();
    } else {
        $(".CustContractPrivate").show();
        $(".CustContractBusiness").hide();
        $(".CustContractOptions").show();
        $(".CustContractOptionsNew").show();
    }
    $(".CustOldContract").hide();
    $(".CustTal").hide();
    $(".CustWbci").hide();
    $(".CustVentelo").hide();
    $(".CustPurtel").hide();
    $(".CustPurtelAccount").hide();
    $(".CustVlan").hide();
    $(".CustTechData").hide();
    $(".CustTechConf").hide();
    $(".CustProgress").hide();
    $(".CustProgressSave").hide();
    $(".CustConnectingRequest").hide();
    $(".CustAbrogate").hide();
    $(".CustSave").show();
    $(".CustVersion").hide();
}

function SwitchOldContract()
{
    $("#area").attr('value', 'SwitchOldContract');
    $(".CustProcedure").hide();
    $(".CustStatus").hide();
    $(".MailForm").hide();
    $(".CustMasterData").hide();
    $(".CustConnection").hide();
    $(".CustAccounting").hide();
    $(".CustInkasso").hide();
    $(".CustContractPrivate").hide();
    $(".CustContractBusiness").hide();
    $(".CustContractOptions").hide();
    $(".CustContractOptionsNew").hide();
    $(".CustOldContract").show();
    $(".CustTal").hide();
    $(".CustWbci").hide();
    $(".CustVentelo").hide();
    $(".CustPurtel").hide();
    $(".CustPurtelAccount").hide();
    $(".CustVlan").hide();
    $(".CustTechData").hide();
    $(".CustTechConf").hide();
    $(".CustProgress").hide();
    $(".CustProgressSave").hide();
    $(".CustConnectingRequest").hide();
    $(".CustAbrogate").hide();
    $(".CustSave").show();
    $(".CustVersion").hide();
}
  
function SwitchTal()
{
    $("#area").attr('value', 'SwitchTal');
    $(".CustProcedure").hide();
    $(".CustStatus").hide();
    $(".MailForm").hide();
    $(".CustMasterData").hide();
    $(".CustConnection").hide();
    $(".CustAccounting").hide();
    $(".CustInkasso").hide();
    $(".CustContractPrivate").hide();
    $(".CustContractBusiness").hide();
    $(".CustContractOptions").hide();
    $(".CustContractOptionsNew").hide();
    $(".CustOldContract").hide();
    $(".CustTal").show();
    $(".CustWbci").hide();
    $(".CustVentelo").hide();
    $(".CustPurtel").hide();
    $(".CustPurtelAccount").hide();
    $(".CustVlan").hide();
    $(".CustTechData").hide();
    $(".CustTechConf").hide();
    $(".CustProgress").hide();
    $(".CustProgressSave").hide();
    $(".CustConnectingRequest").hide();
    $(".CustAbrogate").hide();
    $(".CustSave").show();
    $(".CustVersion").hide();
}
  
function SwitchWbci()
{
    $("#area").attr('value', 'SwitchWbci');
    $(".CustProcedure").hide();
    $(".CustStatus").hide();
    $(".MailForm").hide();
    $(".CustMasterData").hide();
    $(".CustConnection").hide();
    $(".CustAccounting").hide();
    $(".CustInkasso").hide();
    $(".CustContractPrivate").hide();
    $(".CustContractBusiness").hide();
    $(".CustContractOptions").hide();
    $(".CustContractOptionsNew").hide();
    $(".CustOldContract").hide();
    $(".CustTal").hide();
    $(".CustWbci").show();
    $(".CustVentelo").show();
    $(".CustPurtel").hide();
    $(".CustPurtelAccount").hide();
    $(".CustVlan").hide();
    $(".CustTechData").hide();
    $(".CustTechConf").hide();
    $(".CustProgress").hide();
    $(".CustProgressSave").hide();
    $(".CustConnectingRequest").hide();
    $(".CustAbrogate").hide();
    $(".CustSave").show();
    $(".CustVersion").hide();
}

function SwitchPurtel()
{
    $("#area").attr('value', 'SwitchPurtel');
    $(".CustProcedure").hide();
    $(".CustStatus").hide();
    $(".MailForm").hide();
    $(".CustMasterData").hide();
    $(".CustConnection").hide();
    $(".CustAccounting").hide();
    $(".CustInkasso").hide();
    $(".CustContractPrivate").hide();
    $(".CustContractBusiness").hide();
    $(".CustContractOptions").hide();
    $(".CustContractOptionsNew").hide();
    $(".CustOldContract").hide();
    $(".CustTal").hide();
    $(".CustWbci").hide();
    $(".CustVentelo").hide();
    $(".CustPurtel").show();
    $(".CustPurtelAccount").show();
    $(".CustVlan").hide();
    $(".CustTechData").hide();
    $(".CustTechConf").hide();
    $(".CustProgress").hide();
    $(".CustProgressSave").hide();
    $(".CustConnectingRequest").hide();
    $(".CustAbrogate").hide();
    $(".CustSave").show();
    $(".CustVersion").hide();
}

function SwitchVlan()
{
    $("#area").attr('value', 'SwitchVlan');
    $(".CustProcedure").hide();
    $(".CustStatus").hide();
    $(".MailForm").hide();
    $(".CustMasterData").hide();
    $(".CustConnection").hide();
    $(".CustAccounting").hide();
    $(".CustInkasso").hide();
    $(".CustContractPrivate").hide();
    $(".CustContractBusiness").hide();
    $(".CustContractOptions").hide();
    $(".CustContractOptionsNew").hide();
    $(".CustOldContract").hide();
    $(".CustTal").hide();
    $(".CustWbci").hide();
    $(".CustVentelo").hide();
    $(".CustPurtel").hide();
    $(".CustPurtelAccount").hide();
    $(".CustVlan").show();
    $(".CustTechData").hide();
    $(".CustTechConf").hide();
    $(".CustProgress").hide();
    $(".CustProgressSave").hide();
    $(".CustConnectingRequest").hide();
    $(".CustAbrogate").hide();
    $(".CustSave").show();
    $(".CustVersion").hide();
}

function SwitchTechData()
{
    $("#area").attr('value', 'SwitchTechData');
    $(".CustProcedure").hide();
    $(".CustStatus").hide();
    $(".MailForm").hide();
    $(".CustMasterData").hide();
    $(".CustConnection").hide();
    $(".CustAccounting").hide();
    $(".CustInkasso").hide();
    $(".CustContractPrivate").hide();
    $(".CustContractBusiness").hide();
    $(".CustContractOptions").hide();
    $(".CustContractOptionsNew").hide();
    $(".CustOldContract").hide();
    $(".CustTal").hide();
    $(".CustWbci").hide();
    $(".CustVentelo").hide();
    $(".CustPurtel").hide();
    $(".CustPurtelAccount").hide();
    $(".CustVlan").hide();
    $(".CustTechData").show();
    $(".CustTechConf").hide();
    $(".CustProgress").hide();
    $(".CustProgressSave").hide();
    $(".CustConnectingRequest").hide();
    $(".CustAbrogate").hide();
    $(".CustSave").show();
    $(".CustVersion").hide();
}
  
function SwitchTechConf()
{
    $("#area").attr('value', 'SwitchTechConf');
    $(".CustProcedure").hide();
    $(".CustStatus").hide();
    $(".MailForm").hide();
    $(".CustMasterData").hide();
    $(".CustConnection").hide();
    $(".CustAccounting").hide();
    $(".CustInkasso").hide();
    $(".CustContractPrivate").hide();
    $(".CustContractBusiness").hide();
    $(".CustContractOptions").hide();
    $(".CustContractOptionsNew").hide();
    $(".CustOldContract").hide();
    $(".CustTal").hide();
    $(".CustWbci").hide();
    $(".CustVentelo").hide();
    $(".CustPurtel").hide();
    $(".CustPurtelAccount").hide();
    $(".CustVlan").hide();
    $(".CustTechData").hide();
    $(".CustTechConf").show();
    $(".CustProgress").hide();
    $(".CustProgressSave").hide();
    $(".CustConnectingRequest").hide();
    $(".CustAbrogate").hide();
    $(".CustSave").hide();
    $(".CustVersion").hide();
}

function SwitchProgress()
{
    $("#area").attr('value', 'SwitchProgress');
    $(".CustProcedure").hide();
    $(".CustStatus").hide();
    $(".MailForm").hide();
    $(".CustMasterData").hide();
    $(".CustConnection").hide();
    $(".CustAccounting").hide();
    $(".CustInkasso").hide();
    $(".CustContractPrivate").hide();
    $(".CustContractBusiness").hide();
    $(".CustOldContract").hide();
    $(".CustContractOptions").hide();
    $(".CustContractOptionsNew").hide();
    $(".CustTal").hide();
    $(".CustWbci").hide();
    $(".CustVentelo").hide();
    $(".CustPurtel").hide();
    $(".CustPurtelAccount").hide();
    $(".CustVlan").hide();
    $(".CustTechData").hide();
    $(".CustTechConf").hide();
    $(".CustProgress").show();
    $(".CustProgressSave").show();
    $(".CustConnectingRequest").hide();
    $(".CustAbrogate").hide();
    $(".CustSave").hide();
    $(".CustVersion").hide();
}

function SwitchConnectingRequest()
{
    $("#area").attr('value', 'SwitchConnectingRequest');
    $(".CustProcedure").hide();
    $(".CustStatus").hide();
    $(".MailForm").hide();
    $(".CustMasterData").hide();
    $(".CustConnection").hide();
    $(".CustAccounting").hide();
    $(".CustInkasso").hide();
    $(".CustContractPrivate").hide();
    $(".CustContractBusiness").hide();
    $(".CustContractOptions").hide();
    $(".CustContractOptionsNew").hide();
    $(".CustOldContract").hide();
    $(".CustTal").hide();
    $(".CustWbci").hide();
    $(".CustVentelo").hide();
    $(".CustPurtel").hide();
    $(".CustPurtelAccount").hide();
    $(".CustVlan").hide();
    $(".CustTechData").hide();
    $(".CustTechConf").hide();
    $(".CustProgress").hide();
    $(".CustProgressSave").hide();
    $(".CustConnectingRequest").show();
    $(".CustAbrogate").hide();
    $(".CustSave").hide();
    $(".CustVersion").hide();
    ajaxConnectingRequest();
}

function SwitchAbrogate()
{
    $("#area").attr('value', 'SwitchAbrogate');
    $(".CustProcedure").hide();
    $(".CustStatus").hide();
    $(".MailForm").hide();
    $(".CustMasterData").hide();
    $(".CustConnection").hide();
    $(".CustAccounting").hide();
    $(".CustInkasso").hide();
    $(".CustContractPrivate").hide();
    $(".CustContractBusiness").hide();
    $(".CustContractOptions").hide();
    $(".CustContractOptionsNew").hide();
    $(".CustOldContract").hide();
    $(".CustTal").hide();
    $(".CustWbci").hide();
    $(".CustVentelo").hide();
    $(".CustPurtel").hide();
    $(".CustPurtelAccount").hide();
    $(".CustVlan").hide();
    $(".CustTechData").hide();
    $(".CustTechConf").hide();
    $(".CustProgress").hide();
    $(".CustProgressSave").hide();
    $(".CustConnectingRequest").hide();
    $(".CustAbrogate").show();
    $(".CustSave").show();
    $(".CustVersion").hide();
    ajaxConnectingRequest();
}

function SwitchInterest()
{
    $("#Button").hide();
    $(".CustProcedure").hide();
    $(".CustStatus").hide();
    $(".MailForm").hide();
    $(".CustMasterData").show();
    $(".CustConnection").show();
    $(".CustAccounting").hide();
    $(".CustInkasso").hide();
    $(".CustProgress").hide();
    $(".CustContractPrivate").hide();
    $(".CustContractBusiness").hide();
    $(".CustContractOptions").hide();
    $(".CustContractOptionsNew").hide();
    $(".CustOldContract").hide();
    $(".CustTal").hide();
    $(".CustWbci").hide();
    $(".CustVentelo").hide();
    $(".CustPurtel").hide();
    $(".CustPurtelAccount").hide();
    $(".CustVlan").hide();
    $(".CustTechData").hide();
    $(".CustTechConf").hide();
    $(".CustProgress").hide();
    $(".CustProgressSave").hide();
    $(".CustConnectingRequest").hide();
    $(".CustAbrogate").hide();
    $(".CustSave").show();
    $(".CustVersion").hide();
}

function SwitchNewCustomer()
{
    $("#Button").hide();
    $(".CustProcedure").hide();
    $(".CustStatus").hide();
    $(".MailForm").hide();
    $(".CustMasterData").show();
    $(".CustConnection").show();
    $(".CustAccounting").show();
    $(".CustInkasso").hide();
    if ($(":input[name=CustomerClienttype]").val() == 'commercial') {
        $(".CustContractPrivate").hide();
        $(".CustContractBusiness").show();
        $(".CustContractOptions").show();
        $(".CustContractOptionsNew").show();
    } else {
        $(".CustContractPrivate").show();
        $(".CustContractBusiness").hide();
        $(".CustContractOptions").show();
        $(".CustContractOptionsNew").show();
    }
    $(".CustOldContract").hide();
    $(".CustTal").hide();
    $(".CustWbci").hide();
    $(".CustVentelo").hide();
    $(".CustPurtel").hide();
    $(".CustPurtelAccount").hide();
    $(".CustVlan").hide();
    $(".CustTechData").hide();
    $(".CustTechConf").hide();
    $(".CustProgress").hide();
    $(".CustProgressSave").hide();
    $(".CustConnectingRequest").hide();
    $(".CustAbrogate").hide();
    $(".CustSave").show();
    $(".CustVersion").hide();

    $(":input[name=CustomerClienttype]").change (function(){
        if ($(":input[name=CustomerClienttype]").val() == 'commercial') {
            $(".CustContractPrivate").hide();
            $(".CustContractBusiness").show();
            $(".CustContractOptions").show();
            $(".CustContractOptionsNew").show();
        } else {
            $(".CustContractPrivate").show();
            $(".CustContractBusiness").hide();
            $(".CustContractOptions").show();
            $(".CustContractOptionsNew").show();
        }
    });
}

function SwitchCustomer()
{
    $("#Button").show();
    $("#Button h4").replaceWith('<h4>'+$(":input[name=CustomerClientid]").val())+'</h4>';
    var area = $("#area").val();
    if (area == '') {
        area = 'SwitchStatus';
    }
    area = area+'()';
    eval(area);
}

/**
 * JavaScript part to "multiple ipAddresses"
 */
var MultipleIpAddresses = {
    /**
     * Keep track of added ipAddresses.
     * Start with 2 because 1 will always be shown.
     *
     * @member {integer}
     */
    count: 2,

    /**
     * Cache ajax-responses
     *
     * @member {object}
     */
    cache: {},

    /**
     * Init add-ip-button functionality
     *
     * @return void
     */
    init: function()
    {
        if (typeof MultipleIpAddressesCount == 'undefined') {
            return;
        }

        MultipleIpAddresses.count = MultipleIpAddressesCount;

        if (MultipleIpAddresses.count > 2) {
            for (var i = 2; i < MultipleIpAddresses.count; i++) {
                var outerElement = $('#multiple-ip-'+i);

                outerElement.find('.customer-ip-remove').each(function() {
                    $(this).on('click', MultipleIpAddresses.removeIpAddress);
                });

                outerElement.find('select[name^=customer-ip-range-id]').each(function() {
                    $(this).on('change', function() {
                        MultipleIpAddresses.fetchIpRangeDefaults(
                            $(this).closest('div[id^=multiple-ip-]').find('select[name^=customer-ip-address]')
                        )
                    });
                });
            }
        }
        
        $('.add-ip-button').each(function() {
            $(this).on('click', MultipleIpAddresses.addIpAddress);
        });
    },

    /**
     * Add new ipAddress field to HTML
     *
     * @return false
     */
    addIpAddress: function()
    {
        var StartURL = decodeURIComponent($("#StartURL").val());

        var outerDiv = $('<div id="multiple-ip-'+MultipleIpAddresses.count+'"></div>');
        
        var headline = $('<span class="double"><br />\
            <h4>IP-Adresse </h4>\
            </span>'
        );

        var removeThisBlockLink = $('<button type="button" class="customer-ip-remove">\
            <img alt="" src="'+StartURL+'/_img/delete.png" height="12px"> löschen</button>');
        removeThisBlockLink.on('click', MultipleIpAddresses.removeIpAddress);


        var ipAddressRangeSelect = $('<select id="customer-ip-range-id['+MultipleIpAddresses.count+']" \
            name="customer-ip-range-id['+MultipleIpAddresses.count+']"></select>');

        var ipAddressRangeField = $('<span class="double">\
            <label for="customer-ip-range-id['+MultipleIpAddresses.count+']">IP-Block</label><br /></span>'
        ).append(ipAddressRangeSelect);


        var ipAddressSelect = $('<select class="customer-ip-select" id="customer-ip-address['+MultipleIpAddresses.count+']" \
            name="customer-ip-address['+MultipleIpAddresses.count+']"></select>').on('change', function() {
                MultipleTerminals.ipAddressPool.update();
        });

        var ipAddressField = $('<span class="double">\
            <label for="customer-ip-address['+MultipleIpAddresses.count+']">IP Adresse</label><br /></span>'
        ).append(ipAddressSelect);


        var subnetmaskField = $('<span class="double">\
            <label for="customer-subnetmask['+MultipleIpAddresses.count+']">Subnetzmaske</label><br />\
            <input type="text" id="customer-subnetmask['+MultipleIpAddresses.count+']" value="" \
            name="customer-subnetmask['+MultipleIpAddresses.count+']">\
            </span>'
        );

        var gatewayField = $('<span class="double">\
            <label for="customer-gateway['+MultipleIpAddresses.count+']">Gateway</label><br />\
            <input type="text" id="customer-gateway['+MultipleIpAddresses.count+']" value="" \
            name="customer-gateway['+MultipleIpAddresses.count+']">\
            </span>'
        );

        var rDNSField = $('<span class="double">\
            <label for="customer-rdns['+MultipleIpAddresses.count+']">rDNS</label><br />\
            <input type="text" id="customer-rdns['+MultipleIpAddresses.count+']" value="" \
            name="customer-rdns['+MultipleIpAddresses.count+']">\
            </span>'
        );

        headline.find('h4').append(removeThisBlockLink);
        
        outerDiv.append(headline);
        outerDiv.append(ipAddressRangeField);
        outerDiv.append(ipAddressField);
        outerDiv.append(subnetmaskField);
        outerDiv.append(gatewayField);
        outerDiv.append(rDNSField);

        $('#ip-content-wrap').append(outerDiv);

        MultipleIpAddresses.count++;

        MultipleIpAddresses.fetchIpRange(ipAddressRangeSelect);
        MultipleIpAddresses.fetchIpRangeDefaults(ipAddressSelect);

        ipAddressRangeSelect.on('change', function() {
            MultipleIpAddresses.fetchIpRangeDefaults(
                $(this).closest('div[id^=multiple-ip-]').find('select[name^=customer-ip-address]')
            )
        });

        return false;
    },

    /**
     * Cache a variable
     *
     * @param {mixed} key
     * @param {mixed} value
     *
     * @return void
     */
    cacheVar: function(key, value)
    {
        MultipleIpAddresses.cache[key] = value;
    },

    /**
     * Return cached variable if found, null otherwise
     *
     * @param {mixed} key
     *
     * @return {object|null}
     */
    getCachedVarOrNull: function(key)
    {
        if (undefined != MultipleIpAddresses.cache[key]) {
            return MultipleIpAddresses.cache[key];
        }

        return null;
    },

    /**
     * Fills ipAddress-select with content
     *
     * @param {object} selectElement
     *
     * @return void
     */
    fetchIpRangeDefaults: function(selectElement)
    {
        var addIpAddressesToSelect = function(ipRangeDefaults)
        {
            var ipAddresses = ipRangeDefaults.ipAddresses;
            var html = '<option value=""></option>';

            for (var i in ipAddresses) {
                html += '<option value="'+ipAddresses[i]+'">'+ipAddresses[i]+'</option>';
            }

            selectElement.html(html);

            $(selectElement).closest('div[id^=multiple-ip-]')
                .find('input[name^=customer-subnetmask]').val(ipRangeDefaults.subnetmask);
        };

        var networkId = $(":input[name=CustomerNetworkId]").val();
        var ipRangeId = $(selectElement).closest('div[id^=multiple-ip-]').find('select[name^=customer-ip-range-id]').val();

        var ipRangeDefaults = MultipleIpAddresses.getCachedVarOrNull('ipRangeDefaults.'+networkId+'.'+ipRangeId);

        if (null != ipRangeDefaults) {
            addIpAddressesToSelect(ipRangeDefaults);

            return;
        }

        var SelPath = $("#SelPath").val();
        var StartURL = $("#StartURL").val();
        
        $.ajax({
            type: "GET",
            url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxIpAddresses.php",
            data: "network-id="+networkId+"&range-id="+ipRangeId+"&action=get-ip-range-defaults",
            success: function(response){
                response = $.parseJSON(response);
                
                addIpAddressesToSelect(response);

                MultipleIpAddresses.cacheVar('ipRangeDefaults.'+networkId+'.'+ipRangeId, response);
            }
        });
    },

    /**
     * Fills ipRange-select with content
     *
     * @param {object} selectElement
     *
     * @return void
     */
    fetchIpRange: function(selectElement)
    {
        /*
         * Load data live via ajax. If ever needed!
         *
            var SelPath = $("#SelPath").val();
            var StartURL = $("#StartURL").val();
            var NetworkId = $(":input[name=CustomerNetworkId]").val();

            $.ajax({
                type: "GET",
                url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxIpAddresses.php",
                data: "network-id="+NetworkId+"&action=get-ip-range",
                success: function(response){
                    response = $.parseJSON(response);
                    var html = '';

                    for (var ipRangeId in response) {
                        html += '<option value="'+response[ipRangeId].id+'">'+response[ipRangeId].name+' \
                            ('+response[ipRangeId].startIpAddress+' - '+response[ipRangeId].endIpAddress+')</option>'
                        ;
                    }

                    selectElement.html(html);
                }
            });
        */

        // for now, we can just copy
        selectElement.html($('#CustomerIpRangeId10').html());
    },

    /**
     * Removes ipAddress field from HTML
     *
     * @return false
     */
    removeIpAddress: function()
    {
        if (window.confirm("Wollen Sie diese Ip-Adresse wirklich vom Kunde entfernen?\n(Wird erst duch 'Speicher' übernommen)")) {
            $(this).closest('div[id^=multiple-ip-]').remove();
        }

        return false;
    },
};

/**
 * JavaScript part to "multiple terminals"
 */
var MultipleTerminals = {
    /**
     * Keep track of added terminals.
     * Start with 2 because 1 will always be shown.
     *
     * @member {integer}
     */
    count: 2,

    /**
     * Init add-terminal-button functionality
     *
     * @return void
     */
    init: function()
    {
        if (typeof MultipleTerminalsCount == 'undefined') {
            return;
        }

        MultipleTerminals.count = MultipleTerminalsCount;

        if (MultipleTerminals.count > 2) {
            for (var i = 2; i < MultipleTerminals.count; i++) {
                var outerElement = $('#multiple-terminal-'+i);

                outerElement.find('.customer-terminal-remove').each(function() {
                    $(this).on('click', MultipleTerminals.removeTerminal);
                });
            }
        }

        $('.add-terminal-button').each(function() {
            $(this).on('click', MultipleTerminals.addTerminal);
        });

        $('#CustomerIpAddress10').on('change', function () {
            MultipleTerminals.ipAddressPool.update();
        });

        MultipleTerminals.ipAddressPool.update();
    },

    /**
     *
     */
    ipAddressPool: {
        /**
         * Update ipAddress-selects for terminals
         *
         * @return void
         */
        update: function()
        {
            var selectedIpAddresses = [];

            $('.customer-ip-select').each(function () {
                if ($(this).val() != '') {
                    selectedIpAddresses.push($(this).val());

                    $(this).find('option:selected').each(function () {
                        var option = $(this);
                        option.prependTo(option.parent());
                    });
                }
            });

            $('.customer-terminal-ip-select').each(function () {
                var terminalSelectedIp = $(this).val();

                var html = '<option value=""></option>';

                for (var i = 0; i < selectedIpAddresses.length; i++) {
                    var ip = selectedIpAddresses[i];

                    var selected = '';

                    if (ip === terminalSelectedIp) {
                        selected = ' selected="selected"';
                    }

                    html += '<option value="'+ip+'"'+selected+'>'+ip+'</option>';
                }

                $(this).html(html);
            });
        }
    },

    /**
     * Add new terminal field to HTML
     *
     * @return false
     */
    addTerminal: function()
    {
        var StartURL = decodeURIComponent($("#StartURL").val());

        var outerDiv = $('<div id="multiple-terminal-'+MultipleTerminals.count+'"></div>');
        
        var headline = $('<span class="double"><br />\
            <h4>Endgerät </h4>\
            </span>'
        );

        var removeThisBlockLink = $('<button type="button" class="customer-terminal-remove">\
            <img alt="" src="'+StartURL+'/_img/delete.png" height="12px"> löschen</button>');
        removeThisBlockLink.on('click', MultipleTerminals.removeTerminal);


        var typeSelect = $('<select id="customer-terminal-type['+MultipleTerminals.count+']" \
            name="customer-terminal-type['+MultipleTerminals.count+']"></select>');

        typeSelect.html($('#CustomerTerminalType10').html());

        typeSelect.find('option').each(function () {
            if (typeof $(this).attr('selected') != 'undefined') {
                $(this).removeAttr('selected')
            }
        });
        

        var typeField = $('<span>\
            <label for="customer-terminal-type['+MultipleTerminals.count+']">Endgerät Typ</label><br /></span>'
        ).append(typeSelect);

        var ipAddressSelect = $('<select class="customer-terminal-ip-select" id="customer-terminal-ip['+MultipleTerminals.count+']" \
            name="customer-terminal-ip['+MultipleTerminals.count+']"></select>');

        var ipAddressField = $('<span>\
            <label for="customer-terminal-ip['+MultipleTerminals.count+']">IP-Adresse</label><br /></span>'
        ).append(ipAddressSelect);

        var macAddressField = $('<span class="double">\
            <label for="customer-terminal-macaddress['+MultipleTerminals.count+']">Mac Adresse</label><br />\
            <input type="text" id="customer-terminal-macaddress['+MultipleTerminals.count+']" value="" \
            name="customer-terminal-macaddress['+MultipleTerminals.count+']">\
            </span>'
        );

        var serialnumberField = $('<span class="double">\
            <label for="customer-terminal-serialnumber['+MultipleTerminals.count+']">Endgerät Seriennummer</label><br />\
            <input type="text" id="customer-terminal-serialnumber['+MultipleTerminals.count+']" value="" \
            name="customer-terminal-serialnumber['+MultipleTerminals.count+']">\
            </span>'
        );

        var remoteUserField = $('<span>\
            <label for="customer-terminal-remote-user['+MultipleTerminals.count+']">Remote User</label><br />\
            <input type="text" id="customer-terminal-remote-user['+MultipleTerminals.count+']" value="" \
            name="customer-terminal-remote-user['+MultipleTerminals.count+']">\
            </span>'
        );

        var remotePasswordField = $('<span>\
            <label for="customer-terminal-remote-password['+MultipleTerminals.count+']">Remote Passwort</label><br />\
            <input type="text" id="customer-terminal-remote-password['+MultipleTerminals.count+']" value="" \
            name="customer-terminal-remote-password['+MultipleTerminals.count+']">\
            </span>'
        );

        var remotePortField = $('<span>\
            <label for="customer-terminal-remote-port['+MultipleTerminals.count+']">Remote Port</label><br />\
            <input type="text" id="customer-terminal-remote-port['+MultipleTerminals.count+']" value="" \
            name="customer-terminal-remote-port['+MultipleTerminals.count+']">\
            </span>'
        );

        var activationDate = $('<span>\
            <label for="customer-terminal-activation-date['+MultipleTerminals.count+']">Aktiv seit</label><br />\
            <input type="text" class="datePicker" id="customer-terminal-activation-date['+MultipleTerminals.count+']" value="" \
            name="customer-terminal-activation-date['+MultipleTerminals.count+']">\
            </span>'
        );

        var tvIdField = $('<span>\
            <label for="customer-terminal-tv-id['+MultipleTerminals.count+']">Smartcard-ID</label><br />\
            <input type="text" id="customer-terminal-tv-id['+MultipleTerminals.count+']" value="" \
            name="customer-terminal-tv-id['+MultipleTerminals.count+']">\
            </span>'
        );

        var checkIdField = $('<span>\
            <label for="customer-terminal-check-id['+MultipleTerminals.count+']">Chip-ID</label><br />\
            <input type="text" id="customer-terminal-check-id['+MultipleTerminals.count+']" value="" \
            name="customer-terminal-check-id['+MultipleTerminals.count+']">\
            </span>'
        );

        headline.find('h4').append(removeThisBlockLink);
        
        outerDiv.append(headline);
        outerDiv.append(typeField);
        outerDiv.append(ipAddressField);
        outerDiv.append(macAddressField);
        outerDiv.append(serialnumberField);
        outerDiv.append(remoteUserField);
        outerDiv.append(remotePasswordField);
        outerDiv.append(remotePortField);
        outerDiv.append(activationDate);
        outerDiv.append(tvIdField);
        outerDiv.append(checkIdField);

        $('#terminals-content-wrap').append(outerDiv);

        MultipleTerminals.ipAddressPool.update();

        MultipleTerminals.count++;

        return false;
    },

    /**
     * Removes terminal field from HTML
     *
     * @return false
     */
    removeTerminal: function()
    {
        if (window.confirm("Wollen Sie dieses Endgerät wirklich vom Kunde entfernen?\n(Wird erst duch 'Speicher' übernommen)")) {
            $(this).closest('div[id^=multiple-terminal-]').remove();
        }

        return false;
    },
};

/**
 * Add filter for listed customer history events
 */
var ProgressFilter = {
    /**
     * Constructor
     */
    init: function ()
    {
        var prototype = $('#customer-history-event').html();

        prototype = $('<label for="progress-filter-select">Nur ausgew&auml;hlte anzeigen</label><select id="progress-filter-select">'+prototype+'</select><br /><br />');
        prototype.on('change', function () {
            var _this = $(this);
            var eventId = $(this).val();

            if (typeof eventId == 'undefined' || null == eventId.match(/\d+/) || eventId == '') {
                $('#progress-list-wrap div[data-event]').each(function () {
                    $(this).removeClass('hide');
                });
            } else {
                $('#progress-list-wrap div[data-event]').each(function () {
                    if ($(this).data('event') == eventId) {
                        $(this).removeClass('hide');
                    } else if (!$(this).hasClass('hide')) {
                        $(this).addClass('hide');
                    }
                });
            }
        });

        $('#progress-filter-wrap').append(prototype);
    },
};

/**
 * Edit customer progress items
 */
var EditProgressItem = {
    /**
     * Cache
     */
    cache: null,

    /**
     * Constructor
     */
    init: function ()
    {
        $('.button-edit-history-item').on('click', function (event) {
            event.preventDefault();

            var _this = $(this);
            var outerElement = _this.closest('div[data-event]');
            var eventId = outerElement.data('event');
            var prototype = $('#customer-history-event').html();
            var historyItemId = outerElement.find('[data-item-id]').data('item-id');
            var text = outerElement.find('.history-item-text').html();
            text = text.replace(/\r/g, '').replace(/\n/g, '').replace(/<br \/>/g, "\n").replace(/<br>/g, "\n").replace(/<br\/>/g, "\n");

            EditProgressItem.cache = outerElement.html();

            var editBlock = $('<div></div>');
            editBlock.append('<input type="hidden" name="edit-history-item-id" value="'+historyItemId+'" />'
                +'<select name="edit-history-item-event" id="edit-history-item-event-select">'+prototype+'</select><br /><br />'
                +'<textarea name="edit-history-item-text">'
                +text+'</textarea><br />'
            );

            var buttonsWrap = $('<div class="clear-float"></div>');

            var saveButton = $('<button class="fancy-button green right" type="submit">Speichern</button>');
            saveButton.on('click', function (event) {
                event.preventDefault();

                var eventId = $('#edit-history-item-event-select').val();

                if (typeof eventId == 'undefined' || null == eventId.match(/\d+/) || eventId == '') {
                    window.alert('Bitte wählen Sie eine Aktion aus.');
                    return false;
                }

                custSubmit();
            });

            var abbordButton = $('<button type="button" class="fancy-button red right">Abbrechen</button>');
            abbordButton.on('click', function (event) {
                event.preventDefault();

                outerElement.html(EditProgressItem.cache);
                EditProgressItem.cache = null;
                EditProgressItem.init();
            });

            buttonsWrap.append(saveButton);
            buttonsWrap.append(abbordButton);
            editBlock.append(buttonsWrap);
            editBlock.append('<hr /><br />');

            outerElement.html('');
            outerElement.append(editBlock);

            $('#edit-history-item-event-select').find('option').each(function () {
                if (eventId == $(this).val()) {
                    $(this).attr('selected', 'selected');

                    return null;
                }
            });
        });
    },
};

//******************************************************************************
$(document).ready(function()
{
    MultipleIpAddresses.init();
    MultipleTerminals.init();
    ProgressFilter.init();
    EditProgressItem.init();

    if ($("#Customer").length != 0)     {SwitchCustomer()}
    if ($("#NewCustomer").length != 0)  {SwitchNewCustomer()}
    if ($("#Interest").length != 0)     {SwitchInterest()}
  
    $("#ButProcedure").click(function()         {SwitchProcedure();});
    $("#ButStatus").click(function()            {SwitchStatus();});
    $("#ButMasterData").click(function()        {SwitchMasterData()});
    $("#ButContract").click(function()          {SwitchContract()});
    $("#ButOldContract").click(function()       {SwitchOldContract()});
    $("#ButTal").click(function()               {SwitchTal()});
    $("#ButWbci").click(function()              {SwitchWbci()});
    $("#ButPurtel").click(function()            {SwitchPurtel()});
    $("#ButTechData").click(function()          {SwitchTechData()});
    $("#ButTechConf").click(function()          {SwitchTechConf()});
    $("#ButProgress").click(function()          {SwitchProgress()});
    $("#ButConnectingRequest").click(function() {SwitchConnectingRequest()});
    $("#ButAbrogate").click(function()          {SwitchAbrogate()});
    $("#ButVlan").click(function()              {SwitchVlan()});

    $("#custForm").validate({
        submitHandler: function(form)
        {
            htmlstr = "<table><tr><th>Feld</th><th>alter Wert</th><th>neuer Wert</th></tr>";
            field = '';
            $(".changed").each(function(index)
            {
                if ($(this).data('oldval')!= undefined) {

                    label = $("label[for="+$(this).attr("id")+"]").first().text();
                    if (label == '') {
                        label = $(this).attr('name');
                    }
                   field = field+$(this).attr('name')+'|';
                    if ($(this).data('newtext')) {
                        htmlstr = htmlstr+"<tr><td>"+label+"</td><td>"+$(this).data('oldtext')+"</td><td>"+$(this).data('newtext')+"</td></tr>";
                    } else {
                        htmlstr = htmlstr+"<tr><td>"+label+"</td><td>"+$(this).data('oldval')+"</td><td>"+$(this).data('newval')+"</td></tr>";
                    }
                }
            });
            $("#changed_fields").attr('value', field);
            htmlstr = htmlstr+"</table>";
            $("#dialog").html(htmlstr);
            $("#dialog").dialog({
                resizable: true,
                modal: true,
                title: 'Kundendaten speichern',
                position: {my: 'top', at: 'top', of: '#Container'},
                height: 550,
                width: 1024,
                buttons: {
                    'Ja': function ()
                    {
                        $(this).dialog('close');
                        if ($("#command").attr('name') === 'none') {
                            $("#command").attr('name', 'save');
                        }
                        form.submit();
                    },
                    'Nein': function ()
                    {
                        $(this).dialog('close');
                    }
                }
            });
        }
    });
  
    $(":input").focus(function ()
    {
        if ($(this).data('oldval') == undefined) {
            $(this).data('oldval', $(this).val());

            if (null == $(this).val()) {
                var hasSelectedOption = $(this).find('option:selected');

                if (hasSelectedOption.length > 0) {
                    $(this).data('oldval', hasSelectedOption.val());
                }
            }
            
            if ($(this).children(":selected").text()) {
                $(this).data('oldtext', $(this).children(":selected").text());
            }
        }
    });
    
    $(":input[name=CustomerPurtelAdminChoiceNoSubmit]").change(function()
    {
        if(this.selectedIndex != 0) window.open($(this).val(), '_blank');
        this.selectedIndex = 0;
    });

    $(":input[name=CustomerNetworkId]").change(function ()
    {
        ajaxNetwork();
    });
    
    $(":input[name=EMailTemplate]").change(function ()
    {
        var StartPath = $("#StartPath").val();
        var SelPath = $("#SelPath").val();
        var StartURL = $("#StartURL").val();
        var mail = $(this).val().split('####');

        $("div.MailForm").empty();
        $(".MailForm").show();

        MailForm =  '<p><label for="MailAddress">Senden an:</label><br /><input id="MailAddress" type="text" name="MailAddress" value="'+mail[0]+'"></p>';
        MailForm += '<p><label for="MailSubject">Betreff:</label><br /><input id="MailSubject" type="text" name="MailSubject" value="'+mail[1]+'"></p>';
        MailForm += '<p><label for="MailBody">Nachricht:</label><br /><textarea id="MailBody" name="MailBody">'+mail[2]+'</textarea></p>';
        MailForm += '<div id="MailCont" class="clearfix">';
        MailForm += '<div id="MailFileQueue"></div>';

        switch (mail[1]) {
            case "Vertragsunterlagen für DSL DaheimInternet von "+COMPANY_NAME+" Energie AG":
                MailForm += '<div id="MailStdPdf">';
                MailForm += '<p><b>Dateianhang</b></p>';
                MailForm += '<p>'+COMPANY_NAME+'_DaheimInternet_Produktflyer_DSL.pdf</p>';
                MailForm += '<p>'+COMPANY_NAME+'_Formulare_AGBs_mit_Widerruf.pdf</p>';
                MailForm += '<p>'+COMPANY_NAME+'_Formulare_DaheimInternet_DSL_Online.pdf</p>';

                break;

            case "Vertragsunterlagen für Glasfaser DaheimInternet von "+COMPANY_NAME+" Energie AG":
                MailForm += '<div id="MailStdPdf">';
                MailForm += '<p><b>Dateianhang</b></p>';
                MailForm += '<p>'+COMPANY_NAME+'_DaheimInternet_Produktflyer_Glasfaser.pdf</p>';
                MailForm += '<p>'+COMPANY_NAME+'_Formulare_AGBs_mit_Widerruf.pdf</p>';
                MailForm += '<p>'+COMPANY_NAME+'_Formulare_DaheimInternet_Glasfaser_Online.pdf</p>';

                break;

            case "Vertragseingangsbestätigung DaheimInternet der BADEN.NET":
                MailForm += '<div id="MailStdPdf">';
                MailForm += '<p><b>Dateianhang</b></p>';
                MailForm += '<p>Willkommen_SWB.pdf</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';

                break;

            case "Auftragsformular und AGB BADEN.NET":
                MailForm += '<div id="MailStdPdf">';
                MailForm += '<p><b>Dateianhang</b></p>';
                MailForm += '<p>Auftrag_Privatkunde_BADEN-NET.pdf</p>';
                MailForm += '<p>Baden_Net_AGBs.pdf</p>';
                MailForm += '<p>Ergaenzende-Geschaeftsbedingungen-Privatkunden_VoIP.pdf</p>';
                MailForm += '<p>Ergaenzende_Geschaeftsbedingungen_Baden.Net-TV.pdf</p>';
                MailForm += '<p>Flyer Baden.Net.pdf</p>';
                MailForm += '<p>&nbsp;</p>';

                break;

            case "Fehlendes SEPA Lastschriftmandat BADEN.NET":
                MailForm += '<div id="MailStdPdf">';
                MailForm += '<p><b>Dateianhang</b></p>';
                MailForm += '<p>SEPA-Mandat_Baden.net.pdf</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';

                break;

            case "Zahlung per Rechnung nicht möglich":
                MailForm += '<div id="MailStdPdf">';
                MailForm += '<p><b>Dateianhang</b></p>';
                MailForm += '<p>SEPA-Mandat_Baden.net.pdf</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';

                break;

            case "Daten für Telefonbucheintrag":
                MailForm += '<div id="MailStdPdf">';
                MailForm += '<p><b>Dateianhang</b></p>';
                MailForm += '<p>Telefonbucheintrag_Formular_leer_ausfüllbar.pdf</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';

                break;

            case "Willkommen bei BADEN.NET":
                MailForm += '<div id="MailStdPdf">';
                MailForm += '<p><b>Dateianhang</b></p>';
                MailForm += '<p>Baden_Net_AGBs.pdf</p>';
                MailForm += '<p>Ergaenzende-Geschaeftsbedingungen-Privatkunden_VoIP.pdf</p>';
                MailForm += '<p>Ergaenzende_Geschaeftsbedingungen_Baden.Net-TV.pdf</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';

                break;

            case  "Willkommen bei BADEN.NET IP-TV (Buchung nachträglich)":
                MailForm += '<div id="MailStdPdf">';
                MailForm += '<p><b>Dateianhang</b></p>';
                MailForm += '<p>Ergaenzende_Geschaeftsbedingungen_Baden.Net-TV.pdf</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';
                MailForm += '<p>&nbsp;</p>';

                break;
        }

        MailForm += '</div>';
        MailForm += '<p><input id="MailFiles" name="MailFiles" type="file" multiple="true" />&nbsp;&nbsp;';
        MailForm += '<button name="MailFileload" onclick="custMailFileLoad()" type="button"><img alt="" src="'+decodeURIComponent(StartURL)+'/_img/Upload.png" height="12px"> Anhang hochladen</button></p>';
        MailForm += '<p><button name="sendmail" onclick="custMailSend()" type="button"><img alt="" src="'+decodeURIComponent(StartURL)+'/_img/E-mail.png" height="12px"> Senden</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        MailForm += '<button name="mailreset" onclick="custMailReset()" type="button"><img alt="" src="'+decodeURIComponent(StartURL)+'/_img/Erase.png" height="12px"> Schließen</button></p>';
        $("div.MailForm").append(MailForm);
        // window.location = $(this).val();
        //
        $('#MailFiles').uploadifive ({
            'auto':           false,
            'width':          120,
            'height':         20,
            'buttonText':     'Dateiauswahl',
            'queueID':        'MailFileQueue',
            'uploadScript':    decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+'/ajaxMailFiles.php',
            'formData': {
                'StartPath':   $("#StartPath").val(),
                'SelPath':     $("#SelPath").val(),
                'CustomerId':  $(":input[name=CustomerId]").val(),
            },
            'onUploadComplete':  function (file, data)
            {
                var response    = $.parseJSON(data);
                $(this).addClass('test');
                alert ('File: '+response.name);
            }
        });
    });

    $(":input").change(function () 
    {
        var tax = 1.19;
        var upper;
        var prodID;
        var priceNet;
        var priceGross;
                
        if ($(this).attr("name") != 'CustomerPurtelAdminChoiceNoSubmit' &&
            $(this).attr("name") != 'EMailTemplate' && 
            $(this).attr("name") != 'CustomerOptions[]' &&
            $(this).attr("name") != 'bsmSelectbsmContainer0') {

            if ($(this).hasClass('CustomerOptionPriceNet')) {
                priceNet = $(this).val().replace(',','.') * 100;
                priceGross = priceNet * tax;
                priceNet = priceNet / 100;
                priceGross = priceGross / 100;
                priceNet = priceNet.toFixed(2);
                priceGross = priceGross.toFixed(2);
                prodID = $(this).attr("name").split('_')[1];
                $(this).val(priceNet);
                $(":input[name=CustomerOptionPriceGross_"+prodID+"]").val(priceGross);
            }

            if ($(this).hasClass('CustomerOptionPriceGross')) {
                priceGross = $(this).val().replace(',','.') * 100;
                priceNet = priceGross / tax;
                priceGross = priceGross / 100;
                priceNet = priceNet / 100;
                priceGross = priceGross.toFixed(2);
                priceNet = priceNet.toFixed(2);
                prodID = $(this).attr("name").split('_')[1];
                $(":input[name=CustomerOptionPriceNet_"+prodID+"]").val(priceNet);
                $(this).val(priceGross);
            }

            if ($(this).hasClass('uppercase')) {
                upper = $(this).val();
                upper = $(this).val().toUpperCase();
                $(this).val($(this).val().toUpperCase());
            }

            if ($(this).data('oldval') == undefined) {
                $(this).data('oldval', $(this).prop('defaultValue'));
                if ($(this).children(":selected").text()) {
                    $(this).data('oldtext', $(this).children(":selected").text());
                }
            }

            $(this).data('newval', $(this).val());
            if ($(this).children(":selected").text()) {
                $(this).data('newtext', $(this).children(":selected").text());
            }

            if ($(":input[name=PurtelLogin]").val() === '') {
                $("#PurtelAccount").html("Neuer Purtelaccount");     
            } else {
                $("#PurtelAccount").html("Neue Telefonnummer");     
            }

            $(":input[name="+$(this).attr("name")+"]").css('background-color', '#F7F9B6');
            $(":input[name="+$(this).attr("name")+"]").css('border-color', '#089606');
            $(":input[name="+$(this).attr("name")+"]").addClass('changed');
            $(":input[name="+$(this).attr("name")+"]").val($(this).data('newval'));
        }
    });

    (function () {
        $('.clone-element').each(function () {
            var parentElementId = $(this).data('element-id');
            var parentElement = $('#'+parentElementId);
            
            if (parentElement.length == 1) {
                var clonedElement = parentElement.clone();
                clonedElement.removeAttr('id');

                switch (clonedElement.prop('nodeName').toLowerCase()) {
                    case 'select':
                        clonedElement.on('change', function () {
                            parentElement.data('oldval', parentElement.val());
                            parentElement.data('oldtext', parentElement.children(":selected").text());

                            parentElement.val(clonedElement.val());
                            /*var __this = $(this);

                            parentElement.find('option').each(function () {
                                $(this).prop('selected', false);
                                $(this).removeAttr('selected');
                                $(this).removeProp('selected');
                                
                                if ($(this).val() === __this.val()) {
                                    $(this).prop('selected', 'selected');
                                    $(this).attr('selected', 'selected');
                                }
                            });*/

                            parentElement.trigger('change');
                        });

                        break;
                }
                
                $(this).before(clonedElement);
            }
        });

        $('.delete-purtel-account-link').each(function () {
            $(this).on('click', function (event) {
                var message = "Wollen Sie den Purtel-Account wirklich löschen?\n\nWichtig!\nDer Account muss manuell bei Purtel gelöscht werden!!!\n\n";

                if (!window.confirm(message)) {
                    event.preventDefault();
                }
            });
        });
    })();
});
