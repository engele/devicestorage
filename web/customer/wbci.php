<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_PROVIDER_CHANGE');

$disable = 'readonly="readonly"';
$rw = false;   

if ($authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_PROVIDER_CHANGE')) {
    $disable = '';   
    $rw = true;   
}

?>
<h3 class="CustWbci">WBCI</h3>
<div class="CustWbci form">
  <span class="double">
    <label for="CustomerClientid15"><?php echo \Wisotel\Configuration\Configuration::get('companyName'); ?> Nr.</label><br />
    <input type="text" id="CustomerClientid15" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'clientid'); ?>" readonly="readonly" name="CustomerClientid">
  </span>
  <span>
    <label for="CustomerWbciEkpauf15">Aufnehmender EKP</label><br />
    <input type="hidden" id="CustomerWbciEkpaufId15" value="<?php echo $wbci_ekpauf_id; ?>" readonly="readonly" name="CustomerWbciEkpaufId">
    <input type="text" id="CustomerWbciEkpauf15" value="<?php echo $wbci_ekpauf; ?>" readonly="readonly" name="CustomerWbciEkpauf">
  </span>
  <span>
    <label for="CustomerWbciEkpabg15">Abgebender EKP</label><br />
    <select id="CustomerWbciEkpabg15" name="CustomerWbciEkpabg" <?php echo $disable; ?>>
      <?php setOption ($wbci_ekp_option, $customer, 'wbci_ekpabg'); ?>
    </select>
  </span>

  <span>
    <label for="CustomerOldcontractTitle15">Anrede</label><br />
    <select id="CustomerOldcontractTitle15" name="CustomerOldcontractTitle" <?php echo $disable; ?>>
      <?php setOption ($option_oldcontract_title, $customer, 'oldcontract_title'); ?>
    </select>
  </span>
  <span></span>
  <span class="double">
    <label for="CustomerWbciGf15">Geschäftsfall</label><br />
    <select id="CustomerWbciGf15" name="CustomerWbciGf" <?php echo $disable; ?>>
      <?php setOption ($wbci_gf_option, $customer, 'wbci_gf'); ?>
    </select>
  </span>

  <span>
    <label for="CustomerOldcontractFirstname15">Vorname</label><br />
    <input type="text" id="CustomerOldcontractFirstname15" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'oldcontract_firstname'); ?>" name="CustomerOldcontractFirstname">
  </span>
  <span>
    <label for="CustomerOldcontractLastname15">Nachname</label><br />
    <input type="text" id="CustomerOldcontractLastname15" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'oldcontract_lastname'); ?>" name="CustomerOldcontractLastname">
  </span>
  <span>
    <label for="CustomerVenteloPortWishDate15">Portierungswunschdatum</label><br />
    <input type="text" id="CustomerVenteloPortWishDate15" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'ventelo_port_wish_date'); ?>" class="datepicker" name="CustomerVenteloPortWishDate">
  </span>
  <span>
    <label for="CustomerWbciId15">WBCI ID</label><br />
    <input type="text" id="CustomerWbciId15" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wbci_id'); ?>" name="CustomerWbciId">
  </span>
  
  <span>
    <label for="CustomerOldcontractStreet15">Straße</label><br />
    <input type="text" id="CustomerOldcontractStreet15" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'oldcontract_street'); ?>" name="CustomerOldcontractStreet">
  </span>
  <span>
    <label for="CustomerOldcontractStreetno15">Hausnummer</label><br />
    <input type="text" id="CustomerOldcontractStreetno15" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'oldcontract_streetno'); ?>" name="CustomerOldcontractStreetno">
  </span>
  <span>
    <label for="CustomerWbciEingestelltAm15">WBCI eingestellt am</label><br />
    <input type="text" id="CustomerWbciEingestelltAm15" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wbci_eingestellt_am'); ?>" class="datepicker" name="CustomerWbciEingestelltAm">
  </span>
  <span>
    <label for="CustomerWbciEingestelltVon15">WBCI eingestellt von</label><br />
    <select id="CustomerWbciEingestelltVon15" name="CustomerWbciEingestelltVon" <?php echo $disable; ?>>
      <?php setOption ($option_purtel_edit_done_from, $customer, 'wbci_eingestellt_von'); ?>
    </select>
  </span>

  <span>
    <label for="CustomerOldcontractZipcode15">PLZ</label><br />
    <input type="text" id="CustomerOldcontractZipcode15" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'oldcontract_zipcode'); ?>" name="CustomerOldcontractZipcode">
  </span>
  <span>
    <label for="CustomerOldcontractCity15">Stadt</label><br />
    <input type="text" id="CustomerOldcontractCity15" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'oldcontract_city'); ?>" name="CustomerOldcontractCity">
  </span>
  <span>
    <label for="CustomerWbciRuemvaAm15">RUEM-VA erhalten am</label><br />
    <input type="text" id="CustomerWbciRuemvaAm15" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wbci_ruemva_am'); ?>" class="datepicker" name="CustomerWbciRuemvaAm">
  </span>
  <span>
    <label for="CustomerWbciRuemvaVon15">RUEM-VA bearbeitet von</label><br />
    <select id="CustomerWbciRuemvaVon15" <?php echo $disable; ?> name="CustomerWbciRuemvaVon">
      <?php setOption ($option_purtel_edit_done_from, $customer, 'wbci_ruemva_von'); ?>
    </select>
  </span>

  <span class="double">
    <label for="CustomerWbciComment15">Kommentar (WBCI)</label>
    <textarea id="CustomerWbciComment15" name="CustomerWbciComment" <?php echo $disable; ?>><?php setInputValue ($customer, 'wbci_comment'); ?></textarea>
  </span>
  <span class="doublebox">
    <span>
      <label for="CustomerWbciBestaetigtAm15">WBCI bestätigt zum</label><br />
      <input type="text" id="CustomerWbciBestaetigtAm15" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wbci_bestaetigt_am'); ?>" class="datepicker" name="CustomerWbciBestaetigtAm">
    </span>
    <span>
      <label for="CustomerWbciBestaetigtVon15">WBCI Best. bearbeitet von</label><br />
      <select id="CustomerWbciBestaetigtVon15" name="CustomerWbciBestaetigtVon" <?php echo $disable; ?>>
        <?php setOption ($option_purtel_edit_done_from, $customer, 'wbci_bestaetigt_von'); ?>
      </select>
    </span>
    <span>
      <label for="CustomerWbciAkmtrAm15">AKM-TR gesendet am</label><br />
      <input type="text" id="CustomerWbciAkmtrAm15" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wbci_akmtr_am'); ?>" class="datepicker" name="CustomerWbciAkmtrAm">
    </span>
    <span>
      <label for="CustomerWbciAkmtrVon15">AKM-TR gesendet von</label><br />
      <select id="CustomerWbciAkmtrVon15" name="CustomerWbciAkmtrVon" <?php echo $disable; ?>>
        <?php setOption ($option_purtel_edit_done_from, $customer, 'wbci_akmtr_von'); ?>
      </select>
    </span>
    <span>
      <label for="CustomerWbciAkmtr15">AKM-TR gesetzt auf</label><br />
      <select id="CustomerWbciAkmtr15" name="CustomerWbciAkmtr" <?php echo $disable; ?>>
        <?php setOption ($option_wbci_akmtr, $customer, 'wbci_akmtr'); ?>
      </select>
    </span>
  </span>

  <hr>
<?php
  foreach ($purtel_account as $key => $value) {
    if ($value['porting'] == 2) {
      echo "<span>\n";
      echo "<label for='PurtelNummer15_".$key."'>Telefonnummer</label>\n";
      echo "<input type='text' $disable id='PurtelNummer15_".$key."' value='\n";
      setInputValue ($value, 'nummer');
      echo "' name='PurtelNummer_".$key."' readonly='readonly'>\n";
      echo "</span>\n";
    }
  }
?> 
</div>
