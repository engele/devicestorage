<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_CONTRACT');

$disable = 'readonly="readonly"';
$rw = false;   

if ($authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_CONTRACT')) {
    $disable = '';   
    $rw = true;   
}

?>
<h3 class="CustContractPrivate">Vertragsdaten (Privatkunden)</h3>
<div class="CustContractPrivate form">
  <span class="double">
    <label for="CustomerClientid06"><?php echo \Wisotel\Configuration\Configuration::get('companyName'); ?> Nr.</label><br />
    <input id="CustomerClientid06" type="text" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'clientid'); ?>" maxlength="64" readonly="readonly" name="CustomerClientid" />    
  </span>
  <span>
    <label for="CustomerCreditRatingDate06">Boni-prüfung am</label><br />
    <input id="CustomerCreditRatingDate06" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'credit_rating_date'); ?>" name="CustomerCreditRatingDate" />
  </span>
  <span>
    <label for="CustomerCreditRatingOk06">Boni-prüfung Status</label><br />
    <select id="CustomerCreditRatingOk06" name="CustomerCreditRatingOk" <?php echo $disable; ?>>
      <?php setOption ($option_credit_rating_ok, $customer, 'credit_rating_ok'); ?>
    </select>
  </span>
  <?php
  $externalCustomerIdFieldName = 'Externe Kundenkennung';
  
  if (null !== $exCustField = \Wisotel\Configuration\Configuration::get('externalCustomerIdFieldName')) {
    $externalCustomerIdFieldName = $exCustField;
  }
  ?>
  <span class="double">
    <label for="CustomerCustomerId06"><?php echo $externalCustomerIdFieldName; ?></label><br />
    <input id="CustomerCustomerId06" type="text" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'customer_id'); ?>" maxlength="64" name="CustomerCustomerId" />    
  </span>
  <span>
    <label for="CustomerCustomerElectrisity06">Stromkunde</label><br />
    <select id="CustomerCustomerElectrisity06" name="CustomerCustomerElectrisity" <?php echo $disable; ?>>
      <?php setOption ($option_electrisity, $customer, 'customer_electrisity'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerCustomerElectrisityEnddate06">Stromvertrag endet am</label><br />
    <input id="CustomerCustomerElectrisityEnddate06" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'customer_electrisity_enddate'); ?>"  maxlength="11"  name="CustomerCustomerElectrisityEnddate" />
  </span>
  
  <span>
    <label for="CustomerNetworkId06">Netz</label><br />
    <select id="CustomerNetworkId06" name="CustomerNetworkId" <?php echo $disable; ?>>
      <?php setOption ($networks, $customer, 'network_id'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerClienttype06">Kundentyp *</label><br />
    <select id="CustomerClienttype06" name="CustomerClienttype" disabled>
      <?php setOption ($option_clienttype, $customer, 'clienttype'); ?>
    </select>
  </span>
  <span class="double"><br /><h4>Produktdetails</h4></span>
  
  <span>
    <label for="CustomerRegistrationDate06">Interessent seit</label><br />
    <input id="CustomerRegistrationDate06" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'registration_date'); ?>" maxlength="11" name="CustomerRegistrationDate">
  </span>
  <span>
    <label for="CustomerConnectionWishDate06"><b>Anschalt Wunschtermin</b></label><br />
    <input id="CustomerConnectionWishDate06" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'connection_wish_date'); ?>" maxlength="11" name="CustomerConnectionWishDate">
  </span>
  <span class="double">
    <label for="CustomerProductname06">Produktname *</label><br />
    <select id="CustomerProductname06" name="CustomerProductname" <?php echo $disable; ?>>
        <option value=""></option>

        <?php
        $groupedMainProducts = [];

        foreach ($mainProducts as $mainProduct) {
            $category = null !== $mainProduct->getCategory() ? $mainProduct->getCategory()->getId() : 'uncategorized';
            $groupedMainProducts[$category][] = $mainProduct;
        }

        foreach ($groupedMainProducts as $categoryId => $mainProducts_) {
            $categoryHasActiveProducts = false;

            foreach ($mainProducts_ as $mainProduct) {
                if ($mainProduct->getActive()) {
                    $categoryHasActiveProducts = true;

                    break;
                }
            }

            if (!$categoryHasActiveProducts) {
                $customerProductNameCategory = null !== $customer['productname']->getCategory() ? $customer['productname']->getCategory()->getId() : 'uncategorized';

                if ($customerProductNameCategory !== $categoryId) {
                    continue;
                }
            }

            $categoryName = null !== $mainProducts_[0]->getCategory() ? $mainProducts_[0]->getCategory()->getName() : 'unkategorisiert';
            
            echo sprintf('<optgroup label="%s">', $categoryName);

            foreach ($mainProducts_ as $mainProduct) {
                $isSelected = $mainProduct->getIdentifier() == $customer['productname']->getIdentifier();
                $isActive = $mainProduct->getActive();

                if (!$isSelected && !$isActive) {
                    continue;
                }

                $priceGross = $mainProduct->getPriceGross();

                echo sprintf('<option value="%s"%s%s>[%s] %s%s</option>',
                    $mainProduct->getIdentifier(),
                    $isActive ? '' : ' disabled="disabled"',
                    $isSelected ? ' selected="selected"' : '',
                    $mainProduct->getIdentifier(),
                    $mainProduct->getName(),
                    empty($priceGross) ? '' : ' (Preis: '.$priceGross.'€)'
                );
            }

            echo '</optgroup>';
        }
        ?>
    </select>
  </span>
  
  <span>
    <label for="CustomerContractSentDate06">Vertrag vers. am</label><br />
    <input id="CustomerContractSentDate06" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'contract_sent_date'); ?>" name="CustomerContractSentDate">
  </span>
  <span>
    <label for="CustomerContractVersion06">Vertrag unterschr. am</label><br />
    <input id="CustomerContractVersion06" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'contract_version'); ?>" maxlength="11" name="CustomerContractVersion">
  </span>
  <span>
    <label for="CustomerServiceLevel06">Service Level *</label><br />
    <select id="CustomerServiceLevel06" name="CustomerServiceLevel" <?php echo $disable; ?>>
      <?php setOption ($option_servicelevel, $customer, 'service_level'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerServiceLevelSend06">SLA versendet am</label><br />
    <input id="CustomerServiceLevelSend06" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'service_level_send'); ?>" maxlength="11" name="CustomerServiceLevelSend">
  </span>
  
  <span>
    <label for="CustomerContractReceivedDate06">Vertrag empf. am</label><br />
    <input id="CustomerContractReceivedDate06" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'contract_received_date'); ?>" name="CustomerContractReceivedDate">
  </span>
  <span>
    <label for="CustomerContractAcknowledge06">Vert.Best. zugesandt am</label><br />
    <input id="CustomerContractAcknowledge06" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'contract_acknowledge'); ?>" maxlength="10" name="CustomerContractAcknowledge">
  </span>
  <span>
    <label for="CustomerAddPhoneLines06">Zusätzlicher Gesprächskanal *</label><br />
    <input id="CustomerAddPhoneLines06" type="text" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'add_phone_lines'); ?>" maxlength="10" name="CustomerAddPhoneLines">
  </span>
  <span>
    <label for="CustomerAddPhoneNos06">Zusätzliche Tel.-Nr.</label><br />
    <input id="CustomerAddPhoneNos06" type="text" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'add_phone_nos'); ?>" maxlength="10" name="CustomerAddPhoneNos">
  </span>
  
  <span>
    <label for="CustomerHouseConnection06">Kosten Hausanschluss</label><br />
    <input id="CustomerHouseConnection06" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'house_connection'); ?>" type="text" name="CustomerHouseConnection">
  </span>
  <span>
    <label for="CustomerRecruitedBy06">Geworben von</label><br />
    <input id="CustomerRecruitedBy06" type="text" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'recruited_by'); ?>" maxlength="64" name="CustomerRecruitedBy">
  </span>
  <span class="double">
    <label for="CustomerPurtelProduct06">Purtel-Produkt</label><br />
    <select id="CustomerPurtelProduct06" name="CustomerPurtelProduct" <?php echo $disable; ?>>
        <option value=""></option>

        <?php
        foreach ($groupedMainProducts as $categoryId => $mainProducts_) {
            $categoryHasActiveProducts = false;

            foreach ($mainProducts_ as $mainProduct) {
                if ($mainProduct->getActive()) {
                    $categoryHasActiveProducts = true;

                    break;
                }
            }

            if (!$categoryHasActiveProducts) {
                $customerProductNameCategory = null !== $customer['purtel_product']->getCategory() ? $customer['purtel_product']->getCategory()->getId() : 'uncategorized';

                if ($customerProductNameCategory !== $categoryId) {
                    continue;
                }
            }

            $categoryName = null !== $mainProducts_[0]->getCategory() ? $mainProducts_[0]->getCategory()->getName() : 'unkategorisiert';
            
            echo sprintf('<optgroup label="%s">', $categoryName);

            foreach ($mainProducts_ as $mainProduct) {
                $isSelected = $mainProduct->getIdentifier() == $customer['purtel_product']->getIdentifier();
                $isActive = $mainProduct->getActive();

                if (!$isSelected && !$isActive) {
                    continue;
                }

                $priceGross = $mainProduct->getPriceGross();

                echo sprintf('<option value="%s"%s%s>[%s] %s%s</option>',
                    $mainProduct->getIdentifier(),
                    $isActive ? '' : ' disabled="disabled"',
                    $isSelected ? ' selected="selected"' : '',
                    $mainProduct->getIdentifier(),
                    $mainProduct->getName(),
                    empty($priceGross) ? '' : ' (Preis: '.$priceGross.'€)'
                );
            }

            echo '</optgroup>';
        }
        ?>
    </select>
  </span>
  
  <span>
    <label for="CustomerConnectionActivationDate06">Anschluss aktiv seit</label><br />
    <input id="CustomerConnectionActivationDate06" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'connection_activation_date'); ?>" name="CustomerConnectionActivationDate">
  </span>
  <span>
    <label for="CustomerContractChangeTo06">Vertragsänderung zum</label><br />
    <input id="CustomerContractChangeTo06" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'contract_change_to'); ?>" maxlength="11" name="CustomerContractChangeTo">
  </span>
  <span>
    <label for="CustomerPurtelproductAdvanced06">Purtel-Produkt vorab</label><br />
    <select id="CustomerPurtelproductAdvanced06" name="CustomerPurtelproductAdvanced" <?php echo $disable; ?>>
      <?php setOption ($option_purtelproduct_advanced, $customer, 'purtelproduct_advanced'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerFinalPurtelproductDate06">Wechsel zum Endprodukt</label><br />
    <input id="CustomerFinalPurtelproductDate06" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'final_purtelproduct_date'); ?>" maxlength="11" name="CustomerFinalPurtelproductDate">
  </span>
  
  <span>
    <label for="CustomerPurtelEditDone06">Aktivschaltung erledigt</label><br />
    <select id="CustomerPurtelEditDone06" name="CustomerPurtelEditDone" <?php echo $disable; ?>>
      <?php setOption ($option_purtel_edit_done, $customer, 'purtel_edit_done'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerPurtelEditDoneFrom06">von</label><br />  
    <select id="CustomerPurtelEditDoneFrom06" name="CustomerPurtelEditDoneFrom" <?php echo $disable; ?>>
      <?php setOption ($option_purtel_edit_done_from, $customer, 'purtel_edit_done_from'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerSofortdsl06">Highspeed Internet sofort</label><br />
    <select id="CustomerSofortdsl06" name="CustomerSofortdsl" <?php echo $disable; ?>>
      <?php setOption ($option_sofortdsl, $customer, 'sofortdsl'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerSipAccounts06">SIP-Accounts</label><br />
    <select id="CustomerSipAccounts06" name="CustomerSipAccounts" <?php echo $disable; ?>>
      <?php setOption ($option_sip_accounts, $customer, 'sip_accounts'); ?>
    </select>
  </span>
  
  <span>
    Nächster Kündigungstermin<br />
    <?php echo $contract_next_cancle_date; ?>
  </span>
  <span>
    <label for="CustomerWisocontractCanceledDate06"><?php echo \Wisotel\Configuration\Configuration::get('companyName'); ?> Vertrag gekündigt zum</label><br />
    <input id="CustomerWisocontractCanceledDate06" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wisocontract_canceled_date'); ?>" maxlength="11" name="CustomerWisocontractCanceledDate">
  </span>
  <span class="double">
    <label for="CustomerTvService06">TV- Dienst</label><br />
    <?php
    $disableTvDienst = $disable;

    if (!empty($disable) && $authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_TECH_DATA_TECHNICAL')) {
        $disableTvDienst = null;
    }
    ?>
    <select id="CustomerTvService06" name="CustomerTvService" <?php echo $disableTvDienst; ?>>
      <?php setOption ($option_tv_service, $customer, 'tv_service'); ?>
    </select>
  </span>
  
  <span>
    <label for="CustomerCancelTal06">Tal kündigen</label><br />
    <select id="CustomerCancelTal06" name="CustomerCancelTal" <?php echo $disable; ?>>
      <?php setOption ($option_cancel_tal, $customer, 'cancel_tal'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerTalCancelAckDate06">Tal-Kdg. bestätigt zum</label><br />
    <input id="CustomerTalCancelAckDate06" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'tal_cancel_ack_date'); ?>" name="CustomerTalCancelAckDate">
  </span>
  <span>
    <label for="CustomerHdPlusCard06">HD-Plus-Karten</label><br />
    <select id="CustomerHdPlusCard06" name="CustomerHdPlusCard" <?php echo $disable; ?>>
      <?php setOption ($option_hd_plus_card, $customer, 'hd_plus_card'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerAddSatellite06">weitere Satelliten</label><br />
    <select id="CustomerAddSatellite06" name="CustomerAddSatellite" <?php echo $disable; ?>>
      <?php setOption ($option_add_satellite, $customer, 'add_satellite'); ?>
    </select>
  </span>

  <span>
    <label for="CustomerwisoContractSwitchoffFinish06">Kunde inaktiv seit</label><br />
    <input id="CustomerwisoContractSwitchoffFinish06" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wisocontract_switchoff_finish'); ?>" maxlength="11" name="CustomerwisoContractSwitchoffFinish">
  </span>
  <span>
    <label for="CustomerContractOnlineDate06">Online eingegangen am</label><br />
    <input id="CustomerContractOnlineDate06" type="text" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'contract_online_date'); ?>" maxlength="11" readonly="readonly" name="CustomerContractOnlineDate">
  </span>
  <span>
    <label for="CustomerEuFlat06">EU-Flat</label><br />
    <select id="CustomerEuFlat06" name="CustomerEuFlat" <?php echo $disable; ?>>
      <?php setOption ($option_eu_flat, $customer, 'eu_flat'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerMobileFlat06">Mobilfunk-Flat</label><br />
    <select id="CustomerMobileFlat06" name="CustomerMobileFlat" <?php echo $disable; ?>>
      <?php setOption ($option_mobile_flat, $customer, 'mobile_flat'); ?>
    </select>
  </span>

  <span class="double"><br /><h4>Sonstiges</h4></span>
  <span class="double">
    <label for="CustomerIpAddressInet06">Feste IP-Adr. (Internet)</label><br />
    <select id="CustomerIpAddressInet06" name="CustomerIpAddressInet" <?php echo $disable; ?>>
      <?php setOption ($option_ip_address_inet, $customer, 'ip_address_inet'); ?>
    </select>
  </span>

  <span class="doublebox">
    <span>
      <label for="CustomerPaperBill06">Papierrechnung</label><br />
      <select id="CustomerPaperBill06" name="CustomerPaperBill" <?php echo $disable; ?>>
        <?php setOption ($option_paper_bill, $customer, 'paper_bill'); ?>
      </select>
    </span>
    <span>
      <label for="CustomerForderingCosts06">Versandkostenpauschale</label><br />
      <select id="CustomerForderingCosts06" name="CustomerForderingCosts" <?php echo $disable; ?>>
        <?php setOption ($option_fordering_costs, $customer, 'fordering_costs'); ?>
      </select>
    </span>
    <span>
      <label for="CustomerGfBranch06">GF Hausanschluss</label><br />
      <select id="CustomerGfBranch06" name="CustomerGfBranch" <?php echo $disable; ?>>
        <?php setOption ($option_gf_branch, $customer, 'gf_branch'); ?>
      </select>
    </span>
    <span>
      <label for="CustomerConnectionInstallation06">Einrichtung Anschluss</label><br />
      <select id="CustomerConnectionInstallation06" name="CustomerConnectionInstallation" <?php echo $disable; ?>>
        <?php setOption ($option_connection_installation, $customer, 'connection_installation'); ?>
      </select>
    </span>
    <span>
      <label for="CustomerAddChargeTerminal06">Zuzahlung Endgerät</label><br />
      <select id="CustomerAddChargeTerminal06" name="CustomerAddChargeTerminal" <?php echo $disable; ?>>
        <?php setOption ($option_add_charge_terminal, $customer, 'add_charge_terminal'); ?>
      </select>
    </span>
    <span>
      <label for="CustomerTerminalType06">Endgerät Typ</label><br />
      <select id="CustomerTerminalType06" name="CustomerTerminalType" <?php echo $disable; ?>>
        <?php setOption ($option_terminal_type, $customer, 'terminal_type'); ?>
      </select>
    </span>
  </span>
  <span class="double">
    <label for="CustomerContractComment06">Kommentar (Vertragsdetails)</label><br />
    <textarea id="CustomerContractComment06" name="CustomerContractComment" <?php echo $disable; ?>><?php setInputValue ($customer, 'contract_comment'); ?></textarea>
  </span>
  
  <span>
    <label for="CustomerServiceTechnician06">Servicetechniker</label><br />
    <select id="CustomerServiceTechnician06" name="CustomerServiceTechnician" <?php echo $disable; ?>>
      <?php setOption ($option_service_technician, $customer, 'service_technician'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerAddPort06">Zusätzl. Port.</label><br />
    <select id="CustomerAddPort06" name="CustomerAddPort" <?php echo $disable; ?>>
      <?php setOption ($option_add_port, $customer, 'add_port'); ?>
    </select>
  </span>
  <span class="double"><br /><h4>Telefonbucheintrag</h4></span>
  
  <span>
    <label for="CustomerAddCustomerOnFb06">Zusätzl. Mandant auf FB</label><br />
    <select id="CustomerAddCustomerOnFb06" name="CustomerAddCustomerOnFb" <?php echo $disable; ?>>
      <?php setOption ($option_add_customer_on_fb, $customer, 'add_customer_on_fb'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerSecondTal06">Bestellung 2. Tal</label><br />
    <select id="CustomerSecondTal06" name="CustomerSecondTal" <?php echo $disable; ?>>
      <?php setOption ($option_second_tal, $customer, 'second_tal'); ?>
    </select>
  </span>
  <span class="double">
    <label for="CustomerTelefonbuchEintrag06">Kunde wünscht Telefonbucheintrag</label><br />  
    <select id="CustomerTelefonbuchEintrag06" name="CustomerTelefonbuchEintrag" <?php echo $disable; ?>>
      <?php setOption ($option_telefonbuch_eintrag, $customer, 'telefonbuch_eintrag'); ?>
    </select>
  </span>
  
  <span>
    <label for="CustomerSpecialConditionsFrom06">Sonderkonditionen ab</label><br />
    <input id="CustomerSpecialConditionsFrom06" type="text" class="datepicker" <?php echo $disable; ?> name="CustomerSpecialConditionsFrom" value="<?php setInputValue ($customer, 'special_conditions_from'); ?>" >
  </span>
  <span>
    <label for="CustomerSpecialConditionsTill06">Sonderkonditionen bis</label><br />
    <input id="CustomerSpecialConditionsTill06" type="text" class="datepicker" <?php echo $disable; ?> name="CustomerSpecialConditionsTill" value="<?php setInputValue ($customer, 'special_conditions_till'); ?>">
  </span>
  <span>
    <label for="CustomerPhoneentryDoneDate06">Formular erstellt am</label><br />
    <input id="CustomerPhoneentryDoneDate06" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'phoneentry_done_date'); ?>" maxlength="11" name="CustomerPhoneentryDoneDate">
  </span>
  <span>
    <label for="CustomerPhoneentryDoneFrom06">von</label><br />
    <select id="CustomerPhoneentryDoneFrom06" name="CustomerPhoneentryDoneFrom" <?php echo $disable; ?>>
      <?php setOption ($option_phoneentry_done_from, $customer, 'phoneentry_done_from'); ?>
    </select>
  </span>
  
  <span class="double">
    <label for="CustomerSpecialConditionsText06">Sonderkonditionen Text</label><br />
    <textarea id="CustomerSpecialConditionsText06" name="CustomerSpecialConditionsText" <?php echo $disable; ?>><?php setInputValue ($customer, 'special_conditions_text'); ?></textarea>
  </span>
  <span class="doublebox">
    <span class="double"><br /><h4>Zusatzoptionen</h4></span>
    <span class="double">
      <label for="CustomerHigherAvailability06">Höhere Verfügbarkeit/ schnellere Entstörung</label><br />
      <select id="CustomerHigherAvailability06" name="CustomerHigherAvailability" <?php echo $disable; ?>>
        <?php setOption ($option_higher_availability, $customer, 'higher_availability'); ?>
      </select>
    </span>
    <span class="double">
      <label for="CustomerHigherUplinkOne06">Höherer Uplink bis 7,5 Mbit/s</label><br />
      <select id="CustomerHigherUplinkOne06" name="CustomerHigherUplinkOne" <?php echo $disable; ?>>
        <?php setOption ($option_higher_uplink_one, $customer, 'higher_uplink_one'); ?>
      </select>
    </span>
  </span>

  <span>
    <label for="CustomerGutschrift06">Gutschrift</label><br />
    <select id="CustomerGutschrift06" name="CustomerGutschrift" <?php echo $disable; ?>>
      <?php setOption ($option_gutschrift, $customer, 'gutschrift'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerGutschriftTill06">Gutschrift bis</label><br />
    <input id="CustomerGutschriftTill06" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'gutschrift_till'); ?>" maxlength="11" name="CustomerGutschriftTill">
  </span>
  <span class="double">
    <label for="CustomerHigherUplinkTwo06">Höherer Uplink bis 12 Mbit/s</label><br />
    <select id="CustomerHigherUplinkTwo06" name="CustomerHigherUplinkTwo" <?php echo $disable; ?>>
      <?php setOption ($option_higher_uplink_two, $customer, 'higher_uplink_two'); ?>
    </select>
  </span>
</div>

