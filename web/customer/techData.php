<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_TECH_DATA');

$disableConnection = 'readonly="readonly"';
$rwConnection = false;
$roConnection = false;
   

if ($authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_TECH_DATA_CONNECTION')) {
    $disableConnection = '';   
    $rwConnection = true;   
}
if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_TECH_DATA_CONNECTION')) {
    $roConnection = true;   
}


$disableCpe = 'readonly="readonly"';
$rwCpe = false;   
$roCpe = false;   

if ($authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_TECH_DATA_CPE')) {
    $disableCpe = '';   
    $rwCpe = true;   
}
if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_TECH_DATA_CPE')) {
    $roCpe = true;   
}

$disableActivation = 'readonly="readonly"';
$rwActivation = false;   
$roActivation = false;   

if ($authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_TECH_DATA_ACTIVATION')) {
    $disableActivation = '';   
    $rwActivation = true;   
}
if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_TECH_DATA_ACTIVATION')) {
    $roActivation = true;   
}


$disableTechnical = 'readonly="readonly"';
$rwTechnical = false;   
$roTechnical = false;   

if ($authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_TECH_DATA_TECHNICAL')) {
    $disableTechnical = '';   
    $rwTechnical = true;   
}
if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_TECH_DATA_TECHNICAL')) {
    $roTechnical = true;   
}

?>
<h3 class="CustTechData">Technische Daten</h3>
<div class="CustTechData form">
    <div style="overflow: hidden;">
        <?php
        /*
         * Left Column
         */
        ?>
        <div style="float: left; width: 385px;">
            <span class="double"><br /><h4>Anschluss</h4></span>

            <span class="double">
                <label for="CustomerClientid10"><?php echo \Wisotel\Configuration\Configuration::get('companyName'); ?> Nr.</label><br />
                <input type="text" id="CustomerClientid10" <?php echo $disableConnection; ?> value="<?php if ($roConnection) {setInputValue ($customer, 'clientid');} ?>" readonly="readonly" name="CustomerClientid">
            </span>

            <span class="double"><br /><?php if($rwConnection) {?><button onclick="copyMasterAddressToConnection()" type="button">Adresse aus Stammdaten hier einfügen</button><?php } ?></span>

            <span>
                <label for="CustomerConnectionStreet10">Straße *</label><br />
                <input type="text" id="CustomerConnectionStreet10" <?php echo $disableConnection; ?> value="<?php if ($roConnection) {setInputValue ($customer, 'connection_street');} ?>" name="CustomerConnectionStreet">
            </span>
            <span>
                <label for="CustomerConnectionStreetno10">Hausnummer *</label>
                <input type="text" id="CustomerConnectionStreetno10" <?php echo $disableConnection; ?> value="<?php  if ($roConnection) {setInputValue ($customer, 'connection_streetno');} ?>" name="CustomerConnectionStreetno">
            </span>

            <span>
                <label for="CustomerConnectionZipcode10">PLZ *</label><br />
                <input type="text" id="CustomerConnectionZipcode10" <?php echo $disableConnection; ?> value="<?php  if ($roConnection) {setInputValue ($customer, 'connection_zipcode');} ?>" name="CustomerConnectionZipcode">
            </span>
            <span>
                <label for="CustomerConnectionCity10">Stadt *</label>
                <input type="text" id="CustomerConnectionCity10" <?php echo $disableConnection; ?> value="<?php  if ($roConnection) {setInputValue ($customer, 'connection_city', $customer);} ?>" name="CustomerConnectionCity">
            </span>

            <span class="double">
                <label for="CustomerProductname10">Produktname *</label><br />
                <?php
                $productName = null;

                if ($roConnection) {
                    if (!empty($customer['productname']->getId())) {
                        $productName = sprintf('[%s] %s', 
                            $customer['productname']->getIdentifier(),
                            $customer['productname']->getName()
                        );
                    }
                }
                ?>
                <input type="text" id="CustomerProductname10" value="<?php echo $productName; ?>" readonly="readonly" name="CustomerProductname">
            </span>

            <span class="double">
                <label for="CustomerPurtelProduct10">Purtel-Produkt</label><br />
                <?php
                $productName = null;

                if ($roConnection) {
                    if (!empty($customer['purtel_product']->getId())) {
                        $productName = sprintf('[%s] %s', 
                            $customer['purtel_product']->getIdentifier(),
                            $customer['purtel_product']->getName()
                        );
                    }
                }
                ?>
                <input type="text" id="CustomerPurtelProduct10" value="<?php echo $productName; ?>" readonly="readonly" name="CustomerPurtelProduct">
            </span>

            <span class="double"><br /><h4>Endgerät</h4></span>

            <?php
            if ($roCpe) {
                echo '<span class="double">';
                echo '<label>TV-Dienst</label>';
                echo '<span class="clone-element" data-element-id="CustomerTvService06"></span>';
                echo '</span>';
            }
            ?>

            <span>
                <label for="CustomerTerminalType10">Endgerät Typ</label><br />
                <select id="CustomerTerminalType10" name="CustomerTerminalType" <?php echo $disableCpe; ?>>
                  <?php  if ($roCpe) {setOption($option_terminal_type, $customer, 'terminal_type');} ?>
                </select>
            </span>
            <span>
                <label for="CustomerFbVorab10">Fritzbox vorab</label><br />
                <select id="CustomerFbVorab10" name="CustomerFbVorab" <?php echo $disableCpe; ?>>
                  <?php if ($roCpe) {setOption ($option_fb_vorab, $customer, 'fb_vorab');} ?>
                </select>  
            </span>

            <span class="double">
                <label for="CustomerMacAddress10">CWMP</label><br />
                <input type="text" id="CustomerMacAddress10" <?php echo $disableCpe; ?> value="<?php if ($roCpe) {setInputValue ($customer, 'mac_address');} ?>" name="CustomerMacAddress">
            </span>

            <span class="double">
                <label for="CustomerTerminalSerialno10">Endgerät Seriennummer</label><br />
                <input type="text" id="CustomerTerminalSerialno10" <?php echo $disableCpe; ?> value="<?php if ($roCpe) {setInputValue ($customer, 'terminal_serialno');} ?>" name="CustomerTerminalSerialno">
            </span>

            <span>
                <label for="CustomerTerminalReady10">FB konfiguriert am</label><br />
                <input type="text" id="CustomerTerminalReady10" <?php echo $disableCpe; ?> value="<?php if ($roCpe) {setInputValue ($customer, 'terminal_ready');} ?>" class="datePicker" name="CustomerTerminalReady">
            </span>
            <span>
                <label for="CustomerTerminalReadyFrom10">eingerichtet von</label><br />
                <select id="CustomerTerminalReadyFrom10" name="CustomerTerminalReadyFrom" <?php echo $disableCpe; ?>>
                  <?php if ($roCpe) {setOption ($option_terminal_ready_from, $customer, 'terminal_ready_from');} ?>
                </select>
            </span>

            <span>
                <label for="CustomerFirmwareVersion10">Firmware-Version</label><br />
                <input type="text" id="CustomerFirmwareVersion10" <?php echo $disableCpe; ?> value="<?php if ($roCpe) {setInputValue ($customer, 'firmware_version');} ?>" name="CustomerFirmwareVersion">
            </span>
            <span>
                <label for="CustomerAcsId10">ACS Hardware ID</label><br />
                <input type="text" id="CustomerAcsId10" <?php echo $disableCpe; ?> value="<?php if ($roCpe) {setInputValue ($customer, 'acs_id');} ?>" name="CustomerAcsId">
            </span>

            <?php
            $internetUsername = PppoeUsername::create($customer['pppoe_pin'], $customer['clientid']);
            ?>
            <span class="double">
                <label for="CustomerUser10">Internet-Benutzer</label><br />
                <input type="text" id="CustomerUser10" value="<?php if ($roCpe) {echo $internetUsername;} ?>" readonly="readonly" />
            </span>
            <span class="double">
                <label for="CustomerPassword10">Internet-Passwort</label><br />
                <input type="text" id="CustomerPassword10" <?php echo $disableCpe; ?> value="<?php if ($roCpe) {setInputValue ($customer, 'password');} ?>" readonly="readonly" name="CustomerPassword">
            </span>

            <span class="double">
                <label class="yellow" for="CustomerPppoePin">PPPoE-PIN</label><br />
                <input style="width: 90%;" type="text" id="CustomerPppoePin" <?php echo $disableCpe; ?> value="<?php if ($roCpe) {setInputValue ($customer, 'pppoe_pin');} ?>" name="CustomerPppoePin" />
                <?php
                if (empty($disableCpe)) {
                    echo '<a style="display: inline-block; margin-left: 10px;" href="'.$this->generateUrl('customer/technical-data/set-internet-password-from-pppoe-pin', ['id' => $customer['id']]).'" title="Setzte Internet-Passwort aus PPPoE-PIN">
                        <img src="'.$this->container->get('assets.packages')->getUrl('_img/Target.png').'" />
                        </a>';
                }
                ?>
            </span>

            <span class="double">
                <label class="yellow" for="CustomerRemoteLogin10">Remote-Login</label><br />
                <input type="text" id="CustomerRemoteLogin10" <?php echo $disableCpe; ?> value="<?php if ($roCpe) {setInputValue ($customer, 'remote_login');} ?>" name="CustomerRemoteLogin">
            </span>

            <span>
                <label for="CustomerRemotePassword10">Remote-Passwort</label><br />
                <input type="text" id="CustomerRemotePassword10" <?php echo $disableCpe; ?> value="<?php if ($roCpe) {setInputValue ($customer, 'remote_password');} ?>" name="CustomerRemotePassword">
            </span>
            <span>
                <label for="CustomerForderingCosts10">Versandkostenpauschale</label><br />
                <select id="CustomerForderingCosts10" name="CustomerForderingCosts" <?php echo $disableCpe; ?>>
                  <?php if ($roCpe) {setOption ($option_fordering_costs, $customer, 'fordering_costs');} ?>
                </select>
            </span>

            <span>
                <label for="CustomerTerminalSendedDate10">Endgerät versendet am</label><br />
                <input type="text" id="CustomerTerminalSendedDate10" <?php echo $disableCpe; ?> value="<?php if ($roCpe) {setInputValue ($customer, 'terminal_sended_date');} ?>" class="datepicker" name="CustomerTerminalSendedDate">
            </span>
            <span>
                <label for="CustomerTerminalSendedFrom10">versendet von</label>
                <select id="CustomerTerminalSendedFrom10" name="CustomerTerminalSendedFrom" <?php echo $disableCpe; ?>>
                    <?php if ($roCpe) {setOption ($option_terminal_sended_from, $customer, 'terminal_sended_from');} ?>
                </select>
            </span>

            <span class="double"><br /><h4>IP-Adresse</h4></span>

            <span class="double">
                <label for="CustomerIpRangeId10">IP-Block</label><br />
                <select id="CustomerIpRangeId10" name="CustomerIpRangeId" <?php echo $disableTechnical; ?>>
                    <?php
                    if ($roActivation){
                        if (key_exists ('ip_range_id', $customer) and $customer['ip_range_id'] == 0) {        
                            echo "<option value='0' selected></option>\n";
                        } else {
                            echo "<option value='0'></option>\n";
                        }
                        
                        $rangeAssignedToCustomer = isset($customer['ip_range_id']);

                        /**
                         * Loop through all ipRanges for this customer based on network_id
                         */
                        foreach ($ipRanges as $ipRange) {
                            $selected = '';

                            if ($rangeAssignedToCustomer && $customer['ip_range_id'] == $ipRange->getId()) {
                                $selected = ' selected';
                            }

                            echo '<option value="'.$ipRange->getId().'"'.$selected.'>'.$ipRange->getName()
                                .' ('.$ipRange->getStartIpAddress().' - '.$ipRange->getEndIpAddress().')</option>'."\n";
                        }
                    }
                    ?>
                </select>
            </span>

            <span class="double">
                <label for="CustomerIpAddress10">IP-Adresse</label><br />
                <select id="CustomerIpAddress10" name="CustomerIpAddress" class="customer-ip-select" <?php echo $disableTechnical; ?>>
                    <?php
                    if ($roTechnical){
                        if (key_exists ('ip_address', $customer) and $customer['ip_address'] == '') {        
                            echo "<option value='' selected></option>\n";
                        } else {
                            echo "<option value=''></option>\n";
                        }

                        $ipAddressAssignedToCustomer = false;
                        
                        if (isset($customerIpAddresses)) {
                            $ipAddressAssignedToCustomer = isset($customer['ip_address']);

                            /**
                             * Release customers ipAddresse.
                             * If you don't do this here it won't be selectable.
                             */
                            $customerIpAddresses->getIpRange()->releaseIpAddress($customerIpAddresses);

                            /**
                             * Get available ipAddresses for customer based on his ip_range_id
                             */
                            $availableIpAddresses = $customerIpAddresses->getIpRange()->getAvailableIpAddresses();
                        } elseif (isset($customerIpRange)) {
                            // Customer has range but not an ip
                            $availableIpAddresses = $customerIpRange->getAvailableIpAddresses();
                        }

                        foreach ($availableIpAddresses as $k => $ipAddress) {
                            $ip = $ipAddress->getIpAddress();
                            $selected = '';

                            if ($ipAddressAssignedToCustomer && $customer['ip_address'] == $ip) {
                                $selected = ' selected';
                            }

                            echo '<option value="'.$ip.'"'.$selected.'>'.$ip.'</option>'."\n";
                        }

                        if (isset($customerIpAddresses)) {
                            /**
                             * Take customers ipAddresses again.
                             * If you don't do this here they will be selectable somewhere else too.
                             */
                            $customerIpAddresses->getIpRange()->takeIpAddress($customerIpAddresses);
                        }
                    }
                    ?>
                </select>
            </span>

            <span class="double">
                <label for="CustomerSubnetmask10">Subnetzmaske</label><br />
                <input type="text" id="CustomerSubnetmask10" <?php echo $disableTechnical; ?> value="<?php if ($roTechnical){setInputValue ($customer, 'subnetmask');} ?>" name="CustomerSubnetmask">
            </span>

            <span class="double">
                <label for="CustomerGateway10">Gateway</label><br />
                <input type="text" id="CustomerGateway10" <?php echo $disableTechnical; ?> value="<?php if ($roTechnical){echo isset($customerIpAddresses) ? $customerIpAddresses->getIpRange()->getGateway() : '';} ?>" readonly="readonly" />
            </span>
        </div>

        <?php
        /*
         * Right Column
         */
        ?>
        <div style="float: left; width: 385px;">
            <span class="double"><br /><h4>PPPoE Server</h4></span>
            <?php
            /*
             * Todo
             *
             * Der Button und das zugehörige JS soll entfernt werden.
             * Wurde jetzt nur auf die Schnelle auskommentiert.
             *
             * <span class="double"><br /><button onclick="fillTechdata()" type="button">Glasfaseranschluss</button></span>
             */
            ?>
            <span class="double">
                <label for="CustomerPppoeConfigDate10">PPPoE Konfig am</label><br />
                <input type="text" id="CustomerPppoeConfigDate10" <?php echo $disableTechnical; ?> value="<?php if ($roTechnical){setInputValue ($customer, 'pppoe_config_date');} ?>" class="datepicker" name="CustomerPppoeConfigDate">
            </span>

            <span class="double">
                <label for="CustomerDslamArrangedDate10">Am DSLAM/CMTS eingerichtet am</label><br />
                <input type="text" id="CustomerDslamArrangedDate10" <?php echo $disableTechnical; ?> value="<?php if ($roTechnical){setInputValue ($customer, 'dslam_arranged_date');} ?>" class="datepicker" name="CustomerDslamArrangedDate">
            </span>

            <span class="double"><br /><h4>Verteilerdaten</h4></span>

            <span class="double">
                <label class="" for="CustomerPatchDate10">Gepatched am</label><br />
                <input type="text" id="CustomerPatchDate10" <?php echo $disableTechnical; ?> value="<?php if ($roTechnical){setInputValue ($customer, 'patch_date');} ?>" class="datepicker" name="CustomerPatchDate">
            </span>

            <span class="double">                                                                                                                                             
                <label for="CustomerNetworkId10">Netz</label><br />
                <select id="CustomerNetworkId10" name="CustomerNetworkId" <?php echo $disableTechnical; ?>>
                  <?php if ($roTechnical){setOption ($networks, $customer, 'network_id');} ?>
                </select>
            </span>                                                            

            <span class="double">
                <label class="" for="CustomerLocationId10">DSLAM Standort</label><br />
                <select id="CustomerLocationId10" name="CustomerLocationId" <?php echo $disableTechnical; ?>>
                  <?php if ($roTechnical){setOption ($locations, $customer, 'location_id');} ?>
                </select>    
            </span>

            <span>
                <label for="CustomerLsaKvzPartitionId10">KVZ Bezeichnung</label><br />
                <select id="CustomerLsaKvzPartitionId10" name="CustomerLsaKvzPartitionId" <?php echo $disableTechnical; ?>>
                  <?php if ($roTechnical){setOption ($lsa_kvz_partitions, $customer, 'lsa_kvz_partition_id');} ?>
                </select>
            </span>
            <span>
                <label for="CustomerKvzAddition10">KVz Zusatz</label><br />
                <input type="text" id="CustomerKvzAddition10" <?php echo $disableTechnical; ?> value="<?php if ($roTechnical){setInputValue ($customer, 'kvz_addition');} ?>"name="CustomerKvzAddition">
            </span>

            <span class="double">
                <label for="CustomerKvzStandort10">KVz-Standort</label><br />
                <select id="CustomerKvzStandort10" name="CustomerKvzStandort" <?php echo $disableTechnical; ?>>
                  <?php if ($roTechnical){setOption ($option_kvz_standort, $customer, 'kvz_standort');} ?>
                </select>
            </span>

            <span>
                <label for="CustomerLsaPin10">LSA Kontakt</label><br />
                <select id="CustomerLsaPin10" name="CustomerLsaPin" <?php echo $disableTechnical; ?>>
                  <?php
                  if ($roTechnical){
                    if (key_exists ('lsa_pin', $customer) and $customer['lsa_pin'] == '') {        
                      echo "<option value='' selected></option>\n";
                    } else {
                      echo "<option value=''></option>\n";
                    }
                    foreach ($lsa_pins as $value) {
                      if (key_exists ('lsa_pin', $customer) and $customer['lsa_pin'] == $value) {
                        echo "<option value='".$value."' selected>".$value."</option>\n";
                      } else {
                        echo "<option value='".$value."'>".$value."</option>\n";
                      }
                    }
                  }
                  ?>
                </select>
            </span>
            <span>
                <label for="CustomerKvzToggleNo10">KVZ Schaltnummer</label>
                <input type="text" id="CustomerKvzToggleNo10" <?php echo $disableTechnical; ?> value="<?php if ($roTechnical){setInputValue ($customer, 'kvz_toggle_no');} ?>" name="CustomerKvzToggleNo">
            </span>

            <span class="double">
                <label for="CustomerDtagLine10">Leitungsbezeichnung (LBZ)</label><br />
                <input type="text" id="CustomerDtagLine10" <?php echo $disableTechnical; ?> value="<?php if ($roTechnical){setInputValue ($customer, 'dtag_line');} ?>" name="CustomerDtagLine">
            </span>

            <span>
                <label for="CustomerCardId10">DSLAM Karte</label><br />
                <select id="CustomerCardId10" name="CustomerCardId" <?php echo $disableTechnical; ?>>
                    <?php
                    foreach ($cards as $cardId => $cardText) {
                        $isSelected = $cardId == $customer['card_id']->getId();

                        echo sprintf('<option value="%s"%s>%s</option>',
                            $cardId,
                            $isSelected ? ' selected="selected"' : '',
                            $cardText
                        );
                    }
                    ?>
                </select>
            </span>
            <span>
                <label for="CustomerDslamPort10">Kartenport</label><br />
                <select id="CustomerDslamPort10" name="CustomerDslamPort" <?php echo $disableTechnical; ?>>
                    <option value=""></option>

                    <?php
                    if ($roTechnical) {
                        foreach ($cardPorts as $cardPort) {
                            $color = '';
                            $isSelected = $cardPort['number'] == $customer['dslam_port']->getNumber();
                            
                            if (null !== $cardPort['color']) {
                                $color = ' ('.$cardPort['color'].')';
                            }

                            echo sprintf('<option value="%s"%s>%s</option>',
                                $cardPort['number'],
                                $isSelected ? ' selected="selected"' : '',
                                $cardPort['number'].$color
                            );
                        }
                    }
                    ?>
                </select>
            </span>

            <span>
                <label for="CustomerLineIdentifier10">Line Identifier</label><br />
                <input type="text" id="CustomerLineIdentifier10" <?php echo $disableTechnical; ?> value="<?php if ($roTechnical){setInputValue ($customer, 'line_identifier');} ?>" name="CustomerLineIdentifier">
            </span>
            <span>
                <label for="CustomerVectoring10">Vectoring</label><br />
                <select id="CustomerVectoring10" name="CustomerVectoring" <?php echo $disableTechnical; ?>>
                  <?php if ($roTechnical){setOption ($option_vectoring, $customer, 'vectoring');} ?>
                </select>
            </span>

            <span>
                <label for="CustomerVlanID10">VLAN_ID</label><br />
                <input type="text" id="CustomerVlanID10" <?php echo $disableTechnical; ?> value="<?php if ($roTechnical){setInputValue ($customer, 'vlan_ID');} ?>" name="CustomerVlanID">
            </span>
            <span>
                <label for="CustomerTag10">TAG verwenden</label>
                <select id="CustomerTag10" name="CustomerTag" <?php echo $disableTechnical; ?>>
                  <?php if ($roTechnical){setOption ($option_tag, $customer, 'tag');} ?>
                </select>
            </span>

            <span>
                <label for="CustomerReachedUpstream10">Messung Upstream</label><br />
                <input type="text" id="CustomerReachedUpstream10" <?php echo $disableTechnical; ?> value="<?php if ($roTechnical){setInputValue ($customer, 'reached_upstream');} ?>" name="CustomerReachedUpstream">
            </span>
            <span>
                <label for="CustomerReachedDownstream10">Messung Downstream</label><br />
                <input type="text" id="CustomerReachedDownstream10" <?php echo $disableTechnical; ?> value="<?php if ($roTechnical){setInputValue ($customer, 'reached_downstream');} ?>" name="CustomerReachedDownstream">
            </span>

            <span class="double">
                <label for="CustomerTechdataComment10">Kommentar</label><br />
                <textarea id="CustomerTechdataComment10" class="small" name="CustomerTechdataComment" <?php echo $disableTechnical; ?>><?php if ($roTechnical){setInputValue ($customer, 'techdata_comment');} ?></textarea>
            </span>

            <span class="double"><br /><h4>Inbetriebnahme</h4></span>

            <span>
                <label for="CustomerCommissioningWishDate10">Inbetriebnahme geplant</label><br />
                <input type="text" id="CustomerCommissioningWishDate10" <?php echo $disableActivation; ?> value="<?php if ($roActivation) {setInputValue ($customer, 'commissioning_wish_date', $customer);} ?>" class="datepicker" name="CustomerCommissioningWishDate">
            </span>

            <span>
                <label for="CustomerCommissioningDate10">Inbetriebnahme erledigt am</label><br />
                <input type="text" id="CustomerCommissioningDate10" <?php echo $disableActivation; ?> value="<?php if ($roActivation) {setInputValue ($customer, 'commissioning_date', $customer);} ?>" class="datepicker" name="CustomerCommissioningDate">
            </span>

            <span>
                <label for="CustomerCommissioningFrom10">Inbetriebnahme erledigt von</label><br />
                <select id="CustomerCommissioningFrom10" name="CustomerCommissioningFrom" <?php echo $disableActivation; ?>>
                    <?php if ($roActivation) {setOption ($option_purtel_edit_done_from, $customer, 'commissioning_from');} ?>
                </select>
            </span>

            <span class="double"><br /><h4>Aktivierung</h4></span>

            <span>
                <label for="CustomerRoutingWishDate10">Aktivschaltungsdatum (Wunsch)</label><br />
                <input type="text" id="CustomerRoutingWishDate10" <?php echo $disableActivation; ?> value="<?php if ($roActivation) {setInputValue ($customer, 'routing_wish_date', $customer);} ?>" class="datepicker" name="CustomerRoutingWishDate">
            </span>
            <span>
                <label for="CustomerPurtelEditDone10">Aktivschaltung erledigt</label><br />
                <select id="CustomerPurtelEditDone10" name="CustomerPurtelEditDone" <?php echo $disableActivation; ?>>
                  <?php if ($roActivation) {setOption ($option_purtel_edit_done, $customer, 'purtel_edit_done');} ?>
                </select>
            </span>

            <span>
                <label for="CustomerRoutingActiveDate10">Umroutungsdatum</label><br />
                <input type="text" id="CustomerRoutingActiveDate10" <?php echo $disableActivation; ?> value="<?php if ($roActivation) {setInputValue ($customer, 'routing_active_date', $customer);} ?>" class="datepicker" name="CustomerRoutingActiveDate">
            </span>
            <span>
                <label for="CustomerPurtelEditDoneFrom10">von</label><br />
                <select id="CustomerPurtelEditDoneFrom10" name="CustomerPurtelEditDoneFrom" <?php echo $disableActivation; ?>>
                  <?php if ($roActivation) {setOption ($option_purtel_edit_done_from, $customer, 'purtel_edit_done_from');} ?>
                </select>
            </span>

            <span>
                <label for="CustomerRoutingActive10">Umroutung aktiv auf</label><br />
                <input type="text" id="CustomerRoutingActive10" <?php echo $disableActivation; ?> value="<?php if ($roActivation) {setInputValue ($customer, 'routing_active');} ?>" name="CustomerRoutingActive">
            </span>
            <span>
                <label for="CustomerRoutingDeleted10">Umroutung gelöscht am</label><br />
                <input type="text" id="CustomerRoutingDeleted10" <?php echo $disableActivation; ?> value="<?php if ($roActivation) {setInputValue ($customer, 'routing_deleted');} ?>" class="datepicker" name="CustomerRoutingDeleted">
            </span>

            <span>
                <label for="CustomerDPBO10">DPBO</label><br />
                <input type="number" id="CustomerDPBO10" <?php echo $disableActivation; ?> value="<?php if ($roActivation) {setInputValue ($customer, 'DPBO');} ?>" name="CustomerDPBO">
            </span>
            <span>
                <label for="CustomerNoPing10">Kein Ping</label>
                <select id="CustomerNoPing10" name="CustomerNoPing" <?php echo $disableActivation; ?>>
                  <?php if ($roActivation) {setOption ($option_no_ping, $customer, 'no_ping');} ?>
                </select>
            </span>

            <span>
                <label for="CustomerService10">Service</label><br />
                <select id="CustomerService10" name="CustomerService" <?php echo $disableActivation; ?>>
                    <option value=""></option>

                    <?php
                    $groupedMainProducts = [];

                    foreach ($mainProducts as $mainProduct) {
                        $category = null !== $mainProduct->getCategory() ? $mainProduct->getCategory()->getId() : 'uncategorized';
                        $groupedMainProducts[$category][] = $mainProduct;
                    }

                    foreach ($groupedMainProducts as $categoryId => $mainProducts_) {
                        $categoryHasActiveProducts = false;

                        foreach ($mainProducts_ as $mainProduct) {
                            if ($mainProduct->getActive()) {
                                $categoryHasActiveProducts = true;

                                break;
                            }
                        }

                        if (!$categoryHasActiveProducts) {
                            $customerProductNameCategory = null !== $customer['Service']->getCategory() ? $customer['Service']->getCategory()->getId() : 'uncategorized';

                            if ($customerProductNameCategory !== $categoryId) {
                                continue;
                            }
                        }

                        $categoryName = null !== $mainProducts_[0]->getCategory() ? $mainProducts_[0]->getCategory()->getName() : 'unkategorisiert';
                        
                        echo sprintf('<optgroup label="%s">', $categoryName);

                        foreach ($mainProducts_ as $mainProduct) {
                            $isSelected = $mainProduct->getIdentifier() == $customer['Service']->getIdentifier();
                            $isActive = $mainProduct->getActive();

                            if (!$isSelected && !$isActive) {
                                continue;
                            }

                            $priceGross = $mainProduct->getPriceGross();

                            echo sprintf('<option value="%s"%s%s>%s</option>',
                                $mainProduct->getIdentifier(),
                                $isActive ? '' : ' disabled="disabled"',
                                $isSelected ? ' selected="selected"' : '',
                                $mainProduct->getName()
                            );
                        }

                        echo '</optgroup>';
                    }
                    ?>
                </select>
            </span>
            <span>
                <?php
                $connectionType = $customer['dslam_port']->getConnectionType(); // either instance of CardType or null

                if (null === $connectionType) {
                    $connectionType = $customer['card_id']->getCardType(); // either instance of CardType or null
                }
                ?>

                <label for="CustomerSpectrumprofile10">Spectrumprofile</label>
                <select id="CustomerSpectrumprofile10" name="CustomerSpectrumprofile" <?php echo $disableTechnical; ?>>
                    <option value=""></option>
                    <?php
                    //if ($roTechnical) {setOption ($option_spectrumprofile, $customer, 'Spectrumprofile');}

                    if ($roTechnical) {
                        if (null !== $connectionType) {
                            $spectrumProfiles = $doctrine->getRepository(\AppBundle\Entity\SpectrumProfile::class)->findByConnectionType($connectionType);

                            foreach ($spectrumProfiles as $spectrumProfile) {
                                $selected = '';

                                if (0 === strcasecmp($customer['Spectrumprofile'], $spectrumProfile->getProfile())) {
                                    $selected = ' selected="selected"';
                                }

                                echo sprintf('<option value="%s"%s>%s</option>', 
                                    $spectrumProfile->getProfile(),
                                    $selected,
                                    $spectrumProfile->getIdentifier()
                                );
                            }
                        }
                    }
                    ?>
                </select>
            </span>

            <?php
            if (isset($connectionType)) { // if isset == true -> variable exists and is not null
                if (1 === preg_match('/\+ ?vdsl/i', $connectionType->getName())) {
                    $vdslCardType = $doctrine->getRepository(\AppBundle\Entity\Location\CardType::class)->findOneByName('VDSL');
                    $spectrumProfiles = $doctrine->getRepository(\AppBundle\Entity\SpectrumProfile::class)->findByConnectionType($vdslCardType);

                    $customersVdslSpectrumProfileId = $customer['technicalData']->getSpectrumProfileByCardType($vdslCardType);

                    if (null !== $customersVdslSpectrumProfileId) {
                        $customersVdslSpectrumProfileId = $customersVdslSpectrumProfileId->getId();
                    }

                    echo '<span>&nbsp;</span>
                    <span><label for="technical_data_spectrum_profile_vdsl">Spectrumprofile (VDSL)</label>
                    <select name="technical_data_spectrum_profile_vdsl">
                    <option value=""></option>';

                    foreach ($spectrumProfiles as $spectrumProfile) {
                        $selected = '';

                        if ($customersVdslSpectrumProfileId === $spectrumProfile->getId()) {
                            $selected = ' selected="selected"';
                        }

                        echo sprintf('<option value="%s"%s>%s</option>', 
                            $spectrumProfile->getId(),
                            $selected,
                            $spectrumProfile->getIdentifier()
                        );
                    }

                    echo '</select></span>';
                }
            }
            ?>

        </div>
    </div>

</div>

<?php
if ($customer['card_id']->getRfOverlayEnabled()) {
    ?>
    <h3 class="CustTechData">RF-Overlay</h3>
    <div class="CustTechData form">
        <div style="overflow: hidden;">
            <label for="technical_data_rf_overlay_signal_strength">Signalstärke</label>
            <input title="Vor Verwendung Kundendaten speichern" type="number" min="-12" max="12" step="0.1" id="technical_data_rf_overlay_signal_strength" name="technical_data_rf_overlay_signal_strength" value="<?php echo $customer['technicalData']->getRfOverlaySignalStrength(); ?>" placeholder="0" style="font-size: 18px; width: 90px; margin-right: 16px; line-height: 33px;" />
        </div>
    </div>
    <?php
}
?>

<h3 class="CustTechData">weitere Endgeräte</h3>
<div class="CustTechData form">
    <div style="overflow: hidden;">
        <?php
        /*
         * Left Column
         */
        ?>
        <div style="float: left; width: 385px;">
            <div id="terminals-content-wrap">
                <?php
                /**
                 * Keep track of added terminals.
                 * Start with 2 because 1 was shown above.
                 * This variable is also essential for the JavaScript-Code
                 */
                $count = 2;
                
                /**
                 * Loop through all terminals which belong to this customer
                 */
                foreach ($customer_addSomethingToThisNameToMakeItUnique->getTerminals() as $terminal) {
                    $terminalIpAddress = $terminal->getIpAddress();

                    if ($terminalIpAddress instanceof IpAddress) {
                        $terminalIpAddress = $terminalIpAddress->getIpAddress();
                    }
                    ?>

                    <div id="multiple-terminal-<?php echo $count; ?>">
                        <span class="double"><br />
                            <h4>Endgerät 
                                <?php if ($rwCpe) { ?>
                                    <button type="button" class="customer-terminal-remove">
                                        <img alt="" src="<?php echo $StartURL; ?>/_img/delete.png" height="12px"> löschen
                                    </button>
                                <?php } ?>
                            </h4>
                        </span>

                        <span>
                            <label for="customer-terminal-type[<?php echo $count; ?>]">Endgerät Typ</label><br />
                            <select id="customer-terminal-type[<?php echo $count; ?>]" name="customer-terminal-type[<?php echo $count; ?>]"<?php echo ' '.$disableCpe ?>>
                                <?php setOption ($option_terminal_type, ['type' => $terminal->getType()], 'type'); ?>
                            </select>
                        </span>

                        <span>
                            <label for="customer-terminal-ip[<?php echo $count; ?>]">IP-Adresse</label><br />
                            <select class="customer-terminal-ip-select" id="customer-terminal-ip[<?php echo $count; ?>]" 
                            name="customer-terminal-ip[<?php echo $count; ?>]"<?php echo ' '.$disableCpe ?>>
                                <option value="<?php echo $terminalIpAddress; ?>">
                                    <?php echo $terminalIpAddress; ?>
                                </option>
                            </select>
                        </span>

                        <span class="double">
                            <label for="customer-terminal-macaddress[<?php echo $count; ?>]">CWMP</label><br />
                            <input type="text" id="customer-terminal-macaddress[<?php echo $count; ?>]" 
                            value="<?php echo $terminal->getMacAddress(); ?>" 
                            name="customer-terminal-macaddress[<?php echo $count; ?>]"<?php echo ' '.$disableCpe ?>>
                        </span>

                        <span class="double">
                            <label for="customer-terminal-serialnumber[<?php echo $count; ?>]">Endgerät Seriennummer</label><br />
                            <input type="text" id="customer-terminal-serialnumber[<?php echo $count; ?>]" 
                            value="<?php echo $terminal->getSerialnumber(); ?>" 
                            name="customer-terminal-serialnumber[<?php echo $count; ?>]"<?php echo ' '.$disableCpe ?>>
                        </span>

                        <span>
                            <label for="customer-terminal-remote-user[<?php echo $count; ?>]">Remote User</label><br />
                            <input type="text" id="customer-terminal-remote-user[<?php echo $count; ?>]" 
                            value="<?php echo $terminal->getRemoteUser(); ?>" 
                            name="customer-terminal-remote-user[<?php echo $count; ?>]"<?php echo ' '.$disableCpe ?>>
                        </span>

                        <span>
                            <label for="customer-terminal-remote-password[<?php echo $count; ?>]">Remote Passwort</label><br />
                            <input type="text" id="customer-terminal-remote-password[<?php echo $count; ?>]" 
                            value="<?php echo $terminal->getRemotePassword(); ?>" 
                            name="customer-terminal-remote-password[<?php echo $count; ?>]"<?php echo ' '.$disableCpe ?>>
                        </span>

                        <span>
                            <label for="customer-terminal-remote-port[<?php echo $count; ?>]">Remote Port</label><br />
                            <input type="text" id="customer-terminal-remote-port[<?php echo $count; ?>]" 
                            value="<?php echo $terminal->getRemotePort(); ?>" 
                            name="customer-terminal-remote-port[<?php echo $count; ?>]"<?php echo ' '.$disableCpe ?>>
                        </span>
                        <span>
                            <label for="customer-terminal-activation-date[<?php echo $count; ?>]">Aktiv seit</label><br />
                            <input type="text" class="datePicker" id="customer-terminal-activation-date[<?php echo $count; ?>]" 
                            value="<?php echo $terminal->getActivationDateFormat(); ?>" 
                            name="customer-terminal-activation-date[<?php echo $count; ?>]"<?php echo ' '.$disableCpe ?>>
                        </span>

                        <span>
                            <label for="customer-terminal-tv-id[<?php echo $count; ?>]">Smartcard-ID</label><br />
                            <input type="text" id="customer-terminal-tv-id[<?php echo $count; ?>]" 
                            value="<?php echo $terminal->getTvId(); ?>" 
                            name="customer-terminal-tv-id[<?php echo $count; ?>]"<?php echo ' '.$disableCpe ?>>
                        </span>

                        <span>
                            <label for="customer-terminal-check-id[<?php echo $count; ?>]">Chip-ID</label><br />
                            <input type="text" id="customer-terminal-check-id[<?php echo $count; ?>]" 
                            value="<?php echo $terminal->getCheckId(); ?>" 
                            name="customer-terminal-check-id[<?php echo $count; ?>]"<?php echo ' '.$disableCpe ?>>
                        </span>
                    </div>

                    <?php
                    $count++;
                }
                ?>
            </div>
            <script type="text/javascript">
                <?php /**
                 * Make value of $count available for the JavaScript-Code
                 */ ?>
                var MultipleTerminalsCount = <?php echo $count; ?>;
            </script>
            <br />

            <span class="double">
                <?php if ($rwCpe) { ?>
                    <button type="button" class="add-terminal-button">
                        <img alt="" src="<?php echo $StartURL ?>/_img/New document.png" height="12px"> Endgerät hinzufügen
                    </button>
                <?php } ?>
            </span>
        </div>
    </div>
</div>

<h3 class="CustTechData">weitere IP-Adressen</h3>
<div class="CustTechData form">
    <div style="overflow: hidden;">
        <?php
        /*
         * Left Column
         */
        ?>
        <div style="float: left; width: 385px;">
            <div id="ip-content-wrap">
                <?php
                /**
                 * Keep track of added ipAddresses.
                 * Start with 2 because 1 was shown above.
                 * This variable is also essential for the JavaScript-Code
                 */
                $count = 2;

                /**
                 * Loop through all ipAddresses which belong to this customer
                 */
                foreach ($customer_addSomethingToThisNameToMakeItUnique->getIpAddresses() as $ipAddress) {
                    $customersIpAddress = $ipAddress->getIpAddress();
                    $customersIpRangeId = $ipAddress->getIpRange()->getId();
                    ?>

                    <div id="multiple-ip-<?php echo $count; ?>">
                        <span class="double">
                            <br />
                            <h4>IP-Adresse 
                                <?php if ($rwCpe) { ?>
                                    <button type="button" class="customer-ip-remove">
                                        <img alt="" src="<?php echo $StartURL; ?>/_img/delete.png" height="12px"> löschen
                                    </button>
                                <?php } ?>
                            </h4>
                        </span>

                        <span class="double">
                            <label for="customer-ip-range-id[<?php echo $count; ?>]">IP-Block</label><br />
                            <select id="customer-ip-range-id[<?php echo $count; ?>]" name="customer-ip-range-id[<?php echo $count; ?>]"<?php echo ' '.$disableCpe ?>>
                                <?php
                                /**
                                 * Loop through all ipRanges for this customer based on network_id
                                 */
                                foreach ($ipRanges as $ipRange) {
                                    $selected = '';

                                    if ($customersIpRangeId == $ipRange->getId()) {
                                        $selected = ' selected';
                                    }

                                    echo '<option value="'.$ipRange->getId().'"'.$selected.'>'.$ipRange->getName()
                                        .' ('.$ipRange->getStartIpAddress().' - '.$ipRange->getEndIpAddress().')</option>'."\n";
                                }
                                ?>
                            </select>
                        </span>

                        <span class="double">
                            <label for="customer-ip-address[<?php echo $count; ?>]">IP Adresse</label><br />
                            <select id="customer-ip-address[<?php echo $count; ?>]" name="customer-ip-address[<?php echo $count; ?>]" class="customer-ip-select"<?php echo ' '.$disableCpe ?>>
                                <?php
                                /**
                                 * Release customers ipAddresses.
                                 * If you don't do this here they won't be selectable.
                                 */
                                $ipAddress->getIpRange()->releaseIpAddress($ipAddress);

                                /**
                                 * Get available ipAddresses for customer based on his ip_range_id
                                 */
                                $availableIpAddresses = $ipAddress->getIpRange()->getAvailableIpAddresses();

                                foreach ($availableIpAddresses as $ip) {
                                    $address = $ip->getIpAddress();

                                    $selected = '';

                                    if ($customersIpAddress == $address) {
                                        $selected = ' selected';
                                    }

                                    echo '<option value="'.$address.'"'.$selected.'>'.$address.'</option>'."\n";
                                }

                                /**
                                 * Take customers ipAddresses again.
                                 * If you don't do this here they will be selectable somewhere else too.
                                 */
                                $ipAddress->getIpRange()->takeIpAddress($ipAddress);
                                ?>
                            </select>
                        </span>

                        <span class="double">
                            <label for="customer-subnetmask[<?php echo $count; ?>]">Subnetzmaske</label><br />
                            <input type="text" id="customer-subnetmask[<?php echo $count; ?>]" 
                                value="<?php echo $ipAddress->getSubnetmask(); ?>" name="customer-subnetmask[<?php echo $count; ?>]"<?php echo ' '.$disableCpe ?>>
                        </span>

                        <span class="double">
                            <label for="customer-gateway[<?php echo $count; ?>]">Gateway</label><br />
                            <input type="text" id="customer-gateway[<?php echo $count; ?>]" 
                                value="<?php echo $ipAddress->getGateway(); ?>" name="customer-gateway[<?php echo $count; ?>]"<?php echo ' '.$disableCpe ?>>
                        </span>

                        <span class="double">
                            <label for="customer-rdns[<?php echo $count; ?>]">rDNS</label><br />
                            <input type="text" id="customer-rdns[<?php echo $count; ?>]" 
                                value="<?php echo $ipAddress->getReverseDns(); ?>" name="customer-rdns[<?php echo $count; ?>]"<?php echo ' '.$disableCpe ?>>
                        </span>
                    </div>

                    <?php
                    $count++;
                }
                ?>
            </div>
            <script type="text/javascript">
                <?php /**
                 * Make value of $count available for the JavaScript-Code
                 */ ?>
                var MultipleIpAddressesCount = <?php echo $count; ?>;
            </script>
            <br />

            <span class="double">
                <?php if ($rwCpe) { ?>
                    <button type="button" class="add-ip-button">
                        <img alt="" src="<?php echo $StartURL ?>/_img/New document.png" height="12px"> IP Adresse hinzufügen
                    </button>
                <?php } ?>
            </span>
            <br />
            <br />
        </div>
    </div>
</div>