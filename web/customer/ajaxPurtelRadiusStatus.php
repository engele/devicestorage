<?php
$paramKeys = [
    'StartPath',
    'CustomerPurtelMaster',
];

foreach ($paramKeys as $key) {
    if (isset($_POST[$key])) {
        $param[$key] = urldecode($_POST[$key]); 
    } elseif (isset($_GET[$key])) {
        $param[$key] = urldecode($_GET[$key]);
    }
}

if (empty($param['StartPath'])) {
    $response = array(
        'status' => false,
        'mes' => 'StartPath nicht übergeben.',
        'ip' => '',
    );
    print_r(json_encode($response));
    exit(1);    
};

if (empty($param['CustomerPurtelMaster'])){
    $response = array(
        'status' => false,
        'mes' => 'Purtelaccount fehlt.',
        'ip' => '',
    );
    print_r(json_encode($response));
    exit(1);    
}

require_once $param['StartPath'].'/vendor/wisotel/configuration/Configuration.php';
require_once $param['StartPath'].'/vendor/wisotel/purtel-radius-api/PurtelRadiusApi.php';

use WisotelKv\PurtelRadiusApi\PurtelRadius;

$username = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$password = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
$realm = \Wisotel\Configuration\Configuration::get('purtelSipRealm');

$radius = new PurtelRadius($username, $password, $realm);
$radius->statusRadius($param['CustomerPurtelMaster']);

$ip = $radius->getFramedipaddrss();

// when acctstoptime ist not empty, this means that this ip is assigned to this customer any longer
if (!empty($radius->getAcctstoptime())) {
    $ip = '';
}

$response = array(
    'status' => $radius->getStatus(),
    'mes' => $radius->getErrMes(),
    'ip' => $ip,
);

print_r(json_encode($response));
?>
