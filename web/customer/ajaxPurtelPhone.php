<?php

$StartPath = urldecode($_POST['StartPath']);
$SelPath = urldecode($_POST['SelPath']);
$StartURL = urldecode($_POST['StartURL']);
$CustId = urldecode($_POST['CustId']);
$CustClientid = urldecode($_POST['CustClientid']);

require_once $StartPath.'/_conf/database.inc';
require_once $StartPath.'/'.$SelPath.'/_conf/database.inc';
require_once $StartPath.'/vendor/wisotel/configuration/Configuration.php';

$username = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$password = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

$status = 0;    
$intNum = '0049';
$action = 'getnumbers';
$action2 = 'setnumber';

$phone = array();
$purtelPhone = array();
$kvPhone = array();

if ($CustClientid) {
    $PurtelUrl = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s&kundennummer_extern=%s',
        urlencode($username),
        urlencode($password),
        urlencode($action),
        urlencode($CustClientid)
    );

    $curl = curl_init($PurtelUrl);

    curl_setopt_array($curl, array(
        CURLOPT_URL => $PurtelUrl,
        CURLOPT_HEADER => false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
    ));

    $result = curl_exec($curl);

    curl_close($curl);
    
    $resultArray = explode("\n", $result);

    array_shift($resultArray);

    // Purtel Account
    foreach ($resultArray as $result) {
        if ($result) {
            list(
                $phone['account'],
                $phone['tel'],
                $phone['type'],
                $phone['clientid']
            ) = str_getcsv($result, ";");

            $purtelPhone[$phone['tel']] = $phone['account'];
        }
    }

    // Kundenverwaltung Account
    $db_purtel_account = $db->prepare("SELECT * FROM purtel_account WHERE cust_id = ? ORDER BY master DESC, purtel_login");
    $db_purtel_account->bind_param('i', $CustId);
    $db_purtel_account->execute();
    $db_purtel_account_result = $db_purtel_account->get_result();
    
    $purtel_account_db = $db_purtel_account_result->fetch_all(MYSQLI_ASSOC);
    
    foreach ($purtel_account_db AS $temp) {
        $phone = $intNum.substr($temp['nummer'], 1);
        $kvPhone[$phone] = $temp['purtel_login']; 
    }

    $db_purtel_account->close();

    foreach ($kvPhone as $phone => $account) {
        if ($phone && key_exists($phone, $purtelPhone)) {
            if ($account != $purtelPhone[$phone]){
                $PurtelUrl = sprintf(
                    'https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s'
                    .'&rufnummer=%s&anschluss=%s&verschieben_zu=%s',
                    urlencode($username),
                    urlencode($password),
                    urlencode($action2),
                    urlencode($phone),
                    urlencode($purtelPhone[$phone]),
                    urlencode($account)
                );

                $curl = curl_init ($PurtelUrl);

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $PurtelUrl,
                    CURLOPT_HEADER => false,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_SSL_VERIFYPEER => false,
                ));

                $result = curl_exec($curl);
    
                curl_close($curl);
            }
        }
    }
}

$response = array(
    'status' => 0,
);

print_r(json_encode($response));

?>
