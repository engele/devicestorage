<?php

/*
removed usage of:
    $customer['application']
    $customer['application_text']
    $customer['contract']
    $customer['porting']
    $customer['talorder']
    $customer['implementation']
*/

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_SEARCH');

$disable = 'readonly="readonly"';
$rw = false;
$rwPurtel = false;   
    
if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_MASTER_DATA')) {
    $disable = '';   
    $rw = true;   
}

if ($authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_PURTEL')) {
    $rwPurtel = true;   
}

$PurtelRadius = \Wisotel\Configuration\Configuration::get('purtelRadius');

$portingDone = false;
$talOrderDone = false;
$contractDone = false;
$statustext_contract = null;
$statustext_application = $status_text['supply_unknown'];
$applicationStatusClass = 'yellow';
$contractStatusClass = 'gray';
$portingStatusClass = 'gray';
$talorderStatusClass = 'gray';
$implementationStatusClass = 'gray';

switch ($customer['prospect_supply_status']) {
    case 'with_supply':
        $applicationStatusClass = 'green';
        $statustext_application = $status_text[$customer['prospect_supply_status']];
        break;

    case 'without_supply':
        $applicationStatusClass = 'red';
        $statustext_application = $status_text[$customer['prospect_supply_status']];
        break;

    case 'supply_unknown':
        $applicationStatusClass = 'yellow';
        $statustext_application = $status_text[$customer['prospect_supply_status']];
        break;

    case 'not_now_supply':
        $applicationStatusClass = 'yellow';
        $statustext_application = $status_text[$customer['prospect_supply_status']];
        $statustext_contract = $status_text['application_not_done'];
        break;

    default:
        $statustext_contract = $status_text['application_not_done'];
        break;
}

if (null === $statustext_contract) {
    $statustext_contract = 'contract_not_sent';

    if (!empty($customer['contract_sent_date'])) {
        $statustext_contract = 'waiting_for_contract';
        $contractStatusClass = 'yellow';

        if (!empty($customer['contract_received_date'])) {
            $contractDone = true;
            $contractStatusClass = 'green';
            $statustext_contract = 'contract_received###<i>'.$customer['contract_received_date'].'</i>';

            $canceledText = ', contract_not_cancelled';

            if (!empty($customer['wisocontract_canceled_date'])) {
                $contractStatusClass = 'red';
                $canceledText = ', contract_cancelled_on###<i>'.$customer['wisocontract_canceled_date'].'</i>';
            }

            $statustext_contract .= $canceledText;
        }
    }

    $creditRatingText = ', credit_rating_outstanding';

    if (!empty($customer['credit_rating_ok'])) {
        $creditRatingText = ', credit_rating_not_ok';

        if ('successful' == $customer['credit_rating_ok']) {
            $creditRatingText = ', credit_rating_ok';
        } elseif ('in_process' == $customer['credit_rating_ok']) {
            $creditRatingText = ', credit_rating_in_process';
        }
    }

    $statustext_contract .= $creditRatingText;

    if (empty($customer['network_id'])) {
        $statustext_contract .= ', network_id_not_ok';
    }

    if (empty($customer['location_id'])) {
        $statustext_contract .= ', dslam_location_missing';
    }
}

$statustext_contract = str_replace('###', ' ', $statustext_contract);

foreach ($status_text as $key => $value) {
    $statustext_contract = str_replace($key, $value, $statustext_contract);
}


$statustext_talorder = 'talorder_not_done';
$statustext_porting = 'contract_not_done';

if ($contractDone) {
    $statustext_porting = 'no_porting_done_yet';

    if (!empty($customer['wbci_eingestellt_am'])) {
        $statustext_porting = 'wbci_eingestellt_am###<i>'.$customer['wbci_eingestellt_am'].'</i>';
        $portingStatusClass = 'yellow';

        if (!empty($customer['wbci_bestaetigt_am'])) {
            $portingDone = true;
            $portingStatusClass = 'green';
            $statustext_porting = 'porting_confirmed###<i>'.$customer['wbci_bestaetigt_am'].'</i> (WBCI)';
        }
    } elseif (!empty($customer['ventelo_purtel_enter_date'])) {
        $statustext_porting = 'purtel_letter_uploaded###<i>'.$customer['ventelo_purtel_enter_date'].'</i>';

        if (!empty($customer['stati_port_confirm_date'])) {
            $portingDone = true;
            $portingStatusClass = 'green';
            $statustext_porting = 'porting_confirmed###<i>'.$customer['stati_port_confirm_date'].'</i>';
        }
    }

    $statustext_talorder = 'tal_not_ordered';

    if (!empty($customer['tal_order_date'])) {
        $statustext_talorder = 'tal_ordered###<i>'.$customer['tal_order_date'].'</i>';
        $talorderStatusClass = 'yellow';

        if (!empty($customer['tal_order_ack_date'])) {
            $talOrderDone = true;
            $talorderStatusClass = 'green';
            $statustext_talorder = 'tal_order_confirmed###<i>'.$customer['tal_order_ack_date'].'</i>';
        }
    }
}

$statustext_porting = str_replace('###', ' ', $statustext_porting);

foreach ($status_text as $key => $value) {
    $statustext_porting = str_replace($key, $value, $statustext_porting);
}

$statustext_talorder = str_replace('###', ' ', $statustext_talorder);

foreach ($status_text as $key => $value) {
    $statustext_talorder = str_replace($key, $value, $statustext_talorder);
}

$previousStepsBevoreImplementationDone = true;
$statustext_implementation = '';

if (!$talOrderDone && !$portingDone) {
    $statustext_implementation .= 'talorder_and_porting_not_done'; 
    $previousStepsBevoreImplementationDone = false;
} elseif (!$talOrderDone) {
    $statustext_implementation .= 'talorder_not_done';
    $previousStepsBevoreImplementationDone = false; 
} elseif (!$portingDone) {
    $statustext_implementation .= 'porting_not_done';
    $previousStepsBevoreImplementationDone = false; 
}

$states = [
    'pppoe-config' => 'pppoe_not_done',
    'dslam' => 'DSLAM_not_done',
    'terminal' => 'terminal_not_ready',
    'patch' => 'patch_not_done',
    'connection-activation' => 'connection_not_activated',
];

$finishedImplementationStatus = true;

if (!empty($customer['pppoe_config_date'])) {
    $states['pppoe-config'] = 'pppoe_done###<i>'.$customer['pppoe_config_date'].'</i>';
} else {
    $finishedImplementationStatus = false;
}

if (!empty($customer['dslam_arranged_date'])) {
    $states['dslam'] = 'DSLAM_done_date###<i>'.$customer['dslam_arranged_date'].'</i>';
} else {
    $finishedImplementationStatus = false;
}

if (!empty($customer['terminal_ready'])) {
    $states['terminal'] = 'terminal_ready###<i>'.$customer['terminal_ready'].'</i>';
} else {
    $finishedImplementationStatus = false;
}

if (!empty($customer['patch_date'])) {
    $states['patch'] = 'patch_done###<i>'.$customer['patch_date'].'</i>';
} else {
    $finishedImplementationStatus = false;
}

if (!empty($customer['connection_activation_date'])) {
    $states['connection-activation'] = 'connection_activated###<i>'.$customer['connection_activation_date'].'</i>';
} else {
    $finishedImplementationStatus = false;
}

$states = implode(', ', $states);
$statustext_implementation .= empty($statustext_implementation) ? $states : ', '.$states;

$statustext_implementation  = str_replace('###', ' ', $statustext_implementation);

foreach ($status_text as $key => $value) {
    $statustext_implementation = str_replace($key, $value, $statustext_implementation);
}

if ($previousStepsBevoreImplementationDone) {
    $implementationStatusClass = $finishedImplementationStatus ? 'green' : 'yellow';
}

?>

<h3 class="CustStatus"><?php echo 'Status ('.$customer['city'].', '.$customer['district'].')'; ?></h3>

<div class="CustStatus">
    <p>
        <span class="status <?php echo $applicationStatusClass; ?>">
            <span class="control <?php echo $applicationStatusClass; ?>"></span>
            <span class="statustext <?php echo $applicationStatusClass; ?>"><b>Antrag:</b> <?php echo $statustext_application; ?></span>
        </span>
    </p>

    <p>
        <span class="status <?php echo $contractStatusClass; ?>">
            <span class="control <?php echo $contractStatusClass; ?>"></span>
            <span class="statustext <?php echo $contractStatusClass; ?>"><b>Vertrag:</b> <?php echo $statustext_contract; ?></span>
        </span>
    </p>

    <?php
    if ((substr($customer['clientid'], 0, 5) == '00000' || substr($customer['clientid'], 0, 5) == '00001') && $contractDone) {
        echo "<p class='change2customer'>"
            ."<button name='change2customer' id='change2customer' onclick='custChange2customer()' type='button'>"
            ."Interessent in Kunde umwandeln</button></p>";
    }
    ?>

    <p>
        <span class="status <?php echo $portingStatusClass; ?>">
            <span class="control <?php echo $portingStatusClass; ?>"></span>
            <span class="statustext <?php echo $portingStatusClass; ?>"><b>Portierung:</b> <?php echo $statustext_porting; ?></span>
        </span>
    </p>

    <p>
        <span class="status <?php echo $talorderStatusClass; ?>">
            <span class="control <?php echo $talorderStatusClass; ?>"></span>
            <span class="statustext <?php echo $talorderStatusClass; ?>"><b>TAL-Bestellung:</b> <?php echo $statustext_talorder; ?></span>
        </span>
    </p>

    <p>
        <span class="status <?php echo $implementationStatusClass; ?>">
            <span class="control <?php echo $implementationStatusClass; ?>"></span>
            <span class="statustext <?php echo $implementationStatusClass; ?>">
                <b>Implementierung:</b> <?php echo $statustext_implementation; ?>
            </span>
        </span>
    </p>

    <?php
    if ($customer['connection_activation_date'] != '') {
        switch ($customer['payment_performance']) {
            case '':
                echo "<h1><span class='green'>Kunde zahlt pünktlich / zuverlässig okay</span></h1>";
                break;

            case 'Mahnung':
                echo "<h1><span class='yellow'>Kunde zahlt erst nach Mahnung</span></h1>";
                break;

            case 'Ruecklastschriften':
                echo "<h1><span class='red'>Ständige Rücklastschriften</span></h1>";
                break;

            case 'abgeschalten':
                echo "<h1><span class='red'>Abgeschaltet!!!</span></h1>";
                break;

            default:
                echo "<h1><span class='red'>Kunde zahlt nicht, Mahnverfahren eingeleitet</span></h1>";
                break;
        }
    }

    if ($PurtelRadius){
        $lblclass = 'yellow';  
        echo "<p>\n<span class='fbox radius status  $lblclass'>\n"
            ."<span class='fbox radius control $lblclass'></span>\n"
            ."<span class='fbox radius statustext radiustext $lblclass'><strong>Radius IP-Adresse:</strong> wird ermittelt</span>\n</p>\n";    
    }
    $IP = $customer['ip_address'];
    $Rlogin = $customer['remote_login'];
    $RPassw = $customer['remote_password'];
    $state = "pinging";  
    $return = 1;
    $lblclass = 'gray';  
    $achtung = "";

    if ($customer['district'] == "Nonnenberg" ) {
        $achtung = "Kunde ist am Nonnenberg IP Adresse überprüfen !!";
    }

    if ($customer['connection_activation_date'] != '' AND $IP != '') {
        if ($return == 0) {
            $state = "up";
            $lblclass = 'green';
        }
        echo "<p>\n<span class='fbox status  $lblclass'>\n"
            ."<span class='fbox control $lblclass'></span>\n"
            ."<span class='fbox statustext $lblclass'> F-Box <a href='https://$IP' target='_blank' >IP = $IP</a>"
            ." RLogin  = $Rlogin PW = $RPassw <span id='statusval'></span>\n"
            ."</span>\n</p>\n"; 
    }

    if ($rw) {
        ?>
        <h4>E-Mail an Kunden senden</h4>
        
        <p>
            <label for="EMailTemplate">E- Mail Template wählen</label><br />
            <select id="EMailTemplate" name="EMailTemplate" class="max">
                <option value='' selected>- bitte auswählen -</option>
                <?php
                $group_id = 0;
                $opt_group = false;

                foreach ($mail_temp as $key => $value) {
                    $replace = [
                        '###ANREDE###' => htmlspecialchars($customer['title'], ENT_QUOTES),
                        '###NAME###' => htmlspecialchars($customer['lastname'], ENT_QUOTES),
                    ];

                    if ($customer['title'] == 'Herr') {
                        $replace['geehrte'] = 'geehrter';
                    }

                    $value['text'] = str_replace(array_keys($replace), $replace, $value['text']);
                    $email_link = $customer['emailaddress'].'####'.$value['subject'].'####'.$value['text'];
                    $key = $key == '-n-' ? '' : $key;
                    
                    if ($group_id != $value['group_id']) {
                        $group_id = $value['group_id'];
                        
                        if ($opt_group) {
                            echo "</optgroup>\n";
                        }
                        
                        echo "<optgroup label='".$mail_group[$group_id - 1]['name']."'>\n";
                        
                        $opt_group = true; 
                    }
        
                    echo "<option value='".$email_link."'>".$value['name']."</option>\n";
                }
        
                echo "</optgroup>\n";
                
                ?>
        
            </select>
        
            <?php
            if ($rwPurtel) {
                $purtelReseller = \Wisotel\Configuration\Configuration::get('purtelReseller');

                $purtelLogonUrl = 'https://www2.purtel.com/res/'.$purtelReseller.'/index.php?username=%s&passwort=%s'
                    .'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1'
                    .'&admin_ns_suchbegriff='.$purtel_master.'&tab_id=2&marke=VoIP-Status';
                    
                echo '</p><span id="purtelAdmin" class="admin text blue">Purtel Admin:</span>';

                if (isset($_GET['kommando']) && 'Experte' === $_GET['kommando']) {
                    echo sprintf('<span class="fancy-button outline round grey"><a target="_blank" href="%s">%s</a></span>',
                        sprintf($purtelLogonUrl, 
                            \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername'),
                            \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword')
                        ),
                        'Super Admin'
                    )."\n";
                }

                $purtelAdminAccounts = $this->getDoctrine()->getRepository('AppBundle\Entity\PurtelAdminAccount')->findByAuthorizedUser(
                    $this->get('security.token_storage')->getToken()->getUser()
                );

                foreach ($purtelAdminAccounts as $purtelAdminAccount) {
                    echo sprintf('<span class="fancy-button outline round grey"><a target="_blank" href="%s" title="%s">%s</a></span>',
                        sprintf($purtelLogonUrl, $purtelAdminAccount->getUsername(), $purtelAdminAccount->getPassword()),
                        $purtelAdminAccount->getIdentifier(),
                        $purtelAdminAccount->getIdentifier()
                    )."\n";
                }

                echo '</p>';
            }
            ?>
        </p>
    <?php } ?>
</div>

<h3 class="MailForm">Mail senden</h3>
<div class="MailForm"></div>
