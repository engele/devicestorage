<?php

$StartPath = urldecode($_POST['StartPath']);
$SelPath = urldecode($_POST['SelPath']);
$StartURL = urldecode($_POST['StartURL']);
$purtelLogin = urldecode($_POST['PurtelLogin']);
$Purtel = json_decode($_POST['Purtel'], true);
$Purtel2 = json_decode($_POST['Purtel2'], true);

require_once $StartPath.'/vendor/wisotel/configuration/Configuration.php';

$username = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$password = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

$action = 'createcontract';
$action2 = 'changecontract';
$status = 0;
$status2 = 0;
$purtelPassword = '';
$PurtelUrl = '';
$PurtelUrl2 = '';

// Purtelkunde anlegen
if (!$purtelLogin) {
    $PurtelUrl = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($username),
        urlencode($password),
        urlencode($action)
    );

    foreach ($Purtel as $key => $value) {
        $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
    }

    $curl = curl_init($PurtelUrl);
    
    curl_setopt_array($curl, array(
        CURLOPT_URL             => $PurtelUrl,
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));

    $result = curl_exec($curl);

    curl_close($curl);

    $resultArray = str_getcsv($result, ';');

    if($resultArray[0] == '+OK') {
        $status = 1;
    
        foreach($resultArray as $resultKey => $resultValue) {
            if(strpos($resultValue, 'benutzername=') !== false) {
                $purtelLogin = substr($resultValue, 13);
            } else if(strpos($resultValue, 'passwort=') !== false) {
                $purtelPassword = substr($resultValue, 9);
            }
        }
    }
} else {
    $status = 2;
}

if ($purtelLogin) {
    $Purtel2['anschluss'] = $purtelLogin;
    $PurtelUrl2  = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($username),
        urlencode($password),
        urlencode($action2)
    );

    foreach ($Purtel2 as $key => $value) {
        if ($value) {
            $PurtelUrl2 .= "&$key=".urlencode(utf8_decode($value));
        }
    }

    $curl = curl_init($PurtelUrl2);

    curl_setopt_array($curl, array(
        CURLOPT_URL             => $PurtelUrl2,
        CURLOPT_HEADER          => false,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
    ));

    $result = curl_exec($curl);

    curl_close($curl);

    $resultArray = str_getcsv($result, ';');

    if($resultArray[0] == '+OK') {
        $status2 = 1;
    }
} else {
    $status2 = 2;
}

$response = array(
    'purtelLogin' => $purtelLogin,
    'purtelPassword' => $purtelPassword,
    'PurtelUrl' => $PurtelUrl2,
    'status' => $status,
    'status2' => $status2,
);

print_r(json_encode($response));

?>
