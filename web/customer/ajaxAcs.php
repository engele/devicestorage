<?php
  $StartPath  = urldecode($_POST['StartPath']);
  $SelPath    = urldecode($_POST['SelPath']);
  $StartURL   = urldecode($_POST['StartURL']);
  $typeParam  = urldecode($_POST['type']);      # pppoe, voice, both
  $action     = urldecode($_POST['action']);    # activate, modify, delete, deactivate, reactivate 
  $acs        = json_decode($_POST['acs'], true);
  //if ($acs['area_code']) $acs['area_code'] = substr($acs['area_code'], 1);
  if (isset($acs['area_code']) && !empty($acs['area_code'])) {
    $acs['area_code'] = preg_replace('/^0/', '', $acs['area_code']); // strip leading 0 from area-code
  }

  require_once $StartPath.'/_conf/database.inc';
  require_once $StartPath.'/vendor/wisotel/configuration/Configuration.php';
  require_once __DIR__.'/Lib/PppoeUsername/PppoeUsername.php';

  $axirosAcs = \Wisotel\Configuration\Configuration::get('axirosAcs');

  $info       = '';
  $status     = 0;
  $username   = $axirosAcs['username'];
  $password   = $axirosAcs['password'];
  $sql        = "SELECT * FROM purtel_account WHERE cust_id = ".$acs['custId'];
  $cpe_url    = $axirosAcs['url']."/live/CPEManager/DMInterfaces/rest/v1/action/%s";
  $cpe_action = 'DeleteCPEs';
  $cpe_param  = array();
  $acs_url    = $axirosAcs['url']."/live/CPEManager/AXServiceStorage/Interfaces/rest/v1/services/%s/cpeid/%s/action/%s";
  $acs_param  = array();
  $acs_pppoe  = array();
  $acs_voice  = array();
  $pur_voice  = array(); 

  $acs['registrar'] = $axirosAcs['registrar'];

  // doesn't always work this way
  //$acs['acsId']     = substr($acs['macAddress'],7,12);
  //$acs['acsPw']     = substr($acs['macAddress'],-12,12);
  
  // extract mac-address from "macAddress"
  $acs['acsId'] = '';

  if (!empty($acs['macAddress'])) {
    $matchMac = null;

    preg_match('/([0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2})|([0-9a-f]{12})/i', $acs['macAddress'], $matchMac);
    
    if (isset($matchMac[0])) {
        $acs['acsId'] = preg_replace('/[^0-9a-f]/i', '', $matchMac[0]);

        // extract fritzbox-gui-password from "macAddress"
        $matchPw = explode($matchMac[0], $acs['macAddress']);

        if (2 === count($matchPw) && !empty($matchPw[1])) {
            $acs['acsPw'] = $matchPw[1];
        }
    }
  }
  
  if ($acs['acsId']) {
    $acs_pppoe['ServiceIdentifiers'] = array (
      'cpeid' => $acs['acsId']
    );
    $acs_pppoe['CommandOptions'] = array();
    if ($action == 'activate' || $action == 'modify') $acs_pppoe['ServiceParameters'] = array();

    $cpe_param['CPESearchOptions'] = array (
      'cpeid' => $acs['acsId']
    );
    $cpe_param['CommandOptions'] = array();
  }

  $db_purtel_result = $db->query($sql);
  while ($temp = $db_purtel_result->fetch_assoc()) {
    array_push ($pur_voice, $temp);
      
    $acs_param['ServiceIdentifiers'] = array (
      'cpeid'             => $acs['acsId'],
      'directory_number'  => $temp['nummer']
    );
    $acs_param['CommandOptions'] = array();
    if ($action == 'activate' || $action == 'modify') $acs_param['ServiceParameters'] = array();
    array_push ($acs_voice, $acs_param);
  }

  switch ($action) {
    case 'activate':
    case 'modify':
      if ($acs['acsId']) {
        //$acs_pppoe['ServiceParameters']['username'] = $acs['clientId'];

        $pppoePinQuery = $db->query("SELECT `pppoe_pin` FROM `customers` WHERE `id` = ".$acs['custId']);
        $pppoePin = null;
        
        if (1 === $pppoePinQuery->num_rows) {
            $pppoePin = $pppoePinQuery->fetch_assoc();
            $pppoePin = $pppoePin['pppoe_pin'];
            $pppoePinQuery->close();
        }

        $acs_pppoe['ServiceParameters']['username'] = PppoeUsername::create($pppoePin, $acs['clientId']);
        $acs_pppoe['ServiceParameters']['password'] = $acs['password'];

        foreach ($acs_voice as $key => $value) {
            $areaCode = $acs['area_code'];

            if (!empty($pur_voice[$key]['area_code'])) {
                $areaCode = preg_replace('/^0/', '', $pur_voice[$key]['area_code']); // strip leading 0 from area-code
            }

          $acs_voice[$key]['ServiceParameters']['area_code']        = $areaCode;
          $acs_voice[$key]['ServiceParameters']['registrar']        = $acs['registrar'];
          $acs_voice[$key]['ServiceParameters']['username']         = $pur_voice[$key]['purtel_login'];
          $acs_voice[$key]['ServiceParameters']['password']         = $pur_voice[$key]['purtel_password'];
          $acs_voice[$key]['ServiceParameters']['directory_number'] = $pur_voice[$key]['nummer'];
        }
      }
    case 'deactivate':
    case 'reactivate':
    case 'delete':
      if ($acs['version']) $acs['clientId'] .= '_'.$acs['version'];
      if ($acs['acsId']) {
        $acs_post = json_encode($acs_pppoe);
        $acs_post = str_replace('[]', '{}', $acs_post);

        if ($typeParam == 'pppoe' || $typeParam == 'both') {
          $type = 'pppoe';
          $url  = sprintf ($acs_url, $type, $acs['acsId'], $action);

          $curlHandler = curl_init();
          curl_setopt($curlHandler, CURLOPT_URL, $url);
          curl_setopt($curlHandler, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
          curl_setopt($curlHandler, CURLOPT_FRESH_CONNECT, TRUE);
          curl_setopt($curlHandler, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
          curl_setopt($curlHandler, CURLOPT_USERPWD, $username.':'.$password);
          curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curlHandler, CURLOPT_POST, true);
          curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $acs_post);
          $execstr  = curl_exec($curlHandler);
          curl_close($curlHandler);
          $exec     = json_decode($execstr, true);
          $info     .= '<hr>Code: '.$exec['code'].'<br />';
          $info     .= 'Message: '.$exec['message'].'<br />';
          $info     .= 'cpeid: '.$acs['acsId'].'<br />';
          $info     .= 'username: '.$acs['clientId'].'<br /><br />';
          if ($action == 'activate' && $exec['code'] == '500') {
            $action1 = 'modify';
            $url  = sprintf ($acs_url, $type, $acs['acsId'], $action1);

            $curlHandler = curl_init();
            curl_setopt($curlHandler, CURLOPT_URL, $url);
            curl_setopt($curlHandler, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($curlHandler, CURLOPT_FRESH_CONNECT, TRUE);
            curl_setopt($curlHandler, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curlHandler, CURLOPT_USERPWD, $username.':'.$password);
            curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curlHandler, CURLOPT_POST, true);
            curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $acs_post);
            $execstr  = curl_exec($curlHandler);
            curl_close($curlHandler);
            $exec     = json_decode($execstr, true);
            $info     .= '<hr>Code: '.$exec['code'].'<br />';
            $info     .= 'Message: '.$exec['message'].'<br />';
            $info     .= 'cpeid: '.$acs['acsId'].'<br />';
            $info     .= 'username: '.$acs['clientId'].'<br /><br />';
          }
        }

        if ($typeParam == 'voice' || $typeParam == 'both') {
          $type = 'voice';
          $url  = sprintf ($acs_url, $type, $acs['acsId'], $action);

          foreach ($acs_voice as $key => $acs_param) {
            if (isset($acs_param['ServiceParameters']['directory_number']) && empty($acs_param['ServiceParameters']['directory_number'])) {
                // don't send sip without a phonenumber
                $info .= 'Skipped SIP without phonenumber<br />';
                
                continue;
            }

            $acs_post = json_encode($acs_param);
            $acs_post = str_replace('[]', '{}', $acs_post);

            $curlHandler = curl_init();
            curl_setopt($curlHandler, CURLOPT_URL, $url);
            curl_setopt($curlHandler, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($curlHandler, CURLOPT_FRESH_CONNECT, TRUE);
            curl_setopt($curlHandler, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curlHandler, CURLOPT_USERPWD, $username.':'.$password);
            curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curlHandler, CURLOPT_POST, true);
            curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $acs_post);
            $execstr  = curl_exec($curlHandler);
            curl_close($curlHandler);
            $exec     = json_decode($execstr, true);
            $info     .= '<hr>Code: '.$exec['code'].'<br />';
            $info     .= 'Message: '.$exec['message'].'<br />';
            $info     .= 'cpeid: '.$acs['acsId'].'<br />';
            $info     .= 'directory_number: '.$pur_voice[$key]['nummer'].'<br />';
            $info     .= 'area_code: '.$acs['area_code'].'<br />';
            $info     .= 'registrar: '.$acs['registrar'].'<br />';
            $info     .= 'username: '.$pur_voice[$key]['purtel_login'].'<br />';
            $info     .= 'directory_number: '.$pur_voice[$key]['nummer'].'<br /><br />';
            if ($action == 'activate' && $exec['code'] == '500') {
              $action1 = 'modify';
              $url  = sprintf ($acs_url, $type, $acs['acsId'], $action1);

              $curlHandler = curl_init();
              curl_setopt($curlHandler, CURLOPT_URL, $url);
              curl_setopt($curlHandler, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
              curl_setopt($curlHandler, CURLOPT_FRESH_CONNECT, TRUE);
              curl_setopt($curlHandler, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
              curl_setopt($curlHandler, CURLOPT_USERPWD, $username.':'.$password);
              curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($curlHandler, CURLOPT_POST, true);
              curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $acs_post);
              $execstr  = curl_exec($curlHandler);
              curl_close($curlHandler);
              $exec     = json_decode($execstr, true);
              $info     .= '<hr>Code: '.$exec['code'].'<br />';
              $info     .= 'Message: '.$exec['message'].'<br />';
              $info     .= 'cpeid: '.$acs['acsId'].'<br />';
              $info     .= 'directory_number: '.$pur_voice[$key]['nummer'].'<br />';
              $info     .= 'area_code: '.$acs['area_code'].'<br />';
              $info     .= 'registrar: '.$acs['registrar'].'<br />';
              $info     .= 'username: '.$pur_voice[$key]['purtel_login'].'<br />';
              $info     .= 'directory_number: '.$pur_voice[$key]['nummer'].'<br /><br />';
            }
          }
          /** CPE löschen noch nicht aktivv
          if ($action == 'delete' && ($typeParam == 'pppoe' || $typeParam == 'both')) {
            $acs_post = json_encode($cpe_param);
            $acs_post = str_replace('[]', '{}', $acs_post);
            $url  = sprintf ($cpe_url, $cpe_action);
            $curlHandler = curl_init();
            curl_setopt($curlHandler, CURLOPT_URL, $url);
            curl_setopt($curlHandler, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($curlHandler, CURLOPT_FRESH_CONNECT, TRUE);
            curl_setopt($curlHandler, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curlHandler, CURLOPT_USERPWD, $username.':'.$password);
            curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curlHandler, CURLOPT_POST, true);
            curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $acs_post);
            $execstr  = curl_exec($curlHandler);
            curl_close($curlHandler);
            $exec     = json_decode($execstr, true);
            $info     .= '<hr>Code: '.$exec['code'].'<br />';
            $info     .= 'Message: '.$exec['message'].'<br />';
            $info     .= 'cpeid: '.$acs['acsId'].'<br />';
            $info     .= 'username: '.$acs['clientId'].'<br /><br />';
          }
          */
        }
      } else {
        $status = 1;  
        $info = 'Keine Fritzbox angegeben!';
      }
      break;
    default:
      $status = 1;
      $info = "'$action' ist keine unterstützte Funktion!";
      break;
  }

  $db->close();

  $response = array (
    'status'  => $status,
    'info'    => $info,
  );
  print_r (json_encode ($response));
  
?>
