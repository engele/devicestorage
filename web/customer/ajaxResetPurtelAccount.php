<?php

$StartPath = urldecode($_POST['StartPath']);
$SelPath = urldecode($_POST['SelPath']);
$StartURL = urldecode($_POST['StartURL']);
$CustomerExtern = urldecode($_POST['PurtelCustomerExtern']);

require_once $StartPath.'/vendor/wisotel/configuration/Configuration.php';

$username = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$password = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

$action = 'resetkonto';
$art = 3;
$status = 0;

$PurtelUrl = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s&art=%s&kundennummer_extern=%s',
    $username,
    $password,
    $action,
    $art,
    $CustomerExtern
);

$curl = curl_init($PurtelUrl);

curl_setopt_array($curl, array(
    CURLOPT_URL => $PurtelUrl,
    CURLOPT_HEADER => false,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_SSL_VERIFYPEER => false,
));

$result = curl_exec($curl);

curl_close($curl);

$resultArray = str_getcsv($result, ';');

if ($resultArray[0] == '+OK') {
    $status = 1;
}

$response = array('status' => $status);

print_r(json_encode($response));

?>
