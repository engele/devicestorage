<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_CONNECTION_REQUEST');

?>
<h3 class="CustConnectingRequest">Schaltauftrag</h3>
<div class="CustConnectingRequest form">
    <?php
    $address = $customer['title'] == 'Herr' ? "Sehr geehrter" : "Sehr geehrte";

    # '#productname#'                 => $option_productname[$customer['productname']],
    $connect_request_replace = array(
        '#clientid#'                    => $customer['clientid'],
        '#lastname#'                    => $customer['lastname'],
        '#firstname#'                   => $customer['firstname'],
        '#tal_order_ack_date#'          => $customer['tal_order_ack_date'],
        '#terminal_type#'               => $customer['terminal_type'],
        '#mac_address#'                 => $customer['mac_address'],
        '#terminal_serialno#'           => $customer['terminal_serialno'],
        '#password#'                    => $customer['password'],
        '#street#'                      => $customer['street'],
        '#ip_address#'                  => $customer['ip_address'],
        '#streetno#'                    => $customer['streetno'],
        '#subnetmask#'                  => $customer['subnetmask'],
        '#mobilephone#'                 => $customer['mobilephone'],
        '#patch_date#'                  => $customer['patch_date'],
        '#kvz_name#'                    => $customer['kvz_name'],
        '#add_phone_lines#'             => $customer['add_phone_lines'],
        '#kvz_toggle_no#'               => $customer['kvz_toggle_no'],
        '#new_or_ported_phonenumbers#'  => $customer['new_or_ported_phonenumbers'],
        '#lsa_pin#'                     => $customer['lsa_pin'],
        '#dslam_card_no#'               => $customer['dslam_card_no'],
        '#dslam_port#'                  => $customer['dslam_port']->getNumber(),
        '#new_numbers_text#'            => $customer['new_numbers_text'],
        '#reached_downstream#'          => $customer['reached_downstream'],
        '#tal_product#'                 => $customer['tal_product'],
        '#reached_upstream#'            => $customer['reached_upstream'],
        '#purtel_login#'                => $purtel_master,
        '#zusatzaufwand_ja#'            => '',
        '#purtel_passwort#'             => $purtel_masterpw,
        '#hoursofwork#'                 => '',
    );

    $connect_letter_replace = array(
        '#address#'             => $address,
        '#title#'               => $customer['title'],
        '#firstname#'           => $customer['firstname'],
        '#lastname#'            => $customer['lastname'],
        '#street#'              => $customer['street'],
        '#streetno#'            => $customer['streetno'],
        '#zipcode#'             => $customer['zipcode'],
        '#city#'                => $customer['city'],
        '#Date#'                => date("d.m.Y"),
        '#tal_order_ack_date#'  => $customer['tal_order_ack_date'],
        '#clientid#'            => $customer['clientid'],
        '#password#'            => $customer['password'],
        '#phoneareacode#'       => $customer['phoneareacode'],
        '#phonenumber#'         => $customer['phonenumber'],
        '#purtel_login#'        => $purtel_master,
        '#purtel_passwort#'     => $purtel_masterpw,
        '#terminal_type#'       => $customer['terminal_type'],
        '#terminal_serialno#'   => $customer['terminal_serialno'],
        '#remote_login#'        => $customer['remote_login'],
        '#remote_password#'     => $customer['remote_password'],
    );

    $umlaut = array(
        'ä' => 'ae',
        'ü' => 'ue',
        'ö' => 'oe',
        'é'	=> 'e',
        'è'	=> 'e',
        'ú'	=> 'u',
        'ù' => 'u',
        'á' => 'a',
        'à' => 'a',
        'Ä' => 'AE',
        'Ü' => 'UE',
        'Ö' => 'OE',
        'ß' => 'ss',
    );

    echo "<input name='connect_request_replace' id='connect_request_replace' type='hidden' value='".urlencode(json_encode($connect_request_replace))."'>";
    echo "<input name='connect_letter_replace' id='connect_letter_replace' type='hidden' value='".urlencode(json_encode($connect_letter_replace))."'>";
    echo "<input name='umlaut' id='umlaut' type='hidden' value='".urlencode(json_encode($umlaut))."'>";

    //echo "<a href='#' target='_blank' id='connectingLetter1' class='button'>Anschaltungsschreiben DSL</a>&nbsp;&nbsp;&nbsp;&nbsp;";
    //echo "<a href='#' target='_blank' id='connectingLetter2' class='button'>Anschaltungsschreiben Mobilfunk</a>&nbsp;&nbsp;&nbsp;&nbsp;";
    //echo "<a href='#' target='_blank' id='connectingLetter3' class='button'>Anschaltungsschreiben Glasfaser</a>";

    $counter = 0;

    foreach ($this->container->getParameter('customer_letters') as $slug => $letterData) {
        echo sprintf('<nobr><a class="button" href="%s" target="_blank" style="background-color: rgb(234, 234, 234);">%s</a></nobr>&nbsp;&nbsp;&nbsp;&nbsp;',
            $this->generateUrl('customer-letter-create', ['slug' => $slug, 'customerId' => $customer['id']]),
            $letterData['name']
        );

        $counter++;

        if ($counter > 2) {
            echo '<br /><br />';

            $counter = 0;
        }
    }
    ?>
    
</div>
