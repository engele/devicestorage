<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_PURTEL');

$disable = 'readonly="readonly"';
$rw = false;   

if ($authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_PURTEL')) {
    $disable = '';   
    $rw = true;   
}

?>

<h3 class="CustPurtel">Purtel</h3>
<div class="CustPurtel form">
    <span class="double">
        <label for="CustomerClientid13"><?php echo \Wisotel\Configuration\Configuration::get('companyName'); ?> Nr.</label><br />
        <input type="text" id="CustomerClientid13" value="<?php setInputValue ($customer, 'clientid'); ?>" readonly="readonly" name="CustomerClientid">
    </span>
    <span>
        <label for="CustomerPurtelUseVersion13">Purtel-Kundenr. mit Version</label><br />
        <select id="CustomerPurtelUseVersion13" name="CustomerPurtelUseVersion" <?php echo $disable; ?>>
        <?php setOption ($option_purtel_use_version, $customer, 'purtel_use_version'); ?>
        </select>
    </span>
    <span></span>
    
    <span class="double">
        <label for="CustomerPurtelCustomerRegDate13">Kunde bei Purtel angelegt am</label><br />
        <input type="text" id="CustomerPurtelCustomerRegDate13" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'purtel_customer_reg_date'); ?>" name="CustomerPurtelCustomerRegDate">
    </span>
    <span class="double">
        <?php if ($rw) { ?>
        <br \>
        <button id="PurtelContract" onclick="changePurtelContract()" type="button">Stammdaten bei Purtel ändern</button>&nbsp;
        <button id="PurtelReset" onclick="resetPurtelAccount()" type="button">Purtel-Kundenkonto Reset</button>
        <br />
        <?php } ?>  
    </span>
    
    <span>
        <label for="CustomerCancelPurtel13">Purteldaten kündigen</label><br />
        <select id="CustomerCancelPurtel13" name="CustomerCancelPurtel" <?php echo $disable; ?>>
        <?php setOption ($option_cancel_purtel, $customer, 'cancel_purtel'); ?>
        </select>
    </span>
    <span>
        <label for="CustomerCancelPurtelForDate13">Daten kündigen zum</label>
        <input type="text" id="CustomerCancelPurtelForDate13" class="datepicker"  <?php echo $disable; ?> value="<?php setInputValue ($customer, 'cancel_purtel_for_date'); ?>" name="CustomerCancelPurtelForDate">
    </span>
    <span class="double">
        <label for="CustomerPurtelAdminChoiceNoSubmit13">Adminzugriff auf Kunde bei Purtel.de</label><br />
        <select id="CustomerPurtelAdminChoiceNoSubmit13" name="CustomerPurtelAdminChoiceNoSubmit" <?php echo $disable; ?>>
        <?php setOption ($option_purtel_admin_choice_no_submit, $customer, 'purtel_login'); ?>
        </select>
    </span>

    <span class="double">
        <label class="" for="CustomerPurtelDeleteDate13">Purteldaten gelöscht am</label><br />
        <input type="text" id="CustomerPurtelDeleteDate13" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'purtel_delete_date'); ?>" name="CustomerPurtelDeleteDate">
    </span>
    <span>
        <label for="CustomerPurtelLogin13">Purtel Login</label><br />
        <input type="text" id="CustomerPurtelLogin13" value="<?php setInputValue ($customer, 'purtel_login'); ?>" name="CustomerPurtelLogin" readonly='readonly'>
    </span>
    <span>
        <label for="CustomerPurtelPasswort13">Purtel Passwort</label><br />
        <input type="text" id="CustomerPurtelPasswort13" value="<?php setInputValue ($customer, 'purtel_passwort'); ?>" name="CustomerPurtelPasswort" readonly='readonly'>
    </span>

    <span>
        <label for="CustomerPurtelBluckageSendedDate13">Purtel StöMe gesend. am</label><br />
        <input type="text" id="CustomerPurtelBluckageSendedDate13" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'purtel_bluckage_sended_date'); ?>" name="CustomerPurtelBluckageSendedDate">
    </span>
    <span>
        <label for="CustomerPurtelBluckageFullfiledDate13">Purtel StöMe behob. am</label><br />
        <input type="text" id="CustomerPurtelBluckageFullfiledDate13" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'purtel_bluckage_fullfiled_date'); ?>" name="CustomerPurtelBluckageFullfiledDate">
    </span>
    <span class="double">
        <label for="CustomerPurtelTelnr13">Purtel Telefonnr.</label><br />
        <input type="text" id="CustomerPurtelTelnr13" value="<?php setInputValue ($customer, 'purtel_telnr'); ?>" name="CustomerPurtelTelnr" readonly='readonly'>
    </span>
</div>
