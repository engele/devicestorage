<div id="Button">
    <h4></h4><hr>
    <ul>
        <?php

        $authorizationChecker = $this->get('security.authorization_checker');

        if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_PROCEDURE')) {
            ?>
            <li id="ButProcedure">Ablauf</li>
            <?php
        }

        if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_MASTER_DATA')) {
            ?>
            <li id="ButMasterData">Stammdaten</li>
            <?php
        }

        if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_CONTRACT')) {
            ?>
            <li id="ButContract">Vertragsdaten</li>
            <li id="ButOldContract">Altvertrag</li>
            <?php
        }

        if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_PROVIDER_CHANGE')) {
            ?>
            <li id="ButWbci">Anbieterwechsel</li>
            <?php
        }

        if (isset($customer['clientid']) && strpos($customer['clientid'], '0000') !== 0 
            && $authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_PURTEL')) {
            ?>
            <li id="ButPurtel">Purtel</li>
            <?php
        }

        if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_TAL')) {
            ?>
            <li id="ButTal">TAL</li>
            <?php
        }

        if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_VLAN')) {
            ?>
            <li id="ButVlan">VLAN</li>
            <?php
        }

        if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_TECH_DATA')) {
            ?>
            <li id="ButTechData">Techn. Daten</li>
            <?php
        }

        if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_TECH_CONFIGURATION')) {
            ?>
            <li id="ButTechConf">Techn. Einricht.</li>
            <?php
        }

        if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_PROGRESS')) {
            ?>
            <li id="ButProgress">Verlauf</li>
            <?php
        }

        if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_CONNECTION_REQUEST')) {
            ?>
            <li id="ButConnectingRequest">Schaltauftrag</li>
            <?php
        }

        if ($authorizationChecker->isGranted('ROLE_VIEW_CUSTOMER_ABROGATE')) {
            ?>
            <li id="ButAbrogate">Kündigung</li>
            <?php
        }

        ?>
        
        <li id="ButStatus">Status</li>
    </ul>
</div>