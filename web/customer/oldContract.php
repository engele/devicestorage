<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_CONTRACT');

$disable = 'readonly="readonly"';
$rw = false;   

if ($authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_CONTRACT')) {
    $disable = '';   
    $rw = true;   
}

?>

<h3 class="CustOldContract">Altvertrag</h3>
<div class="CustOldContract form">
  <span class="doublebox">
    <span class="double">
      <label for="CustomerClientid08"><?php echo \Wisotel\Configuration\Configuration::get('companyName'); ?> Nr.</label><br />
      <input type="text" id="CustomerClientid08" value="<?php setInputValue ($customer, 'clientid'); ?>" readonly="readonly" name="CustomerClientid">
    </span>
    <span>
      <label for="CustomerOldcontractExists08">Altvertrag vorhanden</label><br />
      <select id="CustomerOldcontractExists08" name="CustomerOldcontractExists" <?php echo $disable; ?>>
        <?php setOption ($option_oldcontract_exists, $customer, 'oldcontract_exists'); ?>
      </select>
    </span>
    <span>
      <label for="CustomerAddPhoneNos08">Zusätzliche Tel.-Nr.</label><br />
      <select id="CustomerAddPhoneNos08" name="CustomerAddPhoneNos" <?php echo $disable; ?>>
        <?php setOption ($option_add_phone_nos, $customer, 'add_phone_nos'); ?>
      </select>
    </span>
  </span>
  <span class="double">
    <label for="CustomerNewOrPortedPhonenumbers08">Rufnummern</label>
    <textarea id="CustomerNewOrPortedPhonenumbers08" class="small" name="CustomerNewOrPortedPhonenumbers" <?php echo $disable; ?>><?php setInputValue ($customer, 'new_or_ported_phonenumbers'); ?></textarea>
  </span>
 
  <span>
      <label for="CustomerOldcontractPorting08">Rufnummernportierung</label><br />
      <select id="CustomerOldcontractPorting08" name="CustomerOldcontractPorting" <?php echo $disable; ?>>
        <?php setOption ($option_oldcontract_porting, $customer, 'oldcontract_porting'); ?>
      </select>
  </span>
  <span>
      <label for="CustomerOldcontractPhoneType08">Anschlusstyp</label><br />
      <select id="CustomerOldcontractPhoneType08" name="CustomerOldcontractPhoneType" <?php echo $disable; ?>>
        <?php setOption ($option_oldcontract_phone_type, $customer, 'oldcontract_phone_type'); ?>
      </select>
  </span>
  <span>
    <label for="CustomerNewNumberDate08">Neue RN bestellt am</label><br /> 
    <input type="text" id="CustomerNewNumberDate08" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'new_number_date'); ?>" name="CustomerNewNumberDate">
  </span>
  <span>
    <label for="CustomerNewNumberFrom08">bestellt von</label><br />
    <select id="CustomerNewNumberFrom08" name="CustomerNewNumberFrom" <?php echo $disable; ?>>
      <?php setOption ($option_new_number_from, $customer, 'new_number_from'); ?>
    </select>
  </span>

  <span class="doublebox">
    <span  class="double">
      <br /><?php if ($rw) { ?><button onclick="copyMasterdataToOldcontract()" type="button">Adresse aus Stammdaten einfügen</button><?php } ?>
    </span>
    <span>
      <label for="CustomerOldcontractTitle08">Altvertrag Anrede</label><br />
      <select id="CustomerOldcontractTitle08" name="CustomerOldcontractTitle" <?php echo $disable; ?>>
        <?php setOption ($option_oldcontract_title, $customer, 'oldcontract_title'); ?>
      </select>
    </span>
    <span></span>
    <span>
      <label for="CustomerOldcontractFirstname08">Altvertrag Vorname</label><br />
      <input type="text" id="CustomerOldcontractFirstname08" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'oldcontract_firstname'); ?>" name="CustomerOldcontractFirstname">
    </span>
    <span>
      <label for="CustomerOldcontractLastname08">Altvertrag Nachname</label><br />
      <input type="text" id="CustomerOldcontractLastname08" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'oldcontract_lastname'); ?>" name="CustomerOldcontractLastname">
    </span>
  </span>
  <span class="double">
    <label for="CustomerNewNumbersText08">Neue Rufnummern</label><br />
    <textarea id="CustomerNewNumbersText08" name="CustomerNewNumbersText" <?php echo $disable; ?>><?php setInputValue ($customer, 'new_numbers_text'); ?></textarea>
  </span>

  <span class="doublebox">
    <span>
      <label for="CustomerOldcontractStreet08">Altvertrag Straße</label><br />
      <input type="text" id="CustomerOldcontractStreet08" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'oldcontract_street'); ?>" name="CustomerOldcontractStreet">
    </span>
    <span>
      <label for="CustomerOldcontractStreetno08">Altvertrag Hausnummer</label><br />
      <input type="text" id="CustomerOldcontractStreetno08" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'oldcontract_streetno'); ?>" name="CustomerOldcontractStreetno">
    </span>
    <span>
      <label for="CustomerOldcontractZipcode08">Altvertrag PLZ</label><br />
      <input type="text" id="CustomerOldcontractZipcode08" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'oldcontract_zipcode'); ?>" name="CustomerOldcontractZipcode">
    </span>
    <span>
      <label for="CustomerOldcontractCity08">Altvertrag Stadt</label><br />
      <input type="text" id="CustomerOldcontractCity08" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'oldcontract_city'); ?>" name="CustomerOldcontractCity">
    </span>
  </span>
  <span class="double">
    <label for="CustomerOldcontractComment08">Altvertrag Kommentar</label><br />
    <textarea id="CustomerOldcontractComment08" class="small" name="CustomerOldcontractComment" <?php echo $disable; ?>><?php setInputValue ($customer, 'oldcontract_comment'); ?></textarea>
  </span>
    
  <span class="double"><br /><h4>Internet</h4></span>
  <span class="double"><br /><h4>Telefon</h4></span>

  <span class="double"><br /><?php if ($rw) { ?><button onclick="fillOldContractInetData()" type="button">Kein Internetvertrag</button><?php } ?></span>
  <span class="double"><br /><?php if ($rw) { ?><button onclick="fillOldContractPhoneData()" type="button">Kein Telefonvertrag</button><?php } ?></span>

  <span class="double"><br /><?php if ($rw) { ?><button onclick="copyOldContractInetData()" type="button">Int.vertragsdaten eintragen</button><?php } ?></span>
  <span class="double"><br /><?php if ($rw) { ?><button onclick="copyOldContractPhoneData()" type="button">Tel.vertragsdaten eintragen</button><?php } ?></span>

  <span class="double">
    <label for="CustomerOldcontractInternetProvider08">Anbieter Internet</label><br />
    <input type="text" id="CustomerOldcontractInternetProvider08" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'oldcontract_internet_provider'); ?>" name="CustomerOldcontractInternetProvider">
  </span>
  <span class="double">
    <label for="CustomerOldcontractPhoneProvider08">Anbieter Telefon</label><br />
    <input type="text" id="CustomerOldcontractPhoneProvider08" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'oldcontract_phone_provider'); ?>" name="CustomerOldcontractPhoneProvider">
  </span>

  <span class="double">
    <label for="CustomerOldcontractInternetClientno08">Kundennummer</label><br />
    <input type="text" id="CustomerOldcontractInternetClientno08" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'oldcontract_internet_clientno'); ?>" name="CustomerOldcontractInternetClientno">
  </span>
  <span class="double">
    <label for="CustomerOldcontractPhoneClientno08">Kundennummer</label><br />
    <input type="text" id="CustomerOldcontractPhoneClientno08" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'oldcontract_phone_clientno'); ?>" name="CustomerOldcontractPhoneClientno">
  </span>

  <span>
    <label for="CustomerExpDateInt08">Kündigungstermin Internet</label><br />
    <input type="text" id="CustomerExpDateInt08" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'exp_date_int'); ?>" name="CustomerExpDateInt">
  </span>
  <span>
    <label for="CustomerExpDoneInt08">Anbieterwechsel abgeschl. am</label><br />
    <input type="text" id="CustomerExpDoneInt08" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'exp_done_int'); ?>" name="CustomerExpDoneInt">
  </span>
  <span>
    <label for="CustomerExpDatePhone08">Kündigungstermin Telefon</label><br />
    <input type="text" id="CustomerExpDatePhone08" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'exp_date_phone'); ?>" name="CustomerExpDatePhone">
  </span>
  <span>
    <label for="CustomerExpDonePhone08">Anbieterwechsel abgeschl. am</label><br />
    <input type="text" id="CustomerExpDonePhone08" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'exp_done_phone'); ?>" name="CustomerExpDonePhone">
  </span>

  <span>
    <label for="CustomerNoticePeriodInternetInt08">Kündigungsfrist Internet</label><br />
    <input type="text" id="CustomerNoticePeriodInternetInt08" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'notice_period_internet_int'); ?>" name="CustomerNoticePeriodInternetInt">
  </span>
  <span>
    <br />
    <select id="CustomerNoticePeriodInternetType" name="CustomerNoticePeriodInternetType" <?php echo $disable; ?>>
      <?php setOption ($option_notice_period_internet_type, $customer, 'notice_period_internet_type'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerNoticePeriodPhoneInt08">Kündigungsfrist Telefon</label><br />
    <input type="text" id="CustomerNoticePeriodPhoneInt08" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'notice_period_phone_int'); ?>" name="CustomerNoticePeriodPhoneInt">
  </span>
  <span>
    <br />
    <select id="CustomerNoticePeriodPhoneType" name="CustomerNoticePeriodPhoneType" <?php echo $disable; ?>>
      <?php setOption ($option_notice_period_phone_type, $customer, 'notice_period_phone_type'); ?>
    </select>
  </span>
  
  <span>
    <label for="CustomerSpecialTerminationInternet08">Sonderkündigung nötig?</label>
    <select id="CustomerSpecialTerminationInternet08" name="CustomerSpecialTerminationInternet" <?php echo $disable; ?>>
      <?php setOption ($option_special_termination_internet, $customer, 'special_termination_internet'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerOldcontractInternetClientCancelled08">Hat Kunde gekündigt?</label>
    <select id="CustomerOldcontractInternetClientCancelled08" name="CustomerOldcontractInternetClientCancelled" <?php echo $disable; ?>>
      <?php setOption ($option_oldcontract_internet_client_cancelled, $customer, 'oldcontract_internet_client_cancelled'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerSpecialTerminationPhone08">Sonderkündigung nötig?</label><br />
    <select id="CustomerSpecialTerminationPhone08" name="CustomerSpecialTerminationPhone" <?php echo $disable; ?>>
      <?php setOption ($option_special_termination_phone, $customer, 'special_termination_phone'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerOldcontractPhoneClientCancelled08">Hat Kunde gekündigt?</label><br />
    <select id="CustomerOldcontractPhoneClientCancelled08" name="CustomerOldcontractPhoneClientCancelled" <?php echo $disable; ?>>
      <?php setOption ($option_oldcontract_phone_client_cancelled, $customer, 'oldcontract_phone_client_cancelled'); ?>
    </select>
  </span>
  
</div>

