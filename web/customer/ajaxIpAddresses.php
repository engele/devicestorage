<?php
/**
 * 
 */

namespace customer;

require __DIR__.'/../_conf/database.inc';
require __DIR__.'/_conf/database.inc';

/**
 * 
 */
class Controller
{
	/**
	 * Maps route value to method
	 * 
	 * @var array
	 */
	public $routesToMethods = [
		'get-ip-range' => 'getIpRange',
		'get-ip-range-defaults' => 'getIpRangeDefaults'
	];

	/**
	 * Instance of mysqli
	 * 
	 * @var \mysqli
	 */
	private $db;

	/**
	 * $sql from _conf/database.inc
	 * 
	 * @var array
	 */
	private $sql;

	/**
     * Constructor
     * 
     * @param string $route
     * 
     * @throws \InvalidArgumentException
     */
    public function __construct(\mysqli $db, array $sql, $route)
    {
        if (!is_string($route) || empty($route))  {
            throw new \InvalidArgumentException('Invalid route');
        }

        if (!isset($this->routesToMethods[$route])) {
            throw new \InvalidArgumentException('No match for route found ('.$route.')');
        }

        $this->db = $db;
        $this->sql = $sql;

        $this->{$this->routesToMethods[$route]}();
    }

    /**
     * Encode and output $response as json object
     * 
     * @param array $response
     * 
     * @return void
     */
    public function outputJson(array $response = array())
    {
    	echo json_encode($response);

    	exit;
    }

    /**
     * Get ipRanges matching $_GET['network-id']
     * 
     * @throws \InvalidArgumentException - if undefined $_GET['network-id']
     * 
     * @return void
     */
    public function getIpRange()
    {
    	if (!isset($_GET['network-id'])) {
    		throw new \InvalidArgumentException('Undefined $_GET[\'network-id\']');
    	}

    	require_once __DIR__.'/Lib/IpAddress/IpRangeCollection.php';

        $ipRanges = new \customer\Lib\IpAddress\IpRangeCollection($this->db, $this->sql);
        $ipRanges->fetchByNetworkId((int) $_GET['network-id']);

        $this->outputJson(array_map(function($ipRange) {
        	return unserialize($ipRange->serialize());
        }, $ipRanges->toArray()));
    }

    /**
     * Get ipAddresses and subnetmask matching $_GET['network-id'] and $_GET['range-id']
     * 
     * @throws \InvalidArgumentException - if undefined $_GET['network-id'] or undefined $_GET['range-id']
     * 
     * @return void
     */
    public function getIpRangeDefaults()
    {
    	if (!isset($_GET['network-id'])) {
    		throw new \InvalidArgumentException('Undefined $_GET[\'network-id\']');
    	}
    	if (!isset($_GET['range-id'])) {
    		throw new \InvalidArgumentException('Undefined $_GET[\'range-id\']');
    	}

    	require_once __DIR__.'/Lib/IpAddress/IpRangeCollection.php';

        $ipRanges = new \customer\Lib\IpAddress\IpRangeCollection($this->db, $this->sql);
        $ipRanges->fetchByNetworkId((int) $_GET['network-id']);

        $ipRange = $ipRanges->findRangeById((int) $_GET['range-id']);
        $availableIpAddresses = $ipRange->getAvailableIpAddresses();

        $ipAddresses = [];

		foreach ($availableIpAddresses as $ipAddress) {
			$ipAddresses[] = $ipAddress->getIpAddress();
		}

		$this->outputJson([
			'ipAddresses' => $ipAddresses,
			'subnetmask' => $ipRange->getSubnetmask(),
		]);
    }
}

try {
	new Controller($db, $sql, $_GET['action']);
} catch (\Exception $e) {
	exit(json_encode([
		$e->getMessage(),
	]));
}

?>
