<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_PROGRESS');

$disable = 'readonly="readonly"';
$rw = false;   

if ($authorizationChecker->isGranted('ROLE_CREATE_CUSTOMER_PROGRESS')) {
    $disable = '';   
    $rw = true;   
}

?>

<h3 class="CustProgress">Verlauf</h3>
<div class="CustProgress form">
  <span class="four">
    <select name="customer_history[event]" id="customer-history-event">
      <option value="">Aktion ausw&auml;hlen</option>
      <?php
        foreach ($historyEventList->getGroupedEventList() as $group => $events) {
            if ($group !== 'ungrouped') {
                echo '<optgroup label="'.$group.'">';
            }

            foreach ($events as $event) {
                echo '<option value="'.$event->getId().'">'.$event->getEvent().'</option>';
            }

            if ($group !== 'ungrouped') {
                echo '</optgroup>';
            }
        }

        $customerHistoryNote = $request->getSession()->getFlashBag()->get('customer.createHistory.missingFields');
        $customerHistoryNote = !empty($customerHistoryNote) ? current($customerHistoryNote) : null;
      ?>
    </select>
    <br />
    <textarea style="height: auto;" rows="15" name="customer_history[note]" <?php echo $disable; ?>><?php echo $customerHistoryNote; ?></textarea>
    <br />
    <br />
  </span>
</div>
<?php if($rw) { ?>
<h3 class="CustProgressSave"></h3>
<div class="CustProgressSave form">
  <button name="save" onclick="custSubmit()" type="button"><img alt="" src="<?php echo $StartURL.'/_img/Yes.png'; ?>" height="12px"> Speichern</button>&nbsp;&nbsp;&nbsp;&nbsp;
  <button name="reset" onclick="custReset()" type="reset"><img alt="" src="<?php echo $StartURL.'/_img/Erase.png'; ?>" height="12px"> Zurücksetzen</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</div>
<?php } ?>
<h3 class="CustProgress"></h3>
<div class="CustProgress form">
    <div id="progress-filter-wrap"></div>
  <span class="four" id="progress-list-wrap">
    <?php
      $separator = '<hr>';

      foreach ($customer_addSomethingToThisNameToMakeItUnique->getHistory() as $key => $history) {
        if($rw) {
            echo sprintf('<div data-event="%u"><strong>[%s %s] %s</strong><br /><br /><div class="history-item-text">%s</div><br />'
                .'<div class="clear-float"><button data-item-id="%u" class="button-edit-history-item right fancy-button outline purple round" type="button">Bearbeiten</button></div>',
                $history->getEvent()->getId(),
                $history->getCreatedAt()->format('d.m.Y H:i'),
                $history->getCreatedByFullname(),
                $history->getEvent()->getEvent(),
                nl2br($history->getNote()),
                $history->getId()
            );
        } else {
            echo sprintf('<div data-event="%u"><strong>[%s %s] %s</strong><br /><br /><div class="history-item-text">%s</div><br />'
                .'<div class="clear-float"></div>',
                $history->getEvent()->getId(),
                $history->getCreatedAt()->format('d.m.Y H:i'),
                $history->getCreatedByFullname(),
                $history->getEvent()->getEvent(),
                nl2br($history->getNote()),
                $history->getId()
            );
        }

        if ($customer_addSomethingToThisNameToMakeItUnique->getHistory()->count() - 1 != $key) {
          echo $separator;
        }

        echo '</div>';
      }

      // old history
      echo '<hr />';
      $customer['history'] = str_replace("\n", "<br>", $customer['history']);
      setInputValue ($customer, 'history');
    ?>
  </span>
</div>
