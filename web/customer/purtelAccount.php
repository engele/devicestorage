<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_PURTEL_ACCOUNT');

$disable = 'readonly="readonly"';
$rw = false;   

if ($authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_PURTEL_ACCOUNT')) {
    $disable = '';   
    $rw = true;   
}

?>

<h3 class="CustPurtelAccount">Purtel Zugänge</h3>

<div class="CustPurtelAccount form">
    <div class="CustPurtelList">
        <?php
        $div_login = 0;
        $purtelReseller = \Wisotel\Configuration\Configuration::get('purtelReseller');
        $allowedToDeleteAccounts = $authorizationChecker->isGranted('ROLE_DELETE_PURTEL_ACCOUNT');

        foreach ($purtel_account as $key => $value) {
            if ($div_login != $value['purtel_login']) { // @todo does not work when purtel_login is not numeric!
                if ($div_login != 0) {
                    echo "<hr>";
                }
                $div_login = $value['purtel_login'];  

                echo "<span>\n<strong><label for='PurtelPurtelLogin14_".$key."'>Purtel Login";
                if ($rw) {
                    echo sprintf(" (<a target='_blank' href='https://www2.purtel.com/res/%s/index.php?username=%s&amp;"
                        ."passwort=%s&amp;senden=1&amp;site=start&amp;link=login'>Kunde</a>)",
                        $purtelReseller,
                        $value['purtel_login'],
                        $value['purtel_password']
                    );
                }

                echo "</label></strong><br />";
                
                if ($value['master']) {
                    echo "<input type='text' class='purtelmaster' id='PurtelPurtelLogin14_".$key."' value='";
                } else {
                    echo "<input type='text' id='PurtelPurtelLogin14_".$key."' value='";
                }

                setInputValue($value, 'purtel_login');
                
                echo "' name='PurtelPurtelLogin_".$key."' readonly='readonly'>"
                    ."</span>\n<span>\n"
                    ."<label for='PurtelPurtelPassword14_".$key."'>Purtel Passwort</label><br />"
                    ."<input type='text' $disable id='PurtelPurtelPassword14_".$key."' value='";
                
                setInputValue($value, 'purtel_password');
                
                echo "' name='PurtelPurtelPassword_".$key."'>"
                    ."</span>\n<span>\n"
                    ."<label for='PurtelMaster14_".$key."'>Hauptzugang</label><br />\n"
                    ."<select id='PurtelMaster14_".$key."' name='PurtelMaster_".$key."' disabled='disabled'>\n";
                
                setOption($option_master, $value, 'master');
                
                echo "</select>\n"
                    ."</span>\n"
                    ."<span>\n"
                    ."</span>\n";
            }

            if ('Stadtwerke Bühl' !== \Wisotel\Configuration\Configuration::get('companyName')) {
                echo "<hr width='70%'>\n<span>\n"
                    . "<label for='PurtelNummer14_" . $key . "'><strong>ONKZ</strong></label><br />\n"
                    . "<input $disable type='text' id='PurtelAreaCode14_" . $key . "' value='\n";
                setInputValue($value, 'area_code');
                echo "' name='PurtelAreaCode_" . $key . "'>\n"
                    . "</span><span></span><span></span>";
            }

            echo "<span style='text-align: right;'>";

            if ($allowedToDeleteAccounts) {
                echo "<a class='delete-purtel-account-link' href='" . $this->generateUrl('delete-purtel-account', ['id' => $value['id'], 'customerId' => $value['cust_id']]) . "' title='Diesen Purtel-Account löschen?'>"
                    . "<img alt='' src='" . $StartURL . '/_img/delete.png' . "' height='12px'>"
                    . "</a>";
            }

            echo "</span>\n";

            echo "<span>\n"
                ."<label for='PurtelNummer14_".$key."'><strong>Telefonnummer</strong></label><br />\n" 
                ."<input $disable type='text' id='PurtelNummer14_".$key."' value='\n";
            setInputValue($value, 'nummer');
            echo "' name='PurtelNummer_".$key."'>\n"
                ."</span>\n";
                
            echo "<span>\n"
                ."<label for='PurtelNummernblock14_".$key."'>Nummernblock</label><br />\n"
                ."<input $disable type='text' id='PurtelNummernblock14_".$key."' value='\n";
            setInputValue($value, 'nummernblock');
            echo "' name='PurtelNummernblock_".$key."'>\n"
                ."</span>\n";
                
            echo "<span>\n"
                ."<label for='PurtelTyp14_".$key."'>Typ</label><br />\n"
                ."<select id='PurtelTyp14_".$key."' name='PurtelTyp_".$key."' $disable>\n";
            setOption($option_purtel_teltype, $value, 'typ');
            echo "</select>\n"
                ."</span>\n";
                
            echo "<span>\n"
                ."<label for='PurtelPorting14_".$key."'>Telefonnr portieren</label><br />\n"
                ."<select id='PurtelPorting14_".$key."' name='PurtelPorting_".$key."' $disable>\n";
            setOption($option_porting, $value, 'porting');
            echo "</select>\n</span>\n";

            echo "<span>\n<br \><strong>Telefonbucheintrag</strong></span>\n";
            
            echo "<span>\n"
                ."<label for='PurtelPhoneBookInformation14_".$key."'>Auskunft</label><br />\n"
                ."<select id='PurtelPhoneBookInformation14_".$key."' name='PurtelPhoneBookInformation_".$key."'>";
            setOption($option_no_ping, $value, 'phone_book_information');
            echo "</select>\n</span>\n";

            echo "<span>\n"
                ."<label for='PurtelPhoneBookDigitalMedia14_".$key."'>Digitale Medien</label><br />\n"
                ."<select id='PurtelPhoneBookDigitalMedia14_".$key."' name='PurtelPhoneBookDigitalMedia_".$key."'>";
            setOption($option_no_ping, $value, 'phone_book_digital_media');
            echo "</select></span>";

            echo "<span>\n"
                ."<label for='PurtelPhoneBookInverseSearch14_".$key."'>Inverssuche</label><br />\n"
                ."<select id='PurtelPhoneBookInverseSearch14_".$key."' name='PurtelPhoneBookInverseSearch_".$key."'>";
            setOption($option_no_ping, $value, 'phone_book_inverse_search');
            echo "</select>\n</span>\n";

            echo "<span><br /><strong>Einzelverbindungsnachweis</strong></span>\n";
     
            echo "<span>\n"
                ."<label for='PurtelItemizedBill14_".$key."'>ja/nein</label><br />\n"
                ."<select id='PurtelItemizedBill14_".$key."' name='PurtelItemizedBill_".$key."'>";
            setOption($option_no_ping, $value, 'itemized_bill');
            echo "</select>\n</span>\n";

            echo "<span>\n"
                ."<label for='PurtelItemizedBillAnonymized14_".$key."'>anonymisiert</label>\n"
                ."<select id='PurtelItemizedBillAnonymized14_".$key."' name='PurtelItemizedBillAnonymized_".$key."'>";
            setOption($option_no_ping, $value, 'itemized_bill_anonymized');
            echo "</select>\n</span>\n";

            echo "<span>\n"
                ."<label for='PurtelItemizedBillShorted14_".$key."'>gekürzt</label>\n"
                ."<select id='PurtelItemizedBillShorted14_".$key."' name='PurtelItemizedBillShorted_".$key."'>";
            setOption($option_no_ping, $value, 'itemized_bill_shorted');
            echo "</select>\n</span>\n";
        }
        
        echo "<hr>";
        
        ?>
    </div>
<?php if ($rw) { ?>
    <span class='four'>
        <?php
        echo "<br \>"
            ."<button id='PurtelChange' onclick='purtelChange()' type='button'>Änderungen speichern</button>&nbsp;&nbsp;&nbsp;&nbsp;"
            ."<button id='PurtelPhone' onclick='purtelPhone(".$customer['id'].")' type='button'>Telefonnr. zuweisen</button>";
        ?>
    </span>
<?php } ?>
</div>
<?php if ($rw) { ?>
<h3 class="CustPurtelAccount">Purtel Zugang hinzufügen</h3>

<div class="CustPurtelAccount form">
    <span>
        <label for="PurtelLogin14">Purtel Login</label>
        <select id="PurtelLogin14" name="PurtelLogin">
            <?php setOption($purtel_option, $purtelNewAccount, 'login'); ?>
        </select>
    </span>

    <span></span>

    <span>
        <label for="PurtelMaster14">Hauptzugang</label>
        <select id="PurtelMaster14" name="PurtelMaster" disabled='disabled'>
            <?php setOption($option_master, $purtelNewAccount, 'master'); ?>
        </select>
    </span>

    <span></span>

    <span>
        <label for="PurtelNummer14">Telefonnummer</label>
        <input type="text" id="PurtelNummer14" value="" name="PurtelNummer">
    </span>

    <span>
        <label for="PurtelNummernblock14">Nummernblock</label>
        <input type="text" id="PurtelNummernblock14" value="" name="PurtelNummernblock">
    </span>

    <span>
        <label for="PurtelTyp14">Typ</label>
        <select id="PurtelTyp14" name="PurtelTyp">
            <?php setOption($option_purtel_teltype, $purtelNewAccount, 'typ'); ?>
        </select>
    </span>

    <span>
        <label for="PurtelPorting14">Telefonnr portieren</label>
        <select id="PurtelPorting14" name="PurtelPorting">
            <?php setOption($option_porting, $purtelNewAccount, 'porting'); ?>
        </select>
    </span>

    <span>
        <?php
        echo "<br \>"
            ."<button id='PurtelAccount' onclick='purtelAccount(".$customer['id'].", \"".$purtel_master."\", 0, \"\")'"
            ." type='button'>Neuer Purtelaccount</button>";
        ?>
    </span>
<?php } ?>

</div>