<?php
  $StartPath    = urldecode($_POST['StartPath']);
  $SelPath      = urldecode($_POST['SelPath']);
  $CustomerId   = urldecode($_POST['CustomerId']);
  
  $dir = $StartPath.'/'.$SelPath.'/_files/_mails/'.$CustomerId;
  if (!is_dir($dir)) mkdir ($dir);
  
  if (!empty($_FILES)) {
    $tempFile   = $_FILES['Filedata']['tmp_name'];
    $targetFile = $dir.'/'.$_FILES['Filedata']['name'];
    move_uploaded_file($tempFile, $targetFile);
  }
  $response = array (
    'dir'   => $dir,
    'name'  => $targetFile
  );
  print_r (json_encode ($response));
?>