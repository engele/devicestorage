<?php

$StartPath  = urldecode($_POST['StartPath']);
$StartURL   = urldecode($_POST['StartURL']);
$connect_request_replace  = json_decode ($_POST['connect_request_replace'], true);
$connect_letter_replace   = json_decode ($_POST['connect_letter_replace'],true);

$umlaut  = json_decode ($_POST['umlaut'],true);

$connect_letter1  = file_get_contents($StartPath.'/customer/_files/Anschaltungsschreiben-NAME_V1-DSL.rtf');
$connect_letter2  = file_get_contents($StartPath.'/customer/_files/Anschaltungsschreiben-NAME_V1-Mobilfunk+DSL.rtf');
$connect_letter3  = file_get_contents($StartPath.'/customer/_files/Anschaltungsschreiben-NAME_V3-Glasfaser.rtf');

$connect_letter1  = utf8_decode(str_replace (array_keys($connect_letter_replace), $connect_letter_replace, $connect_letter1));
$connect_letter2  = utf8_decode(str_replace (array_keys($connect_letter_replace), $connect_letter_replace, $connect_letter2));
$connect_letter3  = utf8_decode(str_replace (array_keys($connect_letter_replace), $connect_letter_replace, $connect_letter3));

$fileName = $connect_request_replace['#lastname#']."_".$connect_request_replace['#firstname#']."_".$connect_request_replace['#clientid#'];
$fileName = str_replace('/', '', $fileName); // a linux file can not have this characters in its name

$connect_letter1_file = str_replace(array_keys($umlaut), $umlaut, "/customer/_files/_tmp/Anschaltungsschreiben_DSL_".$fileName.".rtf");
$connect_letter2_file = str_replace(array_keys($umlaut), $umlaut, "/customer/_files/_tmp/Anschaltungsschreiben_Mobilfunk+DSL_".$fileName.".rtf");
$connect_letter3_file = str_replace(array_keys($umlaut), $umlaut, "/customer/_files/_tmp/Anschaltungsschreiben_Glasfaser_".$fileName.".rtf");

file_put_contents($StartPath.$connect_letter1_file, $connect_letter1);
file_put_contents($StartPath.$connect_letter2_file, $connect_letter2);
file_put_contents($StartPath.$connect_letter3_file, $connect_letter3);

$response = array (
    'urlLetter1'  => $StartURL.$connect_letter1_file,
    'urlLetter2'  => $StartURL.$connect_letter2_file,
    'urlLetter3'  => $StartURL.$connect_letter3_file
);

print_r(json_encode($response));

?>
