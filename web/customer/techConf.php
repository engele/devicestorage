<?php

use AppBundle\src\DslamCommunication\Communicator\SshGateway;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Location\Card;
use AppBundle\Entity\Location\Location;
use AppBundle\Entity\Product\MainProduct;
use Symfony\Component\DependencyInjection\ContainerBuilder;

require_once __DIR__.'/../_conf/database.inc';
require_once $StartPath.'/dslam/_conf/database_service.inc';
require_once $StartPath.'/dslam/_conf/dslam.php';

$doctrine = $this->getDoctrine();
$Autologon = true;
$debug = false;
$DemoDslam = \Wisotel\Configuration\Configuration::get('DemoDslam');
######################################################
# Functions Radius
######################################################

function mysql_fetch_all($result)
{
    while ($row = mysqli_fetch_array($result)) {
        $return[] = $row;
    }

    return $return;
}

function radiusDB()
{
    $radiusAccess = \Wisotel\Configuration\Configuration::get('radiusAccess');
    
    //Verbindung zur Datenbank herstellen    
    $Link = mysqli_connect($radiusAccess['host'].':'.$radiusAccess['port'],
        $radiusAccess['username'],
        $radiusAccess['password']
    );
    
    if ($Link == false) die('Can not connect to database. :: '.mysqli_connect_error());
    
    mysqli_select_db($Link, $radiusAccess['database']) or die ("Datenbank existiert nicht");
    
    return $Link;
}

function radiusWR ($clientid,$password,$IP_A) {
    if (!$Link = radiusDB () ) return false;
    $timestamp = date("Y-m-d H:i:s"); 
    $result = mysqli_query($Link, "SELECT username,attribute,op,value FROM radcheck WHERE username = '$clientid'"); 

    if ($clientid != '') {
        if (mysqli_num_rows($result) != 0 ) {
            echo "$clientid schon vorhanden in Radius radcheck<br /><br />";
            $myrow = mysql_fetch_all($result);
            echo ("| username = ".$myrow[0][0]."&nbsp | attribute = ".$myrow[0][1]."&nbsp | op ".$myrow[0][2]."&nbsp | value = ".$myrow[0][3]."<br />");
        #    echo ("| username = ".$myrow[1][0]."&nbsp | attribute = ".$myrow[1][1]."&nbsp | op ".$myrow[1][2]."&nbsp | value = ".$myrow[1][3]."<br /><br />");
            if ($result = mysqli_query($Link, "UPDATE radcheck SET value = '$password' where username = '$clientid' and attribute = 'Cleartext-Password'"))echo "$clientid schon vorhanden Password gesetzt auf $password <br /><br />";

        }
        else {    
        #    if ($result = mysqli_query($Link, "INSERT INTO radcheck (username, attribute,op,value) VALUES ('$clientid', 'Auth-Type',':=','PAP')"))echo "| username = $clientid | Auth-Type := PAP  in radcheck eingefuegt<br /><br />";
            if ($result = mysqli_query($Link, "INSERT INTO radcheck (username, attribute,op,value) VALUES ('$clientid', 'Cleartext-Password',':=','$password')"))echo "| username = $clientid | Password = $password | in radcheck eingefuegt<br /><br />";
        }
        
        if ($IP_A != '') {
            $result = mysqli_query($Link, "SELECT username,attribute,op,value FROM radreply WHERE username = '$clientid'"); 

            if (mysqli_num_rows($result) != 0 ) { 
                $myrow = mysql_fetch_all($result);
                echo ("| username = ".$myrow[0][0]."&nbsp | attribute = ".$myrow[0][1]."&nbsp | op ".$myrow[0][2]."&nbsp | value = ".$myrow[0][3]."<br />");
                
                if ($result = mysqli_query($Link, "UPDATE radreply SET value = '$IP_A' where username = '$clientid'"))echo "$clientid schon vorhanden in radreply IP gesetzt auf $IP_A <br /><br />";
            }
            else {
                
                if ($result = mysqli_query($Link, "INSERT INTO radreply (username, attribute,op,value) VALUES ('$clientid', 'Framed-Address','=','$IP_A')"))echo "| username = $clientid | attribute Framed-Adress = $IP_A | in radreply eingefuegt<br /><br />";
            }
        } else {
            echo "keine IP Adresse eingegeben<br /><br />";
        }
        $result = mysqli_query($Link, "SELECT username FROM userinfo WHERE username = '$clientid'"); 
        
        if (mysqli_num_rows($result) != 0 ) { 
            
            echo "$clientid schon vorhanden in Radius userinfo <br /> ";
        }
        else {
        
            if ($result = mysqli_query($Link, "INSERT INTO userinfo (username, creationdate) VALUES ('$clientid','$timestamp')"))echo "| username = $clientid | in userinfo eingefuegt<br />";;
        }
        $result = mysqli_query($Link, "SELECT username FROM radusergroup WHERE username = '$clientid'");
        
        if (mysqli_num_rows($result) != 0 ) { 
            
            echo "$clientid schon vorhanden in radusergroup userinfo <br /> ";
        }
        else {
            $radiusUserGroup = \Wisotel\Configuration\Configuration::get('radiusUserGroup');
            
            $result = mysqli_query($Link, "INSERT INTO radusergroup (username, groupname,priority) VALUES ('$clientid','$radiusUserGroup','0')");
            
            if ($result) {
                echo "| username = $clientid | in radusergroup eingefuegt<br />";
            }
        }


    } else {
        echo "kein User eingegeben";
    }
    mysqli_close ($Link);
    return true;
}
function radiusRD ($clientid,$password,$IP_A) {

    if (!$Link = radiusDB () ) return false;
    
    $result = mysqli_query($Link, "SELECT username,attribute,op,value FROM radcheck WHERE username = '$clientid'"); 

    if ($clientid != '') {
        if (mysqli_num_rows($result) != 0 ) {
            echo "Radius radcheck Daten = ";
            $myrow = mysql_fetch_all($result);
            echo ("| username = ".$myrow[0][0]."&nbsp | attribute = ".$myrow[0][1]."&nbsp | op ".$myrow[0][2]."&nbsp | value = ".$myrow[0][3]."<br />");
            #echo ("| username = ".$myrow[1][0]."&nbsp | attribute = ".$myrow[1][1]."&nbsp | op ".$myrow[1][2]."&nbsp | value = ".$myrow[1][3]."<br /><br />");
        }
        else {    
            echo "$clientid nicht vorhanden in Radius radcheck<br /><br />";
        }
        
            $result = mysqli_query($Link, "SELECT username,attribute,op,value FROM radreply WHERE username = '$clientid'"); 

            if (mysqli_num_rows($result) != 0 ) { 
            echo "Radius radreply Daten = &nbsp";

                $myrow = mysql_fetch_all($result);
                echo ("| username = ".$myrow[0][0]."&nbsp | attribute = ".$myrow[0][1]."&nbsp | op ".$myrow[0][2]."&nbsp | value = ".$myrow[0][3]."<br /><br />");
            }
            else {
                
            echo "$clientid nicht vorhanden in Radius radreply<br /><br />";

            }

        $result = mysqli_query($Link, "SELECT username FROM userinfo WHERE username = '$clientid'"); 
        
        if (mysqli_num_rows($result) != 0 ) { 
            
            echo "Radius userinfo Daten &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp = | username = $clientid<br /> ";
        }
        else {
        
            echo "$clientid nicht vorhanden in Radius userinfo <br /> ";
        }
        $result = mysqli_query($Link, "SELECT username,groupname FROM radusergroup WHERE username = '$clientid'"); 
        
        if (mysqli_num_rows($result) != 0 ) { 
            
            echo "Radius radusergroup Daten  = ";
            $myrow = mysql_fetch_all($result);
                echo ("| username = ".$myrow[0][0]."&nbsp | groupname = ".$myrow[0][1]."<br /><br />");
        }
        else {
        
            echo "$clientid nicht vorhanden in Radius radusergroup <br /> ";
        }

    } else {
        echo " keine Kunden Nummer eingegeben ";
    }    
    mysqli_close ($Link);
    return true;
}

function radiusDEL ($clientid,$password,$IP_A) {

    if (!$Link = radiusDB () ) return false;
    $radcheck = "SELECT username FROM radcheck WHERE username = '$clientid'"; 
    $radreply = "SELECT username FROM radreply WHERE username = '$clientid'"; 
    $userinfo = "SELECT username FROM userinfo WHERE username = '$clientid'"; 
    $radusergroup = "SELECT username FROM radusergroup WHERE username = '$clientid'"; 

    if ($clientid != '') {
        $result = mysqli_query($Link, $radcheck);
        if (mysqli_num_rows($result) != 0 ) {
            if (!$result = mysqli_query($Link, "DELETE FROM radcheck WHERE username = '$clientid'")) echo " radcheck nicht gefunden <br />";
            echo "$clientid in radcheck geloescht<br />";
        } else {
            echo "$clientid in radcheck nicht vorhanden<br />";
        }
        
        $result = mysqli_query($Link, $radreply);
        if (mysqli_num_rows($result) != 0 ) {
            if (!$result = mysqli_query($Link, "DELETE FROM radreply WHERE username = '$clientid'")) echo " radreply nicht gefunden <br />";
            echo "$clientid in radreply &nbsp geloescht<br />";
        } else {
            echo "$clientid in radreply &nbsp nicht vorhanden<br />";
        }
        $result = mysqli_query($Link, $userinfo);
        if (mysqli_num_rows($result) != 0 ) {
            if (!$result = mysqli_query($Link, "DELETE FROM userinfo WHERE username = '$clientid'")) echo " userinfo nicht gefunden <br />";
            echo "$clientid in userinfo &nbsp geloescht<br />";
        } else {
            echo "$clientid in userinfo &nbsp nicht vorhanden<br />";                        
        }
        $result = mysqli_query($Link, $radusergroup);
        if (mysqli_num_rows($result) != 0 ) {
            if (!$result = mysqli_query($Link, "DELETE FROM radusergroup WHERE username = '$clientid'")) echo " radusergroup nicht gefunden <br />";
            echo "$clientid in radusergroup &nbsp geloescht<br />";
        } else {
            echo "$clientid in radusergroup &nbsp nicht vorhanden<br />";                        
        }
        
    } else {
        echo " keine Kunden Nummer eingegeben<br /> ";
    }    
        
    mysqli_close ($Link);
    return true;
}

function radiusENA ($clientid) {

    if (!$Link = radiusDB () ) return false;
    $radusergroup = "SELECT username FROM radusergroup WHERE username = '$clientid'";

    if ($clientid != '') {
        $result = mysqli_query($Link, $radusergroup);
        if (mysqli_num_rows($result) != 0 ) {
            $radiusUserGroup = \Wisotel\Configuration\Configuration::get('radiusUserGroup');

            $result = mysqli_query($Link, "UPDATE radusergroup SET groupname = '".$radiusUserGroup."' WHERE username ='$clientid'");

            if ($result) {
                echo "| username = $clientid | groupname ".$radiusUserGroup." in radusergroup geaendert <br />";
            }
        } else {
            echo "$clientid in radusergroup &nbsp nicht vorhanden<br />";                        

        }
        
    } else {
        echo " keine Kunden Nummer eingegeben<br /> ";
    }    
    mysqli_close ($Link);
    return true;
}

function radiusDIS ($clientid) {

    if (!$Link = radiusDB () ) return false;
    $radusergroup = "SELECT username FROM radusergroup WHERE username = '$clientid'"; 

    if ($clientid != '') {
        $result = mysqli_query($Link, $radusergroup);
        
        if (mysqli_num_rows($result) != 0 ) {
            if ($result = mysqli_query($Link, "UPDATE radusergroup SET groupname = 'daloRADIUS-Disabled-Users' WHERE username = '$clientid'"))echo "| username = $clientid | groupname daloRADIUS-Disabled-Users in radusergroup geaendert<br />";;
            
    } else {
            echo "$clientid in radusergroup &nbsp nicht vorhanden<br />";                        
        }
        
    } else {
        echo " keine Kunden Nummer eingegeben<br /> ";
    }    
    mysqli_close ($Link);
    return true;
}

function display_mikrotik ($id,$clientid,$IP_A,$IP_As,$password,$PPPoE_Location,$PPPoE_Adresse,$PPPoE_Adresse1,$Rlogin,$RPassw,$Dslam_comando,$Profile,$Cust_Produkt,$PPPoE_Type, $purtel_master, $customer, $db)  {
    if ($PPPoE_Type == "mikrotik") {
                if ($Dslam_comando == "Experte") {
            
            echo "<span class='text blue'> die nachfolgende Zeile wird im PPPoE Mikrotik Server eingefügt  </span><br /><br />"; 
            echo "<span class='text green'>/ppp secret add name=$clientid service=pppoe password=" . '"' . "$password" . '"' .  " profile=$Profile remote-address=$IP_As" . ';' ."\n</span><br /><br />";
            echo "<span class='fbox statustext $lblclass'> F-Box <a href='https://$IP_As' target='_blank' >IP = $IP_As</a> RLogin  = $Rlogin PW = $RPassw <span id='statusval'>\n</span><br /><br />";

            echo "<span class='text green'> API Password = $passwd  oder cut and paste im PPPoE Terminal Password = $password</span><br /><br />";
        }
        # echo "<hr class='red' />";
        echo "<span class='text blue'> PPPoE Standort = </span><span class='text grey'> $PPPoE_Location </span><span class='text blue'> IP Adresse = </span><span class='text grey'> $PPPoE_Adresse  / $PPPoE_Adresse1 </span><span class='text blue'> Type = </span><span class='text grey'> $PPPoE_Type  </span><span class='text blue'>&nbsp&nbsp&nbsp&nbsp </span><br /><br />";
        ?>
        <a  class="fancy-button outline round blue" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=PPPoE_Status"  ?>">&nbsp;Status&nbsp; </a>&nbsp;&nbsp;
        <a  class="fancy-button outline round blue" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=PPPoE_Status_for_Default_Customer"  ?>">&nbsp;Status zum default Kunden&nbsp; </a>&nbsp;&nbsp;
        <a  class="fancy-button outline round blue" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=PPPoE_Active"  ?>">&nbsp;Active Sessions&nbsp; </a>&nbsp;&nbsp;
        <a  class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=PPPoE_Mikrotik"  ?>">&nbsp;Einrichten&nbsp; </a>&nbsp;&nbsp;
        <a  class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=PPPoE_Mikrotik_neue_IP"  ?>">&nbsp;neue IP&nbsp; </a>&nbsp;&nbsp;
        <a  class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=PPPoE_Loeschen&ansicht=antwort"  ?>">&nbsp;Löschen&nbsp; </a>&nbsp;&nbsp;<br><br>
        <?php

        if ($Dslam_comando == "Experte") {
        ?>
        <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=PPPoE_Labor"  ?>">&nbsp;&nbsp;&nbsp;Labor status&nbsp; </a>&nbsp;&nbsp;
        <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=PPPoE_Labor_IP"  ?>">&nbsp;&nbsp;&nbsp;Labor einrichten&nbsp; </a>&nbsp;&nbsp;
        <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=PPPoE_Labor_neue_IP"  ?>">&nbsp;&nbsp;&nbsp;Labor einrichten neue IP&nbsp; </a>&nbsp;&nbsp;
        <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=PPPoE_Labor_loeschen"  ?>">&nbsp;&nbsp;&nbsp;Labor löschen&nbsp; </a>&nbsp;&nbsp;
        <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=PPPoE_Load"  ?>">&nbsp;Microtik CPU Load&nbsp;   </a>&nbsp;&nbsp;
        <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=PPPoE_Mikrotik_A"  ?>">&nbsp;Einrichten Debug&nbsp; </a>&nbsp;&nbsp;<br><br>

        <a  class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=PPPoE_Backup"  ?>">&nbsp;Backup to FTP &nbsp; </a>&nbsp;&nbsp;
        <a  class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=PPPoE_Backupall"  ?>">&nbsp;Backup all to FTP &nbsp; </a><br><br>

        <?php
        }
    } else if ($PPPoE_Type == "purtel-radius") {
        if (isset($purtel_master) && !empty($purtel_master)) {
            echo "<hr class='red' />";    
            echo '<p style="font-weight: bold;">Purtel Radius</p>';
            echo "<a id='PurtelRadiusCreate' class='fancy-button outline round red' href='#' onclick='createPurtelRadius()' target='_self'>&nbsp;Radiusaccount anlegen&nbsp;&nbsp;</a>&nbsp;&nbsp;";
            echo "<a id='PurtelRadiusUpdate' class='fancy-button outline round red' href='#' onclick='updatePurtelRadius()' target='_self'>&nbsp;Passwort ändern&nbsp;&nbsp;</a>&nbsp;&nbsp;";
            echo "<a id='PurtelRadiusDelete' class='fancy-button outline round red' href='#' onclick='deletePurtelRadius()' target='_self'>&nbsp;Radiusaccount löschen&nbsp;&nbsp;</a>&nbsp;&nbsp;";
            echo "<br><br>";
        } else {
            echo 'Kein (Haupt-) SIP-Account vorhanden<br /><br />';
        }
    } else if ($PPPoE_Type == "static-ip-no-pppoe") {
        if (isset($customer['ip_range_id']) && !empty($customer['ip_range_id'])) {
            $query = $db->query("select `gateway` from `ip_ranges` where `id` = '".$customer['ip_range_id']."'");
            $ipRange = $query->fetch_all(MYSQLI_ASSOC);
            $query->close();

            echo sprintf("Gateway: %s<br />",
                empty($ipRange[0]['gateway']) ? 'unbekannt' : $ipRange[0]['gateway']
            );
        } else {
            echo 'Kein IP-Block ausgewählt.<br />';
        }
    } else if ($PPPoE_Type == "provisioning-server"){
        if (isset($customer['ip_range_id']) && !empty($customer['ip_range_id'])) {
            $query = $db->query("select `pppoe_ip`, `pppoe_location` from `ip_ranges` where `id` = '".$customer['ip_range_id']."'");
            $ipRange = $query->fetch_all(MYSQLI_ASSOC);
            $query->close();

            $pppoeIp = empty($ipRange[0]['pppoe_ip']) ? 'unbekannt' : $ipRange[0]['pppoe_ip'];
            $pppoeLocation = empty($ipRange[0]['pppoe_location']) ? 'unbekannt' : $ipRange[0]['pppoe_location'];
            ?>

            <br />
            <p>
                <span class="text grey">
                    <?php
                    
                    echo $pppoeLocation;
                    
                    if ('WiSoTEL' === \Wisotel\Configuration\Configuration::get('companyName')) {
                        echo 'Burgrieden Roetzer: <span class="text green">User:</span> <span class="text red">admin</span> PW: <span class="text red">9k3IP1fK</span>'
                        . '<a class="fancy-button outline round grey" href="http://'.$pppoeIp.':65001" target="_blank">&nbsp;F-Box an TV Station&nbsp;</a>';
                    }

                    ?>

                    <a class="fancy-button outline round blue" href="https://<?php echo $pppoeIp; ?>" target="_blank">&nbsp;Server WWW&nbsp;</a>
                    <a class="fancy-button outline round grey" href="khl://puttyroot@<?php echo $pppoeIp; ?>" target="_blank">&nbsp;Server SSH&nbsp;</a>
                </span>
            </p>

            <?php
        } else {
            echo 'Kein IP-Block ausgewählt.<br />';
        }
    } else {
        ?>
        <a class="fancy-button outline round blue" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=RLesen" ?>">&nbsp;Radius lesen &nbsp;</a>&nbsp;&nbsp;
        <a class="fancy-button outline round green" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Rena" ?>">&nbsp;Radius freischalten &nbsp;</a>&nbsp;&nbsp;
        <a class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Radius" ?>">&nbsp;Radius Eintrag &nbsp;</a>&nbsp;&nbsp;
        <a class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Rdis" ?>">&nbsp;Radius sperren &nbsp;</a>&nbsp;&nbsp;
        <a class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Rdelete" ?>">&nbsp;Radius loeschen &nbsp;</a><br><br>
       <?php
    }



    #echo "<hr class='red' />";
    $NL = "<br />";

    return true;
}

######################################################
# Functions Ende
######################################################
$DSLAM_NAME    = "";
$DSLAM_NAME   = "";
$logon_ip    = "";
$logon_port    = "";
$DSLAMLOGON    = "";
$DSLAMPW    = "";
$Backup   = "";
$VlanIPTV   = "";
$vlan_ACS_NW   = "";
$vlan_Fbox   = "";
$Vector_location    = "";
$Dslam_conf = "";
$debug = false;

  $ACS = array();
  
  if (key_exists ('id', $customer))                           $id                         = $customer['id'];                          else $id                          = '';
  if (key_exists ('clientid', $customer))                     $clientid                   = $customer['clientid'];                    else $clientid                    = '';
  if (key_exists ('line_identifier', $customer))              $lineident                  = $customer['line_identifier'];             else $lineident                   = '';
  
  // Macht das Sinn?
  // if (key_exists ('purtel_login', $customer))                 $purtelUsername             = $purtel_master;                           else $purtelUsername              = '';
  if (key_exists ('purtel_login', $customer))                 $purtelUsername             = $customer['purtel_login'];                           else $purtelUsername              = '';
  // Macht das Sinn?
  // if (key_exists ('purtel_passwort', $customer))              $purtelPasswort             = $purtel_masterdb;                         else $purtelPasswort              = '';
  if (key_exists ('purtel_passwort', $customer))              $purtelPasswort             = $customer['purtel_passwort'];                         else $purtelPasswort              = '';
  
  if (key_exists ('purtel_telnr', $customer))                    $purtel_telnr             = $customer['purtel_telnr'];              else $purtel_telnr                = '';
  if (key_exists ('purtel_use_version', $customer))           $purtel_use_version         = $customer['purtel_use_version'];          else $purtel_use_version          = '';
  if (key_exists ('purtel_login_1', $customer))               $purtelUsername_1           = $customer['purtel_login_1'];              else $purtelUsername_1            = '';
  if (key_exists ('purtel_passwort_1', $customer))            $purtelPasswort_1           = $customer['purtel_passwort_1'];           else $purtelPasswort_1            = '';
  if (key_exists ('purtel_telnr_1', $customer))                    $purtel_telnr_1           = $customer['purtel_telnr_1'];            else $purtel_telnr_1              = '';
  if (key_exists ('purtel_login_2', $customer))               $purtelUsername_2           = $customer['purtel_login_2'];              else $purtelUsername_2            = '';
  if (key_exists ('purtel_passwort_2', $customer))            $purtelPasswort_2           = $customer['purtel_passwort_2'];           else $purtelPasswort_2            = '';
  if (key_exists ('purtel_telnr_2', $customer))                  $purtel_telnr_2             = $customer['purtel_telnr_2'];              else $purtel_telnr_2              = '';
  if (key_exists ('purtel_login_3', $customer))               $purtelUsername_3           = $customer['purtel_login_3'];              else $purtelUsername_3            = '';
  if (key_exists ('purtel_passwort_3', $customer))            $purtelPasswort_3           = $customer['purtel_passwort_3'];           else $purtelPasswort_3            = '';
  if (key_exists ('purtel_telnr_3', $customer))                  $purtel_telnr_3             = $customer['purtel_telnr_3'];              else $purtel_telnr_3              = '';
  if (key_exists ('location_id', $customer))                  $Location_id                = $customer['location_id'];                 else $Location_id                 = '';
  if (key_exists ('lsa_kvz_partition_id', $customer))         $partition_id               = $customer['lsa_kvz_partition_id'];        else $partition_id                 = '';
  if (key_exists ('kvz_addition', $customer))                 $KVZ_a                      = $customer['kvz_addition'];                else $KVZ_a                       = '';
  if (key_exists ('kvz_name', $customer))                     $KVZ_Name                   = $customer['kvz_name'];                    else $KVZ_Name                    = '';
  if (key_exists ('DPBO', $customer))                         $DPBO                       = $customer['DPBO'];                        else $DPBO                        = '';
  if (key_exists ('Service', $customer))                      $Service                    = $customer['Service']->getIdentifier();    else $Service                     = '';
  if (key_exists ('vlan_ID', $customer))                         $Vlan                       = $customer['vlan_ID'];                        else $Vlan                        = '';
  if (key_exists ('Spectrumprofile', $customer))              $Spectrumprofile            = $customer['Spectrumprofile'];             else $Spectrumprofile             = '';
  if (key_exists ('ip_address', $customer))                   $IP_A                       = $customer['ip_address'];                  else $IP_A                        = '';
  if (key_exists ('dtag_line', $customer))                    $Leitung                    = $customer['dtag_line'];                   else $Leitung                     = '';
  if (key_exists ('password', $customer))                     $password                   = $customer['password'];                    else $password                    = '';
  if (key_exists ('terminal_type', $customer))                $terminal_type              = $customer['terminal_type'];               else $terminal_type               = '';
  if (key_exists ('mac_address', $customer))                  $mac_address                = $customer['mac_address'];                 else $mac_address                 = '';
  if (key_exists ('remote_login', $customer))                 $remote_login               = $customer['remote_login'];                else $remote_login                = '';
  if (key_exists ('remote_password', $customer))              $remote_password            = $customer['remote_password'];             else $remote_password             = '';
  if (key_exists ('$new_or_ported_phonenumbers', $customer))  $new_or_ported_phonenumbers = $customer['$new_or_ported_phonenumbers']; else $new_or_ported_phonenumbers  = '';
  if (key_exists ('firmware_version', $customer))             $firmware_version           = $customer['firmware_version'];            else $firmware_version            = '';
  if (key_exists ('vectoring', $customer))                    $vectoring                  = $customer['vectoring'];                   else $vectoring                   = 'Nein';
  if (key_exists ('acs_id', $customer))                       $acs_id                     = $customer['acs_id'];                      else $acs_id                      = '';
  if (key_exists ('version', $customer))                      $version                    = $customer['version'];                     else $version                     = '';
  if (key_exists ('dslam_port', $customer))                   $dslam_port                 = $customer['dslam_port']->getNumber();     else $dslam_port                  = '';
  if (key_exists ('tv_service', $customer))                   $tv_service                 = $customer['tv_service'];                  else $tv_service                  = 'No';
  if (key_exists ('tag', $customer))                             $tag                        = $customer['tag'];                            else $tag                         =  false;
#if ($tv_service != 'yes') $tv_servic = 'No' ;

$customerObject = null;
$location = null;
$cardObject = null;

try {
    // create instance of AppBundle\Entity\Customer
    $customerObject = Customer::createFromArray($customer, $doctrine);
    // create instance of AppBundle\Entity\Location
    $location = $doctrine->getRepository(Location::class)->findOneById($customerObject->getLocationId());
    // create instance of AppBundle\Entity\Location\Card
    $cardObject = $doctrine->getRepository(Card::class)->findOneById($customerObject->getCardId());

    if ($cardObject instanceOf Card) {
        // check, if location is dependet-mdu
        // has customers cards location at least one card with type 'dependent-MDU' and at least one card port with attribute 'olt-connected'
        // and is that port connected to other exsiting port
        $query = $doctrine->getConnection()->prepare("SELECT cp.`id` FROM `card_ports` cp 
            INNER JOIN `card_ports_to_card_port_attributes` cpa ON cpa.`card_port_id` = cp.`id` 
            INNER JOIN `card_port_attributes` cp_a ON cp_a.`id` = cpa.`card_port_attribute_id` 
            INNER JOIN `cards` c ON c.`id` = cp.`card_id` 
            INNER JOIN `card_types` ct ON ct.`id` = c.`card_type_id` 
            WHERE ct.`name` = 'dependent-MDU' 
            AND cp_a.`identifier` = 'olt-connected' 
            AND cp.`card_port_id` IS NOT NULL 
            AND c.`location_id` = ?");

        $query->bindValue(1, $cardObject->getLocation()->getId());
        $query->execute();

        if ($query->rowCount() > 0) {
            $cardPortId = $query->fetch();
            $cardPortId = $cardPortId['id'];
            $cardPort = $doctrine->getRepository('\AppBundle\Entity\Location\CardPort')->findOneById($cardPortId);
            $parentLocation = $cardPort->getRelatedCardPort()->getCard()->getLocation();
            
            // current location is dependet-mdu, therefore use MDUs parent location (OLT) as location
            $location = $parentLocation;
            $Location_id = $location->getId();
        }
    }
} catch (\Exception $exception) {
    //die(dump( $exception ));
}

$purtelACSadmin = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$purtelACS_PW   = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
$pppoeLaborAccess = \Wisotel\Configuration\Configuration::get('pppoeLabor');
$pppoeAccess = \Wisotel\Configuration\Configuration::get('pppoe');
$axirosAcs = \Wisotel\Configuration\Configuration::get('axirosAcs');
$dnsServer = \Wisotel\Configuration\Configuration::get('dnsServer');

$PPPoEAPI =    $pppoeLaborAccess['username'];
$PPPoEPWW = $pppoeLaborAccess['password'];
$PPPoEPW = $pppoeAccess['password'];
$PPPoE2_IP = isset($pppoeAccess['ipaddress']) ? $pppoeAccess['ipaddress'] : null;


//$tftpserver     = '10.20.0.254';
$SIP_Registrar = $axirosAcs['registrar'];
$vlan_Fbox = "";
$vlan_ACS = "";
$remotehttps = substr ($IP_A, -2);
$dns1 = $dnsServer['1'];

$remotehttps = str_replace(".", "0",$remotehttps);

$FBOX_LOGON = "https://91.103.117.82:560$remotehttps";


  if (key_exists ('kommando', $_GET))                         $Dslam_comando    = $_GET['kommando'];  else  $Dslam_comando  = '';
  if (key_exists ('ansicht', $_GET))                         $ansicht    = $_GET['ansicht'];  else  $ansicht  = '';

  //$ip_source      = $_SERVER['SERVER_ADDR'];
    $clientidv = $clientid;
  if ($version != '' && $purtel_use_version == 1)    $clientidv = $clientid.'_'.$version;
    $mac_address1     = substr ($mac_address, 7, 12);
    $usenet = socket_create (AF_INET, SOCK_STREAM, SOL_TCP);
    //socket_bind ($usenet, $ip_source); // not necessary
     $modell=21;
    
    $passwordACS = urlencode ($password);    
    
    $providedsl="&prov_dsl=1&dsl_username=$clientidv&dsl_password=$passwordACS";

    $terminalTypesToPurtelAcsHwId = [
        '3270' => ['modell' => 46,],
        '3370' => ['modell' => 44,],
        '3390' => ['modell' => 54,],
        '5490' => ['modell' => 26,],
        '6360' => ['modell' => 30, 'providedsl' => '',],
        '6430' => ['modell' => 50, 'providedsl' => '',],
        '6490' => ['modell' => 32, 'providedsl' => '',],
        '6590' => ['modell' => 56, 'providedsl' => '',],
        '7270' => ['modell' => 1,],
        '7360' => ['modell' => 19,],
        '7390' => ['modell' => 4,],
        '7490' => ['modell' => 21,],
        '7530' => ['modell' => 62,],
        '7560' => ['modell' => 48,],
        '7590' => ['modell' => 58,],
    ];

    if (isset($terminalTypesToPurtelAcsHwId[$terminal_type])) {
        $modell = $terminalTypesToPurtelAcsHwId[$terminal_type]['modell'];

        if (isset($terminalTypesToPurtelAcsHwId[$terminal_type]['providedsl'])) {
            $providedsl = $terminalTypesToPurtelAcsHwId[$terminal_type]['providedsl'];
        }
    }

    $acsmail = \Wisotel\Configuration\Configuration::get('purtelAcsProvEmailadresse');

    #$acsmail = str_replace("@", "%40", $acsmail);
  $ACS_SIP  = '';
  $i        = 1;
    $emei_address = substr($mac_address,0,19);
    
    // does not always work
    //$cpeid = substr($mac_address,7,12);

    $cpeid = '';

    if (!empty($mac_address)) {
        $matchMac = null;

        preg_match('/([0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2}(:|-)[0-9a-f]{2})|([0-9a-f]{12})/i', $mac_address, $matchMac);

        if (isset($matchMac[0])) {
            $cpeid = preg_replace('/[^0-9a-f]/i', '', $matchMac[0]);
        }
    }

    $emei_address_dhcp = $mac_address;
    $mac_address = substr($mac_address,0,19);

  foreach ($purtel_account as $key => $value){
     if ($value['nummer']) {
       if (!$ACS_SIP) $ACS_SIP = "&prov_sipaccounts=1&prov_voip=1";
      # $value['nummer'] = '0049'.substr($value['nummer'],1);
       $temp = "&acs_sip".$i."=".$value['purtel_login']."&acs_ortsnetzrufnummer".$i."=".$value['nummer']."&acs_registrar".$i."=".$SIP_Registrar;
       $ACS[$i++-1] = $temp;
     }
 }
    #$purtel_telnr = '0049'.substr($purtel_telnr,1);
    #if ($purtel_telnr != '') $ACS0 = "&acs_sip1=$purtelUsername&acs_ortsnetzrufnummer1=$purtel_telnr&acs_registrar1=sip.100025.purtel.com";     else $ACS0 = '';
    #if ($purtel_telnr != '' or $purtel_telnr_1 != '' or $purtel_telnr_2 != '' or $purtel_telnr_3 != '' ) $ACS_SIP = "&prov_sipaccounts=1";   else $ACS_SIP = '';

  /***
    if ($purtel_telnr != '' or $purtel_telnr_1 != '' or $purtel_telnr_2 != '' or $purtel_telnr_3 != '' ) $ACS_SIP = "&prov_sipaccounts=1";   else $ACS_SIP = '';
    if ($purtel_telnr != '') $ACS0 = "&acs_sip1=$purtelUsername&acs_ortsnetzrufnummer1=$purtel_telnr&acs_registrar1=sip.100025.purtel.com";     else $ACS0 = '';
    if ($purtel_telnr_1 != '') $ACS1 = "&acs_sip2=$purtelUsername_1&acs_ortsnetzrufnummer2=$purtel_telnr_1&acs_registrar2=sip.100025.purtel.com"; else $ACS1 = '';
    if ($purtel_telnr_2 != '') $ACS2 = "&acs_sip3=$purtelUsername_2&acs_ortsnetzrufnummer3=$purtel_telnr_2&acs_registrar3=sip.100025.purtel.com"; else $ACS2 = '';
    if ($purtel_telnr_3!= '') $ACS3 = "&acs_sip4=$purtelUsername_3&acs_ortsnetzrufnummer4=$purtel_telnr_3&acs_registrar4=sip.100025.purtel.com"; else $ACS3 = '';
  */
    
    $acs_cpe_neu = "https://ipcom.purtel.com/index.php?super_username=$purtelACSadmin&super_passwort=$purtelACS_PW&action=acs_cpe_neu&modell=$modell&hersteller=AVM&wanmodus=1&cwmp=$emei_address&prov_profile=default_avm";
    $acs_cpe_neu = $StartURL.'/customer/purtel-acs.php?action=acs_cpe_neu&purtel-url='.urlencode($acs_cpe_neu);
#    $acs_kunde_zuweisen = "https://ipcom.purtel.com/index.php?super_username=$purtelACSadmin&super_passwort=$purtelACS_PW&action=acs_kunde_zuweisen&kundennummer_extern=$clientid&acs_id=$acs_id&hersteller=AVM&prov_sipaccounts=1&acs_sip1=$purtelUsername&acs_ortsnetzrufnummer1=$new_or_ported_phonenumbers&acs_registrar1=sip.100025.purtel.com&prov_dsl=1&dsl_username=$clientidv&dsl_password=$passwordACS&prov_fernwartung=1&fernzugriff=1&prov_email=1&prov_emailadresse=$acsmail";
    $acs_kunde_zuweisen = "https://ipcom.purtel.com/index.php?super_username=$purtelACSadmin&super_passwort=$purtelACS_PW&action=acs_kunde_zuweisen&kundennummer_extern=$clientidv&acs_id=$acs_id&hersteller=AVM$ACS_SIP";
    $acs_kunde_zuweisen_sip = $acs_kunde_zuweisen."&prov_email=1&prov_emailadresse=$acsmail";
    
    foreach ($ACS as $temp) {
        $acs_kunde_zuweisen .= $temp;
        $acs_kunde_zuweisen_sip .= $temp;
    }

    $acs_kunde_zuweisen .= "$providedsl&prov_fernwartung=1&fernzugriff=1&prov_email=1&prov_emailadresse=$acsmail";
    $acs_kunde_zuweisen_show = "super_username=50xx&super_passwort=xx&action=acs_kunde_zuweisen&kundennummer_extern=$clientidv&acs_id=$acs_id&hersteller=AVM";
    $acs_kunde_zuweisen_show1 = "&prov_email=1&prov_emailadresse=$acsmail";
    $acs_kunde_zuweisen_show2 = "$providedsl&prov_fernwartung=1&fernzugriff=1";
    
    $acs_kunde_entfernen  ="https://ipcom.purtel.com/index.php?super_username=$purtelACSadmin&super_passwort=$purtelACS_PW&action=acs_kunde_entfernen&acs_id=$acs_id&hersteller=AVM";
    $acs_cpe_loeschen = "https://ipcom.purtel.com/index.php?super_username=$purtelACSadmin&super_passwort=$purtelACS_PW&action=acs_cpe_loeschen&acs_id=$acs_id&hersteller=AVM";
    $acs_cpe_loeschen = $StartURL.'/customer/purtel-acs.php?action=acs_cpe_loeschen&purtel-url='.urlencode($acs_cpe_loeschen);
    $acs_cpe_suche = "https://ipcom.purtel.com/index.php?super_username=$purtelACSadmin&super_passwort=$purtelACS_PW&action=acs_cpe_suche&state=searchid&search=$acs_id";
    $acs_cpe_suche_k = "https://ipcom.purtel.com/index.php?super_username=$purtelACSadmin&super_passwort=$purtelACS_PW&action=acs_cpe_suche&state=searchcustomer&search=$clientidv";
    $acs_get_cpe = "https://ipcom.purtel.com/index.php?super_username=$purtelACSadmin&super_passwort=$purtelACS_PW&action=acs_get_cpe&macadresse=$acs_id";
    $acs_cpe_parameter ="https://ipcom.purtel.com/index.php?super_username=$purtelACSadmin&super_passwort=$purtelACS_PW&action=acs_cpe_parameter&id=$acs_id";
    $acs_get_numbers = "https://ipcom.purtel.com/index.php?super_username=$purtelACSadmin&super_passwort=$purtelACS_PW&action=getnumbers&kundennummer_extern=$clientidv";
    $acs_get_PW = "https://ipcom.purtel.com/index.php?super_username=$purtelACSadmin&super_passwort=$purtelACS_PW&action=acs_get_fernwartung&acs_id=$acs_id";
    
  $Cust_Produkt        = $Service;
  $IP_As            = $IP_A;
  $IP_Mikrotik      = $IP_A;
  $Staticuser       = "static-user ip-address ";
  $IP_32            = "/32";
  $Service_int      = intval ($Service);
  $sublineident     = substr ($lineident, 0, 5);
  $atm              = "";
  $dpboprofile      = "dpbo-profile ";
  $Transfermode     = "ptm";
  $no_DPBO          = "";
  $no_VLAN          = "";
  $Vlan_Text        = "Vlan war nicht gesetzt/Falsch, Default Vlan-id  aus KVZ Tabelle  wurde gennommen Vlan = ";
  $DPBO_Text        = "Kein DPBO beim Kunden ,DPBO  aus KVZ Tabelle  wurde genommen DPBO =";
  $Config_Kunde     = " Kunde im DSLAM einrichten";
  $Vorsicht         = '<span class = "text blau">';
  $ip_address       = "ip-address ";
  $KVZ_s            = "";
  $dslamLaborAccess = \Wisotel\Configuration\Configuration::get('dslamLabor');
  $logon_ip         = $dslamLaborAccess['ipaddress'];
  $logon_port       = $dslamLaborAccess['port'];
  $tt               = "";
  $cfgTimeOut       = 10;
  $dslamAccess      = \Wisotel\Configuration\Configuration::get('dslam');
  $DSLAMLOGON       = $dslamAccess['username'];
  $DSLAMPW          = $dslamAccess['password'];
  $display_ende        = false;


  if ($firmware_version == ""  ) {
      $ONTSWVERSION     = '3FE53216AOCK06';
      }
  else {
    //$ONTSWVERSION = '3'.$firmware_version;
    $ONTSWVERSION = '3'.substr($firmware_version,-13);
  }
  if ($version) $clientid .= '_'.$version;
  // $usenet = fsockopen($logon_ip, $logon_port, $errno, $errstr, $cfgTimeOut);
  $Dslam_conf       = false;
  $Gpon_up          = "EIR_1_5M";
  $Gpon_down        = "10M_EIR";
  $Ethernet_up_down = "Up_1_5_down_10Mb";
  $Produkt          = "no Produkt";
  $Service_Text     = "";
  $PPPoE_Location   = "";
  $PPPoE_Adresse    = "";
  $PPPoE_Type       = "";
  $DSLAM_Status     = true;
  $PPPoE_Profile    = 'PPPoE';
  $Profile          = "PPPoE";
  $Backup ="Generic_WiSoTEL.tar"; 
    $Adminsave    = "admin save";
  //$Backup_id    = "admin software-mngt database upload actual-active $tftpserver:dm_complete_";
  //$Restore_id   = "admin software-mngt database download $tftpserver:dm_complete_";

  $DSLAM_LOGON = "khl://puttytelnet:".$pppoeLaborAccess['ipaddress'].":".$pppoeLaborAccess['port'];
 # $logon_ip     = "185.8.85.3";
 # $logon_port   = "50240";
 # $DSLAM_NAME   = "Labor ";
 # $Backup       = "Labor.tar";

  //$Service_array = array (1,2,3,4,5,10,11,12,13,14,15,16,10,11,12,13,14,15,16,10,11,12,13,14,40,41,42,43,44,45,46,40,41,42,43,44,45,46,70,71,72,73,74,75,76,50,51,52,53,54,55,56,50,51,52,53,54,55,56,60,61,62,63,64,65,66,67,60,61,62,63,64,65,66);
  // $Service_array aus DB (products - products_index) zusammenbauen (nur noch aktive) 
  $Service_array = array (1,2,3,4,5,10,11,12,13,14,15,16,40,41,42,43,44,45,46,70,71,72,73,74,75,76,50,51,52,53,54,55,56,60,61,62,63,64,65,66,67);
  $DSLAMTYP = "ALU";
  $Vectoring = "";
  $NoVectoring = "";
  if ($vectoring == 'Ja') {
    $Vectoring = "vect-profile 1";
    $NoVectoring = "no vect-profile"; 
   }
  $Vector_location = false;
  
  $PPPoE_Server = array_map(function ($array) {
    return $array['ipaddress'];
  }, \Wisotel\Configuration\Configuration::get('pppoeServer'));


  if (!in_array ($Service,$Service_array)){
    $Service="";
    $Service_Text = "leider keinen Service gefunden";
  } else {
    $Service_Text = "Service beim Kunden gefunden";
  }
  if ($DPBO <> "" ) $DPBO_Text  = "DPBO beim Kunden gefunden"; else $DPBO_Text = "leider kein DPBO beim Kunden gefunden";
  if ($Vlan <> "" ) $Vlan_Text  = "Vlan beim Kunden gefunden"; else $Vlan_Text = "leider kein Vlan gefunden";

  foreach ($lsa_kvz_partitionsdb as $row) {
    if ($row["id"] == $partition_id) {
    
       $Default_VLAN = $row["default_VLAN"];
       $Default_DPBO = $row["dpbo"];
       $esel_wert      = $row["esel_wert"];
       if ($DPBO == "")  {$DPBO =  $Default_DPBO;  $no_DPBO ="no";}
       if ($Vlan == "")  {$Vlan =  $Default_VLAN;  $no_VLAN ="no";}
    
    }

  }

foreach ($locationsdb as $row) {
    if ($row["id"] == $Location_id) {
        $DSLAM_NAME = $row["name"];
        $DSLAM_NAME    = substr($DSLAM_NAME,0,10);
        $logon_ip = $row["dslam_ip_adress"];
        $logon_port = $row["logon_port"];
        $DSLAMLOGON = $row["logon_id"];
        $DSLAMPW = $row["logon_pw"];
        $Backup    = $DSLAM_NAME.".tar";
        $VlanIPTV = $row["vlan_iptv"];
        $vlan_ACS_NW = $row["vlan_acs"];
        $vlan_Fbox = $row["vlan_pppoe"];
        $Vector_location = $row["vectoring"];
        $Dslam_conf    = $row["dslam_conf"];
        $DSLAMTYP = $row["dslam_type"];
        $acs_type = $row["acs_type"];
    }
}
    if ($debug == true) echo "$DSLAM_NAME $logon_ip Port $logon_port User:$DSLAMLOGON PW:$DSLAMPW IPTV:$VlanIPTV ACS:$vlan_ACS_NW F-BOX:$vlan_Fbox Vectoringlocation = $Vector_location Dslam_conf = $Dslam_conf Dslam_typ = $DSLAMTYP <br /><br /><br />";
    $no_location = $Vector_location ;
    if ($DSLAM_NAME != '' and $logon_ip != '' and $logon_port != '' and $DSLAMLOGON != '' and $DSLAMPW != '' ) $no_location = false;
    if ($DSLAM_NAME != (NULL) and $logon_ip != (NULL) and $logon_port != (NULL) and $DSLAMLOGON != (NULL) and $DSLAMPW != (NULL) ) $no_location = false ;

  
  foreach ($products as $row) {
    $Gpon_up          = substr($row["gpon_upload"],0);
    $Gpon_down        = substr($row["gpon_download"],0);
    $Ethernet_up_down = substr($row["ethernet_up_down"],0);
    $Produkt          = substr($row["produkt"],0);
    $Service          = substr($row["produkt_index"],0);
  }

  if ( $terminal_type == "7270") {
    $Transfermode     = "atm";
    $Spectrumprofile  = 2;  // muss ATM sein
  }
  if ( $Location_id == 5 ) {
    $Transfermode     = "ptm";
    $Spectrumprofile  = 1;  // muss ptm sein
    $dpboprofile      = "";
    $DPBO             = "0";
  }

?>

<h3 class="CustTechConf">DSLAM</h3>
<div class="CustTechConf">
  <?php
    $PPPoE_Location = null;
    $PPPoE_Adresse = null;
    $PPPoE_Type = null;
    $PPPoE_Adresse1 = '';   # wenn 2 PPPoE Server am gleichen Netz laufen
    
    $sqlIpRanges = "select * from ip_ranges where id = '".$customer['ip_range_id']."'";
    $db_range = $db->query($sqlIpRanges);
    $rangedb = $db_range->fetch_all(MYSQLI_ASSOC);
    
    if (isset($rangedb[0])) {
        $PPPoE_Location = $rangedb[0]['pppoe_location'];
        $PPPoE_Adresse = $rangedb[0]['pppoe_ip'];
        $PPPoE_Type = $rangedb[0]['pppoe_type'];
        
        if ('WiSoTEL' === \Wisotel\Configuration\Configuration::get('companyName')) {
            if ($PPPoE_Adresse == "213.61.107.128" ) $PPPoE_Adresse1 = '195.178.0.6';
        } elseif ('Stadtwerke Bühl' === \Wisotel\Configuration\Configuration::get('companyName')) {
            $PPPoE_Adresse1 = $PPPoE2_IP;
        }
    }

    $db_range->close();
    
    $Adminsave      = "admin save";
    #$logon_port     = "23";
    /* changed jl
    $PPPoE_Location = "PPPoE POP Zentrale";
    $PPPoE_Adresse  = "10.20.7.254";
    $PPPoE_Type     = "Radius";
    */ 
    
    if ($Vlan != "") $vlan_Fbox        = $Vlan;
    $Vlan           = "7";
    $vlan_ACS         = "9";
    $VlanIPTVlocal = "8";
    $Profile        = "PPPoE";
    $tftpserver = \Wisotel\Configuration\Configuration::get('tftpServer');
    $Backup_id    = "admin software-mngt database upload actual-active $tftpserver:dm_complete_";
    #$Vector_location = true;
    #$Dslam_conf     = true;
    #############################################
    # Biberach spezifisch
    #############################################
#    if ( $Vlan == '') {
        #$Vlan = "7";
        #$vlan_Fbox =  "3001";
#    }
    #$vlan_ACS_NW = "3701";
    #$VlanIPTV = "3501";


    switch ($Location_id) {
/* changed jl
     case 26:
     case 29:
        $DSLAM_LOGON    = "Suedbahnho";
        $DSLAM_NAME     = "Suedbahnhof";
        $Backup         = "Suedbahnhof.tar";
        $Backup         = "Suedbahnhof.tar";
        #host$Backup_id    = "admin software-mngt database upload actual-active $tftpserver:dm_complete_";
        $Adminsave      = "admin save";
        $logon_ip       = "10.20.40.10";
        $logon_port     = "55023";
        $PPPoE_Location = "PPPoE POP Zentrale";
        $PPPoE_Adresse  = "10.20.7.254";
        $PPPoE_Type     = "Mikrotik";
        $Vlan           = "200";
        $vlan_Fbox         = '200';
        $VlanIPTV        = "630";
        $VlanIPTVlocal = $VlanIPTV;
         $vlan_ACS = "";
        $Profile        = "PPPoE";
        $DSLAMPW        =    "zeag@2017";
        $tag            = false;
        $Dslam_conf     = true;
        break;

     case 4:
        $DSLAM_LOGON    = "Mobilfunk";
        $DSLAM_NAME     = "Mobilfunk";
        $Backup         = "Mobilfunk.tar";
        $Adminsave      = "admin save";
        $logon_ip       = "10.20.7.4";
        $logon_port     = "55555";
         $PPPoE_Location = "PPPoE POP Zentrale";
          $PPPoE_Adresse  = "10.20.7.254";
          $PPPoE_Type     = "Mikrotik";
          $Vlan           = "200";
        $Profile        = "PPPoE";
        $Dslam_conf     = true;
        break;
 */
 
/*     case 9:
        $DSLAM_LOGON    = "BIB_1_MA1";
        $DSLAM_NAME     = "BIB_1_MA1";
        $Backup         = "BIB_1_MA1.tar";
        $logon_ip       = "10.20.30.10";
        break;

     case 10:
        $DSLAM_LOGON    = "BIB_2_MA1";
        $DSLAM_NAME     = "BIB_2_MA1";
        $Backup         = "BIB_2_MA1.tar";
        $logon_ip       = "10.20.30.11";
        break;
        
      case 11:
        $DSLAM_LOGON    = "BIB_3_MA1";
        $DSLAM_NAME     = "BIB_3_MA1";
        $Backup         = "BIB_3_MA1.tar";
        $logon_ip       = "10.20.30.12";
        break;
        
      case 12:
        $DSLAM_LOGON    = "BIB_13_MA1";
        $DSLAM_NAME     = "BIB_13_MA1";
        $Backup         = "BIB_13_MA1.tar";
        $logon_ip       = "10.20.30.24";
        break;
 
     case 13:
        $DSLAM_LOGON    = "BIB_4_MA1";
        $DSLAM_NAME     = "BIB_4_MA1";
        $Backup         = "BIB_4_MA1.tar";
        $logon_ip       = "10.20.30.13";
        break;
        
      case 14:
        $DSLAM_LOGON    = "BIB_5_MA1";
        $DSLAM_NAME     = "BIB_5_MA1";
        $Backup         = "BIB_5_MA1.tar";
        $logon_ip       = "10.20.30.14";
        break;


     case 19:
        $DSLAM_LOGON    = "BIB_6_MA1";
        $DSLAM_NAME     = "BIB_6_MA1";
        $Backup         = "BIB_6_MA1.tar";
        $logon_ip       = "10.20.30.15";
        break;

      case 20:
        $DSLAM_LOGON    = "BIB_7_MA1";
        $DSLAM_NAME     = "BIB_7_MA1";
        $Backup         = "BIB_7_MA1.tar";
        $logon_ip       = "10.20.30.16";
        break;

      case 21:
        $DSLAM_LOGON    = "BIB_8_MA1";
        $DSLAM_NAME     = "BIB_8_MA1";
        $Backup         = "BIB_8_MA1.tar";
        $logon_ip       = "10.20.30.17";
        break;

      case 22:
        $DSLAM_LOGON    = "BIB_9_MA1";
        $DSLAM_NAME     = "BIB_9_MA1";
        $Backup         = "BIB_9_MA1.tar";
        $logon_ip       = "10.20.30.18";
        break;

      case 23:
        $DSLAM_LOGON    = "BIB_10_MA1";
        $DSLAM_NAME     = "BIB_10_MA1";
        $Backup         = "BIB_10_MA1.tar";
        $logon_ip       = "10.20.30.20";
        break;

     

      case 24:
        $DSLAM_LOGON    = "BIB_11_MA1";
        $DSLAM_NAME     = "BIB_11_MA1";
        $Backup         = "BIB_11_MA1.tar";
        $logon_ip       = "10.20.30.21";
        break;
 
     case 25:
     case 30:
        $DSLAM_LOGON    = "BIB_12_MA1";
        $DSLAM_NAME     = "BIB_12_MA1";
        $Backup         = "BIB_12_MA1.tar";
        $logon_ip       = "10.20.30.22";
        break;
        
    */    
    }
    
        #############################################
        # Biberach spezifisch Ende
        #############################################
        #############################################
        # Gemrichheim spezifisch 
        #############################################
    #if ( $Vlan == '') $Vlan = "3001";
    #$vlan_ACS_NW = "3701";
    #$VlanIPTV = "3501";


    if ($debug == true) echo "$DSLAM_NAME $logon_ip Port $logon_port User:$DSLAMLOGON PW:$DSLAMPW IPTV:$VlanIPTV ACS:$vlan_ACS_NW F-BOX:$vlan_Fbox Vectoringlocation = $Vector_location Dslam_conf = $Dslam_conf<br /><br /><br />";


    if ($Dslam_conf == true and $Autologon == true) {  
      if ($Vector_location == false) $Vectoring = "";
     #echo "Location = $Vector_location  Vectroing = $Vectoring <br />";
      #echo "<span class='text blue'>Aktion im DSLAM $DSLAMTYP = $Dslam_comando</span><br /><br />";
          ?>
      <a  class="fancy-button outline round  grey"href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Experte"  ?>">&nbsp;Ansicht Experte&nbsp;&nbsp;</a>&nbsp;&nbsp;
      <a  class="fancy-button outline round  green"href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando="  ?>"> &nbsp;Ansicht Kundenteam&nbsp;&nbsp;</a><br> <br>

        <?php
       if ($Dslam_comando == "Experte" and $ansicht != 'antwort') {
        echo "<br />TV = $tv_service &nbsp&nbsp; Vlantag = $tag &nbsp&nbsp; VLAN = $Vlan &nbsp&nbsp; Vlan_Fbox = $vlan_Fbox &nbsp; KVZ_A = $KVZ_a DPBO = $Default_DPBO ESEL = $esel_wert Default vlan = $Default_VLAN<br> <br> Partition_ID = $partition_id Location id = $Location_id DSLAM-Name = $DSLAM_NAME DslamLogon = $DSLAMLOGON DSLAM PW = xxxx no Lcation = $no_location ACS = $acs_type<br />";
      }
    if (($Location_id == 26 or $Location_id == 29) and $ansicht != 'antwort') {
      echo " Fbox-login = $FBOX_LOGON";
      ?>

      <a  class="fancy-button outline round  grey" href="<?php echo "$FBOX_LOGON" ?>" target="_blank">&nbsp;FBOX_LOGON Suedbahnhof&nbsp;</a><br>
      <?php
     }
    
/*    if ($debug == true ) {
        echo "<pre> Produkta <br />";
        var_dump ($products);
        #echo "<br /> Service = $Service";
        echo " location <br />";
        var_dump ($locationsdb);
        echo "</pre>";
    }
*/
    if ($ansicht != 'antwort') {
          echo "<br /><span class='text red'>Achtung !!! </span><span class='text black'> die roten Kommandos ändern Werte im DSLAM Radius oder im Mikrotik bitte nur verwenden wenn keine Warnungen zu sehen sind</span><br /><br />";
          echo "<hr class='red' />";
          echo '<p style="font-weight: bold;">DSLAM '.$DSLAM_NAME.' Kommandos</p>';

        if ($Dslam_comando == "Experte" and $ansicht != 'antwort') {
          echo "<span class='text blue'>DSLAM Logon: </span>";
          echo "<a href='$DSLAM_LOGON' target='_blank'>&lt;$DSLAM_NAME&gt;</a> ";
        
     
         echo "<span class='text blue'>  $DSLAMTYP</span> KVZ Zusatz = $KVZ_a Logon IP = $logon_ip Port = $logon_port<br /><br />";
        }
    }

      $detail = "";
          switch ($DSLAMTYP) {

        case 'huawei':
        case 'ALU':
        if ($ansicht != 'antwort') {    
            if ($DemoDslam == true) {
                ?>
                <a class="fancy-button outline round blue" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Status" ?>">&nbsp; Status </a>&nbsp;&nbsp;
                <a class="fancy-button outline round blue" href="<?php echo" index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Status_maximal" ?>">&nbsp;Status max </a>&nbsp;&nbsp;
                <a class="fancy-button outline round blue" href="<?php echo" index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Config" ?>">&nbsp;&nbsp;&nbsp;Info &nbsp;&nbsp;</a>&nbsp;&nbsp;
                <a class="fancy-button outline round red" href="<?php echo" index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Status" ?>">&nbsp;Port Reset </a>&nbsp;&nbsp;
                <a class="fancy-button outline round red" href="<?php echo" index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Status" ?>">&nbsp;Einrichten </a>&nbsp;&nbsp;
                <a class="fancy-button outline round red" href="<?php echo" index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Status" ?>">&nbsp;Sperren </a>&nbsp;&nbsp;
                <a class="fancy-button outline round green" href="<?php echo" index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Status" ?>">&nbsp;Freischalten </a>&nbsp;&nbsp;
                <a class="fancy-button outline round green" href="<?php echo" index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Status" ?>">&nbsp;Bandbreite </a>&nbsp;&nbsp;
                <a class="fancy-button outline round red" href="<?php echo" index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Status" ?>">&nbsp;Kunde löschen </a>&nbsp;&nbsp;<br><br>
                <?php
            } else {
                ?>
                <a class="fancy-button outline round blue" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Status" ?>">&nbsp; Status </a>&nbsp;&nbsp;
                <a class="fancy-button outline round blue" href="<?php echo" index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Status_maximal" ?>">&nbsp;Status max </a>&nbsp;&nbsp;
                <a class="fancy-button outline round blue" href="<?php echo" index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Config" ?>">&nbsp;&nbsp;&nbsp;Info &nbsp;&nbsp;</a>&nbsp;&nbsp;
                <a class="fancy-button outline round red" href="<?php echo" index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Port_Reset" ?>">&nbsp;Port Reset </a>&nbsp;&nbsp;
                <a class="fancy-button outline round red" href="<?php echo" index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_einrichten" ?>">&nbsp;Einrichten </a>&nbsp;&nbsp;
                <a class="fancy-button outline round red" href="<?php echo" index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_sperren" ?>">&nbsp;Sperren </a>&nbsp;&nbsp;
                <a class="fancy-button outline round green" href="<?php echo" index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_freischalten" ?>">&nbsp;Freischalten </a>&nbsp;&nbsp;
                <a class="fancy-button outline round green" href="<?php echo" index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Bandbreite" ?>">&nbsp;Bandbreite </a>&nbsp;&nbsp;
                <a class="fancy-button outline round red" href="<?php echo" index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Loeschen" ?>">&nbsp;Kunde löschen </a>&nbsp;&nbsp;<br><br>
                <?php
            }
        }
        if ($Dslam_comando == "Experte" and $ansicht != 'antwort') {
          echo "<br /><br /><span class='text black'>Equipment Kommandos</span> => <br />";
         ?>
         <br>
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Equipmentslot"  ?>">&nbsp;Equipmentslot&nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_VLAN"  ?>">&nbsp;Vlan&nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Service"  ?>">&nbsp;service&nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Diagnose_SFP"  ?>">&nbsp;Diagnose_SFP&nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=ShowSoftware"  ?>">&nbsp;zeige Sofware Version&nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=SNTP"  ?>">&nbsp;Zeit&nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=SNTPset"  ?>">&nbsp;Zeit einstellen&nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Backup"  ?>">&nbsp;Backup&nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Backupshow"  ?>">&nbsp;Backup show&nbsp; </a>&nbsp;&nbsp;<br><br>
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DPBO"  ?>">&nbsp;DPBO show&nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DPBO set"  ?>">&nbsp;DPBO modify&nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Promt"  ?>">&nbsp;Promt to $DSLAM_NAME&nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_setup"  ?>">&nbsp;INIT Config all <?php echo "$DSLAM_NAME" ?>&nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Test"  ?>">&nbsp;Test Logon <?php echo "$DSLAM_NAME" ?>&nbsp; </a>&nbsp;&nbsp;
           <?php
          echo "<br /><br /><span class='text grey'>xDSL Kommandos</span> =><br /> ";
          ?>
          <br>
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_show_Port"  ?>">&nbsp;show Port&nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Line_up"  ?>">&nbsp;Leitungen up &nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Line_upsum"  ?>">&nbsp;Summe Leitungen up &nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Line_opup"  ?>">&nbsp;Leitungen down &nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Line"  ?>">&nbsp;zeige alle Leitungen&nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Vector"  ?>">&nbsp;Vectoring Info&nbsp; </a>&nbsp;&nbsp;<br><br>
          <a  class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Vectorinit"  ?>">&nbsp;Vectoring Einschalten&nbsp; </a>&nbsp;&nbsp;
           <?php
          echo "<br /><br /><span class='text blue'>GPON Kommandos</span> => <br />";
          ?>
          <br>
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=ShowPonOptics"  ?>">&nbsp;GPON Information </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=ShowOntOptics"  ?>">&nbsp;ONT Information&nbsp;</a>&nbsp;&nbsp;
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=ShowOntStatus"  ?>">&nbsp;Status all ONT&nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=ShowOntupS"  ?>">&nbsp;Summe ONT up&nbsp;</a>&nbsp;&nbsp;
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=ShowOntdownS"  ?>">&nbsp;Summe ONT down&nbsp;</a>&nbsp;&nbsp;
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=ShowOntup"  ?>">&nbsp;alle ONT up&nbsp; </a>&nbsp;&nbsp;
          <a  class="fancy-button outline round grey" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=ShowOntdown"  ?>">&nbsp;alle ONT down&nbsp;</a>&nbsp;&nbsp;<br><br>
        <?php
          }
         break;

        case 'ISKRATEL';
            if ($Dslam_comando != "Experte") {
                ?>

                <a class="fancy-button outline round green" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Status"  ?>">&nbsp;Status&nbsp; </a>&nbsp;&nbsp;
                <a class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Einrichten"  ?>">&nbsp;einrichten&nbsp; </a>&nbsp;&nbsp;
                <a class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Loeschen"  ?>">&nbsp;Kunde löschen&nbsp; </a>&nbsp;&nbsp;
                <a class="fancy-button outline round blue" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Anzeigen"  ?>">&nbsp;Zeige CLI&nbsp; </a>&nbsp;&nbsp;<br><br>

                <?php
            } else {
                ?>

                <br />
                <a class="fancy-button outline round green" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Status"  ?>">&nbsp;Status&nbsp; </a>&nbsp;&nbsp;
                <a class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Einrichten"  ?>">&nbsp;einrichten&nbsp; </a>&nbsp;&nbsp;
                <a class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Loeschen"  ?>">&nbsp;Kunde löschen&nbsp; </a>&nbsp;&nbsp;
                <a class="fancy-button outline round blue" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Anzeigen"  ?>">&nbsp;Zeige CLI&nbsp; </a>&nbsp;&nbsp;
                <a class="fancy-button outline round blue" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=ONTFirmware"  ?>">&nbsp;zeige ONT FW&nbsp; </a>&nbsp;&nbsp;
                <a class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=ONTSynch"  ?>">&nbsp;Synch ONT&nbsp; </a>&nbsp;&nbsp;
                <a class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=ONTReboot"  ?>">&nbsp;reboot ONT&nbsp; </a>&nbsp;&nbsp;
                <a class="fancy-button outline round blue" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Bridge"  ?>">&nbsp;zeige Bridge&nbsp; </a>&nbsp;&nbsp;<br><br>
                <a class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=ONTFWneu"  ?>">&nbsp;neue Firmware&nbsp; </a>&nbsp;&nbsp;
                <a class="fancy-button outline round blue" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Bridges"  ?>">&nbsp;zeige all Bridge&nbsp; </a>&nbsp;&nbsp;
                <a class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Backup"  ?>">&nbsp;Backup zum tftp&nbsp; </a>&nbsp;&nbsp;
                <a class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Backup_download"  ?>">&nbsp;Backup download&nbsp; </a>&nbsp;&nbsp;
                <a class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Directories_Pono"  ?>">&nbsp;Directories Pono&nbsp; </a>&nbsp;&nbsp;
                <a class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Backup_laden"  ?>">&nbsp;Backup laden&nbsp; </a>&nbsp;&nbsp;<br><br>

                <?php
            }

            //echo "<a href='index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Status'><span class='text green'>&lt;Status&gt;</span> </a>&nbsp;&nbsp;";
            //echo "<a href='index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Einrichten'><span class='text red'>&lt;Einrichten&gt; </span></a>&nbsp;&nbsp;";
            //echo "<a href='index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Loeschen'><span class='text red'>&lt;Kunde löschen&gt; </a>&nbsp;&nbsp;";
            //echo "<a href='index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Anzeigen'><span class='text blue'>&lt;Zeige CLI&gt; </a>&nbsp;&nbsp;";
            //echo "<a href='index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=ONTFirmware'><span class='text blue'>&lt;Zeige ONT FW&gt; </a>&nbsp;&nbsp;";
            //echo "<a href='index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=ONTSynch'><span class='text red'>&lt;Sycn ONT&gt; </a>&nbsp;&nbsp;";
            //echo "<a href='index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=ONTReboot'><span class='text red'>&lt;Reboot ONT&gt; </a>&nbsp;&nbsp;";
            //echo "<a href='index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Bridge'><span class='text blue'>&lt;Zeige Bridge&gt; </a>&nbsp;&nbsp;";
            //echo "<a href='index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=ONTFWneu'><span class='text red'>&lt;neu Firmware&gt; </a>&nbsp;&nbsp;";

            break;
        }

        if ($ansicht != 'antwort') {
            switch  ($acs_type) {
            case 'purtel':
             echo "<hr class='red' />";    
             echo '<p style="font-weight: bold;">ACS Kommandos Purtel</p>';

                if ($acs_id == '') {
                ?>
                   <br>
                   <a  class="fancy-button outline round  red" href="<?php echo "$acs_cpe_neu"  ?>"target="_blank">&nbsp;ACS CPE anlegen &nbsp;&nbsp;</a>&nbsp;&nbsp;
                   <a  class="fancy-button outline round  blue" href="<?php echo "$acs_cpe_suche"  ?>"target="_blank">&nbsp;Suche ACS &nbsp;&nbsp;</a>&nbsp;&nbsp;
                   <a  class="fancy-button outline round  blue" href="<?php echo "$acs_cpe_suche_k"  ?>"target="_blank">&nbsp;Suche Kunde &nbsp;&nbsp;</a>&nbsp;&nbsp;
                   <a  class="fancy-button outline round  blue" href="<?php echo "$acs_cpe_parameter"  ?>"target="_blank">&nbsp;Parameter &nbsp;&nbsp;</a>&nbsp;&nbsp;
                   <a  class="fancy-button outline round  blue" href="<?php echo "$acs_get_numbers"  ?>"target="_blank">&nbsp;get Numbers &nbsp;&nbsp;</a><br><br>
                 <?php
                   if ($Dslam_comando == "Experte") {
                   
                   
                   
                        echo "ACS API = //ipcom.purtel.com/index.php?".$acs_kunde_zuweisen_show;
                        if ($ACS_SIP != '') echo "<br />".$ACS_SIP;
                foreach ($ACS as $temp) {
                  echo "<br />".$temp;     
                    #echo "MAC Adress = $mac_address emei Adress =     $emei_address    emei-dhcp = $emei_address_dhcp";

                }
                /****
                        if ($ACS0 != '') echo "<br />".$ACS0;
                        if ($ACS1 != '') echo "<br />".$ACS1;
                        if ($ACS2 != '') echo "<br />".$ACS2;
                        if ($ACS3 != '') echo "<br />".$ACS3;
                */
                        if ($ACS0 != '') echo "<br />".$ACS0;
                        echo "<br />".$acs_kunde_zuweisen_show1;
                        echo "<br />".$acs_kunde_zuweisen_show2;

                    }
                }
                if ($acs_id != '') {
                    
                  ?>
                    <br>
                    <a  class="fancy-button outline round  blue" href="<?php echo "$acs_cpe_suche_k" ?>" target="_blank">&nbsp;Suche Kunde &nbsp;&nbsp;</a>&nbsp;&nbsp;
                    <a  class='fancy-button outline round  red' href='<?php echo $acs_kunde_zuweisen_sip; ?>' target='_blank'>Kunde zuweisen (nur VoIP)</a>&nbsp;&nbsp;
                    <a  class='fancy-button outline round  red' href='<?php echo $acs_kunde_zuweisen; ?>' target='_blank'>Kunde zuweisen (VoIP + PPPoE)</a>&nbsp;&nbsp;
                    <a  class="fancy-button outline round  blue" href="<?php echo "$acs_cpe_suche" ?>"target="_blank"> &nbsp;Suche CPE &nbsp;&nbsp;</a>&nbsp;&nbsp;
                    <a  class="fancy-button outline round  blue" href="<?php echo "$acs_cpe_parameter" ?>" target="_blank"> &nbsp;Parameter &nbsp;&nbsp;</a>&nbsp;&nbsp;<br /><br />
                    <a  class="fancy-button outline round  blue" href="<?php echo "$acs_get_numbers" ?>" target="_blank">&nbsp;get Numbers &nbsp;&nbsp;</a>&nbsp;&nbsp;
                    <a  class="fancy-button outline round  blue" href="<?php echo "$acs_get_PW" ?>" target="_blank"> &nbsp;Fernwartung &nbsp;&nbsp;</a>&nbsp;&nbsp;
                    <a  class="fancy-button outline round  red" href="<?php echo "$acs_kunde_entfernen" ?>" target=">_blank" >&nbsp;Kunde löschen &nbsp;&nbsp;</a>&nbsp;&nbsp;
                    <a  class="fancy-button outline round  red" href="<?php echo "$acs_cpe_loeschen" ?>" target="_blank"> &nbsp;CPE löschen &nbsp;&nbsp;</a><br><br>
         
          
                <?php
                } # end acs purtel

                if ($Dslam_comando == "Experte") {
                    echo "ACS API = //ipcom.purtel.com/index.php?".$acs_kunde_zuweisen_show;
                    if ($ACS_SIP != '') echo "<br />".$ACS_SIP;
                    foreach ($ACS as $temp) {
                      echo "<br />".$temp;     
                    }
            
                    if ($ACS0 != '') echo "<br />".$ACS0;
            /***        if ($ACS1 != '') echo "<br />".$ACS1;
                    if ($ACS2 != '') echo "<br />".$ACS2;
                    if ($ACS3 != '') echo "<br />".$ACS3;
            */
                    echo "<br />".$acs_kunde_zuweisen_show1;
                    echo "<br />".$acs_kunde_zuweisen_show2;
                }
                
            break;
            case 'zeag':
                 echo "<hr class='red' />";    

                  ?>
                  <p style="font-weight: bold;">ACS Axiros</p>
                    <a class="fancy-button outline round  red" href="#" onclick="acsManage('activate', 'both')" target="_self">&nbsp;CPE im ACS registrieren/ändern&nbsp;&nbsp;</a>&nbsp;&nbsp;
                    
                    <?php

                    if (!empty($cpeid)) {
                        $acsWorkflowUrl = $axirosAcs['url'].'/live/AXCustomerSupportPortal/#/cpe/cpeid/'.$cpeid.'?tab=Workflows';
                        echo '<a class="fancy-button outline round  blue" href="'.$acsWorkflowUrl.'" target="_blank"> &nbsp;am ACS zum CPE Workflow&nbsp;&nbsp;</a>&nbsp;&nbsp;';
                    } else {
                        echo '<a class="fancy-button outline round  blue" href="'.$this->generateUrl('axiros-acs/fetch-cpeid-by-cid2', ['id' => $_GET['id']]).'" target="_blank"> &nbsp;Fetch CPE-ID by CID2&nbsp;&nbsp;</a>&nbsp;&nbsp;';
                    }
                    
                    ?>

                    <a class="fancy-button outline round  blue" href="<?php echo $axirosAcs['url']; ?>" target="_blank"> &nbsp;am ACS Anmelden&nbsp;&nbsp;</a>&nbsp;&nbsp;
                    <a class="fancy-button outline round  red" href="#" onclick="acsManage('delete', 'both')" target="_self">&nbsp;CPE im ACS löschen&nbsp;&nbsp;</a><br><br>
          
                <?php
                if ($Dslam_comando == "Experte") {
                    echo '<a href="'.$this->generateUrl('axiros-acs', ['customerId' => $_GET['id']]).'" target="_blank" class="fancy-button outline round  red">CPE im ACS registrieren (mit 49 statt 0)</a><br />';
                    echo '<a href="'.$this->generateUrl('axiros-acs/voice-by-cid2', ['id' => $_GET['id'], 'action' => 'activate']).'" target="_blank" class="fancy-button outline round  red">CPE im ACS registrieren (Nur voice mit - voice_by_cid2)</a><br />';
                    echo '<a href="'.$this->generateUrl('axiros-acs/voice-by-cid2', ['id' => $_GET['id'], 'action' => 'delete']).'" target="_blank" class="fancy-button outline round  red">Lösche Service (voice_by_cid2) im ACS</a><br />';
                }

            break;
            
            }# end acs switch

            
            // workaround to catch error from file_get_contents
            /*$fileGetContentsError = null;

            set_error_handler(function ($errno, $errstr, $errfile, $errline, $errcontext) use (&$fileGetContentsError) {
                $fileGetContentsError = sprintf('Error: %s in %s on line %s',
                    $errstr,
                    $errfile,
                    $errline
                );
            }, E_ALL);

            $json = file_get_contents($acs_cpe_suche_k);

            if (null === $fileGetContentsError) {
                $obj = json_decode($json);
                echo $obj->access_token;
            } else {
                echo $fileGetContentsError;
            }

            restore_error_handler(); // revert to previous error handler
            */
            
            #echo "MAC Adress = $mac_address emei Adress =     $emei_address    emei-dhcp = $emei_address_dhcp";

            #echo "<a href='$acs_get_cpe' target='_blank'><span class='text green'>&lt;CPE Antwort &gt;&nbsp;&nbsp;&nbsp;<span class='text green'><br /></a> ";
            

                   
        }
    }

    if ($Autologon == true){
        if ($PPPoE_Type !== "purtel-radius") {
          echo "<hr class='red' />";
          echo '<p style="font-weight: bold;">PPPoE '.$PPPoE_Type.' Kommandos</p>';
        }
      if ($PPPoE_Type == "bartels") {
        if ($Dslam_comando == "Experte") {
            echo "<span class='text blue'> <br />die nachfolgende Zeile muss beim PPPoE Bartels Server eingefügt werden </span><br /><br />"; 
            echo('   user "' . $clientid . '" { password "' . $password . '"; ip-address ' . $IP_A . '; }' . "<br />");
            
            echo "<span class='fbox statustext $lblclass'><br /> F-Box <a href='https://$IP_As' target='_blank' >IP = $IP_As</a> RLogin  = $Rlogin PW = $RPassw <span id='statusval'>\n</span><br /><br />";
        }
      } else {
        if ($PPPoE_Type == "mikrotik") {
            $subip = substr ($IP_As, 0, 8);
            $passwd=$password;
            $password = str_replace("$", '\$', $password);
            $password = str_replace("?", '\?', $password);
            if ($Profile == "new2015")    $Profile = $Cust_Produkt;
            $Profile = str_replace("H", '', $Profile);
            $Profile = str_replace("D", '', $Profile);
            #echo "$Dslam_comando";
        }
        
        display_mikrotik(
            $id,
            $clientid,
            $IP_A,
            $IP_As,
            $password,
            $PPPoE_Location,
            $PPPoE_Adresse,
            $PPPoE_Adresse1,
            $Rlogin,
            $RPassw,
            $Dslam_comando,
            $Profile,
            $Cust_Produkt,
            $PPPoE_Type,
            $purtel_master,
            $customer,
            $db
        );

        $Debug = false;
        switch ($Dslam_comando) {
          case 'PPPoE_Labor':
            $Dslam_comando = 'PPPoE_Status';
            $PPPoE_Adresse  = $pppoeLaborAccess['ipaddress'];
            $PPPoE_Location = "Labor DSLAM";
            $Profile        = "pppoe";            
            $IP_As = '';
            $PPPoEPW = $PPPoEPWW;
          break;
          case 'PPPoE_Labor_IP':
            $Dslam_comando = 'PPPoE_Mikrotik';
            $PPPoE_Location = "Labor DSLAM";
            $PPPoE_Adresse  = $pppoeLaborAccess['ipaddress'];
            $IP_As = '';
            $Profile        = "pppoe";            
            $PPPoEPW = $PPPoEPWW;
          break;
          case 'PPPoE_Labor_neue_IP':
            $Dslam_comando = 'PPPoE_Mikrotik_neue_IP';
            $PPPoE_Location = "Labor DSLAM";
            $IP_As = '';
            $Profile        = "pppoe";            
            $PPPoE_Adresse  = $pppoeLaborAccess['ipaddress'];
            $PPPoEPW = $PPPoEPWW;
          break;
          case 'PPPoE_Labor_loeschen':
            $Dslam_comando = 'PPPoE_Loeschen1';
            $Profile        = "pppoe";            
            $IP_As = '';
            $PPPoE_Location = "Labor DSLAM";
            $PPPoE_Adresse  = $pppoeLaborAccess['ipaddress'];
            $PPPoEPW = $PPPoEPWW;
          break;


        }
       switch ($Dslam_comando) {
          case 'PPPoE_Status':
            $NL = "<br />";
            echo "<span class='text green'><H1> Antworten vom PPPoE (".$PPPoE_Adresse.") </H1> </span><br />";
            # echo "wird geloescht ";
            $API = new routeros_api();
            $API->debug = false;
            if ($API->connect($PPPoE_Adresse, $PPPoEAPI, $PPPoEPW)) {
              echo "<pre>";
              $API->write('/ppp/secret/print', false);
              $API->write('?name='.$clientid,true);
              # $API->write('=.proplist=.id');
              $ARRAYS = $API->read();
              # print_r ($ARRAYS);$lsa_kvz_partitionsdb
              $PPP_ADD = $ARRAYS[0]['remote-address'];
              $PPP_PW = $ARRAYS[0]['password'];
              # $Testa =  $API->com('/ppp/secret/print', false);
              # print_r ($ARRAYS);
              # print_r ($PPP_ADD);
              $max = sizeof($ARRAYS);
              $OUTPUT = " <span class='text red'>Kunde nicht im PPPoE $PPPoE_Location Secret </span><br />";
              if ($max <> 0) $OUTPUT = " <span class='text green'> Kunde im PPPoE $PPPoE_Location Secret vorhanden CPE IP = $PPP_ADD Password = $PPP_PW <br /> Profile = $Profile </span><br />";
              print_r ($NL.$clientid.$OUTPUT.$NL);
              $API->write('/ppp/active/print', false);
              $API->write('?name='.$clientid, true);
              # $API->write('=.proplist=.id');
              $ARRAYS = $API->read();
              # print_r ($ARRAYS);
              $PPP_ADD = $ARRAYS[0]['address'];
              $PPP_UP = $ARRAYS[0]['uptime'];
              $PPP_Mac = $ARRAYS[0]['caller-id'];
              $max = sizeof($ARRAYS);
              $OUTPUT = "<span class='text red'> Kunde nicht Aktiv und nicht im Internet im PPPoE $PPPoE_Location </span><br /><br />";
              if ($max <> 0) {
                print_r ($READ);
                if (sizeof($READ)==0) $OUTPUT = " <span class='text green'>Kunde in PPPoE $PPPoE_Location Aktiv im Internet ><br />IP = $PPP_ADD MAC = $PPP_Mac läuft seit $PPP_UP </span><br /> ";
                # print_r ($READ);
              }
              print_r ($NL.$clientid.$OUTPUT.$NL.$NL);
              $API->disconnect();
              echo "</pre>";
            }

            $NL = "<br />";
            if ($PPPoE_Adresse1 != "") {
                echo "<span class='text green'><H1> Antworten vom PPPoE (".$PPPoE_Adresse1.") </H1> </span><br />";
                # echo "wird geloescht ";
                $API = new routeros_api();
                $API->debug = false;
                if ($API->connect($PPPoE_Adresse1, $PPPoEAPI, $PPPoEPW)) {
                    echo "<pre>";
                    $API->write('/ppp/secret/print', false);
                    $API->write('?name='.$clientid,true);
                    # $API->write('=.proplist=.id');
                    $ARRAYS = $API->read();
                    # print_r ($ARRAYS);$lsa_kvz_partitionsdb
                    $PPP_ADD = $ARRAYS[0]['remote-address'];
                    $PPP_PW = $ARRAYS[0]['password'];
                    # $Testa =  $API->com('/ppp/secret/print', false);
                    # print_r ($ARRAYS);
                    # print_r ($PPP_ADD);
                    $max = sizeof($ARRAYS);
                    $OUTPUT = " <span class='text red'>Kunde nicht im PPPoE $PPPoE_Location Secret </span><br />";
                    if ($max <> 0) $OUTPUT = " <span class='text green'> Kunde im PPPoE $PPPoE_Location Secret vorhanden CPE IP = $PPP_ADD Password = $PPP_PW <br /> Profile = $Profile </span><br />";
                    print_r ($NL.$clientid.$OUTPUT.$NL);
                    $API->write('/ppp/active/print', false);
                    $API->write('?name='.$clientid, true);
                    # $API->write('=.proplist=.id');
                    $ARRAYS = $API->read();
                    # print_r ($ARRAYS);
                    $PPP_ADD = $ARRAYS[0]['address'];
                    $PPP_UP = $ARRAYS[0]['uptime'];
                    $PPP_Mac = $ARRAYS[0]['caller-id'];
                    $max = sizeof($ARRAYS);
                    $OUTPUT = "<span class='text red'> Kunde nicht Aktiv und nicht im Internet im PPPoE $PPPoE_Location </span><br /><br />";
                    if ($max <> 0) {
                        print_r ($READ);
                        if (sizeof($READ)==0) $OUTPUT = " <span class='text green'>Kunde in PPPoE $PPPoE_Location Aktiv im Internet ><br />IP = $PPP_ADD MAC = $PPP_Mac läuft seit $PPP_UP </span><br /> ";
                        # print_r ($READ);
                    }
                    print_r ($NL.$clientid.$OUTPUT.$NL.$NL);
                    $API->disconnect();
                    echo "</pre>";
                }
            }

            $Dslam_comando = '';
            break;

            // check if default customer (wisotel@purtel.com) is active
            case 'PPPoE_Status_for_Default_Customer':
                echo "<span class='text green'><H1> Antworten vom PPPoE </H1> </span><br />";
                
                $API = new routeros_api();
                $API->debug = false;
                
                if ($API->connect($PPPoE_Adresse, $PPPoEAPI, $PPPoEPW)) {
                    echo "<pre>";
                    
                    $API->write('/ppp/active/print', false);
                    $API->write('?name=wisotel@purtel.com', true);
                    
                    $ARRAYS = $API->read();
                    $PPP_ADD = $ARRAYS[0]['address'];
                    $PPP_UP = $ARRAYS[0]['uptime'];
                    $PPP_Mac = $ARRAYS[0]['caller-id'];
                    $max = count($ARRAYS);
                    $OUTPUT = "<span class='text red'> Kunde nicht Aktiv und nicht im Internet im PPPoE $PPPoE_Location </span><br /><br />";
                    $isActive = false;

                    if ($max <> 0) {
                        print_r($READ);
                        
                        if (count($READ)==0) {
                            $OUTPUT = " <span class='text green'>Kunde in PPPoE $PPPoE_Location Aktiv im Internet ><br />IP = $PPP_ADD MAC = $PPP_Mac läuft seit $PPP_UP </span><br /> ";
                            $isActive = true;
                        }
                    }

                    echo '<br />wisotel@purtel.com -> '.$OUTPUT.'<br /><br />';

                    $API->disconnect();

                    if ($isActive) {
                        $wkvUserMacAddress = str_replace(':', '', $PPP_Mac);
                        $wkvUserMacAddress = dechex(hexdec($wkvUserMacAddress) -4);

                        $query = $db->query('SELECT `clientid`, `id`, `mac_address` 
                            FROM `customers` WHERE `mac_address` LIKE "%'.$wkvUserMacAddress.'%" GROUP BY `clientid`'
                        );

                        if ($query->num_rows == 1) {
                            $possibleUser = '';

                            while ($data = $query->fetch_assoc()) {
                                $possibleUser .= sprintf("wkv-customer client-id: <a href='"
                                    ."http://172.20.2.221/wkv2/index.php?menu=customer&func=vertrag&id=%s'>%s</a><br />"
                                    ."wkv-customer mac_address: %s",
                                    $data['id'],
                                    $data['clientid'],
                                    $data['mac_address']
                                );
                            }

                            echo '<br />'.$possibleUser.'<br />';
                        } else {
                            echo '<br />unable to find unique user by cwmp: '.$wkvUserMacAddress.'<br />';
                        }
                    }

                    echo "</pre>";
                }
                $Dslam_comando = '';
            break;

          case 'PPPoE_Load':
            $NL = "<br />";
            echo "<span class='text green'><H1> Antworten vom PPPoE </H1> </span><br />";
            # echo "wird geloescht ";
            $API = new routeros_api();
            $API->debug = false;
            if ($API->connect($PPPoE_Adresse, $PPPoEAPI, $PPPoEPW)) {
              echo "<pre>"; 
              $API->write('/system/resource/print', true);
              $ARRAYS = $API->read();
              print_r ($ARRAYS);
              $API->disconnect();
              echo "</pre>";
            }
            $Dslam_comando = '';
            break;
          case 'PPPoE_Mikrotik_A':
          $Debug = true;
          case 'PPPoE_Mikrotik':
            echo "<br /> ";
            echo "<span class='text green'><H1> Antworten vom PPPoE </H1> </span><br />";
            $API = new routeros_api();
            $API->debug = false;
            $OUTPUT = " Kunde wurde im PPPoE eingerichtet ";
            if ($IP_As == "" and $PPPoE_Location != "Labor DSLAM") {
              echo " <span class='text red'><P> <H1>Keine IP Adresse beim Kunden gefunden, einrichten nicht möglich </H1></P></span><br />";
            } else {
              if ($API->connect($PPPoE_Adresse, $PPPoEAPI, $PPPoEPW)) {
                echo "<pre>";
                if ($PPPoE_Location != "Labor DSLAM") {
                $RETURN = $API->comm("/ppp/secret/add", array (
                  "name"            => $clientid,
                  "password"        => $passwd,
                  "remote-address"  => $IP_As,
                  "service"         => "pppoe",
                  "profile"         => $Profile,
                ));
                }
                else {

                $RETURN = $API->comm("/ppp/secret/add", array (
                  "name"            => $clientid,
                  "password"        => $passwd,
                  #"remote-address"  => $IP_As,
                  "service"         => "pppoe",
                  "profile"         => $Profile,
                ));
                }
                # if (in_array("*", $RETURN, true)) {
                #   echo "Irix enthalten";
                #   $OUTPUT = " Kunde ist schon vorhanden im  PPPoE <br /> ";
                # }
                # print_r ($RETURN.$Profile);
                if (strlen($RETURN)) {
                  echo "<span class='text green'>Kunde wurde im PPPoE $PPPoE_Location eingerichtet </span><br /><br />";
                } else {
                  echo "<span class='text red'>Kunde im PPPoE $PPPoE_Location ist schon vorhanden </span><br /><br />";
                }
                # print_r (strlen($RETURN)." srtlen");
                # $maxy = sizeof($RETURN);
                # if (sizeof($RETURN) > 4) $OUTPUT = " Kunde ist schon vorhanden im  PPPoE ";
                # print_r ($maxy);
                if ( $Debug == true) print_r ($RETURN);
                $API->disconnect();
                echo "</pre>";
              }
            }

            if ($PPPoE_Adresse1 != "") {
                if ($IP_As == "" and $PPPoE_Location != "Labor DSLAM") {
                    echo " <span class='text red'><P> <H1>Keine IP Adresse beim Kunden gefunden, einrichten nicht möglich </H1></P></span><br />";
                } else {
                    if ($API->connect($PPPoE_Adresse1, $PPPoEAPI, $PPPoEPW)) {
                        echo "<pre>";
                        if ($PPPoE_Location != "Labor DSLAM") {
                            $RETURN = $API->comm("/ppp/secret/add", array (
                                "name"            => $clientid,
                                "password"        => $passwd,
                                "remote-address"  => $IP_As,
                                "service"         => "pppoe",
                                "profile"         => $Profile,
                            ));
                        } else {
                            $RETURN = $API->comm("/ppp/secret/add", array (
                                "name"            => $clientid,
                                "password"        => $passwd,
                                #"remote-address"  => $IP_As,
                                "service"         => "pppoe",
                                "profile"         => $Profile,
                            ));
                        }
                        # if (in_array("*", $RETURN, true)) {
                        #   echo "Irix enthalten";
                        #   $OUTPUT = " Kunde ist schon vorhanden im  PPPoE <br /> ";
                        # }
                        # print_r ($RETURN.$Profile);
                        if (strlen($RETURN)) {
                            echo "<span class='text green'>Kunde wurde im PPPoE $PPPoE_Location IP $PPPoE_Adresse1 eingerichtet </span><br /><br />";
                        } else {
                            echo "<span class='text red'>Kunde im PPPoE $PPPoE_Location IP $PPPoE_Adresse1 ist schon vorhanden </span><br /><br />";
                        }
                        # print_r (strlen($RETURN)." srtlen");
                        # $maxy = sizeof($RETURN);
                        # if (sizeof($RETURN) > 4) $OUTPUT = " Kunde ist schon vorhanden im  PPPoE ";
                        # print_r ($maxy);
                        if ( $Debug == true) print_r ($RETURN);
                        $API->disconnect();
                        echo "</pre>";
                    }
                }
            }

            $Dslam_comando = '';
            break;
          case 'PPPoE_Loeschen':
            echo "$NL";
            ?>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a  class="fancy-button outline round  red large18" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=PPPoE_Loeschen1"  ?>"> &nbsp;Löschen im PPPoE Bestätigen&nbsp;</a><br /><br />
            
            <?php
            $Dslam_comando ='';
            break;
          case 'PPPoE_Loeschen1':
            $NL = "<br />";
            echo "<span class='text green'><H1> Antworten vom PPPoE </H1> </span><br />";
            # echo "wird geloescht ";
            $API = new routeros_api();
            $API->debug = false;
            if ($API->connect($PPPoE_Adresse, $PPPoEAPI, $PPPoEPW)) {
              echo "<pre>";                         
              $API->write('/ppp/secret/print', false);
              $API->write('?name='.$clientid, false);
              $API->write('=.proplist=.id');
              $ARRAYS = $API->read();
              # print_r ($ARRAYS);
              $max = sizeof($ARRAYS);
              $OUTPUT = " Kunde nicht im PPPoE $PPPoE_Location Secret ";
              if ($max <> 0) {
                $API->write('/ppp/secret/remove', false);
                $API->write('=.id=' . $ARRAYS[0]['.id']);
                $ARRAYS = $API->read();
                # print_r ($ARRAYS);
                if (sizeof($ARRAYS)==0) $OUTPUT = " <span class='text red'> Kunde im PPPoE $PPPoE_Location Secret gelöscht </span><br /><br />";
              }
              print_r ($NL.$clientid.$OUTPUT.$NL);
              $API->write('/ppp/active/print', false);
              $API->write('?name='.$clientid, false);
              $API->write('=.proplist=.id');
              $ARRAYS = $API->read();
              $max = sizeof($ARRAYS);
              $OUTPUT = " Kunde nicht Aktive im PPPoE $PPPoE_Location ";
              if ($max <> 0) {
                print_r ($READ);
                $API->write('/ppp/active/remove', false);
                $API->write('=.id=' . $ARRAYS[0]['.id']);
                $READ = $API->read();
                if (sizeof($READ)==0) $OUTPUT = " <span class='text red'>Kunde in PPPoE $PPPoE_Location Aktive Queue gelöscht</span><br /><br /> ";
                # print_r ($READ);
              }
              print_r ($NL.$clientid.$OUTPUT.$NL.$NL);
              $API->disconnect();
              echo "</pre>";
            }
            $Dslam_comando = '';

            if ($PPPoE_Adresse1 != "") {
                if ($API->connect($PPPoE_Adresse1, $PPPoEAPI, $PPPoEPW)) {
                    echo "<pre>";
                    $API->write('/ppp/secret/print', false);
                    $API->write('?name='.$clientid, false);
                    $API->write('=.proplist=.id');
                    $ARRAYS = $API->read();
                    # print_r ($ARRAYS);
                    $max = sizeof($ARRAYS);
                    $OUTPUT = " Kunde nicht im PPPoE $PPPoE_Location Secret ";
                    if ($max <> 0) {
                        $API->write('/ppp/secret/remove', false);
                        $API->write('=.id=' . $ARRAYS[0]['.id']);
                        $ARRAYS = $API->read();
                        # print_r ($ARRAYS);
                        if (sizeof($ARRAYS)==0) $OUTPUT = " <span class='text red'> Kunde im PPPoE $PPPoE_Location Secret gelöscht </span><br /><br />";
                    }
                    print_r ($NL.$clientid.$OUTPUT.$NL);
                    $API->write('/ppp/active/print', false);
                    $API->write('?name='.$clientid, false);
                    $API->write('=.proplist=.id');
                    $ARRAYS = $API->read();
                    $max = sizeof($ARRAYS);
                    $OUTPUT = " Kunde nicht Aktive im PPPoE $PPPoE_Location ";
                    if ($max <> 0) {
                    print_r ($READ);
                    $API->write('/ppp/active/remove', false);
                    $API->write('=.id=' . $ARRAYS[0]['.id']);
                    $READ = $API->read();
                    if (sizeof($READ)==0) $OUTPUT = " <span class='text red'>Kunde in PPPoE $PPPoE_Location Aktive Queue gelöscht</span><br /><br /> ";
                    # print_r ($READ);
                    }
                    print_r ($NL.$clientid.$OUTPUT.$NL.$NL);
                    $API->disconnect();
                    echo "</pre>";
                }
            }

            break;

          case 'PPPoE_Mikrotik_neue_IP':
            echo " ";
            echo "<span class='text green'><H1> Antworten vom PPPoE </H1> </span><br />";
            $API = new routeros_api();
            $ARRAYKH = array();
            $API->debug = false;
            if ($IP_As == "" and $PPPoE_Location != "Labor DSLAM"){
              echo " <span class='text red'><P> <H1>Keine IP Adresse beim Kunden gefunden, einrichten nicht möglich </H1></P></span><br />";
            } else {
              if ($API->connect($PPPoE_Adresse, $PPPoEAPI, $PPPoEPW)) {
                echo "<pre>";
                $API->write('/ppp/secret/print', false);
                $API->write('?name='.$clientid, false);
                $API->write('=.proplist=.id');
                $ARRAYS = $API->read();
                $API->write('/ppp/secret/remove', false);
                $API->write('=.id=' . $ARRAYS[0]['.id']);
                $READ = $API->read();
                print_r ($READ);
                if ($PPPoE_Location != "Labor DSLAM") {
                $RETURN = $API->comm("/ppp/secret/add", array (
                  "name"            => $clientid,
                  "password"        => $passwd,
                  "remote-address"  => $IP_As,
                  "service"         => "pppoe",
                  "profile"         => $Profile,
                ));
                }
                else {

                $RETURN = $API->comm("/ppp/secret/add", array (
                  "name"            => $clientid,
                  "password"        => $passwd,
                  #"remote-address"  => $IP_As,
                  "service"         => "pppoe",
                  "profile"         => $Profile,
                ));
                }
                $API->write('/ppp/active/print', false);
                $API->write('?name='.$clientid, false);
                $API->write('=.proplist=.id');
                $ARRAYS = $API->read();
                $API->write('/ppp/active/remove', false);
                $API->write('=.id=' . $ARRAYS[0]['.id']);
                $READ = $API->read();
                $ARRAYKH[5] = $ARRAYS[5];
                $API->disconnect();
                echo $ARRAYKH[5];
                echo "<span class='text green'>$clientid Kunde mit neuer IP $IP_As wurde im PPPoE $PPPoE_Location IP $PPPoE_Adresse eingerichtet </span><br /><br />";
                echo "</pre>";
              }
            }

            if ($PPPoE_Adresse1 != "") {
                if ($IP_As == "" and $PPPoE_Location != "Labor DSLAM"){
                    echo " <span class='text red'><P> <H1>Keine IP Adresse beim Kunden gefunden, einrichten nicht möglich </H1></P></span><br />";
                } else {
                    if ($API->connect($PPPoE_Adresse1, $PPPoEAPI, $PPPoEPW)) {
                        echo "<pre>";
                        $API->write('/ppp/secret/print', false);
                        $API->write('?name='.$clientid, false);
                        $API->write('=.proplist=.id');
                        $ARRAYS = $API->read();
                        $API->write('/ppp/secret/remove', false);
                        $API->write('=.id=' . $ARRAYS[0]['.id']);
                        $READ = $API->read();
                        print_r ($READ);
                        if ($PPPoE_Location != "Labor DSLAM") {
                            $RETURN = $API->comm("/ppp/secret/add", array (
                            "name"            => $clientid,
                            "password"        => $passwd,
                            "remote-address"  => $IP_As,
                            "service"         => "pppoe",
                            "profile"         => $Profile,
                            ));
                        } else {
                            $RETURN = $API->comm("/ppp/secret/add", array (
                            "name"            => $clientid,
                            "password"        => $passwd,
                            #"remote-address"  => $IP_As,
                            "service"         => "pppoe",
                            "profile"         => $Profile,
                            ));
                        }
                        $API->write('/ppp/active/print', false);
                        $API->write('?name='.$clientid, false);
                        $API->write('=.proplist=.id');
                        $ARRAYS = $API->read();
                        $API->write('/ppp/active/remove', false);
                        $API->write('=.id=' . $ARRAYS[0]['.id']);
                        $READ = $API->read();
                        $ARRAYKH[5] = $ARRAYS[5];
                        $API->disconnect();
                        echo $ARRAYKH[5];
                        echo "<span class='text green'>$clientid Kunde mit neuer IP $IP_As wurde im PPPoE $PPPoE_Location IP $PPPoE_Adresse1 eingerichtet </span><br /><br />";
                        echo "</pre>";
                    }
                }
            }


            $Dslam_comando = '';
            break;
 /*         case PPPoE_Mikrotik_Test_IP_Bittenfeld:
            echo " ";
            echo "<span class='text green'><H1> Antworten vom PPPoE </H1> </span><br />";
            $API = new routeros_api();
            $ARRAYKH = array();
            $API->debug = false;
            if ($IP_As == ""){
              echo " <span class='text red'><P> <H1>Keine IP Adresse beim Kunden gefunden, einrichten nicht möglich </H1></P></span><br />";
            } else {
              if ($API->connect($PPPoE_Adresse, $PPPoEAPI, $PPPoEPW)) {
                echo "<pre>";
                $IP_As ="37.99.202.250";
                $API->write('/ppp/secret/print', false);
                $API->write('?name='.$clientid, false);
                $API->write('=.proplist=.id');
                $ARRAYS = $API->read();
                $API->write('/ppp/secret/remove', false);
                $API->write('=.id=' . $ARRAYS[0]['.id']);
                $READ = $API->read();
                print_r ($READ);
                $API->comm("/ppp/secret/add", array (
                  "name"            => $clientid,
                  "password"        => $passwd,
                  "remote-address"  => $IP_As,
                  "service"         => "pppoe",
                  "profile"         => $Profile,
                ));
                $API->write('/ppp/active/print', false);
                $API->write('?name='.$clientid, false);
                $API->write('=.proplist=.id');
                $ARRAYS = $API->read();
                $API->write('/ppp/active/remove', false);
                $API->write('=.id=' . $ARRAYS[0]['.id']);
                $READ = $API->read();
                $ARRAYKH[5] = $ARRAYS[5];
                $API->disconnect();
                echo $ARRAYKH[5];
                echo "<span class='text green'>$clientid Kunde mit neuer IP $IP_As wurde im PPPoE $PPPoE_Location eingerichtet </span><br /><br />";
                echo "</pre>";
              }
            }
            break;
    */        
            case 'PPPoE_Backup':
                $NL = "<br />";
                echo "<span class='text green'><H1> Antworten vom PPPoE </H1> </span><br />";
                #echo "wird getestet ";
                $API = new routeros_api();
                $API->debug = false;
                if ($API->connect($PPPoE_Adresse, $PPPoEAPI, $PPPoEPW)) {
                  echo "<pre>";                         
                  $API->write("/system/script/run",false);
                  $API->write("=.id=*1");
                  $ARRAYS = $API->read();
                   $max = sizeof($ARRAYS);
                  #echo "Max = $max<br />";
                  
                  if ($max == 1) {
                    print_r ($ARRAYS);
                  }
                  else {
                    echo " Mikrotik $PPPoE_Location adresse $PPPoE_Adresse Backup wurde zum FTP Server gesendet<br /><br /><br /><br /></pre>";
                  }
                  $API->disconnect();
                 }
            $Dslam_comando = '';
            break;
            case 'PPPoE_Active':
                $NL = "<br />";
                echo "<span class='text green'><H1> Antworten vom PPPoE </H1> </span><br />";
                #echo "wird getestet ";
                $API = new routeros_api();
                $API->debug = false;
                if ($API->connect($PPPoE_Adresse, $PPPoEAPI, $PPPoEPW)) {
                  echo "<pre>";                         
                  $API->write("/ppp/active/print",false);
                  $API->write("=count-only=");
                  $ARRAYS = $API->read();
                   $max = sizeof($ARRAYS);
                  #echo "Max = $max<br />";
                  
                  if ($max != 1) {
                    print_r ($ARRAYS);
                  }
                  else {
                    echo "<span class='text blue'><H1>Mikrotik $PPPoE_Location adresse $PPPoE_Adresse <br />active users  ";
                    print_r ($ARRAYS);
                    echo "</H1> </span><br /><br /><br /><br /></pre>";
                  }
                  $API->disconnect();
                 }

                if ($PPPoE_Adresse1 != "") {
                    if ($API->connect($PPPoE_Adresse1, $PPPoEAPI, $PPPoEPW)) {
                        echo "<pre>";
                        $API->write("/ppp/active/print",false);
                        $API->write("=count-only=");
                        $ARRAYS = $API->read();
                        $max = sizeof($ARRAYS);
                        #echo "Max = $max<br />";
                        if ($max != 1) {
                            print_r ($ARRAYS);
                        } else {
                            echo "<span class='text blue'><H1>Mikrotik $PPPoE_Location adresse $PPPoE_Adresse <br />active users  ";
                            print_r ($ARRAYS);
                            echo "</H1> </span><br /><br /><br /><br /></pre>";
                        }
                        $API->disconnect();
                    }
                }

                $Dslam_comando = '';
                 
                break;

            case 'PPPoE_Backupall':
                $NL = "<br />";
                echo "<span class='text green'><H1> Antworten vom PPPoE </H1> </span><br />";
                #echo "wird getestet ";
                $API = new routeros_api();
                $API->debug = false;
                foreach ($PPPoE_Server as $PPPoE_Adresse) {
                    if ($API->connect($PPPoE_Adresse, $PPPoEAPI, $PPPoEPW)) {
                        echo "<pre>";                                                                                    
                        $API->write("/system/script/run",false);
                        $API->write("=.id=*1");
                        $ARRAYS = $API->read();
                         $max = sizeof($ARRAYS);
                        #echo "Max = $max<br />";
                        if ($max == 1) {
                            print_r ($ARRAYS);
                        }
                        else {
                            echo " Mikrotik  $PPPoE_Adresse Backup wurde zum FTP Server gesendet<br /><br /><br /><br /></pre>";
                        }
                        $API->disconnect();
                   }
                }
                 $Dslam_comando = '';
            break; 

            #################################################
            # Radius API
            #################################################

            case 'Radius':
                radiusWR ($clientid,$password,$IP_A);
            break;
            
            case 'RLesen':
                radiusRD ($clientid,$password,$IP_A);
            break;
            case 'Rdelete':
                ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a  class="fancy-button outline round  red large18" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Rdelete1"  ?>"> &nbsp;Löschen im Radius Bestätigen&nbsp;</a><br /><br />

                <?php

                $Dslam_comando = ''; 
            break;

            case 'Rdelete1':
                radiusDEL ($clientid,$password,$IP_A);

            break;

            case 'Rdis':
                ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a  class="fancy-button outline round  red large18" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=Rdis1"  ?>"> &nbsp;Sperren im Radius Bestätigen&nbsp;</a><br /><br />

                <?php

                $Dslam_comando = ''; 
            break;
            case 'Rdis1':
                radiusDIS ($clientid);
                $Dslam_comando = '';
            break;

            case 'Rena':
                radiusENA ($clientid);
                $Dslam_comando = '';

            break;


            #################################################
            # Radius API ende
            #################################################

        }
      }
      // printf ("$ROT SQL Result $result1 KVZ $KVZ_Name KVZ_a $KVZ_a KVZ_s $KVZ_s  Stringcompare $tt");
          if ($Dslam_comando == "Experte") {
          echo "<span class='text blue'>Labor: </span>";
          echo "<a href='khl://puttytelnet:".$dslamLaborAccess['ipaddress'].":".$dslamLaborAccess['port']."' target='_blank'>DSLAM im Labor</a>";
         }
    } else {
      echo "\n\n<span class='text blue'>     PPPoE logon nicht freigegeben </span>";
    }
    ###################################################################################################################################################
    #    DSLAM Anfang
    ###################################################################################################################################################

    if (null !== $customerObject->getCardId() && $customerObject->getCardId()->getRfOverlayEnabled()) {
        ?>
        <hr class="red" />
        <p style="font-weight: bold;">RF-Overlay</p>
        Signalstärke: <span class="clone-element" data-element-id="technical_data_rf_overlay_signal_strength"></span>
        <a class="fancy-button outline round blue" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=statusRfOverlay"; ?>" style="margin-top: -6px; margin-right: 5px;">RF-Overlay Status</a>
        <a class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=activateRfOverlay"; ?>" style="margin-top: -6px; margin-right: 5px;">RF-Overlay aktivieren</a>
        <a class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=deactivateRfOverlay"; ?>" style="margin-top: -6px;">RF-Overlay deaktivieren</a>
        <a class="fancy-button outline round green" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=signalRfOverlay"; ?>" style="margin-top: -6px;">RF-Overlay Signalstärke</a><br />
        <span style="font-size: 11px; line-height: 30px; font-style: italic; color: #a6241b">
            Änderungen der Signalstärke müssen gespeichert werden, bevor sie verwendet werden.
        </span><br />
        <span class="clone-element" data-element-id="submit-button-save"></span>
        <br />
        <br />
        <?php
    }

    if ($Autologon == true ) {
      #    if ($Dslam_comando == "Experte") {    
            echo "<hr class='red' />";
            echo '<p style="font-weight: bold;">Purtel Logon</p>';
            echo "<span id='purtelAdmin' class='admin text blue'>Purtel Admin:</span>";
        #    echo "<span class='admin'><a target='_blank' href='https://www2.purtel.com/res/res100227/index.php?username=$purtelACSadmin&passwort=$purtelACS_PW&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1&admin_ns_suchbegriff=".$purtelUsername."&tab_id=2&marke=VoIP-Status'>Super Admin</a></span> ";

            $purtelReseller = \Wisotel\Configuration\Configuration::get('purtelReseller');

            $purtelLogonUrl = 'https://www2.purtel.com/res/'.$purtelReseller.'/index.php?username=%s&passwort=%s'
                .'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1'
                .'&admin_ns_suchbegriff='.$purtel_master.'&tab_id=2&marke=VoIP-Status';

            if ($Dslam_comando == "Experte") {
                echo sprintf('<span class="fancy-button outline round red"><a target="_blank" href="%s">%s</a></span>',
                    sprintf($purtelLogonUrl, 
                        \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername'),
                        \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword')
                    ),
                    'Super Admin'
                )."\n";
            }

            $purtelAdminAccounts = $doctrine->getRepository('AppBundle\Entity\PurtelAdminAccount')->findByAuthorizedUser(
                $this->get('security.token_storage')->getToken()->getUser()
            );

            foreach ($purtelAdminAccounts as $purtelAdminAccount) {
                echo sprintf('<span class="fancy-button outline round grey"><a target="_blank" href="%s">%s</a></span>',
                    sprintf($purtelLogonUrl, $purtelAdminAccount->getUsername(), $purtelAdminAccount->getPassword()),
                    $purtelAdminAccount->getIdentifier()
                )."\n";
            }
            
            echo "<hr class='red' />";
            echo "<br>";
            #echo 'ACS API = '.$acs_kunde_zuweisen_show;
        #}
    } else {
      echo "<span class='text blue'>Auto login zu den DSLAM's nicht freigegeben<br /><br /></span>";
    }
    if ($Dslam_conf == true and $Autologon == true  and $DSLAM_NAME != 'Mobilfunk'  and $Dslam_comando != '' and $Dslam_comando != "ACSzuweisen" and $Dslam_comando != "Radius" and $Dslam_comando != "RLesen" and $Dslam_comando != "Rdelete" and $no_location == false ) {
####################################################################################################
#  switch DSLAM Type
#################################################################################################### 
    if ($Dslam_comando == 'DSLAM_Kunden_Loeschen') {                    
        $Dslam_comando = '';
        ?>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a  class="fancy-button outline round  red large18" href="<?php echo "index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Loeschen1"  ?>"> &nbsp;Löschen im DSLAM Bestätigen&nbsp;</a><br /><br />

        <?php
    } else {
        $dslamCommunicator = null;

        if (null !== $location) {
            // find dslam communicator by vendor, model/processor and optionally by software-release version
            $dslamCommunicator = $this->get('dslam_chooser')->findCommunicationClass(
                $location->getType(), $location->getProcessor(), $location->getCurrentSoftwareRelease()
            );
        }

        if (null === $dslamCommunicator) {
            // no communicator was found, try old way
            $location_id = '';
            DSLAM_exec ($Dslam_comando,$location_id,$id,$Dslam_comando1);
        } else {
            // create a service container for dependency injection used for communication classes
            $container = new ContainerBuilder();

            // add some services to the container
            $backup_JN = 'yes';
            $container->set('backup_JN', $backup_JN);
            $container->set(Customer::class, $customerObject);
            $container->set(Location::class, $location);
            $container->set(Card::class, $cardObject);
            $container->set(MainProduct::class, $customer['Service']);
    
            // set service container to dslam communicator for dependency injection
            $dslamCommunicator->setContainer($container);

            // set connection gateway to dslam communicator
            $dslamCommunicator->setGateway(new SshGateway($location->getIpAddress(), $location->getLogonPort(), 10));

            // perform the login
            $dslamCommunicator->login(['username' => $location->getLogonUsername(), 'password' => $location->getLogonPassword()]);

            // Example: Using an alias for a method
            // $method = $dslamCommunicator->getMethodByAlias($Dslam_comando);
            // echo sprintf('<pre>%s</pre>', $dslamCommunicator->$method());

            // Example: Calling a method directly (in this case, dependency injection was used for the required arguments)
            // echo sprintf('<pre>%s</pre>', $dslamCommunicator->findMac());
            
            echo sprintf('<pre>%s</pre>', $dslamCommunicator->$Dslam_comando());
        }
    }
    /*switch ($DSLAMTYP) {
###################################################################################
# ALU end Iskratel begin
###################################################################################    
    case 'ISKRATEL':
      $FSAN = substr ($terminal_type, -4);
      switch ($KVZ_a) {
      case 'PONO':
      case 'GPON':
      $sublineident     = substr ($lineident, -2);
      $PON         = substr ($lineident, -4,1);
      $Einrichten4 ="";
      if ($firmware_version <> "") {
        $Einrichten4 ="onu image download-activate-commit 1/$PON/$sublineident $firmware_version\n";
      }
      switch ($FSAN) {
        case '2426':
        
          $Einrichten = "bridge add 1-1-$PON-$sublineident/gpononu gem 7$sublineident gtp  $Service downlink-pppoe vlan $Vlan epktrule $Service tagged sip eth all wlan 1 rg-bpppoe\n";
          $Einrichten1 = "cpe rg wan modify 1/$PON/$sublineident-gponport vlan $Vlan pppoe-usr-id $clientid pppoe-password $passwd\n";
          $Einrichten2 = "cpe-mgr add local 1-1-$PON-$sublineident/gpononu gem 5$sublineident gtp 1\n";
          $Einrichten3 = "cpe voip add 1/$PON/$sublineident/1 dial-number $purtelUsername username $purtelUsername password $purtelPasswort voip-server-profile WisoTel\n";
        break;
        case '2301':
        case '2311':
        
          $Einrichten = "bridge add 1-1-$PON-$sublineident/gpononu gem 7$sublineident gtp $Service downlink vlan $Vlan epktrule $Service tagged eth 1\n";
          $Einrichten1 = "";
          $Einrichten2 = "";
          $Einrichten3 = "";
        break;
        
      }
      
        switch ($Dslam_comando){
        
          case 'DSLAM Kunden Status':
          $usenet = fsockopen($logon_ip, $logon_port, $errno, $errstr, $cfgTimeOut);
          if(!$usenet) { 
            echo "Connection failed\n"; 
            exit(); 
          } 
          else { 
            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>"; 
            sleep(1);
            fputs ($usenet, "sysadmin\n");  
            sleep(1);
            fputs ($usenet, "sysadmin\n");
            sleep(1);
            fputs ($usenet, "onu show 1/$PON/$sublineident\n");
            sleep(2);
            fputs ($usenet, "onu status 1/$PON/$sublineident\n");
            sleep (4);
            fputs ($usenet, "bridge show 1-1-$PON-7$sublineident\n");
            sleep (2);
            fputs ($usenet, "exit\n");
            echo "<pre>";
            while (!feof($usenet)) { 
              $buffer = fgets($usenet, 400)."<BR>\n";
              $buffer = str_replace("Error", "<span class='text red'> Error </span>", $buffer); 
              echo "$buffer";
            } 
            fclose($usenet);
            echo "</pre>";
          }
        break;
        case 'DSLAM Kunden Loeschen':
          echo "$NL <a href='index.php?menu=customer&id=$id&area=SwitchTechConf&kommando=DSLAM_Kunden_Loeschen1'><span class='text red'><H1>&lt;Löschen im DSLAM Bestätigen&gt; </H1></a>";
            $Dslam_comando = ''; 
                break;
         
        case 'DSLAM Kunden Loeschen1':

          $usenet = fsockopen($logon_ip, $logon_port, $errno, $errstr, $cfgTimeOut);

          if(!$usenet) { 
            echo "Connection failed\n"; 
            exit(); 
          } 
          else { 
            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>"; 
            sleep(1);
            fputs ($usenet, "sysadmin\n");  
            sleep(1);
            fputs ($usenet, "sysadmin\n");
            sleep(1);
            fputs ($usenet, "bridge delete 1-1-$PON-$sublineident/gpononu all\n");
            sleep (3);
            fputs ($usenet, "onu clear 1/$PON/$sublineident\n");
            sleep (1);
            fputs ($usenet, "onu delete 1/$PON/$sublineident\n");
            sleep (5);
            fputs ($usenet, "yes\n");
            sleep (1);
            fputs ($usenet, "no\n");
            sleep (1);
            fputs ($usenet, "yes\n"); 
            sleep (5);
            
            fputs ($usenet, "exit\n");
            echo "<pre>";
            while (!feof($usenet)) { 
              $buffer = fgets($usenet, 400)."<BR>\n";
              $buffer = str_replace("Error", "<span class='text red'> Error </span>", $buffer); 
              echo "$buffer";
            } 
            fclose($usenet);
            echo "</pre>";
          }
        
        break;

        case 'DSLAM Kunden Einrichten':

          $usenet = fsockopen($logon_ip, $logon_port, $errno, $errstr, $cfgTimeOut);

          if(!$usenet) { 
            echo "Connection failed\n"; 
            exit(); 
          } 
          else { 
            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>"; 
            sleep(1);
            fputs ($usenet, "sysadmin\n");  
            sleep(1);
            fputs ($usenet, "sysadmin\n");
            sleep(1);
            fputs ($usenet, "onu set 1/$PON/$sublineident vendorid ZNTS serno fsan $Leitung meprof zhone-$FSAN \n");
            sleep (2);

            fputs ($usenet, $Einrichten);
            sleep (3);
            fputs ($usenet, "bridge stats enable 1-1-$PON-7$sublineident-gponport-$Vlan/bridge\n");
            sleep (2);
            fputs ($usenet, $Einrichten1);
            sleep (2);
            fputs ($usenet, $Einrichten2);
            sleep (2);

            if ($purtelUsername <> '') {
              fputs ($usenet, $Einrichten3);
              sleep (2);
              #fputs ($usenet, "cpe voip add 1/$PON/$sublineident/2 dial-number 071912206790 username $purtelUsername password $purtelPasswort voip-server-profile WisoTel\n");
              #sleep (2);

            }


            fputs ($usenet, "exit\n");
            echo "<pre>";
            while (!feof($usenet)) { 
              $buffer = fgets($usenet, 400)."<BR>\n";
              $buffer = str_replace("Error", "<span class='text red'> Error </span>", $buffer); 
              echo "$buffer";
            } 
            fclose($usenet);
            echo "</pre>";
          }
        
        break;

        case 'Anzeigen':
            echo "Lineident = $lineident Line = $sublineident -- PON = $PON <BR>";  
            echo  "<span class='text green'>Einrichten mit CLI</span><BR><BR>";
            echo  "onu set 1/$PON/$sublineident vendorid ZNTS serno fsan $Leitung meprof zhone-$FSAN\n<BR>";
            echo  $Einrichten."<BR>";
            echo  "bridge stats enable 1-1-$PON-7$sublineident-gponport-$Vlan/bridge\n<BR>";          
            if ($Einrichten1 <> "") echo  $Einrichten1."<BR>";
            if ($Einrichten2 <> "") echo  $Einrichten2."<BR>";
            if ($Einrichten3 <> "") echo  $Einrichten3."<BR>";
            if ($Einrichten4 <> "") echo  $Einrichten4."<BR>";
            
        
        break;
        
        case 'ONTFirmware':
          $usenet = fsockopen($logon_ip, $logon_port, $errno, $errstr, $cfgTimeOut);
          if(!$usenet) { 
            echo "Connection failed\n"; 
            exit(); 
          } 
          else { 
            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>"; 
            sleep(1);
            fputs ($usenet, "sysadmin\n");  
            sleep(1);
            fputs ($usenet, "sysadmin\n");
            sleep(1);
            fputs ($usenet, "onu image show 1/$PON/$sublineident\n");
            sleep(2);
            fputs ($usenet, "exit\n");
            echo "<pre>";
            while (!feof($usenet)) { 
              $buffer = fgets($usenet, 400)."<BR>\n";
              $buffer = str_replace("Error", "<span class='text red'> Error </span>", $buffer); 
              echo "$buffer";
            } 
            fclose($usenet);
            echo "</pre>";
          }
        break;
        
        case 'ONTSynch':
          $usenet = fsockopen($logon_ip, $logon_port, $errno, $errstr, $cfgTimeOut);
          if(!$usenet) { 
            echo "Connection failed\n"; 
            exit(); 
          } 
          else { 
            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>"; 
            sleep(1);
            fputs ($usenet, "sysadmin\n");  
            sleep(1);
            fputs ($usenet, "sysadmin\n");
            sleep(1);            
            fputs ($usenet, "onu resync 1-1-$PON-$sublineident \n");
            sleep(2);
            fputs ($usenet, "exit\n");
            echo "<pre>";
            while (!feof($usenet)) { 
              $buffer = fgets($usenet, 400)."<BR>\n";
              $buffer = str_replace("Error", "<span class='text red'> Error </span>", $buffer); 
              echo "$buffer";
            } 
            fclose($usenet);
            echo "</pre>";
          }
        break;

        case 'ONTReboot':
          $usenet = fsockopen($logon_ip, $logon_port, $errno, $errstr, $cfgTimeOut);
          if(!$usenet) { 
            echo "Connection failed\n"; 
            exit(); 
          } 
          else { 
            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>"; 
            sleep(1);
            fputs ($usenet, "sysadmin\n");  
            sleep(1);
            fputs ($usenet, "sysadmin\n");
            sleep(1);
            fputs ($usenet, "onu reboot 1-1-$PON-$sublineident \n");
            sleep(1);
            fputs ($usenet, "exit\n");
            echo "<pre>";
            while (!feof($usenet)) { 
              $buffer = fgets($usenet, 400)."<BR>\n";
              $buffer = str_replace("Error", "<span class='text red'> Error </span>", $buffer); 
              echo "$buffer";
            } 
            fclose($usenet);
            echo "</pre>";
          }
        break;
        
        case 'Bridge':
          $usenet = fsockopen($logon_ip, $logon_port, $errno, $errstr, $cfgTimeOut);
          if(!$usenet) { 
            echo "Connection failed\n"; 
            exit(); 
          } 
          else { 
            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>"; 
            sleep(1);
            fputs ($usenet, "sysadmin\n");  
            sleep(1);
            fputs ($usenet, "sysadmin\n");
            sleep(1);
            fputs ($usenet, "bridge show\n");
            sleep(1);
            fputs ($usenet, "exit\n");
            echo "<pre>";
            while (!feof($usenet)) { 
              $buffer = fgets($usenet, 400)."<BR>\n";
              $buffer = str_replace("Error", "<span class='text red'> Error </span>", $buffer); 
              echo "$buffer";
            } 
            fclose($usenet);
            echo "</pre>";
          }
        break;
        
        case 'ONTFWneu':
          $usenet = fsockopen($logon_ip, $logon_port, $errno, $errstr, $cfgTimeOut);
          if(!$usenet) { 
            echo "Connection failed\n"; 
            exit(); 
          } 
          else { 
            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>"; 
            sleep(1);
            fputs ($usenet, "sysadmin\n");  
            sleep(1);
            fputs ($usenet, "sysadmin\n");
            sleep(1);
            fputs ($usenet, $Einrichten4);
            sleep (1);
            fputs ($usenet, "exit\n");
            echo "<pre>";
            while (!feof($usenet)) { 
              $buffer = fgets($usenet, 400)."<BR>\n";
              $buffer = str_replace("Error", "<span class='text red'> Error </span>", $buffer); 
              echo "$buffer";
            } 
            fclose($usenet);
            echo "</pre>";
          }
        break;

        }
      break;
      
      }

    break;
    }*/
#######################################################################################
# Iskratel Ende Keymile Beginn
#######################################################################################    
        
    } else {
        if ($no_location == true) {
            echo " mit den Parametern kein Login im DSLAM möglich ";
      echo "<br />DSLAM-Name = $DSLAM_NAME DslamLogon = $DSLAMLOGON DSLAM PW = $DSLAMPW Logon IP = $logon_ip Logon Port $logon_port no Lcation = $no_location<br />";
        }
    }
    
    
  ?>
</div>

