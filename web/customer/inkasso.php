<?php

$this->denyAccessUnlessGranted('ROLE_CREATE_DEBT_COLLECTION_ORDER');

?>

<h3 class="CustInkasso">Inkassoauftrag</h3>
<div class="CustInkasso form">
    <span>
        <label for="CustomerClientid16"><?php echo \Wisotel\Configuration\Configuration::get('companyName'); ?> Nr.</label><br />
        <input id="CustomerClientid16" type="text" name="CustomerClientid" readonly="readonly" value="<?php setInputValue ($customer, 'clientid'); ?>">
    </span>
    <span>
        <label for="CustomerInkassoaccount16">Purtelaccount</label><br />
        <input id="CustomerInkassoaccount16" type="text" name="CustomerInkassoaccount" readonly="readonly" value="<?php echo $purtel_master; ?>">
    </span>
    <span class="double">
        <label for="CustomerInkassoRechnung16">Rechnungsnummern (durch , getrennt)</label><br />
        <input id="CustomerInkassoRechnung16" type="text" name="CustomerInkassoRechnung" value="<?php setInputValue ($customer, 'inkasso_rechnung'); ?>">
    </span>

    <span class="doublebox">
        <span>
            <label for="CustomerInkassoHauptforderung16">Hauptforderung</label><br />
            <input id="CustomerInkassoHauptforderung16" type="number" pattern="^\d+[.,]{0,1}\d{0,2}$" step="0.01" name="CustomerInkassoHauptforderung" value="<?php setInputValue ($customer, 'inkasso_hauptforderung'); ?>">
        </span>
        <span></span>
        <span>
            <label for="CustomerInkassoBankgebuehren16">Bankgebühren</label><br />       
            <input id="CustomerInkassoBankgebuehren16" type="number" pattern="^\d+[.,]{0,1}\d{0,2}$" step="0.01" name="CustomerInkassoBankgebuehren" value="<?php setInputValue ($customer, 'inkasso_bankgebuehren'); ?>">
        </span>
        <span>
            <label for="CustomerInkassoMahngebuehren16">Mahngebühren</label><br />
            <input id="CustomerInkassoMahngebuehren16" type="number" pattern="^\d+[.,]{0,1}\d{0,2}$" step="0.01" name="CustomerInkassoMahngebuehren" value="<?php setInputValue ($customer, 'inkasso_mahngebuehren'); ?>">
        </span>
        <span>
            <label for="CustomerInkassoDatumLetzteMahnung16">Datum der letzten Mahnung</label><br />
            <input id="CustomerInkassoDatumLetzteMahnung16" type="text" class="datepicker" value="<?php setInputValue ($customer, 'inkasso_datum_letzte_mahnung'); ?>" name="CustomerInkassoDatumLetzteMahnung" />
        </span>
        <span>
            <label for="CustomerInkassoDatumRuelastschrift16">Datum der Rücklastschrift</label><br />
            <input id="CustomerInkassoDatumRuelastschrift16" type="text" class="datepicker" value="<?php setInputValue ($customer, 'inkasso_datum_ruecklastschrift'); ?>" name="CustomerInkassoDatumRuelastschrift" />
        </span>
    </span>
    <span class="double">
        <label for="CustomerInkassoGrundRuelastschrift16">Grund der Rücklastschrift</label><br />
        <textarea id="CustomerInkassoGrundRuelastschrift16" name="CustomerInkassoGrundRuelastschrift"><?php setInputValue ($customer, 'inkasso_grund_ruecklastschrift'); ?></textarea>
    </span>

    <span class="four">
        <hr />
        <button id='CustomerInkassoSend' onclick='custInkassoSend()' type='button'>Inkassoauftrag an KSP senden</button>
    </span>
</div>