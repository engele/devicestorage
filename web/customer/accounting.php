<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_ACCOUNTING');

$disable = 'readonly="readonly"';
$rw = false;   

if ($authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_ACCOUNTING')) {
    $disable = '';   
    $rw = true;   
}

?>

<h3 class="CustAccounting">Kontodaten <a target="_blank" href="https://iport.infoscore.de">(Zur Bonitätsprüfung)</a></h3>
<div class="CustAccounting form">
    <span class="double">
        <label for="CustomerBankAccountHolderLastname04">Kontoinhaber</label><br />
        <input id="CustomerBankAccountHolderLastname04" type="text" maxlength="64" name="CustomerBankAccountHolderLastname" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'bank_account_holder_lastname'); ?>">
    </span>
    <span class="double">
        <label for="CustomerNoEze04">Einzugsermächtigung erteilt</label><br />
        <select id="CustomerNoEze04" name="CustomerNoEze" <?php echo $disable; ?>>
            <?php setOption ($option_no_eze, $customer, 'no_eze'); ?>
        </select>
    </span>
    <span>
        <label for="CustomerBankAccountBankname04">Name der Bank</label>
        <input id="CustomerBankAccountBankname04" type="text"  maxlength="64" name="CustomerBankAccountBankname" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'bank_account_bankname'); ?>">
    </span>
    <span>
        <label for="CustomerBankAccountBicNew04">BIC</label>
        <input id="CustomerBankAccountBicNew04" type="text" maxlength="13" name="CustomerBankAccountBicNew" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'bank_account_bic_new'); ?>">
    </span>
    <span class="double">
        <label for="CustomerBankAccountIban04">IBAN</label>
        <input id="CustomerBankAccountIban04" type="iban" class="uppercase" maxlength="30" name="CustomerBankAccountIban" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'bank_account_iban'); ?>">
    </span>
    <span>
        <label for="CustomerBankAccountNumber04">Kontonummer</label>
        <input id="CustomerBankAccountNumber04" type="text" maxlength="24" name="CustomerBankAccountNumber" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'bank_account_number'); ?>">
    </span>
    <span>
        <label for="CustomerBankAccountBic04">BLZ</label>
        <input id="CustomerBankAccountBic04" type="text" maxlength="24" name="CustomerBankAccountBic" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'bank_account_bic'); ?>">
    </span>
    <span class="double">
        <label for="CustomerPaymentPerformance04">Zahlungsverhalten</label><br />
        <select id="CustomerPaymentPerformance04" name="CustomerPaymentPerformance" <?php echo $disable; ?>>
            <?php  setOption ($option_payment_performance, $customer, 'payment_performance'); ?>
        </select>
    </span>

    <span class="double">
        <label for="CustomerBankAccountDebitor04">Debitor</label>
        <input id="CustomerBankAccountDebitor04" type="text" name="CustomerBankAccountDebitor" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'bank_account_debitor'); ?>">
    </span>
    <span class="double">
        <label for="CustomerBankAccountMandantenid04">Mandatsreferenz</label>
        <input id="CustomerBankAccountMandantenid04" type="text" name="CustomerBankAccountMandantenid" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'bank_account_mandantenid'); ?>">
    </span>


    <span class="double">
        <label for="CustomerBankAccountEmailaddress04">E-Mail Adresse für Rechnungsversand <small>(falls abweichend von Kontakt E-Mail)</small></label><br />
        <input id="CustomerBankAccountEmailaddress04" type="text" maxlength="128" name="CustomerBankAccountEmailaddress" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'bank_account_emailaddress'); ?>">
    </span>
    <span class="double"></span>
    <span class="one_half">
        <label for="CustomerBankAccountStreet04">Straße</label><br />
        <input id="CustomerBankAccountStreet04" type="text" maxlength="64" name="CustomerBankAccountStreet" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'bank_account_street'); ?>">
    </span>
    <span class="half">
        <label for="CustomerBankAccountStreetno04">Hausnummer</label><br />
        <input id="CustomerBankAccountStreetno04" type="text" maxlength="8" name="CustomerBankAccountStreetno" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'bank_account_streetno'); ?>">
    </span>
    <span class="half">
        <label for="CustomerBankAccountZipcode04">PLZ</label><br />
        <input id="CustomerBankAccountZipcode04" type="text" maxlength="8" name="CustomerBankAccountZipcode" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'bank_account_zipcode'); ?>">
    </span>
    <span class="one_half">
        <label for="CustomerBankAccountCity04">Stadt</label><br />
        <input id="CustomerBankAccountCity04" type="text" maxlength="64" name="CustomerBankAccountCity" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'bank_account_city'); ?>">
    </span>
    <?php if ($rw) { ?>
    <h4><br />E-Mail an Kunden senden</h4>
    <p>
        <label for="EMailTemplate">E- Mail Template wählen</label><br />
        <select id="EMailTemplate" name="EMailTemplate">
            <option value='' selected>- bitte auswählen -</option>
            <?php
            $group_id = 0;
            $opt_group = false;

            foreach ($mail_temp as $key => $value) {
                if ($value['group_id'] == 3) {
                    $replace = [
                        '###ANREDE###' => htmlspecialchars($customer['title'], ENT_QUOTES),
                        '###NAME###' => htmlspecialchars($customer['lastname'], ENT_QUOTES),
                    ];

                    if ($customer['title'] == 'Herr') {
                        $replace['geehrte'] = 'geehrter';
                    }

                    $value['text'] = str_replace(array_keys($replace), $replace, $value['text']);
                    $email_link = $customer['emailaddress'].'####'.$value['subject'].'####'.$value['text'];
                    $key = $key == '-n-' ? '' : $key;

                    if ($group_id != $value['group_id']) {
                        $group_id = $value['group_id'];
                        
                        if ($opt_group) {
                            echo "</optgroup>\n";
                        }

                        echo "<optgroup label='".$mail_group[$group_id - 1]['name']."'>\n";
                        
                        $opt_group = true; 
                    }
                    
                    echo "<option value='".$email_link."'>".$value['name']."</option>\n";
                }
            }

            echo "</optgroup>\n";
            ?>
        </select>
    </p>
    <?php } ?>
</div>