<?php

$StartPath = urldecode($_POST['StartPath']);
$SelPath = urldecode($_POST['SelPath']);
$StartURL = urldecode($_POST['StartURL']);

require_once $StartPath.'/vendor/wisotel/configuration/Configuration.php';

$Purtel = json_decode($_POST['Purtel'], true);

$purtelAccountMaster = [
    'anschluss' => urldecode($_POST['PurtelLogin']),
];

$PurtelMaster = array_merge(
    $purtelAccountMaster,
    json_decode($_POST['PurtelMaster'], true)
);

$username = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$password = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');

$action = 'changecontract';

$status = 0;
$PurtelUrl = '';

// Purtelkunde ändern
$PurtelUrl = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
    urlencode($username),
    urlencode($password),
    urlencode($action)
);

foreach ($Purtel as $key => $value) {
    if ($value) {
        $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
    }
}

$curl = curl_init($PurtelUrl);

curl_setopt_array($curl, array(
    CURLOPT_URL => $PurtelUrl,
    CURLOPT_HEADER => false,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_SSL_VERIFYPEER => false,
));

$result = curl_exec($curl);

curl_close($curl);

$resultArray = str_getcsv($result, ';');

if ($resultArray[0] == '+OK') {
    $PurtelUrl = '';
    $PurtelUrl = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($username),
        urlencode($password),
        urlencode($action)
    );

    foreach ($PurtelMaster as $key => $value) {
        if ($value) {
            $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
        }
    }

    $curl = curl_init($PurtelUrl);

    curl_setopt_array($curl, array(
        CURLOPT_URL => $PurtelUrl,
        CURLOPT_HEADER => false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
    ));

    $result = curl_exec($curl);

    curl_close($curl);

    $resultArray1 = str_getcsv($result, ';');

    if ($resultArray1[0] == '+OK') {
        $status = 1;
    }
}

$response = array(
    'purtelLogin' => $purtelLogin,
    'purtelPassword' => $purtelPassword,
    'PurtelUrl' => $PurtelUrl,
    'status' => $status,
    'result' => utf8_encode($result),
);

print_r(json_encode($response));
