<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_ABROGATE');

$disable = 'readonly="readonly"';
$rw = false;   

if ($authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_ABROGATE')) {
    $disable = '';
    $rw = true;   
}

?>

<h3 class="CustAbrogate">Kündigung</h3>
<div class="CustAbrogate form">
    <span class='double'>
        <h4><?php echo \Wisotel\Configuration\Configuration::get('companyName'); ?> </h4>
    </span>
    <span class='double'>
        <h4>Purtel</h4>
    </span>

    <span>
        Anschluss aktiv seit<br />
        <?php echo $customer['connection_activation_date']; ?>
    </span>
    <span>
        Nächster Kündigungstermin<br />
        <?php echo $contract_next_cancle_date; ?>
    </span>
    <span>
        <label for="CustomerCancelPurtel14">Purteldaten kündigen</label><br />
        <select id="CustomerCancelPurtel14" name="CustomerCancelPurtel" <?php echo $disable; ?>>
            <?php setOption ($option_cancel_purtel, $customer, 'cancel_purtel'); ?>
        </select>
    </span>
    <span></span>

    <span>
        <?php
        $exceptionalReasonOfCancellationFieldName = 'Sonderkündigung';

        if (null !== $exReasonOfCancellationField = \Wisotel\Configuration\Configuration::get('exceptionalReasonOfCancellationFieldName')) {
            $exceptionalReasonOfCancellationFieldName = $exReasonOfCancellationField;
        }
        ?>
        <label for="CustomerWisocontractCancelExtra14"><?php echo $exceptionalReasonOfCancellationFieldName; ?></label><br />
        <select id="CustomerWisocontractCancelExtra14" name="CustomerWisocontractCancelExtra" <?php echo $disable; ?>>
            <?php setOption ($option_wisocontract_cancel_extra, $customer, 'wisocontract_cancel_extra'); ?>
        </select>
    </span>
    <span>
        <?php
        if ($contract_extra_cancel_date) {
            echo "Sonderkündigungstermin<br />";
            echo $contract_extra_cancel_date;
        }
        ?>
    </span>
    <span>
        <label for="CustomerCancelProduct14">Produkt gekündigt zum</label><br />
        <input id="CustomerCancelProduct14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'cancel_product'); ?>" maxlength="11" name="CustomerCancelProduct">
    </span>
    <span>
        <label for="CustomerCancelProduktFrom14">Produkt gekündigt von</label><br />
        <select id="CustomerCancelProduktFrom14" name="CustomerCancelProduktFrom" <?php echo $disable; ?>>
            <?php setOption ($option_tal_canceled_from, $customer, 'cancel_product_from'); ?>
        </select>
    </span>

    <span>
        <label for="CustomerWisocontractCancelInputDate14">Kündigung eingegangen am</label><br />
        <input id="CustomerWisocontractCancelInputDate14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wisocontract_cancel_input_date'); ?>" maxlength="11" name="CustomerWisocontractCancelInputDate">
    </span>
    <span>
        <label for="CustomerWisocontractCancelInputAck14">Eingang bestätigt am</label><br />
        <input id="CustomerWisocontractCancelInputAck14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wisocontract_cancel_input_ack'); ?>" maxlength="11" name="CustomerWisocontractCancelInputAck">
    </span>
    <span>
        <label for="CustomerCancelTelForDate14">TEL# kündigen zum</label><br />
        <input id="CustomerCancelTelForDate14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'cancel_tel_for_date'); ?>" maxlength="11" name="CustomerCancelTelForDate">
    </span>
    <span>
        <label for="CustomerCancelTelDate14">TEL# gekündigt am</label><br />
        <input id="CustomerCancelTelDate14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'cancel_tel_date'); ?>" maxlength="11" name="CustomerCancelTelDate">
    </span>

    <span>
        <label for="CustomerWisocontractCancelAck14">Kündigung bestätigt am</label><br />
        <input id="CustomerWisocontractCancelAck14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wisocontract_cancel_ack'); ?>" maxlength="11" name="CustomerWisocontractCancelAck">
    </span>
    <span>
        <label for="CustomerWisocontractCancelDate14"><?php echo \Wisotel\Configuration\Configuration::get('companyName'); ?> Vertrag kündigen zum</label><br />
        <input id="CustomerWisocontractCancelDate14" type="text" class="datepicker" <?php echo $abrogate_red; ?> <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wisocontract_cancel_date'); ?>" maxlength="11" name="CustomerWisocontractCancelDate">
    </span>
    <span>
        <label for="CustomerCancelTelFrom14">TEL# gekündigt von</label><br />
        <select id="CustomerCancelTelFrom14" name="CustomerCancelTelFrom" <?php echo $disable; ?>>
            <?php setOption ($option_tal_canceled_from, $customer, 'cancel_tel_from'); ?>
        </select>
    </span>
    <span></span>

    <span>
        <label for="CustomerWisocontractMove14">Umzug am</label><br />
        <input id="CustomerWisocontractMove14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wisocontract_move'); ?>" maxlength="11" name="CustomerWisocontractMove">
    </span>
    <span>
        <label for="CustomerMoveRegistrationAck14">Anmeldebestätigung erhalten am</label><br />
        <input id="CustomerMoveRegistrationAck14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'move_registration_ack'); ?>" maxlength="11" name="CustomerMoveRegistrationAck">
    </span>
    <span>
        <label for="CustomerCancelPurtelForDate14">SIP-Account kündigen zum</label><br />
        <input id="CustomerCancelPurtelForDate14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'cancel_purtel_for_date'); ?>" maxlength="11" name="CustomerCancelPurtelForDate">
    </span>
    <span>
        <label for="CustomerCancelPurtelDate14">SIP-Account gekündigt am</label><br />
        <input id="CustomerCancelPurtelDate14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'cancel_purtel_date'); ?>" maxlength="11" name="CustomerCancelPurtelDate">
    </span>

    <span class='one_half'>
        <label for="CustomerNewStreet14">neue Straße</label><br />
        <input id="CustomerNewStreet14" type="text" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'new_street'); ?>" name="CustomerNewStreet">
    </span>
    <span class='half'>
        <label for="CustomerNewStreetno14">neue Hausnr.</label><br />
        <input id="CustomerNewStreetno14" type="text" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'new_streetno'); ?>" name="CustomerNewStreetno">
    </span>
    <span>
        <label for="CustomerCancelPurtelFrom14">SIP gekündigt von</label><br />
        <select id="CustomerCancelPurtelFrom14" name="CustomerCancelPurtelFrom" <?php echo $disable; ?>>
            <?php setOption ($option_tal_canceled_from, $customer, 'cancel_purtel_from'); ?>
        </select>
    </span>
    <span></span>

    <span class='half'>
        <label for="CustomerNewZipcode14">neue PLZ</label><br />
        <input id="CustomerNewZipcode14" type="text" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'new_zipcode'); ?>" name="CustomerNewZipcode">
    </span>
    <span class='one_half'>
        <label for="CustomerNewCity14">neuer Ort</label><br />
        <input id="CustomerNewCity14" type="text" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'new_city'); ?>" name="CustomerNewCity">
    </span>
    <span class='double'>
        <h4><br />TAL</h4>
    </span>

    <span>
        <label for="CustomerWisocontractCancelAccountDate14">an Rechnungsstelle gemeldet</label><br />
        <input id="CustomerWisocontractCancelAccountDate14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wisocontract_cancel_account_date'); ?>" maxlength="11" name="CustomerWisocontractCancelAccountDate">
    </span>
    <span>
        <label for="CustomerWisocontractCancelAccountFinish14">Schlußrechnung erfolgt am</label><br />
        <input id="CustomerWisocontractCancelAccountFinish14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wisocontract_cancel_account_finish'); ?>" maxlength="11" name="CustomerWisocontractCancelAccountFinish">
    </span>
    <span>
        <label for="CustomerCancelTal14">Tal kündigen</label><br />
        <select id="CustomerCancelTal14" name="CustomerCancelTal" <?php echo $disable; ?>>
            <?php setOption ($option_cancel_tal, $customer, 'cancel_tal'); ?>
        </select>
    </span>
    <span>
        <label for="CustomerTalCanceledForDate14">Tal gekündigt zum</label><br />
        <input id="CustomerTalCanceledForDate14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'tal_canceled_for_date'); ?>" maxlength="11" name="CustomerTalCanceledForDate">
    </span>

    <span>
        <label for="CustomerWisocontractRehire14">Nachmieterübername</label><br />
        <select id="CustomerWisocontractRehire14" name="CustomerWisocontractRehire" <?php echo $disable; ?>>
            <?php setOption ($option_wisocontract_rehire, $customer, 'wisocontract_rehire'); ?>
        </select>
    </span>
    <span>
        <label for="CustomerWisocontractRehirePerson14">Nachmieterübername durch</label><br />
        <input id="CustomerWisocontractRehirePerson14" type="text" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wisocontract_rehire_person'); ?>" name="CustomerWisocontractRehirePerson">
    </span>
    <span>
        <label for="CustomerTalCancelAckDate14">Tal-Kdg. bestätigt zum</label><br />
        <input id="CustomerTalCancelAckDate14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'tal_cancel_ack_date'); ?>" maxlength="11" name="CustomerTalCancelAckDate">
    </span>
    <span>
        <label for="CustomerTalCancelDate14">Tal Kdg.datum</label><br />
        <input id="CustomerTalCancelDate14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'tal_cancel_date'); ?>" maxlength="11" name="CustomerTalCancelDate">
    </span>

    <span>
        <label for="CustomerGetHardwareDate14">Hardware zurück am</label><br />
        <input id="CustomerGetHardwareDate14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'get_hardware_date'); ?>" maxlength="11" name="CustomerGetHardwareDate">
    </span>
    <span>
        <label for="CustomerPayHardwareDate14">Hardware bezahlt am</label><br />
        <input id="CustomerPayHardwareDate14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'pay_hardware_date'); ?>" maxlength="11" name="CustomerPayHardwareDate">
    </span>
    <span>
        <label for="CustomerTechFreeForDate14">Tech. Daten freigeben zum</label><br />
        <input id="CustomerTechFreeForDate14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'tech_free_for_date'); ?>" maxlength="11" name="CustomerTechFreeForDate">
    </span>
    <span>
        <label for="CustomerTechFreeDate14">Tech. Daten freigegeben am</label><br />
        <input id="CustomerTechFreeDate14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'tech_free_date'); ?>" maxlength="11" name="CustomerTechFreeDate">
    </span>

    <span>
        <label for="CustomerWisocontractSwitchoff14"><?php echo \Wisotel\Configuration\Configuration::get('companyName'); ?> abgeschaltet am</label><br />
        <input id="CustomerWisocontractSwitchoff14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wisocontract_switchoff'); ?>" maxlength="11" name="CustomerWisocontractSwitchoff">
    </span>
    <span>
        <label for="CustomerWisocontractSwitchoffFinish14"><?php echo \Wisotel\Configuration\Configuration::get('companyName'); ?> abschalten am</label><br />
        <input id="CustomerWisocontractSwitchoffFinish14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wisocontract_switchoff_finish'); ?>" maxlength="11" name="CustomerWisocontractSwitchoffFinish">
    </span>
    <span>
        <label for="CustomerTalCanceledFrom14">Tal gekündigt von</label><br />
        <select id="CustomerTalCanceledFrom14" name="CustomerTalCanceledFrom" <?php echo $disable; ?>>
            <?php setOption ($option_tal_canceled_from, $customer, 'tal_canceled_from'); ?>
        </select>
    </span>
    <span></span>

    <span class="doublebox">
    <span>
        <label for="CustomerWisocontractCanceledDate14">Abgeschlossen am</label><br />
        <input id="CustomerWisocontractCanceledDate14" type="text" class="datepicker" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'wisocontract_canceled_date'); ?>" maxlength="11" name="CustomerWisocontractCanceledDate">
    </span>
    <span>
        <label for="CustomerWisocontractCanceledFrom14"><?php echo \Wisotel\Configuration\Configuration::get('companyName'); ?> gekündigt von</label><br />
        <select id="CustomerWisocontractCanceledFrom14" name="CustomerWisocontractCanceledFrom" <?php echo $disable; ?>>
            <?php setOption ($option_tal_canceled_from, $customer, 'wisocontract_canceled_from'); ?>
        </select>
    </span>
    <span class='double'></span>
    <span class='double'></span>
    </span>

    <span class="double">
        <label for="CustomerCancelComment14">Kommentar (Kündigung)</label><br />
        <textarea id="CustomerCancelComment14" name="CustomerCancelComment" <?php echo $disable; ?>><?php setInputValue ($customer, 'cancel_comment'); ?></textarea>
    </span>

    <?php if ($rw) { ?>
    <h4><br />E-Mail an Kunden senden</h4>
    <p>
        <label for="EMailTemplate">E- Mail Template wählen</label><br />
        <select id="EMailTemplate" name="EMailTemplate">
            <option value='' selected>- bitte auswählen -</option>
            <?php
            $group_id = 0;
            $opt_group = false;
            
            foreach ($mail_temp as $key => $value) {
                if ($value['group_id'] == 8) {
                    $replace = [
                        '###ANREDE###' => htmlspecialchars($customer['title'], ENT_QUOTES),
                        '###NAME###' => htmlspecialchars($customer['lastname'], ENT_QUOTES),
                    ];

                    if ($customer['title'] == 'Herr') {
                        $replace['geehrte'] = 'geehrter';
                    }

                    $value['text'] = str_replace(array_keys($replace), $replace, $value['text']);
                    $email_link = $customer['emailaddress'].'####'.$value['subject'].'####'.$value['text'];
                    $key = $key == '-n-' ? '' : $key;
                
                    if ($group_id != $value['group_id']) {
                        $group_id = $value['group_id'];
                        
                        if ($opt_group) {
                            echo "</optgroup>\n";
                        }
                        
                        echo "<optgroup label='".$mail_group[$group_id - 1]['name']."'>\n";
                
                        $opt_group = true; 
                    }
                
                    echo "<option value='".$email_link."'>".$value['name']."</option>\n";
                }
            }
            
            echo "</optgroup>\n";
            ?>
        </select>
    </p>
    <?php } ?>

</div>
