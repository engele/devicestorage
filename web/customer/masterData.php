<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_MASTER_DATA');

$disable = 'readonly="readonly"';
$rw = false;   

if ($authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_MASTER_DATA')) {
    $disable = '';   
    $rw = true;   
}

?>

<h3 class="CustMasterData">Kundendaten (Stammdaten)</h3>
<div class="CustMasterData form">
    <span>
        <label for="CustomerClientid02"><?php echo \Wisotel\Configuration\Configuration::get('companyName'); ?> Nr.</label><br />
        <input id="CustomerClientid02" type="text" name="CustomerClientid" readonly="readonly" value="<?php setInputValue ($customer, 'clientid'); ?>">
        <input id="CustomerId02" type="hidden" name="CustomerId" value="<?php setInputValue ($customer, 'id'); ?>">
    </span>

    <span>
        <label for="CustomerClienttype02">Kundentyp *</label><br />
        <select id="CustomerClienttype02" name="CustomerClienttype" <?php echo $disable; ?>>
            <?php setOption ($option_clienttype, $customer, 'clienttype'); ?>
        </select>
    </span>

    <?php
    $externalCustomerIdFieldName = 'Externe Kundenkennung';

    if (null !== $exCustField = \Wisotel\Configuration\Configuration::get('externalCustomerIdFieldName')) {
        $externalCustomerIdFieldName = $exCustField;
    }
    ?>

    <span>
        <label for="CustomerCustomerId02"><?php echo $externalCustomerIdFieldName; ?></label><br />
        <input id="CustomerCustomerId02" type="text" maxlength="64" name="CustomerCustomerId" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'customer_id'); ?>">
    </span>

    <span>
        <label for="CustomerVoucher02">Aktion</label><br />
        <input id="CustomerVoucher02" type="text" maxlength="64" name="CustomerVoucher" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'voucher'); ?>">
    </span>

    <span class="four"><hr></span>
    <span>
        <label for="CustomerTitle02">Anrede *</label><br />
        <select id="CustomerTitle02" name="CustomerTitle" <?php echo $disable; ?>>
            <?php setOption ($option_title, $customer, 'title'); ?>
        </select>
    </span>
    <span></span>

    <span class="half">
        <label for="CustomerPhoneareacode02">Vorwahl *</label><br />
        <input id="CustomerPhoneareacode02" type="text" maxlength="10" name="CustomerPhoneareacode" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'phoneareacode'); ?>">
    </span>

    <span class="half">
        <label for="CustomerPhonenumber02">Durchwahl *</label><br />
        <input id="CustomerPhonenumber02" type="text" maxlength="24" name="CustomerPhonenumber" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'phonenumber'); ?>">
    </span>

    <span class="half">
        <label for="CustomerFax02">Fax</label><br />
        <input id="CustomerFax02" type="text" maxlength="64" name="CustomerFax" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'fax'); ?>">
    </span>

    <span class="half"></span>
    <span>
        <label for="CustomerFirstname02">Vorname *</label><br />
        <input id="CustomerFirstname02" type="text" maxlength="64" name="CustomerFirstname" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'firstname'); ?>">
    </span>

    <span>
        <label for="CustomerLastname02">Nachname *</label><br />
        <input id="CustomerLastname02" type="text" maxlength="64" name="CustomerLastname" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'lastname'); ?>">
    </span>

    <span class="one_half">
        <label for="CustomerMobilephone02">Mobiltelefon</label><br />
        <input id="CustomerMobilephone02" type="text" maxlength="64" name="CustomerMobilephone" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'mobilephone'); ?>">
    </span>

    <span class="half"></span>
    <span class="double">
        <label for="CustomerCompany02">Firma</label><br />
        <input id="CustomerCompany02" type="text" maxlength="64" name="CustomerCompany" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'company'); ?>">
    </span>

    <span class="one_half">
        <label for="CustomerEmailaddress02">E-Mail Adresse *</label><br />
        <input id="CustomerEmailaddress02" type="text" maxlength="128" name="CustomerEmailaddress" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'emailaddress'); ?>">
    </span>

    <span class="half"></span>
    <span class="one_half">
        <label for="CustomerStreet02">Straße *</label><br />
        <input id="CustomerStreet02" type="text" maxlength="64" name="CustomerStreet" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'street'); ?>">
    </span>

    <span class="half">
        <label for="CustomerStreetno02">Hausnummer *</label><br />
        <input id="CustomerStreetno02" type="text" maxlength="8" name="CustomerStreetno" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'streetno'); ?>">
    </span>

    <span>
        <label for="CustomerBirthday02">Geburtstag</label><br />
        <input id="CustomerBirthday02" type="text" class="datepicker" name="CustomerBirthday" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'birthday'); ?>">
    </span>
    <span></span>

    <span class="half">
        <label for="CustomerZipcode02">PLZ *</label><br />
        <input id="CustomerZipcode02" type="text" maxlength="8" name="CustomerZipcode" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'zipcode'); ?>">
    </span>

    <span class="one_half">
        <label for="CustomerCity02">Stadt *</label><br />
        <input id="CustomerCity02" type="text" maxlength="64" name="CustomerCity" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'city'); ?>">
    </span>

    <span class="double">
        <br /> <?php if ($rw) { ?><button onclick="copyConnectionToMasterAddress()" type="button">Adresse aus Anschluss einfügen</button> <?php } ?>
    </span>

    <span class="four"><hr></span>
    <span class="four">
        <label for="CustomerComment02">Kommentar</label><br />
        <textarea id="CustomerComment02" name="CustomerComment" <?php echo $disable; ?>><?php setInputValue ($customer, 'comment'); ?></textarea>
    </span>

    <span>
        <label for="CustomerCustomerId02">Geschäfts-Partner-Nr.</label><br />
        <input id="CustomerCustomerId02" type="text" maxlength="64" name="CustomerCustomerId" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'customer_id'); ?>">
    </span>
    
    <span>
        <label for="CustomerExternalID202">Vertrags-Konto-Nr.</label><br />
        <input id="CustomerExternalID202" type="text" maxlength="64" name="CustomerExternalID2" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'external_id_2'); ?>">
    </span>
</div>
