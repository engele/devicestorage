<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_TAL');

$disable = 'readonly="readonly"';
$rw = false;   

if ($authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_TAL')) {
    $disable = '';   
    $rw = true;   
}

?>
<h3 class="CustTal">TAL</h3>
<div class="CustTal form">
  <span class="double">
    <label for="CustomerClientid09"><?php echo \Wisotel\Configuration\Configuration::get('companyName'); ?> Nr.</label><br />
    <input type="text" id="CustomerClientid09" value="<?php setInputValue ($customer, 'clientid'); ?>" readonly="readonly" name="CustomerClientid">
  </span>
  <span class="double"><br /><?php if($rw) {?><button onclick="fillTaldata()" type="button">Glasfaseranschluss</button><?php } ?></span>

  <span class="double">
    <label for="CustomerTalOrder09">Kunde benötigt</label><br />
    <select id="CustomerTalOrder09" name="CustomerTalOrder" <?php echo $disable; ?>>
      <?php setOption ($option_tal_order, $customer, 'tal_order'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerTalProduct09">TAL-Produkt</label><br />
    <select id="CustomerTalProduct09" name="CustomerTalProduct" <?php echo $disable; ?>>
      <?php setOption ($option_tal_product, $customer, 'tal_product'); ?>
    </select>
  </span>
  <span>
    <label class="" for="CustomerTalOrderDate09">TAL-Bestelldatum</label><br />
    <input type="text" id="CustomerTalOrderDate09" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'tal_order_date'); ?>" class="datepicker" name="CustomerTalOrderDate">
  </span>
  
  <span>
    <label for="CustomerNbCanceledDate09">Kdg.bestät. DSL zum</label><br />
    <input type="text" id="CustomerNbCanceledDate09" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'nb_canceled_date'); ?>" class="datepicker" name="CustomerNbCanceledDate">
  </span>
  <span>
    <label for="CustomerNbCanceledDatePhone09">Kdg.bestät. Tel zum</label><br />
    <input type="text" id="CustomerNbCanceledDatePhone09" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'nb_canceled_date_phone'); ?>" class="datepicker" name="CustomerNbCanceledDatePhone">
  </span>
  <span>
    <label for="CustomerTalOrderdFrom09">TAL bestellt von</label><br />
    <select id="CustomerTalOrderdFrom09" name="CustomerTalOrderdFrom" <?php echo $disable; ?>>
      <?php setOption ($option_tal_orderd_from, $customer, 'tal_orderd_from'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerTalOrderedForDate09">TAL bestellt zum</label><br />
    <input type="text" id="CustomerTalOrderedForDate09" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'tal_ordered_for_date'); ?>" class="datepicker" name="CustomerTalOrderedForDate">
  </span>

  <span class="double"></span>
  <span>
    <label class="" for="CustomerTalOrderAckDate09">TAL bestätigt zum</label><br />
    <input type="text" id="CustomerTalOrderAckDate09" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'tal_order_ack_date'); ?>" class="datepicker" name="CustomerTalOrderAckDate">
  </span>
  <span>
    <label for="CustomerTvsTo09">TVS erledigt</label><br />
    <select id="CustomerTvsTo09" name="CustomerTvsTo" <?php echo $disable; ?>>
      <?php setOption ($option_tvs_to, $customer, 'tvs_to'); ?>
    </select>
  </span>

  <span class="double"></span>
  <span>
    <label for="CustomerTvsDate09">Terminverschiebung zum</label><br />
    <input type="text" id="CustomerTvsDate09" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'tvs_date'); ?>" class="datepicker" name="CustomerTvsDate">
  </span>
  <span>
    <label for="CustomerTvs09">Terminverschiebung am</label><br />
    <input type="text" id="CustomerTvs09" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'tvs'); ?>" class="datepicker" name="CustomerTvs">
  </span>

  <span class="double"></span>
  <span>
    <label for="CustomerTvsFrom09">TVS versendet von</label>
    <select id="CustomerTvsFrom09" name="CustomerTvsFrom" <?php echo $disable; ?>>
      <?php setOption ($option_tvs_from, $customer, 'tvs_from'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerTalDtagAssignmentNo09">DTAG-Vertragsnummer</label><br />
    <input type="text" id="CustomerTalDtagAssignmentNo09" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'tal_dtag_assignment_no'); ?>" name="CustomerTalDtagAssignmentNo">
  </span>

  <span class="double"></span>
  <span>
    <label for="CustomerTalOrderWork09">Arbeiten beim Kunden</label><br />
    <select id="CustomerTalOrderWork09" name="CustomerTalOrderWork" <?php echo $disable; ?>>
      <?php setOption ($option_tal_order_work, $customer, 'tal_order_work'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerDtagLine09">Leitungsbezeichnung (LBZ)</label><br />
    <input type="text" id="CustomerDtagLine09" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'dtag_line'); ?>" name="CustomerDtagLine">
  </span>

  <span class="double"></span>
  <span class="double">
    <label for="CustomerCustomerConnectInfoDate09">Kunde über Anschlussdatum informiert am</label><br />
    <input type="text" id="CustomerCustomerConnectInfoDate09" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'customer_connect_info_date'); ?>" class="datepicker" name="CustomerCustomerConnectInfoDate">
  </span>
    
  <span class="double"></span>
  <span>
    <label for="CustomerCustomerConnectInfoFrom09">informiert von</label><br />
    <select id="CustomerCustomerConnectInfoFrom09" name="CustomerCustomerConnectInfoFrom" <?php echo $disable; ?>>
      <?php setOption ($option_customer_connect_info_from, $customer, 'customer_connect_info_from'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerCustomerConnectInfoHow09">per</label><br />
    <select id="CustomerCustomerConnectInfoHow09" name="CustomerCustomerConnectInfoHow" <?php echo $disable; ?>>
      <?php setOption ($option_customer_connect_info_how, $customer, 'customer_connect_info_how'); ?>
    </select>
  </span>

  <span class="double"></span>
  <span>
    <label for="CustomerCancelTal09">Tal kündigen</label><br />
    <select id="CustomerCancelTal09" name="CustomerCancelTal" <?php echo $disable; ?>>
      <?php setOption ($option_cancel_tal, $customer, 'cancel_tal'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerTalCancelDate09">Tal Kdg.datum</label><br />
    <input type="text" id="CustomerTalCancelDate09" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'tal_cancel_date'); ?>" class="datepicker" name="CustomerTalCancelDate">
  </span>

  <span class="double"></span>
  <span>
    <label for="CustomerTalCanceledFrom09">Tal gekündigt von</label><br />
    <select id="CustomerTalCanceledFrom09" name="CustomerTalCanceledFrom" <?php echo $disable; ?>>
      <?php setOption ($option_tal_canceled_from, $customer, 'tal_canceled_from'); ?>
    </select>
  </span>
  <span>
    <label for="CustomerTalCanceledForDate09">Tal gekündigt zum</label><br />
    <input type="text" id="CustomerTalCanceledForDate09" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'tal_canceled_for_date'); ?>" class="datepicker" name="CustomerTalCanceledForDate">
  </span>

  <span class="double"></span>
  <span class="double">
    <label for="CustomerTalCancelAckDate09">Tal-Kdg. bestätigt zum</label><br />
    <input type="text" id="CustomerTalCancelAckDate09" <?php echo $disable; ?> value="<?php setInputValue ($customer, 'tal_cancel_ack_date'); ?>" class="datepicker" name="CustomerTalCancelAckDate">
  </span>

  <span class="doublebox">
    <span class="double"></span>
    <span class="double"></span>
  </span>
  <span class="double">
    <label for="CustomerStatiOldcontractComment09">TAL Rückmeldung</label><br />
    <textarea id="CustomerStatiOldcontractComment09" class="small" name="CustomerStatiOldcontractComment" <?php echo $disable; ?>><?php setInputValue ($customer, 'stati_oldcontract_comment'); ?></textarea>
  </span>

</div>
