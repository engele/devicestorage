<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_CONTRACT');

$disable = 'readonly="readonly"';
$rw = false;   

if ($authorizationChecker->isGranted('ROLE_EDIT_CUSTOMER_CONTRACT')) {
    $disable = '';   
    $rw = true;   
}
?>

<h3 class="CustContractOptions">Produkt Optionen</h3>
<div class="CustContractOptions form">
<?php
echo "<span><strong>Netto Preis</strong></span>";
echo "<span><strong>Brutto Preis</strong></span>";
echo "<span><strong>Aktivierungsdatum</strong></span>";
echo "<span><strong>Deaktivierungsdatum</strong></span>";
echo "<hr>";

foreach ($cust_options as $key => $value) {
    echo "<span><strong>$key</strong></span>";
    echo "<span class='double'><strong>".$value['option_bezeichnung']."</strong></span>";
    echo "<span><label><strong>Löschen</strong> <input type='checkbox' class='checkbox' id='CustomerOptionDelete16_$key' name='CustomerOptionDelete_$key' value='delete'></label></span>";

    $value['price_net'] = $value['price_net'] ? sprintf('%.2f',$value['price_net']) : '';
    $value['price_gross'] = $value['price_gross'] ? sprintf('%.2f',$value['price_gross']) : '';
    $value['activation_date'] = $value['activation_date'] ? date('d.m.Y', strtotime($value['activation_date'])) : ''; 
    $value['deactivation_date'] = $value['deactivation_date'] ? date('d.m.Y', strtotime($value['deactivation_date'])) : ''; 

    echo "<span><input id='CustomerOptionPriceNet16_$key' type='text' class='CustomerOptionPriceNet' value='".$value['price_net']."' name='CustomerOptionPriceNet_$key'></span>";
    echo "<span><input id='CustomerOptionPriceGross16_$key' type='text' class='CustomerOptionPriceGross' value='".$value['price_gross']."' name='CustomerOptionPriceGross_$key'></span>";
    echo "<span><input id='CustomerOptionActivationDate16_$key' type='text' class='datepicker' value='".$value['activation_date']."' name='CustomerOptionActivationDate_$key'></span>";
    echo "<span><input id='CustomerOptionDeactivationDate16_$key' type='text' class='datepicker' value='".$value['deactivation_date']."' name='CustomerOptionDeactivationDate_$key'></span>";

    echo "<hr>";
}
?>
</div>
<h3 class="CustContractOptionsNew">Produkt Optionen hinzufügen</h3>
<div class="CustContractOptionsNew form">
  <select id="CustomerOptions" class="bsmSelect" name="CustomerOptions[]" multiple="multiple">
    <?php
      foreach ($options as $key => $value) {
        if (!isset ($cust_options[$key])) {
          echo "<option value='$key'>$key -- ".$value['option_bezeichnung']." (Preis: ".$value['Preis_Netto']." / ".$value['Preis_Brutto'].")</option>\n";
        }
      }
    ?>
  </select>
</div>
