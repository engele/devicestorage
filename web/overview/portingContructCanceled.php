<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_OVERVIEW_CONTRACT');

  $headline = array (
    'Kundennr.',
    'Nachname',
    'Mail-Adr.',
    'Stadt',
    \Wisotel\Configuration\Configuration::get('companyName').'<br />Vertrag gek.',
    'TAL<br />kündigen',
    'TAL<br />best. zum',
    'Purtel Login<br />Passwort',
  );
  $head_csv = serialize($headline);
  $head_csv = urlencode($head_csv);
  $_SESSION['head_contructcanceled'] = $head_csv;
  if (isset($portingContructCanceled)) {
    $porting_csv  = serialize($portingContructCanceled);
    $porting_csv  = urlencode($porting_csv);
    $_SESSION['data_contructcanceled'] = $porting_csv;
  }
?>

<div class="Ovr OvrPortingContructCanceled box">
  <h3>gekündigte Verträge</h3>
 
  <p><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button></p> 
  <p><a href='<?php echo $StartURL; ?>/overview/overview_csv.php?csv=contructcanceled' target='_blank'>CSV Export</a></p>
  <table class="Ovr">
  <thead><tr>
    <th class='n31'><?php echo $headline[0]; ?></th>
    <th class='n33'><?php echo $headline[1]; ?></th>
    <th class='n32'><?php echo $headline[2]; ?></th>
    <th class='n34'><?php echo $headline[3]; ?></th>
    <th class='n35'><?php echo $headline[4]; ?></th>
    <th class='n36'><?php echo $headline[5]; ?></th>
    <th class='n37'><?php echo $headline[6]; ?></th>
    <th class='n38'><?php echo $headline[7]; ?></th>
  </tr></thead>
  <tbody>
  <?php
    $row = 'odd';
    if (isset ($portingContructCanceled)) {
      foreach ($portingContructCanceled as $index) {
        if (key_exists ('lastname', $index))                    $lastname                   = $index['lastname'];                   else $lastname                    = '';
        if (key_exists ('emailaddress', $index))                $emailaddress               = $index['emailaddress'];               else $emailaddress                = '';
        if (key_exists ('district', $index))                    $district                   = $index['district'];                   else $district                    = '';
        if (key_exists ('wisocontract_canceled_date', $index))  $wisocontract_canceled_date = $index['wisocontract_canceled_date']; else $wisocontract_canceled_date  = '';
        if (key_exists ('cancel_tal', $index))                  $cancel_tal                 = $index['cancel_tal'];                 else $cancel_tal                  = '';
        if (key_exists ('tal_cancel_ack_date', $index))         $tal_cancel_ack_date        = $index['tal_cancel_ack_date'];        else $tal_cancel_ack_date         = '';
        if (key_exists ('purtel_login', $index))                $purtel_login               = $index['purtel_login'];               else $purtel_login                = '';

        echo "<tr class='$row'>";
        echo "<td><a href='".$StartURL."/index.php?menu=customer&func=vertrag&id=".$index['id']."'>".$index['clientid']."</a></td>";
        echo "<td>".$lastname."</td>";
        echo "<td>".$emailaddress."</td>";
        echo "<td>".$district."</td>";
        echo "<td>".$wisocontract_canceled_date."</td>";
        echo "<td>".$cancel_tal."</td>";
        echo "<td>".$tal_cancel_ack_date."</td>";
        echo "<td>".$purtel_login."</td>";
        echo "</tr>";
        if ($row == 'odd') $row = 'even'; else $row = 'odd';
      }
    }
  ?>
  </tbody>
  </table>
  <br /><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button> 
</div>
