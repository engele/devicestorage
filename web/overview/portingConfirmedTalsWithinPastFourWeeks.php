<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_OVERVIEW_TAL');

?>
<div class="Ovr OvrPortingConfirmedTalsWithinPastFourWeeks box">
    <h3>Bestätigte TALs der letzten 4 Wochen</h3>
    
    <p>
        <button type="button" onclick="ovrShow(['OvrList'])">
            <img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px"> Zurück
        </button>
    </p> 
    <table class="Ovr">
        <thead>
            <tr>
                <th class='n11'><?php echo $headline[0]; ?></th>
                <th class='n12'><?php echo $headline[1]; ?></th>
                <th class='n13'><?php echo $headline[2]; ?></th>
                <th class='n14'><?php echo $headline[3]; ?></th>
                <th class='n15 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[4]; ?></th>
                <th class='n06'><?php echo $headline[5]; ?></th>
                <th class='n16 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[6]; ?></th>
                <th class='n06'><?php echo $headline[7]; ?></th>
                <th class='n16 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[8]; ?></th>
                <th class='n16 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[9]; ?></th>
                <th class='n06'><?php echo $headline[10]; ?></th>
                <th class='n16 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[11]; ?></th>
                <th class='n16 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[12]; ?></th>
                <th class='n16 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[13]; ?></th>
                <th class='n16 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[14]; ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $row = 'odd';
            
            if (isset($confirmedTalsWithinPastFourWeeks)) {
                foreach ($confirmedTalsWithinPastFourWeeks as $index) {
                    $dslam_arranged_date = '';
                    $pppoe_config_date = '';
                    $patch_date = '';
                    $terminal_ready = '';

                    if (!empty($index['dslam_arranged_date'])) $dslam_arranged_date = 'X';
                    if (!empty($index['pppoe_config_date'])) $pppoe_config_date = 'X';
                    if (!empty($index['patch_date'])) $patch_date = 'X';
                    if (!empty($index['terminal_ready'])) $terminal_ready = 'X';

                    echo "<tr class='$row'>";
                    echo "<td><a href='".$StartURL."/index.php?menu=customer&func=vertrag&id=".$index['id']."' target='_blank'>".$index['clientid']."</a></td>";
                    echo "<td>".$index['firstname']."</td>";
                    echo "<td>".$index['lastname']."</td>";
                    echo "<td>".$index['district']."</td>";
                    echo "<td>".$index['stati_port_confirm_date']."</td>";
                    echo "<td>".$index['phone_number_added']."</td>";
                    echo "<td>".$index['tal_order_ack_date']."</td>";
                    echo "<td>".$index['terminal_type']."</td>";
                    echo "<td>".$terminal_ready."</td>";
                    echo "<td>".$index['terminal_sended_date']."</td>";
                    echo "<td>".$index['service_technician']."</td>";
                    echo "<td>".$index['connection_activation_date']."</td>";
                    echo "<td>".$dslam_arranged_date."</td>";
                    echo "<td>".$pppoe_config_date."</td>";
                    echo "<td>".$patch_date."</td>";

                    echo "</tr>";
                    if ($row == 'odd') $row = 'even'; else $row = 'odd';
                }
            }
            ?>
        </tbody>
    </table>
    
    <br />
    <button type="button" onclick="ovrShow(['OvrList'])">
        <img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px"> Zurück
    </button> 
</div>