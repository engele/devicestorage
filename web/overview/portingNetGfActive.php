<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_OVERVIEW_TECHNICAL');

  $headline = array (
    'Kundennr.',
    'Vorname',
    'Nachname',
    'Stadt',
    'Anschluss<br />aktiv seit',
    'Anschalt Wunschtermin',
  );
  $head_csv = serialize($headline);
  $head_csv = urlencode($head_csv);
  $_SESSION['head_netgfactive'] = $head_csv;
  if (isset($portingNetGfActive)) {
    $porting_csv  = serialize($portingNetGfActive);
    $porting_csv  = urlencode($porting_csv);
    $_SESSION['data_netgfactive'] = $porting_csv;
  }
?>


<div class="Ovr OvrPortingNetGfActive box">
  <h3>Aktivschaltungen Glasfaser</h3>

  <p><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button></p> 
  <p><a href='<?php echo $StartURL; ?>/overview/overview_csv.php?csv=netgfactive' target='_blank'>CSV Export</a></p>
  <table class="Ovr">
  <thead><tr>
    <th class='n11'><?php echo $headline[0]; ?></th>
    <th class='n12'><?php echo $headline[1]; ?></th>
    <th class='n13'><?php echo $headline[2]; ?></th>
    <th class='n14'><?php echo $headline[3]; ?></th>
    <th class='n15'><?php echo $headline[4]; ?></th>
    <th class='n16 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[5]; ?></th>
  </tr></thead>
  <tbody>
  <?php
    $row = 'odd';
    if (isset ($portingNetGfActive)) {
      foreach ($portingNetGfActive as $index) {
        if (key_exists ('firstname', $index))                   $firstname                  = $index['firstname'];                  else $firstname                   = '';
        if (key_exists ('lastname', $index))                    $lastname                   = $index['lastname'];                   else $lastname                    = '';
        if (key_exists ('district', $index))                    $district                   = $index['district'];                   else $district                    = '';
        if (key_exists ('connection_activation_date', $index))  $connection_activation_date = $index['connection_activation_date']; else $connection_activation_date  = '';

        echo "<tr class='$row'>";
        echo "<td><a href='".$StartURL."/index.php?menu=customer&func=vertrag&id=".$index['id']."' target='_blank'>".$index['clientid']."</a></td>";
        echo "<td>".$firstname."</td>";
        echo "<td>".$lastname."</td>";
        echo "<td>".$district."</td>";
        echo "<td>".$connection_activation_date."</td>";
        echo "<td>".$index['connection_wish_date']."</td>";
        echo "</tr>";
        if ($row == 'odd') $row = 'even'; else $row = 'odd';
      }
    }
  ?>
  </tbody>
  </table>
  <br /><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button> 
</div>
