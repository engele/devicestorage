<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_OVERVIEW_PORTING');

$headline = array (
    'Kundennr.',
    'Vorname',
    'Nachname',
    'Stadt',
    'Kdg.termin<br />Telefon',
    'Frist',
    'Art'
);

$head_csv = serialize($headline);
$head_csv = urlencode($head_csv);

$_SESSION['head_porting'] = $head_csv;

if (isset($porting)) {
    $porting_csv  = serialize($porting);
    $porting_csv  = urlencode($porting_csv);
    $_SESSION['data_porting'] = $porting_csv;
}
?>

<div class="Ovr OvrPorting box">
  <h3>zu portierende Kunden</h3>

  <p><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px"> Zurück</button></p>
  <p><a href='<?php echo $StartURL; ?>/overview/overview_csv.php?csv=porting' target='_blank'>CSV Export</a></p>
  <table class="Ovr">
  <thead><tr>
    <th class='n01'><?php echo $headline[0]; ?></th>
    <th class='n02'><?php echo $headline[1]; ?></th>
    <th class='n03'><?php echo $headline[2]; ?></th>
    <th class='n04'><?php echo $headline[3]; ?></th>
    <th class='n05 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[4]; ?></th>
    <th class='n06'><?php echo $headline[5]; ?></th>
    <th class='n07'><?php echo $headline[6]; ?></th>
  </tr></thead>
  <tbody>
  <?php
    $row = 'odd';
    if (isset ($porting)) {
      foreach ($porting as $index) {
        if (key_exists ('firstname', $index))                 $firstname                = $index['firstname'];                else $firstname                 = '';
        if (key_exists ('lastname', $index))                  $lastname                 = $index['lastname'];                 else $lastname                  = '';
        if (key_exists ('district', $index))                  $district                 = $index['district'];                 else $district                  = '';
        if (key_exists ('exp_date_phone', $index))            $exp_date_phone           = $index['exp_date_phone'];           else $exp_date_phone            = '';
        if (key_exists ('notice_period_phone_int', $index))   $notice_period_phone_int  = $index['notice_period_phone_int'];  else $notice_period_phone_int   = '';
        if (key_exists ('notice_period_phone_type', $index))  $notice_period_phone_type = $index['notice_period_phone_type']; else $notice_period_phone_type  = '';

        echo "<tr class='$row'>";
        echo "<td><a href='".$StartURL."/index.php?menu=customer&func=vertrag&id=".$index['id']."'>".$index['clientid']."</a></td>";
        echo "<td>".$firstname."</td>";
        echo "<td>".$lastname."</td>";
        echo "<td>".$district."</td>";
        echo "<td>".$exp_date_phone."</td>";
        echo "<td>".$notice_period_phone_int."</td>";
        echo "<td>".$notice_period_phone_type."</td>";
        echo "</tr>";
        if ($row == 'odd') $row = 'even'; else $row = 'odd';
      }
    }
  ?>
  </tbody>
  </table>
  <br /><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button> 

</div>
