<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_OVERVIEW_CONTRACT');

  $headline = array (
    'Kundennr.',
    'Vorname',
    'Nachname',
    'Stadt'
  );
  $head_csv = serialize($headline);
  $head_csv = urlencode($head_csv);
  $_SESSION['head_contructnosupply'] = $head_csv;
  if (isset($portingContructNoSupply)) {
    $porting_csv  = serialize($portingContructNoSupply);
    $porting_csv  = urlencode($porting_csv);
    $_SESSION['data_contructnosupply'] = $porting_csv;
  }
?>

<div class="Ovr OvrPortingContructNoSupply box">
  <h3>Kunden, nicht versorgbar</h3>
 
  <p><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button></p> 
  <p><a href='<?php echo $StartURL; ?>/overview/overview_csv.php?csv=contructnosupply' target='_blank'>CSV Export</a></p>
  <table class="Ovr">
  <thead><tr>
    <th class='n11'><?php echo $headline[0]; ?></th>
    <th class='n13'><?php echo $headline[1]; ?></th>
    <th class='n12'><?php echo $headline[2]; ?></th>
    <th class='n14'><?php echo $headline[3]; ?></th>
  </tr></thead>
  <tbody>
  <?php
    $row = 'odd';
    if (isset ($portingContructNoSupply)) {
      foreach ($portingContructNoSupply as $index) {
        if (key_exists ('firstname', $index))         $firstname          = $index['firstname'];          else $firstname         = '';
        if (key_exists ('lastname', $index))          $lastname           = $index['lastname'];           else $lastname          = '';
        if (key_exists ('district', $index))          $district           = $index['district'];           else $district          = '';

        echo "<tr class='$row'>";
        echo "<td><a href='".$StartURL."/index.php?menu=customer&func=vertrag&id=".$index['id']."'>".$index['clientid']."</a></td>";
        echo "<td>".$firstname."</td>";
        echo "<td>".$lastname."</td>";
        echo "<td>".$district."</td>";
        echo "</tr>";
        if ($row == 'odd') $row = 'even'; else $row = 'odd';
      }
    }
  ?>
  </tbody>
  </table>
  <br /><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button>
</div>
