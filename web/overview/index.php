<?php
require_once ($StartPath.'/_conf/database.inc');
require_once ($StartPath.'/'.$SelMenu.'/_conf/database.inc');

$oneDay      = time() - (24*60*60);
$threemonths = time() - (90*24*60*60);
  
// Anbieterwechsel manuell
// Portierungen die in den nächsten Tagen zu bearbeiten sind
$db_porting_days = $db->query($sql['porting_days']);
$porting_days_count = 0;
while ($db_porting_days_row = $db_porting_days->fetch_assoc()) {
    #Kündigungsfrist in Tage berechnen
    $fristzahl  = trim($db_porting_days_row["notice_period_phone_int"]);
    $fristart   = $db_porting_days_row["notice_period_phone_type"];
    if($fristart == 'months') {
        $fristzahl = $fristzahl * 30;
    } else if($fristart == 'weeks') {
        $fristzahl = $fristzahl * 7;
    } else if ($fristart == 'weeks till months end') {
        $fristzahl = $fristzahl * 7 + 14;
    }
  
    #Frist und 24 Wochen von Kdgdatum abziehen
    //$newDate = strtotime('-' . ($fristzahl + 168) . ' days', strtotime($db_porting_days_row['exp_date_phone']));
    $newDate = 0;
 
    // Kunden zählen  
    if($newDate <= $oneDay) {
        $porting[$porting_days_count]['id']                      = $db_porting_days_row['id'];
        $porting[$porting_days_count]['clientid']                = $db_porting_days_row['clientid'];
        $porting[$porting_days_count]['firstname']               = $db_porting_days_row['firstname'];
        $porting[$porting_days_count]['lastname']                = $db_porting_days_row['lastname'];
        $porting[$porting_days_count]['district']                = $db_porting_days_row['district'];
        $porting[$porting_days_count]['exp_date_phone']          = $db_porting_days_row['exp_date_phone'];
        $porting[$porting_days_count]['exp_done_phone']          = $db_porting_days_row['exp_done_phone'];
        $porting[$porting_days_count]['notice_period_phone_int'] = $db_porting_days_row['notice_period_phone_int'];
        switch ($db_porting_days_row['notice_period_phone_type']) {
            case 'months':
                $porting[$porting_days_count]['notice_period_phone_type'] = 'Monat(e)';
                break;
            case 'weeks':
                $porting[$porting_days_count]['notice_period_phone_type'] = 'Woche(n)';
                break;
            case 'days':
                $porting[$porting_days_count]['notice_period_phone_type'] = 'Tag(e)';
                break;
            case 'weeks till months end':
                $porting[$porting_days_count]['notice_period_phone_type'] = 'Woche(n) bis zum Monatsende';
                break;
        }
        $porting_days_count++;
    }
}
$db_porting_days->free();

// Portierung mehr als eine Woche beim Kunden
$db_porting_weeks = $db->query( $sql['porting_weeks']);
$porting_weeks_count = 0;
while ($db_porting_weeks_row = $db_porting_weeks->fetch_assoc()) {
    # Port versendet String in englisches Datum wandeln
    $newDate = strtotime('+6 days', strtotime($db_porting_weeks_row['ventelo_port_letter_date']));
    if($newDate <= $oneDay) {
        $portingCustomer[$porting_weeks_count]['id']                       = $db_porting_weeks_row['id'];
        $portingCustomer[$porting_weeks_count]['clientid']                 = $db_porting_weeks_row['clientid'];
        $portingCustomer[$porting_weeks_count]['firstname']                = $db_porting_weeks_row['firstname'];
        $portingCustomer[$porting_weeks_count]['lastname']                 = $db_porting_weeks_row['lastname'];
        $portingCustomer[$porting_weeks_count]['district']                 = $db_porting_weeks_row['district'];
        $portingCustomer[$porting_weeks_count]['ventelo_port_letter_date'] = $db_porting_weeks_row['ventelo_port_letter_date'];
        $portingCustomer[$porting_weeks_count]['ventelo_port_wish_date']   = $db_porting_weeks_row['ventelo_port_wish_date'];
        $porting_weeks_count++;
    }
}
$db_porting_weeks->free();

// Anbieterwechsel WBCI

// Portierungen die in den nächsten Tagen zu bearbeiten sind
$db_wbci_days = $db->query( $sql['wbci_days']);
$wbci_days_count = 0;
while ($db_wbci_days_row = $db_wbci_days->fetch_assoc()) {
    #Kündigungsfrist in Tage berechnen
    $fristzahl  = trim($db_wbci_days_row["notice_period_phone_int"]);
    $fristart   = $db_wbci_days_row["notice_period_phone_type"];
    if($fristart == 'months') {
        $fristzahl = $fristzahl * 30;
    } else if($fristart == 'weeks') {
        $fristzahl = $fristzahl * 7;
    } else if ($fristart == 'weeks till months end') {
        $fristzahl = $fristzahl * 7 + 14;
    }
  
    #Frist und 24 Wochen von Kdgdatum abziehen
    //$newDate = strtotime('-' . ($fristzahl + 168) . ' days', strtotime($db_wbci_days_row['exp_date_phone']));
    $newDate = 0;
 
    // Kunden zählen  
    if($newDate <= $oneDay) {
        $wbci[$wbci_days_count]['id']                      = $db_wbci_days_row['id'];
        $wbci[$wbci_days_count]['clientid']                = $db_wbci_days_row['clientid'];
        $wbci[$wbci_days_count]['firstname']               = $db_wbci_days_row['firstname'];
        $wbci[$wbci_days_count]['lastname']                = $db_wbci_days_row['lastname'];
        $wbci[$wbci_days_count]['district']                = $db_wbci_days_row['district'];
        $wbci[$wbci_days_count]['wbci_eingestellt_am']     = $db_wbci_days_row['wbci_eingestellt_am'];
        $wbci[$wbci_days_count]['exp_date_phone']          = $db_wbci_days_row['exp_date_phone'];
        $wbci[$wbci_days_count]['notice_period_phone_int'] = $db_wbci_days_row['notice_period_phone_int'];
        switch ($db_wbci_days_row['notice_period_phone_type']) {
            case 'months':
                $wbci[$wbci_days_count]['notice_period_phone_type'] = 'Monat(e)';
                break;
            case 'weeks':
                $wbci[$wbci_days_count]['notice_period_phone_type'] = 'Woche(n)';
                break;
            case 'days':
                $wbci[$wbci_days_count]['notice_period_phone_type'] = 'Tag(e)';
                break;
            case 'weeks till months end':
                $wbci[$wbci_days_count]['notice_period_phone_type'] = 'Woche(n) bis zum Monatsende';
                break;
        }
        $wbci_days_count++;
    }
}
$db_wbci_days->free();

// Portierung mehr als eine Woche beim Kunden
$db_wbci_weeks = $db->query( $sql['wbci_weeks']);
$wbci_weeks_count = 0;
while ($db_wbci_weeks_row = $db_wbci_weeks->fetch_assoc()) {
    # Port versendet String in englisches Datum wandeln
    $newDate = strtotime('+6 days', strtotime($db_wbci_weeks_row['ventelo_port_letter_date']));
    if($newDate <= $oneDay) {
        $wbciCustomer[$wbci_weeks_count]['id']                       = $db_wbci_weeks_row['id'];
        $wbciCustomer[$wbci_weeks_count]['clientid']                 = $db_wbci_weeks_row['clientid'];
        $wbciCustomer[$wbci_weeks_count]['firstname']                = $db_wbci_weeks_row['firstname'];
        $wbciCustomer[$wbci_weeks_count]['lastname']                 = $db_wbci_weeks_row['lastname'];
        $wbciCustomer[$wbci_weeks_count]['district']                 = $db_wbci_weeks_row['district'];
        $wbciCustomer[$wbci_weeks_count]['ventelo_port_letter_date'] = $db_wbci_weeks_row['ventelo_port_letter_date'];
        $wbciCustomer[$wbci_weeks_count]['ventelo_port_wish_date']   = $db_wbci_weeks_row['ventelo_port_wish_date'];
        $wbci_weeks_count++;
    }
}
$db_wbci_weeks->free();

// Portierung in Purtel
$db_porting_purtel = $db->query( $sql['porting_purtel']);
$porting_purtel_count = 0;
  
while ($db_porting_purtel_row = $db_porting_purtel->fetch_assoc()) {
    $portingPurtel[$porting_purtel_count]['id']                        = $db_porting_purtel_row['id'];
    $portingPurtel[$porting_purtel_count]['clientid']                  = $db_porting_purtel_row['clientid'];
    $portingPurtel[$porting_purtel_count]['firstname']                 = $db_porting_purtel_row['firstname'];
    $portingPurtel[$porting_purtel_count]['lastname']                  = $db_porting_purtel_row['lastname'];
    $portingPurtel[$porting_purtel_count]['district']                  = $db_porting_purtel_row['district'];
    $portingPurtel[$porting_purtel_count]['ventelo_purtel_enter_date'] = $db_porting_purtel_row['ventelo_purtel_enter_date'];
    $portingPurtel[$porting_purtel_count]['ventelo_port_wish_date']    = $db_porting_purtel_row['ventelo_port_wish_date'];
    $porting_purtel_count++;
}
  
$db_porting_purtel->free();
  
// Rufnummern aufschalten 
$db_porting_callnum = $db->query( $sql['porting_callnum']);
$porting_callnum_count = 0;
while ($db_porting_callnum_row = $db_porting_callnum->fetch_assoc()) {
    # Port best. String in englisches Datum wandeln
    $callDate = strtotime('-8 days', strtotime($db_porting_callnum_row['stati_port_confirm_date']));
    if($callDate <= $oneDay) {
        $portingCallnum[$porting_callnum_count]['id']                      = $db_porting_callnum_row['id'];
        $portingCallnum[$porting_callnum_count]['clientid']                = $db_porting_callnum_row['clientid'];
        $portingCallnum[$porting_callnum_count]['firstname']               = $db_porting_callnum_row['firstname'];
        $portingCallnum[$porting_callnum_count]['lastname']                = $db_porting_callnum_row['lastname'];
        $portingCallnum[$porting_callnum_count]['stati_port_confirm_date'] = $db_porting_callnum_row['stati_port_confirm_date'];
        $portingCallnum[$porting_callnum_count]['tal_order_ack_date']      = $db_porting_callnum_row['tal_order_ack_date'];
        $porting_callnum_count++;
    }
}
$db_porting_callnum->free();

// neue Rufnummern 
$db_porting_newcall = $db->query( $sql['porting_newcall']);
$porting_newcall_count = 0;
while ($db_porting_newcall_row = $db_porting_newcall->fetch_assoc()) {
    $portingNewcall[$porting_newcall_count]['id']              = $db_porting_newcall_row['id'];
    $portingNewcall[$porting_newcall_count]['clientid']        = $db_porting_newcall_row['clientid'];
    $portingNewcall[$porting_newcall_count]['firstname']       = $db_porting_newcall_row['firstname'];
    $portingNewcall[$porting_newcall_count]['lastname']        = $db_porting_newcall_row['lastname'];
    $portingNewcall[$porting_newcall_count]['district']        = $db_porting_newcall_row['district'];
    $portingNewcall[$porting_newcall_count]['new_number_date'] = $db_porting_newcall_row['new_number_date'];
    $porting_newcall_count++;
}
  
$db_porting_newcall->free();

// Umroutungen
$db_porting_route = $db->query( $sql['porting_route']);
$porting_route_count = 0;
while ($db_porting_route_row = $db_porting_route->fetch_assoc()) {
    $portingRoute[$porting_route_count]['id']                         = $db_porting_route_row['id'];
    $portingRoute[$porting_route_count]['clientid']                   = $db_porting_route_row['clientid'];
    $portingRoute[$porting_route_count]['firstname']                  = $db_porting_route_row['firstname'];
    $portingRoute[$porting_route_count]['lastname']                   = $db_porting_route_row['lastname'];
    $portingRoute[$porting_route_count]['district']                   = $db_porting_route_row['district'];
    $portingRoute[$porting_route_count]['connection_activation_date'] = $db_porting_route_row['connection_activation_date'];
    $porting_route_count++;
}
$db_porting_route->free();

// TAL's
  
// TALS die zu bestellen sind
// Hertmannsweiler und Kleinaspacher nicht!!!

$db_tal_order1 = $db->query( $sql['tal_order1']);
$db_tal_order2 = $db->query( $sql['tal_order2']);
$db_tal_order3 = $db->query( $sql['tal_order3']);

$tal_order_count = 0;
  
while ($db_tal_order_row = $db_tal_order1->fetch_assoc()) {
    $newDate = strtotime('-6 month', strtotime($db_tal_order_row['stati_port_confirm_date']));
    if($newDate <= $oneDay) {
        $portingOrderTal[$tal_order_count]['id']                      = $db_tal_order_row['id'];
        $portingOrderTal[$tal_order_count]['clientid']                = $db_tal_order_row['clientid'];
        $portingOrderTal[$tal_order_count]['firstname']               = $db_tal_order_row['firstname'];
        $portingOrderTal[$tal_order_count]['lastname']                = $db_tal_order_row['lastname'];
        $portingOrderTal[$tal_order_count]['district']                = $db_tal_order_row['district'];
        $portingOrderTal[$tal_order_count]['ventelo_port_wish_date']  = $db_tal_order_row['ventelo_port_wish_date'];
        $portingOrderTal[$tal_order_count]['stati_port_confirm_date'] = $db_tal_order_row['stati_port_confirm_date'];
        $portingOrderTal[$tal_order_count]['sofortdsl']               = $db_tal_order_row['sofortdsl'];
        $portingOrderTal[$tal_order_count]['oldcontract_exists']      = $db_tal_order_row['oldcontract_exists'];
        $tal_order_count++;
    }
}

while ($db_tal_order_row = $db_tal_order2->fetch_assoc()) {
    $portingOrderTal[$tal_order_count]['id']                      = $db_tal_order_row['id'];
    $portingOrderTal[$tal_order_count]['clientid']                = $db_tal_order_row['clientid'];
    $portingOrderTal[$tal_order_count]['firstname']               = $db_tal_order_row['firstname'];
    $portingOrderTal[$tal_order_count]['lastname']                = $db_tal_order_row['lastname'];
    $portingOrderTal[$tal_order_count]['district']                = $db_tal_order_row['district'];
    $portingOrderTal[$tal_order_count]['ventelo_port_wish_date']  = $db_tal_order_row['ventelo_port_wish_date'];
    $portingOrderTal[$tal_order_count]['stati_port_confirm_date'] = $db_tal_order_row['stati_port_confirm_date'];
    $portingOrderTal[$tal_order_count]['sofortdsl']               = $db_tal_order_row['sofortdsl'];
    $portingOrderTal[$tal_order_count]['oldcontract_exists']      = $db_tal_order_row['oldcontract_exists'];
    $tal_order_count++;
}

while ($db_tal_order_row = $db_tal_order3->fetch_assoc()) {
    $newDate = strtotime('-6 month', strtotime($db_tal_order_row['ventelo_port_wish_date']));
    if($newDate <= $oneDay) {
        $portingOrderTal[$tal_order_count]['id']                      = $db_tal_order_row['id'];
        $portingOrderTal[$tal_order_count]['clientid']                = $db_tal_order_row['clientid'];
        $portingOrderTal[$tal_order_count]['firstname']               = $db_tal_order_row['firstname'];
        $portingOrderTal[$tal_order_count]['lastname']                = $db_tal_order_row['lastname'];
        $portingOrderTal[$tal_order_count]['district']                = $db_tal_order_row['district'];
        $portingOrderTal[$tal_order_count]['ventelo_port_wish_date']  = $db_tal_order_row['ventelo_port_wish_date'];
        $portingOrderTal[$tal_order_count]['stati_port_confirm_date'] = $db_tal_order_row['stati_port_confirm_date'];
        $portingOrderTal[$tal_order_count]['sofortdsl']               = $db_tal_order_row['sofortdsl'];
        $portingOrderTal[$tal_order_count]['oldcontract_exists']      = $db_tal_order_row['oldcontract_exists'];
        $tal_order_count++;
    }
}
$db_tal_order1->free();
$db_tal_order2->free();
$db_tal_order3->free();

// TALs sind bestellt
$db_tal_ordered = $db->query( $sql['tal_ordered']);
$tal_ordered_count = 0;
while ($db_tal_ordered_row = $db_tal_ordered->fetch_assoc()) {
    $portingOrderedTal[$tal_ordered_count]['id']                   = $db_tal_ordered_row['id'];
    $portingOrderedTal[$tal_ordered_count]['clientid']             = $db_tal_ordered_row['clientid'];
    $portingOrderedTal[$tal_ordered_count]['firstname']            = $db_tal_ordered_row['firstname'];
    $portingOrderedTal[$tal_ordered_count]['lastname']             = $db_tal_ordered_row['lastname'];
    $portingOrderedTal[$tal_ordered_count]['district']             = $db_tal_ordered_row['district'];
    $portingOrderedTal[$tal_ordered_count]['tal_order_date']       = $db_tal_ordered_row['tal_order_date'];
    $portingOrderedTal[$tal_ordered_count]['tal_ordered_for_date'] = $db_tal_ordered_row['tal_ordered_for_date'];
    $tal_ordered_count++;
}
$db_tal_ordered->free();

// Kunden über Anschaltterm informieren
$db_tal_connect_info = $db->query( $sql['tal_connect_info']);
$tal_connect_info_count = 0;
while ($db_tal_connect_info_row = $db_tal_connect_info->fetch_assoc()) {
    $portingTalConnectInfo[$tal_connect_info_count]['id']                   = $db_tal_connect_info_row['id'];
    $portingTalConnectInfo[$tal_connect_info_count]['clientid']             = $db_tal_connect_info_row['clientid'];
    $portingTalConnectInfo[$tal_connect_info_count]['firstname']            = $db_tal_connect_info_row['firstname'];
    $portingTalConnectInfo[$tal_connect_info_count]['lastname']             = $db_tal_connect_info_row['lastname'];
    $portingTalConnectInfo[$tal_connect_info_count]['district']             = $db_tal_connect_info_row['district'];
    $portingTalConnectInfo[$tal_connect_info_count]['tal_ordered_for_date'] = $db_tal_connect_info_row['tal_ordered_for_date'];
    $portingTalConnectInfo[$tal_connect_info_count]['tal_order_ack_date']   = $db_tal_connect_info_row['tal_order_ack_date'];
    $tal_connect_info_count++;
}
$db_tal_connect_info->free();

// Bestätigte Tals
$db_tal_confirmed = $db->query( $sql['tal_confirmed']);
$tal_confirmed_count = 0;
$threeWeeks = time() + (90* 24* 60* 60);
$fourWeeksAgo = new DateTime('now - 4 week');
$nowDate = new DateTime('now');
$confirmedTalsWithinPastFourWeeks = array();

while ($db_tal_confirmed_row = $db_tal_confirmed->fetch_assoc()) {
    $newDate = strtotime('+0 days', strtotime($db_tal_confirmed_row['tal_order_ack_date']));
    $tooEarly = $oneDay  <= $newDate;
    $tooLate = $threeWeeks >= $newDate;

    if($tooEarly && $tooLate) {
        $portingTalConfirmed[$tal_confirmed_count]['id']                         = $db_tal_confirmed_row['id'];
        $portingTalConfirmed[$tal_confirmed_count]['clientid']                   = $db_tal_confirmed_row['clientid'];
        $portingTalConfirmed[$tal_confirmed_count]['firstname']                  = $db_tal_confirmed_row['firstname'];
        $portingTalConfirmed[$tal_confirmed_count]['lastname']                   = $db_tal_confirmed_row['lastname'];
        $portingTalConfirmed[$tal_confirmed_count]['district']                   = $db_tal_confirmed_row['district'];
        $portingTalConfirmed[$tal_confirmed_count]['stati_port_confirm_date']    = $db_tal_confirmed_row['stati_port_confirm_date'];
        $portingTalConfirmed[$tal_confirmed_count]['phone_number_added']         = $db_tal_confirmed_row['phone_number_added'];
        $portingTalConfirmed[$tal_confirmed_count]['tal_order_ack_date']         = $db_tal_confirmed_row['tal_order_ack_date'];
        $portingTalConfirmed[$tal_confirmed_count]['terminal_type']              = $db_tal_confirmed_row['terminal_type'];
        $portingTalConfirmed[$tal_confirmed_count]['terminal_ready']             = $db_tal_confirmed_row['terminal_ready'];
        $portingTalConfirmed[$tal_confirmed_count]['terminal_sended_date']       = $db_tal_confirmed_row['terminal_sended_date'];
        $portingTalConfirmed[$tal_confirmed_count]['service_technician']         = $db_tal_confirmed_row['service_technician'];
        $portingTalConfirmed[$tal_confirmed_count]['connection_activation_date'] = $db_tal_confirmed_row['connection_activation_date'];
        $portingTalConfirmed[$tal_confirmed_count]['dslam_arranged_date']        = $db_tal_confirmed_row['dslam_arranged_date'];
        $portingTalConfirmed[$tal_confirmed_count]['pppoe_config_date']          = $db_tal_confirmed_row['pppoe_config_date'];
        $portingTalConfirmed[$tal_confirmed_count]['patch_date']                 = $db_tal_confirmed_row['patch_date'];
        $tal_confirmed_count++;
    } else {
        // Confirmed tals within past four weeks
        $db_tal_confirmed_row['tal_order_ack_date'] = preg_replace('/[^\d.]/', '', $db_tal_confirmed_row['tal_order_ack_date']);
        $talOrderAckDate = new DateTime($db_tal_confirmed_row['tal_order_ack_date']);

        if ($talOrderAckDate >= $fourWeeksAgo && $talOrderAckDate <= $nowDate) {
            $confirmedTalsWithinPastFourWeeks[] = $db_tal_confirmed_row;
        }
    }
}

$db_tal_confirmed->free();

// Fritzboxen
$db_fritzbox1 = $db->prepare($sql['fritz_boxes1']);
$db_fritzbox2 = $db->prepare($sql['fritz_boxes2']);
$db_fritzbox3 = $db->prepare($sql['fritz_boxes3']);
$db_fritzbox4 = $db->prepare($sql['fritz_boxes4']);
$db_fritzbox5 = $db->prepare($sql['fritz_boxes5']);
$db_fritzbox6 = $db->prepare($sql['fritz_boxes6']);
$db_fritzbox7 = $db->prepare($sql['fritz_boxes7']);
$db_fritzbox8 = $db->prepare($sql['fritz_boxes8']);
$db_fritzbox9 = $db->prepare($sql['fritz_boxes9']);

$db_fritzbox1->bind_param('s', $fritzbox_type);
$db_fritzbox2->bind_param('s', $fritzbox_type);
$db_fritzbox3->bind_param('s', $fritzbox_type);
$db_fritzbox4->bind_param('s', $fritzbox_type);
$db_fritzbox5->bind_param('s', $fritzbox_type);
$db_fritzbox6->bind_param('s', $fritzbox_type);
$db_fritzbox7->bind_param('s', $fritzbox_type);
$db_fritzbox8->bind_param('s', $fritzbox_type);
$db_fritzbox9->bind_param('s', $fritzbox_type);
 
// 6320
$fritzbox_type = '6320';
$fritzbox_6320_count = 0;

$db_fritzbox1->execute();
$db_fritzbox1_result = $db_fritzbox1->get_result();
while ($db_fritzbox1_row = $db_fritzbox1_result->fetch_assoc()) {
    $portingFritzbox6320[$fritzbox_6320_count]['id']                      = $db_fritzbox1_row['id'];
    $portingFritzbox6320[$fritzbox_6320_count]['clientid']                = $db_fritzbox1_row['clientid'];
    $portingFritzbox6320[$fritzbox_6320_count]['firstname']               = $db_fritzbox1_row['firstname'];
    $portingFritzbox6320[$fritzbox_6320_count]['lastname']                = $db_fritzbox1_row['lastname'];
    $portingFritzbox6320[$fritzbox_6320_count]['district']                = $db_fritzbox1_row['district'];
    $portingFritzbox6320[$fritzbox_6320_count]['stati_port_confirm_date'] = $db_fritzbox1_row['stati_port_confirm_date'];
    $portingFritzbox6320[$fritzbox_6320_count]['$tal_ordered_for_date']   = $db_fritzbox1_row['tal_ordered_for_date'];
    $portingFritzbox6320[$fritzbox_6320_count]['$tal_order_ack_date']     = $db_fritzbox1_row['tal_order_ack_date'];
    $portingFritzbox6320[$fritzbox_6320_count]['sofortdsl']               = $db_fritzbox1_row['sofortdsl']; 

    $fritzbox_6320_count++;
}

$db_fritzbox2->execute();
$db_fritzbox2_result = $db_fritzbox2->get_result();
while ($db_fritzbox2_row = $db_fritzbox2_result->fetch_assoc()) {
    $newDate = strtotime('-3 months', strtotime($db_fritzbox2_row['stati_port_confirm_date']));
    if($newDate <= $oneDay) {
        $portingFritzbox6320[$fritzbox_6320_count]['id']                      = $db_fritzbox2_row['id'];
        $portingFritzbox6320[$fritzbox_6320_count]['clientid']                = $db_fritzbox2_row['clientid'];
        $portingFritzbox6320[$fritzbox_6320_count]['firstname']               = $db_fritzbox2_row['firstname'];
        $portingFritzbox6320[$fritzbox_6320_count]['lastname']                = $db_fritzbox2_row['lastname'];
        $portingFritzbox6320[$fritzbox_6320_count]['district']                = $db_fritzbox2_row['district'];
        $portingFritzbox6320[$fritzbox_6320_count]['stati_port_confirm_date'] = $db_fritzbox2_row['stati_port_confirm_date'];
        $portingFritzbox6320[$fritzbox_6320_count]['tal_ordered_for_date']    = $db_fritzbox2_row['tal_ordered_for_date'];
        $portingFritzbox6320[$fritzbox_6320_count]['tal_order_ack_date']      = $db_fritzbox2_row['tal_order_ack_date'];
        $portingFritzbox6320[$fritzbox_6320_count]['sofortdsl']               = $db_fritzbox2_row['sofortdsl']; 
        $fritzbox_6320_count++;
    }
}
$db_fritzbox1_result->free_result();
$db_fritzbox2_result->free_result();

// 6360
$fritzbox_type = '6360';
$fritzbox_6360_count = 0;

$db_fritzbox1->execute();
$db_fritzbox1_result = $db_fritzbox1->get_result();
while ($db_fritzbox1_row = $db_fritzbox1_result->fetch_assoc()) {
    $portingFritzbox6360[$fritzbox_6360_count]['id']                      = $db_fritzbox1_row['id'];
    $portingFritzbox6360[$fritzbox_6360_count]['clientid']                = $db_fritzbox1_row['clientid'];
    $portingFritzbox6360[$fritzbox_6360_count]['firstname']               = $db_fritzbox1_row['firstname'];
    $portingFritzbox6360[$fritzbox_6360_count]['lastname']                = $db_fritzbox1_row['lastname'];
    $portingFritzbox6360[$fritzbox_6360_count]['district']                = $db_fritzbox1_row['district'];
    $portingFritzbox6360[$fritzbox_6360_count]['stati_port_confirm_date'] = $db_fritzbox1_row['stati_port_confirm_date'];
    $portingFritzbox6360[$fritzbox_6360_count]['tal_ordered_for_date']    = $db_fritzbox1_row['tal_ordered_for_date'];
    $portingFritzbox6360[$fritzbox_6360_count]['tal_order_ack_date']      = $db_fritzbox1_row['tal_order_ack_date'];
    $portingFritzbox6360[$fritzbox_6360_count]['sofortdsl']               = $db_fritzbox1_row['sofortdsl']; 
    $fritzbox_6360_count++;
}

$db_fritzbox2->execute();
$db_fritzbox2_result = $db_fritzbox2->get_result();
while ($db_fritzbox2_row = $db_fritzbox2_result->fetch_assoc()) {
    $newDate = strtotime('-3 months', strtotime($db_fritzbox2_row['stati_port_confirm_date']));
    if($newDate <= $oneDay) {
        $portingFritzbox6360[$fritzbox_6360_count]['id']                      = $db_fritzbox2_row['id'];
        $portingFritzbox6360[$fritzbox_6360_count]['clientid']                = $db_fritzbox2_row['clientid'];
        $portingFritzbox6360[$fritzbox_6360_count]['firstname']               = $db_fritzbox2_row['firstname'];
        $portingFritzbox6360[$fritzbox_6360_count]['lastname']                = $db_fritzbox2_row['lastname'];
        $portingFritzbox6360[$fritzbox_6360_count]['district']                = $db_fritzbox2_row['district'];
        $portingFritzbox6360[$fritzbox_6360_count]['stati_port_confirm_date'] = $db_fritzbox2_row['stati_port_confirm_date'];
        $portingFritzbox6360[$fritzbox_6360_count]['tal_ordered_for_date']    = $db_fritzbox2_row['tal_ordered_for_date'];
        $portingFritzbox6360[$fritzbox_6360_count]['tal_order_ack_date']      = $db_fritzbox2_row['tal_order_ack_date'];
        $portingFritzbox6360[$fritzbox_6360_count]['sofortdsl']               = $db_fritzbox2_row['sofortdsl']; 
        $fritzbox_6360_count++;
    }
}
$db_fritzbox1->free_result();
$db_fritzbox2_result->free_result();

// 6490
$fritzbox_type = '6490';
$fritzbox_6490_count = 0;

$db_fritzbox1->execute();
$db_fritzbox1_result = $db_fritzbox1->get_result();
while ($db_fritzbox1_row = $db_fritzbox1_result->fetch_assoc()) {
    $portingFritzbox6490[$fritzbox_6490_count]['id']                      = $db_fritzbox1_row['id'];
    $portingFritzbox6490[$fritzbox_6490_count]['clientid']                = $db_fritzbox1_row['clientid'];
    $portingFritzbox6490[$fritzbox_6490_count]['firstname']               = $db_fritzbox1_row['firstname'];
    $portingFritzbox6490[$fritzbox_6490_count]['lastname']                = $db_fritzbox1_row['lastname'];
    $portingFritzbox6490[$fritzbox_6490_count]['district']                = $db_fritzbox1_row['district'];
    $portingFritzbox6490[$fritzbox_6490_count]['stati_port_confirm_date'] = $db_fritzbox1_row['stati_port_confirm_date'];
    $portingFritzbox6490[$fritzbox_6490_count]['tal_ordered_for_date']    = $db_fritzbox1_row['tal_ordered_for_date'];
    $portingFritzbox6490[$fritzbox_6490_count]['tal_order_ack_date']      = $db_fritzbox1_row['tal_order_ack_date'];
    $portingFritzbox6490[$fritzbox_6490_count]['sofortdsl']               = $db_fritzbox1_row['sofortdsl']; 
    $fritzbox_6490_count++;
}

$db_fritzbox2->execute();
$db_fritzbox2_result = $db_fritzbox2->get_result();
while ($db_fritzbox2_row = $db_fritzbox2_result->fetch_assoc()) {
    $newDate = strtotime('-3 months', strtotime($db_fritzbox2_row['stati_port_confirm_date']));
    if($newDate <= $oneDay) {
        $portingFritzbox6360[$fritzbox_6360_count]['id']                      = $db_fritzbox2_row['id'];
        $portingFritzbox6360[$fritzbox_6360_count]['clientid']                = $db_fritzbox2_row['clientid'];
        $portingFritzbox6360[$fritzbox_6360_count]['firstname']               = $db_fritzbox2_row['firstname'];
        $portingFritzbox6360[$fritzbox_6360_count]['lastname']                = $db_fritzbox2_row['lastname'];
        $portingFritzbox6360[$fritzbox_6360_count]['district']                = $db_fritzbox2_row['district'];
        $portingFritzbox6360[$fritzbox_6360_count]['stati_port_confirm_date'] = $db_fritzbox2_row['stati_port_confirm_date'];
        $portingFritzbox6360[$fritzbox_6360_count]['tal_ordered_for_date']    = $db_fritzbox2_row['tal_ordered_for_date'];
        $portingFritzbox6360[$fritzbox_6360_count]['tal_order_ack_date']      = $db_fritzbox2_row['tal_order_ack_date'];
        $portingFritzbox6360[$fritzbox_6360_count]['sofortdsl']               = $db_fritzbox2_row['sofortdsl']; 
        $fritzbox_6360_count++;
    }
}
$db_fritzbox1->free_result();
$db_fritzbox2_result->free_result();
  
// 7270
$fritzbox_type = '7270';
$fritzbox_7270_count = 0;

$db_fritzbox3->execute();
$db_fritzbox3_result = $db_fritzbox3->get_result();
while ($db_fritzbox3_row = $db_fritzbox3_result->fetch_assoc()) {
    $newDate = strtotime('-3 months', strtotime($db_fritzbox3_row['tal_order_ack_date']));
    if($newDate <= $oneDay){
        $portingFritzbox7270[$fritzbox_7270_count]['id']                      = $db_fritzbox3_row['id'];
        $portingFritzbox7270[$fritzbox_7270_count]['clientid']                = $db_fritzbox3_row['clientid'];
        $portingFritzbox7270[$fritzbox_7270_count]['firstname']               = $db_fritzbox3_row['firstname'];
        $portingFritzbox7270[$fritzbox_7270_count]['lastname']                = $db_fritzbox3_row['lastname'];
        $portingFritzbox7270[$fritzbox_7270_count]['district']                = $db_fritzbox3_row['district'];
        $portingFritzbox7270[$fritzbox_7270_count]['stati_port_confirm_date'] = $db_fritzbox3_row['stati_port_confirm_date'];
        $portingFritzbox7270[$fritzbox_7270_count]['tal_ordered_for_date']    = $db_fritzbox3_row['tal_ordered_for_date'];
        $portingFritzbox7270[$fritzbox_7270_count]['tal_order_ack_date']      = $db_fritzbox3_row['tal_order_ack_date'];
        $portingFritzbox7270[$fritzbox_7270_count]['sofortdsl']               = $db_fritzbox3_row['sofortdsl']; 
        $fritzbox_7270_count++;
    }
}

$db_fritzbox4->execute();
$db_fritzbox4_result = $db_fritzbox4->get_result();
while ($db_fritzbox4_row = $db_fritzbox4_result->fetch_assoc()) {
    $newDate = strtotime('-3 months', strtotime($db_fritzbox4_row['tal_ordered_for_date']));
    if($newDate <= $oneDay){
        $portingFritzbox7270[$fritzbox_7270_count]['id']                      = $db_fritzbox4_row['id'];
        $portingFritzbox7270[$fritzbox_7270_count]['clientid']                = $db_fritzbox4_row['clientid'];
        $portingFritzbox7270[$fritzbox_7270_count]['firstname']               = $db_fritzbox4_row['firstname'];
        $portingFritzbox7270[$fritzbox_7270_count]['lastname']                = $db_fritzbox4_row['lastname'];
        $portingFritzbox7270[$fritzbox_7270_count]['district']                = $db_fritzbox4_row['district'];
        $portingFritzbox7270[$fritzbox_7270_count]['stati_port_confirm_date'] = $db_fritzbox4_row['stati_port_confirm_date'];
        $portingFritzbox7270[$fritzbox_7270_count]['tal_ordered_for_date']    = $db_fritzbox4_row['tal_ordered_for_date'];
        $portingFritzbox7270[$fritzbox_7270_count]['tal_order_ack_date']      = $db_fritzbox4_row['tal_order_ack_date'];
        $portingFritzbox7270[$fritzbox_7270_count]['sofortdsl']               = $db_fritzbox4_row['sofortdsl']; 
        $fritzbox_7270_count++;
    }
}
$db_fritzbox3_result->free_result();
$db_fritzbox4_result->free_result();
  
// 7360
$fritzbox_type = '7360';
$fritzbox_7360_count = 0;

$db_fritzbox5->execute();
$db_fritzbox5_result = $db_fritzbox5->get_result();
while ($db_fritzbox5_row = $db_fritzbox5_result->fetch_assoc()) {
    $portingFritzbox7360[$fritzbox_7360_count]['id']                      = $db_fritzbox5_row['id'];
    $portingFritzbox7360[$fritzbox_7360_count]['clientid']                = $db_fritzbox5_row['clientid'];
    $portingFritzbox7360[$fritzbox_7360_count]['firstname']               = $db_fritzbox5_row['firstname'];
    $portingFritzbox7360[$fritzbox_7360_count]['lastname']                = $db_fritzbox5_row['lastname'];
    $portingFritzbox7360[$fritzbox_7360_count]['district']                = $db_fritzbox5_row['district'];
    $portingFritzbox7360[$fritzbox_7360_count]['stati_port_confirm_date'] = $db_fritzbox5_row['stati_port_confirm_date'];
    $portingFritzbox7360[$fritzbox_7360_count]['tal_ordered_for_date']    = $db_fritzbox5_row['tal_ordered_for_date'];
    $portingFritzbox7360[$fritzbox_7360_count]['tal_order_ack_date']      = $db_fritzbox5_row['tal_order_ack_date'];
    $portingFritzbox7360[$fritzbox_7360_count]['sofortdsl']               = $db_fritzbox5_row['sofortdsl']; 
    $fritzbox_7360_count++;
}

$db_fritzbox6->execute();
$db_fritzbox6_result = $db_fritzbox6->get_result();
while ($db_fritzbox6_row = $db_fritzbox6_result->fetch_assoc()) {
    $newDate = strtotime('-3 months', strtotime($db_fritzbox6_row['stati_port_confirm_date']));
    if($newDate <= $oneDay) {
        $portingFritzbox7360[$fritzbox_7360_count]['id']                      = $db_fritzbox6_row['id'];
        $portingFritzbox7360[$fritzbox_7360_count]['clientid']                = $db_fritzbox6_row['clientid'];
        $portingFritzbox7360[$fritzbox_7360_count]['firstname']               = $db_fritzbox6_row['firstname'];
        $portingFritzbox7360[$fritzbox_7360_count]['lastname']                = $db_fritzbox6_row['lastname'];
        $portingFritzbox7360[$fritzbox_7360_count]['district']                = $db_fritzbox6_row['district'];
        $portingFritzbox7360[$fritzbox_7360_count]['stati_port_confirm_date'] = $db_fritzbox6_row['stati_port_confirm_date'];
        $portingFritzbox7360[$fritzbox_7360_count]['tal_ordered_for_date']    = $db_fritzbox6_row['tal_ordered_for_date'];
        $portingFritzbox7360[$fritzbox_7360_count]['tal_order_ack_date']      = $db_fritzbox6_row['tal_order_ack_date'];
        $portingFritzbox7360[$fritzbox_7360_count]['sofortdsl']               = $db_fritzbox6_row['sofortdsl']; 
        $fritzbox_7360_count++;
    }
}

$db_fritzbox7->execute();
$db_fritzbox7_result = $db_fritzbox7->get_result();
while ($db_fritzbox7_row = $db_fritzbox7_result->fetch_assoc()) {
    if($db_fritzbox7_row['tal_order_ack_date'] != '' && $db_fritzbox7_row['tal_order_ack_date'] != $db_fritzbox7_row['stati_port_confirm_date']) {
        $newDate = strtotime('-3 months', strtotime($db_fritzbox7_row['tal_order_ack_date']));
        if($newDate <= $oneDay) {
            $portingFritzbox7360[$fritzbox_7360_count]['id']                      = $db_fritzbox7_row['id'];
            $portingFritzbox7360[$fritzbox_7360_count]['clientid']                = $db_fritzbox7_row['clientid'];
            $portingFritzbox7360[$fritzbox_7360_count]['firstname']               = $db_fritzbox7_row['firstname'];
            $portingFritzbox7360[$fritzbox_7360_count]['lastname']                = $db_fritzbox7_row['lastname'];
            $portingFritzbox7360[$fritzbox_7360_count]['district']                = $db_fritzbox7_row['district'];
            $portingFritzbox7360[$fritzbox_7360_count]['stati_port_confirm_date'] = $db_fritzbox7_row['stati_port_confirm_date'];
            $portingFritzbox7360[$fritzbox_7360_count]['tal_ordered_for_date']    = $db_fritzbox7_row['tal_ordered_for_date'];
            $portingFritzbox7360[$fritzbox_7360_count]['tal_order_ack_date']      = $db_fritzbox7_row['tal_order_ack_date'];
            $portingFritzbox7360[$fritzbox_7360_count]['sofortdsl']               = $db_fritzbox7_row['sofortdsl']; 
            $fritzbox_7360_count++;
        }
    } elseif ($db_fritzbox7_row['tal_ordered_for_date'] != '' && $db_fritzbox7_row['tal_ordered_for_date'] != $db_fritzbox7_row['stati_port_confirm_date']) {
        $newDate = strtotime('-3 months', strtotime($db_fritzbox7_row['tal_ordered_for_date']));
        if($newDate <= $oneDay) {
            $portingFritzbox7360[$fritzbox_7360_count]['id']                      = $db_fritzbox7_row['id'];
            $portingFritzbox7360[$fritzbox_7360_count]['clientid']                = $db_fritzbox7_row['clientid'];
            $portingFritzbox7360[$fritzbox_7360_count]['firstname']               = $db_fritzbox7_row['firstname'];
            $portingFritzbox7360[$fritzbox_7360_count]['lastname']                = $db_fritzbox7_row['lastname'];
            $portingFritzbox7360[$fritzbox_7360_count]['district']                = $db_fritzbox7_row['district'];
            $portingFritzbox7360[$fritzbox_7360_count]['stati_port_confirm_date'] = $db_fritzbox7_row['stati_port_confirm_date'];
            $portingFritzbox7360[$fritzbox_7360_count]['tal_ordered_for_date']    = $db_fritzbox7_row['tal_ordered_for_date'];
            $portingFritzbox7360[$fritzbox_7360_count]['tal_order_ack_date']      = $db_fritzbox7_row['tal_order_ack_date'];
            $portingFritzbox7360[$fritzbox_7360_count]['sofortdsl']               = $db_fritzbox7_row['sofortdsl']; 
            $fritzbox_7360_count++;
        }
    }
}

$db_fritzbox5_result->free_result();
$db_fritzbox6_result->free_result();
$db_fritzbox7_result->free_result();

// 7390
$fritzbox_type = '7390';
$fritzbox_7390_count = 0;

$db_fritzbox5->execute();
$db_fritzbox5_result = $db_fritzbox5->get_result();
while ($db_fritzbox5_row = $db_fritzbox5_result->fetch_assoc()) {
    $portingFritzbox7390[$fritzbox_7390_count]['id']                      = $db_fritzbox5_row['id'];
    $portingFritzbox7390[$fritzbox_7390_count]['clientid']                = $db_fritzbox5_row['clientid'];
    $portingFritzbox7390[$fritzbox_7390_count]['firstname']               = $db_fritzbox5_row['firstname'];
    $portingFritzbox7390[$fritzbox_7390_count]['lastname']                = $db_fritzbox5_row['lastname'];
    $portingFritzbox7390[$fritzbox_7390_count]['district']                = $db_fritzbox5_row['district'];
    $portingFritzbox7390[$fritzbox_7390_count]['stati_port_confirm_date'] = $db_fritzbox5_row['stati_port_confirm_date'];
    $portingFritzbox7390[$fritzbox_7390_count]['tal_ordered_for_date']    = $db_fritzbox5_row['tal_ordered_for_date'];
    $portingFritzbox7390[$fritzbox_7390_count]['tal_order_ack_date']      = $db_fritzbox5_row['tal_order_ack_date'];
    $portingFritzbox7390[$fritzbox_7390_count]['sofortdsl']               = $db_fritzbox5_row['sofortdsl']; 
    $fritzbox_7390_count++;
}

$db_fritzbox6->execute();
$db_fritzbox6_result = $db_fritzbox6->get_result();
while ($db_fritzbox6_row = $db_fritzbox6_result->fetch_assoc()) {
    $newDate = strtotime('-21 days', strtotime($db_fritzbox6_row['stati_port_confirm_date']));
    if($newDate <= $oneDay) {
        $portingFritzbox7390[$fritzbox_7390_count]['id']                      = $db_fritzbox6_row['id'];
        $portingFritzbox7390[$fritzbox_7390_count]['clientid']                = $db_fritzbox6_row['clientid'];
        $portingFritzbox7390[$fritzbox_7390_count]['firstname']               = $db_fritzbox6_row['firstname'];
        $portingFritzbox7390[$fritzbox_7390_count]['lastname']                = $db_fritzbox6_row['lastname'];
        $portingFritzbox7390[$fritzbox_7390_count]['district']                = $db_fritzbox6_row['district'];
        $portingFritzbox7390[$fritzbox_7390_count]['stati_port_confirm_date'] = $db_fritzbox6_row['stati_port_confirm_date'];
        $portingFritzbox7390[$fritzbox_7390_count]['tal_ordered_for_date']    = $db_fritzbox6_row['tal_ordered_for_date'];
        $portingFritzbox7390[$fritzbox_7390_count]['tal_order_ack_date']      = $db_fritzbox6_row['tal_order_ack_date'];
        $portingFritzbox7390[$fritzbox_7390_count]['sofortdsl']               = $db_fritzbox6_row['sofortdsl']; 
        $fritzbox_7390_count++;
    }
}

$db_fritzbox7->execute();
$db_fritzbox7_result = $db_fritzbox7->get_result();
while ($db_fritzbox7_row = $db_fritzbox7_result->fetch_assoc()) {
    if($db_fritzbox7_row['tal_order_ack_date'] != '' && $db_fritzbox7_row['tal_order_ack_date'] != $db_fritzbox7_row['stati_port_confirm_date']) {
        $newDate = strtotime('-21 days', strtotime($db_fritzbox7_row['tal_order_ack_date']));
        if($newDate <= $oneDay) {
            $portingFritzbox7390[$fritzbox_7390_count]['id']                      = $db_fritzbox7_row['id'];
            $portingFritzbox7390[$fritzbox_7390_count]['clientid']                = $db_fritzbox7_row['clientid'];
            $portingFritzbox7390[$fritzbox_7390_count]['firstname']               = $db_fritzbox7_row['firstname'];
            $portingFritzbox7390[$fritzbox_7390_count]['lastname']                = $db_fritzbox7_row['lastname'];
            $portingFritzbox7390[$fritzbox_7390_count]['district']                = $db_fritzbox7_row['district'];
            $portingFritzbox7390[$fritzbox_7390_count]['stati_port_confirm_date'] = $db_fritzbox7_row['stati_port_confirm_date'];
            $portingFritzbox7390[$fritzbox_7390_count]['tal_ordered_for_date']    = $db_fritzbox7_row['tal_ordered_for_date'];
            $portingFritzbox7390[$fritzbox_7390_count]['tal_order_ack_date']      = $db_fritzbox7_row['tal_order_ack_date'];
            $portingFritzbox7390[$fritzbox_7390_count]['sofortdsl']               = $db_fritzbox7_row['sofortdsl']; 
            $fritzbox_7390_count++;
        }
    } elseif ($db_fritzbox7_row['tal_ordered_for_date'] != '' && $db_fritzbox7_row['tal_ordered_for_date'] != $db_fritzbox7_row['stati_port_confirm_date']) {
        $newDate = strtotime('-21 days', strtotime($db_fritzbox7_row['tal_ordered_for_date']));
        if($newDate <= $oneDay) {
            $portingFritzbox7390[$fritzbox_7390_count]['id']                      = $db_fritzbox7_row['id'];
            $portingFritzbox7390[$fritzbox_7390_count]['clientid']                = $db_fritzbox7_row['clientid'];
            $portingFritzbox7390[$fritzbox_7390_count]['firstname']               = $db_fritzbox7_row['firstname'];
            $portingFritzbox7390[$fritzbox_7390_count]['lastname']                = $db_fritzbox7_row['lastname'];
            $portingFritzbox7390[$fritzbox_7390_count]['district']                = $db_fritzbox7_row['district'];
            $portingFritzbox7390[$fritzbox_7390_count]['stati_port_confirm_date'] = $db_fritzbox7_row['stati_port_confirm_date'];
            $portingFritzbox7390[$fritzbox_7390_count]['tal_ordered_for_date']    = $db_fritzbox7_row['tal_ordered_for_date'];
            $portingFritzbox7390[$fritzbox_7390_count]['tal_order_ack_date']      = $db_fritzbox7_row['tal_order_ack_date'];
            $portingFritzbox7390[$fritzbox_7390_count]['sofortdsl']               = $db_fritzbox7_row['sofortdsl']; 
            $fritzbox_7390_count++;
        }
    }
}
  
$db_fritzbox5_result->free_result();
$db_fritzbox6_result->free_result();
$db_fritzbox7_result->free_result();
  
// 7490
$fritzbox_type = '7490';
$fritzbox_7490_count = 0;

$db_fritzbox5->execute();
$db_fritzbox5_result = $db_fritzbox5->get_result();
while ($db_fritzbox5_row = $db_fritzbox5_result->fetch_assoc()) {
    $portingFritzbox7490[$fritzbox_7490_count]['id']                      = $db_fritzbox5_row['id'];
    $portingFritzbox7490[$fritzbox_7490_count]['clientid']                = $db_fritzbox5_row['clientid'];
    $portingFritzbox7490[$fritzbox_7490_count]['firstname']               = $db_fritzbox5_row['firstname'];
    $portingFritzbox7490[$fritzbox_7490_count]['lastname']                = $db_fritzbox5_row['lastname'];
    $portingFritzbox7490[$fritzbox_7490_count]['district']                = $db_fritzbox5_row['district'];
    $portingFritzbox7490[$fritzbox_7490_count]['stati_port_confirm_date'] = $db_fritzbox5_row['stati_port_confirm_date'];
    $portingFritzbox7490[$fritzbox_7490_count]['tal_ordered_for_date']    = $db_fritzbox5_row['tal_ordered_for_date'];
    $portingFritzbox7490[$fritzbox_7490_count]['tal_order_ack_date']      = $db_fritzbox5_row['tal_order_ack_date'];
    $portingFritzbox7490[$fritzbox_7490_count]['sofortdsl']               = $db_fritzbox5_row['sofortdsl']; 
    $fritzbox_7490_count++;
}

$db_fritzbox6->execute();
$db_fritzbox6_result = $db_fritzbox6->get_result();
while ($db_fritzbox6_row = $db_fritzbox6_result->fetch_assoc()) {
    $newDate = strtotime('-21 days', strtotime($db_fritzbox6_row['stati_port_confirm_date']));
    if($newDate <= $oneDay) {
        $portingFritzbox7490[$fritzbox_7490_count]['id']                      = $db_fritzbox6_row['id'];
        $portingFritzbox7490[$fritzbox_7490_count]['clientid']                = $db_fritzbox6_row['clientid'];
        $portingFritzbox7490[$fritzbox_7490_count]['firstname']               = $db_fritzbox6_row['firstname'];
        $portingFritzbox7490[$fritzbox_7490_count]['lastname']                = $db_fritzbox6_row['lastname'];
        $portingFritzbox7490[$fritzbox_7490_count]['district']                = $db_fritzbox6_row['district'];
        $portingFritzbox7490[$fritzbox_7490_count]['stati_port_confirm_date'] = $db_fritzbox6_row['stati_port_confirm_date'];
        $portingFritzbox7490[$fritzbox_7490_count]['tal_ordered_for_date']    = $db_fritzbox6_row['tal_ordered_for_date'];
        $portingFritzbox7490[$fritzbox_7490_count]['tal_order_ack_date']      = $db_fritzbox6_row['tal_order_ack_date'];
        $portingFritzbox7490[$fritzbox_7490_count]['sofortdsl']               = $db_fritzbox6_row['sofortdsl']; 
        $fritzbox_7490_count++;
    }
}

$db_fritzbox7->execute();
$db_fritzbox7_result = $db_fritzbox7->get_result();
while ($db_fritzbox7_row = $db_fritzbox7_result->fetch_assoc()) {
    if($db_fritzbox7_row['tal_order_ack_date'] != '' && $db_fritzbox7_row['tal_order_ack_date'] != $db_fritzbox7_row['stati_port_confirm_date']) {
        $newDate = strtotime('-21 days', strtotime($db_fritzbox7_row['tal_order_ack_date']));
        if($newDate <= $oneDay) {
            $portingFritzbox7490[$fritzbox_7490_count]['id']                      = $db_fritzbox7_row['id'];
            $portingFritzbox7490[$fritzbox_7490_count]['clientid']                = $db_fritzbox7_row['clientid'];
            $portingFritzbox7490[$fritzbox_7490_count]['firstname']               = $db_fritzbox7_row['firstname'];
            $portingFritzbox7490[$fritzbox_7490_count]['lastname']                = $db_fritzbox7_row['lastname'];
            $portingFritzbox7490[$fritzbox_7490_count]['district']                = $db_fritzbox7_row['district'];
            $portingFritzbox7490[$fritzbox_7490_count]['stati_port_confirm_date'] = $db_fritzbox7_row['stati_port_confirm_date'];
            $portingFritzbox7490[$fritzbox_7490_count]['tal_ordered_for_date']    = $db_fritzbox7_row['tal_ordered_for_date'];
            $portingFritzbox7490[$fritzbox_7490_count]['tal_order_ack_date']      = $db_fritzbox7_row['tal_order_ack_date'];
            $portingFritzbox7490[$fritzbox_7490_count]['sofortdsl']               = $db_fritzbox7_row['sofortdsl']; 
            $fritzbox_7490_count++;
        }
    } elseif ($db_fritzbox7_row['tal_ordered_for_date'] != '' && $db_fritzbox7_row['tal_ordered_for_date'] != $db_fritzbox7_row['stati_port_confirm_date']) {
        $newDate = strtotime('-21 days', strtotime($db_fritzbox7_row['tal_ordered_for_date']));
        if($newDate <= $oneDay) {
            $portingFritzbox7490[$fritzbox_7490_count]['id']                      = $db_fritzbox7_row['id'];
            $portingFritzbox7490[$fritzbox_7490_count]['clientid']                = $db_fritzbox7_row['clientid'];
            $portingFritzbox7490[$fritzbox_7490_count]['firstname']               = $db_fritzbox7_row['firstname'];
            $portingFritzbox7490[$fritzbox_7490_count]['lastname']                = $db_fritzbox7_row['lastname'];
            $portingFritzbox7490[$fritzbox_7490_count]['district']                = $db_fritzbox7_row['district'];
            $portingFritzbox7490[$fritzbox_7490_count]['stati_port_confirm_date'] = $db_fritzbox7_row['stati_port_confirm_date'];
            $portingFritzbox7490[$fritzbox_7490_count]['tal_ordered_for_date']    = $db_fritzbox7_row['tal_ordered_for_date'];
            $portingFritzbox7490[$fritzbox_7490_count]['tal_order_ack_date']      = $db_fritzbox7_row['tal_order_ack_date'];
            $portingFritzbox7490[$fritzbox_7490_count]['sofortdsl']               = $db_fritzbox7_row['sofortdsl']; 
            $fritzbox_7490_count++;
        }
    }
}
  
$db_fritzbox5_result->free_result();
$db_fritzbox6_result->free_result();
$db_fritzbox7_result->free_result();

// 7560
$fritzbox_type = '7560';
$fritzbox_7560_count = 0;

$db_fritzbox5->execute();
$db_fritzbox5_result = $db_fritzbox5->get_result();
while ($db_fritzbox5_row = $db_fritzbox5_result->fetch_assoc()) {
    $portingFritzbox7560[$fritzbox_7560_count]['id']                      = $db_fritzbox5_row['id'];
    $portingFritzbox7560[$fritzbox_7560_count]['clientid']                = $db_fritzbox5_row['clientid'];
    $portingFritzbox7560[$fritzbox_7560_count]['firstname']               = $db_fritzbox5_row['firstname'];
    $portingFritzbox7560[$fritzbox_7560_count]['lastname']                = $db_fritzbox5_row['lastname'];
    $portingFritzbox7560[$fritzbox_7560_count]['district']                = $db_fritzbox5_row['district'];
    $portingFritzbox7560[$fritzbox_7560_count]['stati_port_confirm_date'] = $db_fritzbox5_row['stati_port_confirm_date'];
    $portingFritzbox7560[$fritzbox_7560_count]['tal_ordered_for_date']    = $db_fritzbox5_row['tal_ordered_for_date'];
    $portingFritzbox7560[$fritzbox_7560_count]['tal_order_ack_date']      = $db_fritzbox5_row['tal_order_ack_date'];
    $portingFritzbox7560[$fritzbox_7560_count]['sofortdsl']               = $db_fritzbox5_row['sofortdsl']; 
    $fritzbox_7560_count++;
}

$db_fritzbox6->execute();
$db_fritzbox6_result = $db_fritzbox6->get_result();
while ($db_fritzbox6_row = $db_fritzbox6_result->fetch_assoc()) {
    $newDate = strtotime('-21 days', strtotime($db_fritzbox6_row['stati_port_confirm_date']));
    if($newDate <= $oneDay) {
        $portingFritzbox7560[$fritzbox_7560_count]['id']                      = $db_fritzbox6_row['id'];
        $portingFritzbox7560[$fritzbox_7560_count]['clientid']                = $db_fritzbox6_row['clientid'];
        $portingFritzbox7560[$fritzbox_7560_count]['firstname']               = $db_fritzbox6_row['firstname'];
        $portingFritzbox7560[$fritzbox_7560_count]['lastname']                = $db_fritzbox6_row['lastname'];
        $portingFritzbox7560[$fritzbox_7560_count]['district']                = $db_fritzbox6_row['district'];
        $portingFritzbox7560[$fritzbox_7560_count]['stati_port_confirm_date'] = $db_fritzbox6_row['stati_port_confirm_date'];
        $portingFritzbox7560[$fritzbox_7560_count]['tal_ordered_for_date']    = $db_fritzbox6_row['tal_ordered_for_date'];
        $portingFritzbox7560[$fritzbox_7560_count]['tal_order_ack_date']      = $db_fritzbox6_row['tal_order_ack_date'];
        $portingFritzbox7560[$fritzbox_7560_count]['sofortdsl']               = $db_fritzbox6_row['sofortdsl']; 
        $fritzbox_7560_count++;
    }
}

$db_fritzbox7->execute();
$db_fritzbox7_result = $db_fritzbox7->get_result();
while ($db_fritzbox7_row = $db_fritzbox7_result->fetch_assoc()) {
    if($db_fritzbox7_row['tal_order_ack_date'] != '' && $db_fritzbox7_row['tal_order_ack_date'] != $db_fritzbox7_row['stati_port_confirm_date']) {
        $newDate = strtotime('-21 days', strtotime($db_fritzbox7_row['tal_order_ack_date']));
        if($newDate <= $oneDay) {
            $portingFritzbox7560[$fritzbox_7560_count]['id']                      = $db_fritzbox7_row['id'];
            $portingFritzbox7560[$fritzbox_7560_count]['clientid']                = $db_fritzbox7_row['clientid'];
            $portingFritzbox7560[$fritzbox_7560_count]['firstname']               = $db_fritzbox7_row['firstname'];
            $portingFritzbox7560[$fritzbox_7560_count]['lastname']                = $db_fritzbox7_row['lastname'];
            $portingFritzbox7560[$fritzbox_7560_count]['district']                = $db_fritzbox7_row['district'];
            $portingFritzbox7560[$fritzbox_7560_count]['stati_port_confirm_date'] = $db_fritzbox7_row['stati_port_confirm_date'];
            $portingFritzbox7560[$fritzbox_7560_count]['tal_ordered_for_date']    = $db_fritzbox7_row['tal_ordered_for_date'];
            $portingFritzbox7560[$fritzbox_7560_count]['tal_order_ack_date']      = $db_fritzbox7_row['tal_order_ack_date'];
            $portingFritzbox7560[$fritzbox_7560_count]['sofortdsl']               = $db_fritzbox7_row['sofortdsl']; 
            $fritzbox_7560_count++;
        }
    } elseif ($db_fritzbox7_row['tal_ordered_for_date'] != '' && $db_fritzbox7_row['tal_ordered_for_date'] != $db_fritzbox7_row['stati_port_confirm_date']) {
        $newDate = strtotime('-21 days', strtotime($db_fritzbox7_row['tal_ordered_for_date']));
        if($newDate <= $oneDay) {
            $portingFritzbox7560[$fritzbox_7560_count]['id']                      = $db_fritzbox7_row['id'];
            $portingFritzbox7560[$fritzbox_7560_count]['clientid']                = $db_fritzbox7_row['clientid'];
            $portingFritzbox7560[$fritzbox_7560_count]['firstname']               = $db_fritzbox7_row['firstname'];
            $portingFritzbox7560[$fritzbox_7560_count]['lastname']                = $db_fritzbox7_row['lastname'];
            $portingFritzbox7560[$fritzbox_7560_count]['district']                = $db_fritzbox7_row['district'];
            $portingFritzbox7560[$fritzbox_7560_count]['stati_port_confirm_date'] = $db_fritzbox7_row['stati_port_confirm_date'];
            $portingFritzbox7560[$fritzbox_7560_count]['tal_ordered_for_date']    = $db_fritzbox7_row['tal_ordered_for_date'];
            $portingFritzbox7560[$fritzbox_7560_count]['tal_order_ack_date']      = $db_fritzbox7_row['tal_order_ack_date'];
            $portingFritzbox7560[$fritzbox_7560_count]['sofortdsl']               = $db_fritzbox7_row['sofortdsl']; 
            $fritzbox_7560_count++;
        }
    }
}
  
$db_fritzbox5_result->free_result();
$db_fritzbox6_result->free_result();
$db_fritzbox7_result->free_result();

// 7590
$fritzbox_type = '7590';
$fritzbox_7590_count = 0;

$db_fritzbox5->execute();
$db_fritzbox5_result = $db_fritzbox5->get_result();
while ($db_fritzbox5_row = $db_fritzbox5_result->fetch_assoc()) {
    $portingFritzbox7590[$fritzbox_7590_count]['id']                      = $db_fritzbox5_row['id'];
    $portingFritzbox7590[$fritzbox_7590_count]['clientid']                = $db_fritzbox5_row['clientid'];
    $portingFritzbox7590[$fritzbox_7590_count]['firstname']               = $db_fritzbox5_row['firstname'];
    $portingFritzbox7590[$fritzbox_7590_count]['lastname']                = $db_fritzbox5_row['lastname'];
    $portingFritzbox7590[$fritzbox_7590_count]['district']                = $db_fritzbox5_row['district'];
    $portingFritzbox7590[$fritzbox_7590_count]['stati_port_confirm_date'] = $db_fritzbox5_row['stati_port_confirm_date'];
    $portingFritzbox7590[$fritzbox_7590_count]['tal_ordered_for_date']    = $db_fritzbox5_row['tal_ordered_for_date'];
    $portingFritzbox7590[$fritzbox_7590_count]['tal_order_ack_date']      = $db_fritzbox5_row['tal_order_ack_date'];
    $portingFritzbox7590[$fritzbox_7590_count]['sofortdsl']               = $db_fritzbox5_row['sofortdsl']; 
    $fritzbox_7590_count++;
}

$db_fritzbox6->execute();
$db_fritzbox6_result = $db_fritzbox6->get_result();
while ($db_fritzbox6_row = $db_fritzbox6_result->fetch_assoc()) {
    $newDate = strtotime('-21 days', strtotime($db_fritzbox6_row['stati_port_confirm_date']));
    if($newDate <= $oneDay) {
        $portingFritzbox7590[$fritzbox_7590_count]['id']                      = $db_fritzbox6_row['id'];
        $portingFritzbox7590[$fritzbox_7590_count]['clientid']                = $db_fritzbox6_row['clientid'];
        $portingFritzbox7590[$fritzbox_7590_count]['firstname']               = $db_fritzbox6_row['firstname'];
        $portingFritzbox7590[$fritzbox_7590_count]['lastname']                = $db_fritzbox6_row['lastname'];
        $portingFritzbox7590[$fritzbox_7590_count]['district']                = $db_fritzbox6_row['district'];
        $portingFritzbox7590[$fritzbox_7590_count]['stati_port_confirm_date'] = $db_fritzbox6_row['stati_port_confirm_date'];
        $portingFritzbox7590[$fritzbox_7590_count]['tal_ordered_for_date']    = $db_fritzbox6_row['tal_ordered_for_date'];
        $portingFritzbox7590[$fritzbox_7590_count]['tal_order_ack_date']      = $db_fritzbox6_row['tal_order_ack_date'];
        $portingFritzbox7590[$fritzbox_7590_count]['sofortdsl']               = $db_fritzbox6_row['sofortdsl']; 
        $fritzbox_7590_count++;
    }
}

$db_fritzbox7->execute();
$db_fritzbox7_result = $db_fritzbox7->get_result();
while ($db_fritzbox7_row = $db_fritzbox7_result->fetch_assoc()) {
    if($db_fritzbox7_row['tal_order_ack_date'] != '' && $db_fritzbox7_row['tal_order_ack_date'] != $db_fritzbox7_row['stati_port_confirm_date']) {
        $newDate = strtotime('-21 days', strtotime($db_fritzbox7_row['tal_order_ack_date']));
        if($newDate <= $oneDay) {
            $portingFritzbox7590[$fritzbox_7590_count]['id']                      = $db_fritzbox7_row['id'];
            $portingFritzbox7590[$fritzbox_7590_count]['clientid']                = $db_fritzbox7_row['clientid'];
            $portingFritzbox7590[$fritzbox_7590_count]['firstname']               = $db_fritzbox7_row['firstname'];
            $portingFritzbox7590[$fritzbox_7590_count]['lastname']                = $db_fritzbox7_row['lastname'];
            $portingFritzbox7590[$fritzbox_7590_count]['district']                = $db_fritzbox7_row['district'];
            $portingFritzbox7590[$fritzbox_7590_count]['stati_port_confirm_date'] = $db_fritzbox7_row['stati_port_confirm_date'];
            $portingFritzbox7590[$fritzbox_7590_count]['tal_ordered_for_date']    = $db_fritzbox7_row['tal_ordered_for_date'];
            $portingFritzbox7590[$fritzbox_7590_count]['tal_order_ack_date']      = $db_fritzbox7_row['tal_order_ack_date'];
            $portingFritzbox7590[$fritzbox_7590_count]['sofortdsl']               = $db_fritzbox7_row['sofortdsl']; 
            $fritzbox_7590_count++;
        }
    } elseif ($db_fritzbox7_row['tal_ordered_for_date'] != '' && $db_fritzbox7_row['tal_ordered_for_date'] != $db_fritzbox7_row['stati_port_confirm_date']) {
        $newDate = strtotime('-21 days', strtotime($db_fritzbox7_row['tal_ordered_for_date']));
        if($newDate <= $oneDay) {
            $portingFritzbox7590[$fritzbox_7590_count]['id']                      = $db_fritzbox7_row['id'];
            $portingFritzbox7590[$fritzbox_7590_count]['clientid']                = $db_fritzbox7_row['clientid'];
            $portingFritzbox7590[$fritzbox_7590_count]['firstname']               = $db_fritzbox7_row['firstname'];
            $portingFritzbox7590[$fritzbox_7590_count]['lastname']                = $db_fritzbox7_row['lastname'];
            $portingFritzbox7590[$fritzbox_7590_count]['district']                = $db_fritzbox7_row['district'];
            $portingFritzbox7590[$fritzbox_7590_count]['stati_port_confirm_date'] = $db_fritzbox7_row['stati_port_confirm_date'];
            $portingFritzbox7590[$fritzbox_7590_count]['tal_ordered_for_date']    = $db_fritzbox7_row['tal_ordered_for_date'];
            $portingFritzbox7590[$fritzbox_7590_count]['tal_order_ack_date']      = $db_fritzbox7_row['tal_order_ack_date'];
            $portingFritzbox7590[$fritzbox_7590_count]['sofortdsl']               = $db_fritzbox7_row['sofortdsl']; 
            $fritzbox_7590_count++;
        }
    }
}
  
$db_fritzbox5_result->free_result();
$db_fritzbox6_result->free_result();
$db_fritzbox7_result->free_result();

// Gateway 400
$fritzbox_type = 'GW 400';
$fritzbox_GW_400_count = 0;

$db_fritzbox5->execute();
$db_fritzbox5_result = $db_fritzbox5->get_result();
while ($db_fritzbox5_row = $db_fritzbox5_result->fetch_assoc()) {
    $portingFritzboxGW_400[$fritzbox_GW_400_count]['id']                      = $db_fritzbox5_row['id'];
    $portingFritzboxGW_400[$fritzbox_GW_400_count]['clientid']                = $db_fritzbox5_row['clientid'];
    $portingFritzboxGW_400[$fritzbox_GW_400_count]['firstname']               = $db_fritzbox5_row['firstname'];
    $portingFritzboxGW_400[$fritzbox_GW_400_count]['lastname']                = $db_fritzbox5_row['lastname'];
    $portingFritzboxGW_400[$fritzbox_GW_400_count]['district']                = $db_fritzbox5_row['district'];
    $portingFritzboxGW_400[$fritzbox_GW_400_count]['stati_port_confirm_date'] = $db_fritzbox5_row['stati_port_confirm_date'];
    $portingFritzboxGW_400[$fritzbox_GW_400_count]['tal_ordered_for_date']    = $db_fritzbox5_row['tal_ordered_for_date'];
    $portingFritzboxGW_400[$fritzbox_GW_400_count]['tal_order_ack_date']      = $db_fritzbox5_row['tal_order_ack_date'];
    $portingFritzboxGW_400[$fritzbox_GW_400_count]['sofortdsl']               = $db_fritzbox5_row['sofortdsl']; 
    $fritzbox_GW_400_count++;
}

$db_fritzbox6->execute();
$db_fritzbox6_result = $db_fritzbox6->get_result();
while ($db_fritzbox6_row = $db_fritzbox6_result->fetch_assoc()) {
    $newDate = strtotime('-21 days', strtotime($db_fritzbox6_row['stati_port_confirm_date']));
    if($newDate <= $oneDay) {
        $portingFritzboxGW_400[$fritzbox_GW_400_count]['id']                      = $db_fritzbox6_row['id'];
        $portingFritzboxGW_400[$fritzbox_GW_400_count]['clientid']                = $db_fritzbox6_row['clientid'];
        $portingFritzboxGW_400[$fritzbox_GW_400_count]['firstname']               = $db_fritzbox6_row['firstname'];
        $portingFritzboxGW_400[$fritzbox_GW_400_count]['lastname']                = $db_fritzbox6_row['lastname'];
        $portingFritzboxGW_400[$fritzbox_GW_400_count]['district']                = $db_fritzbox6_row['district'];
        $portingFritzboxGW_400[$fritzbox_GW_400_count]['stati_port_confirm_date'] = $db_fritzbox6_row['stati_port_confirm_date'];
        $portingFritzboxGW_400[$fritzbox_GW_400_count]['tal_ordered_for_date']    = $db_fritzbox6_row['tal_ordered_for_date'];
        $portingFritzboxGW_400[$fritzbox_GW_400_count]['tal_order_ack_date']      = $db_fritzbox6_row['tal_order_ack_date'];
        $portingFritzboxGW_400[$fritzbox_GW_400_count]['sofortdsl']               = $db_fritzbox6_row['sofortdsl']; 
        $fritzbox_GW_400_count++;
    }
}

$db_fritzbox7->execute();
$db_fritzbox7_result = $db_fritzbox7->get_result();
while ($db_fritzbox7_row = $db_fritzbox7_result->fetch_assoc()) {
    if($db_fritzbox7_row['tal_order_ack_date'] != '' && $db_fritzbox7_row['tal_order_ack_date'] != $db_fritzbox7_row['stati_port_confirm_date']) {
        $newDate = strtotime('-21 days', strtotime($db_fritzbox7_row['tal_order_ack_date']));
        if($newDate <= $oneDay) {
            $portingFritzboxGW_400[$fritzbox_GW_400_count]['id']                      = $db_fritzbox7_row['id'];
            $portingFritzboxGW_400[$fritzbox_GW_400_count]['clientid']                = $db_fritzbox7_row['clientid'];
            $portingFritzboxGW_400[$fritzbox_GW_400_count]['firstname']               = $db_fritzbox7_row['firstname'];
            $portingFritzboxGW_400[$fritzbox_GW_400_count]['lastname']                = $db_fritzbox7_row['lastname'];
            $portingFritzboxGW_400[$fritzbox_GW_400_count]['district']                = $db_fritzbox7_row['district'];
            $portingFritzboxGW_400[$fritzbox_GW_400_count]['stati_port_confirm_date'] = $db_fritzbox7_row['stati_port_confirm_date'];
            $portingFritzboxGW_400[$fritzbox_GW_400_count]['tal_ordered_for_date']    = $db_fritzbox7_row['tal_ordered_for_date'];
            $portingFritzboxGW_400[$fritzbox_GW_400_count]['tal_order_ack_date']      = $db_fritzbox7_row['tal_order_ack_date'];
            $portingFritzboxGW_400[$fritzbox_GW_400_count]['sofortdsl']               = $db_fritzbox7_row['sofortdsl']; 
            $fritzbox_GW_400_count++;
        }
    } elseif ($db_fritzbox7_row['tal_ordered_for_date'] != '' && $db_fritzbox7_row['tal_ordered_for_date'] != $db_fritzbox7_row['stati_port_confirm_date']) {
        $newDate = strtotime('-21 days', strtotime($db_fritzbox7_row['tal_ordered_for_date']));
        if($newDate <= $oneDay) {
            $portingFritzboxGW_400[$fritzbox_GW_400_count]['id']                      = $db_fritzbox7_row['id'];
            $portingFritzboxGW_400[$fritzbox_GW_400_count]['clientid']                = $db_fritzbox7_row['clientid'];
            $portingFritzboxGW_400[$fritzbox_GW_400_count]['firstname']               = $db_fritzbox7_row['firstname'];
            $portingFritzboxGW_400[$fritzbox_GW_400_count]['lastname']                = $db_fritzbox7_row['lastname'];
            $portingFritzboxGW_400[$fritzbox_GW_400_count]['district']                = $db_fritzbox7_row['district'];
            $portingFritzboxGW_400[$fritzbox_GW_400_count]['stati_port_confirm_date'] = $db_fritzbox7_row['stati_port_confirm_date'];
            $portingFritzboxGW_400[$fritzbox_GW_400_count]['tal_ordered_for_date']    = $db_fritzbox7_row['tal_ordered_for_date'];
            $portingFritzboxGW_400[$fritzbox_GW_400_count]['tal_order_ack_date']      = $db_fritzbox7_row['tal_order_ack_date'];
            $portingFritzboxGW_400[$fritzbox_GW_400_count]['sofortdsl']               = $db_fritzbox7_row['sofortdsl']; 
            $fritzbox_GW_400_count++;
        }
    }
}
  
$db_fritzbox5_result->free_result();
$db_fritzbox6_result->free_result();
$db_fritzbox7_result->free_result();

// INNBOX G2426A
$fritzbox_type = 'IBG2426A';
$fritzbox_IBG2426A_count = 0;

$db_fritzbox5->execute();
$db_fritzbox5_result = $db_fritzbox5->get_result();
while ($db_fritzbox5_row = $db_fritzbox5_result->fetch_assoc()) {
    $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['id']                      = $db_fritzbox5_row['id'];
    $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['clientid']                = $db_fritzbox5_row['clientid'];
    $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['firstname']               = $db_fritzbox5_row['firstname'];
    $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['lastname']                = $db_fritzbox5_row['lastname'];
    $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['district']                = $db_fritzbox5_row['district'];
    $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['stati_port_confirm_date'] = $db_fritzbox5_row['stati_port_confirm_date'];
    $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['tal_ordered_for_date']    = $db_fritzbox5_row['tal_ordered_for_date'];
    $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['tal_order_ack_date']      = $db_fritzbox5_row['tal_order_ack_date'];
    $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['sofortdsl']               = $db_fritzbox5_row['sofortdsl']; 
    $fritzbox_IBG2426A_count++;
}

$db_fritzbox6->execute();
$db_fritzbox6_result = $db_fritzbox6->get_result();
while ($db_fritzbox6_row = $db_fritzbox6_result->fetch_assoc()) {
    $newDate = strtotime('-21 days', strtotime($db_fritzbox6_row['stati_port_confirm_date']));
    if($newDate <= $oneDay) {
        $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['id']                      = $db_fritzbox6_row['id'];
        $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['clientid']                = $db_fritzbox6_row['clientid'];
        $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['firstname']               = $db_fritzbox6_row['firstname'];
        $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['lastname']                = $db_fritzbox6_row['lastname'];
        $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['district']                = $db_fritzbox6_row['district'];
        $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['stati_port_confirm_date'] = $db_fritzbox6_row['stati_port_confirm_date'];
        $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['tal_ordered_for_date']    = $db_fritzbox6_row['tal_ordered_for_date'];
        $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['tal_order_ack_date']      = $db_fritzbox6_row['tal_order_ack_date'];
        $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['sofortdsl']               = $db_fritzbox6_row['sofortdsl']; 
        $fritzbox_IBG2426A_count++;
    }
}

$db_fritzbox7->execute();
$db_fritzbox7_result = $db_fritzbox7->get_result();
while ($db_fritzbox7_row = $db_fritzbox7_result->fetch_assoc()) {
    if($db_fritzbox7_row['tal_order_ack_date'] != '' && $db_fritzbox7_row['tal_order_ack_date'] != $db_fritzbox7_row['stati_port_confirm_date']) {
        $newDate = strtotime('-21 days', strtotime($db_fritzbox7_row['tal_order_ack_date']));
        if($newDate <= $oneDay) {
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['id']                      = $db_fritzbox7_row['id'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['clientid']                = $db_fritzbox7_row['clientid'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['firstname']               = $db_fritzbox7_row['firstname'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['lastname']                = $db_fritzbox7_row['lastname'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['district']                = $db_fritzbox7_row['district'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['stati_port_confirm_date'] = $db_fritzbox7_row['stati_port_confirm_date'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['tal_ordered_for_date']    = $db_fritzbox7_row['tal_ordered_for_date'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['tal_order_ack_date']      = $db_fritzbox7_row['tal_order_ack_date'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['sofortdsl']               = $db_fritzbox7_row['sofortdsl']; 
            $fritzbox_IBG2426A_count++;
        }
    } elseif ($db_fritzbox7_row['tal_ordered_for_date'] != '' && $db_fritzbox7_row['tal_ordered_for_date'] != $db_fritzbox7_row['stati_port_confirm_date']) {
        $newDate = strtotime('-21 days', strtotime($db_fritzbox7_row['tal_ordered_for_date']));
        if($newDate <= $oneDay) {
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['id']                      = $db_fritzbox7_row['id'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['clientid']                = $db_fritzbox7_row['clientid'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['firstname']               = $db_fritzbox7_row['firstname'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['lastname']                = $db_fritzbox7_row['lastname'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['district']                = $db_fritzbox7_row['district'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['stati_port_confirm_date'] = $db_fritzbox7_row['stati_port_confirm_date'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['tal_ordered_for_date']    = $db_fritzbox7_row['tal_ordered_for_date'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['tal_order_ack_date']      = $db_fritzbox7_row['tal_order_ack_date'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['sofortdsl']               = $db_fritzbox7_row['sofortdsl']; 
            $fritzbox_IBG2426A_count++;
        }
    }
}
  
$db_fritzbox5_result->free_result();
$db_fritzbox6_result->free_result();
$db_fritzbox7_result->free_result();

// IBG2426A
$fritzbox_type = 'IBG2426A';
$fritzbox_IBG2426A_count = 0;

$db_fritzbox5->execute();
$db_fritzbox5_result = $db_fritzbox5->get_result();
while ($db_fritzbox5_row = $db_fritzbox5_result->fetch_assoc()) {
    $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['id']                      = $db_fritzbox5_row['id'];
    $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['clientid']                = $db_fritzbox5_row['clientid'];
    $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['firstname']               = $db_fritzbox5_row['firstname'];
    $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['lastname']                = $db_fritzbox5_row['lastname'];
    $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['district']                = $db_fritzbox5_row['district'];
    $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['stati_port_confirm_date'] = $db_fritzbox5_row['stati_port_confirm_date'];
    $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['tal_ordered_for_date']    = $db_fritzbox5_row['tal_ordered_for_date'];
    $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['tal_order_ack_date']      = $db_fritzbox5_row['tal_order_ack_date'];
    $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['sofortdsl']               = $db_fritzbox5_row['sofortdsl']; 
    $fritzbox_IBG2426A_count++;
}

$db_fritzbox6->execute();
$db_fritzbox6_result = $db_fritzbox6->get_result();
while ($db_fritzbox6_row = $db_fritzbox6_result->fetch_assoc()) {
    $newDate = strtotime('-21 days', strtotime($db_fritzbox6_row['stati_port_confirm_date']));
    if($newDate <= $oneDay) {
        $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['id']                      = $db_fritzbox6_row['id'];
        $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['clientid']                = $db_fritzbox6_row['clientid'];
        $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['firstname']               = $db_fritzbox6_row['firstname'];
        $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['lastname']                = $db_fritzbox6_row['lastname'];
        $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['district']                = $db_fritzbox6_row['district'];
        $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['stati_port_confirm_date'] = $db_fritzbox6_row['stati_port_confirm_date'];
        $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['tal_ordered_for_date']    = $db_fritzbox6_row['tal_ordered_for_date'];
        $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['tal_order_ack_date']      = $db_fritzbox6_row['tal_order_ack_date'];
        $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['sofortdsl']               = $db_fritzbox6_row['sofortdsl']; 
        $fritzbox_IBG2426A_count++;
    }
}

$db_fritzbox7->execute();
$db_fritzbox7_result = $db_fritzbox7->get_result();
while ($db_fritzbox7_row = $db_fritzbox7_result->fetch_assoc()) {
    if($db_fritzbox7_row['tal_order_ack_date'] != '' && $db_fritzbox7_row['tal_order_ack_date'] != $db_fritzbox7_row['stati_port_confirm_date']) {
        $newDate = strtotime('-21 days', strtotime($db_fritzbox7_row['tal_order_ack_date']));
        if($newDate <= $oneDay) {
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['id']                      = $db_fritzbox7_row['id'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['clientid']                = $db_fritzbox7_row['clientid'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['firstname']               = $db_fritzbox7_row['firstname'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['lastname']                = $db_fritzbox7_row['lastname'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['district']                = $db_fritzbox7_row['district'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['stati_port_confirm_date'] = $db_fritzbox7_row['stati_port_confirm_date'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['tal_ordered_for_date']    = $db_fritzbox7_row['tal_ordered_for_date'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['tal_order_ack_date']      = $db_fritzbox7_row['tal_order_ack_date'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['sofortdsl']               = $db_fritzbox7_row['sofortdsl']; 
            $fritzbox_IBG2426A_count++;
        }
    } elseif ($db_fritzbox7_row['tal_ordered_for_date'] != '' && $db_fritzbox7_row['tal_ordered_for_date'] != $db_fritzbox7_row['stati_port_confirm_date']) {
        $newDate = strtotime('-21 days', strtotime($db_fritzbox7_row['tal_ordered_for_date']));
        if($newDate <= $oneDay) {
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['id']                      = $db_fritzbox7_row['id'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['clientid']                = $db_fritzbox7_row['clientid'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['firstname']               = $db_fritzbox7_row['firstname'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['lastname']                = $db_fritzbox7_row['lastname'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['district']                = $db_fritzbox7_row['district'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['stati_port_confirm_date'] = $db_fritzbox7_row['stati_port_confirm_date'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['tal_ordered_for_date']    = $db_fritzbox7_row['tal_ordered_for_date'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['tal_order_ack_date']      = $db_fritzbox7_row['tal_order_ack_date'];
            $portingFritzboxIBG2426A[$fritzbox_IBG2426A_count]['sofortdsl']               = $db_fritzbox7_row['sofortdsl']; 
            $fritzbox_IBG2426A_count++;
        }
    }
}
  
$db_fritzbox5_result->free_result();
$db_fritzbox6_result->free_result();
$db_fritzbox7_result->free_result();

// unbekannter FB-Typ
$fritzbox_type = '';
$fritzbox_NoType_count = 0;
  
$db_fritzbox8->execute();
$db_fritzbox8_result = $db_fritzbox8->get_result();

while ($db_fritzbox8_row = $db_fritzbox8_result->fetch_assoc()) {
    if($db_fritzbox8_row['tal_order_ack_date'] != '' && $db_fritzbox8_row['tal_order_ack_date'] != $db_fritzbox8_row['stati_port_confirm_date']) {
        $newDate = strtotime('-21 days', strtotime($db_fritzbox8_row['tal_order_ack_date']));
        if($newDate <= $oneDay) {
            $portingFritzboxNoType[$fritzbox_NoType_count]['id']                      = $db_fritzbox8_row['id'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['clientid']                = $db_fritzbox8_row['clientid'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['firstname']               = $db_fritzbox8_row['firstname'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['lastname']                = $db_fritzbox8_row['lastname'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['district']                = $db_fritzbox8_row['district'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['stati_port_confirm_date'] = $db_fritzbox8_row['stati_port_confirm_date'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['tal_ordered_for_date']    = $db_fritzbox8_row['tal_ordered_for_date'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['tal_order_ack_date']      = $db_fritzbox8_row['tal_order_ack_date'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['sofortdsl']               = $db_fritzbox8_row['sofortdsl']; 
            $fritzbox_NoType_count++;
        }
    } else if ($db_fritzbox8_row['tal_ordered_for_date'] != '' && $db_fritzbox8_row['tal_ordered_for_date'] != $db_fritzbox8_row['stati_port_confirm_date']) {
        $newDate = strtotime('-21 days', strtotime($db_fritzbox8_row['tal_ordered_for_date']));
        if($newDate <= $oneDay) {
            $portingFritzboxNoType[$fritzbox_NoType_count]['id']                      = $db_fritzbox8_row['id'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['clientid']                = $db_fritzbox8_row['clientid'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['firstname']               = $db_fritzbox8_row['firstname'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['lastname']                = $db_fritzbox8_row['lastname'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['district']                = $db_fritzbox8_row['district'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['stati_port_confirm_date'] = $db_fritzbox8_row['stati_port_confirm_date'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['tal_ordered_for_date']    = $db_fritzbox8_row['tal_ordered_for_date'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['tal_order_ack_date']      = $db_fritzbox8_row['tal_order_ack_date'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['sofortdsl']               = $db_fritzbox8_row['sofortdsl']; 
            $fritzbox_NoType_count++;
        }
    }
}

$db_fritzbox9->execute();
$db_fritzbox9_result = $db_fritzbox9->get_result();

while ($db_fritzbox9_row = $db_fritzbox9_result->fetch_assoc()) {
    if($db_fritzbox9_row['sofortdsl'] == 'Ja') {
        $portingFritzboxNoType[$fritzbox_NoType_count]['id']                      = $db_fritzbox9_row['id'];
        $portingFritzboxNoType[$fritzbox_NoType_count]['clientid']                = $db_fritzbox9_row['clientid'];
        $portingFritzboxNoType[$fritzbox_NoType_count]['firstname']               = $db_fritzbox9_row['firstname'];
        $portingFritzboxNoType[$fritzbox_NoType_count]['lastname']                = $db_fritzbox9_row['lastname'];
        $portingFritzboxNoType[$fritzbox_NoType_count]['district']                = $db_fritzbox9_row['district'];
        $portingFritzboxNoType[$fritzbox_NoType_count]['stati_port_confirm_date'] = $db_fritzbox9_row['stati_port_confirm_date'];
        $portingFritzboxNoType[$fritzbox_NoType_count]['tal_ordered_for_date']    = $db_fritzbox9_row['tal_ordered_for_date'];
        $portingFritzboxNoType[$fritzbox_NoType_count]['tal_order_ack_date']      = $db_fritzbox9_row['tal_order_ack_date'];
        $portingFritzboxNoType[$fritzbox_NoType_count]['sofortdsl']               = $db_fritzbox9_row['sofortdsl']; 
        $fritzbox_NoType_count++;
    } else if ($db_fritzbox9_row['stati_port_confirm_date'] != '') {
        $newDate = strtotime('-21 days', strtotime($db_fritzbox8_row['stati_port_confirm_date']));
        if($newDate <= $oneDay) {
            $portingFritzboxNoType[$fritzbox_NoType_count]['id']                      = $db_fritzbox9_row['id'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['clientid']                = $db_fritzbox9_row['clientid'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['firstname']               = $db_fritzbox9_row['firstname'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['lastname']                = $db_fritzbox9_row['lastname'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['district']                = $db_fritzbox9_row['district'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['stati_port_confirm_date'] = $db_fritzbox9_row['stati_port_confirm_date'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['tal_ordered_for_date']    = $db_fritzbox9_row['tal_ordered_for_date'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['tal_order_ack_date']      = $db_fritzbox9_row['tal_order_ack_date'];
            $portingFritzboxNoType[$fritzbox_NoType_count]['sofortdsl']               = $db_fritzbox9_row['sofortdsl']; 
            $fritzbox_NoType_count++;
        }
    }
}
 
$db_fritzbox8_result->free_result();
$db_fritzbox9_result->free_result();


// close
$db_fritzbox1->close();
$db_fritzbox2->close();
$db_fritzbox3->close();
$db_fritzbox4->close();
$db_fritzbox5->close();
$db_fritzbox6->close();
$db_fritzbox7->close();
$db_fritzbox8->close();
$db_fritzbox9->close();
 
// Summe Fritzboxen
$fritzbox_gesamt = $fritzbox_6320_count + $fritzbox_6360_count + $fritzbox_6490_count + $fritzbox_7270_count + $fritzbox_7360_count + $fritzbox_7390_count + $fritzbox_7490_count + $fritzbox_7560_count + $fritzbox_7590_count + $fritzbox_GW_400_count + $fritzbox_IBG2426A_count + $fritzbox_NoType_count;
  
// Netzwerke
// Patchen
$db_net_patchen = $db->query( $sql['net_patchen']);
$net_patchen_count = 0;
while ($db_net_patchen_row = $db_net_patchen->fetch_assoc()) {
    $newDate = strtotime('-8 days', strtotime($db_net_patchen_row['tal_order_ack_date']));
    if($newDate <= $oneDay || $db_net_patchen_row['sofortdsl'] == 'Ja') {
        $portingNetPatch[$net_patchen_count]['id']                   = $db_net_patchen_row['id'];
        $portingNetPatch[$net_patchen_count]['clientid']             = $db_net_patchen_row['clientid'];
        $portingNetPatch[$net_patchen_count]['firstname']            = $db_net_patchen_row['firstname'];
        $portingNetPatch[$net_patchen_count]['lastname']             = $db_net_patchen_row['lastname'];
        $portingNetPatch[$net_patchen_count]['district']             = $db_net_patchen_row['district'];
        $portingNetPatch[$net_patchen_count]['sofortdsl']            = $db_net_patchen_row['sofortdsl'];
        $portingNetPatch[$net_patchen_count]['tal_ordered_for_date'] = $db_net_patchen_row['tal_ordered_for_date'];
        $net_patchen_count++;
    }
}
$db_net_patchen->free();
 
// CMTS einrichten
$db_net_cmts = $db->query( $sql['net_cmts']);
$net_cmts_count = 0;
while ($db_net_cmts_row = $db_net_cmts->fetch_assoc()) {
    $newDate = strtotime('-14 days', strtotime($db_net_cmts_row['tal_order_ack_date']));
//    if($newDate <= $oneDay || $db_net_cmts_row['sofortdsl'] == 'Ja') {
    if($newDate <= $oneDay ) {
        $portingNetDslamCmts[$net_cmts_count]['id']                 = $db_net_cmts_row['id'];
        $portingNetDslamCmts[$net_cmts_count]['clientid']           = $db_net_cmts_row['clientid'];
        $portingNetDslamCmts[$net_cmts_count]['firstname']          = $db_net_cmts_row['firstname'];
        $portingNetDslamCmts[$net_cmts_count]['lastname']           = $db_net_cmts_row['lastname'];
        $portingNetDslamCmts[$net_cmts_count]['district']           = $db_net_cmts_row['district'];
        $portingNetDslamCmts[$net_cmts_count]['sofortdsl']          = $db_net_cmts_row['sofortdsl'];
        $portingNetDslamCmts[$net_cmts_count]['tal_order_ack_date'] = $db_net_cmts_row['tal_order_ack_date'];
        $net_cmts_count++;
    }
}
$db_net_cmts->free();

// PPPoE einrichten
$db_net_pppoe = $db->query( $sql['net_pppoe']);
$net_pppoe_count = 0;
while ($db_net_pppoe_row = $db_net_pppoe->fetch_assoc()) {
    $newDate = strtotime('-14 days', strtotime($db_net_pppoe_row['tal_order_ack_date']));
//    if($newDate <= $oneDay || $db_net_pppoe_row['sofortdsl'] == 'Ja') {
    if($newDate <= $oneDay  ) {
        $portingNetPppoe[$net_pppoe_count]['id']                 = $db_net_pppoe_row['id'];
        $portingNetPppoe[$net_pppoe_count]['clientid']           = $db_net_pppoe_row['clientid'];
        $portingNetPppoe[$net_pppoe_count]['firstname']          = $db_net_pppoe_row['firstname'];
        $portingNetPppoe[$net_pppoe_count]['lastname']           = $db_net_pppoe_row['lastname'];
        $portingNetPppoe[$net_pppoe_count]['district']           = $db_net_pppoe_row['district'];
        $portingNetPppoe[$net_pppoe_count]['sofortdsl']          = $db_net_pppoe_row['sofortdsl'];
        $portingNetPppoe[$net_pppoe_count]['tal_order_ack_date'] = $db_net_pppoe_row['tal_order_ack_date'];
        $net_pppoe_count++;
    }
}
$db_net_pppoe->free();

// Aktiv schalten
$db_net_active = $db->query( $sql['net_active']);
$net_active_count = 0;
while ($db_net_active_row = $db_net_active->fetch_assoc()) {
    $portingNetActive[$net_active_count]['id']                         = $db_net_active_row['id'];
    $portingNetActive[$net_active_count]['clientid']                   = $db_net_active_row['clientid'];
    $portingNetActive[$net_active_count]['firstname']                  = $db_net_active_row['firstname'];
    $portingNetActive[$net_active_count]['lastname']                   = $db_net_active_row['lastname'];
    $portingNetActive[$net_active_count]['district']                   = $db_net_active_row['district'];
    $portingNetActive[$net_active_count]['connection_activation_date'] = $db_net_active_row['connection_activation_date'];
    $portingNetActive[$net_active_count]['tal_order_ack_date']         = $db_net_active_row['tal_order_ack_date'];
    $portingNetActive[$net_active_count]['connection_wish_date']       = $db_net_active_row['connection_wish_date'];
    $net_active_count++;
}

// Aktiv schalten Glasfaser
$db_net_gf_active = $db->query( $sql['gf_configure']);
$net_gf_active_count = 0;
while ($db_net_gf_active_row = $db_net_gf_active->fetch_assoc()) {
    $portingNetGfActive[$net_gf_active_count]['id']                         = $db_net_gf_active_row['id'];
    $portingNetGfActive[$net_gf_active_count]['clientid']                   = $db_net_gf_active_row['clientid'];
    $portingNetGfActive[$net_gf_active_count]['firstname']                  = $db_net_gf_active_row['firstname'];
    $portingNetGfActive[$net_gf_active_count]['lastname']                   = $db_net_gf_active_row['lastname'];
    $portingNetGfActive[$net_gf_active_count]['district']                   = $db_net_gf_active_row['district'];
    $portingNetGfActive[$net_gf_active_count]['connection_activation_date'] = $db_net_gf_active_row['connection_activation_date'];
    $portingNetGfActive[$net_gf_active_count]['connection_wish_date']       = $db_net_gf_active_row['connection_wish_date'];
    $net_gf_active_count++;
}

// Vertragsdaten
// Kündigung eingegangen
$db_contruct_cancel = $db->query( $sql['contruct_cancel_input']);
$contruct_cancel_count = 0;
while ($db_contruct_cancel_row = $db_contruct_cancel->fetch_assoc()) {
    $portingContructCancel[$contruct_cancel_count]['id']                       = $db_contruct_cancel_row['id'];
    $portingContructCancel[$contruct_cancel_count]['clientid']                 = $db_contruct_cancel_row['clientid'];
    $portingContructCancel[$contruct_cancel_count]['lastname']                 = $db_contruct_cancel_row['lastname'];
    $portingContructCancel[$contruct_cancel_count]['emailaddress']             = $db_contruct_cancel_row['emailaddress'];
    $portingContructCancel[$contruct_cancel_count]['district']                 = $db_contruct_cancel_row['district'];
    $portingContructCancel[$contruct_cancel_count]['wisocontract_cancel_date'] = $db_contruct_cancel_row['wisocontract_cancel_input_date'].'<br />'.$db_contruct_cancel_row['wisocontract_cancel_date'];
    $portingContructCancel[$contruct_cancel_count]['cancel_tal']               = $db_contruct_cancel_row['cancel_tal'];
    $portingContructCancel[$contruct_cancel_count]['tal_cancel_ack_date']      = $db_contruct_cancel_row['tal_cancel_ack_date'];
    $portingContructCancel[$contruct_cancel_count]['purtel_login']             = $db_contruct_cancel_row['purtel_login'].'<br />'.$db_contruct_cancel_row['purtel_passwort'];
    $contruct_cancel_count++;
}

// Kündigung abgeschlossen
$db_contruct_canceled = $db->query( $sql['contruct_canceled']);
$contruct_canceled_count = 0;
while ($db_contruct_canceled_row = $db_contruct_canceled->fetch_assoc()) {
    $portingContructCanceled[$contruct_canceled_count]['id']                         = $db_contruct_canceled_row['id'];
    $portingContructCanceled[$contruct_canceled_count]['clientid']                   = $db_contruct_canceled_row['clientid'];
    $portingContructCanceled[$contruct_canceled_count]['lastname']                   = $db_contruct_canceled_row['lastname'];
    $portingContructCanceled[$contruct_canceled_count]['emailaddress']               = $db_contruct_canceled_row['emailaddress'];
    $portingContructCanceled[$contruct_canceled_count]['district']                   = $db_contruct_canceled_row['district'];
    $portingContructCanceled[$contruct_canceled_count]['wisocontract_canceled_date'] = $db_contruct_canceled_row['wisocontract_canceled_date'];
    $portingContructCanceled[$contruct_canceled_count]['cancel_tal']                 = $db_contruct_canceled_row['cancel_tal'];
    $portingContructCanceled[$contruct_canceled_count]['tal_cancel_ack_date']        = $db_contruct_canceled_row['tal_cancel_ack_date'];
    $portingContructCanceled[$contruct_canceled_count]['purtel_login']               = $db_contruct_canceled_row['purtel_login'].'<br />'.$db_contruct_canceled_row['purtel_passwort'];
    $contruct_canceled_count++;
}

// inaktiv
$db_connection_inactive = $db->query( $sql['connection_inactive']);
$connection_inactive_count = 0;
while ($db_connection_inactive_row = $db_connection_inactive->fetch_assoc()) {
    $portingConnectionInactive[$connection_inactive_count]['id']                       = $db_connection_inactive_row['id'];
    $portingConnectionInactive[$connection_inactive_count]['clientid']                 = $db_connection_inactive_row['clientid'];
    $portingConnectionInactive[$connection_inactive_count]['lastname']                 = $db_connection_inactive_row['lastname'];
    $portingConnectionInactive[$connection_inactive_count]['emailaddress']             = $db_connection_inactive_row['emailaddress'];
    $portingConnectionInactive[$connection_inactive_count]['district']                 = $db_connection_inactive_row['district'];
    $portingConnectionInactive[$connection_inactive_count]['connection_inactive_date'] = $db_connection_inactive_row['connection_inactive_date'];
    $connection_inactive_count++;
}

// Interessent
$db_prospective_customers = $db->query( $sql['prospective_customers']);
$prospective_customers_count = 0;
while ($db_prospective_customers_row = $db_prospective_customers->fetch_assoc()) {
    $portingProspectiveCustomers[$prospective_customers_count]['id']                = $db_prospective_customers_row['id'];
    $portingProspectiveCustomers[$prospective_customers_count]['clientid']          = $db_prospective_customers_row['clientid'];
    $portingProspectiveCustomers[$prospective_customers_count]['firstname']         = $db_prospective_customers_row['firstname'];
    $portingProspectiveCustomers[$prospective_customers_count]['lastname']          = $db_prospective_customers_row['lastname'];
    $portingProspectiveCustomers[$prospective_customers_count]['district']          = $db_prospective_customers_row['district'];
    $portingProspectiveCustomers[$prospective_customers_count]['registration_date'] = $db_prospective_customers_row['registration_date'];
    $prospective_customers_count++;
}
$db_prospective_customers->free();

// Eingegangene Onlineverträge
$db_prospective_customers_online = $db->query( $sql['prospective_customers_online']);
$prospective_customers_online_count = 0;
while ($db_prospective_customers_online_row = $db_prospective_customers_online->fetch_assoc()) {
    $portingProspectiveCustomersOnline[$prospective_customers_online_count]['id']                   = $db_prospective_customers_online_row['id'];
    $portingProspectiveCustomersOnline[$prospective_customers_online_count]['clientid']             = $db_prospective_customers_online_row['clientid'];
    $portingProspectiveCustomersOnline[$prospective_customers_online_count]['firstname']            = $db_prospective_customers_online_row['firstname'];
    $portingProspectiveCustomersOnline[$prospective_customers_online_count]['lastname']             = $db_prospective_customers_online_row['lastname'];
    $portingProspectiveCustomersOnline[$prospective_customers_online_count]['city']                 = $db_prospective_customers_online_row['city'];
    $portingProspectiveCustomersOnline[$prospective_customers_online_count]['contract_online_date'] = $db_prospective_customers_online_row['contract_online_date'];
    $prospective_customers_online_count++;
}
$db_prospective_customers_online->free();

// Keine Vertragsdaten
$db_contruct_missing = $db->query( $sql['contruct_missing']);
$contruct_missing_count = 0;
while ($db_contruct_missing_row = $db_contruct_missing->fetch_assoc()) {
    if(!$db_contruct_missing_row['contract_sent_date']) {
        $portingContructMissing[$contruct_missing_count]['id']                = $db_contruct_missing_row['id'];
        $portingContructMissing[$contruct_missing_count]['clientid']          = $db_contruct_missing_row['clientid'];
        $portingContructMissing[$contruct_missing_count]['firstname']         = $db_contruct_missing_row['firstname'];
        $portingContructMissing[$contruct_missing_count]['lastname']          = $db_contruct_missing_row['lastname'];
        $portingContructMissing[$contruct_missing_count]['district']          = $db_contruct_missing_row['district'];
        $portingContructMissing[$contruct_missing_count]['registration_date'] = $db_contruct_missing_row['registration_date'];
        $contruct_missing_count++;
    }
}
$db_contruct_missing->free();

// Keine Antwort
$db_contruct_noanswer = $db->query( $sql['contruct_noanswer']);
$contruct_noanswer_count = 0;
$contruct_noanswer3_count = 0;
while ($db_contruct_noanswer_row = $db_contruct_noanswer->fetch_assoc()) {
    $newDate1 = strtotime('-1 day', strtotime($db_contruct_noanswer_row['reg_answer_date']));
    $newDate2 = strtotime('-1 day', strtotime($db_contruct_noanswer_row['contract_sent_date']));

    if($newDate2 > $threemonths && $newDate1 > $threemonths) {
        $portingContructNoAnswer[$contruct_noanswer_count]['id']                 = $db_contruct_noanswer_row['id'];
        $portingContructNoAnswer[$contruct_noanswer_count]['clientid']           = $db_contruct_noanswer_row['clientid'];
        $portingContructNoAnswer[$contruct_noanswer_count]['firstname']          = $db_contruct_noanswer_row['firstname'];
        $portingContructNoAnswer[$contruct_noanswer_count]['lastname']           = $db_contruct_noanswer_row['lastname'];
        $portingContructNoAnswer[$contruct_noanswer_count]['district']           = $db_contruct_noanswer_row['district'];
        $portingContructNoAnswer[$contruct_noanswer_count]['reg_answer_date']    = $db_contruct_noanswer_row['reg_answer_date'];
        $portingContructNoAnswer[$contruct_noanswer_count]['contract_sent_date'] = $db_contruct_noanswer_row['contract_sent_date'];
        $contruct_noanswer_count++;
    }
    if($newDate2 <= $threemonths && $newDate1 <= $threemonths) {
        $portingContructNoAnswer3[$contruct_noanswer3_count]['id']                 = $db_contruct_noanswer_row['id'];
        $portingContructNoAnswer3[$contruct_noanswer3_count]['clientid']           = $db_contruct_noanswer_row['clientid'];
        $portingContructNoAnswer3[$contruct_noanswer3_count]['firstname']          = $db_contruct_noanswer_row['firstname'];
        $portingContructNoAnswer3[$contruct_noanswer3_count]['lastname']           = $db_contruct_noanswer_row['lastname'];
        $portingContructNoAnswer3[$contruct_noanswer3_count]['district']           = $db_contruct_noanswer_row['district'];
        $portingContructNoAnswer3[$contruct_noanswer3_count]['reg_answer_date']    = $db_contruct_noanswer_row['reg_answer_date'];
        $portingContructNoAnswer3[$contruct_noanswer3_count]['contract_sent_date'] = $db_contruct_noanswer_row['contract_sent_date'];
        $contruct_noanswer3_count++;
    }
}
$db_contruct_noanswer->free();

// Telefonbucheintrag
$contruct_phonebook_count = 0;
$db_contruct_phonebook = $db->query( $sql['contruct_phonebook']);
while ($db_contruct_phonebook_row = $db_contruct_phonebook->fetch_assoc()) {
    $portingContructPhonebook[$contruct_phonebook_count]['id']                         = $db_contruct_phonebook_row['id'];
    $portingContructPhonebook[$contruct_phonebook_count]['clientid']                   = $db_contruct_phonebook_row['clientid'];
    $portingContructPhonebook[$contruct_phonebook_count]['firstname']                  = $db_contruct_phonebook_row['firstname'];
    $portingContructPhonebook[$contruct_phonebook_count]['lastname']                   = $db_contruct_phonebook_row['lastname'];
    $portingContructPhonebook[$contruct_phonebook_count]['connection_activation_date'] = $db_contruct_phonebook_row['connection_activation_date'];
    $contruct_phonebook_count++;
}
$db_contruct_phonebook->free();

// Purtel vorab
$contruct_purtel_count = 0;
$db_contruct_purtel = $db->query( $sql['contruct_purtel']);
while ($db_contruct_purtel_row = $db_contruct_purtel->fetch_assoc()) {
    $portingContructPurtel[$contruct_purtel_count]['id']                         = $db_contruct_purtel_row['id'];
    $portingContructPurtel[$contruct_purtel_count]['clientid']                   = $db_contruct_purtel_row['clientid'];
    $portingContructPurtel[$contruct_purtel_count]['firstname']                  = $db_contruct_purtel_row['firstname'];
    $portingContructPurtel[$contruct_purtel_count]['lastname']                   = $db_contruct_purtel_row['lastname'];
    $portingContructPurtel[$contruct_purtel_count]['connection_activation_date'] = $db_contruct_purtel_row['connection_activation_date'];
    $portingContructPurtel[$contruct_purtel_count]['productname']                = $db_contruct_purtel_row['productname'];
    $portingContructPurtel[$contruct_purtel_count]['purtel_product']             = $db_contruct_purtel_row['purtel_product'];
    $contruct_purtel_count++;
}
$db_contruct_purtel->free();

// Sonderkonditionen
$contruct_special_count = 0;
$db_contruct_special = $db->query( $sql['contruct_special']);
while ($db_contruct_special_row = $db_contruct_special->fetch_assoc()) {
    $portingContructSpecial[$contruct_special_count]['id']                      = $db_contruct_special_row['id'];
    $portingContructSpecial[$contruct_special_count]['clientid']                = $db_contruct_special_row['clientid'];
    $portingContructSpecial[$contruct_special_count]['firstname']               = $db_contruct_special_row['firstname'];
    $portingContructSpecial[$contruct_special_count]['lastname']                = $db_contruct_special_row['lastname'];
    $portingContructSpecial[$contruct_special_count]['special_conditions_from'] = $db_contruct_special_row['special_conditions_from'];
    $portingContructSpecial[$contruct_special_count]['special_conditions_till'] = $db_contruct_special_row['special_conditions_till'];
    $contruct_special_count++;
}
$db_contruct_special->free();

// nicht versorgbar
$contruct_nosupply_count = 0;
$db_contruct_nosupply = $db->query( $sql['contruct_nosupply']);
while ($db_contruct_nosupply_row = $db_contruct_nosupply->fetch_assoc()) {
    $portingContructNoSupply[$contruct_nosupply_count]['id']                          = $db_contruct_nosupply_row['id'];
    $portingContructNoSupply[$contruct_nosupply_count]['clientid']                    = $db_contruct_nosupply_row['clientid'];
    $portingContructNoSupply[$contruct_nosupply_count]['firstname']                   = $db_contruct_nosupply_row['firstname'];
    $portingContructNoSupply[$contruct_nosupply_count]['lastname']                    = $db_contruct_nosupply_row['lastname'];
    $portingContructNoSupply[$contruct_nosupply_count]['district']                    = $db_contruct_nosupply_row['district'];
    $contruct_nosupply_count++;
}
$db_contruct_nosupply->free();

// Fehlende Vertragsunterlagen
// Altvertragsangaben
$contruct_old_missing_count = 0;
$db_contruct_old_missing = $db->query( $sql['miss_contruct_old']);
while ($db_contruct_old_missing_row = $db_contruct_old_missing->fetch_assoc()) {
    $portingContructOldMissing[$contruct_old_missing_count]['id']                             = $db_contruct_old_missing_row['id'];
    $portingContructOldMissing[$contruct_old_missing_count]['clientid']                       = $db_contruct_old_missing_row['clientid'];
    $portingContructOldMissing[$contruct_old_missing_count]['firstname']                      = $db_contruct_old_missing_row['firstname'];
    $portingContructOldMissing[$contruct_old_missing_count]['lastname']                       = $db_contruct_old_missing_row['lastname'];
    $portingContructOldMissing[$contruct_old_missing_count]['oldcontract_internet_provider']  = $db_contruct_old_missing_row['oldcontract_internet_provider']."<br />".$db_contruct_old_missing_row['oldcontract_internet_clientno'];
    $portingContructOldMissing[$contruct_old_missing_count]['exp_date_int']                   = $db_contruct_old_missing_row['exp_date_int'];
    $portingContructOldMissing[$contruct_old_missing_count]['oldcontract_phone_provider']     = $db_contruct_old_missing_row['oldcontract_phone_provider']."<br />".$db_contruct_old_missing_row['oldcontract_phone_clientno'];
    $portingContructOldMissing[$contruct_old_missing_count]['exp_date_phone']                 = $db_contruct_old_missing_row['exp_date_phone'];
    $contruct_old_missing_count++;
}
$db_contruct_old_missing->free();
  
// Produktangaben
$contruct_product_missing_count = 0;
$db_contruct_product_missing = $db->query( $sql['miss_contruct_product']);
while ($db_contruct_product_missing_row = $db_contruct_product_missing->fetch_assoc()) {
    $portingContructProductMissing[$contruct_product_missing_count]['id']                          = $db_contruct_product_missing_row['id'];
    $portingContructProductMissing[$contruct_product_missing_count]['clientid']                    = $db_contruct_product_missing_row['clientid'];
    $portingContructProductMissing[$contruct_product_missing_count]['firstname']                   = $db_contruct_product_missing_row['firstname'];
    $portingContructProductMissing[$contruct_product_missing_count]['lastname']                    = $db_contruct_product_missing_row['lastname'];
    $portingContructProductMissing[$contruct_product_missing_count]['district']                    = $db_contruct_product_missing_row['district'];
    $contruct_product_missing_count++;
}
$db_contruct_product_missing->free();
  
// Einzugsermächtigung
$contruct_eze_missing_count = 0;
$db_contruct_eze_missing = $db->query( $sql['miss_contruct_eze']);
while ($db_contruct_eze_missing_row = $db_contruct_eze_missing->fetch_assoc()) {
    $portingContructEzeMissing[$contruct_eze_missing_count]['id']                         = $db_contruct_eze_missing_row['id'];
    $portingContructEzeMissing[$contruct_eze_missing_count]['clientid']                   = $db_contruct_eze_missing_row['clientid'];
    $portingContructEzeMissing[$contruct_eze_missing_count]['firstname']                  = $db_contruct_eze_missing_row['firstname'];
    $portingContructEzeMissing[$contruct_eze_missing_count]['lastname']                   = $db_contruct_eze_missing_row['lastname'];
    $portingContructEzeMissing[$contruct_eze_missing_count]['district']                   = $db_contruct_eze_missing_row['district'];
    $portingContructEzeMissing[$contruct_eze_missing_count]['connection_activation_date'] = $db_contruct_eze_missing_row['connection_activation_date'];
    $contruct_eze_missing_count++;
}
$db_contruct_eze_missing->free();

// E-Mail
$contruct_email_missing_count = 0;
$db_contruct_email_missing = $db->query( $sql['miss_contruct_mail']);
while ($db_contruct_email_missing_row = $db_contruct_email_missing->fetch_assoc()) {
    $portingContructEmailMissing[$contruct_email_missing_count]['id']        = $db_contruct_email_missing_row['id'];
    $portingContructEmailMissing[$contruct_email_missing_count]['clientid']  = $db_contruct_email_missing_row['clientid'];
    $portingContructEmailMissing[$contruct_email_missing_count]['firstname'] = $db_contruct_email_missing_row['firstname'];
    $portingContructEmailMissing[$contruct_email_missing_count]['lastname']  = $db_contruct_email_missing_row['lastname'];
    $portingContructEmailMissing[$contruct_email_missing_count]['district']  = $db_contruct_email_missing_row['district'];
    $contruct_email_missing_count++;
}
$db_contruct_email_missing->free();

$authorizationChecker = $this->get('security.authorization_checker');

// get list
require_once ($StartPath.'/overview/list.php');

$overviewsViews = [
    'ROLE_VIEW_OVERVIEW_PORTING' => [
        $StartPath.'/overview/porting.php', // get porting
        $StartPath.'/overview/wbci.php', // get wbci
        $StartPath.'/overview/portingCustomer.php', // get porting customer
        $StartPath.'/overview/wbciCustomer.php', // get wbci customer
        $StartPath.'/overview/portingPurtel.php', // get porting purtel
        $StartPath.'/overview/portingNewcall.php', // get porting newcall
        $StartPath.'/overview/portingCallnum.php', // get porting callnum
        $StartPath.'/overview/portingRoute.php', // get porting route
    ],
    'ROLE_VIEW_OVERVIEW_TAL' => [
        $StartPath.'/overview/portingOrderTal.php', // get order TAL
        $StartPath.'/overview/portingOrderedTal.php', // get ordered TAL
        $StartPath.'/overview/portingTalConnectInfo.php', // get connect info TAL
        $StartPath.'/overview/portingConfirmedTal.php', // get comfirmed TAL
        $StartPath.'/overview/portingConfirmedTalsWithinPastFourWeeks.php', // get confirmed TALs within past four weeks
    ],
    'ROLE_VIEW_OVERVIEW_CPE' => [
        $StartPath.'/overview/portingFritzbox6320.php', // get Fritzbox 6320
        $StartPath.'/overview/portingFritzbox6360.php', // get Fritzbox 6360
        $StartPath.'/overview/portingFritzbox6490.php', // get Fritzbox 6490
        $StartPath.'/overview/portingFritzbox7270.php', // get Fritzbox 7270
        $StartPath.'/overview/portingFritzbox7360.php', // get Fritzbox 7360
        $StartPath.'/overview/portingFritzbox7390.php', // get Fritzbox 7390
        $StartPath.'/overview/portingFritzbox7490.php', // get Fritzbox 7490
        $StartPath.'/overview/portingFritzbox7560.php', // get Fritzbox 7560
        $StartPath.'/overview/portingFritzbox7590.php', // get Fritzbox 7590
        $StartPath.'/overview/portingFritzboxGW_400.php', // get Gateway 400
        $StartPath.'/overview/portingFritzboxIBG2426A.php', // get INNBOX G2426A
        $StartPath.'/overview/portingFritzboxNoType.php', // get Fritzbox ohne Typ
    ],
    'ROLE_VIEW_OVERVIEW_TECHNICAL' => [
        $StartPath.'/overview/portingNetPatch.php', // get Patches
        $StartPath.'/overview/portingNetDslamCmts.php', // get DSLAM / CMTS
        $StartPath.'/overview/portingNetPppoe.php', // get PPPoE
        $StartPath.'/overview/portingNetActive.php', // get Activation
        $StartPath.'/overview/portingNetGfActive.php', // get Activation  fiber
    ],
    'ROLE_VIEW_OVERVIEW_CONTRACT' => [
        $StartPath.'/overview/portingContructCancel.php', // get Vertrag Kündigung eingegeangen
        $StartPath.'/overview/portingContructCanceled.php', // get Kündigung abgeschlossen
        $StartPath.'/overview/portingConnectionInactive.php', // get Inaktiv seit
        $StartPath.'/overview/portingProspectiveCustomers.php', // get Intressenten
        $StartPath.'/overview/portingProspectiveCustomersOnline.php', // get Intressenten Online
        $StartPath.'/overview/portingContructMissing.php', // get Keine Vertragsdaten
        $StartPath.'/overview/portingContructNoAnswer.php', // get Keine Antwort bis 3 Monate
        $StartPath.'/overview/portingContructNoAnswer3.php', // get Keine Antwort ab 3 Monate
        $StartPath.'/overview/portingContructPhonebook.php', // get Telefonbucheintrag
        $StartPath.'/overview/portingContructPurtel.php', // get Purtel vorab
        $StartPath.'/overview/portingContructSpecial.php', // get Sonderkonditionen
        $StartPath.'/overview/portingContructNoSupply.php', // get nicht versorgbar
    ],
    'ROLE_VIEW_OVERVIEW_MISSING_CONTRACT' => [
        $StartPath.'/overview/portingContructOldMissing.php', // get Altvertragsangaben fehlen
        $StartPath.'/overview/portingContructProductMissing.php', // get Produktangaben fehlen
        $StartPath.'/overview/portingContructEzeMissing.php', // get Einzugsermächtigung fehlt
        $StartPath.'/overview/portingContructEmailMissing.php', // get E-Mail fehlt
    ],
];

foreach ($overviewsViews as $requiredRole => $views) {
    if (!$authorizationChecker->isGranted($requiredRole)) {
        continue;
    }

    foreach ($views as $templateFile) {
        require_once $templateFile;
    }
}

$db->close();

?>
