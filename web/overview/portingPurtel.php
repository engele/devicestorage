<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_OVERVIEW_PORTING');

  $headline = array (
    'Kundennr.',
    'Vorname',
    'Nachname',
    'Stadt',
    'Portierung<br />eingestellt am',
    'Portierung<br />Wunschdatum'
  );
  $head_csv = serialize($headline);
  $head_csv = urlencode($head_csv);
  $_SESSION['head_purtel'] = $head_csv;
  if (isset($portingPurtel)) {
    $porting_csv  = serialize($portingPurtel);
    $porting_csv  = urlencode($porting_csv);
    $_SESSION['data_purtel'] = $porting_csv;
  }
?>
<div class="Ovr OvrPortingPurtel box">
  <h3>Portierung in Purtel</h3>
 
  <p><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button></p> 
  <p><a href='<?php echo $StartURL; ?>/overview/overview_csv.php?csv=purtel' target='_blank'>CSV Export</a></p>
  <table class="Ovr">
  <thead><tr>
    <th class='n11'><?php echo $headline[0]; ?></th>
    <th class='n12'><?php echo $headline[1]; ?></th>
    <th class='n13'><?php echo $headline[2]; ?></th>
    <th class='n14'><?php echo $headline[3]; ?></th>
    <th class='n15 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[4]; ?></th>
    <th class='n16 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[5]; ?></th>
  </tr></thead>
  <tbody>
  <?php
    $row = 'odd';
    if (isset ($portingPurtel)) {
      foreach ($portingPurtel as $index) {
        if (key_exists ('firstname', $index))                 $firstname                  = $index['firstname'];                  else $firstname                 = '';
        if (key_exists ('lastname', $index))                  $lastname                   = $index['lastname'];                   else $lastname                  = '';
        if (key_exists ('district', $index))                  $district                   = $index['district'];                   else $district                  = '';
        if (key_exists ('ventelo_purtel_enter_date', $index)) $ventelo_purtel_enter_date  = $index['ventelo_purtel_enter_date'];  else $ventelo_purtel_enter_date = '';
        if (key_exists ('ventelo_port_wish_date', $index))    $ventelo_port_wish_date     = $index['ventelo_port_wish_date'];     else $ventelo_port_wish_date    = '';
  
        echo "<tr class='$row'>";
        echo "<td><a href='".$StartURL."/index.php?menu=customer&func=vertrag&id=".$index['id']."'>".$index['clientid']."</a></td>";
        echo "<td>".$firstname."</td>";
        echo "<td>".$lastname."</td>";
        echo "<td>".$district."</td>";
        echo "<td>".$ventelo_purtel_enter_date."</td>";
        echo "<td>".$ventelo_port_wish_date."</td>";
        echo "</tr>";
        if ($row == 'odd') $row = 'even'; else $row = 'odd';
      }
    }
  ?>
  </tbody>
  </table>
  <br /><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button> 
</div>
