<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_OVERVIEW_TAL');

  $headline = array (
    'Kundennr.',
    'Vorname',
    'Nachname',
    'Stadt',
    'TAL Bestell-<br />datum',
    'TAL bestellt<br />zum'
  );
  $head_csv = serialize($headline);
  $head_csv = urlencode($head_csv);
  $_SESSION['head_orderedtal'] = $head_csv;
  if (isset($portingOrderedTal)) {
    $porting_csv  = serialize($portingOrderedTal);
    $porting_csv  = urlencode($porting_csv);
    $_SESSION['data_orderedtal'] = $porting_csv;
  }
?>
<div class="Ovr OvrPortingOrderedTal box">
  <h3>TALs, die bereits bestellt wurden</h3>

  <p><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button></p> 
  <p><a href='<?php echo $StartURL; ?>/overview/overview_csv.php?csv=orderedtal' target='_blank'>CSV Export</a></p>
  <table class="Ovr">
  <thead><tr>
    <th class='n11'><?php echo $headline[0]; ?></th>
    <th class='n12'><?php echo $headline[1]; ?></th>
    <th class='n13'><?php echo $headline[2]; ?></th>
    <th class='n14'><?php echo $headline[3]; ?></th>
    <th class='n15'><?php echo $headline[4]; ?></th>
    <th class='n16'><?php echo $headline[5]; ?></th>
  </tr></thead>
  <tbody>
  <?php
    $row = 'odd';
    if (isset ($portingOrderedTal)) {
      foreach ($portingOrderedTal as $index) {
        if (key_exists ('firstname', $index))             $firstname            = $index['firstname'];            else $firstname             = '';
        if (key_exists ('lastname', $index))              $lastname             = $index['lastname'];             else $lastname              = '';
        if (key_exists ('district', $index))              $district             = $index['district'];             else $district              = '';
        if (key_exists ('tal_order_date', $index))        $tal_order_date       = $index['tal_order_date'];       else $tal_order_date        = '';
        if (key_exists ('tal_ordered_for_date', $index))  $tal_ordered_for_date = $index['tal_ordered_for_date']; else $tal_ordered_for_date  = '';

        echo "<tr class='$row'>";
        echo "<td><a href='".$StartURL."/index.php?menu=customer&func=vertrag&id=".$index['id']."'>".$index['clientid']."</a></td>";
        echo "<td>".$firstname."</td>";
        echo "<td>".$lastname."</td>";
        echo "<td>".$district."</td>";
        echo "<td>".$tal_order_date."</td>";
        echo "<td>".$tal_ordered_for_date."</td>";
        echo "</tr>";
        if ($row == 'odd') $row = 'even'; else $row = 'odd';
      }
    }
  ?>
  </tbody>
  </table>
  <br /><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button> 
</div>
