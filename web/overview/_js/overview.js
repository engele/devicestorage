var itslWindows = {
  OvrList:                                      false,
  OvrPorting:                                   false,
  OvrWbci:                                      false,
  OvrPortingCustomer:                           false,
  OvrWbciCustomer:                              false,
  OvrPortingPurtel:                             false,
  OvrPortingNewcall:                            false,
  OvrPortingCallnum:                            false,
  OvrPortingRoute:                              false,
  OvrPortingOrderTal:                           false,
  OvrPortingOrderedTal:                         false,
  OvrPortingTalConnectInfo:                     false,
  OvrPortingConfirmedTal:                       false,
  OvrPortingConfirmedTalsWithinPastFourWeeks:   false,
  OvrPortingFritzbox6320:                       false,
  OvrPortingFritzbox6360:                       false,
  OvrPortingFritzbox6490:                       false,
  OvrPortingFritzbox7270:                       false,
  OvrPortingFritzbox7360:                       false,
  OvrPortingFritzbox7390:                       false,
  OvrPortingFritzbox7490:                       false,
  OvrPortingFritzbox7560:                       false,
  OvrPortingFritzbox7590:                       false,
  OvrPortingFritzboxGW_400:                     false,
  OvrPortingFritzboxIBG2426A:                   false,
  OvrPortingFritzboxNoType:                     false,
  OvrPortingNetPatch:                           false,
  OvrPortingNetDslamCmts:                       false,
  OvrPortingNetPppoe:                           false,
  OvrPortingNetActive:                          false,
  OvrPortingNetGfActive:                        false,
  OvrPortingContructCancel:                     false,
  OvrPortingContructCanceled:                   false,
  OvrPortingConnectionInactive:                 false,
  OvrPortingContructMissing:                    false,
  OvrPortingProspectiveCustomers:               false,
  OvrPortingProspectiveCustomersOnline:         false,
  OvrPortingContructNoAnswer:                   false,
  OvrPortingContructNoAnswer3:                  false,
  OvrPortingContructPhonebook:                  false,
  OvrPortingContructPurtel:                     false,
  OvrPortingContructSpecial:                    false,
  OvrPortingContructNoSupply:                   false,
  OvrPortingContructOldMissing:                 false,
  OvrPortingContructProductMissing:             false,
  OvrPortingContructEzeMissing:                 false,
  OvrPortingContructEmailMissing:               false
};

function ovrShow (itslWin) {
  for (var win in itslWindows) {
    for (i = 0; i < itslWin.length; i++) {
      if (win == itslWin[i]) {
        itslWindows[win] = true;
      }
    }
    if (itslWindows[win]) {
      $("."+win).show();
      itslWindows[win] = false;
    } else {
      $("."+win).hide();
    }
  }  
}

$(document).ready(function(){
  $("table.Ovr").tablesorter();
});