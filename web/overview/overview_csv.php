<?php
  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename=overview_'.$_GET['csv'].'_'.date ("YmdHis").'.csv');  

  session_start();

  $csv_handle = fopen('php://output', 'w');
  if (isset($_SESSION['head_'.$_GET['csv']])) {
    $headline = urldecode ($_SESSION['head_'.$_GET['csv']]);
    $headline = unserialize ($headline);
    foreach ($headline AS $key => $value) {
      $headline[$key] = utf8_decode(str_replace("<br />", " ", $value));
    }
    fputcsv($csv_handle, $headline, ';');
  }
  if (isset($_SESSION['data_'.$_GET['csv']])) {
    $csv = urldecode ($_SESSION['data_'.$_GET['csv']]);
    $csv = unserialize ($csv);
    foreach ($csv AS $index) {
      $data = array();
      foreach ($index AS $key => $value) {
        if ($key != 'id') array_push ($data, utf8_decode(str_replace("<br />", "\n", $value)));
      }
      fputcsv($csv_handle, $data, ';');
    }
  }
  fclose ($csv_handle);
?>