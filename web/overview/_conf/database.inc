<?php
$sql = array (
    // Anbieterwechsel  manuell
    // Anbieterwechsel die in den nächsten Tagen zu bearbeiten sind
    'porting_days' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (exp_date_phone != '' AND exp_date_phone IS NOT NULL)
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND exp_date_phone != 'Kein Vertrag'
        AND (exp_done_phone = '' OR exp_done_phone IS NULL)
        AND (ventelo_port_letter_date = '' OR ventelo_port_letter_date IS NULL OR ventelo_port_letter_date = 'N/E')
        AND prospect_supply_status = 'with_supply'
        AND ((oldcontract_phone_provider NOT LIKE '%Telekom%'
        AND oldcontract_phone_provider NOT LIKE '%O2%'
        AND oldcontract_phone_provider NOT LIKE '%1%&%1%'
        AND oldcontract_phone_provider NOT LIKE '%1%und%1%')
        OR ((oldcontract_phone_provider = '' OR oldcontract_phone_provider IS NULL)
        AND oldcontract_internet_provider NOT LIKE '%Telekom%'
        AND oldcontract_internet_provider NOT LIKE '%O2%'
        AND oldcontract_internet_provider NOT LIKE '%1%&%1%'
        AND oldcontract_internet_provider NOT LIKE '%1%und%1%'))
    ",
    //  AND (stati_port_confirm_date = '' OR stati_port_confirm_date IS NULL)
    //Anbieterwechsel mehr als eine Woche beim Kunden
    'porting_weeks' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (ventelo_port_letter_date != '' AND ventelo_port_letter_date IS NOT NULL)
        AND (ventelo_confirmation_date = '' OR ventelo_confirmation_date IS NULL)
        AND (exp_done_phone = '' OR exp_done_phone IS NULL) 
        AND ((oldcontract_phone_provider NOT LIKE '%Telekom%'
        AND oldcontract_phone_provider NOT LIKE '%O2%'
        AND oldcontract_phone_provider NOT LIKE '%1%&%1%'
        AND oldcontract_phone_provider NOT LIKE '%1%und%1%')
        OR ((oldcontract_phone_provider = '' OR oldcontract_phone_provider IS NULL)
        AND oldcontract_internet_provider NOT LIKE '%Telekom%'
        AND oldcontract_internet_provider NOT LIKE '%O2%'
        AND oldcontract_internet_provider NOT LIKE '%1%&%1%'
        AND oldcontract_internet_provider NOT LIKE '%1%und%1%'))
        ORDER BY STR_TO_DATE(ventelo_port_letter_date,'%d.%m.%Y')
    ",
    //      AND (stati_port_confirm_date = '' OR stati_port_confirm_date IS NULL)

    // Anbieterwechsel  WBCI
    // Anbieterwechsel die in den nächsten Tagen zu bearbeiten sind
    'wbci_days' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (exp_date_phone != '' AND exp_date_phone IS NOT NULL)
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (wbci_bestaetigt_am = '' OR wbci_bestaetigt_am IS NULL)
        AND  exp_date_phone != 'Kein Vertrag'
        AND (exp_done_phone = '' OR exp_done_phone IS NULL) 
        AND (ventelo_port_letter_date = '' OR ventelo_port_letter_date IS NULL OR ventelo_port_letter_date = 'N/E')
        AND prospect_supply_status = 'with_supply'
        AND (((oldcontract_phone_provider LIKE '%Telekom%' AND oldcontract_phone_provider NOT LIKE '%Tele2%')
        OR oldcontract_phone_provider LIKE '%O2%'
        OR oldcontract_phone_provider LIKE '%1%&%1%'
        OR oldcontract_phone_provider LIKE '%1%und%1%')
        OR ((oldcontract_phone_provider = '' OR oldcontract_phone_provider IS NULL)
        AND ((oldcontract_internet_provider LIKE '%Telekom%' AND oldcontract_internet_provider NOT LIKE '%Tele2%')
        OR oldcontract_internet_provider LIKE '%O2%'
        OR oldcontract_internet_provider LIKE '%1%&%1%'
        OR oldcontract_internet_provider LIKE '%1%und%1%')))
    ",
    //Anbieterwechsel mehr als eine Woche beim Kunden
    'wbci_weeks' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (ventelo_port_letter_date != '' AND ventelo_port_letter_date IS NOT NULL)
        AND (ventelo_confirmation_date = '' OR ventelo_confirmation_date IS NULL)
        AND (exp_done_phone = '' OR exp_done_phone IS NULL) 
        AND (wbci_bestaetigt_am = '' OR wbci_bestaetigt_am IS NULL)
        AND (((oldcontract_phone_provider LIKE '%Telekom%' AND oldcontract_phone_provider NOT LIKE '%Tele2%')
        OR oldcontract_phone_provider LIKE '%O2%'
        OR oldcontract_phone_provider LIKE '%1%&%1%'
        OR oldcontract_phone_provider LIKE '%1%und%1%')
        OR ((oldcontract_phone_provider = '' OR oldcontract_phone_provider IS NULL)
        AND ((oldcontract_internet_provider LIKE '%Telekom%' AND oldcontract_internet_provider NOT LIKE '%Tele2%')
        OR oldcontract_internet_provider LIKE '%O2%'
        OR oldcontract_internet_provider LIKE '%1%&%1%'
        OR oldcontract_internet_provider LIKE '%1%und%1%')))
        ORDER BY STR_TO_DATE(ventelo_port_letter_date,'%d.%m.%Y')
    ",
    //Portierungen in Purtel eingestellt
    'porting_purtel' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (ventelo_purtel_enter_date != '' AND ventelo_purtel_enter_date IS NOT NULL)
        AND (stati_port_confirm_date = '' OR stati_port_confirm_date IS NULL)
    ",
    // Rufnummern aufschalten
    'porting_callnum' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND phone_number_added = 'Ja'
        AND (stati_port_confirm_date != '' AND stati_port_confirm_date IS NOT NULL)
        AND stati_port_confirm_date != 'N/E'
    ",
    // neue Rufnummern
    'porting_newcall' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (new_number_date != '' AND new_number_date IS NOT NULL)
        AND (new_numbers_text = '' OR new_numbers_text IS NULL)
        AND prospect_supply_status = 'with_supply'
    ",
    // Umroutungen
    'porting_route' => "  
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (routing_active = '' OR routing_active IS NULL)
        AND (routing_active_date != '' AND routing_active_date IS NOT NULL)
        AND prospect_supply_status = 'with_supply'
    ",
    // TALs
    // TALs die zu bestellen sind
    // Hertmannsweiler und Kleinaspacher nicht!!!
    'tal_order1' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND prospect_supply_status = 'with_supply'
        AND (stati_port_confirm_date != '' AND stati_port_confirm_date IS NOT NULL)
        AND district != 'Nonnenberg'
        AND district != 'Maubach Wohnen IV'
        AND stati_port_confirm_date != 'N/E'
        AND (tal_order_date = '' OR tal_order_date IS NULL)
        ORDER BY STR_TO_DATE(stati_port_confirm_date,'%d.%m.%Y')
    ",
    'tal_order2' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND prospect_supply_status = 'with_supply'
        AND (sofortdsl = 'Ja' OR oldcontract_exists = 'Nein' OR oldcontract_exists = 'Ja nicht kündigen') 
        AND district != 'Nonnenberg'
        AND district != 'Maubach Wohnen IV'
        AND (tal_order_date = '' OR tal_order_date IS NULL)
        ORDER BY STR_TO_DATE(stati_port_confirm_date,'%d.%m.%Y')
    ",
    'tal_order3' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND prospect_supply_status = 'with_supply'
        AND district != 'Nonnenberg'
        AND district != 'Maubach Wohnen IV'
        AND (tal_order_date = '' OR tal_order_date IS NULL)
        AND (stati_port_confirm_date = '' OR stati_port_confirm_date IS NULL)
        AND (ventelo_port_wish_date != '' AND ventelo_port_wish_date IS NOT NULL)
        AND ventelo_port_wish_date != 'N/E'
        ORDER BY STR_TO_DATE(stati_port_confirm_date,'%d.%m.%Y')
    ",
    //TALs sind bestellt
    'tal_ordered' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (tal_order_date != '' AND tal_order_date IS NOT NULL)
        AND tal_order_date != 'N/E da GF'
        AND (tal_order_ack_date = '' OR tal_order_ack_date IS NULL)
    ",
    // Kunden über Anschaltterm informieren
    'tal_connect_info' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (tal_order_ack_date != '' AND tal_order_ack_date IS NOT NULL)
        AND tal_order_ack_date != 'N/E da GF'
        AND (customer_connect_info_date = '' OR customer_connect_info_date IS NULL)
        AND tal_order_ack_date NOT LIKE '%.2012'
        AND tal_order_ack_date NOT LIKE '%.2011'
        AND (connection_activation_date = '' OR connection_activation_date IS NULL)
        ORDER BY STR_TO_DATE(tal_order_ack_date,'%d.%m.%Y')
    ",
    // Bestätigte Tals
      // AND tal_order_ack_date != 'N/E da GF'
      //AND connection_activation_date = ''
    'tal_confirmed' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (tal_order_ack_date != '' AND tal_order_ack_date IS NOT NULL)
        ORDER BY STR_TO_DATE(tal_order_ack_date,'%d.%m.%Y')
    ",
    // Fritzboxen
    // 6320, 6360, 6490
    'fritz_boxes1' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND district = 'Nonnenberg'
        AND (connection_activation_date = '' OR connection_activation_date IS NULL)
        AND (terminal_sended_date = '' OR terminal_sended_date IS NULL)
        AND prospect_supply_status = 'with_supply'
        AND terminal_type = ?
        AND sofortdsl = 'Ja'
        ORDER BY STR_TO_DATE(stati_port_confirm_date,'%d.%m.%Y')
    ",
    'fritz_boxes2' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (connection_activation_date = '' OR connection_activation_date IS NULL)
        AND (terminal_sended_date = '' OR terminal_sended_date IS NULL)
        AND prospect_supply_status = 'with_supply'
        AND terminal_type = ?
        AND (stati_port_confirm_date != '' AND stati_port_confirm_date IS NOT NULL)
        AND sofortdsl != 'Ja'
        ORDER BY STR_TO_DATE(stati_port_confirm_date,'%d.%m.%Y')
    ",
    // 7270
    'fritz_boxes3' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND clientid NOT LIKE '0000_%'
        AND (contract_received_date != '' AND contract_received_date IS NOT NULL)
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (connection_activation_date = '' OR connection_activation_date IS NULL)
        AND (terminal_sended_date = '' OR terminal_sended_date IS NULL)
        AND prospect_supply_status = 'with_supply'
        AND terminal_type = ?
        AND (tal_order_ack_date != '' AND tal_order_ack_date IS NOT NULL)
        ORDER BY  STR_TO_DATE(tal_order_ack_date,'%d.%m.%Y')
    ",
    'fritz_boxes4' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND clientid NOT LIKE '0000_%'
        AND (contract_received_date != '' AND contract_received_date IS NOT NULL)
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (connection_activation_date = '' OR connection_activation_date IS NULL)
        AND (terminal_sended_date = '' OR terminal_sended_date IS NULL)
        AND prospect_supply_status = 'with_supply'
        AND terminal_type = ?
        AND (tal_ordered_for_date != '' AND tal_ordered_for_date IS NOT NULL)
        ORDER BY  STR_TO_DATE(tal_order_ack_date,'%d.%m.%Y')
    ",
    // 7360, 7390, 7490, 7560, 7590
    'fritz_boxes5' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND clientid NOT LIKE '0000_%'
        AND (contract_received_date != '' AND contract_received_date IS NOT NULL)
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND district = 'Maubach Wohnen IV'
        AND (connection_activation_date = '' OR connection_activation_date IS NULL)
        AND (terminal_sended_date = '' OR terminal_sended_date IS NULL)
        AND prospect_supply_status = 'with_supply'
        AND terminal_type = ?
        AND sofortdsl = 'Ja'
        ORDER BY  STR_TO_DATE(tal_ordered_for_date,'%d.%m.%Y')
    ",
    'fritz_boxes6' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND clientid NOT LIKE '0000_%'
        AND (contract_received_date != '' AND contract_received_date IS NOT NULL)
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (connection_activation_date = '' OR connection_activation_date IS NULL)
        AND (terminal_sended_date = '' OR terminal_sended_date IS NULL)
        AND prospect_supply_status = 'with_supply'
        AND terminal_type = ?
        AND (stati_port_confirm_date != '' AND stati_port_confirm_date IS NOT NULL)
        AND stati_port_confirm_date != 'N/E'
        ORDER BY  STR_TO_DATE(tal_ordered_for_date,'%d.%m.%Y')
    ",
    'fritz_boxes7' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND clientid NOT LIKE '0000_%'
        AND (contract_received_date != '' AND contract_received_date IS NOT NULL)
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (connection_activation_date = '' OR connection_activation_date IS NULL)
        AND (terminal_sended_date = '' OR terminal_sended_date IS NULL)
        AND prospect_supply_status = 'with_supply'
        AND terminal_type = ?
        AND ((tal_order_ack_date != '' AND tal_order_ack_date IS NOT NULL) OR (tal_ordered_for_date != '' AND tal_ordered_for_date IS NOT NULL))
        ORDER BY STR_TO_DATE(tal_ordered_for_date,'%d.%m.%Y')
    ",
    // unbekannter FB-Typ
    'fritz_boxes8' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND district != 'Nonnenberg'
        AND district != 'Maubach Wohnen IV'
        AND (connection_activation_date = '' OR connection_activation_date IS NULL)
        AND (terminal_sended_date = '' OR terminal_sended_date IS NULL)
        AND prospect_supply_status = 'with_supply'
        AND (terminal_type = ? OR terminal_type IS NULL)
        ORDER BY  STR_TO_DATE(tal_order_ack_date,'%d.%m.%Y')
    ",
    'fritz_boxes9' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (district = 'Nonnenberg' OR district = 'Maubach Wohnen IV')
        AND (connection_activation_date = '' OR connection_activation_date IS NULL)
        AND (terminal_sended_date = '' OR terminal_sended_date IS NULL)
        AND prospect_supply_status = 'with_supply'
        AND (terminal_type = ? OR terminal_type IS NULL)
        ORDER BY  STR_TO_DATE(tal_order_ack_date,'%d.%m.%Y')
    ",
    // Netzwerke
    // Patchen
    'net_patchen' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (connection_activation_date = '' OR connection_activation_date IS NULL)
        AND (patch_date = '' OR patch_date IS NULL)
        AND (sofortdsl = 'Ja' OR (tal_order_ack_date != '' AND tal_order_ack_date IS NOT NULL))
        AND tal_order_ack_date != 'N/E da GF'
        AND clientid NOT LIKE '0000_.%'
    ",
    // CMTS einrichten
	//      AND connection_activation_date = ''
	//      AND  (sofortdsl = 'Ja' OR tal_order_ack_date != '')
    'net_cmts' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (dslam_arranged_date = '' OR dslam_arranged_date IS NULL)
        AND (tal_order_ack_date != '' AND tal_order_ack_date IS NOT NULL)
        AND clientid NOT LIKE '0000_.%'
    ",
    // PPPoE einrichten
    'net_pppoe' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (pppoe_config_date = '' OR pppoe_config_date IS NULL)
        AND (tal_order_ack_date != '' AND tal_order_ack_date IS NOT NULL)
        AND tal_order_ack_date != 'N/E da GF'
        AND clientid NOT LIKE '0000_.%'
    ",
    // Aktiv schalten   AND purtel_edit_done = ''
    'net_active' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (connection_activation_date = '' OR connection_activation_date IS NULL)
        AND (tal_order_ack_date != '' AND tal_order_ack_date IS NOT NULL)
        AND clientid NOT LIKE '0000_.%'
        ORDER BY STR_TO_DATE(tal_order_ack_date,'%d.%m.%Y')
    ",
    // Vertragsdaten
    // Interessenten
    'prospective_customers' => "
        SELECT * FROM customers
        WHERE clientid LIKE '00000%'
    ",
    // Online
    'prospective_customers_online' => "
        SELECT * FROM customers
        WHERE clientid LIKE '00001%'
        ORDER BY STR_TO_DATE(contract_online_date,'%d.%m.%Y')
    ",
    // Kündigung eingegangen
    'contruct_cancel_input' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_cancel_input_date != '' AND wisocontract_cancel_input_date IS NOT NULL)
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
    ",
    // Kündigung abgeschlossen
    'contruct_canceled' => "
        SELECT * FROM customers
        WHERE (wisocontract_canceled_date != '' AND wisocontract_canceled_date IS NOT NULL)
    ",
    // Kunde inaktiv
    'connection_inactive' => "
        SELECT * FROM customers
        WHERE (wisocontract_switchoff_finish != '' AND wisocontract_switchoff_finish IS NOT NULL)
    ",
    // Keine Vertragsdaten
    'contruct_missing' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (registration_date != '' AND registration_date IS NOT NULL)
        AND (reg_answer_date = '' OR reg_answer_date IS NULL)
        AND prospect_supply_status != 'without_supply'
        ORDER BY STR_TO_DATE(registration_date,'%d.%m.%Y')
    ",
    // Keine Antwort
    'contruct_noanswer' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND clientid LIKE '0000_.%'
        AND (contract_received_date = '' OR contract_received_date IS NULL)
        AND prospect_supply_status != 'without_supply'
        AND ((reg_answer_date != '' AND reg_answer_date IS nOT NULL) OR (contract_sent_date != '' AND contract_sent_date IS NOT NULL))
    ",
    // Telefonbucheintrag
    'contruct_phonebook' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND telefonbuch_eintrag = 'Ja'
        AND (connection_activation_date != '' AND connection_activation_date IS NOT NULL)
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND (phoneentry_done_date = '' OR phoneentry_done_date IS NULL)
    ",
    // Purtel vorab
    'contruct_purtel' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (connection_activation_date != '' AND connection_activation_date IS NOT NULL)
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND purtelproduct_advanced = 'Ja'
        AND (final_purtelproduct_date = '' OR final_purtelproduct_date IS NULL)
    ",
    // glasfaser einrichten
    'gf_configure' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (connection_activation_date = '' OR connection_activation_date IS NULL)
        AND tal_order_ack_date LIKE '%GF%'
    ",
    // Sonderkonditionen
    'contruct_special' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (special_conditions_till != '' AND special_conditions_till IS NOT NULL)
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
    ",
    // nicht versorgt
    'contruct_nosupply' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND prospect_supply_status = 'without_supply'
        ORDER BY district
    ",
    // Fehlende Vertragsunterlagen
    // Altvertragsangaben
    'miss_contruct_old' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND prospect_supply_status = 'with_supply'
        AND (connection_activation_date = '' OR connection_activation_date IS NULL)
        AND ((exp_date_phone = '' OR exp_date_phone IS NULL) OR (notice_period_phone_int = '' OR notice_period_phone_int IS NULL) OR (exp_date_int = '' OR exp_date_int IS NULL) OR (notice_period_internet_int = '' OR notice_period_internet_int IS NULL))
    ",
    
    // Produktangaben
    'miss_contruct_product' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND prospect_supply_status = 'with_supply'
        AND ((contract_version != '' AND contract_version IS NOT NULL) OR (contract_received_date != '' AND contract_received_date IS NOT NULL))
        AND (productname = '' OR productname IS NULL)
    ",
    
    // Einzugsermächtigung
    'miss_contruct_eze' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (connection_activation_date != '' AND connection_activation_date IS NOT NULL)
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND prospect_supply_status = 'with_supply'
        AND ((bank_account_iban = '' OR bank_account_iban IS NULL) OR (bank_account_bic_new = '' OR bank_account_bic_new IS NULL))
        AND no_eze != 'no'
    ",

    // E-Mail
    'miss_contruct_mail' => "
        SELECT * FROM customers
        WHERE version IS NULL
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND prospect_supply_status = 'with_supply'
        AND (emailaddress = '' OR emailaddress IS NULL)
        AND (contract_version != '' OR contract_received_date != '')
    ",
);

$GLOBALS['sql'] = & $sql;
?>
