<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_OVERVIEW_CPE');

  $headline = array (
    'Kundennr.',
    'Vorname',
    'Nachname',
    'Stadt',
    'Bestätigtes<br />Portdatum',
    'Tal bestellt<br />zum',
    'Tal bestät.<br />zum',
    'DSL<br />sofort'
  );
  $head_csv = serialize($headline);
  $head_csv = urlencode($head_csv);
  $_SESSION['head_6490'] = $head_csv;
  if (isset($portingFritzbox6490)) {
    $porting_csv  = serialize($portingFritzbox6490);
    $porting_csv  = urlencode($porting_csv);
    $_SESSION['data_6490'] = $porting_csv;
  }
?>

<div class="Ovr OvrPortingFritzbox6490 box">
  <h3>6490 Fritzboxen für die nächten drei Monate</h3>

  <p><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button></p> 
  <p><a href='<?php echo $StartURL; ?>/overview/overview_csv.php?csv=6490' target='_blank'>CSV Export</a></p>
  <table class="Ovr">
  <thead><tr>
    <th class='n31'><?php echo $headline[0]; ?></th>
    <th class='n32'><?php echo $headline[1]; ?></th>
    <th class='n33'><?php echo $headline[2]; ?></th>
    <th class='n34'><?php echo $headline[3]; ?></th>
    <th class='n35 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[4]; ?></th>
    <th class='n36 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[5]; ?></th>
    <th class='n37 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[6]; ?></th>
    <th class='n38'><?php echo $headline[7]; ?></th>
  </tr></thead>
  <tbody>
  <?php
    $row = 'odd';
    if (isset ($portingFritzbox6490)) {
      foreach ($portingFritzbox6490 as $index) {
        if (key_exists ('firstname', $index))               $firstname                = $index['firstname'];                else $firstname               = '';
        if (key_exists ('lastname', $index))                $lastname                 = $index['lastname'];                 else $lastname                = '';
        if (key_exists ('district', $index))                $district                 = $index['district'];                 else $district                = '';
        if (key_exists ('stati_port_confirm_date', $index)) $stati_port_confirm_date  = $index['stati_port_confirm_date'];  else $stati_port_confirm_date = '';
        if (key_exists ('tal_ordered_for_date', $index))    $tal_ordered_for_date     = $index['tal_ordered_for_date'];     else $tal_ordered_for_date    = '';
        if (key_exists ('tal_order_ack_date', $index))      $tal_order_ack_date       = $index['tal_order_ack_date'];       else $tal_order_ack_date      = '';
        if (key_exists ('sofortdsl', $index))               $sofortdsl                = $index['sofortdsl'];                else $sofortdsl               = '';

        echo "<tr class='$row'>";
        echo "<td><a href='".$StartURL."/index.php?menu=customer&func=vertrag&id=".$index['id']."'>".$index['clientid']."</a></td>";
        echo "<td>".$firstname."</td>";
        echo "<td>".$lastname."</td>";
        echo "<td>".$district."</td>";
        echo "<td>".$stati_port_confirm_date."</td>";
        echo "<td>".$tal_ordered_for_date."</td>";
        echo "<td>".$tal_order_ack_date."</td>";
        echo "<td>".$sofortdsl."</td>";
        echo "</tr>";
        if ($row == 'odd') $row = 'even'; else $row = 'odd';
      }
    }
  ?>
  </tbody>
  </table>

  <br /><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button> 
</div>
