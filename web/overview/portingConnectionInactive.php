<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_OVERVIEW_CONTRACT');

  $headline = array (
    'Kundennr.',
    'Nachname',
    'Mail-Adr.',
    'Stadt',
    'Inaktiv seit',
  );
  $head_csv = serialize($headline);
  $head_csv = urlencode($head_csv);
  $_SESSION['head_connectioninactive'] = $head_csv;
  if (isset($portingConnectionInactive)) {
    $porting_csv  = serialize($portingConnectionInactive);
    $porting_csv  = urlencode($porting_csv);
    $_SESSION['data_connectioninactive'] = $porting_csv;
  }
?>

<div class="Ovr OvrPortingConnectionInactive box">
  <h3>inaktive Kunden</h3>
 
  <p><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button></p> 
  <p><a href='<?php echo $StartURL; ?>/overview/overview_csv.php?csv=connectioninactive' target='_blank'>CSV Export</a></p>
  <table class="Ovr">
  <thead><tr>
    <th class='n31'><?php echo $headline[0]; ?></th>
    <th class='n33'><?php echo $headline[1]; ?></th>
    <th class='n32'><?php echo $headline[2]; ?></th>
    <th class='n34'><?php echo $headline[3]; ?></th>
    <th class='n35'><?php echo $headline[4]; ?></th>
  </tr></thead>
  <tbody>
  <?php
    $row = 'odd';
    if (isset ($portingConnectionInactive)) {
      foreach ($portingConnectionInactive as $index) {
        if (key_exists ('lastname', $index))                    $lastname                   = $index['lastname'];                 else $lastname                  = '';
        if (key_exists ('emailaddress', $index))                $emailaddress               = $index['emailaddress'];             else $emailaddress              = '';
        if (key_exists ('district', $index))                    $district                   = $index['district'];                 else $district                  = '';
        if (key_exists ('connection_inactive_date', $index))    $connection_inactive_date   = $index['connection_inactive_date']; else $connection_inactive_date  = '';

        echo "<tr class='$row'>";
        echo "<td><a href='".$StartURL."/index.php?menu=customer&func=vertrag&id=".$index['id']."'>".$index['clientid']."</a></td>";
        echo "<td>".$lastname."</td>";
        echo "<td>".$emailaddress."</td>";
        echo "<td>".$district."</td>";
        echo "<td>".$connection_inactive_date."</td>";
        echo "</tr>";
        if ($row == 'odd') $row = 'even'; else $row = 'odd';
      }
    }
  ?>
  </tbody>
  </table>
  <br /><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button> 
</div>
