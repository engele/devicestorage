<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_OVERVIEW_PORTING');

$headline = array (
    'Kundennr.',
    'Vorname',
    'Nachname',
    'Stadt',
    'Eingestellt am',
    'Kdg.termin<br />Telefon',
    'Frist',
    'Art'
);
$head_csv = serialize($headline);
$head_csv = urlencode($head_csv);
$_SESSION['head_wbci'] = $head_csv;
if (isset($wbci)) {
    $wbci_csv  = serialize($wbci);
    $wbci_csv  = urlencode($wbci_csv);
    $_SESSION['data_wbci'] = $wbci_csv;
}
?>
<div class="Ovr OvrWbci box">
    <h3>Kunden für Anbieterwechsel WBCI</h3>
    <p><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px"> Zurück</button></p>
    <p><a href='<?php echo $StartURL; ?>/overview/overview_csv.php?csv=wbci' target='_blank'>CSV Export</a></p>
    <table class="Ovr">
        <thead>
            <tr>
                <th class='n31'><?php echo $headline[0]; ?></th>
                <th class='n32'><?php echo $headline[1]; ?></th>
                <th class='n33'><?php echo $headline[2]; ?></th>
                <th class='n34'><?php echo $headline[3]; ?></th>
                <th class='n35 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[4]; ?></th>
                <th class='n35 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[5]; ?></th>
                <th class='n36'><?php echo $headline[6]; ?></th>
                <th class='n37'><?php echo $headline[7]; ?></th>
            </tr>
        </thead>
        <tbody>
<?php
$row = 'odd';
if (isset ($wbci)) {
    foreach ($wbci as $index) {
        if (key_exists ('firstname', $index))                 $firstname                = $index['firstname'];                else $firstname                 = '';
        if (key_exists ('lastname', $index))                  $lastname                 = $index['lastname'];                 else $lastname                  = '';
        if (key_exists ('district', $index))                  $district                 = $index['district'];                 else $district                  = '';
        if (key_exists ('wbci_eingestellt_am', $index))       $wbci_eingestellt_am      = $index['wbci_eingestellt_am'];      else $wbci_eingestellt_am       = '';
        if (key_exists ('exp_date_phone', $index))            $exp_date_phone           = $index['exp_date_phone'];           else $exp_date_phone            = '';
        if (key_exists ('notice_period_phone_int', $index))   $notice_period_phone_int  = $index['notice_period_phone_int'];  else $notice_period_phone_int   = '';
        if (key_exists ('notice_period_phone_type', $index))  $notice_period_phone_type = $index['notice_period_phone_type']; else $notice_period_phone_type  = '';

        echo "<tr class='$row'>";
        echo "<td><a href='".$StartURL."/index.php?menu=customer&func=vertrag&id=".$index['id']."'>".$index['clientid']."</a></td>";
        echo "<td>".$firstname."</td>";
        echo "<td>".$lastname."</td>";
        echo "<td>".$district."</td>";
        echo "<td>".$wbci_eingestellt_am."</td>";
        echo "<td>".$exp_date_phone."</td>";
        echo "<td>".$notice_period_phone_int."</td>";
        echo "<td>".$notice_period_phone_type."</td>";
        echo "</tr>";
        if ($row == 'odd') $row = 'even'; else $row = 'odd';
    }
}
?>
        </tbody>
    </table>
    <br /><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button> 
</div>