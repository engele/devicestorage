<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_OVERVIEW_TAL');

  $headline = array (
    'Kundennr.',
    'Vorname',
    'Nachname',
    'Stadt',
    'Portierung<br />Wunschdat.',
    'Best.<br />Port.dat',
    'DSL<br />sofort',
    'Altvertrag'
  );
  $head_csv = serialize($headline);
  $head_csv = urlencode($head_csv);
  $_SESSION['head_ordertal'] = $head_csv;
  if (isset($portingOrderTal)) {
    $porting_csv  = serialize($portingOrderTal);
    $porting_csv  = urlencode($porting_csv);
    $_SESSION['data_ordertal'] = $porting_csv;
  }
?>
<div class="Ovr OvrPortingOrderTal box">
  <h3>TALs, die zu bestellen sind</h3>
 
  <p><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button></p> 
  <p><a href='<?php echo $StartURL; ?>/overview/overview_csv.php?csv=ordertal' target='_blank'>CSV Export</a></p>
  <table class="Ovr">
  <thead><tr>
    <th class='n21'><?php echo $headline[0]; ?></th>
    <th class='n22'><?php echo $headline[1]; ?></th>
    <th class='n23'><?php echo $headline[2]; ?></th>
    <th class='n24'><?php echo $headline[3]; ?></th>
    <th class='n25 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[4]; ?></th>
    <th class='n26 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[5]; ?></th>
    <th class='n27'><?php echo $headline[6]; ?></th>
    <th class='n28'><?php echo $headline[7]; ?></th>
  </tr></thead>
  <tbody>
  <?php
    $row = 'odd';
    if (isset ($portingOrderTal)) {
      foreach ($portingOrderTal as $index) {
        if (key_exists ('firstname', $index))               $firstname                = $index['firstname'];                else $firstname               = '';
        if (key_exists ('lastname', $index))                $lastname                 = $index['lastname'];                 else $lastname                = '';
        if (key_exists ('district', $index))                $district                 = $index['district'];                 else $district                = '';
        if (key_exists ('ventelo_port_wish_date', $index))  $ventelo_port_wish_date   = $index['ventelo_port_wish_date'];   else $ventelo_port_wish_date  = '';
        if (key_exists ('stati_port_confirm_date', $index)) $stati_port_confirm_date  = $index['stati_port_confirm_date'];  else $stati_port_confirm_date = '';
        if (key_exists ('sofortdsl', $index))               $sofortdsl                = $index['sofortdsl'];                else $sofortdsl               = '';
        if (key_exists ('oldcontract_exists', $index))      $oldcontract_exists       = $index['oldcontract_exists'];       else $oldcontract_exists      = '';

        echo "<tr class='$row'>";
        echo "<td><a href='".$StartURL."/index.php?menu=customer&func=vertrag&id=".$index['id']."'>".$index['clientid']."</a></td>";
        echo "<td>".$firstname."</td>";
        echo "<td>".$lastname."</td>";
        echo "<td>".$district."</td>";
        echo "<td>".$ventelo_port_wish_date."</td>";
        echo "<td>".$stati_port_confirm_date."</td>";
        echo "<td>".$sofortdsl."</td>";
        echo "<td>".$oldcontract_exists."</td>";
        echo "</tr>";
        if ($row == 'odd') $row = 'even'; else $row = 'odd';
      }
    }
  ?>
  </tbody>
  </table>

  <br /><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button> 
</div>
