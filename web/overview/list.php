<h1 class="OvrList">Übersicht</h1>
<div class="OvrList accordion">
    <?php
    if ($authorizationChecker->isGranted('ROLE_VIEW_OVERVIEW_PORTING')) {
        ?>
        <h3>Portierungen</h3>
        <div>
            <p>
            <span class="fixNum" onclick="ovrShow(['OvrPorting'])"><?php echo $porting_days_count; ?></span>Anbieterwechsel sind <u>manuell zu bearbeiten</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrWbci'])"><?php echo $wbci_days_count; ?></span>Anbieterwechsel sind <u>über WBCI zu bearbeiten</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingCustomer'])"><?php echo $porting_weeks_count; ?></span>Anbieterwechsel (manuell) sind seit <u>(über) einer Woche beim Kunden</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrWbciCustomer'])"><?php echo $wbci_weeks_count; ?></span>Anbieterwechsel (WBCI) sind seit <u>(über) einer Woche beim Kunden</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingPurtel'])"><?php echo $porting_purtel_count; ?></span>Anbieterwechsel sind <u> in Purtel eingestellt</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingCallnum'])"><?php echo $porting_callnum_count; ?></span>Rufnummern müssen in den <u> nächsten 7 Tagen geschaltet werden</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingNewcall'])"><?php echo $porting_newcall_count; ?></span>Rufnummern sind <u> bestellt  und noch nicht bestätigt</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingRoute'])"><?php echo $porting_route_count; ?></span><u>Umroutungen</u> sind zu erledigen
            <hr>
            </p>
        </div>
        <?php
    }

    if ($authorizationChecker->isGranted('ROLE_VIEW_OVERVIEW_TAL')) {
        ?>
        <h3>TALs</h3>
        <div>
            <p>
            <span class="fixNum" onclick="ovrShow(['OvrPortingOrderTal'])"><?php echo $tal_order_count; ?></span>TALs sind <u>zu bestellen</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingOrderedTal'])"><?php echo $tal_ordered_count; ?></span>TALs wurden <u>bestellt</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingConfirmedTalsWithinPastFourWeeks'])">
            <?php echo count($confirmedTalsWithinPastFourWeeks); ?>
            </span>TALs sind in den <u>letzten 4 Wochen bestätigt</u> worden
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingTalConnectInfo'])"><?php echo $tal_connect_info_count; ?></span>Kunden, die noch über ihren <u> Anschalttermin informiert werden müssen</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingConfirmedTal'])"><?php echo $tal_confirmed_count; ?></span>TALs sind auf die <u>nächsten 3 Monaten bestätigt</u> worden
            <hr>
            </p>
        </div>
        <?php
    }

    if ($authorizationChecker->isGranted('ROLE_VIEW_OVERVIEW_CPE')) {
        ?>
        <h3>Endgeräte</h3>
        <div>
            <p>
            <span class="fixNumNL"><?php echo $fritzbox_gesamt; ?></span>FRITZ!Boxen sind insgesamt in den <u>nächsten 3 Monaten zu konfigurieren</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingFritzbox6320'])"><?php echo $fritzbox_6320_count; ?></span>FRITZ!Boxen sind vom <u>Typ 6320</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingFritzbox6360'])"><?php echo $fritzbox_6360_count; ?></span>FRITZ!Boxen sind vom <u>Typ 6360</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingFritzbox6490'])"><?php echo $fritzbox_6490_count; ?></span>FRITZ!Boxen sind vom <u>Typ 6490</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingFritzbox7270'])"><?php echo $fritzbox_7270_count; ?></span>FRITZ!Boxen sind vom <u>Typ 7270</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingFritzbox7360'])"><?php echo $fritzbox_7360_count; ?></span>FRITZ!Boxen sind vom <u>Typ 7360</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingFritzbox7390'])"><?php echo $fritzbox_7390_count; ?></span>FRITZ!Boxen sind vom <u>Typ 7390</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingFritzbox7490'])"><?php echo $fritzbox_7490_count; ?></span>FRITZ!Boxen sind vom <u>Typ 7490</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingFritzbox7560'])"><?php echo $fritzbox_7560_count; ?></span>FRITZ!Boxen sind vom <u>Typ 7560</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingFritzbox7590'])"><?php echo $fritzbox_7590_count; ?></span>FRITZ!Boxen sind vom <u>Typ 7590</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingFritzboxGW_400'])"><?php echo $fritzbox_GW_400_count; ?></span>Boxen vom Typ <u>Gateway 400</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingFritzboxIBG2426A'])"><?php echo $fritzbox_IBG2426A_count; ?></span>Boxen vom Typ <u>INNBOX G2426A</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingFritzboxNoType'])"><?php echo $fritzbox_NoType_count; ?></span>Boxen sind <u>ohne Typ</u>
            <hr>
            </p>
        </div>
        <?php
    }

    if ($authorizationChecker->isGranted('ROLE_VIEW_OVERVIEW_TECHNICAL')) {
        ?>
        <h3>Netztechnik</h3>
        <div>
            <p>
            <span class="fixNum" onclick="ovrShow(['OvrPortingNetPatch'])"><?php echo $net_patchen_count; ?></span>Kunden sind in den nächsten 7 Tagen zu <u>patchen</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingNetDslamCmts'])"><?php echo $net_cmts_count; ?></span>Kunden sind in den nächsten 14 Tagen am <u>DSLAM / CMTS einzurichten</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingNetPppoe'])"><?php echo $net_pppoe_count; ?></span>Kunden sind in den nächsten 14 Tagen am <u>PPPoE einzurichten</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingNetActive'])"><?php echo $net_active_count; ?></span>Kunden sind <u>aktiv zu schalten</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingNetGfActive'])"><?php echo $net_gf_active_count; ?></span>Glasfaserkunden sind <u>aktiv zu schalten</u>
            <hr>
            </p>
        </div>
        <?php
    }

    if ($authorizationChecker->isGranted('ROLE_VIEW_OVERVIEW_CONTRACT')) {
        ?>
        <h3>Vertragsdaten</h3>
        <div>
            <p>
            <span class="fixNum" onclick="ovrShow(['OvrPortingContructCancel'])"><?php echo $contruct_cancel_count; ?></span><u>Kündigungen</u> sind eingegangen und noch nicht abgeschlossen
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingContructCanceled'])"><?php echo $contruct_canceled_count; ?></span><u>Kündigungen</u> sind abgeschlossen
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingConnectionInactive'])"><?php echo $connection_inactive_count; ?></span>Kunden sind <u>inaktiv</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingProspectiveCustomers'])"><?php echo $prospective_customers_count; ?></span>Interessenten
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingProspectiveCustomersOnline'])"><?php echo $prospective_customers_online_count; ?></span>Eingegangene Onlineverträge
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingContructMissing'])"><?php echo $contruct_missing_count; ?></span>Interessenten die noch <u>keine Infos</u> erhalten haben
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingContructNoAnswer'])"><?php echo $contruct_noanswer_count; ?></span>Interessenten, die <u>in den letzten 3 Monaten keine Rückmeldung</u> gaben 
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingContructNoAnswer3'])"><?php echo $contruct_noanswer3_count; ?></span>Interessenten, die <u>seit über 3 Monaten keine Rückmeldung</u> gaben
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingContructPhonebook'])"><?php echo $contruct_phonebook_count; ?></span><u>Telefonbucheinträge</u> sind zu bearbeiten
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingContructPurtel'])"><?php echo $contruct_purtel_count; ?></span>Kunden erhalten <u>Purtel Vorabprodukte</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingContructSpecial'])"><?php echo $contruct_special_count; ?></span>Kunden erhalten <u>Sonderkonditionen</u>
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingContructNoSupply'])"><?php echo $contruct_nosupply_count; ?></span>Kunden sind <u>nicht versorgbar</u>
            <hr>
            </p>
        </div>
        <?php
    }

    if ($authorizationChecker->isGranted('ROLE_VIEW_OVERVIEW_MISSING_CONTRACT')) {
        ?>
        <h3>Fehlende Vertragsunterlagen</h3>
        <div>
            <p>
            <span class="fixNum" onclick="ovrShow(['OvrPortingContructOldMissing'])"><?php echo $contruct_old_missing_count; ?></span>Fehlende Altvertragsangaben
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingContructProductMissing'])"><?php echo $contruct_product_missing_count; ?></span>Fehlende Produktangaben
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingContructEzeMissing'])"><?php echo $contruct_eze_missing_count; ?></span>Fehlende Einzugsermächtigungen (aktive Kunden) 
            <hr>
            <span class="fixNum" onclick="ovrShow(['OvrPortingContructEmailMissing'])"><?php echo $contruct_email_missing_count; ?></span>Fehlende E-Mail-Adressen
            <hr>
            </p>
        </div>
        <?php
    }
    ?>
</div>
