<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_OVERVIEW_CONTRACT');

  $headline = array (
    'Kundennr.',
    'Vorname',
    'Nachname',
    'Stadt',
    'Zuletzt<br />bearbeitet am',
    'Vertrag<br />versendet am',
  );
  $head_csv = serialize($headline);
  $head_csv = urlencode($head_csv);
  $_SESSION['head_contructnoanswer3'] = $head_csv;
  if (isset($portingContructNoAnswer3)) {
    $porting_csv  = serialize($portingContructNoAnswer3);
    $porting_csv  = urlencode($porting_csv);
    $_SESSION['data_contructnoanswer3'] = $porting_csv;
  }
?>

<div class="Ovr OvrPortingContructNoAnswer3 box">
  <h3>Interessenten, ohne Rückmeldung (mehr als 3 Monate)</h3>
  
  <p><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button></p> 
  <p><a href='<?php echo $StartURL; ?>/overview/overview_csv.php?csv=contructnoanswer3' target='_blank'>CSV Export</a></p>
  <table class="Ovr">
  <thead><tr>
    <th class='n11'><?php echo $headline[0]; ?></th>
    <th class='n13'><?php echo $headline[1]; ?></th>
    <th class='n12'><?php echo $headline[2]; ?></th>
    <th class='n14'><?php echo $headline[3]; ?></th>
    <th class='n15'><?php echo $headline[4]; ?></th>
    <th class='n16'><?php echo $headline[5]; ?></th>
  </tr></thead>
  <tbody>
  <?php
    $row = 'odd';
    if (isset ($portingContructNoAnswer3)) {
      foreach ($portingContructNoAnswer3 as $index) {
        if (key_exists ('firstname', $index))           $firstname            = $index['firstname'];          else $firstname           = '';
        if (key_exists ('lastname', $index))            $lastname             = $index['lastname'];           else $lastname            = '';
        if (key_exists ('district', $index))            $district             = $index['district'];           else $district            = '';
        if (key_exists ('reg_answer_date', $index))     $reg_answer_date      = $index['reg_answer_date'];    else $reg_answer_date     = '';
        if (key_exists ('contract_sent_date', $index))  $contract_sent_date   = $index['contract_sent_date']; else $contract_sent_date  = '';

        echo "<tr class='$row'>";
        echo "<td><a href='".$StartURL."/index.php?menu=customer&func=vertrag&id=".$index['id']."'>".$index['clientid']."</a></td>";
        echo "<td>".$firstname."</td>";
        echo "<td>".$lastname."</td>";
        echo "<td>".$district."</td>";
        echo "<td>".$reg_answer_date."</td>";
        echo "<td>".$contract_sent_date."</td>";
        echo "</tr>";
        if ($row == 'odd') $row = 'even'; else $row = 'odd';
      }
    }
  ?>
  </tbody>
  </table>
  <br /><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button> 
</div>
