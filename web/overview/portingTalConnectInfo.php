<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_OVERVIEW_TAL');

  $headline = array (
    'Kundennr.',
    'Vorname',
    'Nachname',
    'Stadt',
    'TAL bestellt<br />zum',
    'TAL bestätigt<br />zum'
  );
  $head_csv = serialize($headline);
  $head_csv = urlencode($head_csv);
  $_SESSION['head_talconnectinfo'] = $head_csv;
  if (isset($portingTalConnectInfo)) {
    $porting_csv  = serialize($portingTalConnectInfo);
    $porting_csv  = urlencode($porting_csv);
    $_SESSION['data_talconnectinfo'] = $porting_csv;
  }
?>
<div class="Ovr OvrPortingTalConnectInfo box">
  <h3>Kunden, die noch nicht informiert wurden</h3>

  <p><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button></p> 
  <p><a href='<?php echo $StartURL; ?>/overview/overview_csv.php?csv=talconnectinfo' target='_blank'>CSV Export</a></p>
  <table class="Ovr">
  <thead><tr>
    <th class='n11'><?php echo $headline[0]; ?></th>
    <th class='n12'><?php echo $headline[1]; ?></th>
    <th class='n13'><?php echo $headline[2]; ?></th>
    <th class='n14'><?php echo $headline[3]; ?></th>
    <th class='n15 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[4]; ?></th>
    <th class='n16 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[5]; ?></th>
  </tr></thead>
  <tbody>
  <?php
    $row = 'odd';
    if (isset ($portingTalConnectInfo)) {
      foreach ($portingTalConnectInfo as $index) {
        if (key_exists ('firstname', $index))             $firstname            = $index['firstname'];            else $firstname             = '';
        if (key_exists ('lastname', $index))              $lastname             = $index['lastname'];             else $lastname              = '';
        if (key_exists ('district', $index))              $district             = $index['district'];             else $district              = '';
        if (key_exists ('tal_ordered_for_date', $index))  $tal_ordered_for_date = $index['tal_ordered_for_date']; else $tal_ordered_for_date  = '';
        if (key_exists ('tal_order_ack_date', $index))    $tal_order_ack_date   = $index['tal_order_ack_date'];   else $tal_order_ack_date    = '';

        echo "<tr class='$row'>";
        echo "<td><a href='".$StartURL."/index.php?menu=customer&func=vertrag&id=".$index['id']."'>".$index['clientid']."</a></td>";
        echo "<td>".$firstname."</td>";
        echo "<td>".$lastname."</td>";
        echo "<td>".$district."</td>";
        echo "<td>".$tal_ordered_for_date."</td>";
        echo "<td>".$tal_order_ack_date."</td>";
        echo "</tr>";
        if ($row == 'odd') $row = 'even'; else $row = 'odd';
       }
    }
  ?>
  </tbody>
  </table>
  <br /><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button> 
</div>
