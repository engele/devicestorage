<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_OVERVIEW_CONTRACT');

  $headline = array (
    'Kundennr.',
    'Vorname',
    'Nachname',
    'Anschluss<br />aktiv seit',
    'Produktname',
    'Purtel Produkt'
  );
  $head_csv = serialize($headline);
  $head_csv = urlencode($head_csv);
  $_SESSION['head_contructpurtel'] = $head_csv;
  if (isset($portingContructPurtel)) {
    $porting_csv  = serialize($portingContructPurtel);
    $porting_csv  = urlencode($porting_csv);
    $_SESSION['data_contructpurtel'] = $porting_csv;
  }
?>

<div class="Ovr OvrPortingContructPurtel box">
  <h3>Purtel Vorabprodukte</h3>

  <p><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button></p> 
  <p><a href='<?php echo $StartURL; ?>/overview/overview_csv.php?csv=contructpurtel' target='_blank'>CSV Export</a></p>
  <table class="Ovr">
  <thead><tr>
    <th class='n11'><?php echo $headline[0]; ?></th>
    <th class='n13'><?php echo $headline[1]; ?></th>
    <th class='n12'><?php echo $headline[2]; ?></th>
    <th class='n14'><?php echo $headline[3]; ?>/th>
    <th class='n15'><?php echo $headline[4]; ?></th>
    <th class='n16'><?php echo $headline[5]; ?></th>
  </tr></thead>
  <tbody>
  <?php
    $row = 'odd';
    if (isset ($portingContructPurtel)) {
      foreach ($portingContructPurtel as $index) {
        if (key_exists ('firstname', $index))                   $firstname                  = $index['firstname'];                  else $firstname                   = '';
        if (key_exists ('lastname', $index))                    $lastname                   = $index['lastname'];                   else $lastname                    = '';
        if (key_exists ('connection_activation_date', $index))  $connection_activation_date = $index['connection_activation_date']; else $connection_activation_date  = '';
        if (key_exists ('productname', $index))                 $productname                = $index['productname'];                else $productname                 = '';
        if (key_exists ('purtel_product', $index))              $purtel_product             = $index['purtel_product'];             else $purtel_product              = '';

        echo "<tr class='$row'>";
        echo "<td><a href='".$StartURL."/index.php?menu=customer&func=vertrag&id=".$index['id']."'>".$index['clientid']."</a></td>";
        echo "<td>".$firstname."</td>";
        echo "<td>".$lastname."</td>";
        echo "<td>".$connection_activation_date."</td>";
        echo "<td>".$productname."</td>";
        echo "<td>".$purtel_product."</td>";
        echo "</tr>";
        if ($row == 'odd') $row = 'even'; else $row = 'odd';
      }
    }
  ?>
  </tbody>
  </table>
  <br /><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button>
</div>
