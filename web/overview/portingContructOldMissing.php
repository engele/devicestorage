<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_OVERVIEW_MISSING_CONTRACT');

  $headline = array (
    'Kundennr.',
    'Vorname',
    'Nachname',
    'Int Anb.<br />Kdn.Nr.',
    'Kdg.datum<br />Internet',
    'Tel Anb.<br />Kdn.Nr.',
    'Kdg.datum<br />Telefon'
  );
  $head_csv = serialize($headline);
  $head_csv = urlencode($head_csv);
  $_SESSION['head_contructoldmissing'] = $head_csv;
  if (isset($portingContructOldMissing)) {
    $porting_csv  = serialize($portingContructOldMissing);
    $porting_csv  = urlencode($porting_csv);
    $_SESSION['data_contructoldmissing'] = $porting_csv;
  }
?>

<div class="Ovr OvrPortingContructOldMissing box">
  <h3>Altvertragsdaten fehlen</h3>

  <p><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button></p> 
  <p><a href='<?php echo $StartURL; ?>/overview/overview_csv.php?csv=contructoldmissing' target='_blank'>CSV Export</a></p>
  <table class="Ovr">
  <thead><tr>
    <th class='n21'><?php echo $headline[0]; ?></th>
    <th class='n22'><?php echo $headline[1]; ?></th>
    <th class='n23'><?php echo $headline[2]; ?></th>
    <th class='n24'><?php echo $headline[3]; ?></th>
    <th class='n25'><?php echo $headline[4]; ?></th>
    <th class='n26'><?php echo $headline[5]; ?></th>
    <th class='n27'><?php echo $headline[6]; ?></th>
  </tr></thead>
  <tbody>
  <?php
    $row = 'odd';
    if (isset ($portingContructOldMissing)) {
      foreach ($portingContructOldMissing as $index) {
        if (key_exists ('firstname', $index))                     $firstname                      = $index['firstname'];                      else $firstname                       = '';
        if (key_exists ('lastname', $index))                      $lastname                       = $index['lastname'];                       else $lastname                        = '';
        if (key_exists ('oldcontract_internet_provider', $index)) $oldcontract_internet_provider  = $index['oldcontract_internet_provider'];  else $doldcontract_internet_provider  = '';
        if (key_exists ('exp_date_int', $index))                  $exp_date_int                   = $index['exp_date_int'];                   else $exp_date_int                    = '';
        if (key_exists ('oldcontract_phone_provider', $index))    $oldcontract_phone_provider     = $index['oldcontract_phone_provider'];     else $oldcontract_phone_provider      = '';
        if (key_exists ('exp_date_phone', $index))                $exp_date_phone                 = $index['exp_date_phone'];                 else $exp_date_phone                  = '';

        echo "<tr class='$row'>";
        echo "<td><a href='".$StartURL."/index.php?menu=customer&func=vertrag&id=".$index['id']."'>".$index['clientid']."</a></td>";
        echo "<td>".$firstname."</td>";
        echo "<td>".$lastname."</td>";
        echo "<td>".$oldcontract_internet_provider."</td>";
        echo "<td>".$exp_date_int."</td>";
        echo "<td>".$oldcontract_phone_provider."</td>";
        echo "<td>".$exp_date_phone."</td>";
        echo "</tr>";
        if ($row == 'odd') $row = 'even'; else $row = 'odd';
      }
    }
  ?>
  </tbody>
  </table>
  <br /><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button>
</div>
