<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_OVERVIEW_TAL');

  $headline = array (
    'Kundennr.',
    'Vorname',
    'Nachname',
    'Stadt',
    'Portierung',
    'Tel',
    'TAL',
    'F-Box',
    'done',
    'vers.',
    'Tech',
    'Aktiv',
    'DSL',
    'PoE',
    'Pat'
  );
  $head_csv = serialize($headline);
  $head_csv = urlencode($head_csv);
  $_SESSION['head_talconfirmed'] = $head_csv;
  if (isset($portingTalConfirmed)) {
    $porting_csv  = serialize($portingTalConfirmed);
    $porting_csv  = urlencode($porting_csv);
    $_SESSION['data_talconfirmed'] = $porting_csv;
  }
?>

<div class="Ovr OvrPortingConfirmedTal box">
  <h3>Bestätigte TALs in den nächsten 2 Wochen</h3>

  <p><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button></p> 
  <p><a href='<?php echo $StartURL; ?>/overview/overview_csv.php?csv=talconfirmed' target='_blank'>CSV Export</a></p>
  <table class="Ovr">
  <thead><tr>
    <th class='n11'><?php echo $headline[0]; ?></th>
    <th class='n12'><?php echo $headline[1]; ?></th>
    <th class='n13'><?php echo $headline[2]; ?></th>
    <th class='n14'><?php echo $headline[3]; ?></th>
    <th class='n15 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[4]; ?></th>
    <th class='n06'><?php echo $headline[5]; ?></th>
    <th class='n16 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[6]; ?></th>
    <th class='n06'><?php echo $headline[7]; ?></th>
    <th class='n16 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[8]; ?></th>
    <th class='n16 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[9]; ?></th>
    <th class='n06'><?php echo $headline[10]; ?></th>
    <th class='n16 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[11]; ?></th>
    <th class='n16 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[12]; ?></th>
    <th class='n16 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[13]; ?></th>
    <th class='n16 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[14]; ?></th>

  </tr></thead>
  <tbody>
  <?php
    $row = 'odd';
    if (isset ($portingTalConfirmed)) {
      foreach ($portingTalConfirmed as $index) {
        if (key_exists ('firstname', $index))             $firstname            = $index['firstname'];            else $firstname             = '';
        if (key_exists ('lastname', $index))              $lastname             = $index['lastname'];             else $lastname              = '';
        if (key_exists ('district', $index))              $district             = $index['district'];             else $district              = '';
        if (key_exists ('stati_port_confirm_date', $index))  $stati_port_confirm_date = $index['stati_port_confirm_date']; else $stati_port_confirm_date  = '';
        if (key_exists ('phone_number_added', $index))  $phone_number_added = $index['phone_number_added']; else $phone_number_added  = '';
        if (key_exists ('tal_order_ack_date', $index))    $tal_order_ack_date   = $index['tal_order_ack_date'];   else $tal_order_ack_date    = '';
        if (key_exists ('terminal_type', $index))    $terminal_type   = $index['terminal_type'];   else $terminal_type    = '';
        if (key_exists ('terminal_ready', $index))    $terminal_ready   = $index['terminal_ready'];   else $terminal_ready    = '';
        if (key_exists ('terminal_sended_date', $index))    $terminal_sended_date   = $index['terminal_sended_date'];   else $terminal_sended_date    = '';
        if (key_exists ('service_technician', $index))    $service_technician   = $index['service_technician'];   else $service_technician    = '';
        if (key_exists ('connection_activation_date', $index))    $connection_activation_date   = $index['connection_activation_date'];   else $connection_activation_date    = '';
        if (key_exists ('dslam_arranged_date', $index))    $dslam_arranged_date   = $index['dslam_arranged_date'];   else $dslam_arranged_date    = '';
        if (key_exists ('pppoe_config_date', $index))    $pppoe_config_date   = $index['pppoe_config_date'];   else $pppoe_config_date    = '';
        if (key_exists ('patch_date', $index))    $patch_date   = $index['patch_date'];   else $patch_date    = '';
        if ($dslam_arranged_date <> '') $dslam_arranged_date = 'X';
        if ($pppoe_config_date <> '') $pppoe_config_date = 'X';
        if ($patch_date <> '') $patch_date = 'X';
        if ($terminal_ready <> '') $terminal_ready = 'X';
        echo "<tr class='$row'>";
        echo "<td><a href='".$StartURL."/index.php?menu=customer&func=vertrag&id=".$index['id']."' target='_blank'>".$index['clientid']."</a></td>";
        echo "<td>".$firstname."</td>";
        echo "<td>".$lastname."</td>";
        echo "<td>".$district."</td>";
        echo "<td>".$stati_port_confirm_date."</td>";
        echo "<td>".$phone_number_added."</td>";
        echo "<td>".$tal_order_ack_date."</td>";
        echo "<td>".$terminal_type."</td>";
        echo "<td>".$terminal_ready."</td>";
        echo "<td>".$terminal_sended_date."</td>";
        echo "<td>".$service_technician."</td>";
        echo "<td>".$connection_activation_date."</td>";
        echo "<td>".$dslam_arranged_date."</td>";
        echo "<td>".$pppoe_config_date."</td>";
        echo "<td>".$patch_date."</td>";

        echo "</tr>";
        if ($row == 'odd') $row = 'even'; else $row = 'odd';
       }
    }
  ?>
  </tbody>
  </table>
  <br /><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button> 
</div>
