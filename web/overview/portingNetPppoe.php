<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_OVERVIEW_TECHNICAL');

  $headline = array (
    'Kundennr.',
    'Vorname',
    'Nachname',
    'Stadt',
    'DSL<br />sofort',
    'TAL',
  );
  $head_csv = serialize($headline);
  $head_csv = urlencode($head_csv);
  $_SESSION['head_netpppoe'] = $head_csv;
  if (isset($portingNetPppoe)) {
    $porting_csv  = serialize($portingNetPppoe);
    $porting_csv  = urlencode($porting_csv);
    $_SESSION['data_netpppoe'] = $porting_csv;
  }
?>

<div class="Ovr OvrPortingNetPppoe box">
  <h3>PPPoE-Server einrichten</h3>

  <p><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button></p> 
  <p><a href='<?php echo $StartURL; ?>/overview/overview_csv.php?csv=netpppoe' target='_blank'>CSV Export</a></p>
  <table class="Ovr">
  <thead><tr>
    <th class='n11'><?php echo $headline[0]; ?></th>
    <th class='n12'><?php echo $headline[1]; ?></th>
    <th class='n13'><?php echo $headline[2]; ?></th>
    <th class='n14'><?php echo $headline[3]; ?></th>
    <th class='n15'><?php echo $headline[4]; ?></th>
    <th class='n16 sorter-shortDate dateFormat-ddmmyyyy empty-top'><?php echo $headline[5]; ?></th>
  </tr></thead>
  <tbody>
  <?php
    $row = 'odd';
    if (isset ($portingNetPppoe)) {
      foreach ($portingNetPppoe as $index) {
        if (key_exists ('firstname', $index))             $firstname            = $index['firstname'];          else $firstname           = '';
        if (key_exists ('lastname', $index))              $lastname             = $index['lastname'];           else $lastname            = '';
        if (key_exists ('district', $index))              $district             = $index['district'];           else $district            = '';
        if (key_exists ('sofortdsl', $index))             $sofortdsl            = $index['sofortdsl'];          else $sofortdsl           = '';
        if (key_exists ('tal_order_ack_date', $index))    $tal_order_ack_date   = $index['tal_order_ack_date']; else $tal_order_ack_date  = '';

        echo "<tr class='$row'>";
        echo "<td><a href='".$StartURL."/index.php?menu=customer&func=vertrag&id=".$index['id']."' target='_blank'>".$index['clientid']."</a></td>";
        echo "<td>".$firstname."</td>";
        echo "<td>".$lastname."</td>";
        echo "<td>".$district."</td>";
        echo "<td>".$sofortdsl."</td>";
        echo "<td>".$tal_order_ack_date."</td>";
        echo "</tr>";
        if ($row == 'odd') $row = 'even'; else $row = 'odd';
      }
    }
  ?>
  </tbody>
  </table>
  <br /><button type="button" onclick="ovrShow(['OvrList'])"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">  Zurück</button>
</div>
