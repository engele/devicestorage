<?php

setlocale(LC_TIME, "de_DE.utf8");

$StartPath = __DIR__;

$server = array_merge([
    'HTTP_HOST' => 'localhost',
    'SERVER_PORT' => 80,
    'HTTPS' => false,
    'SCRIPT_NAME' => '',
], $_SERVER);

$server['SERVER_PORT'] = (int) $server['SERVER_PORT'];
$server['HTTPS'] = 'on' == $server['HTTPS'] || '1' == $server['HTTPS'] ? true : false;

$url = '';

if (false !== strpos($server['SCRIPT_NAME'], '/')) {
    $url = explode('/', $server['SCRIPT_NAME']);
    
    array_pop($url);
    
    $url = implode('/', $url).'/';
}

if ('/' === substr($url, -1)) {
    $url = substr($url, 0, -1);
}

$StartURL = sprintf('%s://%s',
    $server['HTTPS'] ? 'https' : 'http',
    $server['HTTP_HOST'].$url
);

/* Port is already on HTTP_HOST
if (80 !== $server['SERVER_PORT'] && 443 !== $server['SERVER_PORT']) {
    $StartURL .= ':'.$server['SERVER_PORT'];
}
*/

require_once $StartPath.'/vendor/wisotel/configuration/Configuration.php';

$Favicon        = '/favicon.ico';
$HeadTitle      = \Wisotel\Configuration\Configuration::get('headTitle');
$SecKey         = \Wisotel\Configuration\Configuration::get('secKey');

$GLOBALS['StartPath'] = & $StartPath;
$GLOBALS['StartURL'] = & $StartURL;

$KVVesion       = 'wkv 2.3.0.0 (23.08.2018)';   // Many changes e.g. merge from wkv/zkv (current commit: ef43195c1037743673f8c792fa2cf955743aa2c3) 
/**********************************************
$KVVesion       = 'zkv 2.0.3.6 (11.05.2017)';   // Velauf 
$KVVesion       = 'zkv 2.0.3.5 (13.03.2017)';   // Purtelaccount Porduktanpassung 
$KVVesion       = 'zkv 2.0.3.4 (07.03.2017)';   // Groupware api implementiert 
$KVVesion       = 'zkv 2.0.3.3 (06.03.2017)';   // Statistik Leistungen ergänzt und csv-Export eingefügt
$KVVesion       = 'zkv 2.0.3.2 (05.03.2017)';   // Tricket Erzeugung
$KVVesion       = 'zkv 2.0.3.1 (19.01.2017)';   // Ortskennzahl zu Purtel übertragen
$KVVesion       = 'zkv 2.0.3.0 (21.02.2017)';   // Berechtigungssystem
$KVVesion       = 'zkv 2.0.2.2 (12.02.2017)';   // Stromkunden Infomationen eingefügt
$KVVesion       = 'zkv 2.0.2.1 (19.01.2017)';   // Ortskennzahl zu Purtel übertragen
$KVVesion       = 'zkv 2.0.2.0 (21.11.2016)';   // Login hinzugefügt
$KVVesion       = 'zkv 2.0.1.4 (07.11.2016)';   // WBCI Felder hinzugefügt
$KVVesion       = 'zkv 2.0.1.3 (06.11.2016)';   // WBCI hinzugefügt, Verbindung API Purtel fehlt ncoh
$KVVesion       = 'zkv 2.0.1.2 (06.11.2016)';   // Umstellung der Purtelverwaltung auf neue Struktur, Editierbarkeit der Purtel Konten
$KVVesion       = 'zkv 2.0.1.1 (27.10.2016)';   // Datenbank Fehler beim speichern beseitigt
$KVVesion       = 'zkv 2.0.1.0 (21.10.2016)';   // Verwaltung der Purtelaccounts
$KVVesion       = 'zkv 2.0.0.5 (06.10.2016)';   // Service aus Produktdatenbank
$KVVesion       = 'zkv 2.0.0.4 (05.10.2016)';   // Benutzerlisten geändert
$KVVesion       = 'zkv 2.0.0.3 (05.10.2016)';   // Feld TAG hinzugefügt
$KVVesion       = 'zkv 2.0.0.2 (04.10.2016)';   // Version in der Deleteanzeige    
$KVVesion       = 'zkv 2.0.0.1 (26.09.2016)';   // neue Version csvExport      
$KVVesion       = 'zkv 2.0.0.0 (09.09.2016)';   // zkv Versionnummern eingeführt
**********************************************/

?>
