<?php
//------------------------------------------------------------------------------
  require_once ('../_conf/database.inc');
  require_once ('_conf/form.inc');
  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename=cust_dslam_'.date ("YmdHis").'.csv');  
  $csv_handle = fopen('php://output', 'w');
  $sqlstr = "SELECT ";
  foreach ($sql_dslam AS $field) {
    $sqlstr .= $field.", ";
  }
  $sqlstr = substr($sqlstr, 0, -2);
  $sqlstr .= " FROM customers";
  $db_customers = $db->query($sqlstr);
  $customersdb = $db_customers->fetch_all(MYSQLI_ASSOC);
  $db_customers->close();

  $headline = array_keys ($customersdb[0]);
  fputcsv($csv_handle, $headline, ';');
  foreach ($customersdb as $customers) {
    $customers = array_map('utf8_decode', $customers);
    fputcsv($csv_handle, $customers, ';');
  }
  fclose ($csv_handle);
  $db->close();  
?>
