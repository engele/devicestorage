<?php
  $sql_fields = array (
  'id',
  'add_charge_terminal',                   // varchar(64)         NULL
  'add_customer_on_fb',                    // varchar(5)          NULL
  'add_ip_address',                        // varchar(5)          NULL
  'add_ip_address_cost',                   // varchar(10)         NULL
  'add_phone_lines_cost',                  // varchar(10)         NULL
  'add_phone_lines',                       // varchar(8)          NULL
  'add_phone_nos',                         // varchar(5)          NULL
  'add_port',                              // varchar(5)          NULL
  'add_satellite',                         // varchar(5)          NULL
  'application',                           // varchar(32)         NULL
  'bank_account_bankname',                 // varchar(64)         NULL
  'bank_account_bic',                      // varchar(24)         NULL
  'bank_account_bic_new',                  // varchar(13)         NULL
  'bank_account_city',                     // varchar(64)         NULL
  'bank_account_emailaddress',             // varchar(128)        NULL
  'bank_account_holder_firstname',         // varchar(64)         NULL
  'bank_account_holder_lastname',          // varchar(64)         NULL
  'bank_account_iban',                     // varchar(30)         NULL
  'bank_account_number',                   // varchar(24)         NULL
  'bank_account_street',                   // varchar(64)         NULL
  'bank_account_streetno',                 // varchar(8)          NULL
  'bank_account_zipcode',                  // varchar(8)          NULL
  'birthday',                              // varchar(24)         NULL
  'call_data_record',                      // varchar(5)          NULL
  'cancel_purtel',                         // varchar(5)          NULL
  'cancel_purtel_for_date',                // varchar(11)         NOT NULL
  'cancel_tal',                            // varchar(20)         NULL
  'card_id',                               // int(11)             NOT NULL
  'city',                                  // varchar(64)         NULL
  'clientid',                              // varchar(64)         NOT NULL
  'clienttype',                            // varchar(24)         NULL
  'company',                               // varchar(64)         NULL
  'connection_activation_date',            // varchar(11)         NULL
  'connection_address_equal',              // varchar(5)          NULL
  'connection_city',                       // varchar(64)         NULL
  'connection_fee_height',                 // varchar(64)         NULL
  'connection_fee_paid',                   // varchar(10)         NULL
  'connection_inactive_date',              // varchar(11)         NULL
  'connection_installation',               // varchar(20)         NULL
  'connection_street',                     // varchar(64)         NULL
  'connection_streetno',                   // varchar(8)          NULL
  'connection_zipcode',                    // varchar(8)          NULL
  'contract',                              // varchar(32)         NULL
  'contract_acknowledge',                  // varchar(10)         NULL
  'contract_change_to',                    // varchar(11)         NULL
  'contract_id',                           // varchar(64)         NULL
  'contract_online_date',                  // varchar(11)         NULL
  'contract_received_date',                // varchar(11)         NULL
  'contract_sent_date',                    // varchar(11)         NULL
  'contract_version',                      // varchar(11)         NULL
  'credit_rating_date',                    // varchar(128)        NOT NULL
  'credit_rating_link',                    // varchar(512)        NULL
  'credit_rating_ok',                      // varchar(64)         NULL
  'customer_connect_info_date',            // varchar(11)         NOT NULL
  'customer_connect_info_from',            // varchar(10)         NOT NULL
  'customer_connect_info_how',             // varchar(20)         NOT NULL
  'direct_debit_allowed',                  // varchar(5)          NULL
  'district',                              // varchar(64)         NULL
  'DPBO',                                  // tinyint(4) unsigned NULL
  'dslam_arranged_date',                   // varchar(11)         NOT NULL
  'dslam_card_no',                         // varchar(16)         NULL
  'dslam_location',                        // varchar(64)         NULL
  'dslam_port',                            // varchar(8)          NULL
  'dtag_line',                             // varchar(256)        NULL
  'dtagbluckage_fullfiled',                // varchar(5)          NULL
  'dtagbluckage_fullfiled_date',           // varchar(11)         NULL
  'dtagbluckage_sended',                   // varchar(5)          NULL
  'dtagbluckage_sended_date',              // varchar(11)         NULL
  'emailaddress',                          // varchar(128)        NULL
  'eu_flat',                               // varchar(5)          NULL
  'exp_date_int',                          // varchar(12)         NOT NULL
  'exp_date_phone',                        // varchar(12)         NOT NULL
  'fax',                                   // varchar(64)         NULL
  'fb_vorab',                              // varchar(5)          NULL
  'final_purtelproduct_date',              // varchar(11)         NOT NULL
  'firewall_router',                       // varchar(10)         NULL
  'firmware_version',                      // varchar(64)         NULL
  'firstname',                             // varchar(64)         NULL
  'flat_eu_network',                       // varchar(5)          NULL
  'flat_eu_network_cost',                  // varchar(10)         NULL
  'flat_german_network',                   // varchar(10)         NULL
  'flat_german_network_cost',              // varchar(10)         NULL
  'fordering_costs',                       // varchar(5)          NULL
  'german_mobile',                         // varchar(5)          NULL
  'german_mobile_price',                   // varchar(10)         NULL
  'german_network',                        // varchar(5)          NULL
  'german_network_price',                  // varchar(10)         NULL
  'gf_branch',                             // varchar(5)          NOT NULL
  'gf_cabling',                            // varchar(10)         NULL
  'gf_cabling_cost',                       // varchar(10)         NULL
  'gutschrift',                            // varchar(5)          NULL
  'gutschrift_till',                       // varchar(11)         NULL
  'hd_plus_card',                          // varchar(5)          NULL
  'higher_availability',                   // varchar(5)          NULL
  'higher_uplink_one',                     // varchar(5)          NULL
  'higher_uplink_two',                     // varchar(5)          NULL
  'house_connection',                      // varchar(10)         NULL
  'implementation',                        // varchar(64)         NULL
  'international_calls_price',             // varchar(10)         NULL
  'ip_address',                            // varchar(64)         NULL
  'ip_address_inet',                       // varchar(5)          NULL
  'ip_address_phone',                      // varchar(5)          NULL
  'ip_range_id',                           // int(11)             NOT NULL
  'isdn_backup',                           // varchar(10)         NULL
  'isdn_backup2',                          // varchar(10)         NULL
  'Kommando_query',                        // varchar(32)         NULL
  'kvz_addition',                          // varchar(128)        NOT NULL
  'kvz_name',                              // varchar(16)         NULL
  'kvz_standort',                          // varchar(5)          NULL
  'kvz_toggle_no',                         // varchar(64)         NULL
  'lapse_notice_date_inet',                // varchar(11)         NULL
  'lapse_notice_date_tel',                 // varchar(11)         NULL
  'lastname',                              // varchar(64)         NULL
  'line_identifier',                       // varchar(128)        NULL
  'little_bar',                            // varchar(256)        NULL
  'location_id',                           // int(11)             NOT NULL
  'lsa_kvz_partition_id',                  // int(11)             NOT NULL
  'lsa_pin',                               // varchar(16)         NULL
  'mac_address',                           // varchar(64)         NULL
  'media_converter',                       // varchar(5)          NULL
  'mobile_flat',                           // varchar(5)          NULL
  'mobilephone',                           // varchar(64)         NULL
  'moved',                                 // varchar(2)          NULL
  'nb_canceled_date',                      // varchar(11)         NOT NULL
  'nb_canceled_date_phone',                // varchar(11)         NULL
  'network',                               // varchar(16)         NULL
  'network_id',                            // int(11)             NULL
  'new_clientid_permission',               // tinyint(1)          NOT NULL
  'new_number_date',                       // varchar(11)         NOT NULL
  'new_number_from',                       // varchar(10)         NULL
  'no_eze',                                // varchar(5)          NULL
  'no_ping',                               // tinyint(4)          NULL
  'notice_period_internet_int',            // varchar(25)         NULL
  'notice_period_internet_type',           // varchar(24)         NULL
  'notice_period_phone_int',               // varchar(25)         NULL
  'notice_period_phone_type',              // varchar(24)         NULL
  'oldcontract_active',                    // varchar(32)         NULL
  'oldcontract_address_equal',             // varchar(64)         NULL
  'oldcontract_city',                      // varchar(64)         NOT NULL
  'oldcontract_exists',                    // varchar(25)         NULL
  'oldcontract_firstname',                 // varchar(64)         NULL
  'oldcontract_internet_client_cancelled', // varchar(10)         NULL
  'oldcontract_internet_clientno',         // varchar(64)         NULL
  'oldcontract_internet_provider',         // varchar(64)         NULL
  'oldcontract_lastname',                  // varchar(64)         NULL
  'oldcontract_phone_client_cancelled',    // varchar(10)         NULL
  'oldcontract_phone_clientno',            // varchar(64)         NULL
  'oldcontract_phone_provider',            // varchar(64)         NULL
  'oldcontract_phone_type',                // varchar(25)         NULL
  'oldcontract_porting',                   // varchar(25)         NULL
  'oldcontract_street',                    // varchar(64)         NOT NULL
  'oldcontract_streetno',                  // varchar(8)          NOT NULL
  'oldcontract_title',                     // varchar(24)         NULL
  'oldcontract_zipcode',                   // varchar(8)          NOT NULL
  'paper_bill',                            // varchar(5)          NULL
  'password',                              // varchar(16)         NULL
  'patch_date',                            // varchar(11)         NOT NULL
  'payment_performance',                   // varchar(20)         NULL
  'phone_adapter',                         // varchar(10)         NULL
  'phone_number_added',                    // varchar(4)          NULL
  'phone_number_block',                    // varchar(10)         NULL
  'phoneareacode',                         // varchar(10)         NULL
  'phoneentry_done_date',                  // varchar(11)         NOT NULL
  'phoneentry_done_from',                  // varchar(10)         NOT NULL
  'phonenumber',                           // varchar(24)         NULL
  'porting',                               // varchar(32)         NULL
  'pppoe_config_date',                     // varchar(11)         NOT NULL
  'productname',                           // varchar(64)         NULL
  'prospect_supply_status',                // varchar(32)         NOT NULL
  'purtel_bluckage_fullfiled_date',        // varchar(11)         NULL
  'purtel_bluckage_sended_date',           // varchar(11)         NULL
  'purtel_customer_reg_date',              // varchar(64)         NULL
  'purtel_data_record_id',                 // int(11)             NOT NULL
  'purtel_delete_date',                    // varchar(11)         NULL
  'purtel_edit_done',                      // varchar(5)          NOT NULL
  'purtel_edit_done_from',                 // varchar(10)         NOT NULL
  'purtel_login',                          // varchar(64)         NULL
  'purtel_passwort',                       // varchar(64)         NULL
  'purtel_product',                        // varchar(64)         NULL
  'purtelproduct_advanced',                // varchar(5)          NOT NULL
  'rDNS_count',                            // varchar(5)          NULL
  'rDNS_installation',                     // varchar(10)         NULL
  'reached_downstream',                    // varchar(32)         NULL
  'reached_upstream',                      // varchar(32)         NULL
  'recommended_product',                   // varchar(64)         NULL
  'recruited_by',                          // varchar(64)         NULL
  'reg_answer_date',                       // varchar(11)         NOT NULL
  'registration_date',                     // varchar(11)         NOT NULL
  'remote_login',                          // varchar(24)         NULL
  'remote_password',                       // varchar(64)         NULL
  'routing_active',                        // varchar(32)         NULL
  'routing_active_date',                   // varchar(11)         NOT NULL
  'routing_deleted',                       // varchar(11)         NULL
  'routing_wish_date',                     // varchar(11)         NULL
  'second_tal',                            // varchar(5)          NULL
  'Service',                               // varchar(50)         NULL
  'service_level',                         // varchar(20)         NULL
  'service_level_send',                    // varchar(11)         NULL
  'service_technician',                    // varchar(5)          NULL
  'sip_accounts',                          // varchar(5)          NULL
  'sofortdsl',                             // varchar(5)          NOT NULL
  'special_conditions_from',               // varchar(30)         NULL
  'special_conditions_text',               // text                NULL
  'special_conditions_till',               // varchar(30)         NULL
  'special_termination_internet',          // varchar(5)          NULL
  'special_termination_phone',             // varchar(5)          NULL
  'Spectrumprofile',                       // tinyint(4) unsigned NULL
  'stati_port_confirm_date',               // varchar(11)         NOT NULL
  'status',                                // varchar(64)         NULL
  'stnz_fb',                               // varchar(10)         NULL
  'street',                                // varchar(64)         NULL
  'streetno',                              // varchar(8)          NULL
  'subnetmask',                            // varchar(64)         NULL
  'tal_assigned_phoneno',                  // varchar(64)         NULL
  'tal_cancel_ack_date',                   // varchar(11)         NOT NULL
  'tal_cancel_date',                       // varchar(11)         NOT NULL
  'tal_canceled_for_date',                 // varchar(11)         NOT NULL
  'tal_canceled_from',                     // varchar(10)         NULL
  'tal_dtag_assignment_no',                // varchar(64)         NULL
  'tal_dtag_revisor',                      // varchar(64)         NULL
  'tal_lended_fritzbox',                   // varchar(64)         NULL
  'tal_order_ack_date',                    // varchar(11)         NOT NULL
  'tal_order_date',                        // varchar(11)         NOT NULL
  'tal_ordered_for_date',                  // varchar(11)         NOT NULL
  'tal_order',                             // varchar(64)         NULL
  'tal_orderd_from',                       // varchar(10)         NOT NULL
  'tal_order_work',                        // varchar(5)          NOT NULL
  'tal_product',                           // varchar(32)         NULL
  'tal_releasing_operator',                // varchar(64)         NULL
  'talorder',                              // varchar(64)         NULL
  'talorder_text',                         // text                NULL
  'techdata_status',                       // varchar(256)        NULL
  'telefonbuch_eintrag',                   // varchar(5)          NOT NULL
  'terminal_ready',                        // varchar(11)         NULL
  'terminal_ready_from',                   // varchar(10)         NOT NULL
  'terminal_sended_date',                  // varchar(11)         NOT NULL
  'terminal_sended_from',                  // varchar(15)         NULL
  'terminal_serialno',                     // varchar(64)         NULL
  'terminal_type',                         // varchar(8)          NULL
  'terminal_type_exists',                  // varchar(5)          NULL
  'title',                                 // varchar(24)         NULL
  'tv_service',                            // varchar(5)          NOT NULL
  'tvs',                                   // varchar(11)         NULL
  'tvs_date',                              // varchar(11)         NULL
  'tvs_from',                              // varchar(10)         NULL
  'tvs_to',                                // varchar(11)         NULL
  'vectoring',                             // varchar(20)         NOT NULL
  'ventelo_confirmation_date',             // varchar(11)         NOT NULL
  'ventelo_port_letter_date',              // varchar(11)         NOT NULL
  'ventelo_port_letter_from',              // varchar(10)         NOT NULL
  'ventelo_port_letter_how',               // varchar(10)         NOT NULL
  'ventelo_port_wish_date',                // varchar(11)         NOT NULL
  'ventelo_purtel_enter_date',             // varchar(11)         NOT NULL
  'ventelo_purtel_enter_from',             // varchar(10)         NULL
  'vlan_ID',                               // varchar(16)         NULL
  'voucher',                               // varchar(64)         NULL
  'wisocontract_canceled_date',            // varchar(11)         NOT NULL
  'wisotelno',                             // varchar(64)         NULL
  'zipcode',                               // varchar(8)          NULL
 );
 
 $sql_dslam  = array (
  'id',
  'clientid',
  'productname',
  'kvz_addition',
  'line_identifier',
  'location_id',
  'terminal_type',
  'dtag_line',
  'ip_address',
  'reached_downstream',
  'card_id',
  'pppoe_config_date',
  'dslam_arranged_date',
  'patch_date',
  'tal_ordered_for_date',
  'tal_order_ack_date',
  'connection_activation_date',
  'terminal_ready',
  'remote_password',
  'remote_login',
  'connection_activation_date',
  'firstname',
  'lastname',
  'street',
  'streetno',
  'district',
  'terminal_type',
  'mac_address',
  'id',
  'lsa_kvz_partition_id',
  'lsa_pin',
  'vectoring',
  'connection_activation_date',
 );
?>
