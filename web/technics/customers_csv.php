<?php
//------------------------------------------------------------------------------
  require_once ('../_conf/database.inc');
  require_once ('_conf/database.inc');
  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename=customers_all_'.date ("YmdHis").'.csv');  
  $csv_handle = fopen('php://output', 'w');

  $db_customers = $db->query( $sql['customers']);
  $customersdb = $db_customers->fetch_all(MYSQLI_ASSOC);
  $db_customers->close();

  $headline = array_keys ($customersdb[0]);
  fputcsv($csv_handle, $headline, ';');
  foreach ($customersdb as $customers) {
    $customers = array_map('utf8_decode', $customers);
    fputcsv($csv_handle, $customers, ';');
  }
  fclose ($csv_handle);
  $db->close();  
?>
