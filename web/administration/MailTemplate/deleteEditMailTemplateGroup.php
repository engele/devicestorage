<?php

/**
 *
 */

require_once __DIR__.'/Lib/MailTemplateGroupCollection.php';

use \MailTemplate\MailTemplateGroupCollection;

if (!isset($_GET['templategroup-id'])) {
	throw new Exception('404 - Page not found'); // mabye make it a propper response
}

$_GET['templategroup-id'] = (int) $_GET['templategroup-id'];

$mailTemplateGroupCollection = new MailTemplateGroupCollection($db, $sql);

try {
	$mailTemplateGroupCollection->revokePersistencyById($_GET['templategroup-id']);

	$notify = 'mailtemplategroup.success.delete';
} catch (Exception $e) {
	$notify = 'mailtemplategroup.failure.delete';
}

header('Location: '.$StartURL.'/index.php?func=mail-templates&menu=administration&notify='.$notify);
exit;
