<?php

/**
 *
 */

namespace MailTemplate;

/**
 * 
 */
class Notification
{
	/**
	 * Messages according to $identifier
	 * 
	 * @var array
	 */
	protected static $messages = [
		/*
		 * mailTemplate create
		 */
		'mailtemplate.success.create' => [
			'type' => 'success',
			'text' => 'Template wurde erstellt.',
		],
		'mailtemplate.failure.create' => [
			'type' => 'error',
			'text' => 'Das Template konnte nicht erstellt werden.',
		],
		/*
		 * mailTemplate edit
		 */
		'mailtemplate.success.edit' => [
			'type' => 'success',
			'text' => 'Template erfolgreich gespeichert.',
		],
		'mailtemplate.failure.edit' => [
			'type' => 'error',
			'text' => 'Das Template konnte nicht gespeichert werden.',
		],
		/*
		 * mailTemplate delete
		 */
		'mailtemplate.success.delete' => [
			'type' => 'success',
			'text' => 'Das Template wurde gelöscht.',
		],
		'mailtemplate.failure.delete' => [
			'type' => 'error',
			'text' => 'Das Template konnte nicht gelöscht werden.',
		],
		/*
		 * mailTemplateGroup create
		 */
		'mailtemplategroup.success.create' => [
			'type' => 'success',
			'text' => 'Template-Gruppe wurde erstellt.',
		],
		'mailtemplategroup.failure.create' => [
			'type' => 'error',
			'text' => 'Die Template-Gruppe konnte nicht erstellt werden.',
		],
		/*
		 * mailTemplateGroup edit
		 */
		'mailtemplategroup.success.edit' => [
			'type' => 'success',
			'text' => 'Template-Grupper erfolgreich gespeichert.',
		],
		'mailtemplategroup.failure.edit' => [
			'type' => 'error',
			'text' => 'Die Template-Gruppe konnte nicht gespeichert werden.',
		],
		/*
		 * mailTemplateGroup delete
		 */
		'mailtemplategroup.success.delete' => [
			'type' => 'success',
			'text' => 'Die Template-Grupp wurde gelöscht.',
		],
		'mailtemplategroup.failure.delete' => [
			'type' => 'error',
			'text' => 'Die Template-Gruppe konnte nicht gelöscht werden.',
		],
	];

	/**
	 * Get message according to $identifier
	 * 
	 * @param string $identifier
	 * 
	 * @return array
	 */
	public static function getMessage($identifier = '')
	{
		if (isset(self::$messages[$identifier])) {
			return self::$messages[$identifier];
		}

		return [];
	}
}
