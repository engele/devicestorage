<?php

/**
 *
 */

namespace MailTemplate;

require_once __DIR__.'/../../vendor/propertyreader/propertyreader/PropertyReader.php';

use PropertyReader\PropertyReader;

/**
 * 
 */
abstract class Persistency
{
	/**
     * Does this template exist in database
     * 
     * @var boolean
     */
    protected $persistent = false;

	/**
     * Is persistent?
     * 
     * @return boolean
     */
    public function isPersistent()
    {
        return $this->persistent;
    }

    /**
     * Set persistent on given object
     * 
     * @param mixed $object
     * 
     * @return void
     */
    protected static function setPersistent($object)
    {
        $persistent = & PropertyReader::newInstance()->read($object, 'persistent');
        $persistent = true;
    }
}