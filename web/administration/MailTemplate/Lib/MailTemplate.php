<?php

/**
 *
 */

namespace MailTemplate;

require_once __DIR__.'/Persistency.php';
require_once __DIR__.'/MailTemplateGroup.php';

use MailTemplate\Persistency;
use MailTemplate\MailTemplateGroup;

/**
 *
 */
class MailTemplate extends Persistency
{
	/**
	 * Id of template
	 * 
	 * @var integer
	 */
	private $id;

	/**
	 * Instance of associated MailTemplateGroup
	 * 
	 * @var MailTemplateGroup
	 */
	private $group;

	/**
	 * MailId of template
	 * 
	 * @var string
	 */
	private $mailId;

	/**
	 * Name of template
	 * 
	 * @var string
	 */
	private $name;

	/**
	 * Subject of template
	 * 
	 * @var string
	 */
	private $subject;

	/**
	 * Text of template
	 * 
	 * @var string
	 */
	private $text;

	/**
	 * Constructor
	 */
	public function __construct()
	{
	}

    /**
     * Alias for Construct
     */
    public static function newInstance()
    {
        return new self;
    }

    /**
     * Find template according to $id in database
     * 
     * @param mysqli  $db
     * @param array   $sql
     * @param integer $id
     * 
     * @return MailTemplate|null - null if not in database
     */
    public static function findById($db, $sql, $id)
    {
    	$mailTemplate = null;

    	$statement = $db->prepare($sql['select.mailTemplateById']);
        $statement->bind_param('d', $id);
        $statement->execute();

        $dataResult = $statement->get_result();

        if ($data = $dataResult->fetch_assoc()) {
        	$mailTemplateGroup = MailTemplateGroup::newInstance()
        		->setId($data['group_id'])
        		->setName($data['group_name']);

        	self::setPersistent($mailTemplateGroup);

        	$mailTemplate = self::newInstance()
        		->setId($data['id'])
        		->setGroup($mailTemplateGroup)
        		->setMailId($data['mail_id'])
        		->setName($data['name'])
        		->setSubject($data['subject'])
        		->setText($data['text']);

        	self::setPersistent($mailTemplate);
        }
       	
        $dataResult->free();
        $statement->close();

        return $mailTemplate;
    }

	/**
	 * Set id
	 * 
	 * @param integer $id
	 * 
	 * @throws \InvalidArgumentException
	 * 
	 * @return MailTemplate
	 */
	public function setId($id)
	{
		if (!is_int($id)) {
			throw new \InvalidArgumentException('$id not an integer.');
		}

		$this->id = $id;

		return $this;
	}

	/**
	 * Set group
	 * 
	 * @param MailTemplateGroup $mailTemplateGroup
	 * 
	 * @return MailTemplate
	 */
	public function setGroup(MailTemplateGroup $mailTemplateGroup)
	{
		$this->group = $mailTemplateGroup;

		return $this;
	}

	/**
	 * Set mailId
	 * 
	 * @param string|null $mailId
	 * 
	 * @return MailTemplate
	 */
	public function setMailId($mailId = null)
	{
		$mailId = (string) $mailId;

		if (empty($mailId)) {
			$mailId = null;
		}

		$this->mailId = $mailId;

		return $this;
	}

	/**
	 * Set name
	 * 
	 * @param string $name
	 * 
	 * @return MailTemplate
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Set subject
	 * 
	 * @param string $subject
	 * 
	 * @return MailTemplate
	 */
	public function setSubject($subject = null)
	{
		$subject = (string) $subject;

		if (empty($subject)) {
			$subject = null;
		}

		$this->subject = $subject;

		return $this;
	}

	/**
	 * Set text
	 * 
	 * @param string $text
	 * 
	 * @return MailTemplate
	 */
	public function setText($text)
	{
		$this->text = $text;

		return $this;
	}

	/**
	 * Get id
	 * 
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Get mailId
	 * 
	 * @return string|null
	 */
	public function getMailId()
	{
		return $this->mailId;
	}

	/**
	 * Get group
	 * 
	 * @return MailTemplateGroup|null
	 */
	public function getGroup()
	{
		return $this->group;
	}

	/**
	 * Get name
	 * 
	 * @return string|null
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Get subject
	 * 
	 * @return string|null
	 */
	public function getSubject()
	{
		return $this->subject;
	}

	/**
	 * Get text
	 * 
	 * @return string|null
	 */
	public function getText()
	{
		return $this->text;
	}
}
