<?php

/**
 *
 */

namespace MailTemplate;

require_once __DIR__.'/Collection.php';
require_once __DIR__.'/MailTemplate.php';
require_once __DIR__.'/MailTemplateGroup.php';

use MailTemplate\Collection;
use MailTemplate\MailTemplate;
use MailTemplate\MailTemplateGroup;

/**
 *
 */
class MailTemplateCollection extends Collection
{
	/**
	 * {@inherited}
	 * 
	 * Overwrite this method to use "type hinting".
     * PHP 5.4 throws catchable fatal error instead of TypeError-Exception.
     * Because of this we don't use type hinting, instead we check manually 
     * and throw a common Exception if $mailTemplate not instanceOf MailTemplate.
	 * 
	 * @param MailTemplate $mailTemplate
	 * 
	 * @return MailTemplateCollection
	 */
	public function append($mailTemplate)
	{
        if (!($mailTemplate instanceOf MailTemplate)) {
            throw new \Exception('$mailTemplate must be an instance of MailTemplate\MailTemplate.');
        }

		parent::append($mailTemplate);

		if (!$mailTemplate->isPersistent()) {
			$this->volatileObjects[] = $mailTemplate;
		}

		return $this;
	}

    /**
     * Finds all MailTemplates in database
     * 
     * @param \mysqli $db
     * @param array   $sql
     * 
     * @return MailTemplateCollection
     */
    public static function findAllTemplates(\mysqli $db, array $sql)
    {
        $mailTemplates = [];

        $statement = $db->prepare($sql['select.mailTemplates']);
        $statement->execute();

        $dataResult = $statement->get_result();

        while ($data = $dataResult->fetch_assoc()) {
            $mailTemplateGroup = MailTemplateGroup::newInstance()
                ->setId($data['group_id'])
                ->setName($data['group_name']);

            self::setPersistent($mailTemplateGroup);

            $mailTemplate = MailTemplate::newInstance()
                ->setId($data['id'])
                ->setGroup($mailTemplateGroup)
                ->setMailId($data['mail_id'])
                ->setName($data['name'])
                ->setSubject($data['subject'])
                ->setText($data['text']);

            self::setPersistent($mailTemplate);

            $mailTemplates[] = $mailTemplate;
        }
        
        $dataResult->free();
        $statement->close();

        return new self($db, $sql, $mailTemplates);
    }

    /**
     * {@inherited}
     */
    public function getPersistVolatileQuery()
    {
        return $this->sql['insert.mailtemplate'];
    }

    /**
     * {@inherited}
     */
    public function getRevokePersistencyQuery()
    {
        return $this->sql['delete.mailTemplateById'];
    }

    /**
     * {@inherited}
     */
    public function getPersistVolatileQueryValues()
    {
        $valuesPlaceholder = "(%d, %s, %s, %s, %s, %s),";
        $values = '';

        foreach ($this->volatileObjects as $object) {
            $groupId = $object->getGroup()->getId();
            $groupId = null === $groupId ? 'NULL' : $groupId;

            $mailId = $object->getMailId();
            $mailId = null === $mailId ? 'NULL' : "'".$mailId."'";

            $name = $object->getName();
            $name = null === $name ? 'NULL' : "'".$name."'";

            $subject = $object->getSubject();
            $subject = null === $subject ? 'NULL' : "'".$subject."'";

            $text = $object->getText();
            $text = null === $text ? 'NULL' : "'".$text."'";

            $values .= sprintf($valuesPlaceholder,
                $object->getId(),
                $groupId,
                $mailId,
                $name,
                $subject,
                $text
            );
        }

        return substr($values, 0, -1);
    }

    /**
     * {@inherited}
     * 
     * @todo
     *      Think of an easy way to detect changed values and only update those.
     *      Because updating all is extremely slow.
     */
    public function getUpdatePersistentQueryValues()
    {
        $query = [];

        $valuesPlaceholder = "`id` = %d, `group_id` = %s, `mail_id` = %s, `name` = %s, `subject` = %s, `text` = %s";

        foreach ($this as $mailTemplate) {
            $groupId = $mailTemplate->getGroup()->getId();
            $groupId = null === $groupId ? 'NULL' : $groupId;

            $mailId = $mailTemplate->getMailId();
            $mailId = null === $mailId ? 'NULL' : "'".$mailId."'";

            $name = $mailTemplate->getName();
            $name = null === $name ? 'NULL' : "'".$name."'";

            $subject = $mailTemplate->getSubject();
            $subject = null === $subject ? 'NULL' : "'".$subject."'";

            $text = $mailTemplate->getText();
            $text = null === $text ? 'NULL' : "'".$text."'";

            $query[] = sprintf($this->sql['update.mailTemplateById'],
                sprintf($valuesPlaceholder,
                    $mailTemplate->getId(),
                    $groupId,
                    $mailId,
                    $name,
                    $subject,
                    $text
                ),
                $mailTemplate->getId()
            );
        }

        return $query;
    }
}
