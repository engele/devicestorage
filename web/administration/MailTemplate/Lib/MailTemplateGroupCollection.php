<?php

/**
 *
 */

namespace MailTemplate;

require_once __DIR__.'/Collection.php';
require_once __DIR__.'/MailTemplateGroup.php';

use MailTemplate\Collection;
use MailTemplate\MailTemplateGroup;

/**
 * 
 */
class MailTemplateGroupCollection extends Collection
{
	/**
	 * {@inherited}
	 * 
	 * Overwrite this method to use "type hinting".
     * PHP 5.4 throws catchable fatal error instead of TypeError-Exception.
     * Because of this we don't use type hinting, instead we check manually 
     * and throw a common Exception if $mailTemplate not instanceOf MailTemplate.
	 * 
	 * @param MailTemplateGroup $mailTemplate
	 * 
	 * @return MailTemplateGroupCollection
	 */
	public function append($templateGroup)
	{
        if (!($templateGroup instanceOf MailTemplateGroup)) {
            throw new \Exception('$templateGroup must be an instance of MailTemplate\MailTemplate.');
        }

		parent::append($templateGroup);

		if (!$templateGroup->isPersistent()) {
			$this->volatileObjects[] = $templateGroup;
		}

		return $this;
	}

	/**
	 * Finds all MailTemplateGroups in database
	 * 
	 * @param \mysqli $db
	 * @param array   $sql
	 * 
	 * @return MailTemplateGroupCollection
	 */
	public static function findAllGroups(\mysqli $db, array $sql)
	{
		$mailTemplateGroups = [];

		$statement = $db->prepare($sql['select.mailGroups']);
        $statement->execute();

        $dataResult = $statement->get_result();

        while ($data = $dataResult->fetch_assoc()) {
        	$mailTemplateGroup = MailTemplateGroup::newInstance()
        		->setId($data['id'])
        		->setName($data['name']);

        	self::setPersistent($mailTemplateGroup);

        	$mailTemplateGroups[] = $mailTemplateGroup;
        }

        $dataResult->free();
        $statement->close();

        return new self($db, $sql, $mailTemplateGroups);
	}

	/**
     * {@inherited}
     */
    public function getPersistVolatileQuery()
    {
        return $this->sql['insert.mailTemplateGroup'];
    }

    /**
     * {@inherited}
     */
    public function getRevokePersistencyQuery()
    {
        return $this->sql['delete.mailTemplateGroupById'];
    }

	/**
     * {@inherited}
     */
    public function getPersistVolatileQueryValues()
    {
        $valuesPlaceholder = "(%s),";
        $values = "";

        foreach ($this->volatileObjects as $object) {
            $name = $object->getName();
            $name = null === $name ? 'NULL' : '"'.$name.'"';

            $values .= sprintf($valuesPlaceholder, $name);
        }

        return substr($values, 0, -1);
    }

    /**
     * {@inherited}
     * 
     * @todo
     *      Think of an easy way to detect changed values and only update those.
     *      Because updating all is extremely slow.
     */
    public function getUpdatePersistentQueryValues()
    {
        $query = [];

        foreach ($this as $mailTemplateGroup) {
            $name = $mailTemplateGroup->getName();
            $name = null === $name ? 'NULL' : "'".$name."'";

            $query[] = sprintf($this->sql['update.mailTemplateGroupById'],
                $name,
                $mailTemplateGroup->getId()
            );
        }

        return $query;
    }
}
