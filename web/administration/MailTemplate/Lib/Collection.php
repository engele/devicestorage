<?php

/**
 *
 */

namespace MailTemplate;

require_once __DIR__.'/../../vendor/propertyreader/propertyreader/PropertyReader.php';

use PropertyReader\PropertyReader;
use MailTemplate\Persistency;

/**
 *
 */
abstract class Collection extends \ArrayIterator
{
	/**
	 * Instance of mysqli
	 * 
	 * @var \mysqli
	 */
	protected $db;

	/**
	 * $sql from _conf/database.inc
	 * 
	 * @var array
	 */
	protected $sql;

	/**
	 * Not yet persisted objects
	 * 
	 * @var array
	 */
	protected $volatileObjects = [];

	/**
     * Return "Values"-part for sql-query (INSERT)
     * 
     * @return string
     */
    abstract public function getPersistVolatileQueryValues();

    /**
     * Return insert sql-query
     * 
     * @return string
     */
    abstract public function getPersistVolatileQuery();

    /**
     * Return delete sql-query
     * 
     * @return string
     */
    abstract public function getRevokePersistencyQuery();

    /**
     * Return "Values"-part for sql-query (UPDATE)
     * 
     * @return array
     */
    abstract public function getUpdatePersistentQueryValues();

	/**
	 * Custructor
	 * 
	 * {@inherited}
	 * 
	 * @param \mysqli $db
	 * @param array   $sql
	 * @param array   $array
	 */
	public function __construct(\mysqli $db, array $sql, array $array = [])
	{
		$this->db = $db;
		$this->sql = $sql;

		parent::__construct($array);

        foreach ($array as $object) {
            if (!$object->isPersistent()) {
                $this->volatileObjects[] = $object;
            }
        }
	}

	/**
	 * Save mailTemplates to database
     * 
     * @throws \Exception - if query fails
	 * 
	 * @return MailTemplateCollection
	 */
	public function persist()
	{
        try {
            $this->persistVolatile();
            $this->updatePersistent();
        } catch (\Exception $e) {
            throw $e;
        }

        return $this;
	}

	/**
     * Saves "new" objects to database
     * 
     * @throws \Exception - if query fails
     * 
     * @return void
     */
    private function persistVolatile()
    {
        $values = $this->getPersistVolatileQueryValues();

        if (!empty($values)) {
            $result = $this->db->query(sprintf(
                $this->getPersistVolatileQuery(),
                $values
            ));

            if (true === $result) { // success
                $this->setVolatilePersistent();

                return;
            }

            throw new \Exception('Failed to persist mailtemplate. - '.$this->db->error.' -- '.sprintf(
                $this->getPersistVolatileQuery(),
                $values
            ));
        }
    }

    /**
     * Updates objects in database
     * 
     * @throws \Exception - if one query fails
     * 
     * @return void
     */
    private function updatePersistent()
    {
        $values = $this->getUpdatePersistentQueryValues();

        if (!empty($values)) {
            foreach ($values as $query) {
                $result = $this->db->query($query);

                if (false === $result) { // failure
                    throw new \Exception('Failed to persist mailtemplate. - '.$this->db->error.' -- '.$query);
                }
            }
        }
    }

    /**
     * Set persistent on all volatile objects.
     * Make sure to only use this after successfuly persistVolatile()!
     * 
     * @return void
     */
    private function setVolatilePersistent()
    {
        foreach ($this->volatileObjects as $object) {
            $persistent = & PropertyReader::newInstance()->read($object, 'persistent');
            $persistent = true;
        }

        $this->volatileObjects = [];
    }

    /**
     * Set persistent on given object
     * 
     * @param Persistency $object
     * 
     * @return void
     */
    protected static function setPersistent(Persistency $object)
    {
        $persistent = & PropertyReader::newInstance()->read($object, 'persistent');
        $persistent = true;
    }

    /**
     * Remove object from database
     * 
     * @param Persistency $object
     * 
     * @throws \Exception - if query fails
     * 
     * @return MailTemplateCollection
     */
    public function revokePersistency(Persistency $object)
    {
        $statement = $this->db->prepare($this->getRevokePersistencyQuery());
        $statement->bind_param('d', $object->getId());

        if ($statement->execute()) {
            return $this;
        }

        throw new \Exception('Failed to remove from database. - '.$this->db->error);
    }

    /**
     * Remove object from database by Id
     * 
     * @param integer $id
     * 
     * @throws \Exception - if query fails
     * 
     * @return MailTemplateCollection
     */
    public function revokePersistencyById($id)
    {
        $statement = $this->db->prepare($this->getRevokePersistencyQuery());
        $statement->bind_param('d', $id);

        if ($statement->execute()) {
            return $this;
        }

        throw new \Exception('Failed to remove from database. - '.$this->db->error);
    }
}
