<?php

/**
 *
 */

namespace MailTemplate;

require_once __DIR__.'/Persistency.php';

use MailTemplate\Persistency;

/**
 *
 */
class MailTemplateGroup extends Persistency
{
	/**
	 * Id of group
	 * 
	 * @var integer
	 */
	private $id;

	/**
	 * Name of group
	 * 
	 * @var string
	 */
	private $name;

	/**
	 * Alias for Constructor
	 */
	public static function newInstance()
	{
		return new self;
	}

	/**
     * Find templateGroup according to $id in database
     * 
     * @param mysqli  $db
     * @param array   $sql
     * @param integer $id
     * 
     * @return MailTemplateGroup|null - null if not in database
     */
    public static function findById($db, $sql, $id)
    {
    	$mailTemplateGroup = null;

    	$statement = $db->prepare($sql['select.mailTemplateGroupById']);
        $statement->bind_param('d', $id);
        $statement->execute();

        $dataResult = $statement->get_result();

        if ($data = $dataResult->fetch_assoc()) {
        	$mailTemplateGroup = self::newInstance()
        		->setId($data['id'])
        		->setName($data['name']);

        	self::setPersistent($mailTemplateGroup);
        }
       	
        $dataResult->free();
        $statement->close();

        return $mailTemplateGroup;
    }

	/**
	 * Set id
	 * 
	 * @param integer $id
	 * 
	 * @throws \InvalidArgumentException
	 * 
	 * @return MailTemplateGroup
	 */
	public function setId($id)
	{
		if (!is_int($id)) {
			throw new \InvalidArgumentException('$id must be an integer.');
		}

		$this->id = $id;

		return $this;
	}

	/**
	 * Get id
	 * 
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set name
	 * 
	 * @param string $name
	 * 
	 * @return MailTemplateGroup
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 * 
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
}
