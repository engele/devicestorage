<?php

/**
 * 
 */

require_once __DIR__.'/Lib/MailTemplateGroup.php';
require_once __DIR__.'/Lib/MailTemplateGroupCollection.php';
require_once __DIR__.'/Lib/MailTemplateCollection.php';
require_once __DIR__.'/Lib/MailTemplate.php';
require_once __DIR__.'/Lib/Notification.php';

use \MailTemplate\MailTemplateGroup;
use \MailTemplate\MailTemplateGroupCollection;
use \MailTemplate\MailTemplate;
use \MailTemplate\MailTemplateCollection;
use \MailTemplate\Notification;

$mailTemplate = new MailTemplate;
$isEdit = false;

/**
 * If edit, fetch data from database
 */
if (isset($_GET['template-id'])) {
	$_GET['template-id'] = (int) $_GET['template-id'];
	
	$mailTemplate = MailTemplate::findById($db, $sql, $_GET['template-id']);

	if (null !== $mailTemplate->getId()) {
		$isEdit = true;
	}
}

/**
 * Handle POST
 */
if (isset($_POST['mailtemplate-create-save'])) {
	/**
	 * Basic sanitation on POST-values
	 */
	$_POST['mailtemplate-create-id'] = (int) $_POST['mailtemplate-create-id'];
	$_POST['mailtemplate-create-groupid'] = (int) $_POST['mailtemplate-create-groupid'];
	$_POST['mailtemplate-create-mailid'] = addslashes(strip_tags($_POST['mailtemplate-create-mailid']));
	$_POST['mailtemplate-create-name'] = addslashes(strip_tags($_POST['mailtemplate-create-name']));
	$_POST['mailtemplate-create-subject'] = addslashes(strip_tags($_POST['mailtemplate-create-subject']));
	$_POST['mailtemplate-create-text'] = addslashes($_POST['mailtemplate-create-text']);

	$mailTemplateGroup = new MailTemplateGroup; // Maybe "fetchById". This way we can also confirm the posted groupid is real.
	$mailTemplateGroup->setId($_POST['mailtemplate-create-groupid']);

	$mailTemplate
		->setId($_POST['mailtemplate-create-id'])
		->setGroup($mailTemplateGroup)
		->setMailId($_POST['mailtemplate-create-mailid'])
		->setName($_POST['mailtemplate-create-name'])
		->setSubject($_POST['mailtemplate-create-subject'])
		->setText($_POST['mailtemplate-create-text']);

    $mailTemplateCollection = new MailTemplateCollection($db, $sql);

    try {
        $mailTemplateCollection->append($mailTemplate)->persist();

        $notify = $isEdit ? 'mailtemplate.success.edit' : 'mailtemplate.success.create';

        header('Location: '.$StartURL.'/index.php?func=mail-templates&menu=administration&template-id='.$mailTemplate->getId().'&notify='.$notify);
    	exit;
    } catch (\Exception $e) {
        $notify = $isEdit ? 'mailtemplate.failure.edit' : 'mailtemplate.failure.create';

        $errorMessage = Notification::getMessage($notify);
        $errorMessage['text'] .= ' - '.$e->getMessage();
    }
}

?>

<form method="post" action="">
	<div class="box">
	    <h3>Neues Mail-Template erstellen</h3>
	    <div>
	    	<?php
	    	if (isset($errorMessage)) {
	    		echo '<p class="'.$errorMessage['type'].'">'.$errorMessage['text'].'</p>';
	    	}
	    	?>

			<label for="mailtemplate-create-id">Id des Templates</label><br>
			<input type="text" id="mailtemplate-create-id" name="mailtemplate-create-id" value="<?php echo $mailTemplate->getId(); ?>" />
			<br />

			<label for="mailtemplate-create-groupid">Gruppe des Templates</label><br>
			<select id="mailtemplate-create-groupid" class="max" name="mailtemplate-create-groupid">
				<option value="">Gruppe auswählen</option>
				<?php
				$mailTemplateGroupCollection = MailTemplateGroupCollection::findAllGroups($db, $sql);

				$mailTemplateGroupId = false;

				if (null !== $mailTemplate->getGroup()) {
					$mailTemplateGroupId = $mailTemplate->getGroup()->getId();
				}

				foreach ($mailTemplateGroupCollection as $mailTemplateGroup) {
					$selected = '';

					if ($mailTemplateGroupId === $mailTemplateGroup->getId()) {
						$selected = ' selected';
					}

					echo '<option value="'.$mailTemplateGroup->getId().'"'.$selected.'>'
						.$mailTemplateGroup->getName().'</option>';
				}
				?>
			</select>
			<br />

			<label for="mailtemplate-create-mailid">Mail-Id des Templates</label><br>
			<input type="text" id="mailtemplate-create-mailid" name="mailtemplate-create-mailid" value="<?php echo $mailTemplate->getMailId(); ?>" />
			<br />

			<label for="mailtemplate-create-name">Name des Templates</label><br>
			<input type="text" id="mailtemplate-create-name" name="mailtemplate-create-name" value="<?php echo $mailTemplate->getName(); ?>" />
			<br />

			<label for="mailtemplate-create-subject">Betreff des Templates</label><br>
			<input type="text" id="mailtemplate-create-subject" name="mailtemplate-create-subject" value="<?php echo $mailTemplate->getSubject(); ?>" />
			<br />

			<label for="mailtemplate-create-text">Mail-Template</label><br>
			<textarea id="mailtemplate-create-text" name="mailtemplate-create-text"><?php echo $mailTemplate->getText(); ?></textarea>
	    </div>
	</div>

	<div class="box">
	    <h3></h3>
	    <div>
	       	<button onclick="backButton()" type="button">
	            <img alt="" src="<?php echo $StartURL; ?>/_img/Back.png" height="12px"> Zurück
	        </button>
	        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	        <button name="mailtemplate-create-save" type="submit">
	        	<img alt="" src="<?php echo $StartURL.'/_img/Yes.png'; ?>" height="12px"> Speichern
	       	</button>
	    </div>
	</div>
</form>
