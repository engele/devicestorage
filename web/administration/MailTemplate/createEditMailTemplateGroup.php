<?php

/**
 *
 */

require_once __DIR__.'/Lib/MailTemplateGroup.php';
require_once __DIR__.'/Lib/MailTemplateGroupCollection.php';
require_once __DIR__.'/Lib/Notification.php';

use \MailTemplate\MailTemplateGroup;
use \MailTemplate\MailTemplateGroupCollection;
use \MailTemplate\Notification;

$mailTemplateGroup = new MailTemplateGroup;
$isEdit = false;

/**
 * If edit, fetch data from database
 */
if (isset($_GET['templategroup-id'])) {
	$_GET['templategroup-id'] = (int) $_GET['templategroup-id'];
	
	$mailTemplateGroup = MailTemplateGroup::findById($db, $sql, $_GET['templategroup-id']);

	if (null !== $mailTemplateGroup->getId()) {
		$isEdit = true;
	}
}

/**
 * Handle POST
 */
if (isset($_POST['mailtemplategroup-create-save'])) {
	/**
	 * Basic sanitation on POST-values
	 */
	$_POST['mailtemplategroup-create-name'] = addslashes(strip_tags($_POST['mailtemplategroup-create-name']));

	$mailTemplateGroup->setName($_POST['mailtemplategroup-create-name']);

	$mailTemplateGroupCollection = new MailTemplateGroupCollection($db, $sql);

    try {
        $mailTemplateGroupCollection->append($mailTemplateGroup)->persist();

        $notify = $isEdit ? 'mailtemplategroup.success.edit' : 'mailtemplategroup.success.create';

        header('Location: '.$StartURL.'/index.php?func=mail-templates&menu=administration&templategroup-id='.$mailTemplateGroup->getId().'&notify='.$notify);
    	exit;
    } catch (\Exception $e) {
        $notify = $isEdit ? 'mailtemplategroup.failure.edit' : 'mailtemplategroup.failure.create';

        $errorMessage = Notification::getMessage($notify);
        $errorMessage['text'] .= ' - '.$e->getMessage();
    }
}

?>

<form method="post" action="">
	<div class="box">
	    <h3>Neue Mail-Template-Gruppe erstellen</h3>
	    <div>
	    	<?php
	    	if (isset($errorMessage)) {
	    		echo '<p class="'.$errorMessage['type'].'">'.$errorMessage['text'].'</p>';
	    	}
	    	?>

			<label for="mailtemplategroup-create-name">Name der Template-Gruppe</label><br>
			<input type="text" id="mailtemplategroup-create-name" name="mailtemplategroup-create-name" value="<?php echo $mailTemplateGroup->getName(); ?>" />
	    </div>
	</div>

	<div class="box">
	    <h3></h3>
	    <div>
	       	<button onclick="backButton()" type="button">
	            <img alt="" src="<?php echo $StartURL; ?>/_img/Back.png" height="12px"> Zurück
	        </button>
	        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	        <button name="mailtemplategroup-create-save" type="submit">
	        	<img alt="" src="<?php echo $StartURL.'/_img/Yes.png'; ?>" height="12px"> Speichern
	       	</button>
	    </div>
	</div>
</form>
