<?php

/**
 *
 */

require_once __DIR__.'/Lib/MailTemplateCollection.php';
require_once __DIR__.'/Lib/MailTemplateGroupCollection.php';
require_once __DIR__.'/Lib/Notification.php';

use \MailTemplate\MailTemplateCollection;
use \MailTemplate\MailTemplateGroupCollection;
use \MailTemplate\Notification;

?>

<div class="box">
    <h3>Mail-Templates</h3>
    <div>
        <?php
        if (isset($_GET['notify']) && false !== strpos($_GET['notify'], 'mailtemplate.')) {
            $message = Notification::getMessage($_GET['notify']);

            echo '<p class="'.$message['type'].'">'.$message['text'].'</p>';
        }
        ?>

        <select id="mailtemplate-select-template"  class="max">
            <option value="">Template auswählen</option>
            <?php
            $selectTemplate = false;

            if (isset($_GET['template-id'])) {
                $selectTemplate = (int) $_GET['template-id'];
            }

            $mailTemplates = MailTemplateCollection::findAllTemplates($db, $sql);

            foreach ($mailTemplates as $mailTemplate) {
                $selected = '';

                if ($selectTemplate === $mailTemplate->getId()) {
                    $selected = ' selected';
                }

                echo '<option value="'.$mailTemplate->getId().'"'.$selected.'>'
                    .$mailTemplate->getName().' ('.$mailTemplate->getMailId().')</option>';
            }
            ?>
        </select>

        <br />
        <br />

        <button type="submit" id="mailtemplate-button-edit">
            <img alt="" src="<?php echo $StartURL; ?>/_img/Modify.png" height="12px"> Bearbeiten
        </button>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="submit" id="mailtemplate-button-delete">
            <img alt="" src="<?php echo $StartURL; ?>/_img/delete.png" height="12px"> Löschen
        </button>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;
        <button type="button" id="mailtemplate-button-create">
            <img alt="" src="<?php echo $StartURL; ?>/_img/New document.png" height="12px"> Neues Template
        </button>
    </div>


    <br />


    <h3>Mail-Template-Gruppen</h3>
    <div>
        <?php
        if (isset($_GET['notify']) && false !== strpos($_GET['notify'], 'mailtemplategroup.')) {
            $message = Notification::getMessage($_GET['notify']);

            echo '<p class="'.$message['type'].'">'.$message['text'].'</p>';
        }
        ?>

        <select id="mailtemplategroup-select-template">
            <option value="">Gruppe auswählen</option>
            <?php
            $selectTemplateGroup = false;

            if (isset($_GET['templategroup-id'])) {
                $selectTemplateGroup = (int) $_GET['templategroup-id'];
            }

            $mailTemplateGroups = MailTemplateGroupCollection::findAllGroups($db, $sql);

            foreach ($mailTemplateGroups as $mailTemplateGroup) {
                $selected = '';

                if ($selectTemplateGroup === $mailTemplateGroup->getId()) {
                    $selected = ' selected';
                }

                echo '<option value="'.$mailTemplateGroup->getId().'"'.$selected.'>'
                    .$mailTemplateGroup->getName().'</option>';
            }
            ?>
        </select>

        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;
        <button type="submit" id="mailtemplategroup-button-edit">
            <img alt="" src="<?php echo $StartURL; ?>/_img/Modify.png" height="12px"> Bearbeiten
        </button>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="submit" id="mailtemplategroup-button-delete">
            <img alt="" src="<?php echo $StartURL; ?>/_img/delete.png" height="12px"> Löschen
        </button>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;
        <button type="button" id="mailtemplategroup-button-create">
            <img alt="" src="<?php echo $StartURL; ?>/_img/New document.png" height="12px"> Neue Template-Gruppe
        </button>
    </div>
</div>
