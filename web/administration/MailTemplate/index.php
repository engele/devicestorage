<?php

/**
 *
 */

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        /**
         * Delete Mail-Template
         */
        case 'delete-mail-template':
            require __DIR__.'/deleteEditMailTemplate.php';
            break;

        /**
         * Create or edit Mail-Template
         */
        case 'create-edit-mail-template':
            require __DIR__.'/createEditMailTemplate.php';
            break;

        /**
         * Create or edit Mail-Template-Group
         */
        case 'create-edit-mail-template-group':
            require __DIR__.'/createEditMailTemplateGroup.php';
            break;

        /**
         * Delete Mail-Template-Group
         */
        case 'delete-mail-template-group':
            require __DIR__.'/deleteEditMailTemplateGroup.php';
            break;

        /**
         * No reason to be here
         */
        default:
            header('Location: ?menu=administration&func=mail-templates');
            exit;
    }
} else {
    /**
     * default view
     */
    require __DIR__.'/default.php';
}

?>
