<?php

/**
 *
 */

require_once __DIR__.'/Lib/MailTemplateCollection.php';

use \MailTemplate\MailTemplateCollection;

if (!isset($_GET['template-id'])) {
	throw new Exception('404 - Page not found'); // mabye make it a propper response
}

$_GET['template-id'] = (int) $_GET['template-id'];

$mailTemplateCollection = new MailTemplateCollection($db, $sql);

try {
	$mailTemplateCollection->revokePersistencyById($_GET['template-id']);

	$notify = 'mailtemplate.success.delete';
} catch (Exception $e) {
	$notify = 'mailtemplate.failure.delete';
}

header('Location: '.$StartURL.'/index.php?func=mail-templates&menu=administration&notify='.$notify);
exit;
