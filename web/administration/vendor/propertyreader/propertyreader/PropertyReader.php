<?php

/**
 *
 */

namespace PropertyReader;

/**
 * For PHP < 7 :
 * PropertyReader::newInstance()->read();
 * (To avoide "PHP Warning: Cannot bind an instance to a static closure")
 */
class PropertyReader
{
    /**
     * Alias for Constructor
     */
    public static function newInstance()
    {
        return new self;
    }
    
	/**
	 * Accessing ANY (including protected & private) class property by reference
	 * 
	 * @param mixed  $object
	 * @param string $property
	 */
	public function & read($object, $property)
	{
		$value = & \Closure::bind(function & () use ($property) {
            return $this->{$property};
        }, $object, $object)->__invoke();

        return $value;
	}
}
