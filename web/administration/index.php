<?php
/**
 * 
 */

$this->denyAccessUnlessGranted('ROLE_VIEW_ADMINISTRATION');

require_once ($StartPath.'/_conf/database.inc');
require_once ($StartPath.'/'.$SelMenu.'/_conf/database.inc');

if (isset($_GET['func'])) {
    switch ($_GET['func']) {
    /**
    * Mail-Templates
    */
        case 'mail-templates':
            require __DIR__.'/MailTemplate/index.php';
            $db->close();
            break;
            
        /**
         * No reason to be here
         */
        default:
            header('Location: ?menu=administration');
            exit;
    }
} else {
    /**
     * default view
     */
    require __DIR__.'/default.php';
}
?>
