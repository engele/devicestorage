<?php
$sql = array (
    // Benutzer und Gruppen
    /*'account' => "
        SELECT * FROM kv_user ORDER BY account
    ",

    'group' => "
        SELECT * FROM kv_group ORDER BY group_name
    ",*/
    
    'right_group' => "
        SELECT * FROM right_group ORDER BY right_group.order
    ",
    // Mailtemplate
    'insert.mailtemplate' => "
        INSERT INTO `mail_templates`
        (`id`, `group_id`, `mail_id`, `name`, `subject`, `text`) 
        VALUES %s
    ",

    'select.mailGroups' => "
        SELECT * FROM `mail_group`
    ",

    'select.mailTemplates' => "
        SELECT t.*, g.`name` as 'group_name' FROM `mail_templates` t 
        LEFT JOIN `mail_group` g ON t.`group_id` = g.`id`
    ",

    'select.mailTemplateById' => "
        SELECT t.*, g.`name` as 'group_name' FROM `mail_templates` t 
        LEFT JOIN `mail_group` g ON t.`group_id` = g.`id` WHERE t.`id` = ?
    ",

    'update.mailTemplateById' => "
        UPDATE `mail_templates` SET %s WHERE `id` = %d
    ",

    'delete.mailTemplateById' => "
        DELETE FROM `mail_templates` WHERE `id` = ?
    ",

    'select.mailTemplateGroupById' => "
        SELECT * FROM `mail_group` WHERE `id` = ?
    ",

    'insert.mailTemplateGroup' => "
        INSERT INTO `mail_group` (`name`) VALUES %s
    ",

    'update.mailTemplateGroupById' => "
        UPDATE `mail_group` SET `name` = %s WHERE `id` = %d
    ",

    'delete.mailTemplateGroupById' => "
        DELETE FROM `mail_group` WHERE `id` = ?
    ",
);

$GLOBALS['sql'] = & $sql;
?>
