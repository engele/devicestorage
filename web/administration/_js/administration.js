function adminAccountSubmit(){
  htmlstr = "Wollen Sie den Benutzer anlegen/ändern?"
  $("#dialog").html(htmlstr);
  $("#dialog").dialog({
    resizable: true,
    modal: true,
    title: 'Benutzer hinzufügen/ändern',
    position: {my: 'top', at: 'top', of: '#Container'},
    height: 300,
    width: 500,
    buttons: {
      'Ja': function () {
        $(this).dialog('close');
        callAccountSubmit(true);
      },
      'Nein': function () {
        $(this).dialog('close');
        callAccountSubmit(false);
      }
    }
  });
}

function callAccountSubmit(save) {
  if (save){
    $("#command").attr('name', 'save_account');
    $("#addAccountForm").submit();
  }
}

function adminAccountDelete(){
  version = '';
  htmlstr = "Wollen Sie den Benutzer '"+$(":input[name=addAccountName]").val()+"' wirklich löschen?";
  $("#dialog").html(htmlstr);
  $("#dialog").dialog({
    resizable: true,
    modal: true,
    title: 'Kundendaten löschen',
    position: {my: 'top', at: 'top', of: '#Container'},
    height: 300,
    width: 500,
    buttons: {
      'Ja': function () {
        $(this).dialog('close');
        callAccountDelete(true);
      },
      'Nein': function () {
        $(this).dialog('close');
        callAccountDelete(false);
      }
    }
  });
}

function callAccountDelete(save) {
  if (save){
    $("#command").attr('name', 'delete_account');
    $("#addAccountForm").submit();
  }
}

function callAccount() {
  var StartPath = $("#StartPath").val();
  var SelPath   = $("#SelPath").val();
  var StartURL  = $("#StartURL").val();
  var Account   = $("#optAccountUser option:selected").val();
  $.ajax({
    type: "POST",
    url: decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/ajaxAccount.php",
    data: "StartPath="+StartPath+"&SelPath="+SelPath+"&Account="+Account,
    success: function(data){ajaxAccount(data)}
  });
}

function ajaxAccount(data) {
  var response  = $.parseJSON(data);
  
  if (response.Account != '') {
    $("#addAccountUser").attr("readonly","readonly");  
    $("#addAccountUser").val(response.Account);
    if (!$("#addAccountName").hasClass('changed')) $("#addAccountName").val(response.Fullname);
    if (!$("#addPurtelLogin").hasClass('changed')) $("#addPurtelLogin").val(response.PurtelLogin);
    if (!$("#addPurtelPw").hasClass('changed'))    $("#addPurtelPw").val(response.PurtelPW);  
    if (!$("#addEgwLogin").hasClass('changed'))    $("#addEgwLogin").val(response.EgwLogin);  
  } else {
    $("#addAccountUser").removeAttr("readonly");  
  }
}

function adminGroupSubmit(){
  htmlstr = "Wollen Sie die Gruppe anlegen/ändern?"
  $("#dialog").html(htmlstr);
  $("#dialog").dialog({
    resizable: true,
    modal: true,
    title: 'Gruppe hinzufügen/ändern',
    position: {my: 'top', at: 'top', of: '#Container'},
    height: 300,
    width: 500,
    buttons: {
      'Ja': function () {
        $(this).dialog('close');
        callGroupSubmit(true);
      },
      'Nein': function () {
        $(this).dialog('close');
        callGroupSubmit(false);
      }
    }
  });
}


function callGroupSpan(){
  var Group = "."+$("#optGroupSpan option:selected").val();
  
  $(".RightGroup").hide();
  if (Group != '') {
    $(Group).show();
  }  
}

function callGroupGroup(){
  var Group = $("#optGroupGroup option:selected").val();
  if (Group != '') {
    $("#addGroupGroup").val(Group);
    $("#addGroupGroup").attr("readonly","readonly");
  } else {
    $("#addGroupGroup").removeAttr("readonly");  
  } 
}

function callGroupSubmit(save) {
  if (save){
    $("#command").attr('name', 'save_group');
    $("#addAccountForm").submit();
  }
}

$(document).ready(function() {
  $("#addAccountForm").change(callAccount);
  $("#optGroupSpan").change(callGroupSpan);
  $("#optGroupGroup").change(callGroupGroup);
  
  $(":input").change(function () {
    $(":input[name="+$(this).attr("name")+"]").css('background-color', '#F7F9B6');
    $(":input[name="+$(this).attr("name")+"]").css('border-color', '#089606');
    $(":input[name="+$(this).attr("name")+"]").addClass('changed');
  });
  
  $(".RightGroup").hide();   

	/**
	 * MailTemplate
	 */
	var MailTemplate = (function($) {
		/**
		 * Click mailTemplate edit-button
		 */
		$('#mailtemplate-button-edit').on('click', function() {
			var templateId = $('#mailtemplate-select-template').val();

			if ('' !== templateId) {
				window.location = '?func=mail-templates&menu=administration&action=create-edit-mail-template&template-id='+templateId;
			} else {
				window.alert('Sie muessen zuerst ein Template auswaehlen.');
			}

			return false;
		});

		/**
		 * Click mailTemplate create-button
		 */
		$('#mailtemplate-button-create').on('click', function() {
			window.location = '?func=mail-templates&menu=administration&action=create-edit-mail-template';
		});

		/**
		 * Click mailTemplate delete-button
		 */
		$('#mailtemplate-button-delete').on('click', function() {
			var templateId = $('#mailtemplate-select-template').val();

			if ('' !== templateId) {
				window.location = '?func=mail-templates&menu=administration&action=delete-mail-template&template-id='+templateId;
			} else {
				window.alert('Sie muessen zuerst ein Template auswaehlen.');
			}

			return false;
		});

		/**
		 * Click mailTemplateGroup edit-button
		 */
		$('#mailtemplategroup-button-edit').on('click', function() {
			var templateGroupId = $('#mailtemplategroup-select-template').val();

			if ('' !== templateGroupId) {
				window.location = '?func=mail-templates&menu=administration&action=create-edit-mail-template-group&templategroup-id='+templateGroupId;
			} else {
				window.alert('Sie muessen zuerst ein Template auswaehlen.');
			}

			return false;
		});

		/**
		 * Click mailTemplateGroup create-button
		 */
		$('#mailtemplategroup-button-create').on('click', function() {
			window.location = '?func=mail-templates&menu=administration&action=create-edit-mail-template-group';
		});

		/**
		 * Click mailTemplateGroup delete-button
		 */
		$('#mailtemplategroup-button-delete').on('click', function() {
			var templateId = $('#mailtemplategroup-select-template').val();

			if ('' !== templateId) {
				window.location = '?func=mail-templates&menu=administration&action=delete-mail-template-group&templategroup-id='+templateId;
			} else {
				window.alert('Sie muessen zuerst ein Template auswaehlen.');
			}

			return false;
		});
	}(window.jQuery));
});
