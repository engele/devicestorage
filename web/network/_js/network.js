function netSubmit(){
 htmlstr = "Wollen Sie die Änderungen speichern?"
  $("#dialog").html(htmlstr);
  $("#dialog").dialog({
    resizable: true,
    modal: true,
    title: 'Netz bearbeiten',
    position: {my: 'top', at: 'top', of: '#Container'},
    height: 300,
    width: 500,
    buttons: {
      'Ja': function () {
        $(this).dialog('close');
        callSubmit(true);
      },
      'Nein': function () {
        $(this).dialog('close');
        callSubmit(false);
      }
    }
  });
}

function callSubmit(save) {
  if (save){
    $("#command").attr('name', 'save');
    $("#netForm").submit();
  }
}

function netDelete(){
 htmlstr = "Wollen Sie das Netz wirklich löschen?"
  $("#dialog").html(htmlstr);
  $("#dialog").dialog({
    resizable: true,
    modal: true,
    title: 'Netz löschen',
    position: {my: 'top', at: 'top', of: '#Container'},
    height: 300,
    width: 500,
    buttons: {
      'Ja': function () {
        $(this).dialog('close');
        callDelete(true);
      },
      'Nein': function () {
        $(this).dialog('close');
        callDelete(false);
      }
    }
  });
}

function callDelete(save) {
  if (save){
    $("#command").attr('name', 'delete');
    $("#netForm").submit();
  }
}

$(document).ready(function(){
  $("table.Network").tablesorter({
    widgets: ['zebra']
  });
  /*.after($("#NetworkIpRangeNone").click(function() {
    $('select.bsmContainer').children("[selected]").removeAttr("selected").end().change();
    return false;
  })).after($("#NetworkIpRangeAll").click(function() {
    $('select.bsmContainer').children().attr("selected", "selected").end().change();
    return false;
  })); */
});