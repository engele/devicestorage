<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_NETWORK');

require_once $StartPath.'/_conf/database.inc';
require_once $StartPath.'/'.$SelMenu.'/_conf/database.inc';
require_once $StartPath.'/'.$SelPath.'/_conf/form.inc';

  //############################################################################
  if (key_exists ('func', $_GET) and $_GET['func'] == 'view') {
    if (key_exists ('id', $_GET)) $id = $_GET['id']; else $id = 0;
    $db_networks = $db->prepare($sql['network_single']);
    $db_networks->bind_param('d', $id);
    $db_networks->execute();
    $db_networks_result = $db_networks->get_result();
    $net_result = $db_networks_result->fetch_assoc();
    $db_networks_result->free();

    $db_net_iprange = $db->prepare($sql['net_iprange']);
    $db_net_iprange->bind_param('d', $id);
    $db_net_iprange->execute();
    $db_net_iprange_result = $db_net_iprange->get_result();

    $db_net_localtion = $db->prepare($sql['net_location']);
    $db_net_localtion->bind_param('d', $id);
    $db_net_localtion->execute();
    $db_net_localtion_result = $db_net_localtion->get_result();
?>
<div class="Network box">
  <h1 class="Network"><?php echo $net_result['name']; ?> (<?php echo $net_result['id_string']; ?>)</h1>
  <h3>IP-Blöcke</h3>
  <br />
  <table class="Network">
    <thead>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>Start-Adresse</th>
      <th>End-Adresse</th>
    </tr>
    </thead>
    <tbody>
    <?php
      while ($db_iprange_row = $db_net_iprange_result->fetch_assoc()) {
        $editLink = $this->generateUrl('ip-range-edit', ['ipRange' => $db_iprange_row['id']]);

        echo "<tr>";
        //echo "<td class='num'><a href='$StartURL/index.php?menu=iprange&func=view&id=".$db_iprange_row['id']."'>".$db_iprange_row['id']."</a></td>";
        echo "<td class='num'><a href='".$editLink."'>".$db_iprange_row['id']."</a></td>";
        //echo "<td><a href='$StartURL/index.php?menu=iprange&func=view&id=".$db_iprange_row['id']."'>".$db_iprange_row['name']."</a></td>";
        echo "<td><a href='".$editLink."'>".$db_iprange_row['name']."</a></td>";
        echo "<td>".$db_iprange_row['start_address']."</td>";
        echo "<td>".$db_iprange_row['end_address']."</td>";
        echo "</tr>";
      }
    ?>
    </tbody>
  </table>
  <br />
  <h3>Standorte</h3>
  <br />
  <table class="Network">
    <thead>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>KVz Prefix</th>
      <th>Kundennr. Startwert</th>
      <th>Kundennr. Endwert</th>
      <th>Aktion</th>
    </tr>
    </thead>
    <tbody>
    <?php
      while ($db_net_location_row = $db_net_localtion_result->fetch_assoc()) {
        echo "<tr>";
        echo "<td class='num'><a href='$StartURL/index.php?menu=location&func=view&id=".$db_net_location_row['id']."'>".$db_net_location_row['id']."</a></td>";
        echo "<td><a href='$StartURL/index.php?menu=location&func=view&id=".$db_net_location_row['id']."'>".$db_net_location_row['name']."</a></td>";
        echo "<td>".$db_net_location_row['kvz_prefix']."</td>";
        echo "<td>".$db_net_location_row['cid_suffix_start']."</td>";
        echo "<td>".$db_net_location_row['cid_suffix_end']."</td>";
        echo "<td>";
            if ($this->get('security.authorization_checker')->isGranted('ROLE_EDIT_LOCATION')) {
                echo "<a href=\"".$this->generateUrl('location-edit', ['networkId' => $id, 'id' => $db_net_location_row['id']])."\">Bearbeiten</a>
                &nbsp;&nbsp;
                <a href=\"".$this->generateUrl('location-clone', ['networkId' => $id, 'id' => $db_net_location_row['id']])."\">Clone</a>";
            }
        echo "</td>";
        echo "</tr>";
      }
    ?>
    </tbody>
  </table>
  <br />
  <a href="<?php echo $this->generateUrl('location-create-new', ['id' => $id]); ?>">Standort Hinzufügen</a>
  <br /><br />
  <button name="back" onclick="backButton()" type="button"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">
    Zurück
  </button>

</div>

<?php
    $db_net_localtion_result->free();
    $db_net_localtion->close();
    $db_net_iprange_result->free();
    $db_net_iprange->close();
    $db_networks->close();
    $db->close();
  //############################################################################
  } elseif (key_exists ('func', $_GET) and $_GET['func'] == 'edit') {
    $this->denyAccessUnlessGranted('ROLE_EDIT_NETWORK');

    if (key_exists ('save', $_POST)) {
      if (!$_POST['NetworkId']) {
        $sql_save = "INSERT INTO networks SET ";
        $sql_save .= "id_string='".$_POST['NetworkIdString']."',";
        $sql_save .= "name='".$_POST['NetworkName']."',";
        $sql_save .= "partner='".$_POST['NetworkPartner']."',";
        $sql_save .= "network_ready='".$_POST['NetworkReady']."',";
        $sql_save .= "area_code='".$_POST['NetworkAreaCode']."'";
        $db_save_result = $db->query($sql_save);
        if ($db_save_result) {
          $customer_saved = 'wurde hinzugefügt';
        } else {
          $customer_saved = '<span class="text red">konnte nicht hinzugefügt werden!</span>';
        }
        echo "<div class='Network box'>";
        echo "<h3>Dieses Netz $customer_saved</h3>";
        echo "<div class='Network form'><table>";
        echo "<tr><td>Netzkennung:</td><td>".$_POST['NetworkIdString']."</td></tr>";
        echo "<tr><td>Name:</td><td>".$_POST['NetworkName']."</td></tr>";
        echo "<tr><td>Partner:</td><td>".$_POST['NetworkPartner']."</td></tr>";
        echo "<tr><td>fertig seit:</td><td>".$_POST['NetworkReady']."</td></tr>";
        echo "<tr><td>Vorwahl-Bereich:</td><td>".$_POST['NetworkAreaCode']."</td></tr>";
        echo "</table></div></div>";
      } else {
        if (key_exists ('NetworkIpRange', $_POST)) $networkiprange = $_POST['NetworkIpRange']; else $networkiprange = array();
        $sql_save = "UPDATE networks SET ";
        $sql_save .= "id_string='".$_POST['NetworkIdString']."',";
        $sql_save .= "name='".$_POST['NetworkName']."',";
        $sql_save .= "partner='".$_POST['NetworkPartner']."',";
        $sql_save .= "network_ready='".$_POST['NetworkReady']."',";
        $sql_save .= "area_code='".$_POST['NetworkAreaCode']."'";
        $sql_save .= "WHERE id=".$_POST['NetworkId'];
        $db_save_result = $db->query($sql_save);
        if ($db_save_result) {
          $customer_saved = 'wurde geändert';
        } else {
          $customer_saved = '<span class="text red">konnte nicht geändert werden!</span>';
        }
        $db_iprange_network = $db->prepare($sql['iprange_network']);
        $db_iprange_network->bind_param('d', $_POST['NetworkId']);
        $db_iprange_network->execute();
        $db_iprange_network_result = $db_iprange_network->get_result();
        $ipnet = array();
        while ($ipnet_result = $db_iprange_network_result->fetch_row()) {
          array_push ($ipnet, $ipnet_result[0]);
        }
        $db_iprange_network_result->free();
        $db_iprange_network->close();

        $dipnet = array_diff ($ipnet, $networkiprange);
        $aipnet = array_diff ($networkiprange, $ipnet);

        $dsql = '';
        foreach ($dipnet as $dipnetv) {
          $dsql .= "DELETE FROM ip_ranges_networks WHERE network_id ='".$_POST['NetworkId']."' AND ip_range_id='".$dipnetv."'; ";
        }
        if ($dsql) $db_dsql_result = $db->multi_query($dsql);

        $asql = '';
        foreach ($aipnet as $aipnetv) {
          $asql .= "INSERT INTO ip_ranges_networks SET network_id ='".$_POST['NetworkId']."', ip_range_id='".$aipnetv."'; ";
        }
        if ($asql) $db_asql_result = $db->multi_query($asql);

        echo "<div class='Network box'>";
        echo "<h3>Dieses Netz $customer_saved</h3>";
        echo "<div class='Network form'><table>";
        echo "<tr><td>Netzkennung:</td><td>".$_POST['NetworkIdString']."</td></tr>";
        echo "<tr><td>Name:</td><td>".$_POST['NetworkName']."</td></tr>";
        echo "<tr><td>Partner:</td><td>".$_POST['NetworkPartner']."</td></tr>";
        echo "<tr><td>fertig seit:</td><td>".$_POST['NetworkReady']."</td></tr>";
        echo "<tr><td>Vorwahl-Bereich:</td><td>".$_POST['NetworkAreaCode']."</td></tr>";
        echo "</table></div></div>";
      }
    } elseif (key_exists ('delete', $_POST)) {
      $sql_delete = "DELETE FROM networks WHERE id = ".$_POST['NetworkId'];
      $db_delete_result = $db->query($sql_delete);
      $sql_delete = "DELETE FROM ip_ranges_networks WHERE network_id = ".$_POST['NetworkId'];
      $db_delete_result = $db->query($sql_delete);

      echo "<div class='Network box'>";
      echo "<h3>Dieses Netwerk wurde gelöscht</h3>";
      echo "<div class='Network form'><table>";
      echo "<tr><td>Netzkennung:</td><td>".$_POST['NetworkIdString']."</td></tr>";
      echo "<tr><td>Name:</td><td>".$_POST['NetworkName']."</td></tr>";
      echo "<tr><td>Partner:</td><td>".$_POST['NetworkPartner']."</td></tr>";
      echo "<tr><td>fertig seit:</td><td>".$_POST['NetworkReady']."</td></tr>";
      echo "<tr><td>Vorwahl-Bereich:</td><td>".$_POST['NetworkAreaCode']."</td></tr>";
      echo "</table></div></div>";
      
    } else {
      if (key_exists ('id', $_GET)) $id = $_GET['id']; else $id = 0;
      $db_networks = $db->prepare($sql['network_single']);
      $db_networks->bind_param('d', $id);
      $db_networks->execute();
      $db_networks_result = $db_networks->get_result();
      $net_result = $db_networks_result->fetch_assoc();
      $db_networks_result->free();
      if (!$net_result) $net_result = array();
      if (key_exists ('id', $net_result))             $net_db['id']             = $net_result['id'];            else $net_db['id']            = 0;
      if (key_exists ('id_string', $net_result))      $net_db['id_string']      = $net_result['id_string'];     else $net_db['id_string']     = '';
      if (key_exists ('name', $net_result))           $net_db['name']           = $net_result['name'];          else $net_db['name']          = '';
      if (key_exists ('partner', $net_result))        $net_db['partner']        = $net_result['partner'];       else $net_db['partner']       = '';
      if (key_exists ('network_ready', $net_result))  $net_db['network_ready']  = $net_result['network_ready']; else $net_db['network_ready'] = '';
      if (key_exists ('area_code', $net_result))      $net_db['area_code']      = $net_result['area_code'];     else $net_db['area_code']     = '';
    
      $db_iprange = $db->query( $sql['iprange']);
      while ($db_ipranges = $db_iprange->fetch_assoc()) {
        $ipranges[$db_ipranges['id']] = $db_ipranges['name']." (".$db_ipranges['start_address']." - ".$db_ipranges['end_address'].")";
      }
      $db_iprange->close();

      $db_net_iprange = $db->prepare($sql['net_iprange']);
      $db_net_iprange->bind_param('d', $id);
      $db_net_iprange->execute();
      $db_net_iprange_result = $db_net_iprange->get_result();
      while ($db_iprange_row = $db_net_iprange_result->fetch_assoc()) {
        $net_ipranges[$db_iprange_row['id']] = true;
      }
      $db_net_iprange_result->free();
    
?>
<form id="netForm" action="<?php echo $StartURL.'/index.php?menu=network&func=edit'; ?>" method="post">
<div class="Network box">
  <h3>Netz <?php if ($net_db['id']) echo "bearbeiten"; else echo "hinzufügen"; ?></h3>
  <div class="Network form">
    <input name="NetworkId" id="NetworkId" type="hidden" value="<?php echo $net_db['id']; ?>">
    <p>
      <label for="NetworkIdString">Netzkennung</label><br />
      <input id="NetworkIdString" type="text" size="60" name="NetworkIdString" <?php if ($net_db['id']) echo "readonly"; ?> value="<?php echo $net_db['id_string']; ?>">
    </p>
    <p>
      <label for="NetworkName">Name</label><br />
      <input id="NetworkName" type="text" size="60" name="NetworkName" value="<?php echo $net_db['name']; ?>">
    </p>
    <p>
      <label for="NetworkPartner">Partner</label><br />
      <select id="NetworkPartner" name="NetworkPartner">
        <?php
          foreach ($option_partner as $key => $value) {
            if ($key == '-n-') $key='';
            if (key_exists ('partner', $net_db) and $net_db['partner'] == $key) {
              echo "<option value='$key' selected>$value</option>\n";
            } else {
              echo "<option value='$key'>$value</option>\n";
            }
          }
        ?>
      </select>
    </p>
    <p>
      <label for="NetworkReady">fertig seit</label><br />
      <input id="NetworkReady" type="text" size="60" name="NetworkReady" value="<?php echo $net_db['network_ready']; ?>">
    </p>
    <p>
      <label for="NetworkAreaCode">Vorwahl-Bereich</label><br />
      <input id="NetworkAreaCode" type="text" size="60" name="NetworkAreaCode" value="<?php echo $net_db['area_code']; ?>">
    </p>
    <?php if ($net_db['id']) { ?>
    <p>
      <label for="NetworkIpRange">IP-Blöcke wählen</label><br />
      <select id="NetworkIpRange" name="NetworkIpRange[]" multiple="multiple" class="bsmSelect">
        <?php
          foreach ($ipranges as $key => $value) {
            if (key_exists ($key, $net_ipranges)) {
              echo "<option value='$key' selected>$value</option>\n";
            } else {
              echo "<option value='$key'>$value</option>\n";
            }
          }
        ?>
      </select>
    </p>
    <?php } ?>
  </div>
  <h3 class="Network"></h3>
  <div class="Network form">
    <input name='none' id='command' type='hidden' value='yes'>
    <button name="back" onclick="backButton()" type="button"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px">
        Zurück
    </button>&nbsp;&nbsp;&nbsp;&nbsp;
    <button name="save" onclick="netSubmit()" type="button"><img alt="" src="<?php echo $StartURL.'/_img/Yes.png'; ?>" height="12px">
        Speichern
    </button>&nbsp;&nbsp;&nbsp;&nbsp;
    <button name="reset" type="reset"><img alt="" src="<?php echo $StartURL.'/_img/Erase.png'; ?>" height="12px">
        Zurücksetzten
    </button>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <?php
    if ($id) {
        echo "<button name='delete' onclick='netDelete()' type='button'>"
            .".<img alt='' src='$StartURL/_img/delete.png' height='12px'> Löschen"
            ."</button>";
    }
    ?>
  </div>
</div>
</form>
<div id="dialog"></div>
<?php
      $db_net_iprange->close();
      $db_networks->close();
      $db->close();
    }
  //############################################################################
  } else {
  $db_networks = $db->query( $sql['network']);

?>
<div class="Network box">
  <h3>Netze</h3>
  <br /><a href='<?php echo $StartURL; ?>/index.php?menu=network&func=edit'>Hinzufügen</a><br /><br />
  <table class="Network">
    <thead>
    <tr>
      <th>ID</th>
      <th>Netzkennung</th>
      <th>Name</th>
      <th>Partner</th>
      <th>fertig seit</th>
      <th>Aktionen</th>
    </tr>
    </thead>
    <tbody>
    <?php
      while ($db_networks_row = $db_networks->fetch_assoc()) {
        echo "<tr>";
        echo "<td class='num'><a href='$StartURL/index.php?menu=network&func=view&id=".$db_networks_row['id']."'>".$db_networks_row['id']."</a></td>";
        echo "<td><a href='$StartURL/index.php?menu=network&func=view&id=".$db_networks_row['id']."'>".$db_networks_row['id_string']."</a></td>";
        echo "<td><a href='$StartURL/index.php?menu=network&func=view&id=".$db_networks_row['id']."'>".$db_networks_row['name']."</a></td>";
        echo "<td><a href='$StartURL/index.php?menu=network&func=view&id=".$db_networks_row['id']."'>".$db_networks_row['partner']."</a></td>";
        echo "<td><a href='$StartURL/index.php?menu=network&func=view&id=".$db_networks_row['id']."'>".$db_networks_row['network_ready']."</a></td>";
        echo "<td>";
            if ($this->get('security.authorization_checker')->isGranted('ROLE_EDIT_NETWORK')) {
                echo "<a href='$StartURL/index.php?menu=network&func=edit&id=".$db_networks_row['id']."'>Bearbeiten</a>";
            }
        echo "</td>";
        echo "</tr>";
      }
    ?>
    </tbody>
  </table>
  <h3 class="Network"></h3>
  <div class="Network form">
    <span class="text blue">ICINGA: </span>
      <a href="<?php echo \Wisotel\Configuration\Configuration::get('urlToIcinga'); ?>" target="_blank">
        <?php echo \Wisotel\Configuration\Configuration::get('companyName'); ?>icinga
      </a> /
    <br />

    <?php
    
    $purtelLogin = null;

    if (isset($customer, $customer['purtel_login'])) {
        $purtelLogin = $customer['purtel_login'];
    }

    $purtelReseller = \Wisotel\Configuration\Configuration::get('purtelReseller');

    $purtelLogonUrl = 'https://www2.purtel.com/res/'.$purtelReseller.'/index.php?username=%s&passwort=%s'
        .'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1'
        .'&admin_ns_suchbegriff='.$purtelLogin.'&tab_id=2&marke=VoIP-Status';

    $purtelAdminLogonUrl = 'https://www2.purtel.com/res/'.$purtelReseller.'/index.php?username=%s&passwort=%s'
        .'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1'
        .'&admin_ns_suchbegriff=&tab_id=2&marke=Anschluss suchen';
    ?>

    <span class="text blue">Purtel: </span>
    <?php
    $purtelReseller = \Wisotel\Configuration\Configuration::get('purtelReseller');

    $purtelLogonUrl = 'https://www2.purtel.com/res/'.$purtelReseller.'/index.php?username=%s&passwort=%s'
        .'&senden=1&site=admin&link=login&aktion=&admin_ns_suchart=1&admin_ns_suche=1'
        .'&admin_ns_suchbegriff='.$purtel_master.'&tab_id=2&marke=VoIP-Status';
        
    echo '</p><span id="purtelAdmin" class="admin text blue">Purtel Admin:</span>';

    $purtelAdminAccounts = $this->getDoctrine()->getRepository('AppBundle\Entity\PurtelAdminAccount')->findByAuthorizedUser(
        $this->get('security.token_storage')->getToken()->getUser()
    );

    foreach ($purtelAdminAccounts as $purtelAdminAccount) {
        echo sprintf('<span class="fancy-button outline round grey"><a target="_blank" href="%s">%s</a></span>',
            sprintf($purtelLogonUrl, $purtelAdminAccount->getUsername(), $purtelAdminAccount->getPassword()),
            $purtelAdminAccount->getIdentifier()
        )."\n";
    }

    echo '</p>';
    
    ?>

    <br />
  </div>
</div>

<?php
    $db_networks->free();
    $db->close();
  }
?>
