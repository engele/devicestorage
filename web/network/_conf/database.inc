<?php
  $sql = array (
    // Network
    'network' => "
      SELECT * FROM networks
      ORDER BY id_string
    ",

    'network_single' => "
      SELECT * FROM networks
      WHERE id = ?
    ",
    // IP-Blöcke
    'net_iprange' => "
      SELECT ipr.id, ipr.start_address, ipr.end_address, ipr.name  FROM  ip_ranges AS ipr, ip_ranges_networks AS iprn, networks AS net
      WHERE net.id = iprn.network_id
      AND iprn.ip_range_id = ipr.id
      AND net.id = ?
    ",

    'iprange_network' => "
      SELECT ip_range_id FROM ip_ranges_networks
      WHERE network_id = ?
    ",

    'iprange' => "
      SELECT * FROM ip_ranges
    ",
    // Locations
    'net_location' => "
      SELECT * FROM locations
      WHERE network_id = ?
    ",
  );

$GLOBALS['sql'] = & $sql;
?>
