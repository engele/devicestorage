/* *******************************************************************************************
*
*	jQuery Multi-Accordion
*	Copyright (c) 2001-2011. JJ Jiles
*	site: http://jjis.me
*
*	Licences: MIT, GPL
*	http://www.opensource.org/licenses/mit-license.php
*	http://www.gnu.org/licenses/gpl.html
*
**********************************************************************************************
*
*	Use:
*		Ability to have multiple accordions without limitations of HTML hierarchy
*		It also allows the control of multiple accordions to interact
*
*		This accordion technique is not limited to HTML layout constraints. I use
*		traversing to locate the specified elements so that you can design your
*		HTML more freely. Hopefully I've compensated for everything
*
*		Callbacks are available for "afterclose" and "afteropen", but only on
*		the accordion current accordion group. Callbacks will not fire on "afterclose"
*		on the non-focused accordion. i.e. Accordion-2 closes Accordion-1 collapsibles
*
*	Required:
*		parent      : '#id-of-element' or '.class-of-element' or 'tagName' !not an object
*		header      : '#id-of-element' or '.class-of-element' or 'tagName' !not an object
*		collapsible : '#id-of-element' or '.class-of-element' or 'tagName' !not an object
*
*	Optional:
*		afterclose  : function() { alert('this fires after a collapsible is closed'); }
*		afteropen   : function() { alert('this fires after a collapsible is opened'); }
*
*	HTML Markup Example:
*		<style>
*			body { font: 100%/105% Arial, san-serif; background: #dbdbdb; margin: 20px auto; }
*
*			#content-container   { width: 900px; margin: 0 auto; }
*			.sub-content         { background: #fff; padding: 20px; margin-bottom: 20px; }
*			.accordion-headers a { color: #333; font-weight: bold; font-size: 85%; display: inline-block; *display: inline; *zoom: 1; padding: 8px; margin: 0 12px 0 12px; border: #d3d3d3; }
*			
*			.sub-content, .accordion-headers a, .collapsible li { 
*				border-radius: 4px; -moz-border-radius: 4px; -webkit-border-radius: 4px; 
*			}
*			
*			.collapsible    { position: relative; z-index: 2; height: auto; list-style: none; margin: -6px 0 0 0; padding: 0; display: none; }
*			.collapsible li { font-size: 90%; color: #333; margin: 4px 0 4px 0; padding: 14px; }
*			
*			.clickable-header-accordion-1, .accordion1-collapsibles li { background: #e0d3e3; }
*			.clickable-header-accordion-2, .accordion2-collapsibles li { background: #f5edf6; }
*		</style>
*
*		<div id="content-container">
*			<!-- subsection of content container accordion 1 and 2 -->
*			<div class="sub-content">
*				<div class="accordion-headers">
*					<a href="#" class="clickable-header-accordion-1">Click Me</a>
*					<a href="#" class="clickable-header-accordion-2">Click Me</a>
*				</div>
*			
*				<ul class="collapsible accordion1-collapsibles">
*					<li>First Accordion :: Item 1</li>
*					<li>First Accordion :: Item 2</li>
*					<li>First Accordion :: Item 3</li>
*					<li>First Accordion :: Item 4</li>
*				</ul>
*				<ul class="collapsible accordion2-collapsibles">
*					<li>Second Accordion :: Item 1</li>
*					<li>Second Accordion :: Item 2</li>
*					<li>Second Accordion :: Item 3</li>
*					<li>Second Accordion :: Item 4</li>
*				</ul>
*			</div>
*			<!-- // -->
*			
*			<!-- next subsection of content container accordion 1 and 2 -->
*			<div class="sub-content">
*				<div class="accordion-headers">
*					<a href="#" class="clickable-header-accordion-1">Click Me</a>
*					<a href="#" class="clickable-header-accordion-2">Click Me</a>
*				</div>
*			
*				<ul class="collapsible accordion1-collapsibles">
*					<li>First Accordion :: Item 1</li>
*					<li>First Accordion :: Item 2</li>
*					<li>First Accordion :: Item 3</li>
*					<li>First Accordion :: Item 4</li>
*				</ul>
*				<ul class="collapsible accordion2-collapsibles">
*					<li>Second Accordion :: Item 1</li>
*					<li>Second Accordion :: Item 2</li>
*					<li>Second Accordion :: Item 3</li>
*					<li>Second Accordion :: Item 4</li>
*				</ul>
*			</div>
*			<!-- // -->
*		</div>
*		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
*		<script type="text/javascript" src="jquery.multi-accordion.js"></script>
*		<script type="text/javascript">
*		$(document).ready(function() {
*			var accordions = [
*				{
*					'parent'      : '.sub-content',                   // parent element containing the clickable header and collapsible
*					'header'      : '.clickable-header-accordion-1',  // clickable header
*					'collapsible' : '.accordion1-collapsibles'        // collapsible element
*				},		
*				{
*					'parent'      : '.sub-content',                      // parent element containing the clickable header and collapsible
*					'header'      : '.clickable-header-accordion-2',     // clickable header
*					'collapsible' : '.accordion2-collapsibles',          // collapsible element
*					'afterclose'  : function() { alert('callback that fires after close'); },// callback that is fired after the accordion is closed
*					'afteropen'   : function() { alert('callback that fires after open'); }  // callback that is fired after the accordion is opened
*				}
*			];
*			$().accordion(accordions);
*		});
*		</script>
*
******************************************************************************************** */	
(function($){
     $.fn.accordion = function(options) {
		 
		// establish the options
		var options         = typeof(options) != 'undefined' ? options : '';
		
		// establish some globals so we can run multiple accordions
		var options_len = options.length-1;
		var _cnt_;
		var _header      = [];
		var _collapsible = [];
		var _parent      = [];
		var _callback    = [];
		var _afteropen   = [];
		var _afterclose  = [];
		
		// loop through the accordions and bind events
		for ( _cnt_=0; _cnt_ <= options_len; _cnt_++ ) {
			
			/* ***
			* set counter as a global for the array */
			var $cnt = _cnt_;
			
			/* ***
			* primary elements specified */
			_parent[_cnt_]      = typeof(options[_cnt_].parent)      != 'undefined' ? options[_cnt_].parent      : '';
			_header[_cnt_]      = typeof(options[_cnt_].header)      != 'undefined' ? options[_cnt_].header      : '';
			_collapsible[_cnt_] = typeof(options[_cnt_].collapsible) != 'undefined' ? options[_cnt_].collapsible : '';
		
			/* ***
			* setup the callback function */
			_afterclose[_cnt_] = typeof(options[_cnt_].afterclose)!= 'undefined' ? options[_cnt_].afterclose : '';
			_afterclose[_cnt_] = _afterclose[_cnt_];
			_afteropen[_cnt_] = typeof(options[_cnt_].afteropen)!= 'undefined' ? options[_cnt_].afteropen : '';
			_afteropen[_cnt_] = _afteropen[_cnt_];
			
			
			/* ***
			* hide all collapsible items */
			$(_collapsible[_cnt_]).css({ 'display' : 'none' });
				
			/* ****
			* 	bind the accordion header click event. we handle the event binding this
			* 	so we don't overwrite existing clicks on previously established accordions
			**** */
			$(_parent[_cnt_]).find(_header[_cnt_]).each(function() {
				
				var _$cnt   = $cnt;
				// current header in the array
				var $header = $(this);
				
				/* ***
				* bind click event to it */
				$header.unbind('click');
				$header.bind('click',function() {				
					var $header        = $(this);
					
					/* ****
					* Traverse and find the header's parent and collapsibles */					
					var $header_parent = $header.parentsUntil(_parent[_$cnt]).parent(); // parent
					var content_obj    = $header_parent.find(_collapsible[_$cnt]);      // collapsible
					
					/* ****
					* if the clicked header collapsible is currently closed, open it */
					if(content_obj.is(":hidden")) {
					
						/* ***
						* loop through all established accordions and close open collapsibles */
						for ( j=options_len; j >=0; j-- ) {
							$(_parent[j]).find(_collapsible[j]).each(function() {
								$(this).slideUp('fast');
							});
						}
						
						/* ****
						* show the collapsible of the clicked header */
						content_obj.slideDown('fast',function() {
							/* ****
							*  fire the AfterOpen callback */
							if(typeof _afteropen[_$cnt] == 'function'){
								_afteropen[_$cnt].call(this);
							}
						});
						
						
					/* ****
					* if the clicked header collapsible is currently open, close it */
					} else {
						
						/* ****
						* close the collapsible of the clicked header */
						content_obj.slideUp('fast',function() {
							/* ****
							* fire the AfterClose callback */
							if(typeof _afterclose[_$cnt] == 'function'){
								_afterclose[_$cnt].call(this);
							}
						});
					}
					
					return false;
					
				});
			});
		}
     }
})(jQuery);