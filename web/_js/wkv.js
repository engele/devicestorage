function forceLogin() {
  var html;
  var user;
  var pw;
  html = "<form id='LoginForm' action='#'>";
  html += "<table>";
  html += "<tr><td><label for='LoginUser'>Benutzer:</label></td><td><input type='text' id='LoginUser' value='' name='LoginUser'></td></tr>";
  html += "<tr><td>&nbsp;</td><td></td></tr>";
  html += "<tr><td><label for='LoginPW'>Passwort:</label></td><td><input type='password' id='LoginPW' value='' name='LoginPW'></td></tr>";
  html += "</table>";
  html += "</form>";
  
  $("body").css('overflow', 'hidden');
  $("#LoginBack").show();
  $("#ForceLogin").html(html);
  $("#ForceLogin").dialog({
    resizable: false,
    modal: true,
    title: 'Login KV',
    position: {my: 'top', at: 'top', of: '#Container'},
    height: 200,
    width: 300,
    buttons: {
      'Login': function () {
        user  = $("#LoginUser").val();
        pw    = $("#LoginPW").val();
        $(this).dialog('close');
        callLogin(true, user, pw);
      },
      'Abbruch': function () {
        user  = '';
        pw    = '';
        $(this).dialog('close');
        callLogin(false, user, pw);
      }
    }
  });
}

function callLogin(save, user, pw) {
  if (save){
    var StartPath   = $("#StartPath").val();
    var SelPath     = $("#SelPath").val();
    var StartURL    = $("#StartURL").val();
    $.ajax({
      type: "POST",
      url: decodeURIComponent(StartURL)+"/ajaxLogin.php",
      data: "StartPath="+StartPath+"&SelPath="+SelPath+"&User="+user+"&PW="+pw,
      success: function(data){ajaxLogin(data)}
    });
  } else {
    location.reload();
  }
}

function ajaxLogin(data) {
  var response  = $.parseJSON(data);
  var login     = response.Login;
  
  if (login) {
    $("body").css('overflow', 'hidden');
    $("#ForceLogin").remove();
  }
  location.reload();
}

$.extend($.ui.accordion.prototype.options, {
  beforeActivate: function(event, ui) {
    // The accordion believes a panel is being opened
    if (ui.newHeader[0]) {
      var currHeader  = ui.newHeader;
      var currContent = currHeader.next('.ui-accordion-content');
    // The accordion believes a panel is being closed
    } else {
      var currHeader  = ui.oldHeader;
      var currContent = currHeader.next('.ui-accordion-content');
    }
    // Since we've changed the default behavior, this detects the actual status
    var isPanelSelected = currHeader.attr('aria-selected') == 'true';
 
    // Toggle the panel's header
    currHeader.toggleClass('ui-corner-all',isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top',!isPanelSelected).attr('aria-selected',((!isPanelSelected).toString()));
  
    // Toggle the panel's icon
    currHeader.children('.ui-icon').toggleClass('ui-icon-plus',isPanelSelected).toggleClass('ui-icon-minus',!isPanelSelected);
  
    // Toggle the panel's content
    currContent.toggleClass('accordion-content-active',!isPanelSelected);
    if (isPanelSelected) { currContent.slideUp(); }  else { currContent.slideDown(); }
      
    return false; // Cancels the default action
  },
  collapsible: true
});

$(function($){
  $.datepicker.regional['de'] = {
    clearText:        'löschen',
    clearStatus:      'aktuelles Datum löschen',
    closeText:        'schließen',
    closeStatus:      'ohne Änderungen schließen',
    prevText:         '<zurück',
    prevStatus:       'letzten Monat zeigen',
    nextText:         'Vor>',
    nextStatus:       'nächsten Monat zeigen',
    currentText:      'heute', currentStatus: '',
    monthNames:       ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
    monthNamesShort:  ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
    monthStatus:      'anderen Monat anzeigen', yearStatus: 'anderes Jahr anzeigen',
    weekHeader:       'Wo', weekStatus: 'Woche des Monats',
    dayNames:         ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
    dayNamesShort:    ['So','Mo','Di','Mi','Do','Fr','Sa'],
    dayNamesMin:      ['So','Mo','Di','Mi','Do','Fr','Sa'],
    dayStatus:        'Setze DD als ersten Wochentag',
    dateStatus:       'Wähle D, M d',
    dateFormat:       'dd.mm.yy',
    firstDay:         1, 
    initStatus:       'Wähle ein Datum',
    isRTL:            false
  };
  $.datepicker.setDefaults($.datepicker.regional['de']);
});

function accordion_expand_all()
{
  var sections = $('.accordion').find("h3");
  sections.each(function(index, section){
    if ($(section).hasClass('ui-state-default')) {
      $(section).click();
    }
  });
}

function accordion_collapse_all()
{
  var sections = $('.accordion').find("h3");
  sections.each(function(index, section){
    if ($(section).hasClass('ui-state-active')) {
      $(section).click();
    }
  });
}

function backButton() {
  parent.history.back();
}

$(document).ready(function(){
  $.tablesorter.defaults.widgets    = ['zebra'];
  $.tablesorter.defaults.locale     = 'de';
  $.tablesorter.defaults.dateFormat = 'ddmmyyyy';

  $("#Container").corner('5px');
  $("#Logo").corner('5px');
  $("#Content").corner('5px');
  $("#Button").corner('5px');
  $(".box h3").corner('top 5px');
  $(".box div").corner('bottom 5px');
  
  $(":input.datepicker").datepicker({
    constrainInput: false,
    changeMonth:    true,
    changeYear:     true
  });

  $("#Menu").menu();
  $(".accordion-single").accordion({active: false, heightStyle: 'content'});
  $(".accordion").accordion({active: false, heightStyle: 'content'});
  accordion_expand_all();

  $("select[multiple].bsmSelect").bsmSelect({
    title: 'Bitte wählen Sie ...',
    animate: true,
    hideWhenAdded: true,
    removeLabel: 'X',
  });
  
  if ($("#ForceLogin").length){
    $('#ForceLogin').on('keyup', function(e){
      if (e.keyCode == 13) {
        $(':button:contains("Login")').click();
      }
    });
    forceLogin();
  }
});

