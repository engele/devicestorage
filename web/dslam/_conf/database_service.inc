<?php
    $sql = array (
		// Kundensuche
		'customer' => "
		  SELECT * FROM `customers`
		  WHERE id = ?
		",
		// IP-Addresses
		'ip_addresses' => "
		  SELECT ip_address FROM customers 
		  WHERE INET_ATON(ip_address) BETWEEN ? AND ?
		",
		// location by id  // --> pppoe_type added jl
		'locations' => "
		  SELECT * FROM locations
		  WHERE id = ?
		",
        // all locations
        'allLocations' => "SELECT 
            `locations`.`id`, `locations`.`name`, `locations`.`network_id`, `networks`.`name` as network_name 
            FROM `locations` LEFT JOIN `networks` ON `locations`.`network_id` = `networks`.`id` 
            WHERE `locations`.`dslam_conf` = '1'
            ORDER BY `name` ASC",
		// lsa_kvz_partitions
		'lsa_kvz_partitions' => "
		  SELECT * FROM lsa_kvz_partitions
		  WHERE location_id = ?
		",
		// lsa_kvz_partition
		'lsa_kvz_partition' => "
		  SELECT * FROM lsa_kvz_partitions
		  WHERE id = ?
		",
		// cards
		'cards' => "
		  SELECT c.id, c.port_amount, c.line as line, c.name AS cardname, t.name AS cardtype FROM cards AS c, card_types AS t
		  WHERE c.card_type_id = t.id
		  AND c.location_id = ?
		",
		// card
		'card' => "
		  SELECT c.id, c.port_amount, c.line as line, c.name AS cardname, t.name AS cardtype FROM cards AS c, card_types AS t
		  WHERE c.card_type_id = t.id
		  AND c.id = ?
		",
		// product
		'produkte' => "
		  SELECT * FROM produkte
		  WHERE produkt = ?
		",
		// product select
		'produkte_sel' => "
		  SELECT * FROM produkte ORDER BY group_id
		",
	
		// Purtel account
		'purtel_account' => "
		  SELECT * FROM purtel_account
		  WHERE cust_id = ? AND master = 1
		",

        'ipRangesVlansByNetworkId' => 
            "SELECT ipr.`vlan_ID` FROM `ip_ranges` ipr 
            INNER JOIN `ip_ranges_networks` iprn ON iprn.`ip_range_id` = ipr.`id` 
            WHERE iprn.`network_id` = ? AND ipr.`vlan_ID` IS NOT NULL
        ",
	);

$GLOBALS['sql'] = &$sql;  
?>
