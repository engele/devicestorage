<?php

switch (\Wisotel\Configuration\Configuration::get('companyName')) {
    case 'ZEAG':
        require_once __DIR__.'/zeag_produkte.inc';

        zeag_produkte($usenet);

        break;

    case 'WiSoTEL':
        require_once __DIR__.'/wiso_produkte.inc';

        wiso_produkte($usenet);
        
        break;

    case 'Stadtwerke Bühl':
        require_once __DIR__.'/buehl_produkte.inc';

        buehl_produkte($usenet);

        break;
}
