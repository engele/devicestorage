<?php
######################################################
# Functions
######################################################
require_once __DIR__.'/zeag_produkte.inc';
require_once __DIR__.'/wiso_produkte.inc';
if (file_exists(__DIR__.'/produkte_clear.inc')) {
    require_once __DIR__.'/produkte_clear.inc';
}
if (file_exists(__DIR__.'/produkte_update.php')) {
    require_once __DIR__.'/produkte_update.php';
}
require_once __DIR__.'/../../vendor/wisotel/configuration/Configuration.php';
require_once __DIR__.'/../../csvExport/vendor/streamsocket/streamsocket/Lib/StreamSocket.php';

use \StreamSocket\Lib\StreamSocket;
use \StreamSocket\Exception\WaitUntilStringIsFoundException;

/**
 * DSLAM open (not Iskratel)
 */
function DSLAM_open ($usenet,$logon_ip, $logon_port,$DSLAMLOGON,$DSLAMPW) {
    $usenet = socket_create (AF_INET, SOCK_STREAM, SOL_TCP);
    socket_bind ($usenet, $ip_source);
    socket_set_option($usenet, SOL_SOCKET, SO_RCVTIMEO, array('sec' => 2, 'usec' => 0));
    socket_set_option($usenet, SOL_SOCKET, SO_SNDTIMEO, array('sec' => 2, 'usec' => 0));


    $buffer = '';
    $result = socket_connect ($usenet, $logon_ip, $logon_port);
    if($result == false) { 
      echo  "DSLAM Connection failed\n".$logon_ip." Port ".$logon_port." Error ".$errno." Errorstr ".$errstr."<br />"; 
      return false; 
    }
    else {
    $usenet->write( "$DSLAMLOGON\r\n");
    $usenet->write( "$DSLAMPW\r\n");
#    socket_set_nonblock($usenet);
/*    $usenet->write( "show trapp \r\n");

    while (true) {
        $buf = $usenet->read($usenet, 1);
        $buffer .= $buf;
        $i ++;
        if (($txt = substr($buffer,-8)) == 'password'){
            
            
            $usenet = false;
        }

        #if (($txt = substr($buffer,-5)) == 'Error') break;
        #if (($txt = substr($buffer,-7)) == 'invalid') break;
        #if (($txt = substr($buffer,-8)) == 'activate') break;
        #if (($txt = substr($buffer,-9)) == 'show trap') break;
    }
        if ($usenet == false) {
            socket_close ($usenet);
            echo " &nbsp;&nbsp;&nbsp;&nbsp;Nanu <pre>$buffer</pre>";
        }
*/    
    return $usenet;
    }
}

function DSLAM_close ($usenet,$Dslam_comando,$client_id,$exit_in_up,$exit_line,$down_red) {
    # $usenet->write("show trap \r\n") 
    $usenet->write( "show trap \r\n");
        if ($client_id == '') {
        
            try {
                $usenet
                ->waitUntilStringIsFound('show trap', 40, null, false)
                ->write("logout\r\n");
                } catch (WaitUntilStringIsFoundException $exception) {
                    echo prettifyWaitUntilStringIsFoundException($exception);
                    echo '<br /><br /><pre>'.$usenet->getLastReadData().'</pre>';
                }
                #echo sprintf('<pre>%s</pre>',
                #str_replace("Error", "<span class='text red'> Error </span>", $usenet->getLastReadData())
                #);
                
                $usenet->write( "logout\r\n");
                $usenet->disconnect();
                $buffer = $usenet->getLastReadData();
                $pos = strpos($buffer,'last login');
                $pos1 = strpos($buffer,'show trap');
                $buffer = substr($buffer,$pos,$pos1);
                $buffer = str_replace("Error", "<font color ='text red'> Error </font>", $buffer); 
                $buffer = str_replace("invalid token", "<font color ='text red'> invalid token</font> ", $buffer); 
                $buffer = str_replace("1D", "", $buffer);# -[\[ 
                $buffer = str_replace("-[\[ [", "", $buffer);
                $buffer = str_replace("[", "", $buffer);
                
                echo " <pre>$buffer</pre>";
            
        } else {
            try {
                $usenet
                ->waitUntilStringIsFound('show trap', 40, null, false)
                ->write("logout\r\n");
                } catch (WaitUntilStringIsFoundException $exception) {
                    echo prettifyWaitUntilStringIsFoundException($exception);
                    echo '<br /><br /><pre>'.$usenet->getLastReadData().'</pre>';
                }
                
                $buf = $usenet->getLastReadData();
                $buffer .= $buf;
                $buffer = str_replace("Error", "<span class='text red'> Error </span>",$buffer);
                $buffer = str_replace("up", "<span class='text green'> up </span>",$buffer);
                $buffer = str_replace("$down_red", "<span class='text red'>$down_red</span>",$buffer);
                $pos = strpos($buffer,'last login');
                $pos1 = strpos($buffer,'show trap');
                $buffer = substr($buffer,$pos,$pos1);
                #print ("pos $pos pos1 $pos1");
                
            
            $usenet->write( "logout\r\n");
            $usenet->disconnect();

            $buffer = str_replace(" up ", "<font color='green'> up </font>", $buffer);
            if (strpos($buffer, '#configure') !== false) {
            } else {
                $buffer = str_replace("$down_red", "<font color ='red'> $down_red </font>", $buffer); 
            }
            $buffer = str_replace("line count", "<font color s='grey'> </font></font></font>line count ", $buffer); 
            $buffer = str_replace("-[1D", "", $buffer);# -[\[ 
            $buffer = str_replace("\\", "", $buffer);# -[\[ 
            $buffer = str_replace("1D", "", $buffer);# -[\[ 
            $buffer = str_replace("|", "", $buffer);# -[\[ 
            $buffer = str_replace("[", "", $buffer);# -[\[ 
            #$buffer = str_replace("/", "", $buffer);
            $buffer = str_replace("[", "", $buffer);
            $buffer = str_replace("Error", "<font color ='red'> Error </font>", $buffer); 
            $buffer = str_replace(" no ", "<font color ='red'> no </font>", $buffer); 
            $buffer = str_replace("invalid token", "<font color ='red'> invalid token</font> ", $buffer); 
            if ( $display_ende == true) {
                  echo " &nbsp;&nbsp;&nbsp;&nbsp;<font color ='text blue'><br />#################################################################</font><br />";
                  echo " &nbsp;&nbsp;&nbsp;&nbsp;# Bitte ?berpr?fen ob es Fehler vom DSLAM gab !!!!<br />";
                  echo " &nbsp;&nbsp;&nbsp;&nbsp;#<font color ='text green'>Online DSLAM Anwort>Connected\n</font><br />";
                  echo " &nbsp;&nbsp;&nbsp;&nbsp;<font color ='text blue'>#################################################################</font><br />";
            }
            echo " &nbsp;&nbsp;&nbsp;&nbsp;<pre>$buffer</pre>";
            if ( $display_ende == true) {
                  echo " &nbsp;&nbsp;&nbsp;&nbsp;<font color ='text blue'>#################################################################<br />";
                  echo " &nbsp;&nbsp;&nbsp;&nbsp;#CLI Kommandos zum sicheern<br />";
                  echo " &nbsp;&nbsp;&nbsp;&nbsp;#admin save<br />";
                  echo " &nbsp;&nbsp;&nbsp;&nbsp;#admin software-mngt database upload actual-active $tftpserver:dm_complete_$Backup<br />";
                  echo " &nbsp;&nbsp;&nbsp;&nbsp;<font color ='text blue'>#################################################################</font><br />";
            }
        }#    $Dslam_comando = "";

}

/**
 * Iskratel DSLAM open
 * 
 * @param string $logon_ip
 * @param integer $logon_port
 * @param string $DSLAMLOGON
 * @param string $DSLAMPW
 * 
 * @return \StreamSocket\Lib\StreamSocket
 */
function DSLAM_open_iskratel ($logon_ip, $logon_port, $DSLAMLOGON, $DSLAMPW) {
    try {
        // Create new tcp stream socket to DSLAM
        $stream = StreamSocket::newInstance()->connect(
            StreamSocket::TCP, // connection type
            $logon_ip,  // connection ip-address
            $logon_port, // connection port
            array('sec' => 10, 'usec' => 0) // connection timeout
        );

        $stream
            // wait for login promt
            ->waitUntilStringIsFound('login:', null, 'Did not receive login-prompt', false)
            // write username
            ->write($DSLAMLOGON."\r\n")
            // wait for password promt
            ->waitUntilStringIsFound('password:', null, 'Did not receive password-prompt', false)
            // write password
            ->write($DSLAMPW."\r\n")
            // wait for command promt or 'Login incorrect'-message
            ->waitUntilStringIsFound('WISOTEL_PONO>', null, null, false, ['incorrect' => 'Login incorrect'])
        ;

        return $stream; // return the StreamSocket instance
    } catch (Exception $exception) {
        if ($exception instanceof WaitUntilStringIsFoundException) {
            // A 'WaitUntilStringIsFoundException'-exception was thrown.
            // See if it happend while waiting for login confirmation.
            if ('Login incorrect' == $exception->getMessage()) {
                echo sprintf('DSLAM Connection failed - %s Port: %s Error: %s Username: %s Passwort: %s<br />',
                    $logon_ip,
                    $logon_port,
                    $exception->getMessage(),
                    $DSLAMLOGON,
                    'XXX'
                );

                return false;
            }
        }

        echo sprintf(
            'An error occured while trying to connect/login in to DSLAM<br />'
            .'DSLAM Connection failed - %s Port: %s Error: %s Username: %s Passwort: %s<br />',
            $logon_ip,
            $logon_port,
            $exception->getMessage(),
            $DSLAMLOGON,
            'XXX'
        );
    }

    return false;
}

/**
 * Function to prettify errors from WaitUntilStringIsFoundException
 * 
 * @param \StreamSocket\Exception\WaitUntilStringIsFoundException $exception
 * 
 * @return string
 */
function prettifyWaitUntilStringIsFoundException(WaitUntilStringIsFoundException $exception)
{
    $trace = $exception->getTrace();
            
    return sprintf('<br /><span class="text red">Das Kommando wurde aufgrund eines Fehlers abgebrochen!<br />'
        .'Fehler:<br />%s<br />Code-Zeile: %s</span><br /><br />',
        $exception->getMessage(),
        $trace[0]['line']
    );
}

/**
 * Iskratel DSLAM open
 * 
 * @param string $logon_ip
 * @param integer $logon_port
 * @param string $DSLAMLOGON
 * @param string $DSLAMPW
 * 
 * @return \StreamSocket\Lib\StreamSocket
 */
function DSLAM_open_new ($usenet,$logon_ip, $logon_port, $DSLAMLOGON, $DSLAMPW) {
    try {
        // Create new tcp stream socket to DSLAM
        $usenet = StreamSocket::newInstance()->connect(
            StreamSocket::TCP, // connection type
            $logon_ip,  // connection ip-address
            $logon_port, // connection port
            array('sec' => 10, 'usec' => 0) // connection timeout
        );

        $usenet
            // wait for login promt
            ->waitUntilStringIsFound('login:', null, 'Did not receive login-prompt', false)
            // write username
            ->write($DSLAMLOGON."\r\n")
            // wait for password promt
            ->waitUntilStringIsFound('password:', null, 'Did not receive password-prompt', false)
            // write password
            ->write($DSLAMPW."\r\n")
            // wait for command promt or 'Login incorrect'-message
            ->waitUntilStringIsFound('last login', null, null, false, ['incorrect' => 'Login incorrect'])
        ;

        return $usenet; // return the StreamSocket instance
    } catch (Exception $exception) {
        if ($exception instanceof WaitUntilStringIsFoundException) {
            // A 'WaitUntilStringIsFoundException'-exception was thrown.
            // See if it happend while waiting for login confirmation.
            if ('Login incorrect' == $exception->getMessage()) {
                echo sprintf('DSLAM Connection failed - %s Port: %s Error: %s Username: %s Passwort: %s<br />',
                    $logon_ip,
                    $logon_port,
                    $exception->getMessage(),
                    $DSLAMLOGON,
                    'XXX'
                );

                return false;
            }
        }

        echo sprintf(
            'An error occured while trying to connect/login in to DSLAM<br />'
            .'DSLAM Connection failed - %s Port: %s Error: %s Username: %s Passwort: %s<br />',
            $logon_ip,
            $logon_port,
            $exception->getMessage(),
            $DSLAMLOGON,
            'XXX'
        );
    }

    return false;
}


function DSLAM_Bridge ($usenet,$line_identifier,$vlan_id,$vlan_pppoe_local,$vlan_pppoe,$vlan_acs_local,$vlan_acs,$vlan_iptv_local,$vlan_iptv,$tag,$tv_service,$atm, $acs_qos_profile) {

      $usenet->write( "configure bridge no port $line_identifier$atm \r\n");
      $usenet->write( "configure bridge port $line_identifier$atm \r\n");
      $usenet->write( "max-unicast-mac 16 \r\n");
    #  $usenet->write( "no pvid\r\n");
    #  $usenet->write( "no vlan-id $vlan_id \r\n");
    if ($vlan_pppoe_local == '') {
        $vlan_pppoe_local = $vlan_id;
    }

    $usenet->write( "vlan-id $vlan_pppoe_local \r\n");
    $usenet->write( "vlan-scope local \r\n");
    $usenet->write("network-vlan $vlan_id \r\n"); #bei ZEAG ab R.5.4 l2fwder-vlan

    if ($tag == true ) {
        $usenet->write("tag single-tagged\r\n");
        if ('WiSoTEL' === \Wisotel\Configuration\Configuration::get('companyName')) {
            $usenet->write("exit \r\n");
            $usenet->write("vlan-id 805 \r\n");
            $usenet->write("tag single-tagged \r\n");
            $usenet->write( "vlan-scope local \r\n");
            $usenet->write("network-vlan 805 \r\n");
        }

    } else {
        $usenet->write("tag untagged\r\n");
        $usenet->write("configure bridge port $line_identifier$atm pvid $vlan_pppoe_local \r\n");
    }
      
    if ( $vlan_pppoe_local != $vlan_id) {
        $usenet->write("configure bridge port $line_identifier$atm vlan-id $vlan_pppoe_local \r\n");
        $usenet->write( "vlan-scope local \r\n");
        $usenet->write( "network-vlan $vlan_id \r\n");
    }
      $usenet->write( "exit \r\n");
        
    if ( $vlan_acs_local != '') {
        $usenet->write( "configure bridge port $line_identifier$atm vlan-id $vlan_acs_local \r\n");
        $usenet->write( "vlan-scope local \r\n");
        $usenet->write( "network-vlan $vlan_acs \r\n");
        $usenet->write("tag single-tagged\r\n");
        $usenet->write("qos-profile name:$acs_qos_profile\r\n");
        $usenet->write( "exit \r\n");
    }
      
    if ($tv_service == "yes" ){
        if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName')) {
            if (!empty($vlan_iptv_local)) {
                  $usenet->write("configure bridge port $line_identifier$atm vlan-id $vlan_iptv_local\r\n");
                if ($vlan_iptv != $vlan_iptv_local) {
                    $usenet->write( "vlan-scope local \r\n");
                    $usenet->write( "network-vlan $vlan_iptv \r\n");
                }
                  $usenet->write("tag single-tagged\r\n");
                  $usenet->write("configure igmp channel vlan:$line_identifier$atm:$vlan_iptv_local \r\n");
                  $usenet->write("max-num-group 10 mcast-vlan-id $vlan_iptv_local \r\n");
                  $usenet->write( "exit \r\n");
                } else {
                    $usenet->write("configure bridge port $line_identifier$atm no vlan-id $vlan_iptv_local\r\n");
                }
            } else {
                echo "vlan_iptv_local nicht gesetzt.<br />\n";
            }
        }
}
##############################
# DSLAM Bridge Port
##############################
function DSLAM_Bridge_show($usenet,$line_identifier,$vlan_id,$vlan_pppoe_local,$vlan_pppoe,$vlan_acs_local,$vlan_acs,$vlan_iptv_local,$vlan_iptv,$tag,$tv_service,$atm, $acs_qos_profile) {
      echo  "configure bridge no port $line_identifier <br>";
      echo  "configure bridge port $line_identifier <br>";
      echo  "max-unicast-mac 14 <br>";
    #  echo  "no pvid<br>";
    #  echo  "no vlan-id $vlan_id <br>";
      echo  "vlan-id $vlan_pppoe_local <br>";
    if ($tag == true ) {
        echo " &nbsp;&nbsp;&nbsp;&nbsp;tag single-tagged<br>";
    } else {
        echo " &nbsp;&nbsp;&nbsp;&nbsp;tag untagged<br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp;configure bridge port $line_identifier pvid $vlan_pppoe_local <br>";
    }
      
    if ( $vlan_pppoe_local != $vlan_id) {
        echo " &nbsp;&nbsp;&nbsp;&nbsp;configure bridge port $line_identifier vlan-id $vlan_pppoe_local <br>";
        echo  "vlan-scope local <br>";
        echo  "network-vlan $vlan_id <br>";
    }
      echo  "exit <br>";
        
    if ( $vlan_acs_local != '') {
        echo  "configure bridge port $line_identifier vlan-id $vlan_acs_local <br>";
        echo  "vlan-scope local <br>";
        echo  "network-vlan $vlan_acs <br>";
        echo " &nbsp;&nbsp;&nbsp;&nbsp;tag single-tagged<br>";
        echo " qos-profile name:$acs_qos_profile<br>";
        echo  "exit <br>";
    }
      
    if ($tv_service == "yes" ){
          echo " &nbsp;&nbsp;&nbsp;&nbsp;configure bridge port $line_identifier vlan-id $vlan_iptv_local<br>";
        if ($vlan_iptv != $vlan_iptv_local) {
            echo  "vlan-scope local <br>";
            echo  "network-vlan $vlan_iptv <br>";
        }
          echo " &nbsp;&nbsp;&nbsp;&nbsp;tag single-tagged<br>";
          echo " &nbsp;&nbsp;&nbsp;&nbsp;configure igmp channel vlan:$line_identifier:$vlan_iptv_local <br>";
          echo " &nbsp;&nbsp;&nbsp;&nbsp;max-num-group 10 mcast-vlan-id $vlan_iptv_local <br>";
          echo  "exit <br>";
        } else {
            echo " &nbsp;&nbsp;&nbsp;&nbsp;configure bridge port $line_identifier no vlan-id $vlan_iptv_local<br>";
        }

}

############################################
# VLAN set UP
############################################
function vlan_setup ($usenet,$vlan_id,$vlan_name) {
                
                    $usenet->write( " configure vlan id $vlan_id mode residential-bridge \r\n");
                    $usenet->write( "name $vlan_name\r\n");
                    $usenet->write( "no new-secure-fwd \r\n");
                    $usenet->write( "circuit-id-dhcp physical-id\r\n");
                    $usenet->write( "remote-id-dhcp customer-id\r\n");
                    $usenet->write( "dhcp-opt-82 \r\n");
                    $usenet->write( "pppoe-linerate addactuallinerate\r\n");
                    $usenet->write( "circuit-id-pppoe physical-id\r\n");
                    $usenet->write( "remote-id-pppoe customer-id\r\n");
                    $usenet->write( "pppoe-linerate addactuallinerate\r\n");
                    $usenet->write( "pppoe-relay-tag configurable\r\n");
                    $usenet->write( "dhcp-linerate addactuallinerate\r\n");
                    $usenet->write( "in-qos-prof-name name:2Q_HSI-VOIP\r\n"); #GPON braucht das
                    $usenet->write( "info \r\n");
                    $usenet->write( "exit\r\n");



} # end VLAN setup

############################################
# VLAN delete
############################################
function vlan_delete ($usenet,$vlan_id,$vlan_name) {
                
                    $usenet->write( " configure vlan no id $vlan_id  \r\n"); 
                    $usenet->write( "exit\r\n"); 



} # end VLAN delete

function default_dpbo ($usenet,$Nr) {
                    
        $usenet->write( "configure xdsl dpbo-profile $Nr name DPBO$Nr"."_Defaul80_MUS_1120\r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr es-elect-length 800\r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr es-cable-model-a 14 es-cable-model-b 234 \r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr es-cable-model-c 8 \r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr min-usable-signal -1120 \r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr min-frequency 256 max-frequency 2208 rs-elect-length 665\r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 1 psd 900\r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 2 frequency 93 psd 900\r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 3 frequency 209 psd 620\r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 4 frequency 254 psd 485\r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 5 frequency 254 psd 365\r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 6 frequency 1104 psd 365\r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 7 frequency 1622 psd 465\r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 8 frequency 2208 psd 478\r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 9 frequency 2500 psd 594\r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 10 frequency 3002 psd 800\r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 11 frequency 3132 psd 950\r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 12 frequency 30000 psd 950\r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr epsd-pt-down 13 no frequency no psd\r\n");
        $usenet->write( "configure xdsl dpbo-profile $Nr active\r\n");

}

############################################
# die Main DSLAM Funktion
############################################
function DSLAM_exec ($Dslam_comando,$location_id,$client_id,$Dslam_comando1 = '',$Dslam_comando2 = '',$Dslam_comando3 = '',$Dslam_comando4 = '',$dslam_backup = 'Y') {
    $cfgTimeOut = 10; // for old code connections

    $db=$GLOBALS['db'];
    $sql=$GLOBALS['sql'];
    $customer_id = $client_id;
    $location_id1 = $location_id;
    $lsa_id1 = '';
    $rtxProfile = '';

    if ($customer_id != '') {
     #########################################################
     # serch for customer and lsa and other things from client
     # Kunden ID wurde ?bergeben
     ##########################################################
        $client_array = array();
        $db_customer = $db->prepare($sql['customer']);
        $db_customer->bind_param('d', $customer_id);
        $db_customer->execute();
        $db_customer_result = $db_customer->get_result();
        $customer = $db_customer_result->fetch_assoc();
        $db_customer_result->close();
        $location_id1         = $location_id;
        $lsa_id                = $customer["lsa_kvz_partition_id"];
        $location_id         = $customer["location_id"];
        $line_identifier     = $customer["line_identifier"];
        $card_id            = $customer["card_id"];
        $clientid            = $customer["clientid"];
        $version            = $customer["version"];
        if ($version != "") $clientid = $clientid."_".$version;
        $vlan_id            = $customer["vlan_ID"];
        $vlan_ID_u            = $customer["reached_downstream"];
        $tag                  = $customer["tag"];
        $ip_address         = $customer["ip_address"];
        $terminal_type        = $customer["terminal_type"];
        $dslam_port            = $customer["dslam_port"];
        $tv_service            = $customer["tv_service"];
        $DPBO                 = $customer["DPBO"];
        $Service            = $customer["Service"];
        $Spectrumprofile    = $customer["Spectrumprofile"];
        $firmware_version    = $customer["firmware_version"];
        $dtag_line            = $customer["dtag_line"];
        $vectoring            = $customer["vectoring"];
        $pppoePassword      = $customer["password"];

        $purtelAccountQuery = $db->prepare($sql['purtel_account']);
        $purtelAccountQuery->bind_param('d', $customer_id);
        $purtelAccountQuery->execute();
        $purtelAccountResult = $purtelAccountQuery->get_result();
        $purtelMasterAccount = $purtelAccountResult->fetch_assoc();
        $purtelAccountResult->close();

        $db_location = $db->prepare($sql['locations']);
        $db_location->bind_param('d', $location_id);
        $db_location->execute();
        $db_location_result = $db_location->get_result();
        $location = $db_location_result->fetch_assoc();
        $db_location_result->close();

        $DSLAMTYP = $location["dslam_type"];
        
        if ($vlan_id == '') $vlan_id = $vlan_ID_u; # old vlan was used
        if($Dslam_comando == 'Test1') {
            echo " &nbsp;&nbsp;&nbsp;&nbsp;<hr>";
            print_r ($customer);
            echo " &nbsp;&nbsp;&nbsp;&nbsp;<hr>";
        }
            ####################################################
            # weiter mit lsa_kvz_partition
            ####################################################
        
        $db_location = $db->prepare($sql['lsa_kvz_partition']);
        $db_location->bind_param('d', $lsa_id);
        $db_location->execute();
        $db_location_result = $db_location->get_result();
        $row = $db_location_result->fetch_assoc();
        $db_location_result->close();
           $Default_VLAN1 = $row["default_VLAN"];
           $Default_DPBO1 = $row["dpbo"];
           $esel_wert1      = $row["esel_wert"];
           $kvz_name1 =    $row["kvz_name"];
        if ($DPBO != $Default_DPBO1) {$Default_DPBO1 = $DPBO; $esel_wert1 = '';$kvz_name1 = 'Falsch';}
        $kvz_name1 = str_replace(" ", "_", $kvz_name1);

        $db_location = $db->prepare($sql['card']);
        $db_location->bind_param('d', $card_id);
        $db_location->execute();
        $db_location_result = $db_location->get_result();
        $row = $db_location_result->fetch_assoc();
        $db_location_result->close();
        $cardtype = $row["cardtype"];
#        echo " &nbsp;&nbsp;&nbsp;&nbsp;<hr>";
#        print_r ($row);
#         echo " &nbsp;&nbsp;&nbsp;&nbsp;<hr>";
        $produkte = array ();
        $db_location = $db->prepare($sql['produkte']);
        $db_location->bind_param('s', $Service);
        $db_location->execute();
        $db_location_result = $db_location->get_result();
        $row = $db_location_result->fetch_assoc();
        $produkt_index         = $row["produkt_index"];
        $gpon_download         = $row["gpon_download"];
        $gpon_upload           = $row["gpon_upload"];
        $ethernet_up_down     = $row["ethernet_up_down"];
        $rtxProfile = $row["rtx_profile"];
        $db_location_result->close();

        #firmware_version dtag_line
        $dtag_line = substr($dtag_line,-8);
        
        if ($firmware_version == '') {
            $firmware_version = $location['default_ont_firmware_release'];
        }
        
        if (!empty($firmware_version)) {
            $firmware_version = '3'.substr($firmware_version,-13); #compatible mit WiSoTEL alten Daten
        }


        echo " &nbsp;&nbsp;&nbsp;&nbsp;<hr>";
#        print_r ($row);
#         echo " &nbsp;&nbsp;&nbsp;&nbsp;<hr>";
#        echo " &nbsp;&nbsp;&nbsp;&nbsp;<hr>";
#        print_r ($produkte);
#         echo " &nbsp;&nbsp;&nbsp;&nbsp;<hr>";

    } else {
        
            ####################################################
            # weiter mit Location Info
            ####################################################
            
        $location_array = array();
        $db_location = $db->prepare($sql['lsa_kvz_partitions']);
        $db_location->bind_param('d', $location_id1);
        $db_location->execute();
        $db_location_result = $db_location->get_result();
     
        while ($location1 = $db_location_result->fetch_assoc()){
            
            array_push ($location_array,$location1);
        }
        
        $db_location_result->close();
        $i="1";
        foreach ($location_array as $row) {
            
           ${"Default_VLAN".$i} = $row["default_VLAN"];
           ${"Default_DPBO".$i} = $row["dpbo"];
           ${"esel_wert".$i}      = $row["esel_wert"];
           ${"kvz_name".$i} =    $row["kvz_name"];
           $i++;
        }
        $kvz_name1 = str_replace(" ", "_", $kvz_name1);
        $kvz_name2 = str_replace(" ", "_", $kvz_name2);
        $kvz_name3 = str_replace(" ", "_", $kvz_name3);
        
    }


    $db_location = $db->prepare($sql['locations']);
    $db_location->bind_param('d', $location_id);
    $db_location->execute();
    $db_location_result = $db_location->get_result();
    $location = $db_location_result->fetch_assoc();
    $db_location_result->close();
 #  print_r ($location);
    

/*    echo " &nbsp;&nbsp;&nbsp;&nbsp;<hr>";
    print_r ($location);
    echo " &nbsp;&nbsp;&nbsp;&nbsp;<hr>";
    print_r ($location_array);
    echo " &nbsp;&nbsp;&nbsp;&nbsp;<hr>";
*/    $row = $location;
    $dslam_name         = $row["name"];

    $dslamNameParted = null;

    if (1 === preg_match('/^((([^_]+)_.*)_R\d{2})\/.*/', $dslam_name, $dslamNameParted)) {
        $dslam_name = $dslamNameParted[2];
        $dslam_name_ext = $dslamNameParted[1];
        $vlan_name1 = $dslamNameParted[3];
    } else {
        $dslam_name_ext     = substr($dslam_name,0,14);
        $dslam_name_ext     = preg_replace('/[^0-9a-z_]/i', '_', $dslam_name_ext);
        $dslam_name         = substr($dslam_name,0,10);
        $vlan_name1         = substr($dslam_name,0,3);
    }

    $logon_ip           = $row["dslam_ip_adress"];
    $dslam_ip_real      = $row["dslam_ip_real"];
    $dslam_router       = $row["dslam_router"];
    $dslam_netz         = $row["netz"];
    $logon_port         = $row["logon_port"];
    $DSLAMLOGON         = $row["logon_id"];
    $DSLAMPW            = $row["logon_pw"];
    $Backup             = $dslam_name_ext.".tar";
    $vlan_iptv          = $row["vlan_iptv"];
    $vlan_acs           = $row["vlan_acs"];
    $vlan_dcn           = $row["vlan_dcn"];
    $vlan_pppoe         = $row["vlan_pppoe"];

    if ($vlan_pppoe == '' && isset($vlan_id) && $vlan_id == '') {
        $vlan_pppoe = '200';
    } elseif ($vlan_pppoe == '' && isset($vlan_id) && $vlan_id != '') {
        $vlan_pppoe = $vlan_id;
    }

    $Vector_location     = $row["vectoring"];
    $Dslam_conf            = $row["dslam_conf"];
    $DSLAMTYP            = $row["dslam_type"];
    $vlan_iptv_local    = $row["vlan_iptv_local"];
    $vlan_pppoe_local    = $row["vlan_pppoe_local"];

    if ($vlan_pppoe_local == '' && isset($vlan_id) && $vlan_id == '') {
        $vlan_pppoe_local = '200';
    } elseif ($vlan_pppoe_local == '' && isset($vlan_id) && $vlan_id != '') {
        $vlan_pppoe_local = $vlan_id;
    }

    $vlan_acs_local        = $row["vlan_acs_local"];
    $prozessor            = $row["prozessor"];
    $acs_qos_profile     = $row["acs_qos_profile"];
    $Vectoring             = "vect-profile 1";
    $NoVectoring = "no vect-profile";
    if ($Vector_location == "1" and $vectoring == "Nein") {  $Vectoring = "no vect-profile"; }
    if ($Vector_location == false ) {  $Vectoring = ""; $NoVectoring = "";}
    $Transfermode     = "ptm";
    $atm            ="";
    #echo "spectrum = $Spectrumprofile";
    if ($Spectrumprofile == "2") {
        $Transfermode     = "atm";
        $atm = ":1:32";
    }

    if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName')) {
        if ($location_id == "43" and $tag == false and $client_id != '' ) {  # Südbahnhof F-Box Brand alt 
            $vlan_iptv_local = '630';
            $vlan_pppoe_local = $vlan_pppoe;
        }
    }
    
if($Dslam_comando == 'Test') {
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    DSLAM_NAME         =     $dslam_name    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    logon_ip          =     $logon_ip     <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    logon_port          =     $logon_port     <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    DSLAMLOGON          =     $DSLAMLOGON     <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    DSLAMPW          =     *******     <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    Backup              =     $Backup    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    Vlan IPTV         =     $vlan_iptv    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    vlan_ACS         =     $vlan_acs    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    vlan_pppoe         =     $vlan_pppoe    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    Vlan IPTV local    =     $vlan_iptv_local    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    vlan_ACS local    =     $vlan_acs_local    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    vlan_pppoe_local=     $vlan_pppoe_local    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    Vector_location    =     $Vector_location    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    Dslam_conf         =     $Dslam_conf    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    DSLAMTYP         =     $DSLAMTYP    <br><hr>";

    if ($kvz_name1 != '') echo " &nbsp;&nbsp;&nbsp;&nbsp;KVZ = $kvz_name1 DPBO = $Default_DPBO1 ESEL = $esel_wert1 <br>";
    if ($kvz_name2 != '')echo " &nbsp;&nbsp;&nbsp;&nbsp;KVZ = $kvz_name2 DPBO = $Default_DPBO2 ESEL = $esel_wert2 <br>";
    if ($kvz_name3 != '')echo " &nbsp;&nbsp;&nbsp;&nbsp;KVZ = $kvz_name3 DPBO = $Default_DPBO3 ESEL = $esel_wert3 <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;Cardtyp = $cardtype<br>";
    echo " <hr>&nbsp;&nbsp;&nbsp;&nbsp;    Kunde         = $clientid    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    lsa_id         = $lsa_id    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    location_id          = $location_id     <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    line_identifier          = $line_identifier     <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    card_id         = $card_id    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    vlan_ID         = $vlan_id    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;  VLAN_ID-old  = $vlan_ID_u<br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    tag          = $tag     <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    ip_address         = $ip_address    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    dslam_port         = $dslam_port    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    tv_service         = $tv_service    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    DPBO         = $DPBO    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    Service         = $Service    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    Spectrumprofile         = $Spectrumprofile    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    vectoring            = $vectoring    <br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    produkt_index         = $produkt_index<br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    gpon_download         = $gpon_download<br>"; 
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    gpon_upload           = $gpon_upload<br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    ethernet_up_down     = $ethernet_up_down<br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    firmware_version     = $firmware_version<br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp;    dtag_line             = $dtag_line<br>";
    echo " &nbsp;&nbsp;&nbsp;&nbsp; IPTV local VLAN = $vlan_iptv_local";
    echo " &nbsp;&nbsp;&nbsp;&nbsp; Prozessor = $prozessor";
    #firmware_version dtag_line
}

    $dpboProfile = ' dpbo-profile '.$DPBO.' ';

    if (true !== \Wisotel\Configuration\Configuration::get('useDpboProfile')) {
        $dpboProfile = '';
    }

    if (!empty($rtxProfile)) {
        $rtxProfile = ' rtx-profile '.$rtxProfile.' ';

        if (true !== \Wisotel\Configuration\Configuration::get('useRtxProfile')) {
            $rtxProfile = '';
        }
    }

    $dns1 = \Wisotel\Configuration\Configuration::get('dnsServer');
    $dns1 = $dns1['1'];
    $tftpserver = \Wisotel\Configuration\Configuration::get('tftpServer');
    $admin_save = "admin save";

    #echo " &nbsp;&nbsp;&nbsp;&nbsp; Prozessor = $prozessor";

    switch ($prozessor) {
        case 'NRNT-A':
        case 'NANT-A':
            $admin_save = "admin software-mngt shub database save";
        break;
        case 'RANT-A':        
            $admin_save = "";
        break;
    }
    
    $Backup_id    = "admin software-mngt database upload actual-active:".$tftpserver.":dm_complete_$dslam_name_ext".".tar";
    $Backup_restore = "admin software-mngt database download ".$tftpserver.":dm_complete_$dslam_name_ext".".tar";
    $DSLAM_Status     = true;
    $detail ="";
    if ($Dslam_comando == 'Test') echo " &nbsp;&nbsp;&nbsp;&nbsp;logonm = $logon_ip ACS = $vlan_acs IP = $vlan_pppoe IPTV = $vlan_iptv DCN = $vlan_dcn Backup = $Backup_id<br>";
    if ( $Dslam_comando == 'DSLAM_Kunden_Status_maximal') {
        $Dslam_comando = "DSLAM_Kunden_Status";
        $detail =" detail";
    }
    if ( $Dslam_comando == 'DSLAM_Kunden_Config') {
        $Dslam_comando = "DSLAM_Kunden_Status";
        $DSLAM_Status     = false;
    }
        $exit_line = 'sonicht';
        $exit_in_up = 'sonicht';
        $down_red = 'sonicht';
        if ($Dslam_comando == 'DSLAM_Kunden_Status') {
            $exit_line = 'line count';
            $down_red = ' down '; 
        }

        if ($DSLAM_Status == true) {
            $exit_in_up = 'inp-up';
            $down_red = ' down '; 
        }


    if (!$Dslam_conf or (($vlan_id == "" or $DPBO == "" or $Service == "" or $Spectrumprofile ==  "" or $line_identifier == "" or $clientid == "") and $location_id == '')) {
        if ($Dslam_conf) {
            echo " &nbsp;&nbsp;&nbsp;&nbsp; <br> es fehlen Kunden Daten<br> ";
            echo " &nbsp;&nbsp;&nbsp;&nbsp;    line_identifier          = $line_identifier     <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp;    card_id         = $card_id    <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp;    vlan_ID         = $vlan_ID    <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp;  VLAN_ID-old  = $vlan_ID_u<br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp;    ip_address         = $ip_address    <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp;    dslam_port         = $dslam_port    <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp;    DPBO         = $DPBO    <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp;    Service         = $Service    <br>";
            echo " &nbsp;&nbsp;&nbsp;&nbsp;    Spectrumprofile         = $Spectrumprofile    <hr>";
        }
    } else {
        ##############################################################################
        # Commandos ohne logon
        ##############################################################################

        switch ($DSLAMTYP) {
            case 'ALU':
        

            switch ($Dslam_comando) {
                
                case 'reboot1':
                    $Dslam_comando = '';
                    ?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a class="fancy-button outline round  red large18" href="<?php echo "index.php?kommando=reboot&location_id=$location_id"  ?>"> &nbsp;reboot <?php echo "$dslam_name" ?>  Bestaetigen&nbsp;</a><br /><br />
                    <?php
                break;

                case 'reboot2':
                    $Dslam_comando = '';
                    ?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a class="fancy-button outline round  red large18" href="<?php echo "index.php?kommando=reboot3&location_id=$location_id"  ?>"> &nbsp;reboot <?php echo "$dslam_name" ?>  Bestaetigen&nbsp;</a><br /><br />
                    <?php
                break;

                
                case 'DSLAM_Kunden_Loeschen':
                    $Dslam_comando = '';
                    ?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a class="fancy-button outline round  red large18" href="<?php echo " index.php?kommando=DSLAM_Kunden_Loeschen1&id=$customer_id"  ?>"> &nbsp;Kunde Loeschen <?php echo "$customer_id im DSLAM $dslam_name" ?>  Bestaetigen&nbsp;</a><br /><br />
                    <?php
                break;
            }
        ##############################################################################
        $usenet = false;
        if ($Dslam_comando != 'Experte' and $Dslam_comando != '' ) {
            $usenet = DSLAM_open_new ($usenet,$logon_ip, $logon_port,$DSLAMLOGON,$DSLAMPW);
        } else {
            $usenet = true;
        }
        if($usenet != false and $Dslam_comando != '') {
            
            switch ($Dslam_comando) {
                

                case 'reboot':
                    echo "reboot gestartet";#admin equipment reboot-isam without-self-test 
                    $usenet->write(" admin equipment reboot-isam without-self-test\r\n");

                break;
                case 'reboot3':
                    echo "reboot gestartet";#admin equipment reboot-isam without-self-test 
                    $usenet->write(" admin equipment reboot-isam with-self-test\r\n");

                break;

                case 'Test':
                echo 'Test';
                break;
                case 'Testx':
                echo 'Testx - location-id: '.$location_id.'<br />';
                break;
                case 'EQ_Slot':
                case 'EQ_Slots':
                case 'DSLAM_Equipmentslot':
                            
                    $down_red = 'sonicht';
                    $usenet->write("show equipment slot\r\n");
                    
                    if ($Dslam_comando != 'EQ_Slots') $usenet->write( " info configure equipment \r\n");
                    
                #    $usenet->write("info configure xdsl board flat\r\n");
                #    $usenet->write("show xdsl vp-board\r\n");
                    

                    
                    
                    
                break;
                # Software Management
                
                case 'Software':
                    echo "Dslam_comando1 = $Dslam_comando1 / Dslam_comando2 = $Dslam_comando2 / Dslam_comando3 = $Dslam_comando3 / Dslam_comando4 = $Dslam_comando4 / Dslam_comando5 = $Dslam_comando5";
                    switch ($Dslam_comando1) {
                        case 'show software-mngt oswp':
                            $usenet->write("$Dslam_comando1 \r\n");
                        break;
                        
                        case 'show software-mngt swp-disk-file':
                            $usenet->write("$Dslam_comando1 \r\n");
                        break;
                        
                        case 'show equipment ont sw-version':
                            $usenet->write("$Dslam_comando1 \r\n");
                            $usenet->write("show pon sfp-inventory \r\n");
                            $usenet->write("show pon optics \r\n");
                            $usenet->write("info configure pon flat\r\n"); 
                        break;
                        case 'admin software-mngt oswp 1 download L6GPAA58.474':
                        case 'admin software-mngt oswp 2 download L6GPAA58.474':
                        case 'admin software-mngt oswp 1 download L6GPAA56.452':
                        case 'admin software-mngt oswp 2 download L6GPAA56.452':
                        case 'admin software-mngt oswp 1 download L6GPAA54.543':
                        case 'admin software-mngt oswp 2 download L6GPAA54.543':
                            $usenet->write("$Dslam_comando1 \r\n");
                            $usenet->write("show software-mngt oswp \r\n");
                        break;
                        case 'admin software-mngt database download 195.178.0.114':
                            $usenet->write("$Dslam_comando1:$Dslam_comando3 \r\n");
                            $usenet->write("show software-mngt upload-download \r\n");
                        break;
                        case 'admin software-mngt oswp':
                            $usenet->write("$Dslam_comando1 $Dslam_comando2 $Dslam_comando4 \r\n");
                        break;
                    break;
                    }
                break;
                
                case 'IP_setzen': # setzen der richtigen DSLAM IP nach erstbetrieb
                    $newIP = substr($dslam_ip_real,0,9).$Dslam_comando1;
                    echo "das waere die IP nachdem der Service von ZEAG nach WiSoTEL geaendert worden ist !! IP = $newIP";
                    if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName')) {
                        switch ($vlan_dcn) {
                            case '530':
                                $iesInterface = "interface MGNT-VLAN-BIB-530";
                            break;
                            case '531':
                                $iesInterface = "interface MGNT-VLAN-BIB-531";
                            break;
                            case '532':
                                $iesInterface = "interface MGNT-VLAN-GEM-532";
                            break;
                            case '530':
                                $iesInterface = "interface MGNT-VLAN-HN-533";
                            break;
                            case '530':
                                $iesInterface = "interface MGNT-VLAN-HN-540";
                            break;
                        }
                        $usenet->write("configure service ies 2 customer 1\r\n");
                        $usenet->write("$iesInterface\r\n");
                        $usenet->write("address $newIP/24\r\n");
                        #echo "Test new IP = $newIP";
                        $usenet->write("info\r\n");
                    }
                    if ('WiSoTEL' === \Wisotel\Configuration\Configuration::get('companyName')) {
                        $usenet->write("configure service ies 1 customer 1\r\n");
                        $usenet->write("interface WiSoTEL_DCN\r\n");
                        $usenet->write("address $newIP/21\r\n");
                        #echo "Test new IP = $newIP";
                        $usenet->write("info\r\n");
                    }
                    if ('Stadtwerke Bühl' === \Wisotel\Configuration\Configuration::get('companyName')) {
                        $usenet->write("configure service ies 2 customer 1\r\n");
                        $usenet->write("interface MGNT-VLAN-Lab-805\r\n");
                        $usenet->write("address $newIP/21\r\n");
                        #echo "Test new IP = $newIP";
                        $usenet->write("info\r\n");
                    }
                break;
                
                case 'IPTV':
                    $usenet->write(" info configure igmp\r\n");
                break;
                case 'IGMP':
                    $usenet->write(" show igmp channel protocol detail\r\n");
                    $usenet->write(" show igmp channel counter detail\r\n");
                break;
                
                case 'IPTV_setup':
                    $usenet->write("configure igmp system src-ip-address $dslam_ip_real\r\n");
                    $usenet->write("configure igmp system start\r\n");
                    $usenet->write("configure igmp vlan $vlan_iptv netw-igmp-version 3\r\n");
                    $usenet->write("configure igmp vlan $vlan_iptv_local netw-igmp-version 3\r\n");
                    $usenet->write("configure igmp package 1\r\n");
                    $usenet->write("name IPTV\n");
                    $usenet->write("template-version 1\r\n");
                    $usenet->write("configure igmp mcast-svc-context Default\r\n");
                    
                break;
                case 'aging_time':
                    
                    $usenet->write( " configure vlan id $vlan_acs aging-time 6000 \r\n");
                    $usenet->write( " configure vlan id $vlan_acs_local aging-time 6000 \r\n");
                    $usenet->write( " configure vlan id $vlan_iptv aging-time 6000 \r\n");
                    $usenet->write( " configure vlan id $vlan_iptv_local aging-time 6000 \r\n");
                    
                break;


                case 'LAG_setup':
                    switch ($prozessor) {
                        case 'NANT-E':
                        case 'FANT-F':
                            $usenet->write(" configure port nt-a:xfp:3 ethernet use-vlan-dot1q-etype \r\n");
                            $usenet->write(" configure port nt-a:xfp:3 no shutdown \r\n");
                            $usenet->write(" configure port nt-a:xfp:4 ethernet use-vlan-dot1q-etype \r\n");
                            $usenet->write(" configure port nt-a:xfp:4 no shutdown \r\n");
                            $usenet->write(" configure lag 1  \r\n");
                            $usenet->write(" port nt-a:xfp:3  \r\n");
                            $usenet->write(" port nt-a:xfp:4  \r\n");
                            $usenet->write(" local_nt_ports_only \r\n");
                            $usenet->write(" no shutdown \r\n");
                            if ($vlan_pppoe != '')     $usenet->write(" configure service vpls $vlan_pppoe sap lag-1:$vlan_pppoe create\r\n");
                            if ($vlan_acs != '')    $usenet->write(" configure service vpls $vlan_acs sap lag-1:$vlan_acs create\r\n");
                            if ($vlan_dcn != '')    $usenet->write(" configure service vpls $vlan_dcn sap lag-1:$vlan_dcn create\r\n");
                            if ($vlan_iptv != '') {
                                $usenet->write(" configure service vpls $vlan_iptv sap lag-1:$vlan_iptv create\r\n");
                                $usenet->write(" configure service vpls $vlan_iptv sap lag-1:$vlan_iptv igmp-snooping\r\n");
                                $usenet->write(" configure service vpls $vlan_iptv sap lag-1:$vlan_iptv igmp-snooping mrouter-port\r\n");
                            }
                        break;
                        case 'NRNT-A':
                            #echo " LAG nicht implementiert fuer $prozessor";
                            $usenet->write(" configure interface shub port nt:sfp:3 port-type network admin-status up \r\n");
                            $usenet->write(" configure la aggregator-port nt:sfp:3 name lag1 selection-policy mac-src-dst actor-key 1234 active-lacp short-timeout aggregatable lacp-mode disable-lacp \r\n");
                            $usenet->write(" configure interface shub port nt:sfp:4 port-type network admin-status up \r\n");
                            $usenet->write(" configure la aggregator-port nt:sfp:4 name lag1 selection-policy mac-src-dst actor-key 1234 active-lacp short-timeout aggregatable lacp-mode disable-lacp \r\n");

                        break;
                        case 'RANT-A':
                            #echo " LAG nicht implementiert fuer $prozessor";
                            $usenet->write(" configure link-agg group nt-a:xfp:1 \r\n"); 
                            $usenet->write(" load-sharing-policy ip-src-dst mode static \r\n"); 
                            $usenet->write(" port nt-a:xfp:1 \r\n");
                            $usenet->write(" exit \r\n");
                            $usenet->write(" port nt-b:xfp:1 \r\n");
                            $usenet->write(" exit \r\n");
                        break;
                    }
                 break;
                 
                case 'LAG_show':
                    switch ($prozessor) {
                        case 'NANT-E':
                        case 'FANT-F':
                            $usenet->write(" show lag 1 detail\r\n");
                            
                        break;
                        case 'NRNT-A':
                            #echo " LAG nicht implementiert fuer $prozessor";
                            $usenet->write(" show la aggregator-info\r\n");
                            $usenet->write(" show la network-port-info \r\n");

                        break;
                        case 'RANT-A':
                            #echo " LAG nicht implementiert fuer $prozessor";
                            $usenet->write(" info configure link-agg group \r\n"); 
                            $usenet->write(" show link-agg member-port\r\n"); 
                        break;
                    }
                 break;


                case 'Vectoring':
                case 'DSLAM_Vector':
                    echo " &nbsp;&nbsp;&nbsp;&nbsp; Vectoring am DSLAM eingeschalten = $Vectoring <br />";
                    $usenet->write("info configure xdsl board flat\r\n");
                    $usenet->write("show xdsl vp-board\r\n");
                    $usenet->write(" exit all\r\n");
                break;

                case 'DPBO_setup':
                echo "DPBO Setup  $Dslam_comando3 default Profile werden eingerichtet<br />";
                $iii = intval ($Dslam_comando3);
                
                    $ii = 1;
                    while ($ii <= $iii ) {
                        $items = strval($ii); 
                        default_dpbo ($usenet,$items);
                        # echo "$ii input $items<br />";
                        $ii++;
                    }
            
                break;

                case 'DPBO':
                    if ($kvz_name1 != '' && $Default_DPBO1 > 0) $usenet->write( " info configure xdsl dpbo-profile $Default_DPBO1 flat \r\n");
                    if ($kvz_name2 != '' && $Default_DPBO2 > 0) $usenet->write( " info configure xdsl dpbo-profile $Default_DPBO2 flat \r\n");
                    if ($kvz_name3 != '' && $Default_DPBO3 > 0) $usenet->write( " info configure xdsl dpbo-profile $Default_DPBO3 flat \r\n");
                    if ($kvz_name4 != '' && $Default_DPBO3 > 0) $usenet->write( " info configure xdsl dpbo-profile $Default_DPBO4 flat \r\n");

                break;
                case 'DPBO_set':
                 
                    if ($esel_wert1 != '' ) {
                        $usenet->write( " configure xdsl dpbo-profile $Default_DPBO1  \r\n");
                        $usenet->write( " modification start \r\n");
                        $usenet->write( " es-elect-length  $esel_wert1 \r\n");
                        $usenet->write( "name  $kvz_name1 \r\n");
                        $usenet->write( " modification complete \r\n");
                    } else {
                        if ($kvz_name1 != '')echo " &nbsp;&nbsp;&nbsp;&nbsp; kein gueltiger ESEL Wert f?r KVZ  = $kvz_name1 <br />" ;
                    }
                    if ($esel_wert2 != '' ) {
                        $usenet->write( " configure xdsl dpbo-profile $Default_DPBO2  \r\n");
                        $usenet->write( " modification start \r\n");
                        $usenet->write( " es-elect-length  $esel_wert2 \r\n");
                        $usenet->write( "name  $kvz_name2 \r\n");
                        $usenet->write( " modification complete \r\n");
                    } else {
                        if ($kvz_name2 != '') echo " &nbsp;&nbsp;&nbsp;&nbsp; kein gueltiger ESEL Wert f?r KVZ  = $kvz_name2 <br />" ;
                    }
                    if ($esel_wert3 != '' ) {
                        $usenet->write( " configure xdsl dpbo-profile $Default_DPBO3  \r\n");
                        $usenet->write( " modification start \r\n");
                        $usenet->write( " es-elect-length  $esel_wert3 \r\n");
                        $usenet->write( "name  $kvz_name3 \r\n");
                        $usenet->write( " modification complete \r\n");
                    } else {
                        if ($kvz_name3 != '' ) echo " &nbsp;&nbsp;&nbsp;&nbsp; kein gueltiger ESEL Wert f?r KVZ  = $kvz_name3 <br />" ;
                    }
                    if ($esel_wert4 != '' ) {
                        $usenet->write( " configure xdsl dpbo-profile $Default_DPBO4  \r\n");
                        $usenet->write( " modification start \r\n");
                        $usenet->write( " es-elect-length  $esel_wert4 \r\n");
                        $usenet->write( "name  $kvz_name4 \r\n");
                        $usenet->write( " modification complete \r\n");
                    } else {
                        if ($kvz_name3 != '' ) echo " &nbsp;&nbsp;&nbsp;&nbsp; kein gueltiger ESEL Wert f?r KVZ  = $kvz_name4 <br />" ;
                    }


                    $usenet->write( " $Adminsave \r\n");
                    #$usenet->write( " $Backup_id \r\n\r\n\r\n");
                    #$usenet->write( " show software-mngt upload-download \r\n");
                break;

                    $usenet->write( " info configure xdsl dpbo-profile flat \r\n");

                break;
                case 'DPBO_setx':
                        $usenet->write( " configure xdsl dpbo-profile 1  \r\n");
                        $usenet->write( " modification start \r\n");
                        $usenet->write( " es-elect-length  $Dslam_comando1 \r\n");
                        $usenet->write( " modification complete \r\n");

                break;

                case 'Software':
                    echo " &nbsp;&nbsp;&nbsp;&nbsp; fehlt noch <br />";

                break;
                case 'DSLAM_Service':
                     $down_red = 'sonicht';
                    $usenet->write("info configure service \r\n");
                    $usenet->write(" exit all\r\n");
                     
                break;
                case 'DSLAM_VLAN':
                     
                    $usenet->write("info configure vlan \r\n");
                    $usenet->write(" exit all\r\n");
                     
                break;
                case 'ShowSoftware':
                     
                    $usenet->write("show software-mngt oswp\r\n");
                    $usenet->write("show software-mngt swp-disk-file\r\n");
                    $usenet->write(" exit all\r\n");
                    
                break;
                    
                case 'DSLAM_Diagnose_SFP':
                     
                    $usenet->write("show equipment diagnostics sfp\r\n");
                    $usenet->write(" exit all\r\n");
                    
                break;
                case 'DSLAM_show_Port':
                     
                    $usenet->write("show port \r\n");
                    $usenet->write("info configure equipment external-link-assign flat\r\n");
                    $usenet->write(" exit all\r\n");
                    
                break;
                case 'DSLAM_Diagnose_SFP':
                     
                    $usenet->write("show equipment diagnostics sfp\r\n");
                    $usenet->write(" exit all\r\n");
                    
                break;
                case 'DSLAM_Line_up':
                     
                    $usenet->write("show xdsl operational-data line | match exact:up | match exact:up | match exact::  | count lines \r\n");
                    $usenet->write(" exit all\r\n");
                    
                break;
                case 'DSLAM_Line_upsum':
                     
                    $usenet->write("show xdsl operational-data line | match exact:up | match exact:up | match exact::  | count summary \r\n");
                    $usenet->write(" exit all\r\n");
                    
                break;
                case 'DSLAM_Line_opup':
                     
                    $usenet->write("show xdsl operational-data line | match exact:up | match exact:down  | count lines \r\n");
                    $usenet->write(" exit all\r\n");
                    
                break;
                case 'DSLAM_Line':
                     
                    $usenet->write("show xdsl operational-data line | match exact:up | count lines \r\n");
                    $usenet->write(" exit all\r\n");
                    
                break;
                case 'SNTP':
                    $usenet->write( " show sntp \r\n");
                    $usenet->write( " info configure system sntp \r\n");
                break;
                case 'MAXspeed':
                    
                    $usenet->write( " configure system max-lt-link-speed  link-speed ten-gb\r\n");
                    $usenet->write( "$admin_save \r\n");
                break;

                case 'SNTPset':
                    $usenet->write( " configure system sntp server-ip-addr ".\Wisotel\Configuration\Configuration::get('sntpServer')." enable timezone-offset 60 \r\n");
                break;
                case 'DPBOshow':
                    $usenet->write( " info configure xdsl dpbo-profile  flat \r\n");

                break;
                case 'SNMP':
                    $usenet->write( "info configure system security snmp\r\n");
                break;
                case 'SNMPset':
                    switch (\Wisotel\Configuration\Configuration::get('companyName')) {
                        case 'ZEAG':
                            #$usenet->write( "info configure system security snmp\r\n");
                            $usenet->write( "configure system security snmp user zeagams authentication sha1:plain:Wienaesaithee9ja privacy aes-128:plain:eBi3paPoo4ohSh3e\r\n");
                            $usenet->write( "configure system security snmp group zeaggroup security-level privacy-and-auth context all\r\n");
                            $usenet->write( "configure system security snmp map-user-group zeagams group-name zeaggroup\r\n");
                            
                            break;

                        case 'WiSoTEL':
                            #$usenet->write( "info configure system security snmp\r\n");
                            $usenet->write( "configure system security snmp user wisoamsdes authentication sha1:plain:wisotel@2018. privacy des:plain:wisotel@2018.\r\n");
                            $usenet->write( "configure system security snmp group wisogroup security-level privacy-and-auth context all\r\n");
                            $usenet->write( "configure system security snmp map-user-group wisoamsdes group-name wisogroup\r\n");
                            
                            break;

                        case 'Stadtwerke Bühl':
                            #$usenet->write( "info configure system security snmp\r\n");
                            $usenet->write( "configure system security snmp user buehlamsdes authentication sha1:plain:buehl@2018. privacy des:plain:buehl@2018.\r\n");
                            $usenet->write( "configure system security snmp group buehlgroup security-level privacy-and-auth context all \r\n");
                            $usenet->write( "configure system security snmp map-user-group buehlamsdes group-name buehlgroup \r\n");

                            break;
                    }

                break;

                case 'DSLAM_Vectorinit':
                    $usenet->write( "configure xdsl vect-profile 1 name VECT_US_DS\r\n");
                    $usenet->write( "version 1\r\n");
                    $usenet->write( "leg-can-dn-m1\r\n");
                    $usenet->write( "band-control-up 1:6\r\n");
                    $usenet->write( "active\r\n");
                    $usenet->write( "exit\r\n");

                    $usenet->write( "configure xdsl vect-profile 11 name ZTV_VECT_US_DS_m1\r\n");
                    $usenet->write( "version 1\r\n");
                    $usenet->write( "leg-can-dn-m1\r\n");
                    $usenet->write( "legacy-cpe\r\n");
                    $usenet->write( "active\r\n");
                    $usenet->write( "exit\r\n");
                    $usenet->write( "configure xdsl vce-profile 1 name vce_default version 1 active\r\n");

                    if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName')) {
                        if ($location_id != "43" and $location_id != "21" and $location_id != "34" and $location_id != "32") {
                            $usenet->write( "configure xdsl board 1/1/9\r\n");
                            $usenet->write( "vce-profile 1\r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write("configure xdsl board 1/1/9 vplt-autodiscover enable\r\n");
                        }
                    }
                        
                    $usenet->write("configure xdsl board 1/1/1 vplt-autodiscover enable\r\n");
                    $usenet->write( "configure xdsl board 1/1/1 vce-profile 1\r\n");
                    $usenet->write("configure xdsl board 1/1/2 vplt-autodiscover enable\r\n");
                    $usenet->write( "configure xdsl board 1/1/2 vce-profile 1\r\n");
                    $usenet->write("configure xdsl board 1/1/3 vplt-autodiscover enable\r\n");
                    $usenet->write( "configure xdsl board 1/1/3 vce-profile 1\r\n");
                    $usenet->write("configure xdsl board 1/1/4 vplt-autodiscover enable\r\n");
                    $usenet->write( "configure xdsl board 1/1/4 vce-profile 1\r\n");
                    $usenet->write("configure xdsl board 1/1/5 vplt-autodiscover enable\r\n");
                    $usenet->write( "configure xdsl board 1/1/5 vce-profile 1\r\n");
                    $usenet->write("configure xdsl board 1/1/6 vplt-autodiscover enable\r\n");
                    $usenet->write( "configure xdsl board 1/1/6 vce-profile 1\r\n");
                    if ($prozessor != 'RANT-A' and $prozessor != 'NRNT-A' ) {
                        $usenet->write("configure xdsl board 1/1/7 vplt-autodiscover enable\r\n");
                        $usenet->write( "configure xdsl board 1/1/7 vce-profile 1\r\n");
                        $usenet->write("configure xdsl board 1/1/8 vplt-autodiscover enable\r\n");
                        $usenet->write( "configure xdsl board 1/1/8 vce-profile 1\r\n");
                    }

                    # jetzt alle möglichen REM's
                    $usenet->write("configure xdsl board ctrl:1/2 vce-profile 1\r\n");
                    $usenet->write("configure xdsl board ctrl:1/2 vplt-autodiscover enable\r\n");
                    $usenet->write("configure xdsl board 1/2/1 vplt-autodiscover enable\r\n");
                    $usenet->write( "configure xdsl board 1/2/2 vce-profile 1\r\n");
                    $usenet->write("configure xdsl board ctrl:1/3 vce-profile 1\r\n");
                    $usenet->write("configure xdsl board ctrl:1/3 vplt-autodiscover enable\r\n");
                    $usenet->write("configure xdsl board 1/3/1 vplt-autodiscover enable\r\n");
                    $usenet->write( "configure xdsl board 1/3/2 vce-profile 1\r\n");
                    $usenet->write("configure xdsl board ctrl:1/4 vce-profile 1\r\n");
                    $usenet->write("configure xdsl board ctrl:1/4 vplt-autodiscover enable\r\n");
                    $usenet->write("configure xdsl board 1/4/1 vplt-autodiscover enable\r\n");
                    $usenet->write( "configure xdsl board 1/4/2 vce-profile 1\r\n");
                    $usenet->write("configure xdsl board ctrl:2/1 vce-profile 1\r\n");
                    $usenet->write("configure xdsl board ctrl:2/1 vplt-autodiscover enable\r\n");
                    $usenet->write("configure xdsl board 2/1/1 vplt-autodiscover enable\r\n");
                    $usenet->write( "configure xdsl board 2/1/2 vce-profile 1\r\n");

                    $usenet->write(" exit all\r\n");
                    switch ($prozessor) {
                        case 'RANT-A':
                        $usenet->write( "configure xdsl board nt \r\n");
                        $usenet->write( "vce-capacity-mode extended\r\n");
                        $usenet->write( "vce-profile 1\r\n");
                        $usenet->write( "vplt-autodiscover enable \r\n");
                        $usenet->write(" exit all\r\n");
                     break;

                    }
                    $usenet->write("show xdsl vp-board\r\n");

                break;

                case 'DSLAM_Bandwith_setW':
                    wiso_produkte ($usenet);
                break;

                case 'DSLAM_Bandwith_set':
                    require_once __DIR__.'/products.php';
                break;

                case 'DSLAM_QOS':
                    $usenet->write( "info configure qos profiles flat\r\n");
                    $usenet->write( "info configure xdsl service-profile flat\r\n");
                break;
                case 'Vlan_set':
                    vlan_setup($usenet, $Dslam_comando1, $Dslam_comando2);
                    
                break;
                case 'Vlan_delete':
                    vlan_delete ($usenet, $Dslam_comando1, $Dslam_comando2);
                    
                break;

                case 'DSLAM_Vlan_set':
                    $vlansWritten = false;

                    try {
                        // try to get vlans from ipRanges

                        if (empty($location['network_id'])) {
                            throw new Excption('Failure: network_id in location not set');
                        }

                        $statement = $db->prepare($sql['ipRangesVlansByNetworkId']);
                        
                        if (!empty($db->error)) {
                            throw new \Exception($db->error." - ".$sql['ipRangesVlansByNetworkId']);
                        }

                        $statement->bind_param('i', $location['network_id']);

                        if (!$statement->execute()) {
                            throw new Excption(sprintf('Failure: query "ipRangesVlansByNetworkId" failed. Db-error: %s',
                                $db->error
                            ));
                        }

                        $ipRangesVlansResult = $statement->get_result();
                        $statement->close();

                        if ($ipRangesVlansResult->num_rows > 0) {
                            while ($ipRangesVlans = $ipRangesVlansResult->fetch_assoc()) {
                                $vlanName = $vlan_name1.'_IpRange_'.$ipRangesVlans['vlan_ID'];

                                vlan_setup($usenet, $ipRangesVlans['vlan_ID'], $vlanName);
                                vlan_setup($usenet, $ipRangesVlans['vlan_ID'], $vlanName);

                                $vlansWritten = true;
                            }
                        }

                        $ipRangesVlansResult->close();
                    } catch (Excption $exception) {
                        echo $exception->getMessage();

                        break; // an error occurred, stop here
                    }

                    // set vlans from location
                    
                    $vlans = [
                        'vlan_iptv',
                        'vlan_acs',
                        'vlan_dcn',
                        'vlan_pppoe',
                        'vlan_iptv_local',
                        'vlan_pppoe_local',
                        'vlan_acs_local',
                    ];

                    foreach ($vlans as $vlan) {
                        if (!empty($location[$vlan])) {
                            $vlanId = $location[$vlan];
                            $vlanName = $vlan_name1.$vlan;

                            vlan_setup($usenet, $vlanId, $vlanName);
                            vlan_setup($usenet, $vlanId, $vlanName);

                            $vlansWritten = true;
                        }
                    }

                    if ($vlansWritten) {
                        socket_write($usenet, $admin_save." \r\n");
                    } else {
                        echo "No Vlans configured!";
                    }

                    /* khl original
                    $vlan_id     = '7';
                    $vlan_name = 'ZEAG_F_BOX_Internet';
                    vlan_setup ($usenet,$vlan_id,$vlan_name);
                    vlan_setup ($usenet,$vlan_id,$vlan_name);
                    $vlan_id     = '8';
                    $vlan_name = 'ZEAG_F_BOX_iptv';
                    vlan_setup ($usenet,$vlan_id,$vlan_name);
                    vlan_setup ($usenet,$vlan_id,$vlan_name);
                    $vlan_id     = '9';
                    $vlan_name = 'ZEAG_F_BOX_acs';
                    vlan_setup ($usenet,$vlan_id,$vlan_name);
                    vlan_setup ($usenet,$vlan_id,$vlan_name);
                    $vlan_id     = "$vlan_pppoe";
                    $vlan_name = "$vlan_name1"."_pppoe_network";
                    vlan_setup ($usenet,$vlan_id,$vlan_name);
                    vlan_setup ($usenet,$vlan_id,$vlan_name);
                    $vlan_id     = "$vlan_iptv";
                    $vlan_name = "$vlan_name1"."_iptv_network";
                    vlan_setup ($usenet,$vlan_id,$vlan_name);
                    vlan_setup ($usenet,$vlan_id,$vlan_name);
                    $vlan_id     = "$vlan_acs";
                    $vlan_name = "$vlan_name1"."_acs_network";
                    vlan_setup ($usenet,$vlan_id,$vlan_name);
                    vlan_setup ($usenet,$vlan_id,$vlan_name);
                    $vlan_id     = "$vlan_dcn";
                    $vlan_name = "$vlan_name1"."_DCN";
                    vlan_setup ($usenet,$vlan_id,$vlan_name);
                    vlan_setup ($usenet,$vlan_id,$vlan_name);
                    $usenet->write( "$admin_save \r\n");
                    */
                break;
                case 'Service_modify':
                    $sfp_port = "sfp";
                    switch ($prozessor) {
                        case 'NANT-E':
                        case 'FANT-F':
                        $sfp_port = "xfp";
                    }
                    $usenet->write( "configure service vpls $Dslam_comando1 customer $Dslam_comando2 v-vpls vlan $Dslam_comando1 create\r\n");
                        $usenet->write( "stp\r\n");
                        $usenet->write( "shutdown\r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap nt-a:$sfp_port:$Dslam_comando3:$Dslam_comando1 create\r\n");
                        $usenet->write( "exit\r\n");
                        
                        $usenet->write( "sap lt:1/1/$Dslam_comando4:$Dslam_comando1 create\r\n");
                        $usenet->write( "exit\r\n");
                break;
                case 'DSLAM_Service_modify':
                    switch ($prozessor) {
                        case 'NANT-E':
                        case 'FANT-F':
                        $usenet->write( "configure service vpls $vlan_acs customer 1 v-vpls vlan $vlan_acs \r\n");
                        $usenet->write( "stp\r\n");
                        $usenet->write( "shutdown\r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap nt-a:xfp:1:$vlan_acs \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap nt-a:xfp:2:$vlan_acs \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/1:$vlan_acs \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/2:$vlan_acs \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/3:$vlan_acs \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/4:$vlan_acs \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/5:$vlan_acs \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/6:$vlan_acs \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/7:$vlan_acs \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/8:$vlan_acs \r\n");
                        $usenet->write( "exit\r\n");

                        if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName')) {
                            if ($location_id != "43" and $location_id != "12" and $location_id != "32" and $location_id != "31" and $location_id != "34" and $location_id != "59") {
                                $usenet->write( "sap lt:1/1/10:$vlan_acs \r\n");
                                $usenet->write( "exit\r\n");
                            }
                            if ($location_id != "32" and $location_id != "12"  and $location_id != "59") {
                                $usenet->write( "sap lag-1:$vlan_acs \r\n");
                                $usenet->write( "exit\r\n");
                            }
                        }

                        $usenet->write( "no shutdown\r\n");  
                            
                        $usenet->write( "exit\r\n"); 
                        $usenet->write( "vpls $vlan_pppoe customer 1 v-vpls vlan $vlan_pppoe \r\n");
                        $usenet->write( "stp\r\n");
                        $usenet->write( "shutdown\r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap nt-a:xfp:1:$vlan_pppoe \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap nt-a:xfp:2:$vlan_pppoe \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/1:$vlan_pppoe \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/2:$vlan_pppoe \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/3:$vlan_pppoe \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/4:$vlan_pppoe \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/5:$vlan_pppoe \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/6:$vlan_pppoe \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/7:$vlan_pppoe \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/8:$vlan_pppoe \r\n");
                        $usenet->write( "exit\r\n");

                        if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName')) {
                            if ($location_id != "43" and $location_id != "12" and $location_id != "32" and $location_id != "31"  and $location_id != "34" and $location_id != "59") {
                                $usenet->write( "sap lt:1/1/10:$vlan_pppoe \r\n");
                                $usenet->write( "exit\r\n");
                            }
                            if ($location_id != "32" and $location_id != "12" and $location_id != "59") {
                                $usenet->write( "sap lag-1:$vlan_pppoe \r\n");
                                $usenet->write( "exit\r\n");
                            }
                        }

                        $usenet->write( "no shutdown\r\n");  
                            
                        $usenet->write( "exit\r\n"); 
                        $usenet->write( "vpls $vlan_iptv customer 1 v-vpls vlan $vlan_iptv \r\n");
                        $usenet->write( "stp\r\n");
                        $usenet->write( "shutdown\r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "igmp-snooping\r\n");
                        $usenet->write( "no shutdown\r\n");
                        $usenet->write( "exit\r\n");
                        
                        $usenet->write( "sap nt-a:xfp:1:$vlan_iptv \r\n");
                        $usenet->write( "igmp-snooping\r\n");
                        $usenet->write( "mrouter-port\r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "exit\r\n");

                        $usenet->write( "sap nt-a:xfp:2:$vlan_iptv \r\n");
                        $usenet->write( "igmp-snooping\r\n");
                        $usenet->write( "mrouter-port\r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "exit\r\n");
                        
                        $usenet->write( "sap lt:1/1/1:$vlan_iptv \r\n");
                        $usenet->write( "igmp-snooping\r\n");
                        $usenet->write( "send-queries\r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "exit\r\n");
                        
                        $usenet->write( "sap lt:1/1/2:$vlan_iptv \r\n");
                        $usenet->write( "igmp-snooping\r\n");
                        $usenet->write( "send-queries\r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "exit\r\n");

                        $usenet->write( "sap lt:1/1/3:$vlan_iptv \r\n");
                        $usenet->write( "igmp-snooping\r\n");
                        $usenet->write( "send-queries\r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "exit\r\n");

                        $usenet->write( "sap lt:1/1/4:$vlan_iptv \r\n");
                        $usenet->write( "igmp-snooping\r\n");
                        $usenet->write( "send-queries\r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "exit\r\n");

                        $usenet->write( "sap lt:1/1/5:$vlan_iptv \r\n");
                        $usenet->write( "igmp-snooping\r\n");
                        $usenet->write( "send-queries\r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "exit\r\n");
                        
                        $usenet->write( "sap lt:1/1/6:$vlan_iptv \r\n");
                        $usenet->write( "igmp-snooping\r\n");
                        $usenet->write( "send-queries\r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "exit\r\n");
                        
                        $usenet->write( "sap lt:1/1/7:$vlan_iptv \r\n");
                        $usenet->write( "igmp-snooping\r\n");
                        $usenet->write( "send-queries\r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "exit\r\n");
                        
                        $usenet->write( "sap lt:1/1/8:$vlan_iptv \r\n");
                        $usenet->write( "igmp-snooping\r\n");
                        $usenet->write( "send-queries\r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "exit\r\n");
                        
                        if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName')) {
                            if ($location_id != "32" and $location_id != "12"  ) {
                                $usenet->write( "sap lag-1:$vlan_iptv \r\n");
                                $usenet->write( "igmp-snooping\r\n");
                                $usenet->write( "mrouter-port\r\n");
                                $usenet->write( "exit\r\n");
                                $usenet->write( "exit\r\n");
                            }
                            if ($location_id != "32" and $location_id != "12" and $location_id != "43" and $location_id != "31" and $location_id != "34"  and $location_id != "59" ) {
                                $usenet->write( "sap lt:1/1/10:$vlan_iptv \r\n");
                                $usenet->write( "igmp-snooping\r\n");
                                $usenet->write( "send-queries\r\n");
                                $usenet->write( "exit\r\n");
                                $usenet->write( "exit\r\n");
                            }
                        }

                        $usenet->write( "no shutdown\r\n");  
                            
                        $usenet->write( "exit\r\n"); 
                        $usenet->write( "$admin_save \r\n");
                     break;
                    case 'NRNT-A':
                     #echo " Service noch nicht implementiert fuer $prozessor";
                        $usenet->write("  igmp shub igs-system enable-snooping self-ip-addr-mode \r\n");
                        $usenet->write("  self-ip-addr $dslam_ip_real start-snooping \r\n");
                        $usenet->write("  igmp system start \r\n");
                        $usenet->write("  igmp package 1 name IPTV template-version 1 \r\n");
                        $usenet->write("  mcast general package-member 1 \r\n");
                     break;
                    case 'RANT-A':
                        $usenet->write( "configure bridge port nt-a:xfp:1\r\n");
                        $usenet->write( "  max-unicast-mac 2496\r\n");
                        $usenet->write( "  vlan-id $vlan_pppoe\r\n");
                        $usenet->write( "  exit\r\n");
                        $usenet->write( "  vlan-id $vlan_acs\r\n");
                        $usenet->write( "  exit\r\n");
                        $usenet->write( "  vlan-id $vlan_iptv\r\n");
                        $usenet->write( "  exit all\r\n");
                     break;

                    }
                break;

                case 'DSLAM_Service_set':
                    switch ($prozessor) {
                        case 'NANT-E':
                        case 'FANT-F':
                        $usenet->write( "configure service vpls $vlan_acs customer 1 v-vpls vlan $vlan_acs create\r\n");
                        $usenet->write( "stp\r\n");
                        $usenet->write( "shutdown\r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap nt-a:xfp:1:$vlan_acs create \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap nt-a:xfp:2:$vlan_acs create \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/1:$vlan_acs create \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/2:$vlan_acs create \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/3:$vlan_acs create \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/4:$vlan_acs create \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/5:$vlan_acs create \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/6:$vlan_acs create \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/7:$vlan_acs create \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/8:$vlan_acs create \r\n");
                        $usenet->write( "exit\r\n");

                        if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName')) {
                            if ($location_id != "32" and $location_id != "12"  and $location_id != "59") {
                                $usenet->write( "sap lag-1:$vlan_acs create \r\n");
                                $usenet->write( "exit\r\n");
                            }
                            if ($location_id != "43" and $location_id != "12" and $location_id != "32" and $location_id != "31" and $location_id != "34"  and $location_id != "59") {
                                $usenet->write( "sap lt:1/1/10:$vlan_acs create \r\n");
                                $usenet->write( "exit\r\n");
                            }
                        }
                        
                        $usenet->write( "no shutdown\r\n");  
                            
                        $usenet->write( "exit\r\n"); 
                        $usenet->write( "vpls $vlan_pppoe customer 1 v-vpls vlan $vlan_pppoe create\r\n");
                        $usenet->write( "stp\r\n");
                        $usenet->write( "shutdown\r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap nt-a:xfp:1:$vlan_pppoe create \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap nt-a:xfp:2:$vlan_pppoe create \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/1:$vlan_pppoe create \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/2:$vlan_pppoe create \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/3:$vlan_pppoe create \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/4:$vlan_pppoe create \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/5:$vlan_pppoe create \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/6:$vlan_pppoe create \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/7:$vlan_pppoe create \r\n");
                        $usenet->write( "exit\r\n");
                        $usenet->write( "sap lt:1/1/8:$vlan_pppoe create \r\n");
                        $usenet->write( "exit\r\n");

                        if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName')) {
                            if ($location_id != "32" and $location_id != "12" and $location_id != "59") {
                                $usenet->write( "sap lag-1:$vlan_pppoe create \r\n");
                                $usenet->write( "exit\r\n");
                            }
                            if ($location_id != "43" and $location_id != "12" and $location_id != "32" and $location_id != "31" and $location_id != "34"  and $location_id != "59") {
                                $usenet->write( "sap lt:1/1/10:$vlan_pppoe create \r\n");
                                $usenet->write( "exit\r\n");
                            }
                        }

                        $usenet->write( "no shutdown\r\n");  
                            
                        $usenet->write( "exit\r\n"); 
                        if ($vlan_iptv != '') {
                            $usenet->write( "vpls $vlan_iptv customer 1 v-vpls vlan $vlan_iptv create\r\n");
                            $usenet->write( "stp\r\n");
                            $usenet->write( "shutdown\r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write( "sap nt-a:xfp:1:$vlan_iptv create \r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write( "sap nt-a:xfp:2:$vlan_iptv create \r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write( "sap lt:1/1/1:$vlan_iptv create \r\n");
                            $usenet->write( "igmp-snooping\r\n");
                            $usenet->write( "send-queries\r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write( "sap lt:1/1/2:$vlan_iptv create \r\n");
                            $usenet->write( "igmp-snooping\r\n");
                            $usenet->write( "send-queries\r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write( "sap lt:1/1/3:$vlan_iptv create \r\n");
                            $usenet->write( "igmp-snooping\r\n");
                            $usenet->write( "send-queries\r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write( "sap lt:1/1/4:$vlan_iptv create \r\n");
                            $usenet->write( "igmp-snooping\r\n");
                            $usenet->write( "send-queries\r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write( "sap lt:1/1/5:$vlan_iptv create \r\n");
                            $usenet->write( "igmp-snooping\r\n");
                            $usenet->write( "send-queries\r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write( "sap lt:1/1/6:$vlan_iptv create \r\n");
                            $usenet->write( "igmp-snooping\r\n");
                            $usenet->write( "send-queries\r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write( "sap lt:1/1/7:$vlan_iptv create \r\n");
                            $usenet->write( "igmp-snooping\r\n");
                            $usenet->write( "send-queries\r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write( "sap lt:1/1/8:$vlan_iptv create \r\n");
                            $usenet->write( "igmp-snooping\r\n");
                            $usenet->write( "send-queries\r\n");
                            $usenet->write( "exit\r\n");
                            $usenet->write( "exit\r\n");

                            if ('ZEAG' === \Wisotel\Configuration\Configuration::get('companyName')) {
                                if ($location_id != "32" and $location_id != "12" and $location_id != "59") {
                                    $usenet->write( "sap lag-1:$vlan_iptv create \r\n");
                                     $usenet->write( "igmp-snooping\r\n");
                                     $usenet->write( "mrouter-port\r\n");
                                     $usenet->write( "exit\r\n");
                                    $usenet->write( "exit\r\n");
                                }
                                if ($location_id != "43" and $location_id != "12" and $location_id != "34" and $location_id != "31" and $location_id != "32"  and $location_id != "59") {
                                    $usenet->write( "sap lt:1/1/10:$vlan_iptv create \r\n");
                                    $usenet->write( "igmp-snooping\r\n");
                                    $usenet->write( "exit\r\n");
                                    $usenet->write( "exit\r\n");
                                }
                            }
                            $usenet->write( "no shutdown\r\n"); 
                        }
                            
                        $usenet->write( "exit\r\n"); 
                        $usenet->write( "$admin_save \r\n");
                     break;
                    case 'NRNT-A':
                        #echo " Service noch nicht implementiert fuer $prozessor";
                        $usenet->write("  igmp shub igs-system enable-snooping self-ip-addr-mode \r\n");
                        $usenet->write("  self-ip-addr $dslam_ip_real start-snooping \r\n");
                        $usenet->write("  igmp system start \r\n");
                        $usenet->write("  igmp package 1 name IPTV template-version 1 \r\n");
                        $usenet->write("  mcast general package-member 1 \r\n");

                     break;
                    case 'RANT-A':
                        $usenet->write( "configure bridge port nt-a:xfp:1\r\n");
                        $usenet->write( "  max-unicast-mac 2496\r\n");
                        $usenet->write( "  vlan-id $vlan_pppoe\r\n");
                        $usenet->write( "  exit\r\n");
                        $usenet->write( "  vlan-id $vlan_acs\r\n");
                        $usenet->write( "  exit\r\n");
                        $usenet->write( "  vlan-id $vlan_iptv\r\n");
                        $usenet->write( "  exit all\r\n");
                        #echo " Service noch nicht implementiert fuer $prozessor";
                     break;

                    }
                break;

                case 'Backupshow':
                    $down_red = 'so nicht';
                    $usenet->write( " show software-mngt upload-download \r\n");
                break;
                case 'DSLAM_Line_upsum':
                     
                    $usenet->write("show xdsl operational-data line | match exact:up | match exact:up | match exact::  | count summary \r\n");
                    $usenet->write(" exit all\r\n");
                    
                break;
                case 'DSLAM_sSecurity_show':
                     
                    $usenet->write("info configure system security \r\n");
                    $usenet->write(" exit all\r\n");
                    
                break;
                case 'DSLAM_sSecurity_set':
                case 'Promt':
                    switch (\Wisotel\Configuration\Configuration::get('companyName')) {
                        case 'ZEAG':
                            $login_banner ="\"#######################################\\r\\n##                                   ##\\r\\n##         <<< $dslam_name >>>        ##\\r\\n##                                   ##\\r\\n##      Authorized access only!      ##\\r\\n##                                   ##\\r\\n##            Z  E  A  G             ##\\r\\n##                                   ##\\r\\n#######################################\\r\\n#######################################\"";

                            break;

                        case 'WiSoTEL':
                            $login_banner ="\"#######################################\\r\\n##                                   ##\\r\\n##     <<< $dslam_name_ext >>>        ##\\r\\n##                                   ##\\r\\n##      Authorized access only!      ##\\r\\n##                                   ##\\r\\n##           W i S o T E L           ##\\r\\n##                                   ##\\r\\n#######################################\\r\\n#######################################\"";

                            break;

                        case 'Stadtwerke Bühl':
                            $login_banner ="\"#######################################\\r\\n##                                   ##\\r\\n##     <<< $dslam_name_ext >>>        ##\\r\\n##                                   ##\\r\\n##      Authorized access only!      ##\\r\\n##                                   ##\\r\\n##           Stadtwerke Buehl        ##\\r\\n##                                   ##\\r\\n#######################################\\r\\n#######################################\"";
                            
                            break;
                    }

                    $welcome_banner = "\"###################################\\r\\n##          $dslam_name           ##\\r\\n## MNGT IP      |$logon_ip     ##\\r\\n##-------------------------------##\\r\\n## MGMT-VLAN    |$vlan_dcn             ##\\r\\n##-------------------------------##\\r\\n## S-VLAN-PPPOE |$vlan_pppoe            ##\\r\\n##-------------------------------##\\r\\n## S-VLAN-IPTV  |$vlan_iptv            ##\\r\\n##-------------------------------##\\r\\n## S-VLAN-ACS   |$vlan_acs            ##\\r\\n###################################\"";

                    #echo " &nbsp;&nbsp;&nbsp;&nbsp;$login_banner";
                    
                    if ($dslam_name != '') {
                        $usenet->write("configure system security \r\n");
                        $usenet->write("login-banner $login_banner\r\n" );
                        $usenet->write("welcome-banner $welcome_banner\r\n" );
                        $usenet->write(" operator isadmin prompt $dslam_name%d%c \r\n");
                        $usenet->write(" exit all\r\n");
                        $usenet->write( "$admin_save \r\n");
                    }
                break;
                case 'DSLAM_extendet_slot':
                    switch ($prozessor) {
                        case 'NANT-E':
                        case 'FANT-F':
                        case 'NANT-A':
                        case 'NANT-C':
                        case 'NANT-D':                     
                            $usenet->write("configure system port-num-in-proto type-based \r\n");
                            $usenet->write("configure system security profile admin slot-numbering type-based\r\n");
                            $usenet->write("configure  equipment shelf 1/1 mode extended-lt-slots \r\n");
                            $usenet->write(" exit all\r\n");
                            $usenet->write( "$admin_save \r\n");
                         break;
                        case 'NRNT-A':
                            echo " extendet slot nicht vorhanden fuer $prozessor";
                         break;
                        case 'RANT-A':
                            $usenet->write("configure system port-num-in-proto type-based \r\n");
                            $usenet->write("configure system security profile admin slot-numbering type-based\r\n");
                            #configure system security profile admin slot-numbering type-based
                            echo " extendet slot nicht vorhanden fuer $prozessor";
                         break;

                        }
                break;
                case 'DSLAM_slot_numm':
                    $usenet->write("configure system port-num-in-proto type-based \r\n");
                    $usenet->write("configure system security profile admin slot-numbering type-based\r\n");
                    $usenet->write(" exit all\r\n");
                    $usenet->write( "$admin_save \r\n");
                    $usenet->write( "admin equipment reboot-isam without-self-test\r\n");
                
                break;
                case 'DSLAM_bridge_port':
                     
                    $usenet->write("vlan bridge-port-fdb \r\n");
                    $usenet->write( "show vlan fdb-board \r\n");
                    $usenet->write(" exit all\r\n");
                break;

                case 'DSLAM_Backup':
                case 'Backup':

                     
                    $usenet->write( "$admin_save \r\n");
                    $usenet->write("$Backup_id \r\n");
                    $usenet->write( "show software-mngt upload-download  \r\n");
                break;
                case 'DSLAM_Backup_show':
                case 'Backupshow': 
                    $usenet->write( "show software-mngt upload-download  \r\n");
                    $usenet->write("show software-mngt oswp\r\n");
                    
                break;
                case 'Backupladen': 
                    $usenet->write( "$Backup_restore  \r\n");
                    $usenet->write( "show software-mngt upload-download  \r\n");
                    $usenet->write("show software-mngt oswp\r\n");
                    
                break;
                case 'Backupaktivieren1': 
                    $usenet->write( "admin software-mngt oswp 1 activate with-linked-db  \r\n");
                    
                break;
                case 'Backupaktivieren2': 
                    $usenet->write( "admin software-mngt oswp 2 activate with-linked-db  \r\n");
                    
                break;
                case 'DSLAM_oswp_set':
                     
                    $usenet->write( "configure software-mngt oswp 1 primary-file-server-id ".$tftpserver."\r\n");
                    $usenet->write( "configure software-mngt oswp 2 primary-file-server-id ".$tftpserver."\r\n");
                    
                break;
                 
                case 'ShowOntOptics':
                     
                    $usenet->write("show equipment ont sw-version \r\n");
                    $usenet->write("show pon sw-download \r\n");
                    
                    $usenet->write(" exit all\r\n");
                    
                break;
                case 'ShowPonOptics':
                     
                    $usenet->write("show pon sfp-inventory \r\n");
                    $usenet->write("show pon optics \r\n");
                    $usenet->write("info configure pon flat \r\n");
                    $usenet->write(" exit all\r\n");
                    
                break;
                ############################################################
                # TFTP write ONT Firmware
                # tftp DSLAMIP
                # ( put FE56065AFIA53 /pub/OntSw/Download/FE56065AFIA53 ) ?
                # ( put FE56065AFEB65 /pub/OntSw/Download/FE56065AFEB65 ) ?
                #
                case 'ShowOntStatus':
                     
                    $usenet->write("show interface port | match exact:ont: \r\n");
                    
                    $usenet->write(" exit all\r\n");
                    
                break;
                    
                case 'ShowOntupS':
                     
                    $usenet->write("show interface port | match exact:ont:  | match exact:up | match exact:up   | match exact::  | count summary \r\n");
                    
                    $usenet->write(" exit all\r\n");
                    
                break;
                
                case 'ShowOntdownS':
                     
                    $usenet->write("show interface port | match exact:ont:  | match exact:down  | match exact::  | count summary \r\n");
                    
                    $usenet->write(" exit all\r\n");
                    
                break;
                
                case 'ShowOntup':
                  
                     
                    $usenet->write("show interface port | match exact:ont:  | match exact:up   | match exact:down skip \r\n");
                    
                    $usenet->write(" exit all\r\n");
                break;
                
                case 'ShowOntdown':
                     
                    $usenet->write("show interface port | match exact:ont:  | match exact:up  | match exact:down   \r\n");
                    
                    $usenet->write(" exit all\r\n");
                    
                break;
                case 'Spectrum setzen':
                    $usenet->write( "configure xdsl spectrum-profile 1 name VDSL2_17a_excladsl2plus version 1 dis-ansi-t1413 dis-etsi-dts dis-g992-1-a dis-g992-1-b dis-g992-2-a dis-g992-3-a dis-g992-3-b dis-etsi-ts g993-2-17a rf-band-list 07:12:07:d0:19:0d:ac:0e:d8:19:1b:58:1b:bc:19:27:74:27:a6:19:36:b0:38:0e:19\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 1 vdsl vdsl-band-plan annex-b-998ade optional-band up optional-endfreq 276 adsl-band allow-adsl max-agpowlev-up 145 opt-startfreq 138\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 1 vdsl pbo 1 param-a 4000\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 1 vdsl pbo 2 param-a 4450 param-b 2930\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 1 vdsl pbo 3 param-a 4550 param-b 1660\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 1 vdsl pbo 4 param-a 4000 param-b 1077\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 1 vdsl pbo 5 param-a 4000 param-b 943\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 1 vdsl2 max-agpowlev-down 145 max-agpowlev-up 145 psd-shape-down regionbm2-psd-down cs-psd-shape-dn 00:01:14:be:08:a0:be:08:a0:60 psd-shape-up regionbm2-psd-up rx-psd-shape-up equal-fext psd-pbo-par-a-up 0f:a0:11:62:11:c6:0f:a0:0f:a0 psd-pbo-par-b-up 00:00:0b:72:06:7c:04:35:03:af\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 1 vdsl2 cust-psd-pt-down 1 frequency 276 psd 190\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 1 vdsl2 cust-psd-pt-down 2 frequency 2208 psd 190\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 1 vdsl2 cust-psd-pt-down 3 frequency 2208 psd 96\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 1 vdsl2 pbo 1 equal-fext 18 param-a 4000\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 1 vdsl2 pbo 2 equal-fext 237 param-a 4450 param-b 2930\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 1 vdsl2 pbo 3 equal-fext 118 param-a 4550 param-b 1660\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 1 vdsl2 pbo 4 equal-fext 18 param-a 4000 param-b 1077\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 1 vdsl2 pbo 5 equal-fext 18 param-a 4000 param-b 943\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 1 active\r\n");

                    $usenet->write( "configure xdsl spectrum-profile 2 name ADSL2_Plus version 1 dis-ansi-t1413 dis-etsi-dts dis-g992-1-a dis-g992-1-b dis-g992-2-a dis-g992-3-a dis-g992-3-b g992-5-b dis-etsi-ts\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 2 vdsl vdsl-band-plan annex-b-998ade adsl-band allow-adsl\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 2 vdsl pbo 1 param-a 4000\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 2 vdsl pbo 2 param-a 4730 param-b 1977\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 2 vdsl pbo 3 param-a 5400 param-b 1577\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 2 vdsl pbo 4 param-a 5400 param-b 1077\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 2 vdsl pbo 5 param-a 5400 param-b 943\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 2 vdsl2 pbo 1 param-a 4000\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 2 vdsl2 pbo 2 param-a 4730 param-b 1977\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 2 vdsl2 pbo 3 param-a 5400 param-b 1577\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 2 vdsl2 pbo 4 param-a 5400 param-b 1077\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 2 vdsl2 pbo 5 param-a 5400 param-b 943\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 2 active\r\n");
                      
                    $usenet->write( "configure xdsl spectrum-profile 3 name VDSL_17a_VDSL_allow version 1 dis-ansi-t1413 dis-etsi-dts dis-g992-1-a dis-g992-1-b dis-g992-2-a dis-g992-3-a dis-g992-3-b dis-etsi-ts g993-2-17a\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 3 vdsl vdsl-band-plan annex-b-998ade optional-band up optional-endfreq 276 max-agpowlev-up 145\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 3 vdsl pbo 1 param-a 4000\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 3 vdsl pbo 2 param-a 4450 param-b 2930\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 3 vdsl pbo 3 param-a 4550 param-b 1660\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 3 vdsl pbo 4 param-a 4000\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 3 vdsl pbo 5 param-a 4000\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 3 vdsl2 propr-feat-value 0 max-agpowlev-down 145 max-agpowlev-up 145 psd-shape-down regionbm2-psd-down psd-shape-up regionbm2-psd-up rx-psd-shape-up equal-fext psd-pbo-par-a-up 0f:a0:11:62:11:c6:0f:a0:0f:a0 psd-pbo-par-b-up 00:00:0b:72:06:7c:00:00:00:00\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 3 vdsl2 pbo 1 equal-fext 18 param-a 4000\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 3 vdsl2 pbo 2 equal-fext 237 param-a 4450 param-b 2930\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 3 vdsl2 pbo 3 equal-fext 118 param-a 4550 param-b 1660\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 3 vdsl2 pbo 4 equal-fext 18 param-a 4000\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 3 vdsl2 pbo 5 equal-fext 18 param-a 4000\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 3 active\r\n");
                      
                    $usenet->write( "configure xdsl spectrum-profile 4 name VDSL_17a_incl_adsl version 1 dis-ansi-t1413 dis-etsi-dts dis-g992-1-a dis-g992-1-b dis-g992-2-a dis-g992-3-a dis-g992-3-b dis-etsi-ts g993-2-17a\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 4 vdsl vdsl-band-plan annex-b-998ade optional-band up optional-endfreq 276 adsl-band allow-adsl max-agpowlev-up 145\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 4 vdsl pbo 1 param-a 4000\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 4 vdsl pbo 2 param-a 4450 param-b 2930\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 4 vdsl pbo 3 param-a 4550 param-b 1660\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 4 vdsl pbo 4 param-a 4000\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 4 vdsl pbo 5 param-a 4000\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 4 vdsl2 propr-feat-value 0 max-agpowlev-down 145 max-agpowlev-up 145 psd-shape-down regionbm2-psd-down psd-shape-up regionbm2-psd-up rx-psd-shape-up equal-fext psd-pbo-par-a-up 0f:a0:11:62:11:c6:0f:a0:0f:a0 psd-pbo-par-b-up 00:00:0b:72:06:7c:00:00:00:00\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 4 vdsl2 pbo 1 equal-fext 18 param-a 4000\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 4 vdsl2 pbo 2 equal-fext 237 param-a 4450 param-b 2930\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 4 vdsl2 pbo 3 equal-fext 118 param-a 4550 param-b 1660\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 4 vdsl2 pbo 4 equal-fext 18 param-a 4000\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 4 vdsl2 pbo 5 equal-fext 18 param-a 4000\r\n");
                    $usenet->write( "configure xdsl spectrum-profile 4 active\r\n");
                break;
                case 'spectrum anzeigen':
                    $usenet->write( "info configure xdsl spectrum-profile flat\r\n");
                break;
                    #########################################
                    # Ende DSLAM Standort Kommandos
                    #########################################
                default:
                    echo " &nbsp;&nbsp;&nbsp;&nbsp;<a class='fancy-button outline round red' href='$logon_ip:$logon_port' target='_blank'>&nbsp;&nbsp;$dslam_name logon $logon_ip:$logon_port&nbsp;&nbsp;</a>";
                    switch ($cardtype) {
                        case 'ADSL':
                            echo 'ADSL wird nicht mehr unterstuetzt<br />';
                        break;
                        case 'VDSL':
                        case 'VDSL-BLV':    
                        case 'VDSL-SLV':
                            $display_ende =true;
                            
                            
                            
                            switch ($Dslam_comando) {
                                case "DSLAM_Kunden_Loeschen1":
                                $usenet->write("configure xdsl line $line_identifier$atm no admin-up  no service-profile no dpbo-profile $NoVectoring\r\n");
                                $usenet->write("configure interface port xdsl-line:$line_identifier$atm no admin-up  no user\r\n");
                                $usenet->write("configure bridge no port $line_identifier$atm\r\n");
                                echo '<hr> geloescht<hr>';
                             break;

                            case "DSLAM_Kunden_Vectoring_an":
                                $usenet->write("configure xdsl line $line_identifier $Vectoring \r\n");
                                $usenet->write( "exit all \r\n");
                            break;
                            
                            case "DSLAM_Kunden_Vectoring_aus":
                                $usenet->write("configure xdsl line $line_identifier $NoVectoring \r\n");
                                $usenet->write( "exit all \r\n");
                            break;
 
                            case 'DSLAM_Kunden_Status':
                                if ( $DSLAM_Status  == true) {
                                  $usenet->write( "show xdsl operational-data line $line_identifier $detail\r\n");

                                  
                                } else {
                                  
                                  $usenet->write( "show xdsl operational-data line $line_identifier detail\r\n");
                                  $usenet->write( "info configure xdsl service-profile $produkt_index \r\n");
                                  $usenet->write( " info configure xdsl line $line_identifier \r\n"); 

                                  $usenet->write( " info configure bridge port $line_identifier$atm \r\n"); 
                                  $usenet->write( "show vlan bridge-port-fdb $line_identifier$atm \r\n"); 
                                  if ($tv_service == "yes" ) $usenet->write("info configure igmp channel vlan:$line_identifier:$vlan_iptv_local \r\n");

                                 }

                             break;


                                case 'DSLAM_Kunden_Einrichten':
                                case 'DSLAM_Kunden_einrichten':
                                case 'DSLAM_Kunden_Bandbreite':
                                case 'DSLAM_Kunden_sperren':
                                case 'DSLAM_Kunden_freischalten':
                                case 'DSLAM_Port_Reset':
                                if ($vlan_pppoe_local == '') $vlan_pppoe_local = $vlan_id;
                                if ($Dslam_comando <> "DSLAM_Kunden_Bandbreite" and $Dslam_comando <> "DSLAM_Kunden_sperren" and $Dslam_comando <> "DSLAM_Kunden_freischalten" and $Dslam_comando <> "DSLAM Port Reset") {
                                    $usenet->write("configure xdsl line $line_identifier service-profile $produkt_index   spectrum-profile $Spectrumprofile $Vectoring transfer-mode $Transfermode dpbo-profile $DPBO admin-up \r\n");
                                    $usenet->write( "exit all \r\n");
                                    
                                    if ( $Transfermode == "atm" ) {
                                        $usenet->write( "configure atm pvc $line_identifier$atm \r\n");
                                    }

                                    $usenet->write( "configure interface port xdsl-line:$line_identifier user $clientid admin-up \r\n");
                                    DSLAM_Bridge ($usenet,$line_identifier,$vlan_id,$vlan_pppoe_local,$vlan_pppoe,$vlan_acs_local,$vlan_acs,$vlan_iptv_local,$vlan_iptv,$tag,$tv_service,$atm, $acs_qos_profile);                             
                                }
                                if ($Dslam_comando == "DSLAM_Kunden_Bandbreite" ) $usenet->write("configure xdsl line $line_identifier service-profile $produkt_index admin-up \r\n");
                                if ($Dslam_comando == "DSLAM_Kunden_freischalten" ) $usenet->write("configure xdsl line $line_identifier admin-up \r\n");
                                if ($Dslam_comando == "DSLAM_Kunden_sperren" ) $usenet->write("configure xdsl line $line_identifier no admin-up \r\n");
                                if ($Dslam_comando == "DSLAM_Port_Reset" ){
                                    $usenet->write("configure xdsl line $line_identifier no admin-up \r\n");
                                    $usenet->write("admin-up \r\n");
                                }
                                    
                                    $usenet->write( "exit all \r\n");
                                 break;
                                case 'Experte':
                                    echo " &nbsp;&nbsp;&nbsp;&nbsp;<br><span class='text blue'> xDSL CLI Kommandos </span>";
                                    echo " &nbsp;&nbsp;&nbsp;&nbsp;<br><span class='text green'> configure xdsl line $line_identifier service-profile $produkt_index   spectrum-profile $Spectrumprofile $Vectoring transfer-mode $Transfermode dpbo-profile $DPBO admin-up <br>";
                                    echo  "exit all <br>";
                                    echo  "configure interface port xdsl-line:$line_identifier user $clientid admin-up <br>";
                                    DSLAM_Bridge_show ($usenet,$line_identifier,$vlan_id,$vlan_pppoe_local,$vlan_pppoe,$vlan_acs_local,$vlan_acs,$vlan_iptv_local,$vlan_iptv,$tag,$tv_service,$atm, $acs_qos_profile);                                
                                
                                 break;
                            } # ende switch 2nd dslam_comando vdsl
                      break;
                    case 'NELTB':
                    case 'ETHERNET':
                        $line_identifierE = substr($line_identifier, 0, 5);

                        switch ($Dslam_comando) {
                            case 'DSLAM_Kunden_Einrichten':
                            case 'DSLAM_Kunden_einrichten':
                            case 'DSLAM_Kunden_Bandbreite':
                            case 'DSLAM Port Reset':
                            case 'DSLAM_Kunden_sperren':
                            case "DSLAM_Kunden_freischalten":
                                $display_ende =true;
                                
                                if ($Dslam_comando == "DSLAM_Kunden_Einrichten" or $Dslam_comando == "DSLAM_Kunden_einrichten") {
                                    $usenet->write("configure ethernet line $line_identifier mau 1 type 1000basebx10d power up \r\n");
                                    $usenet->write("exit \r\n");
                                    $usenet->write("admin-up \r\n");
                                    DSLAM_Bridge ($usenet,$line_identifier,$vlan_id,$vlan_pppoe_local,$vlan_pppoe,$vlan_acs_local,$vlan_acs,$vlan_iptv_local,$vlan_iptv,$tag,$tv_service,$atm, $acs_qos_profile);                             
                                    //$usenet->write("configure bridge port $line_identifier qos-profile name:$ethernet_up_down \r\n");
                                    $usenet->write("configure bridge port $line_identifier no qos-profile \r\n");
                                    $usenet->write("configure bridge port $line_identifier vlan-id $vlan_pppoe_local qos-profile name:$ethernet_up_down \r\n");
                                }

                                if ($Dslam_comando == "DSLAM_Kunden_Bandbreite") {
                                    //$usenet->write("configure bridge port $line_identifier vlan-id $vlan_pppoe_local no qos-profile \r\n");
                                    //$usenet->write("configure bridge port $line_identifier qos-profile name:$ethernet_up_down \r\n");
                                    $usenet->write("configure bridge port $line_identifier no qos-profile \r\n");
                                    $usenet->write("configure bridge port $line_identifier vlan-id $vlan_pppoe_local no qos-profile \r\n");
                                    $usenet->write("configure bridge port $line_identifier vlan-id $vlan_pppoe_local qos-profile name:$ethernet_up_down \r\n");
                                }

                                if ($Dslam_comando == "DSLAM Port Reset") {
                                    $usenet->write("configure ethernet line $line_identifier mau 1 power down \r\n");
                                    $usenet->write("power up \r\n");
                                }

                                if ($Dslam_comando == "DSLAM Kunde sperren") {
                                    $usenet->write("configure ethernet line $line_identifier no admin-up \r\n");
                                }

                                if ($Dslam_comando == "DSLAM Kunde freischalten") {
                                    $usenet->write("configure ethernet line $line_identifier admin-up \r\n");
                                }

                                break;

                            case 'DSLAM_Kunden_Status':
                                $sfp='/';

                                if ($prozessor == 'NANT-E' or $prozessor == 'FANT-F') {
                                    $sfp=':sfp:';
                                }
                                
                                $PolicerD = "D_$Service";
                                $PolicerU = "U_$Service";
                                $PolicerD = substr($PolicerD, 0, 10);
                                $PolicerU = substr($PolicerU, 0, 10);

                                $usenet->write("show interface port ethernet-line:$line_identifier \r\n");

                                $Line = substr($line_identifier, 0, 6);

                                if (($tmp = substr($Line, -1)) == "/") {
                                    $Line = substr($line_identifier, 0, 5);
                                }

                                if (($tmp = substr($line_identifier, -2, 1)) != "/") {
                                    $Line1 = substr($line_identifier, -2);
                                } else {
                                    $Line1 = substr($line_identifier, -1);
                                }

                                if ($DSLAM_Status == false) {
                                    $down_red = 'so nicht';
                                    $showsfp = "show equipment diagnostics sfp lt:$Line$sfp$Line1 detail\r\n";
                                    
                                    $usenet->write($showsfp);
                                    $usenet->write("info configure ethernet line $line_identifier \r\n");
                                    $usenet->write("info configure interface port ethernet-line:$line_identifier\r\n");
                                    $usenet->write("info configure bridge port $line_identifier \r\n");

                                    if ($tv_service == "yes") {
                                        $usenet->write("info configure igmp channel vlan:$line_identifier:$vlan_iptv_local \r\n");
                                    }

                                    $usenet->write("info configure qos profiles session $ethernet_up_down \r\n");

                                    if ('WiSoTEL' === \Wisotel\Configuration\Configuration::get('companyName')) {
                                        $usenet->write ("info configure qos profiles policer $PolicerD flat \r\n");
                                        $usenet->write ("info configure qos profiles policer $PolicerU flat \r\n");
                                    } elseif ('Stadtwerke Bühl' === \Wisotel\Configuration\Configuration::get('companyName')) {
                                        $usenet->write ("info configure qos profiles policer $PolicerD flat \r\n");
                                        $usenet->write ("info configure qos profiles policer $PolicerU flat \r\n");
                                    }

                                    $usenet->write( "show vlan bridge-port-fdb $line_identifier \r\n"); 
                                }

                                break;
                            
                            case 'DSLAM_Kunden_Loeschen1':
                                $display_ende =true;
                                $usenet->write("configure ethernet line $line_identifier \r\n");
                                $usenet->write("no admin-up \r\n");
                                $usenet->write("configure bridge no port $line_identifier \r\n");
                                $usenet->write("configure interface port ethernet-line:$line_identifier no admin-up  user \r\n");
                                $usenet->write("$admin_save\r\n");
                                #$usenet->write("$Backup_id\r\n\r\n");

                                break;

                            case 'Experte':
                                echo " &nbsp;&nbsp;&nbsp;&nbsp;<br><span class='text blue'> ETHERNET NELTB CLI Kommandos </span><span class='text green'>  show interface port ethernet-line:$line_identifier detail</span><br />";
                                echo " &nbsp;&nbsp;&nbsp;&nbsp;<span class='text blue'> Show SFP Optics</span><span class='text green'>   show equipment diagnostics sfp lt:".substr ($line_identifier,0,5).":sfp:".$dslam_port."</span><br />";
                                echo " &nbsp;&nbsp;&nbsp;&nbsp;<span class='text blue'> Kunde im DSLAM einrichten <br><span class='text green'>";
                                echo " &nbsp;&nbsp;&nbsp;&nbsp;configure ethernet line $line_identifier mau 1 type 1000basebx10d power up <br>";

                                DSLAM_Bridge_show ($usenet,$line_identifier,$vlan_id,$vlan_pppoe_local,$vlan_pppoe,$vlan_acs_local,$vlan_acs,$vlan_iptv_local,$vlan_iptv,$tag,$tv_service,$atm, $acs_qos_profile);                                

                                break;
                        }# end switch 3rd dslam_comando ethernet

                        break;

                    case 'GPON':
                        $line_identifierG = $line_identifier."/1/1";
                        $display_ende = true;

                        switch ($Dslam_comando) {
                            case "DSLAM_Kunden_Einrichten":
                            case "DSLAM_Kunden_einrichten":
                                $usenet->write("configure equipment ont interface $line_identifier admin-state down\r\n");
                                $usenet->write("configure equipment ont slot $line_identifier/1 admin-state down\r\n");
                                $usenet->write("configure equipment ont no interface $line_identifier\r\n");
                                $usenet->write("######\r\n");
                                $usenet->write("configure equipment ont interface $line_identifier sw-ver-pland $firmware_version\r\n");
                                $usenet->write("configure equipment ont interface $line_identifier sernum ALCL:$dtag_line sw-dnload-version disabled desc1 $clientid\r\n");
                                $usenet->write("configure equipment ont interface $line_identifier admin-state up\r\n");
                                $usenet->write("configure equipment ont slot $line_identifier/1 planned-card-type 10_100base plndnumdataports 1 plndnumvoiceports 0\r\n");
                                $usenet->write("configure interface port uni:$line_identifier/1/1 admin-up\r\n");
                                $usenet->write("configure qos interface $line_identifier/1/1 queue 0 priority 6 weight 34 queue-profile name:NGLT_Default shaper-profile name:$gpon_download\r\n");
                                $usenet->write("configure qos interface $line_identifier/1/1 upstream-queue 0 weight 1 bandwidth-profile name:$gpon_upload\r\n");
                                $usenet->write("configure qos interface $line_identifier/1/1 upstream-queue 6 weight 1 bandwidth-profile name:CIR_1M\r\n");

                                DSLAM_Bridge ($usenet,$line_identifierG,$vlan_id,$vlan_pppoe_local,$vlan_pppoe,$vlan_acs_local,$vlan_acs,$vlan_iptv_local,$vlan_iptv,$tag,$tv_service,$atm, $acs_qos_profile);
                                
                                break;

                            case "DSLAM_Kunden_Bandbreite":
                                $usenet->write("configure qos interface $line_identifier/1/1 queue 0 priority 6 weight 34 queue-profile name:NGLT_Default shaper-profile name:$gpon_download\r\n");
                                $usenet->write("configure qos interface $line_identifier/1/1 upstream-queue 0 weight 1 bandwidth-profile name:$gpon_upload\r\n");
                                $usenet->write("configure qos interface $line_identifier/1/1 upstream-queue 6 weight 1 bandwidth-profile name:CIR_1M\r\n");
                                
                                break;

                            case "DSLAM_Port_Reset":
                                $usenet->write("configure equipment ont interface $line_identifier admin-state down\r\n");
                                $usenet->write("configure equipment ont slot $line_identifier/1 admin-state down\r\n");
                                $usenet->write("configure equipment ont interface $line_identifier admin-state up\r\n");
                                $usenet->write("configure equipment ont slot $line_identifier/1 admin-state up\r\n");

                                break;

                            case "DSLAM_Kunden_sperren":
                                $usenet->write("configure equipment ont interface $line_identifier admin-state down\r\n");
                                
                                break;

                            case "DSLAM_Kunden_freischalten":
                                $usenet->write("configure equipment ont interface $line_identifier admin-state up\r\n");

                                break;

                            case 'DSLAM_Kunden_Loeschen1':
                                $display_ende = true;

                                $usenet->write("configure equipment ont interface $line_identifier admin-state down\r\n");
                                $usenet->write("configure equipment ont slot $line_identifier/1 admin-state down\r\n");
                                $usenet->write("configure equipment ont no interface $line_identifier\r\n");
                                $usenet->write("configure bridge no port $line_identifier/1/1\r\n");
                                $usenet->write("######\r\n");

                                break;

                            case 'DSLAM_Kunden_Status':
                                $usenet->write("show interface port ont:$line_identifier\r\n");

                                if ($DSLAM_Status == false) {
                                    $usenet->write("show equipment ont interface  $line_identifier\r\n");
                                    $usenet->write("show equipment ont optics $line_identifier\r\n");
                                    $usenet->write("info configure bridge port $line_identifier/1/1 \r\n");
                                    $usenet->write("info configure qos interface $line_identifier/1/1 queue 0\r\n");
                                    $usenet->write("info configure qos interface $line_identifier/1/1 upstream-queue 0\r\n");
                                    $usenet->write("show vlan bridge-port-fdb $line_identifier/1/1 \r\n"); 
                                    $usenet->write("show equipment ont sw-download $line_identifier\r\n");
                                    
                                    $anzahl = 7;
                                    $Test = substr($line_identifier, 6, 1);

                                    if ($Test == "/") {
                                        $anzahl = 8;
                                    }

                                    $line_identifierE = substr($line_identifier, 0, $anzahl);

                                    $usenet->write("show pon sw-download $line_identifierE\r\n");
                                }

                                break;

                            case 'Experte':
                                echo " &nbsp;&nbsp;&nbsp;&nbsp;<br><span class='text blue'> GPON CLI Kommandos </span>";
                                echo " &nbsp;&nbsp;&nbsp;&nbsp;<br><span class='text green'>configure equipment ont interface $line_identifier admin-state down<br>";
                                echo " &nbsp;&nbsp;&nbsp;&nbsp;configure equipment ont slot $line_identifier/1 admin-state down<br>";
                                echo " &nbsp;&nbsp;&nbsp;&nbsp;configure equipment ont no interface $line_identifier<br>";
                                echo " &nbsp;&nbsp;&nbsp;&nbsp;######<br>";
                                echo " &nbsp;&nbsp;&nbsp;&nbsp;configure equipment ont interface $line_identifier sw-ver-pland $firmware_version<br>";
                                echo " &nbsp;&nbsp;&nbsp;&nbsp;sernum ALCL:$dtag_line sw-dnload-version disabled desc1 $clientid<br>";
                                echo " &nbsp;&nbsp;&nbsp;&nbsp;configure equipment ont interface $line_identifier admin-state up<br>";
                                echo " &nbsp;&nbsp;&nbsp;&nbsp;configure equipment ont slot $line_identifier/1 planned-card-type 10_100base plndnumdataports 1 plndnumvoiceports 0<br>";
                                echo " &nbsp;&nbsp;&nbsp;&nbsp;configure interface port uni:$line_identifier/1/1 admin-up<br>";
                                echo " &nbsp;&nbsp;&nbsp;&nbsp;configure qos interface $line_identifier/1/1 queue 0 priority 6 weight 34 queue-profile name:NGLT_Default shaper-profile name:$gpon_download<br>";
                                echo " &nbsp;&nbsp;&nbsp;&nbsp;configure qos interface $line_identifier/1/1 upstream-queue 0 weight 1 bandwidth-profile name:$gpon_upload<br>";
                                echo " &nbsp;&nbsp;&nbsp;&nbsp;configure qos interface $line_identifier/1/1 upstream-queue 6 weight 1 bandwidth-profile name:CIR_1M<br>";

                                DSLAM_Bridge_show ($usenet,$line_identifierG,$vlan_id,$vlan_pppoe_local,$vlan_pppoe,$vlan_acs_local,$vlan_acs,$vlan_iptv_local,$vlan_iptv,$tag,$tv_service,$atm, $acs_qos_profile);

                                break;
                        } #end switch dslam_comando GPON

                        break;

                } # ende switch cardt typ
                
                break;
                
            } # ende switch 1st dslam_conamdo
            if ($Dslam_comando != 'Experte') {
                echo "Komando war $Dslam_comando Prozessor war $prozessor <br>";

                switch ($Dslam_comando) {
                    case 'DSLAM_Kunden_Einrichten':
                    case 'DSLAM_Kunden_einrichten':
                    case 'DSLAM_Kunden_Bandbreite':
                    case 'DSLAM_Kunden_Loeschen1':
                    case 'DSLAM_Kunden_sperren':
                    case "DSLAM_Kunden_freischalten":
                    case 'IPTV_setup':
#                   switch ($prozessor) {
#                       case 'NANT-E':
#                       case 'FANT-F':
#                       case 'NRNT-A':
#                       case 'NANT-A':
#                       case 'NANT-C':
#                       case 'NANT-D':
                    if ($dslam_backup == 'Y') {
                        $usenet->write(" exit all\r\n");
                        $usenet->write("$admin_save\r\n");
                        $usenet->write("$Backup_id \r\n");
                        $usenet->write( "show software-mngt upload-download  \r\n"); 
                    }
#                    break;
#                    }
                 break;
                }
                DSLAM_close ($usenet,$Dslam_comando,$client_id,$exit_in_up,$exit_line,$down_red);
            }
        } else {   # dslam logon nich m?glich wegen Parameter Fehler
         if ($Dslam_comando != '') {
            echo 'Dslam logon fehlgeschlagen<br>';
            echo sprintf("Connection to %s:%s failed - Location-id: %s<br />\n",
                $logon_ip,
                $logon_port,
                $location_id
            );
         }
        }

        break;

        case 'ISKRATEL':
            $FSAN = substr ($terminal_type, -4);
            
            switch ($cardtype) {
                case 'PONO':
                case 'GPON':
                    $sublineident = substr ($line_identifier, -2);
                    $PON = substr ($line_identifier, -4,1);
                    $Einrichten4 ="";

                    if ($firmware_version <> "") {
                        $Einrichten4 ="onu image download-activate-commit 1/$PON/$sublineident $firmware_version\n";
                    }
            
                    switch ($FSAN) {
                        case '2426':
                            $Einrichten = "bridge add 1-1-$PON-$sublineident/gpononu gem 7$sublineident gtp  $produkt_index downlink-pppoe vlan $vlan_id epktrule $produkt_index tagged sip eth all wlan 1 rg-bpppoe\n";
                            $Einrichten1 = "cpe rg wan modify 1/$PON/$sublineident-gponport vlan $vlan_id pppoe-usr-id $clientid pppoe-password $pppoePassword\n";
                            $Einrichten2 = "cpe-mgr add local 1-1-$PON-$sublineident/gpononu gem 5$sublineident gtp 1\n";
                            $Einrichten3 = "cpe voip add 1/$PON/$sublineident/1 dial-number ".$purtelMasterAccount['purtel_login']." username ".$purtelMasterAccount['purtel_login']." password ".$purtelMasterAccount['purtel_password']." voip-server-profile WisoTel\n";
                            
                            break;
            
                        case '2301':
                        case '2311':
                            $Einrichten = "bridge add 1-1-$PON-$sublineident/gpononu gem 7$sublineident gtp $produkt_index downlink vlan $vlan_id epktrule $produkt_index tagged eth 1\n";
                            $Einrichten1 = "";
                            $Einrichten2 = "";
                            $Einrichten3 = "";
                            
                            break;
                    }

                    // connecto to DSLAM
                    if ($Dslam_comando != 'Anzeigen' and $Dslam_comand != "Experte" ) {
                        $usenet = DSLAM_open_iskratel($logon_ip, $logon_port, $DSLAMLOGON, $DSLAMPW);
                    }

                    // this must be a part of the text the DSLAM shows when ready to handle commands
                    $dslamReadyToHandleCommandsIdentifier = 'WISOTEL_PONO>';

                    switch ($Dslam_comando) {
                        case 'DSLAM_Kunden_Status':
                            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>"; 
                            
                            try{
                                $usenet
                                    ->write("onu show 1/$PON/$sublineident\n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier)

                                    ->write("onu status 1/$PON/$sublineident\n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier)

                                    ->write("bridge show 1-1-$PON-7$sublineident\n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier)

                                    ->write("exit\n");
                            } catch (WaitUntilStringIsFoundException $exception) {
                                echo prettifyWaitUntilStringIsFoundException($exception);
                            }

                            echo sprintf('<pre>%s</pre>',
                                str_replace("Error", "<span class='text red'> Error </span>", $usenet->getLastReadData())
                            );

                            #fputs ($usenet, "onu show 1/$PON/$sublineident\n");
                            #sleep(2);
                            #fputs ($usenet, "onu status 1/$PON/$sublineident\n");
                            #sleep (4);
                            #fputs ($usenet, "bridge show 1-1-$PON-7$sublineident\n");
                            #sleep (2);
                            #fputs ($usenet, "exit\n");
                            #echo "<pre>";
                            #while (!feof($usenet)) { 
                            #$buffer = fgets($usenet, 400)."<BR>\n";
                            #$buffer = str_replace("Error", "<span class='text red'> Error </span>", $buffer); 
                            #echo "$buffer";
                            #} 
                            #fclose($usenet);
                            #echo "</pre>";

                            break;

                        case 'DSLAM_Kunden_Loeschen':
                            echo "$NL <a href='index.php?menu=customer&id=$customer_id&area=SwitchTechConf&kommando=DSLAM%20Kunden%20Loeschen1'><span class='text red'><H1>&lt;Löschen im DSLAM Bestätigen&gt; </H1></a>";
                            $Dslam_comando = ''; 
                            
                            break;

                        case 'DSLAM_Kunden_Loeschen1':
                            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>";

                            try{
                                $usenet
                                    ->write("bridge delete 1-1-$PON-$sublineident/gpononu all\n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier)

                                    ->write("onu clear 1/$PON/$sublineident\n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier)

                                    ->write("onu delete 1/$PON/$sublineident\n")
                                    ->waitUntilStringIsFound('Ok to delete')

                                    ->write("yes\n")
                                    ->waitUntilStringIsFound('Do you want to exit')

                                    ->write("no\n")
                                    ->waitUntilStringIsFound('Are you sure')

                                    ->write("yes\n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier)

                                    ->write("exit\n");
                            } catch (WaitUntilStringIsFoundException $exception) {
                                echo prettifyWaitUntilStringIsFoundException($exception);
                            }

                            echo sprintf('<pre>%s</pre>',
                                str_replace("Error", "<span class='text red'> Error </span>", $usenet->getLastReadData())
                            );
                            
                            #fputs ($usenet, "bridge delete 1-1-$PON-$sublineident/gpononu all\n");
                            #sleep (3);
                            #fputs ($usenet, "onu clear 1/$PON/$sublineident\n");
                            #sleep (1);
                            #fputs ($usenet, "onu delete 1/$PON/$sublineident\n");
                            #sleep (5);
                            #fputs ($usenet, "yes\n");
                            #sleep (1);
                            #fputs ($usenet, "no\n");
                            #sleep (1);
                            #fputs ($usenet, "yes\n"); 
                            #sleep (5);
                            
                            #fputs ($usenet, "exit\n");
                            #echo "<pre>";
                            #while (!feof($usenet)) { 
                            #  $buffer = fgets($usenet, 400)."<BR>\n";
                            #  $buffer = str_replace("Error", "<span class='text red'> Error </span>", $buffer); 
                            #  echo "$buffer";
                            #} 
                            #fclose($usenet);
                            #echo "</pre>";

                            break;

                        case 'DSLAM_Kunden_Einrichten':
                            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>";
                            # cpe system common show all
                            try{
                                $usenet
                                    ->write("onu set 1/$PON/$sublineident vendorid ZNTS serno fsan $dtag_line meprof zhone-$FSAN \n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier, null, null, false, ['is wrong format' => 'Invalid command', 'Error' => 'Error'])
                                    //->write("cpe system add 1/$PON/$sublineident sys-common-profile 1 \n")
                                    //->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier, null, null, false, ['is wrong format' => 'Invalid command', 'Error' => 'Error'])

                                    ->write($Einrichten)
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier, null, null, false, ['Error' => 'Error'])

                                    ->write("bridge stats enable 1-1-$PON-7$sublineident-gponport-$vlan_id/bridge\n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier, null, null, false, ['Error' => 'Error'])

                                    ->write($Einrichten1)
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier, null, null, false, ['Error' => 'Error'])

                                    ->write($Einrichten2)
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier, null, null, false, ['Error' => 'Error']);

                                if (isset($purtelMasterAccount) && !empty($purtelMasterAccount['nummer'])) {
                                    $usenet->write($Einrichten3)
                                        ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier, null, null, false, ['Error' => 'Error']);
                                }

                                $usenet->write("exit\n");
                            } catch (WaitUntilStringIsFoundException $exception) {
                                echo prettifyWaitUntilStringIsFoundException($exception);
                            }

                            echo sprintf('<pre>%s</pre>',
                                str_replace("Error", "<span class='text red'> Error </span>", $usenet->getLastReadData())
                            );

                            #fputs ($usenet, "onu set 1/$PON/$sublineident vendorid ZNTS serno fsan $dtag_line meprof zhone-$FSAN \n");
                            #sleep (2);
                            #fputs ($usenet, $Einrichten);
                            #sleep (3);
                            #fputs ($usenet, "bridge stats enable 1-1-$PON-7$sublineident-gponport-$vlan_id/bridge\n");
                            #sleep (2);
                            #fputs ($usenet, $Einrichten1);
                            #sleep (2);
                            #fputs ($usenet, $Einrichten2);
                            #sleep (2);
                            #if ($purtelUsername <> '') {
                            #fputs ($usenet, $Einrichten3);
                            #sleep (2);
                            #}
                            #fputs ($usenet, "exit\n");
                            #echo "<pre>";
                            #while (!feof($usenet)) { 
                            #$buffer = fgets($usenet, 400)."<BR>\n";
                            #$buffer = str_replace("Error", "<span class='text red'> Error </span>", $buffer); 
                            #echo "$buffer";
                            #} 
                            #fclose($usenet);
                            #echo "</pre>";

                            break;

                        case 'Anzeigen':
                            echo "Lineident = $line_identifier Line = $sublineident -- PON = $PON <BR>";  
                            echo  "<span class='text green'>Einrichten mit CLI</span><BR><BR>";
                            echo  "onu set 1/$PON/$sublineident vendorid ZNTS serno fsan $dtag_line meprof zhone-$FSAN\n<BR>";
                            echo  $Einrichten."<BR>";
                            echo  "bridge stats enable 1-1-$PON-7$sublineident-gponport-$vlan_id/bridge\n<BR>";
                            
                            if ($Einrichten1 <> "") echo  $Einrichten1."<BR>";
                            if ($Einrichten2 <> "") echo  $Einrichten2."<BR>";
                            if (isset($purtelMasterAccount) && !empty($purtelMasterAccount['nummer'])) {
                                if ($Einrichten3 <> "") echo  $Einrichten3."<BR>";
                            }
                            if ($Einrichten4 <> "") echo  $Einrichten4."<BR>";

                            break;
            
                        case 'ONTFirmware':
                            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>";

                            try{
                                $usenet
                                    ->write("onu image show 1/$PON/$sublineident\n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier)
                                    ->write("exit\n");
                            } catch (WaitUntilStringIsFoundException $exception) {
                                echo prettifyWaitUntilStringIsFoundException($exception);
                            }

                            echo sprintf('<pre>%s</pre>',
                                str_replace("Error", "<span class='text red'> Error </span>", $usenet->getLastReadData())
                            );

                            #fputs ($usenet, "onu image show 1/$PON/$sublineident\n");
                            #sleep(2);
                            #fputs ($usenet, "exit\n");
                            #echo "<pre>";
                            #while (!feof($usenet)) { 
                            #$buffer = fgets($usenet, 400)."<BR>\n";
                            #$buffer = str_replace("Error", "<span class='text red'> Error </span>", $buffer); 
                            #echo "$buffer";
                            #} 
                            #fclose($usenet);
                            #echo "</pre>";

                            break;
            
                        case 'ONTSynch':
                            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>";

                            try{
                                $usenet
                                    ->write("onu resync 1-1-$PON-$sublineident \n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier)
                                    ->write("exit\n");
                            } catch (WaitUntilStringIsFoundException $exception) {
                                echo prettifyWaitUntilStringIsFoundException($exception);
                            }

                            echo sprintf('<pre>%s</pre>',
                                str_replace("Error", "<span class='text red'> Error </span>", $usenet->getLastReadData())
                            );

                            #fputs ($usenet, "onu resync 1-1-$PON-$sublineident \n");
                            #sleep(2);
                            #fputs ($usenet, "exit\n");
                            #echo "<pre>";
                            #while (!feof($usenet)) { 
                            #$buffer = fgets($usenet, 400)."<BR>\n";
                            #$buffer = str_replace("Error", "<span class='text red'> Error </span>", $buffer); 
                            #echo "$buffer";
                            #} 
                            #fclose($usenet);
                            #echo "</pre>";

                            break;

                        case 'ONTReboot':
                            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>";

                            try{
                                $usenet
                                    ->write("onu reboot 1-1-$PON-$sublineident \n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier)
                                    ->write("exit\n");
                            } catch (WaitUntilStringIsFoundException $exception) {
                                echo prettifyWaitUntilStringIsFoundException($exception);
                            }

                            echo sprintf('<pre>%s</pre>',
                                str_replace("Error", "<span class='text red'> Error </span>", $usenet->getLastReadData())
                            );

                            #fputs ($usenet, "onu reboot 1-1-$PON-$sublineident \n");
                            #sleep(1);
                            #fputs ($usenet, "exit\n");
                            #echo "<pre>";
                            #while (!feof($usenet)) { 
                            #$buffer = fgets($usenet, 400)."<BR>\n";
                            #$buffer = str_replace("Error", "<span class='text red'> Error </span>", $buffer); 
                            #echo "$buffer";
                            #} 
                            #fclose($usenet);
                            #echo "</pre>";

                            break;
            
                        case 'Bridge':
                            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>";

                            try{
                                $usenet
                                    ->write("bridge show $line_identifier/gpononu\n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier)
                                    ->write("exit\n");
                            } catch (WaitUntilStringIsFoundException $exception) {
                                echo prettifyWaitUntilStringIsFoundException($exception);
                            }

                            echo sprintf('<pre>%s</pre>',
                                str_replace("Error", "<span class='text red'> Error </span>", $usenet->getLastReadData())
                            );

                            #fputs ($usenet, "bridge show\n");
                            #sleep(1);
                            #fputs ($usenet, "exit\n");
                            #echo "<pre>";
                            #while (!feof($usenet)) { 
                            #$buffer = fgets($usenet, 400)."<BR>\n";
                            #$buffer = str_replace("Error", "<span class='text red'> Error </span>", $buffer); 
                            #echo "$buffer";
                            #} 
                            #fclose($usenet);
                            #echo "</pre>";

                            break;

                        case 'Bridges':
                            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>";
                            
                            $dslamReadyToHandleCommandsIdentifier = 'PONO>';
                            $dslamReadyToHandleCommandsIdentifier1 = 'A for all';

                            try {
                                $usenet
                                    ->write("bridge show \n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier, 20, null, false, ['Error' => 'Error', 'A for all' => 'A for all'])
                                    ->write("exit\n");
                            } catch (WaitUntilStringIsFoundException $exception) {
                                if (false !== strpos( $exception->getMessage() , 'A for all')) {
                                    try {
                                        $usenet
                                            ->write("A\n")
                                            ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier, 20, null, false, ['Error' => 'Error'])
                                            ->write("exit\n");
                                    } catch (WaitUntilStringIsFoundException $exception) {
                                        echo prettifyWaitUntilStringIsFoundException($exception);
                                    }
                                } else {
                                    echo prettifyWaitUntilStringIsFoundException($exception);
                                }
                            }

                            echo sprintf('<pre>%s</pre>',
                                str_replace("Error", "<span class='text red'> Error </span>", $usenet->getLastReadData())
                            );

                            break;
            
                        case 'ONTFWneu':
                            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>";

                            try{
                                $usenet
                                    ->write($Einrichten4)
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier)
                                    ->write("exit\n");
                            } catch (WaitUntilStringIsFoundException $exception) {
                                echo prettifyWaitUntilStringIsFoundException($exception);
                            }

                            echo sprintf('<pre>%s</pre>',
                                str_replace("Error", "<span class='text red'> Error </span>", $usenet->getLastReadData())
                            );

                            #fputs ($usenet, $Einrichten4);
                            #sleep (1);
                            #fputs ($usenet, "exit\n");
                            #echo "<pre>";
                            #while (!feof($usenet)) { 
                            #$buffer = fgets($usenet, 400)."<BR>\n";
                            #$buffer = str_replace("Error", "<span class='text red'> Error </span>", $buffer); 
                            #echo "$buffer";
                            #} 
                            #fclose($usenet);
                            #echo "</pre>";
                            
                            break;

                        case 'Backup':
                            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>";

                            try {
                                $usenet
                                    ->write("dump file $dslam_name_ext \n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier, 100, null, false, ['Error' => 'Error'])
                                    ->write("file upload 195.178.0.114  $dslam_name_ext $dslam_name_ext \n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier, 40, null, false, ['Error' => 'Error'])
                                    
                                    ->write("exit\n");
                            } catch (WaitUntilStringIsFoundException $exception) {
                                echo prettifyWaitUntilStringIsFoundException($exception);
                            }

                            echo sprintf('<pre>%s</pre>',
                                str_replace("Error", "<span class='text red'> Error </span>", $usenet->getLastReadData())
                            );
                            
                            break;
                            
                        case 'Backup_download':
                            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>";

                            try {
                                $usenet
                                    ->write("file download 195.178.0.114  $dslam_name_ext /$dslam_name_ext \n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier, 40, null, false, ['Error' => 'Error'])
                                    ->write("copy /$dslam_name_ext /onreboot/restore \n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier, 40, null, false, ['Error' => 'Error'])
                                    ->write("dir / \n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier, 40, null, false, ['Error' => 'Error'])
                                    ->write("dir /onreboot \n")
                                    ->waitUntilStringIsFound($dslamReadyToHandleCommandsIdentifier, 40, null, false, ['Error' => 'Error'])
                                    ->write("exit\n");
                            } catch (WaitUntilStringIsFoundException $exception) {
                                echo prettifyWaitUntilStringIsFoundException($exception);
                            }

                            echo sprintf('<pre>%s</pre>',
                                str_replace("Error", "<span class='text red'> Error </span>", $usenet->getLastReadData())
                            );
                            
                            break;
                            
                        case 'Backup_laden':
                            ?>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a class="fancy-button outline round red" href="<?php echo "index.php?menu=customer&id=$customer_id&area=SwitchTechConf&kommando=Backup_laden1"  ?>">&nbsp;reboot PONO mit Backup&nbsp; </a>&nbsp;&nbsp;<br><br>
                            <?php
                            $Dslam_comando = ''; 
                            
                            break;

                        case 'Backup_laden1':
                            echo "<br /><br /><span class='text green'> Online DSLAM Anwort </span><br />Connected\n<BR>";
                            echo "das wird 5 Minuten dauern !! Kaffe Pause";

                            try {
                                $usenet
                                    ->write("set2default\n")
                                    ->waitUntilStringIsFound('Ok to reset')

                                    ->write("yes\n")
                                    ->waitUntilStringIsFound('Do you want to exit')

                                    ->write("no\n")
                                    ->waitUntilStringIsFound('Are you sure')

                                    ->write("yes\n")
                                    ->waitUntilStringIsFound('Please')

                                    ->write("exit\n");
                            } catch (WaitUntilStringIsFoundException $exception) {
                                echo prettifyWaitUntilStringIsFoundException($exception);
                            }

                            echo sprintf('<pre>%s</pre>',
                                str_replace("Error", "<span class='text red'> Error </span>", $usenet->getLastReadData())
                            );

                            break;

                    } // end switch ($Dslam_comando)

                    // terminate connection to dslam if it existed
                    if (isset($usenet) && $usenet instanceof StreamSocket) {
                        $usenet->disconnect();
                    }
          break;
          
          }

        break;
        }
    } # ende Kunden Aktionen
}# ende Funktion

?>
