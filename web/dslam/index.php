<?php

use AppBundle\Entity\Customer;
use AppBundle\Entity\Location\Location;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use AppBundle\src\DslamCommunication\Communicator\SshGateway;
use AppBundle\Entity\Location\Card;
use AppBundle\Entity\Location\LsaKvz;
use AppBundle\Entity\Product\MainProduct;
require_once __DIR__.'/../_conf/database.inc';
require_once '_conf/database_service.inc';
require_once '_conf/dslam.php';
require_once  __DIR__.'/../vendor/wisotel/configuration/Configuration.php';

$dslamLabor = \Wisotel\Configuration\Configuration::get('dslamLabor');
$useddb = \Wisotel\Configuration\Configuration::get('wkvDatabase');
$useddbase = $useddb['database'];

function einrichten_kunden ($location_id1,$db1) {
    $Dslam_comando2 = '';
    $Dslam_comando3 = '';
    $Dslam_comando4 = '';
    $Dslam_comando5 = '';
    $location_id    = $location_id1;
    $db = $db1;


    // find all active customers for selected location
    $activeCustomersQuery = $db->query("SELECT `id`
        FROM `customers`
        WHERE `location_id` = ".$location_id."
        AND `connection_activation_date` IS NOT NULL
        AND `connection_activation_date` != ''
        AND (`wisocontract_canceled_date` IS NULL OR `wisocontract_canceled_date` = '')
    ");

    echo sprintf("%d_Kunden_werden eingerichtet\n",
        $activeCustomersQuery->num_rows
    );

    while ($customer = $activeCustomersQuery->fetch_assoc()) {
        // execute 'DSLAM_Kunden_Einrichten' for each customer
        DSLAM_exec('DSLAM_Kunden_Einrichten', '', $customer['id'], $Dslam_comando2, $Dslam_comando3, $Dslam_comando4, $Dslam_comando5,'N');
    }
    echo "Einrichten abgeschlossen für Location $location_id1 <br />";




}
$overwrite = isset($_GET['overwrite-location-ips']) ? $_GET['overwrite-location-ips'] : null;

if ($overwrite == 1) {
    $db->query("UPDATE `locations` SET `dslam_ip_adress` = '".$dslamLabor['ipaddress']."',`logon_port` = '".$dslamLabor['port']."'");

    if (!empty($db->error)) {
        die($db->error);
    }

    header('Location: ./');
}

?>
<html lang="de" xmlns="http://www.w3.org/1999/xhtml" xml:lang="de">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <title>DSLAM globale Einstellungen</title>
    <link media="all" href="_css/dslam.css" type="text/css" rel="stylesheet" />
</head>
<body>
    
      <?php
      if ($overwrite == 2) {
         if (strpos($useddbase, '-dev-') !== false) {
            ?>
            <div style="position: absolute; top: 10px; right:10px;">
            <a href="./?overwrite-location-ips=1" class="fancy-button outline round red">
            alle Locations DSLAM IP Überschreiben bestätigen !!!!
            <?php
            echo "<br>used dbase = $useddbase";
            ?>
            </a>
            </div>
            <?php
        } else {
            ?>
            <div style="position: absolute; top: 10px; right:10px;">
                <a href="./?overwrite-location-ips=''" class="fancy-button outline round red">
                die Datenbank ist keine -dev- Datenbank und wird deshalb nicht geändert !!!!
                <?php
                echo "<br>used dbase = $useddbase";
                ?>

                </a>
                </div>
                <?php
            
                
        }

      } else {
        if ($dslamLabor['ipaddress'] != '' and $dslamLabor['port'] != '') {
            ?>
            <div style="position: absolute; top: 10px; right:10px;">
            <a href="./?overwrite-location-ips=2" class="fancy-button outline round red">
                <?php
                $countLocationsTotal = $db->query("SELECT COUNT(*) FROM `locations`");
                $countLocationsTotal = $countLocationsTotal->fetch_row();

                $countLocationsPointingToLabor = $db->query("SELECT COUNT(*)
                    FROM `locations`
                    WHERE `dslam_ip_adress` = '".$dslamLabor['ipaddress']."'");
                $countLocationsPointingToLabor = $countLocationsPointingToLabor->fetch_row();

                echo sprintf('%d/%d Locations zeigen zu %s Port %s<br />Alle Locations überschreiben?',
                    $countLocationsPointingToLabor[0],
                    $countLocationsTotal[0],
                    $dslamLabor['ipaddress'],
                    $dslamLabor['port']
                );
                ?>
            </a>
        </div>
        <?php
        }   
    }
  ?>

    <h1>&nbsp;&nbsp;&nbsp;&nbsp;DSLAM globale Einstellungen</h1>

    <?php

    $debug = false;

    $Dslam_comando = isset($_GET['kommando']) ? $_GET['kommando'] : '';
    
    $location_id = isset($_GET['location_id']) ? $_GET['location_id'] : '';
    $PPPoE = isset($_GET['kommando1']) ? $_GET['kommando1'] : '';
    $Dslam_comando2 = isset($_GET['kommando2']) ? $_GET['kommando2'] : '';
    $Dslam_comando3 = isset($_GET['kommando3']) ? $_GET['kommando3'] : '';
    $Dslam_comando4 = isset($_GET['kommando4']) ? $_GET['kommando4'] : null;
    $Dslam_comando5 = isset($_GET['kommando5']) ? $_GET['kommando5'] : '';
    $Dslam_SWcomando = isset($_GET['SWkommando']) ? $_GET['SWkommando'] : '';
    $Dslam_SWcomando2 = isset($_GET['SWkommando2']) ? $_GET['SWkommando2'] : '';
    $Dslam_SWcomando3 = isset($_GET['SWkommando3']) ? $_GET['SWkommando3'] : '';
    $Dslam_SWcomando4 = isset($_GET['SWkommando4']) ? $_GET['SWkommando4'] : null;
    $Dslam_SWcomando5 = isset($_GET['SWkommando5']) ? $_GET['SWkommando5'] : '';
    $Dslam_SW1comando = isset($_GET['SW1kommando']) ? $_GET['SW1kommando'] : '';
    $Dslam_SW1comando2 = isset($_GET['SW1kommando2']) ? $_GET['SW1kommando2'] : '';
    $Dslam_SW1comando3 = isset($_GET['SW1kommando3']) ? $_GET['SW1kommando3'] : '';
    $Dslam_SW1comando4 = isset($_GET['SW1kommando4']) ? $_GET['SW1kommando4'] : null;
    $Dslam_SW1comando5 = isset($_GET['SW1kommando5']) ? $_GET['SW1kommando5'] : '';
    $Dslam_SW2comando = isset($_GET['SW2kommando']) ? $_GET['SW2kommando'] : '';
    $Dslam_SW2comando2 = isset($_GET['SW2kommando2']) ? $_GET['SW2kommando2'] : '';
    $Dslam_SW2comando3 = isset($_GET['SW2kommando3']) ? $_GET['SW2kommando3'] : '';
    $Dslam_SW2comando4 = isset($_GET['SW2kommando4']) ? $_GET['SW2kommando4'] : null;
    $Dslam_SW2comando5 = isset($_GET['SW2kommando5']) ? $_GET['SW2kommando5'] : '';
    $Dslam_SW3comando = isset($_GET['SW3kommando']) ? $_GET['SW3kommando'] : '';
    $Dslam_SW3comando2 = isset($_GET['SW3kommando2']) ? $_GET['SW3kommando2'] : '';
    $Dslam_SW3comando3 = isset($_GET['SW3kommando3']) ? $_GET['SW3kommando3'] : '';
    $Dslam_SW3comando4 = isset($_GET['SW3kommando4']) ? $_GET['SW3kommando4'] : null;
    $Dslam_SW3comando5 = isset($_GET['SW3kommando5']) ? $_GET['SW3kommando5'] : '';
    $Dslam_SW4comando = isset($_GET['SW4kommando']) ? $_GET['SW4kommando'] : '';
    $Dslam_SW4comando2 = isset($_GET['SW4kommando2']) ? $_GET['SW4kommando2'] : '';
    $Dslam_SW4comando3 = isset($_GET['SW4kommando3']) ? $_GET['SW4kommando3'] : '';
    $Dslam_SW4comando4 = isset($_GET['SW4kommando4']) ? $_GET['SW4kommando4'] : null;
    $Dslam_SW4comando5 = isset($_GET['SW4kommando5']) ? $_GET['SW4kommando5'] : '';
    $Dslam_SW5comando = isset($_GET['SW5kommando']) ? $_GET['SW5kommando'] : '';
    $Dslam_SW5comando2 = isset($_GET['SW5kommando2']) ? $_GET['SW5kommando2'] : '';
    $Dslam_SW5comando3 = isset($_GET['SW5kommando3']) ? $_GET['SW5kommando3'] : '';
    $Dslam_SW5comando4 = isset($_GET['SW5kommando4']) ? $_GET['SW5kommando4'] : null;
    $Dslam_SW5comando5 = isset($_GET['SW5kommando5']) ? $_GET['SW5kommando5'] : '';
    $Dslam_SW6comando = isset($_GET['SW6kommando']) ? $_GET['SW6kommando'] : '';
    $Dslam_SW6comando2 = isset($_GET['SW6kommando2']) ? $_GET['SW6kommando2'] : '';
    $Dslam_SW6comando3 = isset($_GET['SW6kommando3']) ? $_GET['SW6kommando3'] : '';
    $Dslam_SW6comando4 = isset($_GET['SW6kommando4']) ? $_GET['SW6kommando4'] : null;
    $Dslam_SW6comando5 = isset($_GET['SW6kommando5']) ? $_GET['SW6kommando5'] : '';


    $id = isset($_GET['id']) ? $_GET['id'] : '';
    $client_id = '';

    ?>

    <h3>&nbsp;&nbsp;&nbsp;&nbsp;DSLAM Standorte <span class='red'>rote Komandos veraendern Werte im DSLAM !!!</span></h3>

    <div>
        <p>
            <span class="text blue">
                <form method="get" action="">
                    &nbsp;&nbsp;&nbsp;&nbsp; DSLAM:&nbsp;&nbsp;

                    <?php

                    $query = $db->query($sql['allLocations']);
                    $statement = $db->prepare($sql['allLocations']);
                    $statement->execute();
                    $locationsResult = $statement->get_result();
                    $sortedLocations = [];

                    while ($location = $locationsResult->fetch_assoc()) {
                        if (!isset($sortedLocations[$location['network_id']])) {
                            $sortedLocations[$location['network_id']] = [
                                'name' => $location['network_name'],
                                'locations' => [],
                            ];
                        }

                        $sortedLocations[$location['network_id']]['locations'][] = $location;
                    }

                    $locationsResult->close();
                    $statement->close();

                    echo '<select name="location_id">';

                    switch (\Wisotel\Configuration\Configuration::get('companyName')) {
                        case 'ZEAG':
                            $dslamIpOffset = 9;
                            echo '<option value="">DSLAM auswählen</option>'
                                .'<option disabled="disabled"></option>'
                                .'<option value="all">all DSLAMs</option>'
                                .'<option value="Kunde1">Kunde 79000.16.0001_5 Tests</option>'
                                .'<option disabled="disabled"></option>';

                            break;

                        case 'WiSoTEL':
                            $dslamIpOffset = 1;
                            echo '<option value="">DSLAM auswählen</option>'
                                .'<option disabled="disabled"></option>'
                                .'<option value="all">all DSLAMs</option>'
                                .'<option value="Kunde1">Kunde 79000.16.0001_5 Tests</option>'
                                .'<option disabled="disabled"></option>';

                            break;
                    }

                    foreach ($sortedLocations as $networkId => $array) {
                        echo '<optgroup label="'.$array['name'].'">';

                        foreach ($array['locations'] as $location) {
                            echo '<option value="'.$location['id'].'">'.$location['name'].'</option>';
                        }

                        echo '</optgroup>';
                    }

                    echo '</select><br />';

                    ?>

                    &nbsp;&nbsp;&nbsp;&nbsp;<h3>&nbsp;&nbsp;&nbsp;&nbsp;Software Aktionen Kommandos</h3>
                    <br /><br />

                  &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="" checked> Leer 0
                   &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="SW1kommando" value="" checked> Leer 1
                   &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="SW2kommando" value="" checked> Leer 2
                   &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="SW3kommando" value="" checked> Leer 3
                   &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="SW4kommando" value="" checked> Leer 4
                   &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="SW5kommando" value="" checked> Leer 5
                   &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="SW6kommando" value="" checked> Leer 6

                   <br><br>

                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="SW4kommando" value="Software"><span class='red'>Software Aktionen&nbsp;&nbsp;</span>
                     &nbsp;&nbsp;&nbsp;<form method="get" action="">
                    &nbsp;&nbsp;&nbsp;&nbsp; <span class='red'>Upgrade:&nbsp;&nbsp;</span>
                         <select name="SW4kommando2">
                          <option value="">Software auswählen</option>
                          <option disabled="disabled"></option>
                          <option value="show software-mngt oswp">Zeige OSWP an</option>
                          <option value="show software-mngt swp-disk-file">Zeige swp-disk-file an</option>
                          <option value="show equipment ont sw-version">Zeige ont sw-information an</option>
                          <option value="admin software-mngt oswp 1 download L6GPAA58.474">SW L6GQAA58.474 wie ZEAG oswp 1</option>
                          <option value="admin software-mngt oswp 2 download L6GPAA58.474">SW L6GQAA58.474 wie ZEAG oswp 2</option>
                          <option value="admin software-mngt oswp 1 download L6GPAA56.452">SW L6GQAA56.452 next Release oswp 1</option>
                          <option value="admin software-mngt oswp 2 download L6GPAA56.452">SW L6GQAA56.452 next Release oswp 2</option>
                          <option value="admin software-mngt database download 195.178.0.114">download Backup SW ?</option>
                          <option value="admin software-mngt oswp">activate comit / abort load / oswp 1/2</option>
                          <option disabled="disabled"></option>
                          </optgroup>
                          </select>

                            OSWP 1 / 2?&nbsp;&nbsp;<input type="text" name="SWkommando3" value="1">
                         <select name="SW4kommando4">
                          <option value="">TFTPoderBackup auswählen</option>
                          <option disabled="disabled"></option>
                          <option value="195.178.0.114">TFTP Adresse 195.178.0.114</option>
                          <option value="dm_complete_NANT_D_R56.tar">Lade NANT-D default R56</option>
                          <option value="dm_complete_NANT_E_R56.tar">Lade NANT-E default R56</option>
                          <option disabled="disabled"></option>
                          </optgroup>
                          </select>


                         Aktion <select name="SW4kommando5">
                          <option value="">Optionen auswählen</option>
                          <option disabled="disabled"></option>
                          <option value="abort-download">Download abbrechen</option>
                          <option value="activate with-linked-db">activate mit geladener Datenbank</option>
                          <option value="commit">OSWP wird commited</option>
                          <option disabled="disabled"></option>
                          </optgroup>
                          </select>

                        &nbsp;&nbsp;<br><br>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value="execute" class="fancy-button outline round red">
                    <h3>&nbsp;&nbsp;&nbsp;&nbsp;DSLAM Kommandos lesen</h3>

                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="Vectoring">Vectoring&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DPBO"> DPBO&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="ShowSoftware"> Software show&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_Service"> Service&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_VLAN"> VLAN&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_Diagnose_SFP"> SFP Diag&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_Line_up"> Lines up&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_Line_upsum">Lines up Sum&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_show_Port"> show port&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DPBOshow"> DPBO show&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="SNTP"> SNTP&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="SNMP"> SNMP&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="IPTV"> IP-TV Information&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="IGMP"> IGMP Channel Information&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="EQ_Slot"> EQ Slot &nbsp;&nbsp;<br><br>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="EQ_Slots"> EQ Slot short &nbsp;&nbsp;<br><br>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_sSecurity_show"> System security show&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_bridge_port">show Bridge&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_Backup_show">show Backup&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_QOS">show Bandbreiten Profile&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="Spectrum_anzeigen">Spectrum Profile setzen&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="ShowOntOptics">GPON ONT Info&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="LAG_show">LAG Info&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="ShowPonOptics">PON Optics Info&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="Testx">logon Versuch&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="Test">Test logon&nbsp;&nbsp;<br><br>

                    <h3>&nbsp;&nbsp;&nbsp;&nbsp;Test Kunde</h3>

                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_Kunden_einrichten"><span class='red'>Test Kunde einrichten vdsl&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_Kunden_Status">Test Kunde Status &nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_Kunden_Status_maximal">Test Kunde Status max&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_Kunden_Config">Test Kunde Info&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_Kunden_Loeschen"><span class='red'>Test Kunde loeschen&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="Experte">Experte&nbsp;&nbsp;

                    <h3>&nbsp;&nbsp;&nbsp;&nbsp;DSLAM Kommandos schreiben</h3>

                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DPBO_setup"><span class='red'>setze 5 default DPBO Profile&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;Anzahl Profile<input type="text" name="kommando4" value="5">&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DPBO_set"><span class='red'> DPBO ESEL Werte setzen&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="SNTPset"><span class='red'> SNTP set&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="SNMPset"><span class='red'> SNMP set&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="IPTV_setup"><span class='red'> IGMP setup&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="MAXspeed"> <span class='red'>set 10 GB LT&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_Vectorinit"><span class='red'> set Vectoring&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="aging_time"><span class='red'> set agin timeg&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_Bandwith_set"><span class='red'> setze alle QOS Profile&nbsp;&nbsp;</span><br><br>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_Vlan_set"><span class='red'> VLAN set&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_Service_set"><span class='red'> Service set&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_Service_modify"><span class='red'> Service modify&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_sSecurity_set"> <span class='red'>System security set&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_extendet_slot"><span class='red'>set extendet LT Slots&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_slot_numm"><span class='red'>set LT Slots&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="Backup"><span class='red'>Backup to TFTP&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="LAG_setup"><span class='red'>set LAG&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_oswp_set"><span class='red'>set tftp server&nbsp;&nbsp;<br><br></span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DPBO_setx"><span class='red'> DPBO ESEL Wert setzen im Test Profile 4&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="DSLAM_Bandwith_setnew"><span class='red'> neue Produkt Profile&nbsp;&nbsp;</span>

                    &nbsp;&nbsp;&nbsp;&nbsp;ESEL * 10 z.B: 80,0 ist 800 im DSLAM:
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="kommando2" value="800">&nbsp;&nbsp;<br><br>

                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="IP_setzen"><span class='red'>setze IP Adresse&nbsp;&nbsp;aenndern zu x.x.x.Y: Wert =
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="kommando3" value="254"></span> setzte letzte Stelle IP Adresse von 10 bis 254 default Adresse in jedem Netz ist 254 xx-99_MA1 &nbsp;&nbsp;<br><br>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="SW5kommando" value="Vlan_set"><span class='red'>setup VLAN&nbsp;&nbsp;Vlan zu xvz: Wert =
                    </span> VLAN 200?&nbsp;&nbsp;<input type="text" name="SW5kommando2" value="">
                    VLAN Name?&nbsp;&nbsp;<input type="text" name="SW5kommando3" value=""></span><br><br>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="SW6kommando" value="Vlan_delete"><span class='red'>delete VLAN&nbsp;&nbsp;Vlan zu xvz: Wert =
                    </span> VLAN 200?&nbsp;&nbsp;<input type="text" name="SW6kommando2" value=""><br><br>


                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="SW2kommando" value="CardCommit"><span class='red'>Karte einrichten&nbsp;&nbsp;: Karte = </span>
                     ?&nbsp;&nbsp;&nbsp;<input type="text" name="SW2kommando2" value="1">
                     Card =    <select name="SW2kommando3">
                         <option disabled="disabled"></option>

                         <option value="lt:1/1/">lt:1/1/</option>

                         <option disabled="disabled"></option>
                          </optgroup>
                          </select>



                    Type : ?&nbsp;&nbsp;<select name="SW2kommando4">
                          <option disabled="disabled"></option>
                          <option value="nelt-b">Ethernet</option>
                          <option value="nglt-a">Gpon</option>
                          <option disabled="disabled"></option>
                          </optgroup>
                          </select>

                    </span><br><br>

                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="SW1kommando" value="Service_create"><span class='red'>create service&nbsp;&nbsp;Vlan zu serice xfp ?: Wert = </span>
                    servive VLAN ?&nbsp;&nbsp;&nbsp;<input type="text" name="SW1kommando2" value="">
                     SFP / LAG =    <select name="SW1kommando3">
                         <option disabled="disabled"></option>
                         <option value='No'>empty</option>
                         <option value="nt-a:xfp:1">nt-a:xfp:1:</option>
                         <option value="nt-a:xfp:2">nt-a:xfp:2:</option>
                         <option value="nt-a:xfp:3">nt-a:xfp:3:</option>
                         <option value="lag-1:">lag-1:</option>
                         <option value="nt-a:sfp:1">nt-a:sfp:1:</option>
                         <option value="nt-a:sfp:2">nt-a:sfp:2:</option>
                         <option value="nt-a:sfp:3">nt-a:sfp:3:</option>
                         <option disabled="disabled"></option>
                          </optgroup>
                          </select>



                    customer?&nbsp;&nbsp;<select name="SW1kommando4">
                          <option disabled="disabled"></option>
                          <option value="1">customer 1</option>
                          <option value="10">customer 10</option>
                          <option disabled="disabled"></option>
                          </optgroup>
                          </select>

                    Karte lt:?&nbsp;&nbsp;<input type="text" name="SW1kommando5" value=""></span><br><br>


                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="SW3kommando" value="Service_delete"><span class='red'>delete service&nbsp;&nbsp;Vlan zu serice xfp ?: Wert = </span>
                    servive VLAN ?&nbsp;&nbsp;&nbsp;<input type="text" name="SW3kommando2" value="">
                     SFP / LAG =    <select name="SW3kommando3">
                         <option disabled="disabled"></option>
                         <option value='No'>empty</option>
                         <option value="nt-a:xfp:1">nt-a:xfp:1:</option>
                         <option value="nt-a:xfp:2">nt-a:xfp:2:</option>
                         <option value="nt-a:xfp:3">nt-a:xfp:3:</option>
                         <option value="lag-1:">lag-1:</option>
                         <option value="nt-a:sfp:1">nt-a:sfp:1:</option>
                         <option value="nt-a:sfp:2">nt-a:sfp:2:</option>
                         <option value="nt-a:sfp:3">nt-a:sfp:3:</option>
                         <option disabled="disabled"></option>
                          </optgroup>
                          </select>



                    customer?&nbsp;&nbsp;<select name="SW3kommando4">
                          <option disabled="disabled"></option>
                          <option value="1">customer 1</option>
                          <option value="10">customer 10</option>
                          <option disabled="disabled"></option>
                          </optgroup>
                          </select>

                    Karte lt:?&nbsp;&nbsp;<input type="text" name="SW3kommando5" value=""></span><br><br>

                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="SWkommando" value="VPLS_delete"><span class='red'>delete Vlan VPLS&nbsp;&nbsp;Vlan: Wert = </span>
                    servive VLAN ?&nbsp;&nbsp;&nbsp;<input type="text" name="SWkommando2" value="">
                    customer?&nbsp;&nbsp;<select name="SWkommando3">
                          <option disabled="disabled"></option>
                          <option value="1">customer 1</option>
                          <option value="10">customer 10</option>
                          <option disabled="disabled"></option>
                          </optgroup>
                          </select>
                    <br><br>

                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="VPLS_create"><span class='red'>create Vlan VPLS&nbsp;&nbsp;Vlan: Wert = </span>
                    servive VLAN ?&nbsp;&nbsp;&nbsp;<input type="text" name="kommando2" value="">
                    customer?&nbsp;&nbsp;<select name="kommando3">
                          <option disabled="disabled"></option>
                          <option value="1">customer 1</option>
                          <option value="10">customer 10</option>
                          <option disabled="disabled"></option>
                          </optgroup>
                          </select>
                    <br><br>



                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="Spectrum_setzen"><span class='red'>Spectrum Profile setzen&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="Backupladen"><span class='red'>Backup laden vom TFTP&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="Backupaktivieren1"><span class='red'>Backup aktivieren mit OSWP1&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="Backupaktivieren2"><span class='red'>Backup aktivieren mit OSWP2&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="reboot1"><span class='red'>Reboot DSLAM no self Test&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="reboot2"><span class='red'>Reboot DSLAM with self-test&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="first_setup"><span class='red'>Setup DSLAM voll&nbsp;&nbsp;</span><br><br>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="alle_aktiven_einrichten"><span class='red'>Alle aktiven_Kunden_dieser Location einrichten&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="alle_aktiven_vectoring_an"><span class='red'>Alle aktiven_Kunden_dieser Location Vectoring an&nbsp;&nbsp;</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando" value="alle_aktiven_vectoring_aus"><span class='red'>Alle aktiven_Kunden_dieser Location Vectoring aus&nbsp;&nbsp;</span><br><br>





                    &nbsp;&nbsp;&nbsp;&nbsp;<h3>&nbsp;&nbsp;&nbsp;&nbsp;PPPoE Kommandos</h3>

                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando1" value=""> Leer&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando1" value="PPPoE" />PPPoE aktive Sessions&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando1" value="biberach-kirchhausen-cisco-show-interfaces" />Biberach Kirchhausen CISCO - show Interfaces&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando1" value="gemmrigheim-cisco-show-interfaces" />Gemmrigheim CISCO - show Interfaces
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="kommando1" value="show_ping_errors" />Ping errors
                    <br /><br />

                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value="execute" class="fancy-button outline round red">

                    <?php
                    switch (\Wisotel\Configuration\Configuration::get('companyName')) {
                        case 'ZEAG':
                            ?>

                            <a class="fancy-button outline round blue" href="<?php echo "http://10.20.6.11:8303/index.php" ?>">&nbsp; ZEAGKVdev </a>&nbsp;&nbsp;
                            <a class="fancy-button outline round blue" href="<?php echo "http://10.20.6.7:8303/index.php" ?>">&nbsp; ZEAGKV </a>&nbsp;&nbsp;

                            <?php

                            break;

                        case 'WiSoTEL':
                            ?>

                            <a class="fancy-button outline round blue" href="<?php echo "http://72.20.2.221/wkv2/web/index.php" ?>">&nbsp; WKV2KVdev </a>&nbsp;&nbsp;
                            <a class="fancy-button outline round blue" href="<?php echo "http://72.20.2.223/wkv2dev/web/index.php" ?>">&nbsp; WKV2 </a>&nbsp;&nbsp;

                            <?php

                            break;
                    }
                    ?>
                </form>
            </span>
        </p>
    </div>

    <?php

    $backup_JN = true;

    if ($Dslam_SWcomando != "") {
        $Dslam_comando = $Dslam_SWcomando;
        $Dslam_comando1 = $Dslam_SWcomando1;
        $Dslam_comando2 = $Dslam_SWcomando2;
        $Dslam_comando3 = $Dslam_SWcomando3;
        $Dslam_comando4 = $Dslam_SWcomando4;
        $Dslam_comando5 = $Dslam_SWcomando5;

    }

    if ($Dslam_SW1comando != "") {
        $Dslam_comando = $Dslam_SW1comando;
        $Dslam_comando1 = $Dslam_SW1comando1;
        $Dslam_comando2 = $Dslam_SW1comando2;
        $Dslam_comando3 = $Dslam_SW1comando3;
        $Dslam_comando4 = $Dslam_SW1comando4;
        $Dslam_comando5 = $Dslam_SW1comando5;

    }
    if ($Dslam_SW2comando != "") {
        $Dslam_comando = $Dslam_SW2comando;
        $Dslam_comando1 = $Dslam_SW2comando1;
        $Dslam_comando2 = $Dslam_SW2comando2;
        $Dslam_comando3 = $Dslam_SW2comando3;
        $Dslam_comando4 = $Dslam_SW2comando4;
        $Dslam_comando5 = $Dslam_SW2comando5;

    }
    if ($Dslam_SW3comando != "") {
        $Dslam_comando = $Dslam_SW3comando;
        $Dslam_comando1 = $Dslam_SW3comando1;
        $Dslam_comando2 = $Dslam_SW3comando2;
        $Dslam_comando3 = $Dslam_SW3comando3;
        $Dslam_comando4 = $Dslam_SW3comando4;
        $Dslam_comando5 = $Dslam_SW3comando5;

    }
    if ($Dslam_SW4comando != "") {
        $Dslam_comando = $Dslam_SW4comando;
        $Dslam_comando1 = $Dslam_SW4comando1;
        $Dslam_comando2 = $Dslam_SW4comando2;
        $Dslam_comando3 = $Dslam_SW4comando3;
        $Dslam_comando4 = $Dslam_SW4comando4;
        $Dslam_comando5 = $Dslam_SW4comando5;

    }
    if ($Dslam_SW5comando != "") {
        $Dslam_comando = $Dslam_SW5comando;
        $Dslam_comando1 = $Dslam_SW5comando1;
        $Dslam_comando2 = $Dslam_SW5comando2;
        $Dslam_comando3 = $Dslam_SW5comando3;
        $Dslam_comando4 = $Dslam_SW5comando4;
        $Dslam_comando5 = $Dslam_SW5comando5;

    }
    if ($Dslam_SW6comando != "") {
        $Dslam_comando = $Dslam_SW6comando;
        $Dslam_comando1 = $Dslam_SW6comando1;
        $Dslam_comando2 = $Dslam_SW6comando2;
        $Dslam_comando3 = $Dslam_SW6comando3;
        $Dslam_comando4 = $Dslam_SW6comando4;
        $Dslam_comando5 = $Dslam_SW6comando5;

    }

        if (empty ($Dslam_comando1)) $Dslam_comando1 = "empty";
        if (empty ($Dslam_comando2)) $Dslam_comando2 = "empty";
        if (empty ($Dslam_comando3)) $Dslam_comando3 = "empty";
        if (empty ($Dslam_comando4)) $Dslam_comando4 = "empty";
        if (empty ($Dslam_comando5)) $Dslam_comando5 = "empty";

        $container = new ContainerBuilder();
        $container->set('Dslam_comando1', $Dslam_comando1);
        $container->set('Dslam_comando2', $Dslam_comando2);
        $container->set('Dslam_comando3', $Dslam_comando3);
        $container->set('Dslam_comando4', $Dslam_comando4);
        $container->set('Dslam_comando5', $Dslam_comando5);
        $container->set('backup_JN', $backup_JN);

        if ($location_id != 'Kunde1' && !empty($location_id)) {

            $locationObject = $this->getDoctrine()->getRepository(Location::class)->findOneById($location_id);
            $location = $locationObject;
            $dslamCommunicator = $this->get('dslam_chooser')->findCommunicationClass(
            $locationObject->getType(), $locationObject->getProcessor(), $locationObject->getCurrentSoftwareRelease());


            // create a service container for dependency injection used for communication classes

            $cardObjects = $this->getDoctrine()->getRepository(Card::class)->findByLocation($locationObject);
            // = $this->getDoctrine()->getRepository(Card::class)->findOneById($cardObject->getLocation());
            $lsa_kvz_partitions = $this->getDoctrine()->getRepository(LsaKvz::class)->findByLocation($locationObject);



            // add some services to the container
            $container->set(Location::class, $locationObject);
            //$container->set(Customer::class, new Customer());
            $container->set('cards', $cardObjects);
            $container->set('lsa_kvz_partitions', $lsa_kvz_partitions);
            //$container->set(MainProduct::class, $customer['Service']);
            // set service container to dslam communicator for dependency injection
            $dslamCommunicator->setContainer($container);

            // set connection gateway to dslam communicator
            $dslamCommunicator->setGateway(new SshGateway($location->getIpAddress(), $location->getLogonPort(), 10));

            // perform the login
            $dslamCommunicator->login(['username' => $location->getLogonUsername(), 'password' => $location->getLogonPassword()]);





        } elseif ($location_id == 'Kunde1') {
            $id = 3281;

            $backup_JN = 'no';
            $activeCustomersQuery = $db->query("SELECT *
                FROM `customers`
                WHERE `id` = ".$id."");
                $customer = $activeCustomersQuery->fetch_assoc();

            //######################################
            // SETUP CONTAINER FÜR CUSTOMER aufraufe
            // load product-object for Service
            $customer['Service'] = $this->getDoctrine()->getRepository('AppBundle\Entity\Product\MainProduct')->
                findOneByIdentifier($customer['Service']);

            if (!($customer['Service'] instanceof \AppBundle\Entity\Product\MainProduct)) {
                $customer['Service'] = new \AppBundle\Entity\Product\MainProduct();
            }

            // load card-object for card_id
            $customer['card_id'] = $this->getDoctrine()->getRepository('AppBundle\Entity\Location\Card')->
                findOneById($customer['card_id']);

            if (!($customer['card_id'] instanceof \AppBundle\Entity\Location\Card)) {
                $customer['card_id'] = new \AppBundle\Entity\Location\Card();
            }

            $customerObject = Customer::createFromArray($customer, $this->getDoctrine());

            $cardObject = $customerObject->getCardId();


            // create instance of AppBundle\Entity\Location
            $location = $this->getDoctrine()->getRepository(Location::class)->findOneById($customerObject->getLocationId());
            $location_id = $location->getId();

            $dslamCommunicator = $this->get('dslam_chooser')->findCommunicationClass(
            $location->getType(), $location->getProcessor(), $location->getCurrentSoftwareRelease());

            // add some services to the container
            $container->set(Customer::class, $customerObject);
            $container->set(Location::class, $location);
            $container->set(Card::class, $cardObject);
            $container->set(MainProduct::class, $customer['Service']);

            // set service container to dslam communicator for dependency injection
            $dslamCommunicator->setContainer($container);

            // set connection gateway to dslam communicator
            $dslamCommunicator->setGateway(new SshGateway($location->getIpAddress(), $location->getLogonPort(), 10));

            // perform the login
            $dslamCommunicator->login(['username' => $location->getLogonUsername(), 'password' => $location->getLogonPassword()]);
            //####################
            //# Ende Setup Container für Customer aufrufe
            //#
            //$container->set('backup_JN', $backup_JN);
            echo sprintf('<pre>%s</pre>', $dslamCommunicator->DSLAM_Kunden_Einrichten());
            $id = '';
            $Dslam_comando = '';
            $Dslam_SWcomando = '';


        }



                                    /* var_dump ($cardObjects);
                                     echo "lsa <br> ";
                                     var_dump ($lsa_kvz_partitions );
                                     echo "lsa <br> ";
                                     //var_dump ($cardObject1 );
                                     exit;
                                     */

        /*
             if ($id != '' and $Dslam_comando != "") {
                 //DSLAM_exec ($Dslam_comando,'',$id);
                 #echo "<br> ID = $id Kommando =  $Dslam_comando<br>";
                $backup_JN = false;
                $customerObject = Customer::createFromArray($id, $this->getDoctrine());
                $container->set(Customer::class, new Customer());
                $container->set('backup_JN', $backup_JN);

                echo sprintf('<pre>%s</pre>', $dslamCommunicator->DSLAM_Kunden_Einrichten());
                 $id = '';
                 $Dslam_comando = '';
                 $Dslam_SWcomando = '';
             }
             */
    if ($Dslam_comando != "" and $location_id != '' and $PPPoE == '') {
        switch ($location_id) {
            case 'all':
                $allConfigurableLocationsQuery = $db->query("SELECT `id` FROM `locations` WHERE `dslam_conf` = 1");

                echo sprintf("%d Locations werden verarbeitet\n",
                    $allConfigurableLocationsQuery->num_rows
                );

                $container = new ContainerBuilder();
                $container->set(Card::class, $cardObject);
                $container->set(MainProduct::class, $customer['Service']);
                //$container->set('Dslam_comando1', $Dslam_comando1);
                $container->set('Dslam_comando2', $Dslam_comando2);
                $container->set('Dslam_comando3', $Dslam_comando3);
                if (empty ($Dslam_comando4)) $Dslam_comando4 = "A";
                $container->set('Dslam_comando4', $Dslam_comando4);
                $container->set('Dslam_comando5', $Dslam_comando5);

                while ($location = $allConfigurableLocationsQuery->fetch_assoc()) {
                    $locationObject = $this->getDoctrine()->getRepository(Location::class)->findOneById($location['id']);

                    $dslamCommunicator = $this->get('dslam_chooser')->findCommunicationClass(
                        $locationObject->getType(), $locationObject->getProcessor(), $locationObject->getCurrentSoftwareRelease()
                    );

                    if (null === $dslamCommunicator) {
                        // no communicator was found, try old way
                        DSLAM_exec($Dslam_comando, $location['id'], '', $Dslam_comando2, $Dslam_comando3, $Dslam_comando4, $Dslam_comando5);
                    } else {
                        // set service container to dslam communicator for dependency injection
                        $dslamCommunicator->setContainer($container);

                        // set connection gateway to dslam communicator
                        $dslamCommunicator->setGateway(new SshGateway($locationObject->getIpAddress(), $locationObject->getLogonPort(), 10));

                        // perform the login
                        $dslamCommunicator->login(['username' => $locationObject->getLogonUsername(), 'password' => $locationObject->getLogonPassword()]);

                        // Example: Calling a method directly (in this case, dependency injection was used for the required arguments)
                        echo sprintf('<pre>%s</pre>', $dslamCommunicator->$Dslam_comando($locationObject));
                    }
                }

                /*DSLAM_exec ($Dslam_comando,'9','');
                DSLAM_exec ($Dslam_comando,'10','');
                DSLAM_exec ($Dslam_comando,'11','');
                DSLAM_exec ($Dslam_comando,'12','');
                DSLAM_exec ($Dslam_comando,'13','');
                DSLAM_exec ($Dslam_comando,'14','');
                DSLAM_exec ($Dslam_comando,'19','');
                DSLAM_exec ($Dslam_comando,'20','');
                DSLAM_exec ($Dslam_comando,'21','');
                DSLAM_exec ($Dslam_comando,'22','');
                DSLAM_exec ($Dslam_comando,'23','');
                DSLAM_exec ($Dslam_comando,'24','');
                DSLAM_exec ($Dslam_comando,'25','');
                DSLAM_exec ($Dslam_comando,'33','');
                DSLAM_exec ($Dslam_comando,'34','');
                DSLAM_exec ($Dslam_comando,'35','');
                DSLAM_exec ($Dslam_comando,'36','');
                DSLAM_exec ($Dslam_comando,'37','');
                DSLAM_exec ($Dslam_comando,'38','');
                DSLAM_exec ($Dslam_comando,'39','');
                DSLAM_exec ($Dslam_comando,'40','');
                DSLAM_exec ($Dslam_comando,'41','');
                DSLAM_exec ($Dslam_comando,'44','');
                DSLAM_exec ($Dslam_comando,'32','');
                DSLAM_exec ($Dslam_comando,'33','');
                DSLAM_exec ($Dslam_comando,'45','');
                DSLAM_exec ($Dslam_comando,'46','');
                DSLAM_exec ($Dslam_comando,'47','');
                DSLAM_exec ($Dslam_comando,'48','');
                DSLAM_exec ($Dslam_comando,'49','');
                DSLAM_exec ($Dslam_comando,'50','');
                DSLAM_exec ($Dslam_comando,'51','');
                DSLAM_exec ($Dslam_comando,'52','');
                DSLAM_exec ($Dslam_comando,'53','');
                DSLAM_exec ($Dslam_comando,'54','');
                DSLAM_exec ($Dslam_comando,'55','');*/

                //DSLAM_exec ($Dslam_comando,'',''); loc 26 31 Labor Back und S?dbahnhof

                break;

            case 'allBIB':
                DSLAM_exec ($Dslam_comando,'9','');
                DSLAM_exec ($Dslam_comando,'10','');
                DSLAM_exec ($Dslam_comando,'11','');
                DSLAM_exec ($Dslam_comando,'12','');
                DSLAM_exec ($Dslam_comando,'13','');
                DSLAM_exec ($Dslam_comando,'14','');
                DSLAM_exec ($Dslam_comando,'19','');
                DSLAM_exec ($Dslam_comando,'20','');
                DSLAM_exec ($Dslam_comando,'21','');
                DSLAM_exec ($Dslam_comando,'22','');
                DSLAM_exec ($Dslam_comando,'23','');
                DSLAM_exec ($Dslam_comando,'24','');
                DSLAM_exec ($Dslam_comando,'25','');

                //DSLAM_exec ($Dslam_comando,'',''); loc 26 31 Labor Back und S?dbahnhof

                break;

            case 'allKHS':
                DSLAM_exec ($Dslam_comando,'34','');
                DSLAM_exec ($Dslam_comando,'35','');
                DSLAM_exec ($Dslam_comando,'36','');
                DSLAM_exec ($Dslam_comando,'37','');
                DSLAM_exec ($Dslam_comando,'38','');
                DSLAM_exec ($Dslam_comando,'39','');
                DSLAM_exec ($Dslam_comando,'40','');
                DSLAM_exec ($Dslam_comando,'41','');

                break;

            case 'allGEM':
                #$location_id="10";
                DSLAM_exec ($Dslam_comando,'32','');
                DSLAM_exec ($Dslam_comando,'33','');
                DSLAM_exec ($Dslam_comando,'45','');
                DSLAM_exec ($Dslam_comando,'46','');
                DSLAM_exec ($Dslam_comando,'47','');
                DSLAM_exec ($Dslam_comando,'48','');
                DSLAM_exec ($Dslam_comando,'49','');
                DSLAM_exec ($Dslam_comando,'50','');
                DSLAM_exec ($Dslam_comando,'51','');
                DSLAM_exec ($Dslam_comando,'52','');
                DSLAM_exec ($Dslam_comando,'53','');
                DSLAM_exec ($Dslam_comando,'54','');
                DSLAM_exec ($Dslam_comando,'55','');

                //DSLAM_exec ($Dslam_comando,'',''); loc 26 31 Labor Back und S?dbahnhof

                break;

            case 'allWID':
                #$location_id="10";
                DSLAM_exec ($Dslam_comando,'63','');
                DSLAM_exec ($Dslam_comando,'64','');
                DSLAM_exec ($Dslam_comando,'65','');
                DSLAM_exec ($Dslam_comando,'66','');
                DSLAM_exec ($Dslam_comando,'67','');
                DSLAM_exec ($Dslam_comando,'68','');
                DSLAM_exec ($Dslam_comando,'69','');

                break;

            case 'all_NELL':
                DSLAM_exec ($Dslam_comando,'28','');
                DSLAM_exec ($Dslam_comando,'33','');
                DSLAM_exec ($Dslam_comando,'34','');
                DSLAM_exec ($Dslam_comando,'35','');
                DSLAM_exec ($Dslam_comando,'36','');

                //DSLAM_exec ($Dslam_comando,'',''); loc 26 31 Labor Back und S?dbahnhof

                break;

            case 'all_NELL_einrichten':
                einrichten_kunden ('28',$db);
                einrichten_kunden ('33',$db);
                einrichten_kunden ('34',$db);
                einrichten_kunden ('35',$db);
                einrichten_kunden ('36',$db);

                break;

            case'Kunde1':
                switch (\Wisotel\Configuration\Configuration::get('companyName')) {
                    case 'ZEAG':
                        DSLAM_exec ($Dslam_comando,'','367');

                        break;

                    case 'WiSoTEL':
                        DSLAM_exec ($Dslam_comando,'','987');

                        break;
                }

                break;

            default:
                if ($Dslam_comando === 'alle_aktiven_einrichten') {
                    // find all active customers for selected location
                    $activeCustomersQuery = $db->query("SELECT *
                        FROM `customers`
                        WHERE `location_id` = ".$location_id."
                        AND `connection_activation_date` IS NOT NULL
                        AND `connection_activation_date` != ''
                        AND (`wisocontract_canceled_date` IS NULL OR `wisocontract_canceled_date` = '')
                    ");

                    echo sprintf("%d_Kunden_werden eingerichtet\n",
                        $activeCustomersQuery->num_rows
                    );

                    while ($customer = $activeCustomersQuery->fetch_assoc()) {
                        // execute 'DSLAM_Kunden_Einrichten' for each customer
                        //DSLAM_exec('DSLAM_Kunden_Einrichten', '', $customer['id'], $Dslam_comando2, $Dslam_comando3, $Dslam_comando4, $Dslam_comando5,'N');

                        $backup_JN = false;

                        //######################################
                        // SETUP CONTAINER FÜR CUSTOMER aufraufe
                        // load product-object for Service
                        $customer['Service'] = $this->getDoctrine()->getRepository('AppBundle\Entity\Product\MainProduct')->
                            findOneByIdentifier($customer['Service']);

                        if (!($customer['Service'] instanceof \AppBundle\Entity\Product\MainProduct)) {
                            $customer['Service'] = new \AppBundle\Entity\Product\MainProduct();
                        }

                        // load card-object for card_id
                        $customer['card_id'] = $this->getDoctrine()->getRepository('AppBundle\Entity\Location\Card')->
                            findOneById($customer['card_id']);

                        if (!($customer['card_id'] instanceof \AppBundle\Entity\Location\Card)) {
                            $customer['card_id'] = new \AppBundle\Entity\Location\Card();
                        }

                        $customerObject = Customer::createFromArray($customer, $this->getDoctrine());

                        $cardObject = $customerObject->getCardId();


                        // create instance of AppBundle\Entity\Location
                        $location = $this->getDoctrine()->getRepository(Location::class)->findOneById($customerObject->getLocationId());
                        $location_id = $location->getId();

                        $dslamCommunicator = $this->get('dslam_chooser')->findCommunicationClass(
                        $location->getType(), $location->getProcessor(), $location->getCurrentSoftwareRelease());

                        // add some services to the container
                        $container->set(Customer::class, $customerObject);
                        $container->set(Location::class, $location);
                        $container->set(Card::class, $cardObject);
                        $container->set(MainProduct::class, $customer['Service']);

                        // set service container to dslam communicator for dependency injection
                        $dslamCommunicator->setContainer($container);

                        // set connection gateway to dslam communicator
                        $dslamCommunicator->setGateway(new SshGateway($location->getIpAddress(), $location->getLogonPort(), 10));

                        // perform the login
                        $dslamCommunicator->login(['username' => $location->getLogonUsername(), 'password' => $location->getLogonPassword()]);
                        //####################
                        //# Ende Setup Container für Customer aufrufe
                        //#
                        //$container->set('backup_JN', $backup_JN);




                        echo sprintf('<pre>%s</pre>', $dslamCommunicator->DSLAM_Kunden_Einrichten());
                    }

                    echo 'Einrichten abgeschlossen';
                    $backup_JN = true;
                    $container->set('backup_JN', $backup_JN);

                    echo sprintf('<pre>%s</pre>', $dslamCommunicator->DSLAM_Kunden_Einrichten());
                    break;
                }

                if ($Dslam_comando === 'alle_aktiven_vectoring_aus') {
                    // find all active customers for selected location
                    $activeCustomersQuery = $db->query("SELECT `id`
                        FROM `customers`
                        WHERE `location_id` = ".$location_id."
                        AND `connection_activation_date` IS NOT NULL
                        AND `connection_activation_date` != ''
                        AND (`wisocontract_canceled_date` IS NULL OR `wisocontract_canceled_date` = '')
                    ");

                    echo sprintf("%d Vectoring für alle_Kunden_aus \n",
                        $activeCustomersQuery->num_rows
                    );

                    while ($customer = $activeCustomersQuery->fetch_assoc()) {
                        // execute 'DSLAM_Kunden_Einrichten' for each customer
                        //DSLAM_exec('DSLAM_Kunden_Vectoring_aus', '', $customer['id'], $Dslam_comando2, $Dslam_comando3, $Dslam_comando4, $Dslam_comando5,'N');

                        //######################################
                        // SETUP CONTAINER FÜR CUSTOMER aufraufe
                        // load product-object for Service
                        $customer['Service'] = $this->getDoctrine()->getRepository('AppBundle\Entity\Product\MainProduct')->
                            findOneByIdentifier($customer['Service']);

                        if (!($customer['Service'] instanceof \AppBundle\Entity\Product\MainProduct)) {
                            $customer['Service'] = new \AppBundle\Entity\Product\MainProduct();
                        }

                        // load card-object for card_id
                        $customer['card_id'] = $this->getDoctrine()->getRepository('AppBundle\Entity\Location\Card')->
                            findOneById($customer['card_id']);

                        if (!($customer['card_id'] instanceof \AppBundle\Entity\Location\Card)) {
                            $customer['card_id'] = new \AppBundle\Entity\Location\Card();
                        }

                        $customerObject = Customer::createFromArray($customer, $this->getDoctrine());

                        $cardObject = $customerObject->getCardId();


                        // create instance of AppBundle\Entity\Location
                        $location = $this->getDoctrine()->getRepository(Location::class)->findOneById($customerObject->getLocationId());
                        $location_id = $location->getId();

                        $dslamCommunicator = $this->get('dslam_chooser')->findCommunicationClass(
                        $location->getType(), $location->getProcessor(), $location->getCurrentSoftwareRelease());

                        // add some services to the container
                        $container->set(Customer::class, $customerObject);
                        $container->set(Location::class, $location);
                        $container->set(Card::class, $cardObject);
                        $container->set(MainProduct::class, $customer['Service']);

                        // set service container to dslam communicator for dependency injection
                        $dslamCommunicator->setContainer($container);

                        // set connection gateway to dslam communicator
                        $dslamCommunicator->setGateway(new SshGateway($location->getIpAddress(), $location->getLogonPort(), 10));

                        // perform the login
                        $dslamCommunicator->login(['username' => $location->getLogonUsername(), 'password' => $location->getLogonPassword()]);
                        //####################
                        //# Ende Setup Container für Customer aufrufe
                        //#
                        //$container->set('backup_JN', $backup_JN);


                        echo sprintf('<pre>%s</pre>', $dslamCommunicator->DSLAM_Kunden_Vectoring_aus());
                    }
                    echo 'Einrichten abgeschlossen';
                    break;
                }
                if ($Dslam_comando === 'alle_aktiven_vectoring_an') {
                    // find all active customers for selected location
                    $activeCustomersQuery = $db->query("SELECT `id`
                        FROM `customers`
                        WHERE `location_id` = ".$location_id."
                        AND `connection_activation_date` IS NOT NULL
                        AND `connection_activation_date` != ''
                        AND (`wisocontract_canceled_date` IS NULL OR `wisocontract_canceled_date` = '')
                    ");

                    echo sprintf("%d Vectoring für alle_Kunden_an\n",
                        $activeCustomersQuery->num_rows
                    );

                    while ($customer = $activeCustomersQuery->fetch_assoc()) {
                        // execute 'DSLAM_Kunden_Einrichten' for each customer
                        //DSLAM_exec('DSLAM_Kunden_Vectoring_an', '', $customer['id'], $Dslam_comando2, $Dslam_comando3, $Dslam_comando4, $Dslam_comando5,'N');

                        //######################################
                        // SETUP CONTAINER FÜR CUSTOMER aufraufe
                        // load product-object for Service
                        $customer['Service'] = $this->getDoctrine()->getRepository('AppBundle\Entity\Product\MainProduct')->
                            findOneByIdentifier($customer['Service']);

                        if (!($customer['Service'] instanceof \AppBundle\Entity\Product\MainProduct)) {
                            $customer['Service'] = new \AppBundle\Entity\Product\MainProduct();
                        }

                        // load card-object for card_id
                        $customer['card_id'] = $this->getDoctrine()->getRepository('AppBundle\Entity\Location\Card')->
                            findOneById($customer['card_id']);

                        if (!($customer['card_id'] instanceof \AppBundle\Entity\Location\Card)) {
                            $customer['card_id'] = new \AppBundle\Entity\Location\Card();
                        }

                        $customerObject = Customer::createFromArray($customer, $this->getDoctrine());

                        $cardObject = $customerObject->getCardId();


                        // create instance of AppBundle\Entity\Location
                        $location = $this->getDoctrine()->getRepository(Location::class)->findOneById($customerObject->getLocationId());
                        $location_id = $location->getId();

                        $dslamCommunicator = $this->get('dslam_chooser')->findCommunicationClass(
                        $location->getType(), $location->getProcessor(), $location->getCurrentSoftwareRelease());

                        // add some services to the container
                        $container->set(Customer::class, $customerObject);
                        $container->set(Location::class, $location);
                        $container->set(Card::class, $cardObject);
                        $container->set(MainProduct::class, $customer['Service']);

                        // set service container to dslam communicator for dependency injection
                        $dslamCommunicator->setContainer($container);

                        // set connection gateway to dslam communicator
                        $dslamCommunicator->setGateway(new SshGateway($location->getIpAddress(), $location->getLogonPort(), 10));

                        // perform the login
                        $dslamCommunicator->login(['username' => $location->getLogonUsername(), 'password' => $location->getLogonPassword()]);
                        //####################
                        //# Ende Setup Container für Customer aufrufe
                        //#
                        //$container->set('backup_JN', $backup_JN);

                        echo sprintf('<pre>%s</pre>', $dslamCommunicator->DSLAM_Kunden_Vectoring_an());
                    }
                    echo 'Einrichten abgeschlossen';
                    break;
                }


                if ($Dslam_comando != "IP_setzen" and $Dslam_comando != "first-setup") {
                        // Example: Calling a method directly (in this case, dependency injection was used for the required arguments)
                        echo sprintf('<pre>%s</pre>', $dslamCommunicator->$Dslam_comando());

                       switch ($Dslam_comando) {
                           case 'DSLAM_Kunden_Einrichten':
                           case 'DSLAM_Kunden_einrichten':
                           case 'DSLAM_Kunden_Bandbreite':
                           case 'DSLAM_Kunden_Loeschen1':
                           case 'DSLAM_Kunden_sperren':
                           case "DSLAM_Kunden_freischalten":
                           case 'IPTV_setup':
                           if ($dslam_backup == 'Y') {
                            //echo sprintf('<pre>%s</pre>', $dslamCommunicator->backup());
                           }
                           break;
                           }



                echo"Dslam_comando = $Dslam_comando / Location = $location_id / client_id = $client_id / Dslam_comando2 = $Dslam_comando2 / Dslam_comando3 = $Dslam_comando3 / Dslam_comando4 = $Dslam_comando4/ Dslam_comando5 = $Dslam_comando5";
                } elseif ($Dslam_comando == "IP_setzen") {
                    if (intval($Dslam_comando3) < 255 and intval($Dslam_comando3) > $dslamIpOffset) {
                        //DSLAM_exec ($Dslam_comando, $location_id, $client_id, $Dslam_comando3);
                        echo sprintf('<pre>%s</pre>', $dslamCommunicator->$Dslam_comando());
                    } else {
                       echo "IP nicht im Range 1 bis 254  Eingabe war $Dslam_comando3 Intvelue ".intval($Dslam_comando3);
                    }
                } elseif ($Dslam_comando == "first-setup") {
                    // first-setup

                    //DSLAM_exec ("DSLAM sSecurity set", $location_id, $client_id, $Dslam_comando2);
                    echo sprintf('<pre>%s</pre>', $dslamCommunicator->DSLAM_sSecurity_set($id));
                    if ($location_id == '59') {
                        //DSLAM_exec ("DSLAM_Bandwith_setW", $location_id, $client_id, $Dslam_comando2);
                        echo sprintf('<pre>%s</pre>', $dslamCommunicator->DSLAM_Bandwith_setW());
                    } else {
                        //DSLAM_exec ("DSLAM_Bandwith_set", $location_id, $client_id, $Dslam_comando2);
                        echo sprintf('<pre>%s</pre>', $dslamCommunicator->DSLAM_Bandwith_set());
                    }

                    //DSLAM_exec ("DSLAM_Vlan_set", $location_id, $client_id, $Dslam_comando2);
                    echo sprintf('<pre>%s</pre>', $dslamCommunicator->DSLAM_Bandwith_set());
                    //DSLAM_exec ("DSLAM_Service_set", $location_id, $client_id, $Dslam_comando2);
                    echo sprintf('<pre>%s</pre>', $dslamCommunicator->DSLAM_Bandwith_set());
                    //DSLAM_exec ("DPBO_setup", $location_id, $client_id, $Dslam_comando2);
                    echo sprintf('<pre>%s</pre>', $dslamCommunicator->DPBO_setup());
                    //DSLAM_exec ("Spectrum_setzen", $location_id, $client_id, $Dslam_comando2);
                    echo sprintf('<pre>%s</pre>', $dslamCommunicator->Spectrum_setzen());
                    //DSLAM_exec ("MAXspeed", $location_id, $client_id, $Dslam_comando2);
                    echo sprintf('<pre>%s</pre>', $dslamCommunicator->MAXspeed());
                    //DSLAM_exec ("DSLAM_sSecurity_set", $location_id, $client_id, $Dslam_comando2);
                    echo sprintf('<pre>%s</pre>', $dslamCommunicator->DSLAM_sSecurity_set());
                }

                break;
        }
    } else {
        if ($PPPoE != '') {
            switch ($PPPoE) {
                case 'PPPoE':
                    $output = shell_exec('/usr/shells/ShowActivePppoeSession.sh');
                    break;

                case 'biberach-kirchhausen-cisco-show-interfaces':
                    $output = shell_exec('/usr/shells/show_interfaces_BibKhs.sh');
                    break;

                case 'gemmrigheim-cisco-show-interfaces':
                    $output = shell_exec('/usr/shells/show_interfaces_Gem.sh');
                    break;

                case 'show_ping_errors':
                    $output = shell_exec('/usr/shells/show_ping_log.sh');
                    break;
            }

            echo "<pre>$output</pre>";
        } else {
            echo " alle Felder stehen auf leer ? ";
            echo"Dslam_comando = $Dslam_comando / Location = $location_id / client_id = $client_id / Dslam_comando2 = $Dslam_comando2 / Dslam_comando3 = $Dslam_comando3 / Dslam_comando4 = $Dslam_comando4/ Dslam_comando5 = $Dslam_comando5";
            echo "  overwirte = $overwrite";
        }
    }

    $db->close();

    ?>
</body>
</html>
