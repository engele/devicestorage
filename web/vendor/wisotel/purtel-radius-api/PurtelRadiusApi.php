<?php
/**
 * This file is part of the wkv project.
 */

namespace WisotelKv\PurtelRadiusApi;

/**
 *  Manage the Purtel Radius Server
 */
class PurtelRadius 
{
    /**
    *  Purtel command for Radius
    */
    /**
    * used purtel url
    */
    const PURTELURL = 'https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s';

    /**
    * purtel  radius commands
    */
    const CREATE_RADIUS = 'create_radius';
    const UPDATE_RADIUS = 'update_radius';
    const CLOSE_RADIUS = 'close_radius';
    const OPEN_RADIUS = 'open_radius';
    const CANCEL_RADIUS = 'cancel_radius';
    const STATUS_RADIUS = 'status_radius';

    /**
    * super_username
    * @var string
    */
    protected $super_username = null;
    
    /**
    * super_passwort
    * @var string
    */
    protected $super_passwort = null;
    
    /**
     * request 
     * format [traffic, traffic_gesamt, ip, {leer}]
     * @var array
     */
    protected $request = [
        'anschluss' => null,
        'via' => 'purtel',
        'realm' => null,
        'radius_benutzername' => null,
        'radius_passwort' => null,
        'radius_acs' => 0,
        'radius_ont_id' => null,
        'is_cpeid' => null,
        'access_upstream' => null,
        'access_downstream' => null,
        'erweitert' => 1,
        'format' => null,
        'vom_tag' => null,
        'vom_monat' => null,
        'vom_jahr' => null,
        'format' => null,
        'bis_tag' => null,
        'bis_monat' => null,
        'bis_jahr' => null,
        'ip' => null,
    ];

    /**
     * response 
     * 
     * @var array
     */
    protected $response = [
        'status' => null,
        'err_mes' => null,
        'anschluss' => null,
        'username_short' => null,
        'username' => null,
        'radius_passwort' => null,
        'radacctid' => null,
        'acctsessionid' => null,
        'acctuniqueid' => null,
        'groupname' => null,
        'realm' => null,
        'nasipaddress' => null,
        'nasportid' => null,
        'nasporttype' => null,
        'acctstarttime' => null,
        'acctstoptime' => null,
        'acctsessiontime' => null,
        'acctauthentic' => null,
        'connectinfo_start' => null,
        'connectinfo_stop' => null,
        'acctinputoctets' => null,
        'acctoutputoctets' => null,
        'calledstationid' => null,
        'callingstationid' => null,
        'acctterminatecause' => null,
        'servicetype' => null,
        'framedprotocol' => null,
        'framedipaddrss' => null,
        'acctstartdelay' => null,
        'acctstopdelay' => null,
        'xascendsessionsvrkey' => null,
        'ADSL_Agent_Remote_Id' => null,
    ];

    /**
     * set initial values
     *  
     * @param string $super_user
     * @param string $super_passwort
     * @param string $realm
     */
    function __construct($super_user, $super_passwort, $realm)
    {
        $this->super_username = $super_user;
        $this->super_passwort = $super_passwort;
        $this->request['realm'] = $realm;    
    }

    /**
     *  free memory
     */
    function __destruct()
    {
        unset($this->request);
        unset($this->response);
    }     

    /**
     * execute the purtel radius command
     *  
     * @param string $action
     * 
     * @return boolean status
     */
    
    protected function executeCommand($action)
    {
        if (!isset($this->super_username)) {
            $this->clear();
            $this->response['status'] = false;
            $this->response['err_mes'] = 'Purtel Superuser fehlt';
            return false;
        }
        if (!isset($this->super_passwort)) {
            $this->clear();
            $this->response['status'] = false;
            $this->response['err_mes'] = 'Purtel Superpasswort fehlt';
            return false;
        }
        if (!isset($this->request['realm'])) {
            $this->clear();
            $this->response['status'] = false;
            $this->response['err_mes'] = 'Purtel RadiusREALM fehlt';
            return false;
        }
        $PurtelUrl = sprintf(self::PURTELURL,
            urlencode($this->super_username),
            urlencode($this->super_passwort),
            urlencode($action)
        );

        foreach ($this->request as $key => $value) {
            if (isset($value)) {
                $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
            }
        }
        $curl = curl_init($PurtelUrl);

        curl_setopt_array($curl, array(
            CURLOPT_URL             => $PurtelUrl,
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));

        $result = curl_exec($curl);

        curl_close($curl);

        $resultLine = explode("\n", $result);
        $resultHeader = str_getcsv(array_shift($resultLine),';');
        $resultStatus = array_shift($resultHeader);

        if($resultStatus == '-ERR') {
            $this->response['status'] = false;
            $this->response['error_mes'] = $result;
        } elseif (strpos($resultStatus, '+OK') === 0) {
            if (isset($resultHeader[0])) $this->response['anschluss'] = $resultHeader[0];
            if (isset($resultHeader[1])) $this->response['username_short'] = $resultHeader[1];
            if (isset($resultHeader[2])) $this->response['radius_passwort'] = $resultHeader[2];
            if (isset($resultHeader[3])) $this->response['username'] = $resultHeader[3];
        } else {
            $resultData = str_getcsv($resultLine[0],';');
            array_unshift ($resultHeader, $resultStatus);
            foreach ($resultHeader as $key => $value) {
                $this->response[$value] = $resultData[$key];
            }
            $this->response['status'] = true;
        }
        return $this->response['status'];        
    }

    /**
     * clear request and response
     * 
     * @return void
     */
    protected function clear() 
    {
        foreach ($this->response as $key => $value) {
            $this->response[$key] = null;        
        }                           
    }
    
    /**
     *  create radius account by Purtel
     *  
     * @param string $anschluss
     * 
     * @return boolean status
     */
    public function createRadius($anschluss)
    {
        if (!isset($anschluss)) {
            $this->response['status'] = false;
            $this->response['err_mes'] = 'Purtel Anschluss fehlt!';
            return false;            
        }
        $this->clear();
        $this->request['anschluss'] = $anschluss;
        
        return $this->executeCommand(self::CREATE_RADIUS);
    }

    /**
     *  update radius account by Purtel
     *  
     * @param string $anschluss
     * @param string $radius_passwort
     * @param string $access_upstream
     * @param string $access_downstream
     * @param string $is_cpeid
     * 
     * @return boolean status
     */
    public function updateRadius($anschluss, $radius_passwort, $access_upstream = null, $access_downstream = null, $is_cpeid = null)
    {
        if (!isset($anschluss)) {
            $this->response['status'] = false;
            $this->response['err_mes'] = 'Purtel Anschluss fehlt!';
            return;            
        }
        $this->clear();
        $this->request['anschluss'] = $anschluss;
        $this->request['radius_passwort'] = $radius_passwort;
        $this->request['access_upstream'] = $access_upstream;
        $this->request['access_downstream'] = $access_downstream;
        $this->request['is_cpeid'] = $is_cpeid;
        
        return $this->executeCommand(self::UPDATE_RADIUS);
    }

    /**
     *  close radius account by Purtel
     *  
     * @param string $anschluss
     * 
     * @return boolean status
    */
    public function closeRadius($anschluss)
    {
        if (!isset($anschluss)) {
            $this->response['status'] = false;
            $this->response['err_mes'] = 'Purtel Anschluss fehlt!';
            return;            
        }
        $this->clear();
        $this->request['anschluss'] = $anschluss;
        
        return $this->executeCommand(self::CLOSE_RADIUS);
    }

    /**
     *  open radius account by Purtel
     *  
     * @param string $anschluss
     * 
     * @return boolean status
     */
    public function openRadius($anschluss)
    {
        if (!isset($anschluss)) {
            $this->response['status'] = false;
            $this->response['err_mes'] = 'Purtel Anschluss fehlt!';
            return;            
        }
        $this->clear();
        $this->request['anschluss'] = $anschluss;
        
        return $this->executeCommand(self::OPEN_RADIUS);
    }

    /**
     *  csncel radius account by Purtel
     *  
     * @param string $anschluss
     * 
     * @return boolean status
     */
    public function cancelRadius($anschluss)
    {
        if (!isset($anschluss)) {
            $this->response['status'] = false;
            $this->response['err_mes'] = 'Purtel Anschluss fehlt!';
            return;            
        }
        $this->clear();
        $this->request['anschluss'] = $anschluss;
        
        return $this->executeCommand(self::CANCEL_RADIUS);
    }

    /**
     *  status radius account by Purtel
     *  
     * @param string $anschluss 
     * @param string $format
     * @param string $vom_tag
     * @param string $vom_monat
     * @param string $vom_jahr
     * @param string $bis_tag
     * @param string $bis_monat
     * @param string $bis_jahr
     * @param string $ip
     * 
     * @return boolean status
     */
    public function statusRadius($anschluss, $format = null, $vom_tag = null, $vom_monat = null, $vom_jahr = null, $bis_tag = null, $bis_monat = null, $bis_jahr = null, $ip = null)
    {
        if (!isset($anschluss)) {
            $this->response['status'] = false;
            $this->response['err_mes'] = 'Purtel Anschluss fehlt!';
            return;            
        }
        $this->clear();
        $this->request['anschluss'] = $anschluss;
        $this->request['format'] = $format;
        $this->request['vom_tag'] = $vom_tag; 
        $this->request['vom_monat'] = $vom_monat;
        $this->request['vom_jahr'] = $vom_jahr;
        $this->request['bis_tag'] = $bis_tag;
        $this->request['bis_monat'] = $bis_monat;
        $this->request['bis_jahr'] = $bis_jahr;
        $this->request['ip'] = $ip;
        
        return $this->executeCommand(self::STATUS_RADIUS);
    }

    /**
    * get status
    * 
    * @return boolean
    */
    public function getStatus() {
        return $this->response['status'];
    }
    
    /**
    * get err_mes
    * 
    * @return string
    */
    public function getErrMes() {
        return $this->response['err_mes'];
    }
    
    /**
    * get anschluss
    * 
    * @return string
    */
    public function getAnschluss() {
        return $this->response['anschluss'];
    }
    
    /**
    * get username_short
    * 
    * @return string
    */
    public function getUsernameShort() {
        return $this->response['username_short'];
    }
    
    /**
    * get username
    * 
    * @return string
    */
    public function getUsername() {
        return $this->response['username'];
    }
    
    /**
    * get radius_passwort
    * 
    * @return string
    */
    public function getRadiusPasswort() {
        return $this->response['radius_passwort'];
    }
    
    /**
    * get radacctid
    * 
    * @return integer
    */
    public function getRadacctid() {
        return $this->response['radacctid'];
    }
    
    /**
    * get acctsessionid
    * 
    * @return integer
    */
    public function getAcctsessionid() {
        return $this->response['acctsessionid'];
    }
    
    /**
    * get acctuniqueid
    * 
    * @return integer
    */
    public function getAcctuniqueid() {
        return $this->response['acctuniqueid'];
    }
    
    /**
    * get groupname
    * 
    * @return string
    */
    public function getGroupname() {
        return $this->response['groupname'];
    }
    
    /**
    * get realm
    * 
    * @return string
    */
    public function getRealm() {
        return $this->response['realm'];
    }
    
    /**
    * get nasipaddress
    * 
    * @return string
    */
    public function getNasipaddress() {
        return $this->response['nasipaddress'];
    }
    
    /**
    * get nasportid
    * 
    * @return string
    */
    public function getNasportid() {
        return $this->response['nasportid'];
    }
    
    /**
    * get nasporttype
    * 
    * @return string
    */
    public function getNasporttype() {
        return $this->response['nasporttype'];
    }
    
    /**
    * get acctstarttime
    * 
    * @return string
    */
    public function getAcctstarttime() {
        return $this->response['acctstarttime'];
    }
    
    /**
    * get acctstoptime
    * 
    * @return string
    */
    public function getAcctstoptime() {
        return $this->response['acctstoptime'];
    }
    
    /**
    * get acctsessiontime
    * 
    * @return string
    */
    public function getAcctsessiontime() {
        return $this->response['acctsessiontime'];
    }
    
    /**
    * get acctauthentic
    * 
    * @return string
    */
    public function getAcctauthentic() {
        return $this->response['acctauthentic'];
    }
    
    /**
    * get connectinfo_start
    * 
    * return string
    */
    public function getConnectinfoStart () {
        return $this->response['connectinfo_start'];
    }
    
    /**
    * get connectinfo_stop
    * 
    * @return string
    */
    public function getConnectinfoStop() {
        return $this->response['connectinfo_stop'];
    }
    
    /**
    * get acctinputoctets
    * 
    * @return string
    */
    public function getAcctinputoctets() {
        return $this->response['acctinputoctets'];
    }
    
    /**
    * get acctoutputoctets
    * 
    * @return string
    */
    public function getAcctoutputoctets() {
        return $this->response['acctoutputoctets'];
    }
    
    /**
    * get calledstationid
    * 
    * @return string
    */
    public function getCalledstationid () {
        return $this->response['calledstationid'];
    }
     
    /**
    * get callingstationid
    * 
    * @return string
    */
    public function getCallingstationid() {
        return $this->response['callingstationid'];
    }
     
    /**
    * get acctterminatecause
    * 
    * @return string
    */
    public function getAcctterminatecause() {
        return $this->response['acctterminatecause'];
    }
     
    /**
    * get servicetype
    * 
    * @return string
    */
    public function getServicetype() {
        return $this->response['servicetype'];
    }
     
    /**
    * get framedprotocol
    * 
    * @return string
    */
    public function getFramedprotocol() {
        return $this->response['framedprotocol'];
    }
     
    /**
    * get framedipaddrss
    * 
    * @return string
    */
    public function getFramedipaddrss() {
        return $this->response['framedipaddrss'];
    }
     
    /**
    * get acctstartdelay
    * 
    * @return string
    */
    public function getAcctstartdelay() {
        return $this->response['acctstartdelay'];
    }
     
    /**
    * get acctstopdelay
    * 
    * @return string
    */
    public function getAcctstopdelay() {
        return $this->response['acctstopdelay'];
    }
     
    /**
    * get xascendsessionsvrkey
    * 
    * @return string
    */
    public function getXascendsessionsvrkey() {
        return $this->response['xascendsessionsvrkey'];
    }
     
    /**
    * get ADSL_Agent_Remote_Id
    * 
    * @return string
    */
    public function getAdslAgentRemoteId() {
        return $this->response['ADSL_Agent_Remote_Id'];
    }
}
?>
