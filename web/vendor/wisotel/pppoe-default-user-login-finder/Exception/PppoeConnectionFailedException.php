<?php

/**
 * This file is part of the wkv project.
 */

namespace Wisotel\PppoeDefaultUserLoginFinder\Exception;

/**
 * PPPoE Connection Failed Exception
 */
class PppoeConnectionFailedException extends \Exception
{
    /**
     * Constructor
     */
    public function __construct($message, $id = null)
    {
        parent::__construct($message, $id);
    }
}

?>
