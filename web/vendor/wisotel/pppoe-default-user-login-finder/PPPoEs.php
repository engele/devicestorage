<?php

/**
 * This file is part of the wkv project.
 */

namespace Wisotel\PppoeDefaultUserLoginFinder;

/**
 * Query these PPPoEs for default user logins
 */
class PPPoEs
{
    /**
     * Query these PPPoEs
     * 
     * @var array
     */
    protected static $pppoes = array(
        array(
            'ipAddress' => '213.61.107.128',
            'username' => 'WiSoTELapi',
            'password' => 'Karl172115.',
        ),
        array(
            'ipAddress' => '37.99.202.1',
            'username' => 'WiSoTELapi',
            'password' => 'Karl172115.',
        ),
        array(
            'ipAddress' => '37.99.200.10',
            'username' => 'WiSoTELapi',
            'password' => 'Karl172115.',
        ),
    );

    /**
     * Get PPPoEs
     * 
     * @return array
     */
    public static function getPppoes()
    {
        return self::$pppoes;
    }
}

?>
