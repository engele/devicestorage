<?php

/**
 * This file is part of the wkv project.
 */

namespace Wisotel\PppoeDefaultUserLoginFinder;

/**
 * Holds information about detected login
 */
class DetectedLogin
{
    /**
     * CPE MAC address
     * 
     * @var string
     */
    protected $callerId;

    /**
     * IP address
     * 
     * @var string
     */
    protected $address;

    /**
     * Logged in since
     * 
     * @var string
     */
    protected $uptime;

    /**
     * How often detected
     * 
     * @var integer
     */
    public $counter = 1;

    /**
     * Alias for constructor
     * 
     * @return DetectedLogin
     */
    public static function newInstance()
    {
        return new self();
    }

    /**
     * Serialize
     * 
     * @return array
     */
    public function serialize()
    {
        return array(
            $this->callerId,
            $this->address,
            $this->uptime,
            $this->counter,
        );
    }

    /**
     * Unserialize
     * 
     * @param array $array
     * 
     * @return DetectedLogin
     */
    public function unserialize($array)
    {
        list(
            $this->callerId,
            $this->address,
            $this->uptime,
            $this->counter,
        ) = $array;

        return $this;
    }

    /**
     * Set callerId
     * 
     * @param string $callerId
     * 
     * @return DetectedLogin
     */
    public function setCallerId($callerId)
    {
        $this->callerId = $callerId;

        return $this;
    }

    /**
     * Set address
     * 
     * @param string $address
     * 
     * @return DetectedLogin
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Set uptime
     * 
     * @param string $uptime
     * 
     * @return DetectedLogin
     */
    public function setUptime($uptime)
    {
        $this->uptime = $uptime;

        return $this;
    }

    /**
     * Set counter
     * 
     * @param integer $counter
     * 
     * @return DetectedLogin
     */
    public function setCounter($counter)
    {
        $this->counter = $counter;

        return $this;
    }

    /**
     * Get callerId
     * 
     * @return string
     */
    public function getCallerId()
    {
        return $this->callerId;
    }

    /**
     * Get address
     * 
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Get uptime
     * 
     * @return string
     */
    public function getUptime()
    {
        return $this->uptime;
    }

    /**
     * Get counter
     * 
     * @return string
     */
    public function getCounter()
    {
        return $this->counter;
    }
}

?>
