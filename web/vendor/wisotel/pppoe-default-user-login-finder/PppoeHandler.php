<?php

/**
 * This file is part of the wkv project.
 */

namespace Wisotel\PppoeDefaultUserLoginFinder;

require_once __DIR__.'/../../../_inc/Mikrotik_api.php';
require_once __DIR__.'/Exception/PppoeConnectionFailedException.php';
require_once __DIR__.'/DetectedLogin.php';

use \routeros_api;
use Wisotel\PppoeDefaultUserLoginFinder\Exception\PppoeConnectionFailedException;

/**
 * Handles communications with PPPoE
 */
class PppoeHandler
{
    /**
     * Name of the default user account
     * 
     * @var string
     */
    protected $defaultUsersName = 'wisotel@purtel.com';

    /**
     * PPPoE login informations
     * 
     * @var array
     */
    protected $pppoe;

    /**
     * Constructor
     * 
     * @param array $pppoe
     */
    public function __construct($pppoe)
    {
        $this->pppoe = $pppoe;
    }

    /**
     * Connect to PPPoE and see if default user is loged in, return it's cpe's mac-address
     * 
     * @param array $pppoe
     * 
     * @throws PppoeConnectionFailedException
     * 
     * @return DetectedLogin|null
     */
    public function detectDefaultUserLogin()
    {
        $pppoeApi = new routeros_api();
        $pppoeApi->debug = false;

        $connected = $pppoeApi->connect(
            $this->pppoe['ipAddress'], 
            $this->pppoe['username'], 
            $this->pppoe['password']
        );

        if (!$connected) {
            // connection to pppoe failed
            throw new PppoeConnectionFailedException(sprintf('Connection to PPPoE failed. IP: %s',
                $this->pppoe['ipAddress']
            ));
        }

        $pppoeApi->write('/ppp/active/print', false);
        $pppoeApi->write('?name='.$this->defaultUsersName, true);

        $result = $pppoeApi->read();

        $pppoeApi->disconnect();
        
        if (count($result) > 0) {
            return DetectedLogin::newInstance()
                ->setCallerId($result[0]['caller-id'])
                ->setAddress($result[0]['address'])
                ->setUptime($result[0]['uptime'])
            ;
        }

        return null;
    }
}

?>
