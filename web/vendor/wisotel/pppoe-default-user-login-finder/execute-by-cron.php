<?php

/**
 * This file is part of the wkv project.
 */

namespace Wisotel\PppoeDefaultUserLoginFinder;

require_once __DIR__.'/PPPoEs.php';
require_once __DIR__.'/PppoeHandler.php';
require_once __DIR__.'/../../../_inc/phpmailer/class.phpmailer.php';
require_once __DIR__.'/../../../_inc/phpmailer/class.smtp.php';
require_once __DIR__.'/../../../_conf/mail.inc';
require_once __DIR__.'/../../../_conf/database.inc';
require_once __DIR__.'/../configuration/Configuration.php';
require_once __DIR__.'/DetectedLogin.php';

use Wisotel\PppoeDefaultUserLoginFinder\Exception\PppoeConnectionFailedException;
use Wisotel\Configuration\Configuration;
use \PHPMailer;
use \SMTP;

/**
 * Find default user logins and notify admin
 */
class PppoeDefaultUserLoginFinder
{
    /**
     * Send admin notifications to these e-mail addresses
     * 
     * @var array
     */
    protected $admins = array(
        'karl-heinz.legat@wisotel.com',
        'david.brix@wisotel.com',
        'sven.siebrands@wisotel.com',
        'dieter.horrmann@wisotel.com',
        'joachim.bratke@wisotel.com',
        'jan.gruber@tkt-teleconsult.de',
    );

    /**
     * Send first notifications to admins after x counts
     * 
     * @var integer
     */
    protected $sendFirstNotificationToAdminsAfter = 3;

    /**
     * Send following notifications to admins every x counts.
     * Following notifications will be send after $sendFirstNotificationToAdminsAfter.
     * 
     * @var integer
     */
    protected $sendFollowingNotificationToAdminsInterval = 8;

    /**
     * Send email notification to customer after x counts
     * 
     * @var integer
     */
    protected $sendEmailNotificationToCustomerAfter = 6;

    /**
     * Send notifications to admins only within from and till
     * 
     * @var array
     */
    protected $adminsBusinessHours = array(
        'from' => array(
            'hour' => 8,
            'minute' => 0,
        ),
        'till' => array(
            'hour' => 19,
            'minute' => 0,
        ),
    );

    /**
     * Previous discovered logins
     * 
     * @var array
     */
    protected $previousDetectedLogins = array();

    /**
     * Previous notified customers
     * 
     * @var array
     */
    protected $previousNotifiedCustomers = array();

    /**
     * Temporary file for previous detected logins.
     * Set this value in __construct().
     * 
     * @var string
     */
    protected $previousDetectedLoginsTemporaryFile;

    /**
     * Temporary file for previous notified customers.
     * Set this value in __construct().
     * 
     * @var string
     */
    protected $previousNotifiedCustomersTemporaryFile;

    /**
     * Notifications to be send to admin
     * 
     * @var array
     */
    protected $adminNotifications = array();

    /**
     * Notifications to be send to customer
     * 
     * @var array
     */
    protected $customerNotifications;

    /**
     * Constructor
     */
    public function __construct()
    {
        // set a value for temporary file for previous detected logins
        $this->previousDetectedLoginsTemporaryFile = __DIR__.'/detected-logins.log';

        // set a value for temporary file for previous notified customer
        $this->previousNotifiedCustomersTemporaryFile = __DIR__.'/notified-customers.log';

        $this->previousDetectedLogins = $this->getPreviousDetectedLoginsFromTemporaryFile();
        $this->previousNotifiedCustomers = $this->getPreviousNotifiedCustomers();

        if (null === $this->previousNotifiedCustomers) {
            $this->previousNotifiedCustomers = array();
        }

        $now = new \DateTime('now -1 day', new \DateTimeZone('Europe/Berlin'));

        // remove older entries
        foreach ($this->previousNotifiedCustomers as $key => $date) {
            $date = new \DateTime($date);

            if ($date < $now) {
                unset($this->previousNotifiedCustomers[$key]);
            }
        }

        // set default value for sendFirstNotificationToAdminsAfter
        if (!is_int($this->sendFirstNotificationToAdminsAfter) || $this->sendFirstNotificationToAdminsAfter < 1) {
            $this->sendFirstNotificationToAdminsAfter = 1;
        }

        // set default value for sendFirstNotificationToAdminsAfter
        if (!is_int($this->sendFollowingNotificationToAdminsInterval) || $this->sendFollowingNotificationToAdminsInterval < 1) {
            $this->sendFollowingNotificationToAdminsInterval = 1;
        }

        // set default value for sendEmailNotificationToCustomerAfter
        if (!is_int($this->sendEmailNotificationToCustomerAfter) || $this->sendEmailNotificationToCustomerAfter < 1) {
            $this->sendEmailNotificationToCustomerAfter = 4;
        }
    }

    /**
     * Destructor
     */
    public function __destruct()
    {
        file_put_contents($this->previousDetectedLoginsTemporaryFile, json_encode(array_map(function (DetectedLogin $detectedLogin) {
            return $detectedLogin->serialize();
        }, $this->previousDetectedLogins)));

        file_put_contents($this->previousNotifiedCustomersTemporaryFile, json_encode($this->previousNotifiedCustomers));
    }

    /**
     * Get previous detected logins from temporary file
     * 
     * @return array|null
     */
    protected function getPreviousDetectedLoginsFromTemporaryFile()
    {
        if (file_exists($this->previousDetectedLoginsTemporaryFile) && is_readable($this->previousDetectedLoginsTemporaryFile)) {
            $data = json_decode(file_get_contents($this->previousDetectedLoginsTemporaryFile), true);

            if (is_array($data)) {
                return array_map(function ($array) {
                    return DetectedLogin::newInstance()->unserialize($array);
                }, $data);
            }
        }

        return null;
    }

    /**
     * Get previous notified customers from temporary file
     * 
     * @return array|null
     */
    protected function getPreviousNotifiedCustomers()
    {
        if (file_exists($this->previousNotifiedCustomersTemporaryFile) && is_readable($this->previousNotifiedCustomersTemporaryFile)) {
            return json_decode(file_get_contents($this->previousNotifiedCustomersTemporaryFile), true);
        }

        return null;
    }

    /**
     * Send notifications to admin and customer
     * 
     * @return void
     */
    public function sendNotifications()
    {
        $this->sendCustomerEmailNotifications();
        $this->sendAdminNotifications();
    }

    /**
     * Is now (time) within admins business hours?
     * 
     * @return boolean
     */
    private function withinAdminsBusinessHours()
    {   
        if (!isset($this->adminNotifications) || empty($this->adminNotifications)) {
            return true;
        }

        if (!isset($this->adminNotifications['from']) || empty($this->adminNotifications['from'])) {
            return true;
        }

        if (!isset($this->adminNotifications['from']['hour']) && !is_int($this->adminNotifications['from']['hour'])) {
            return true;
        }

        if (!isset($this->adminNotifications['from']['minute']) && !is_int($this->adminNotifications['from']['minute'])) {
            return true;
        }

        if (!isset($this->adminNotifications['till']) || empty($this->adminNotifications['till'])) {
            return true;
        }

        if (!isset($this->adminNotifications['till']['hour']) && !is_int($this->adminNotifications['till']['hour'])) {
            return true;
        }

        if (!isset($this->adminNotifications['till']['minute']) && !is_int($this->adminNotifications['till']['minute'])) {
            return true;
        }

        $timeZone = new \DateTimeZone('Europe/Berlin');

        $from = new \DateTime('now', $timeZone);
        $from->setTime($this->adminNotifications['from']['hour'], $this->adminNotifications['from']['minute']);

        $till = new \DateTime('now', $timeZone);
        $till->setTime($this->adminNotifications['till']['hour'], $this->adminNotifications['till']['minute']);

        $now = new \DateTime('now', $timeZone);

        if ($now >= $from && $now <= $till) {
            return true;
        }

        return false;
    }

    /**
     * Send notifications to admin
     * 
     * @return void
     */
    public function sendAdminNotifications()
    {
        if (empty($this->adminNotifications)) {
            return;
        }

        if (!$this->withinAdminsBusinessHours()) {
            return;
        }

        global $CharSet, $smtpHost, $smtpPort, $smtpAuth, $smtpUser, $smtpPasswd, $from, $fromName;

        $mail = new PHPMailer();
        $mail->SetLanguage("de", __DIR__.'/../../../_inc/phpmailer');
        $mail->IsSMTP();
        $mail->IsHTML(true);
        $mail->CharSet = $CharSet;
        $mail->Host = $smtpHost;
        $mail->Port = $smtpPort;
        $mail->SMTPAuth = $smtpAuth;
        $mail->Username = $smtpUser;
        $mail->Password = $smtpPasswd;
        $mail->From = $from;
        $mail->FromName = $fromName;

        $mail->Subject = 'Default-User wisotel@purtel.com eingeloggt';

        $mail->Body = 'Der Default-User <u>wisotel@purtel.com</u> ist seit einiger Zeit in folgenden PPPoEs eingeloggt:<br /><br />';

        $mail->Body .= implode('<br /><br />', array_map(function ($message) {
            return str_replace("\n", '<br />', $message);
        }, $this->adminNotifications));

        foreach ($this->admins as $mailAddress) {
            $mail->AddAddress($mailAddress);
        }

        $mail->send();
    }

    /**
     * Send email notifications to customer
     * 
     * @return void
     */
    public function sendCustomerEmailNotifications()
    {
        if (empty($this->customerNotifications)) {
            return;
        }

        if (isset($this->previousNotifiedCustomers[$this->customerNotifications['email']])) {
            return;
        }

        global $CharSet;

        $mailAccount = Configuration::get('technicMailAccount');

        $mail = new PHPMailer();
        $mail->SetLanguage("de", __DIR__.'/../../../_inc/phpmailer');
        $mail->IsSMTP();
        $mail->IsHTML(true);
        $mail->CharSet = $CharSet;
        $mail->Host = $mailAccount['host'];
        $mail->Port = $mailAccount['port'];
        $mail->SMTPAuth = false;
        $mail->Username = $mailAccount['username'];
        $mail->Password = $mailAccount['password'];
        $mail->From = $mailAccount['username'];
        $mail->FromName = 'WiSoTEL Technik';
        $mail->Subject = 'Ihr Anschluss ist in einem falschen Zustand';
        $mail->Body = $this->customerNotifications['message'];

        $mail->AddAddress($this->customerNotifications['email']);

        $mail->send();

        $now = new \DateTime('now', new \DateTimeZone('Europe/Berlin'));

        $this->previousNotifiedCustomers[$this->customerNotifications['email']] = $now->format(\DateTime::ATOM);

        $this->adminNotifications[] = sprintf(
            '<br /><strong>Kunde wurde per E-Mail an (%s) benachrichtigt.<br />Nachricht an Kunden:</strong><br />%s',
            $this->customerNotifications['email'],
            $this->customerNotifications['message']
        );
    }

    /**
     * Create message that will be send to admins
     * 
     * @param array $pppoe
     * @param DetectedLogin $detectedLogin
     * 
     * @return string
     */
    protected function createAdminNotificationMessage($pppoe, DetectedLogin $detectedLogin)
    {
        global $db;

        $wkvUserMacAddress = $this->detectedMacToCustomerMac($detectedLogin->getCallerId());

        $query = $db->query('SELECT `clientid`, `id`, `mac_address` 
            FROM `customers` WHERE `mac_address` LIKE "%'.$wkvUserMacAddress.'%" GROUP BY `clientid`'
        );

        $possibleUser = '';

        while ($data = $query->fetch_assoc()) {
            $possibleUser .= sprintf("wkv-customer client-id: <a href='"
                ."http://172.20.2.221/wkv2/index.php?menu=customer&func=vertrag&id=%s'>%s</a>\n"
                ."wkv-customer mac_address: %s",
                $data['id'],
                $data['clientid'],
                $data['mac_address']
            );
        }

        return sprintf("<b>PPPoE IP: %s</b>\nCPE MAC: %s\nCPE IP: <a href='https://%s'>%s</a>\nUptime: %s\n%s\ncounted: %s\n",
            $pppoe['ipAddress'],
            $detectedLogin->getCallerId(),
            $detectedLogin->getAddress(),
            $detectedLogin->getAddress(),
            $detectedLogin->getUptime(),
            $possibleUser,
            $detectedLogin->getCounter()
        );
    }

    /**
     * Calculates customers mac-address by detected mac-address
     * 
     * @param string $macAddress
     * 
     * @return string
     */
    protected function detectedMacToCustomerMac($macAddress)
    {
        $wkvUserMacAddress = str_replace(':', '', $macAddress);

        return dechex(hexdec($wkvUserMacAddress) -4);
    }

    /**
     * Create message that will be send to customer by email
     * 
     * @param array $pppoe
     * @param DetectedLogin $detectedLogin
     * 
     * @return string|null
     */
    protected function createCustomerEmailNotificationMessage($pppoe, DetectedLogin $detectedLogin)
    {
        global $db;

        $wkvUserMacAddress = $this->detectedMacToCustomerMac($detectedLogin->getCallerId());

        $query = $db->query('SELECT DISTINCT(`emailaddress`), `title`, `firstname`, `lastname` 
            FROM `customers` WHERE `mac_address` LIKE "%'.$wkvUserMacAddress.'%"'
        );

        if ($query->num_rows != 1) {
            return null; // unable to find unique user
        }

        $data = $query->fetch_assoc();

        $customerNotificationTemplate = __DIR__.'/mail/customer-notification.html';

        if (!file_exists($customerNotificationTemplate) || !is_readable($customerNotificationTemplate)) {
            return null; // unable to load template file
        }

        $customerNotificationTemplate = str_replace(
            array('{salutation}', '{title}', '{surname}'),
            array(
                $data['title'] == 'Frau' ? 'geehrte' : 'geehrter',
                $data['title'],
                $data['lastname']
            ),
            file_get_contents($customerNotificationTemplate)
        );

        return array(
            'message' => $customerNotificationTemplate,
            'email' => $data['emailaddress'],
        );
    }

    /**
     * Find and compare default user logins
     * 
     * @return void
     */
    public function run()
    {
        foreach (PPPoEs::getPppoes() as $pppoeId => $pppoe) {
            $pppoeHandler = new PppoeHandler($pppoe);

            try {
                $detectedLogin = $pppoeHandler->detectDefaultUserLogin();
                //$detectedLogin['counter'] = 1;

                if (null === $detectedLogin) { // no default user login detected
                    unset($this->previousDetectedLogins[$pppoeId]);

                    continue;
                }
            } catch (PppoeConnectionFailedException $e) { // connection to PPPoE failed
                $this->adminNotifications[] = $e->getMessage();

                continue;
            }

            // default user is loged in

            if (!isset($this->previousDetectedLogins[$pppoeId])) { // no previous login was detected
                $this->previousDetectedLogins[$pppoeId] = $detectedLogin; // save mac-address for next run

                continue;
            }

            if ($detectedLogin->getCallerId() === $this->previousDetectedLogins[$pppoeId]->getCallerId()) { // same mac-address is still connected
                $detectedLogin->setCounter($this->previousDetectedLogins[$pppoeId]->getCounter()); // remember old counter

                $this->previousDetectedLogins[$pppoeId] = $detectedLogin; // update saved data, just in case something hast changed

                $detectedLogin->counter++;

                $notifyAdminAfter = $this->sendFirstNotificationToAdminsAfter;

                if ($detectedLogin->counter > $this->sendFirstNotificationToAdminsAfter) {
                    $notifyAdminAfter += $this->sendFollowingNotificationToAdminsInterval;
                }

                // notify admins?
                if (is_int($detectedLogin->counter / $notifyAdminAfter)) {
                    $this->adminNotifications[] = $this->createAdminNotificationMessage($pppoe, $detectedLogin);
                }

                // notify customer?
                if (is_int($detectedLogin->counter / $this->sendEmailNotificationToCustomerAfter)) {
                    $this->customerNotifications = $this->createCustomerEmailNotificationMessage($pppoe, $detectedLogin);

                    if (null !== $customerNotification) {
                        $this->adminNotifications[] = $this->createAdminNotificationMessage($pppoe, $detectedLogin);
                    }
                }
            } else {
                $this->previousDetectedLogins[$pppoeId] = $detectedLogin; // save mac-address for next run
            }
        }

        $this->sendNotifications();
    }
}

$pppoeDefaultUserLoginFinder = new PppoeDefaultUserLoginFinder();
$pppoeDefaultUserLoginFinder->run();

?>
