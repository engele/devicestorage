<?php

/**
 * This file is part of the wkv project.
 */

namespace Wisotel\Configuration;

/**
 * Get values from configuration file
 */
class Configuration
{
    /**
     * Configuration values
     *
     * @var array
     */
    protected static $configuration;

    /**
     * Load configuration values from file
     * 
     * @param string $file
     * 
     * @return void
     */
    protected static function loadConfigurationFromFile($file)
    {
        if (!file_exists($file) || !is_readable($file)) {
            throw new \InvalidArgumentException(sprintf('Configuration file (%s) not found or not readable', $file));
        }

        self::$configuration = require $file;
    }

    /**
     * Get configuration value
     *
     * @param mixed $value
     *
     * @return mixed
     */
    public static function get($value)
    {
        if (null === self::$configuration) {
            self::loadConfigurationFromFile(__DIR__.'/../../../_conf/global-config.php');
        }

        if (isset(self::$configuration[$value])) {
            return self::$configuration[$value];
        }

        return null;
    }
}

?>
