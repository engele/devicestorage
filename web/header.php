<html lang="de" xmlns="http://www.w3.org/1999/xhtml" xml:lang="de">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <title><?php echo $HeadTitle; ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $Favicon; ?>" />
    <link media="all" href="<?php echo $StartURL.'/_css/jquery-ui.min.css'; ?>" type="text/css" rel="stylesheet" />
    <link media="all" href="<?php echo $StartURL.'/_css/jquery.bsmselect.css'; ?>" type="text/css" rel="stylesheet" />
    <link media="all" href="<?php echo $StartURL.'/_css/jquery.typeahead.css'; ?>" type="text/css" rel="stylesheet" />
    <link media="all" href="<?php echo $StartURL.'/_css/wkv.css'; ?>" type="text/css" rel="stylesheet" />
    <?php
    if (file_exists($StartPath.'/'.$SelPath.'/_css/'.$SelPath.'.css')) {
        echo '<link media="all" href="'.$StartURL.'/'.$SelPath.'/_css/'.$SelPath.'.css" type="text/css" rel="stylesheet" />'."\n";
    }
    ?>
    <script type="text/javascript">var COMPANY_NAME = '<?php echo \Wisotel\Configuration\Configuration::get("companyName"); ?>';</script>
    <script type="text/javascript" src="<?php echo $StartURL.'/_js/jquery.min.js?v=2'; ?>"></script>
    <script type="text/javascript" src="<?php echo $StartURL.'/_js/jquery-ui.min.js?v=2'; ?>"></script>
    <script type="text/javascript" src="<?php echo $StartURL.'/_js/jquery.validate.js?v=2'; ?>"></script>
    <script type="text/javascript" src="<?php echo $StartURL.'/_js/additional-methods.js?v=2'; ?>"></script>
    <script type="text/javascript" src="<?php echo $StartURL.'/_js/message.de.js?v=2'; ?>"></script>
    <script type="text/javascript" src="<?php echo $StartURL.'/_js/jquery.metadata.js?v=2'; ?>"></script>
    <script type="text/javascript" src="<?php echo $StartURL.'/_js/jquery.bsmselect.js?v=2'; ?>"></script>
    <script type="text/javascript" src="<?php echo $StartURL.'/_js/jquery.tablesorter.min.js?v=2'; ?>"></script>
    <script type="text/javascript" src="<?php echo $StartURL.'/_js/jquery.uploadifive.min.js?v=2'; ?>"></script>
    <script type="text/javascript" src="<?php echo $StartURL.'/_js/jquery.corner.js?v=2'; ?>"></script>
    <script type="text/javascript" src="<?php echo $StartURL.'/_js/jquery.typeahead.js?v=2'; ?>"></script>
    <script type="text/javascript" src="<?php echo $StartURL.'/_js/wkv.js?v=2'; ?>"></script>
    <?php
    if (file_exists($StartPath.'/'.$SelPath.'/_js/'.$SelPath.'.js')) {
        echo '<script type="text/javascript" src="'.$StartURL.'/'.$SelPath.'/_js/'.$SelPath.'.js?v=3"></script>';
    }
    ?>
</head>
<body>