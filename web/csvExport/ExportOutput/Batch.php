<?php
/**
 * 
 */

namespace csvExport\Output;

require_once __DIR__.'/Output.php';

/**
 * Output as .bat
 */
class Batch extends Output
{
    /**
     * @return CSV
     */
    public static function newInstance()
    {
        return new self();
    }

    /**
     * @param string $filename
     * 
     * @return void
     */
    protected function setHeader($filename)
    {
        header('Content-Type: application/bat; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$filename.'.bat');
    }

    /**
     * @param array $data
     * 
     * @throws \Exception
     * 
     * @return void
     */
    public function output(array $data)
    {
        $this->validateData($data);

        $this->setHeader($data['filename']);

        $resource = fopen('php://output', 'w');

        foreach ($data['output'] as $line) {
            fwrite($resource, $line);
        }

        fclose($resource);
    }
}
?>