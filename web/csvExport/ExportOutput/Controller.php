<?php
/**
 * 
 */

namespace csvExport\Output;

require_once $StartPath.'/'.$SelMenu.'/Export/ExportTables.php';
require_once $StartPath.'/'.$SelMenu.'/Export/WherePlaceholder.php';
require_once $StartPath.'/'.$SelMenu.'/Export/Relations.php';
require_once $StartPath.'/'.$SelMenu.'/Export/Export.php';
require_once $StartPath.'/'.$SelMenu.'/Export/ExportResult.php';

use \csvExport\ExportTables;
use \csvExport\Relations;
use \csvExport\WherePlaceholder;
use \csvExport\Export;
use \csvExport\ExportResult;

/**
 * Map methods to route
 */
class Controller
{
    /**
     * $_GET['func'] to method
     * 
     * @var array
     */
    protected $routesToMethods = array(
        'export-csv' => 'defaultCsv',
        'export-ping-csv' => 'pingCsv',
        'export-ping-only-failed-csv' => 'pingOnlyFailedCsv',
        'export-dslam-status-csv' => 'dslamStatusCsv',
        'export-dslam-config-batch' => 'dslamConfigBatch',
        'export-pppee-config-batch' => 'pppeeConfigBatch',
        'export-iban-verification' => 'ibanVerificationCsv',
        'export-microtik-ppp-secrets' => 'microtikPppSecretsCsv',
        'export-sap-debitors-creation' => 'sapDebitorsCreationCsv',
    );

    /**
     * @var mysqli
     */
    protected $db;

    /**
     * @var array
     */
    protected $sql;

    /**
     * Constructor
     * 
     * @param string $route
     * 
     * @throws \Exception
     */
    public function __construct($route, \mysqli $db, $sql)
    {
        if (!is_string($route) || empty($route))  {
            throw new \Exception('Invalid route');
        }

        if (!isset($this->routesToMethods[$route])) {
            throw new \Exception('No match for route found ('.$route.')');
        }

        $this->db = $db;
        $this->sql = $sql;

        $this->{$this->routesToMethods[$route]}();
    }

    /**
     * Output profile as .csv
     * 
     * @return void
     */
    public function defaultCsv()
    {
        require_once __DIR__.'/CSV.php';

        $wherePlaceholder = new WherePlaceholder();

        if (isset($_POST['placeholder'])) {
            $wherePlaceholder->setPlaceholderValues($_POST['placeholder']);
        }

        try {
            $export = new Export(new ExportTables, new Relations, $wherePlaceholder, new ExportResult, $this->db, $this->sql);
            $export->useProfile($_GET['profile-id']);

            CSV::newInstance()->outputFromExport($export);

            exit; //End this script here!
        } catch(\csvExport\PlaceholderNotProvided $e) {
            $export->askForPlaceholderReplacements();
        }
    }

    /**
     * Verify IBANs
     * 
     * @return void
     */
    public function ibanVerificationCsv()
    {
        require_once __DIR__.'/CSV.php';
        require_once __DIR__.'/../Lib/IbanVerification.php';

        $wherePlaceholder = new WherePlaceholder();

        if (isset($_POST['placeholder'])) {
            $wherePlaceholder->setPlaceholderValues($_POST['placeholder']);
        }

        try {
            $export = new Export(new ExportTables, new Relations, $wherePlaceholder, new ExportResult, $this->db, $this->sql);

            CSV::newInstance()->output(
                \csvExport\Lib\IbanVerification::newInstance($export)->process($_GET['profile-id'])
            );

            exit; //End this script here!
        } catch(\csvExport\PlaceholderNotProvided $e) {
            $export->askForPlaceholderReplacements();
        } catch (\csvExport\ProfileNotUseable $e) {
            echo $e->getMessage();
        }
    }

    /**
     * SAP debitors creation
     * 
     * @return void
     */
    public function sapDebitorsCreationCsv()
    {
        require_once __DIR__.'/CSV.php';
        require_once __DIR__.'/../Lib/Sap/DebitorsCreation.php';

        $wherePlaceholder = new WherePlaceholder();

        if (isset($_POST['placeholder'])) {
            $wherePlaceholder->setPlaceholderValues($_POST['placeholder']);
        }

        try {
            $export = new Export(new ExportTables, new Relations, $wherePlaceholder, new ExportResult, $this->db, $this->sql);

            CSV::newInstance()->output(
                \csvExport\Lib\Sap\DebitorsCreation::newInstance($export)->process($_GET['profile-id'])
            );

            exit; //End this script here!
        } catch(\csvExport\PlaceholderNotProvided $e) {
            $export->askForPlaceholderReplacements();
        } catch (\csvExport\ProfileNotUseable $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Ping profile and output as .csv
     * 
     * @return void
     */
    public function pingCsv()
    {
        require_once __DIR__.'/CSV.php';
        require_once __DIR__.'/../Lib/Ping/Ping.php';

        $wherePlaceholder = new WherePlaceholder();

        if (isset($_POST['placeholder'])) {
            $wherePlaceholder->setPlaceholderValues($_POST['placeholder']);
        }

        try {
            $export = new Export(new ExportTables, new Relations, $wherePlaceholder, new ExportResult, $this->db, $this->sql);

            CSV::newInstance()->output(
                \csvExport\Lib\Ping\Ping::newInstance($export)->process($_GET['profile-id'])
            );

            exit; //End this script here!
        } catch(\csvExport\PlaceholderNotProvided $e) {
            $export->askForPlaceholderReplacements();
        } catch (\csvExport\ProfileNotUseable $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Create Microtik PPP-Secrets and output as .csv
     * 
     * @return void
     */
    public function microtikPppSecretsCsv()
    {
        require_once __DIR__.'/CSV.php';
        require_once __DIR__.'/../Lib/Microtik/PppSecrets.php';

        $wherePlaceholder = new WherePlaceholder();

        if (isset($_POST['placeholder'])) {
            $wherePlaceholder->setPlaceholderValues($_POST['placeholder']);
        }

        try {
            $export = new Export(new ExportTables, new Relations, $wherePlaceholder, new ExportResult, $this->db, $this->sql);

            CSV::newInstance()->output(
                \csvExport\Lib\Microtik\PppSecrets::newInstance($export)->process($_GET['profile-id'])
            );

            exit; //End this script here!
        } catch(\csvExport\PlaceholderNotProvided $e) {
            $export->askForPlaceholderReplacements();
        } catch (\csvExport\ProfileNotUseable $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Ping profile and output only the failed results as .csv
     * 
     * @return void
     */
    public function pingOnlyFailedCsv()
    {
        require_once __DIR__.'/CSV.php';
        require_once __DIR__.'/../Lib/Ping/PingOnlyFailed.php';

        $wherePlaceholder = new WherePlaceholder();

        if (isset($_POST['placeholder'])) {
            $wherePlaceholder->setPlaceholderValues($_POST['placeholder']);
        }

        try {
            $export = new Export(new ExportTables, new Relations, $wherePlaceholder, new ExportResult, $this->db, $this->sql);

            CSV::newInstance()->output(
                \csvExport\Lib\Ping\PingOnlyFailed::newInstance($export)->process($_GET['profile-id'])
            );

            exit; //End this script here!
        } catch(\csvExport\PlaceholderNotProvided $e) {
            $export->askForPlaceholderReplacements();
        } catch (\csvExport\ProfileNotUseable $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Run DSLAM Status Commands on profile and return output as .csv
     * 
     * @return void
     */
    public function dslamStatusCsv()
    {
        require_once __DIR__.'/CSV.php';
        require_once __DIR__.'/../Lib/Dslam/DslamStatus.php';

        $wherePlaceholder = new WherePlaceholder();

        if (isset($_POST['placeholder'])) {
            $wherePlaceholder->setPlaceholderValues($_POST['placeholder']);
        }

        try {
            $export = new Export(new ExportTables, new Relations, $wherePlaceholder, new ExportResult, $this->db, $this->sql);

            CSV::newInstance()->output(
                \csvExport\Lib\Dslam\DslamStatus::newInstance($export)->process($_GET['profile-id'])
            );

            exit; //End this script here!
        } catch(\csvExport\PlaceholderNotProvided $e) {
            $export->askForPlaceholderReplacements();
        } catch (\csvExport\ProfileNotUseable $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Output DSLAM Config Commands from profile as .bat
     * 
     * @return void
     * 
     * @todo
     */
    public function dslamConfigBatch()
    {
        require_once __DIR__.'/Batch.php';
        require_once __DIR__.'/../Lib/Dslam/DslamConfig.php';

        $wherePlaceholder = new WherePlaceholder();

        if (isset($_POST['placeholder'])) {
            $wherePlaceholder->setPlaceholderValues($_POST['placeholder']);
        }

        try {
            $export = new Export(new ExportTables, new Relations, $wherePlaceholder, new ExportResult, $this->db, $this->sql);

            Batch::newInstance()->output(
                \csvExport\Lib\Dslam\DslamConfig::newInstance($export)->process($_GET['profile-id'])
            );

            exit; //End this script here!
        } catch(\csvExport\PlaceholderNotProvided $e) {
            $export->askForPlaceholderReplacements();
        } catch (\csvExport\ProfileNotUseable $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Output PPPeE Config Commands from profile as .bat
     * 
     * @return void
     * 
     * @todo
     */
    public function pppeeConfigBatch()
    {
        require_once __DIR__.'/Batch.php';
        require_once __DIR__.'/../Lib/PPPeE/PppeeConfig.php';

        $wherePlaceholder = new WherePlaceholder();

        if (isset($_POST['placeholder'])) {
            $wherePlaceholder->setPlaceholderValues($_POST['placeholder']);
        }

        try {
            $export = new Export(new ExportTables, new Relations, $wherePlaceholder, new ExportResult, $this->db, $this->sql);

            Batch::newInstance()->output(
                \csvExport\Lib\PPPeE\PppeeConfig::newInstance($export)->process($_GET['profile-id'])
            );

            exit; //End this script here!
        } catch(\csvExport\PlaceholderNotProvided $e) {
            $export->askForPlaceholderReplacements();
        } catch (\csvExport\ProfileNotUseable $e) {
            echo $e->getMessage();
        }
    }
}

?>