<?php
/**
 * 
 */

namespace csvExport\Output;

/**
 * 
 */
abstract class Output
{
    /**
     * @param array $data
     * 
     * @return array
     */
    abstract public function output(array $data);

    /**
     * @param array $data
     * 
     * @throws \InvalidArgumentException
     * 
     * @return void
     */
    public function validateData(array $data)
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException('data is not an array');
        }

        if (!isset($data['output']) || !is_array($data['output'])) {
            throw new \InvalidArgumentException('data[output] is not an array');
        }

        if (!isset($data['filename']) || empty($data['filename'])) {
            throw new \InvalidArgumentException('data[filename] must be a not empty string');
        }
    }
}

?>