<?php
/**
 * 
 */

namespace csvExport\Output;

require_once __DIR__.'/Output.php';

/**
 * Output as .csv
 */
class CSV extends Output
{
    /**
     * @return CSV
     */
    public static function newInstance()
    {
        return new self();
    }

    /**
     * @param string $filename
     * 
     * @return void
     */
    protected function setHeader($filename)
    {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$filename.'.csv');
    }

    /**
     * Output as csv
     * 
     * @param array $data
     * 
     * @throws \Exception
     * 
     * @return void
     */
    public function output(array $data)
    {
        $this->validateData($data);

        $this->setHeader($data['filename']);

        $csvHandle = fopen('php://output', 'w');

        foreach ($data['output'] as $array) {
            fputcsv($csvHandle, $array, ';');
        }

        fclose($csvHandle);
    }

    /**
     * Output as csv directly from Export
     * 
     * @param \csvExport\Export $export
     * 
     * @return void
     */
    public function outputFromExport(\csvExport\Export $export)
    {
        /**
         * Get values from profile
         */
        $exportResult = $export->getResolvedProfile();

        if (!$export->isOutputAllowed()) {
            throw new Exception($export->whyIsOutputNotAllowed());
        }

        /**
         * Set filename for output
         */
        $profileName = preg_replace('/[^a-z0-9-_\.]/i', '-', $exportResult->getProfileName());

        $this->setHeader($profileName.'_'.date("YmdHis"));

        $csvHandle = fopen('php://output', 'w');

        /**
         * Ouput column names
         */
        $sortedOutputNames = $exportResult->getSortedColumnNames();

        $sortedOutputNames['original'] = array_map(
            function($value) {
                return '('.utf8_decode($value).')';
            },
            $sortedOutputNames['original']
        );

        fputcsv($csvHandle, array_map('utf8_decode', $sortedOutputNames['humanized']), ';');
        fputcsv($csvHandle, $sortedOutputNames['original'], ';');


        foreach ($exportResult->getData() as $array) {
            $array = array_map('utf8_decode', $array);

            fputcsv($csvHandle, $array, ';');
        }

        fclose($csvHandle);
    }
}
?>