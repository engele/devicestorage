<?php

$sql = array(
    'save_profile' => "
      INSERT INTO `export_profiles` (`profile_name`, `profile`, `profile_sort`, `profile_where`, `user_id`) VALUES (?, ?, ?, ?, ?)
    ",

    'update_profile' => "
      UPDATE `export_profiles` SET `profile_name` = ?, `profile` = ?, `profile_sort` = ?, `profile_where` = ? 
      WHERE `id` = ?
    ",

    'select_profiles' => "
      SELECT `id`, `profile_name` FROM `export_profiles` WHERE `user_id` = ? OR `user_id` IS NULL
    ",
    
    'select_profile_by_id' => "
      SELECT `profile_name`, `profile`, `profile_sort`, `profile_where` FROM `export_profiles` WHERE `id` = ?
    ",

    'select_whole_profile_by_id' => "
      SELECT * FROM `export_profiles` WHERE `id` = ?
    ",

    'delete_profile' => "
      DELETE FROM `export_profiles` WHERE `id` = ?
    ",
);

$GLOBALS['sql'] = & $sql;

?>
