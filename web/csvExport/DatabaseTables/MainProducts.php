<?php

namespace DatabaseTables;

require_once __DIR__.'/Table.php';

use DatabaseTables\Table;

/**
 * Add this class to ExportTables::usedTables
 */
class MainProducts extends Table
{
    /**
     * If humanized name is given,
     * then it will be shown to the user instead of
     * displaying the columns original name
     * 
     * Musst be protected or public!
     * 
     * NOTE:
     *   Within the current wkv2-database some VARCHAR-fields hold date-values.
     *   To be able to do date-comparisons to those values add the following 
     *   to each affected column-array:
     * 
     *   'useInsteadOfName' => array( // possible keys are: select, where
     *       'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
     *   )
     * 
     *   ####
     *   # Use 'useInsteadOfName' only if realy necessary because it will slow down 
     *   # the execution time of the query quite a lot.
     *   ####
     * 
     *   ## Complete example for 'useInsteadOfName'##
     * 
     *   'column_original_name' => [
     *       'accessRestriction' => '',
     *       'humanizedName' => '',
     *       'useInsteadOfName' => array( // possible keys are: select, where
     *           'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
     *       )
     *   ]
     * 
     * @var array
     */
    protected $columns = array(
        'id' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'produkt' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Identifier'
        ),
        'group_id' => array(
            'accessRestriction' => '',
            'humanizedName' => 'category'
        ),
        'produkt_index' => array(
            'accessRestriction' => '',
            'humanizedName' => 'xdslServiceProfile'
        ),
        'gpon_download' => array(
            'accessRestriction' => '',
            'humanizedName' => 'gponDownload'
        ),
        'gpon_upload' => array(
            'accessRestriction' => '',
            'humanizedName' => 'gponUpload'
        ),
        'ethernet_up_down' => array(
            'accessRestriction' => '',
            'humanizedName' => 'ethernetUpDown'
        ),
        'down' => array(
            'accessRestriction' => '',
            'humanizedName' => 'download'
        ),
        'up' => array(
            'accessRestriction' => '',
            'humanizedName' => 'upload'
        ),
        'produkt_bezeichnung' => array(
            'accessRestriction' => '',
            'humanizedName' => 'name'
        ),
        'Preis_Netto' => array(
            'accessRestriction' => '',
            'humanizedName' => '',
            'useInsteadOfName' => array(
                'select' => 'FORMAT({column}, 2, "de_DE")',
            )
        ),
        'Preis_Brutto' => array(
            'accessRestriction' => '',
            'humanizedName' => '',
            'useInsteadOfName' => array(
                'select' => 'FORMAT({column}, 2, "de_DE")',
            )
        ),
        'active' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
    );

    /**
     * key = foreign-table.foreign.column
     * value = own-column
     * 
     * @var array
     */
    protected $relations = array(
        'produkte.id' => 'group_id',
        'customer.productname' => 'produkt',
    );

    /**
     * Table original name
     * Must follow the exact same case as this column was named in SQL-Database
     *
     * @return string
     */
    public function getOriginalName()
    {
        return 'produkte';
    }

    /**
     * humanized name for this table
     *
     * @return string
     */
    public function getHumanizedName()
    {
        return 'MainProducts';
    }
}

?>