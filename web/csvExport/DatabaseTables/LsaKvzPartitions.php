<?php

namespace DatabaseTables;

require_once __DIR__.'/Table.php';

use DatabaseTables\Table;

/**
 * Add this class to ExportTables::usedTables
 */
class LsaKvzPartitions extends Table
{
    /**
     * If humanized name is given,
     * then it will be shown to the user instead of
     * displaying the columns original name
     * 
     * Musst be protected or public!
     * 
     * NOTE:
     *   Within the current wkv2-database some VARCHAR-fields hold date-values.
     *   To be able to do date-comparisons to those values add the following 
     *   to each affected column-array:
     * 
     *   'useInsteadOfName' => array( // possible keys are: select, where
     *       'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
     *   )
     * 
     *   ####
     *   # Use 'useInsteadOfName' only if realy necessary because it will increase 
     *   # the execution time of the query quite a lot.
     *   ####
     * 
     *   ## Complete example for 'useInsteadOfName'##
     * 
     *   'column_original_name' => [
     *       'accessRestriction' => '',
     *       'humanizedName' => '',
     *       'useInsteadOfName' => array( // possible keys are: select, where
     *           'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
     *       )
     *   ]
     * 
     * @var array
     */
    protected $columns = array(
        'id' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'location_id' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'kvz_name' => array(
            'accessRestriction' => '',
            'humanizedName' => 'KVZ Name'
        ),
        'pin_amount' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'dpbo' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'default_VLAN' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'VLAN1' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'VLAN2' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'VLAN3' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
    );

    /**
     * key = foreign-table.foreign.column
     * value = own-column
     * 
     * @var array
     */
    protected $relations = array(
        'customers.lsa_kvz_partition_id' => 'id',
        'locations.id' => 'location_id',
    );

    /**
     * Table original name
     * Must follow the exact same case as this column was named in SQL-Database
     *
     * @return string
     */
    public function getOriginalName()
    {
        return 'lsa_kvz_partitions';
    }

    /**
     * humanized name for this table
     *
     * @return string
     */
    public function getHumanizedName()
    {
        return 'LSA-KVZ-Partitionen';
    }
}

?>