<?php

namespace DatabaseTables;

require_once __DIR__.'/Table.php';
//require_once __DIR__.'/../../vendor/wisotel/configuration/Configuration.php';

use \DatabaseTables\Table;
use \Wisotel\Configuration\Configuration;

/**
 * Add this class to ExportTables::usedTables
 */
class Customers extends Table
{
    /**
     * If humanized name is given,
     * then it will be shown to the user instead of
     * displaying the columns original name
     * 
     * Musst be protected or public!
     * 
     * NOTE:
     *   Within the current wkv2-database some VARCHAR-fields hold date-values.
     *   To be able to do date-comparisons to those values add the following 
     *   to each affected column-array:
     * 
     *   'useInsteadOfName' => array( // possible keys are: select, where
     *       'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
     *   )
     * 
     *   ####
     *   # Use 'useInsteadOfName' only if realy necessary because it will increase 
     *   # the execution time of the query quite a lot.
     *   ####
     * 
     *   ## Complete example for 'useInsteadOfName'##
     * 
     *   'column_original_name' => [
     *       'accessRestriction' => '',
     *       'humanizedName' => '',
     *       'useInsteadOfName' => array( // possible keys are: select, where
     *           'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
     *       )
     *   ]
     * 
     * @var array
     */
    protected $columns = array(
        'id' => array(
            'accessRestriction' => '',
            'humanizedName' => '',
            'useInsteadOfName' => array(
                'select' => 'CONCAT(\'=HYPERLINK("{startUrl}/index.php?menu=customer&func=vertrag&area=SwitchTechData&id=\', {column}, \'"; "\', {column}, \'")\')'
            )
        ),
        'customer_id' => array(
            'humanizedName' => 'Externe Kundenummer',
        ),
        'add_charge_terminal' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Zuzahlung Endgerät'
        ),
        'add_customer_on_fb' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Zusätzl. Mandant auf FB'
        ),
        'add_ip_address' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Anzahl zusätzl. IP-Adr.'
        ),
        'add_ip_address_cost' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Preis zusätzl. IP-Adr.'
        ),
        'add_phone_lines_cost' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Preis je Kanal'
        ),
        'add_phone_lines' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Zusätzliche Tel.-Ltg'
        ),
        'add_phone_nos' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Zusätzliche Tel.-Nr.'
        ),
        'add_port' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Zusätzl. Port.'
        ),
        'add_satellite' => array(
            'accessRestriction' => '',
            'humanizedName' => 'weitere Satelliten'
        ),
        'application' => array(
            'accessRestriction' => '',
            'humanizedName' => '_Status'
        ),
        'bank_account_bankname' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Name der Bank'
        ),
        'bank_account_bic' => array(
            'accessRestriction' => '',
            'humanizedName' => 'BIC'
        ),
        'bank_account_bic_new' => array(
            'accessRestriction' => '',
            'humanizedName' => 'BLZ'
        ),
        'bank_account_city' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Stadt'
        ),
        'bank_account_emailaddress' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'bank_account_holder_firstname' => array(
            'accessRestriction' => '',
            'humanizedName' => '__unused'
        ),
        'bank_account_holder_lastname' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kontoinhaber'
        ),
        'bank_account_iban' => array(
            'accessRestriction' => '',
            'humanizedName' => 'IBAN'
        ),
        'bank_account_number' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kontonummer'
        ),
        'bank_account_street' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'bank_account_streetno' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'bank_account_zipcode' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'bank_account_debitor' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Debitor'
        ),
        'bank_account_mandantenid' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Mandanten ID'
        ),
        'birthday' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Geburtstag',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'call_data_record' => array(
            'accessRestriction' => '',
            'humanizedName' => '__unused'
        ),
        'cancel_purtel' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Purteldaten kündigen'
        ),
        'cancel_purtel_for_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Daten kündigen zum'
        ),
        'cancel_tal' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Tal kündigen'
        ),
        'card_id' => array(
            'accessRestriction' => '',
            'humanizedName' => 'DSLAM Karte'
        ),
        'city' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Stadt'
        ),
        'clientid' => array(
            'accessRestriction' => '',
            'humanizedName' => '{companyName} Nr.',
            'useInsteadOfName' => array(
                'select' => 'CONCAT(\'=HYPERLINK("{startUrl}/index.php?menu=customer&func=vertrag&area=SwitchTechData&id=\', `customers`.`id`, \'"; "\', {column}, \'")\')'
            )
        ),
        'clienttype' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kundentyp'
        ),
        'company' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Firma'
        ),
        'commissioning_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Inbetriebnahme erledigt am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'commissioning_from' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Inbetriebnahme erledigt von'
        ),
        'commissioning_wish_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Inbetriebnahme geplant',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'connection_activation_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Anschluss aktiv seit',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'connection_address_equal' => array(
            'accessRestriction' => '',
            'humanizedName' => '__unused'
        ),
        'connection_city' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Stadt'
        ),
        'connection_fee_height' => array(
            'accessRestriction' => '',
            'humanizedName' => '__unused'
        ),
        'connection_fee_paid' => array(
            'accessRestriction' => '',
            'humanizedName' => '__unused'
        ),
        'connection_inactive_date' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'connection_installation' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Einrichtung Anschluss'
        ),
        'connection_street' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Straße'
        ),
        'connection_streetno' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Hausnummer',
            'useInsteadOfName' => array(
                'select' => 'CONCAT("\'", {column})'
            )
        ),
        'connection_zipcode' => array(
            'accessRestriction' => '',
            'humanizedName' => 'PLZ'
        ),
        'contract' => array(
            'accessRestriction' => '',
            'humanizedName' => '_Status'
        ),
        'contract_acknowledge' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Vert.Best. zugesandt am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'contract_change_to' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Vertragsänderung zum',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'contract_id' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'contract_online_date' => array(
            'accessRestriction' => '',
            'humanizedName' => '',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'contract_received_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Vertrag empf. am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'contract_sent_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Vertrag vers. am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'contract_version' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Vertrag unterschr. am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'contract_comment' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kommentar (Vertragsdetails)'
        ),
        'comment' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kommentar (Stammdaten)'
        ),
        'techdata_comment' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kommentar (Tech Data)'
        ),
        'credit_rating_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Boni-prüfung am'
        ),
        'credit_rating_link' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'credit_rating_ok' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Boni-prüfung Status'
        ),
        'customer_connect_info_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kunde über Anschlussdatum informiert am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'customer_connect_info_from' => array(
            'accessRestriction' => '',
            'humanizedName' => 'informiert von'
        ),
        'customer_connect_info_how' => array(
            'accessRestriction' => '',
            'humanizedName' => 'per'
        ),
        'direct_debit_allowed' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'district' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Ortsteil'
        ),
        'DPBO' => array(
            'accessRestriction' => '',
            'humanizedName' => 'DPBO'
        ),
        'dslam_arranged_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Am DSLAM/ CMTS eingerichtet am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'dslam_card_no' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'dslam_location' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'dslam_port' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kartenport'
        ),
        'dtag_line' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Leitungsbezeichnung (LBZ)'
        ),
        'dtagbluckage_fullfiled' => array(
            'accessRestriction' => '',
            'humanizedName' => 'DTAG StöMe behoben am'
        ),
        'dtagbluckage_fullfiled_date' => array(
            'accessRestriction' => '',
            'humanizedName' => '',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'dtagbluckage_sended' => array(
            'accessRestriction' => '',
            'humanizedName' => 'DTAG StöMe gesend. am'
        ),
        'dtagbluckage_sended_date' => array(
            'accessRestriction' => '',
            'humanizedName' => '',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'emailaddress' => array(
            'accessRestriction' => '',
            'humanizedName' => 'E-Mail Adresse'
        ),
        'eu_flat' => array(
            'accessRestriction' => '',
            'humanizedName' => 'EU-Flat'
        ),
        'exp_date_int' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kündigungstermin Internet',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'exp_date_phone' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kündigungstermin Telefon',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'fax' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Fax'
        ),
        'fb_vorab' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Fritzbox vorab'
        ),
        'final_purtelproduct_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Wechsel zum Endprodukt',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'firewall_router' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Firewall Router'
        ),
        'firmware_version' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Firmware-Version'
        ),
        'firstname' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Vorname'
        ),
        'flat_eu_network' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Business Flat EU, Kanäle'
        ),
        'flat_eu_network_cost' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Preis pro Kanal'
        ),
        'flat_german_network' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Business Flat Dt., Kanäle'
        ),
        'flat_german_network_cost' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Preis pro Kanal'
        ),
        'fordering_costs' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Versandkostenpauschale'
        ),
        'german_mobile' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Gespräche dt. Mobilnetz'
        ),
        'german_mobile_price' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Preis pro Minute'
        ),
        'german_network' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Gespräche dt. Festnetz'
        ),
        'german_network_price' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Preis pro Minute'
        ),
        'gf_branch' => array(
            'accessRestriction' => '',
            'humanizedName' => 'GF Hausanschluss'
        ),
        'gf_cabling' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Hausint. GF-Verkabelung in m'
        ),
        'gf_cabling_cost' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kosten für die GF-Verkabelung'
        ),
        'gutschrift' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Gutschrift'
        ),
        'gutschrift_till' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Gutschrift bis'
        ),
        'hd_plus_card' => array(
            'accessRestriction' => '',
            'humanizedName' => 'HD-Plus-Karten'
        ),
        'higher_availability' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Höhere Verfügbarkeit/ schnellere Entstörung'
        ),
        'higher_uplink_one' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Höherer Uplink bis 7,5 Mbit/s'
        ),
        'higher_uplink_two' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Höherer Uplink bis 12 Mbit/s'
        ),
        'house_connection' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kosten Hausanschluss'
        ),
        'implementation' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'international_calls_price' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'ip_address' => array(
            'accessRestriction' => '',
            'humanizedName' => 'IP Adresse'
        ),
        'ip_address_inet' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Feste IP-Adr. (Internet)'
        ),
        'ip_address_phone' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Feste IP-Adr. (Telefon)'
        ),
        'ip_range_id' => array(
            'accessRestriction' => '',
            'humanizedName' => 'IP-Block'
        ),
        'isdn_backup' => array(
            'accessRestriction' => '',
            'humanizedName' => 'ISDN Backup einmalig'
        ),
        'isdn_backup2' => array(
            'accessRestriction' => '',
            'humanizedName' => 'ISDN Backup monatl.'
        ),
        'Kommando_query' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'kvz_addition' => array(
            'accessRestriction' => '',
            'humanizedName' => 'KVz Zusatz'
        ),
        'kvz_name' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'kvz_standort' => array(
            'accessRestriction' => '',
            'humanizedName' => 'KVz-Standort'
        ),
        'kvz_toggle_no' => array(
            'accessRestriction' => '',
            'humanizedName' => 'KVZ Schaltnummer'
        ),
        'lapse_notice_date_inet' => array(
            'accessRestriction' => '',
            'humanizedName' => '',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'lapse_notice_date_tel' => array(
            'accessRestriction' => '',
            'humanizedName' => '',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'lastname' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Nachname'
        ),
        'line_identifier' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Line Identifier'
        ),
        'little_bar' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'location_id' => array(
            'accessRestriction' => '',
            'humanizedName' => 'DSLAM Standort'
        ),
        'lsa_kvz_partition_id' => array(
            'accessRestriction' => '',
            'humanizedName' => 'KVZ Bezeichnung'
        ),
        'lsa_pin' => array(
            'accessRestriction' => '',
            'humanizedName' => 'LSA Kontakt'
        ),
        'mac_address' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Mac Adresse'
        ),
        'media_converter' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Medien-Konverter'
        ),
        'mobile_flat' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Mobilfunk-Flat'
        ),
        'mobilephone' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Mobiltelefon'
        ),
        'moved' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'nb_canceled_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kdg.bestät. Tel zum',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'nb_canceled_date_phone' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kdg.bestät. DSL zum',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'network' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'network_id' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Netz'
        ),
        'new_clientid_permission' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'new_number_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Neue RN bestellt am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'new_number_from' => array(
            'accessRestriction' => '',
            'humanizedName' => 'bestellt von'
        ),
        'no_eze' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Einzugsermächtigung erteilt'
        ),
        'no_ping' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kein Ping',
            'useInsteadOfName' => array(
                'where' => 'COALESCE({column}, 0)'
            )
        ),
        'notice_period_internet_int' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kündigungsfrist Internet'
        ),
        'notice_period_internet_type' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kündigungsfrist Internet Typ'
        ),
        'notice_period_phone_int' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kündigungsfrist Telefon'
        ),
        'notice_period_phone_type' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kündigungsfrist Telefon Typ'
        ),
        'oldcontract_address_equal' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'oldcontract_city' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Altvertrag Stadt'
        ),
        'oldcontract_exists' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Altvertrag vorhanden'
        ),
        'oldcontract_firstname' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Altvertrag Vorname'
        ),
        'oldcontract_internet_client_cancelled' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Hat Kunde gekündigt?'
        ),
        'oldcontract_internet_clientno' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kundennummer Internet'
        ),
        'oldcontract_internet_provider' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Anbieter Internet'
        ),
        'oldcontract_lastname' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Altvertrag Nachname'
        ),
        'oldcontract_phone_client_cancelled' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Hat Kunde gekündigt?'
        ),
        'oldcontract_phone_clientno' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kundennummer Telefon'
        ),
        'oldcontract_phone_provider' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Anbieter Telefon'
        ),
        'oldcontract_phone_type' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'oldcontract_porting' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'oldcontract_street' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Altvertrag Straße'
        ),
        'oldcontract_streetno' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Altvertrag Hausnummer'
        ),
        'oldcontract_title' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Altvertrag Anrede'
        ),
        'oldcontract_zipcode' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Altvertrag PLZ'
        ),
        'paper_bill' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Papierrechnung'
        ),
        'password' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Internet-Passwort'
        ),
        'patch_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Gepatched am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'payment_performance' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Zahlungsverhalten'
        ),
        'phone_adapter' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Telefonadapter'
        ),
        'phone_number_added' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Nur RN Aufschaltung'
        ),
        'phone_number_block' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Einrichtung Rufnummernblock/blöcke Preis'
        ),
        'phoneareacode' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Vorwahl'
        ),
        'phoneentry_done_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Formular erstellt am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'phoneentry_done_from' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Formular erstellt von'
        ),
        'phonenumber' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Durchwahl'
        ),
        'porting' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'pppoe_config_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'PPPoE Konfig am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'productname' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Produktname'
        ),
        'prospect_supply_status' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Versorgbarkeit'
        ),
        'purtel_bluckage_fullfiled_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Purtel StöMe behob. am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'purtel_bluckage_sended_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Purtel StöMe gesend. am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'purtel_customer_reg_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kunde bei Purtel angelegt am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'purtel_data_record_id' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'purtel_delete_date' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'purtel_edit_done' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Aktivschaltung erledigt'
        ),
        'purtel_edit_done_from' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Aktivschaltung erledigt von'
        ),
        'purtel_login' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Purtel Login'
        ),
        'purtel_passwort' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Purtel Passwort'
        ),
        'purtel_product' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Purtel-Produkt'
        ),
        'purtelproduct_advanced' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Purtel-Produkt vorab'
        ),
        'rDNS_count' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Anzahl IP-Adressen'
        ),
        'rDNS_installation' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Einr. IP-Adr.bereich Preis'
        ),
        'reached_downstream' => array(
            'accessRestriction' => '',
            'humanizedName' => 'VLAN_ID'
        ),
        'reached_upstream' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Messung Upstream'
        ),
        'recommended_product' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'recruited_by' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Geworben von'
        ),
        'reg_answer_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Zuletzt bearbeitet am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'registration_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Interessent seit',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'remote_login' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Remote-Login'
        ),
        'remote_password' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Remote-Passwort'
        ),
        'routing_active' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Umroutung aktiv auf'
        ),
        'routing_active_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Umroutungsdatum',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'routing_deleted' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Umroutung gelöscht am'
        ),
        'routing_wish_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Aktivschaltungsdatum (Wunsch)'
        ),
        'second_tal' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Bestellung 2. Tal'
        ),
        'Service' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Service'
        ),
        'service_level' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Service Level'
        ),
        'service_level_send' => array(
            'accessRestriction' => '',
            'humanizedName' => 'SLA versendet am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'service_technician' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Servicetechniker'
        ),
        'sip_accounts' => array(
            'accessRestriction' => '',
            'humanizedName' => 'SIP-Accounts'
        ),
        'sofortdsl' => array(
            'accessRestriction' => '',
            'humanizedName' => 'DSL sofort'
        ),
        'special_conditions_from' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Sonderkonditionen ab',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'special_conditions_text' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Sonderkonditionen Text'
        ),
        'special_conditions_till' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Sonderkonditionen bis',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'special_termination_internet' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Sonderkündigung nötig?'
        ),
        'special_termination_phone' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Sonderkündigung nötig?'
        ),
        'Spectrumprofile' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Spectrumprofile'
        ),
        'stati_port_confirm_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Bestätigtes Portdat.',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'status' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'stnz_fb' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Fritzbox 7390'
        ),
        'street' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Straße'
        ),
        'streetno' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Hausnummer'
        ),
        'subnetmask' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Subnetzmaske'
        ),
        'tag' => array(
            'accessRestriction' => '',
            'humanizedName' => 'TAG verwenden',
        ),
        'tal_assigned_phoneno' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'tal_cancel_ack_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Tal-Kdg. bestätigt zum',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'tal_cancel_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Tal Kdg.datum',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'tal_canceled_for_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Tal gekündigt zum',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'tal_canceled_from' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Tal gekündigt von'
        ),
        'tal_dtag_assignment_no' => array(
            'accessRestriction' => '',
            'humanizedName' => 'DTAG-Vertragsnummer'
        ),
        'tal_dtag_revisor' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'tal_lended_fritzbox' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'tal_order_ack_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'TAL bestätigt zum',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'tal_order_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'TAL-Bestelldatum',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'tal_ordered_for_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'TAL bestellt zum',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'tal_order' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kunde benötigt'
        ),
        'tal_orderd_from' => array(
            'accessRestriction' => '',
            'humanizedName' => 'bestellt von'
        ),
        'tal_order_work' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Arbeiten beim Kunden'
        ),
        'tal_product' => array(
            'accessRestriction' => '',
            'humanizedName' => 'TAL-Produkt'
        ),
        'tal_releasing_operator' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'talorder' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'talorder_text' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'techdata_status' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'telefonbuch_eintrag' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kunde wünscht Telefonbucheintrag'
        ),
        'terminal_ready' => array(
            'accessRestriction' => '',
            'humanizedName' => 'FB konfiguriert am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'terminal_ready_from' => array(
            'accessRestriction' => '',
            'humanizedName' => 'eingerichtet von'
        ),
        'terminal_sended_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Endgerät versendet am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'terminal_sended_from' => array(
            'accessRestriction' => '',
            'humanizedName' => 'versendet von'
        ),
        'terminal_serialno' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Endgerät Seriennummer'
        ),
        'terminal_type' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Endgerät Typ'
        ),
        'terminal_type_exists' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'title' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Anrede'
        ),
        'tv_service' => array(
            'accessRestriction' => '',
            'humanizedName' => 'TV- Dienst'
        ),
        'tvs' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Terminverschiebung am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'tvs_date' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'tvs_from' => array(
            'accessRestriction' => '',
            'humanizedName' => 'TVS versendet von'
        ),
        'tvs_to' => array(
            'accessRestriction' => '',
            'humanizedName' => 'TVS erledigt'
        ),
        'vectoring' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'ventelo_confirmation_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Portierung erhalten am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'ventelo_port_letter_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Portierung versendet am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'ventelo_port_letter_from' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Portierung versendet von'
        ),
        'ventelo_port_letter_how' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Portierung versendet per'
        ),
        'ventelo_port_wish_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Portierungswunschdatum',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'ventelo_purtel_enter_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Port. eingestellt am',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'ventelo_purtel_enter_from' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Port. hochgeladen von'
        ),
        'version' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Version'
        ),
        'vlan_ID' => array(
            'accessRestriction' => '',
            'humanizedName' => 'VLAN_ID'
        ),
        'voucher' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'wisocontract_canceled_date' => array(
            'accessRestriction' => '',
            'humanizedName' => '{companyName} Vertrag gekündigt zum',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'wisotelno' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'zipcode' => array(
            'accessRestriction' => '',
            'humanizedName' => 'PLZ'
        ),
        'pppoe_pin' => array(
            'accessRestriction' => '',
            'humanizedName' => 'PPPoE-Pin'
        ),
        'wisocontract_cancel_extra' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Sonderkündigung'
        ),
        'wisocontract_cancel_input_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kündigung eingegangen am'
        ),
        'wisocontract_cancel_ack' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kündigung bestätigt am'
        ),
        'cancel_comment' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kommentar (Kündigung)'
        ), 
    );

    /**
     * key = foreign-table.foreign.column
     * value = own-column
     * 
     * @var array
     */
    protected $relations = array(
        'lsa_kvz_partitions.id' => 'lsa_kvz_partition_id',
        'networks.id' => 'network_id',
        'locations.id' => 'location_id',
        'purtel_account.cust_id' => 'id',
        'produkte.produkt' => 'productname',
        'ip_ranges.id' => 'ip_range_id',
        'customer_history.customer_id' => 'id',
        'customer_options.cust_id' => 'id',
        'customer_terminals.customer_id' => 'id',
    );

    /**
     * Table original name
     * Must follow the exact same case as this column was named in SQL-Database
     *
     * @return string
     */
    public function getOriginalName()
    {
        return 'customers';
    }

    /**
     * humanized name for this table
     *
     * @return string
     */
    public function getHumanizedName()
    {
        return sprintf('%s Kunden', Configuration::get('company'));
    }

    /**
     * Constructor
     * 
     * @return void
     */
    public function __construct()
    {
        global $StartURL;

        $this->columns['id']['useInsteadOfName']['select'] = str_replace(
            '{startUrl}',
            $StartURL,
            $this->columns['id']['useInsteadOfName']['select']
        );

        $this->columns['clientid']['useInsteadOfName']['select'] = str_replace(
            '{startUrl}',
            $StartURL,
            $this->columns['clientid']['useInsteadOfName']['select']
        );

        $this->columns['clientid']['humanizedName'] = str_replace(
            '{companyName}',
            Configuration::get('companyName'),
            $this->columns['clientid']['humanizedName']
        );

        $this->columns['wisocontract_canceled_date']['humanizedName'] = str_replace(
            '{companyName}',
            Configuration::get('companyName'),
            $this->columns['wisocontract_canceled_date']['humanizedName']
        );

        if (null !== $externalCustomerIdFieldName = \Wisotel\Configuration\Configuration::get('externalCustomerIdFieldName')) {
            $this->columns['customer_id']['humanizedName'] = $externalCustomerIdFieldName;
        }
    }
}

?>