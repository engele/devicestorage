<?php

namespace DatabaseTables;
use DatabaseTables\Table;

/**
 * Add this class to ExportTables::usedTables
 */
class Optionen extends Table
{
    /**
     * If humanized name is given,
     * then it will be shown to the user instead of
     * displaying the columns original name
     *
     * Musst be protected or public!
     *
     * NOTE:
     *   Within the current wkv2-database some VARCHAR-fields hold date-values.
     *   To be able to do date-comparisons to those values add the following
     *   to each affected column-array:
     *
     *   'useInsteadOfName' => array( // possible keys are: select, where
     *       'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
     *   )
     *
     *   ####
     *   # Use 'useInsteadOfName' only if realy necessary because it will increase
     *   # the execution time of the query quite a lot.
     *   ####
     *
     *   ## Complete example for 'useInsteadOfName'##
     *
     *   'column_original_name' => [
     *       'accessRestriction' => '',
     *       'humanizedName' => '',
     *       'useInsteadOfName' => array( // possible keys are: select, where
     *           'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
     *       )
     *   ]
     *
     * @var array
     */
    protected $columns = array(
        'option' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Produkcode'
        ),
        'option_bezeichnung' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Option Bezeichnung'
        ),
        'Preis_Netto' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Netto Preis',
            'useInsteadOfName' => array(
                'select' => 'FORMAT({column}, 2, "de_DE")'
            )
       ),
        'Preis_Brutto' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Brutto Preis',
            'useInsteadOfName' => array(
                'select' => 'FORMAT({column}, 2, "de_DE")'
            )
        ),
        'purtel_product_id' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Purtelprodukt ID'
        ),
        'contract_period' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kündigungsfrist',
        ),
        'active' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Aktiv',
        ),
    );

    /**
     * key = foreign-table.foreign.column
     * value = own-column
     *
     * @var array
     */
    protected $relations = array(
        'customer_options.opt_id' => 'option',
    );

    /**
     * Table original name
     * Must follow the exact same case as this column was named in SQL-Database
     *
     * @return string
     */
    public function getOriginalName()
    {
        return 'optionen';
    }

    /**
     * humanized name for this table
     *
     * @return string
     */
    public function getHumanizedName()
    {
        return 'Produktoptionen';
    }
}
?>
