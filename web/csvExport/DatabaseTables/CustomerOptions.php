<?php

namespace DatabaseTables;
use DatabaseTables\Table;

/**
 * Add this class to ExportTables::usedTables
 */
class CustomerOptions extends Table
{
    /**
     * If humanized name is given,
     * then it will be shown to the user instead of
     * displaying the columns original name
     *
     * Musst be protected or public!
     *
     * NOTE:
     *   Within the current wkv2-database some VARCHAR-fields hold date-values.
     *   To be able to do date-comparisons to those values add the following
     *   to each affected column-array:
     *
     *   'useInsteadOfName' => array( // possible keys are: select, where
     *       'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
     *   )
     *
     *   ####
     *   # Use 'useInsteadOfName' only if realy necessary because it will increase
     *   # the execution time of the query quite a lot.
     *   ####
     *
     *   ## Complete example for 'useInsteadOfName'##
     *
     *   'column_original_name' => [
     *       'accessRestriction' => '',
     *       'humanizedName' => '',
     *       'useInsteadOfName' => array( // possible keys are: select, where
     *           'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
     *       )
     *   ]
     *
     * @var array
     */
    protected $columns = array(
        'id' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'cust_id' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'opt_id' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Produktcode'
        ),
        'price_net' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kunden Netto Preis'
        ),
        'price_gross' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Kunden Brutto Preis'
        ),
        'activation_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Aktivierungsdatum',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
       ),
        'deactivation_date' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Deaktivierungsdatum',
            'useInsteadOfName' => array(
                'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
            )
        ),
        'tax_rate_id' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Steuersatz'
        ),
    );

    /**
     * key = foreign-table.foreign.column
     * value = own-column
     *
     * @var array
     */
    protected $relations = array(
        'customers.id' => 'cust_id',
        'optionen.option' => 'opt_id'
    );

    /**
     * Table original name
     * Must follow the exact same case as this column was named in SQL-Database
     *
     * @return string
     */
    public function getOriginalName()
    {
        return 'customer_options';
    }

    /**
     * humanized name for this table
     *
     * @return string
     */
    public function getHumanizedName()
    {
        return 'Kunden Produktoptionen';
    }
}
?>