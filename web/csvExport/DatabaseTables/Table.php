<?php

namespace DatabaseTables;

abstract class Table
{
    abstract public function getOriginalName();

    /**
     * Humanized name for this table
     * 
     * Fallback method
     * In case it wasn't overwritten return original name
     *
     * @return string
     */
    public function getHumanizedName()
    {
        return $this->getOriginalName();
    }

    /**
     * @param $accessGroup
     * 
     * @return array
     */
    public function getColumns($accessGroup = null)
    {
        $columns = array();

        foreach ($this->columns as $columnOriginalName => $info) {
            if (!is_null($accessGroup) && $accessGroup !== $info['accessRestriction']) {
                continue;
            }

            $columns[$columnOriginalName] = empty($info['humanizedName']) ? $columnOriginalName : $info['humanizedName'];
        }

        return $columns;
    }

    /**
     * @param string $originalName
     * 
     * @return string
     */
    public function getColumnHumanizedNameByOriginalName($originalName)
    {
        if (!empty($this->columns[$originalName]['humanizedName'])) {
            return $this->columns[$originalName]['humanizedName'];
        }

        return $originalName;
    }

    /**
     * Returns useInsteadOfName if specified, $originalName instead.
     * 
     * @param string $originalName
     * @param string $within
     * 
     * @return string
     */
    public function useColumnAs($originalName, $within)
    {
        if (isset($this->columns[$originalName]['useInsteadOfName']) 
            && isset($this->columns[$originalName]['useInsteadOfName'][$within]) 
            && !empty($this->columns[$originalName]['useInsteadOfName'][$within])) {

            return $this->columns[$originalName]['useInsteadOfName'][$within];
        }

        return $originalName;
    }

    /**
     * @return array
     */
    public function getRelations()
    {
        return $this->relations;
    }

    /**
     * @param string       $foreignTable
     * @param boolean|null $asQueryString
     * 
     * @return string|null
     */
    public function getRelationByForeignTable($foreignTable, $asQueryString = null)
    {
        foreach ($this->relations as $foreign => $own) {
            $foreign = explode('.', $foreign);

            if (0 === strcmp($foreign[0], $foreignTable)) {
                if ($asQueryString) {
                    return '`'.$foreign[0].'`.`'.$foreign[1].'` = `'.$this->getOriginalName().'`.`'.$own.'`';
                }

                return array('foreign' => $foreign[1], 'own' => $own);
            }
        }

        return null;
    }
}

?>