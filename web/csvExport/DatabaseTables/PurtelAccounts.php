<?php

namespace DatabaseTables;

require_once __DIR__.'/Table.php';

use DatabaseTables\Table;

/**
 * Add this class to ExportTables::usedTables
 */
class PurtelAccounts extends Table
{
    /**
     * If humanized name is given,
     * then it will be shown to the user instead of
     * displaying the columns original name
     * 
     * Musst be protected or public!
     * 
     * NOTE:
     *   Within the current wkv2-database some VARCHAR-fields hold date-values.
     *   To be able to do date-comparisons to those values add the following 
     *   to each affected column-array:
     * 
     *   'useInsteadOfName' => array( // possible keys are: select, where
     *       'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
     *   )
     * 
     *   ####
     *   # Use 'useInsteadOfName' only if realy necessary because it will slow down 
     *   # the execution time of the query quite a lot.
     *   ####
     * 
     *   ## Complete example for 'useInsteadOfName'##
     * 
     *   'column_original_name' => [
     *       'accessRestriction' => '',
     *       'humanizedName' => '',
     *       'useInsteadOfName' => array( // possible keys are: select, where
     *           'where' => 'STR_TO_DATE({column}, "%d.%m.%Y")'
     *       )
     *   ]
     * 
     * @var array
     */
    protected $columns = array(
        'id' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'purtel_login' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Purtel Username'
        ),
        'purtel_password' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Purtel Passwort'
        ),
        'master' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Ist Purtel Hauptaccount?'
        ),
        'porting' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'nummer' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Rufnummer'
        ),
        'nummernblock' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Nummernblock'
        ),
        'typ' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'cust_id' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Customer-Id'
        ),
        'phone_book_information' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'phone_book_digital_media' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'phone_book_inverse_search' => array(
            'accessRestriction' => '',
            'humanizedName' => ''
        ),
        'itemized_bill' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Einzelverbindungsnachweis'
        ),
        'itemized_bill_anonymized' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Einzelverbindungsnachweis anonymisiert'
        ),
        'itemized_bill_shorted' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Einzelverbindungsnachweis gekürzt'
        ),
        'area_code' => array(
            'accessRestriction' => '',
            'humanizedName' => 'Ortsnetzkennzahl'
        ),
    );

    /**
     * key = foreign-table.foreign.column
     * value = own-column
     * 
     * @var array
     */
    protected $relations = array(
        'customers.id' => 'cust_id',
    );

    /**
     * Table original name
     * Must follow the exact same case as this column was named in SQL-Database
     *
     * @return string
     */
    public function getOriginalName()
    {
        return 'purtel_account';
    }

    /**
     * humanized name for this table
     *
     * @return string
     */
    public function getHumanizedName()
    {
        return 'PurtelAccounts';
    }
}

?>