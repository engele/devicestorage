<?php

require_once $StartPath.'/'.$SelMenu.'/Export/ExportTables.php';

$export = new \csvExport\ExportTables();


/**
 * Handle post input
 */
if (isset($_POST['save'])) {
    $profile = array();
    $where = array();
    $sort = array();

    /**
     * verify post input
     * if input ok, add it to $profile
     */
    foreach ($export->getTables() as $table) {
        $tableName = $table->getOriginalName();

        /**
         * Verify posted columns
         */
        if (isset($_POST[$tableName]) && is_array($_POST[$tableName])) {
            $profile[$tableName] = array();

            $columns = $table->getColumns();

            foreach ($_POST[$tableName] as $columnName => $dummyValue) {
                if (isset($columns[$columnName])) {
                    $profile[$tableName][] = $columnName;

                    /**
                     * Sort
                     */
                    if (isset($_POST['sort'][$tableName][$columnName]) 
                        && !empty($_POST['sort'][$tableName][$columnName])) {

                        if (!isset($sort[$tableName])) {
                            $sort[$tableName] = array(
                                $columnName => (int) $_POST['sort'][$tableName][$columnName]
                            );
                        } else {
                            $sort[$tableName][$columnName] = (int) $_POST['sort'][$tableName][$columnName];
                        }
                    }
                }
            }
        }

        /**
         * Verify posted where-criteria
         */
        if (isset($_POST['where'][$tableName])) {
            $where[$tableName] = array();

            $columns = $table->getColumns();

            foreach ($_POST['where'][$tableName] as $columnName => $value) {
                $value = !isset($columns[$columnName]) ? null : addslashes(strip_tags(trim($value)));

                if (!empty($value)) {
                    $where[$tableName][$columnName] = $value;
                }
            }

            if (empty($where[$tableName])) {
                unset($where[$tableName]);
            }
        }
    }

    try {
        if (empty($profile)) {
            throw new Exception('Es wurde nichts ausgewählt.');
        }

        /**
         * Duplicated entry (index.php). Remember when changing this.
         */
        $profileName = !isset($_POST['profile-name']) ? null : preg_replace('/[^a-z0-9-_\s\.]/i', '', $_POST['profile-name']);

        if (empty($_POST['profile-name'])) {
            throw new Exception('Es wurde kein gültiger Name für dieses Profil eingegeben.');
        }

        if (!empty($where)) {
            $whereCriteria = isset($_POST['where_criteria']) && $_POST['where_criteria'] == 'or' ? 'or' : 'and';
            $where = array('criteria' => $whereCriteria, 'data' => $where);
            $where = serialize($where);
        } else {
            $where = null;
        }

        $sort = empty($sort) ? null : serialize($sort);


        /**
         * userId
         * For now, it is always null
         */
        $userId = null;

        $profile = serialize($profile);


        /**
         * Update or create new profile?
         */
        if (isset($_GET['profile-id'])) {
            $statement = $db->prepare($sql['update_profile']);
            $statement->bind_param('ssssi', $profileName, $profile, $sort, $where, $_GET['profile-id']);
        } else {
            $statement = $db->prepare($sql['save_profile']);
            $statement->bind_param('ssssi', $profileName, $profile, $sort, $where, $userId);
        }
        

        if ($statement->execute()) {
            $successInfo = 'create-new';

            if (isset($_GET['profile-id'])) {
                $successInfo = 'edit';
                $profileId = $_GET['profile-id'];
            } else {
                $profileId = $db->insert_id;
            }

            header('Location: ?menu=csvExport&success-info='.$successInfo.'&preselect='.$profileId);
            exit;
        }

        throw new Exception('Speichern des Profils fehlgeschlagen.');
    } catch(Exception $e) {
        $postError = $e->getMessage();
    }
} else {
    /**
     * default view
     */


    /**
     * Edit profile
     * Select profile from database
     * 
     * Todo:
     * Add "and user_id = .." to sql
     */
    if (isset($_GET['profile-id'])) {
        $statement = $db->prepare($sql['select_profile_by_id']);
        $statement->bind_param('i', $_GET['profile-id']);
        $statement->execute();

        $profilesResult = $statement->get_result();

        $profile = $profilesResult->fetch_assoc();
        $profileName = $profile['profile_name'];

        $where = unserialize($profile['profile_where']);
        $whereCriteria = $where['criteria'];
        $where = $where['data'];

        $sort = unserialize($profile['profile_sort']);

        $profile = unserialize($profile['profile']);

        $profilesResult->free();
        $statement->close();
    }

    if (!isset($whereCriteria)) {
        $whereCriteria = 'and';
    }
}
?>


<h1>CSV-Export (Profil anlegen)</h1>

<?php
if (isset($postError)) {
    echo '<h2 style="color: red;">'.$postError.'</h2>';
}
?>

<form method="post" action="">
    <div class="accordion-single">
        <h3>Info: Eingrenz-Parameter</h3>
        <div>
            <div class="float-wrap">
                <p align="right">
                    <a href="<?php echo $StartURL.'/'.$SelMenu; ?>/_miscellaneous/CSV-Export.doc.pdf" 
                        target="_blank" title="Dokumentation herunterladen" class="highlight-profile-entry">
                        Dokumentation herunterladen (pdf)
                    </a>
                    <br>
                    <br>
                </p>
                <div class="width-50">
                    <u>Mögliche Eingrenz-Parameter</u><br />
                    <i>Wert</i><br />
                    nicht <i>Wert</i><br />
                    > <i>Wert</i><br />
                    < <i>Wert</i><br />
                    >= <i>Wert</i><br />
                    <= <i>Wert</i><br />
                    wie %<i>Wert</i>%<br />
                    nicht wie %<i>Wert</i>%<br />
                    leer<br />
                    nicht leer<br />
                    null<br />
                    leerOderNull<br />
                    zwischen <i>Wert</i> und <i>Wert</i><br />
                </div>
                <div class="width-50">
                    <u>Mögliche Werte für "<i>Wert</i>"</u><br />
                    datum(Tag.Monat.Jahr)<br />
                    jetzt()<br />
                    [Platzhalterbeschreibung]<br />
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />

    <div class="accordion-single" style="display: none" id="list-active">
        <h3>Aktuelle Auswahl</h3>
        <div>
            <br />
            <table><tr><td colspan="2">Anzeigen?</td><td>Eingrenzen?</td><td>Anzeige-Reihenfolge</td></tr></table>
        </div>
    </div>

    <br />
    <br />

    <?php
    /**
     * If a column is used in grouped columns, don't list it again.
     * Because, the second (other listed) input will override the first (grouped).
     */
    $hideColumns = array();

    /**
     * Get grouped columns
     */
    $groupedColumns = $export->getGroupedColumns();

    if (!empty($groupedColumns)) {
        echo '<div class="accordion-single">';

        foreach ($groupedColumns as $groupHeadline => $columns) {
            echo '<h3>'.$groupHeadline.'</h3>'
                .'<div><br />'
                .'<table><tr><td colspan="2">Anzeigen?</td><td>Eingrenzen?</td><td>Anzeige-Reihenfolge</td></tr>';

            foreach ($columns as $tableAndColumn) {
                $table = $tableAndColumn[0];
                $columnOriginalName = $tableAndColumn[1];
                $humanizedName = $table->getColumnHumanizedNameByOriginalName($columnOriginalName);

                /**
                 * Add column to $hideColumns
                 */
                if (!isset($hideColumns[$table->getOriginalName()])) {
                    $hideColumns[$table->getOriginalName()] = array();
                }

                $hideColumns[$table->getOriginalName()][$columnOriginalName] = true;


                $highlightEntry = false;
                $checked = '';

                if (isset($profile[$table->getOriginalName()])) {
                    if (false !== array_search($columnOriginalName, $profile[$table->getOriginalName()])) {
                        $checked = ' checked="true"';
                        $highlightEntry = true;
                    }
                }

                $whereValue = '';

                if (isset($where[$table->getOriginalName()][$columnOriginalName])) {
                    $whereValue = $where[$table->getOriginalName()][$columnOriginalName];
                    $highlightEntry = true;
                }

                $sortValue = '';

                if (isset($sort[$table->getOriginalName()]) 
                    && isset($sort[$table->getOriginalName()][$columnOriginalName])) {

                    $sortValue = $sort[$table->getOriginalName()][$columnOriginalName];
                    $highlightEntry = true;
                }

                $highlightStyle = '';

                if ($highlightEntry) {
                    $highlightStyle = ' class="highlight-profile-entry"';
                }

                echo '<tr>'
                    .'<td><input type="checkbox" name="'.$table->getOriginalName().'['.$columnOriginalName.']"'
                    .$checked.' value="y" /></td>'
                    .'<td'.$highlightStyle.'>'.$humanizedName.' <span class="column-original-name">('.$columnOriginalName.' : '
                    .$table->getOriginalName().')</span></td>'
                    .'<td><input type="text" name="where['.$table->getOriginalName().']['.$columnOriginalName.']" value="'
                    .$whereValue.'" /></td>'
                    .'<td align="center"><input class="input-sort" type="text" '
                    .'name="sort['.$table->getOriginalName().']['.$columnOriginalName.']" value="'.$sortValue.'" />'
                    .'</td>'
                    .'</tr>';
            }

            echo '</table></div>';
        }

        echo '</div><br /><br />';
    }
    ?>

    <div class="accordion-single">
        <?php
        foreach ($export->getTables() as $table) {
            echo '<h3>'.$table->getHumanizedName().'</h3>'
                .'<div><br />'
                .'<table><tr><td colspan="2">Anzeigen?</td><td>Eingrenzen?</td><td>Anzeige-Reihenfolge</td></tr>'
            ;

            /**
             * $table->getColumns(only_for_this_role)
             */
            foreach ($table->getColumns() as $columnOriginalName => $humanizedName) {
                /**
                 * Don't list column if it's in $hideColumns
                 */
                if (isset($hideColumns[$table->getOriginalName()][$columnOriginalName])) {
                    continue;
                }
                

                $highlightEntry = false;
                $checked = '';

                if (isset($profile[$table->getOriginalName()])) {
                    if (false !== array_search($columnOriginalName, $profile[$table->getOriginalName()])) {
                        $checked = ' checked="true"';
                        $highlightEntry = true;
                    }
                }

                $whereValue = '';

                if (isset($where[$table->getOriginalName()][$columnOriginalName])) {
                    $whereValue = $where[$table->getOriginalName()][$columnOriginalName];
                    $highlightEntry = true;
                }

                $sortValue = '';

                if (isset($sort[$table->getOriginalName()]) 
                    && isset($sort[$table->getOriginalName()][$columnOriginalName])) {

                    $sortValue = $sort[$table->getOriginalName()][$columnOriginalName];
                    $highlightEntry = true;
                }

                $highlightStyle = '';

                if ($highlightEntry) {
                    $highlightStyle = ' class="highlight-profile-entry"';
                }

                echo '<tr>'
                    .'<td><input type="checkbox" name="'.$table->getOriginalName().'['.$columnOriginalName.']"'
                    .$checked.' value="y" /></td>'
                    .'<td'.$highlightStyle.'>'.$humanizedName.' <span class="column-original-name">('.$columnOriginalName.')</span></td>'
                    .'<td><input type="text" name="where['.$table->getOriginalName().']['.$columnOriginalName.']" value="'
                    .$whereValue.'" /></td>'
                    .'<td align="center"><input class="input-sort" type="text" '
                    .'name="sort['.$table->getOriginalName().']['.$columnOriginalName.']" value="'.$sortValue.'" />'
                    .'</td>'
                    .'</tr>'
                ;
            }

            echo '</table></div>';
        }

        $templatingAssets = $this->get('templating.helper.assets');
        ?>
    </div>
    <br />
    <br />

    <div class="box">
        <h3></h3>
        <div>
            Profilname: <input type="text" name="profile-name" value="<?php echo isset($profileName) ? $profileName : '' ?>" />
            <br />
            Eingrenzen verwenden als: 
            <input type="radio" name="where_criteria" value="and"<?php echo ($whereCriteria == 'or' ? '' : ' checked="true"') ?> /> UND 
            <input type="radio" name="where_criteria" value="or"<?php echo ($whereCriteria == 'or' ? ' checked="true"' : '') ?> /> ODER
            <br />
            <br />
            <button name="back" onclick="backButton()" type="button">
                <img alt="" src="<?php echo $templatingAssets->getUrl('_img/Back.png'); ?>" height="12px"> Zurück
            </button>&nbsp;&nbsp;&nbsp;&nbsp;
            <button name="save" type="submit">
                <img alt="" src="<?php echo $templatingAssets->getUrl('_img/Yes.png'); ?>" height="12px"> Speichern
            </button>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <button name="reset" type="reset">
                <img alt="" src="<?php echo $templatingAssets->getUrl('_img/Erase.png'); ?>" height="12px"> Zurücksetzten
            </button>
        </div>
     </div>
</form>
