<?php
/**
 * 
 */

namespace csvExport\Lib\PPPeE;

require_once __DIR__.'/../Lib.php';

use \csvExport\Lib\Lib;

/**
 * 
 */
class PppeeConfig extends Lib
{
    /**
     * Profile must use these columns.
     * Uses Assert-Checks to ensure all columns provide correct values.
     *      
     *      Always end Assert with () !
     * 
     *      'database' => array(
     *          'column' => array(
     *              'notEmpty()',
     *              'eq(value="some string")',
     *              ...
     *          ),
     *          ...
     *      ),
     *      ...
     * 
     * @var array
     * 
     * @see https://github.com/webmozart/assert
     */
    protected $requiredColumns = array(
        
    );

    /**
     * This value will be shown if the used profile isn't using all columns specified in $requiredColumns
     * 
     * @var string
     */
    protected $messageProfileNotUseingAllRequiredColumns = 'To be able to use PPPeE-Config your profile must use the following columns:';

    /**
     * @param integer $profileId
     * 
     * @throws \Exception
     * 
     * @return array
     */
    public function process($profileId)
    {
        
    }
}

?>