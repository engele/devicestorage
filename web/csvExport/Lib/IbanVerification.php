<?php
/**
 * 
 */

namespace csvExport\Lib;

require_once __DIR__.'/Lib.php';

use \csvExport\Lib\Lib;

/**
 * 
 */
class IbanVerification extends Lib
{
    /**
     * Profile must use these columns.
     * Uses Assert-Checks to ensure all columns provide correct values.
     *      
     *      Always end Assert with () !
     * 
     *      'database' => array(
     *          'column' => array(
     *              'notEmpty()',
     *              'eq(value="some string")',
     *              ...
     *          ),
     *          ...
     *      ),
     *      ...
     * 
     * @var array
     * 
     * @see https://github.com/webmozart/assert
     */
    protected $requiredColumns = array(
        'customers' => array(
            'bank_account_iban' => array(
                'notEmpty()'
            ),
        ),
    );

    /**
     * This value will be shown if the used profile isn't using all columns specified in $requiredColumns
     * 
     * @var string
     */
    protected $messageProfileNotUseingAllRequiredColumns = 'To be able to use IBAN-Verification-Export your profile must use the following columns:';

    /**
     * Only used to easily keep track of what happened
     * 
     * @var array
     */
    protected $global = array(
        'verificationsSuccessful' => array(
            'message' => 'Prüfungen erfolgreich = ', 'counter' => 0
        ),
        'verificationsFailed' => array(
            'message' => 'Prüfungen fehlgeschlagen = ', 'counter' => 0
        ),
        'notTestable' => array(
            'message' => 'Nicht prüfbar = ', 'counter' => 0
        ),
        'countAll' => array(
            'message' => 'Datensätze gesamt = ', 'counter' => 0
        ),
    );

    /**
     * Actual verification
     * 
     * @param string $iban
     * 
     * @return string
     */
    public function isValid($iban)
    {
        $iban = str_replace(' ', '', $iban);

        if (1 !== preg_match('/^[A-Z]{2}/', $iban)) {
            $this->global['verificationsFailed']['counter']++;

            return 'corrupted :: not 2 uppercase letters at the beginning';
        }

        $calcIban = $iban;
        $letters = substr($calcIban, 0, 2);
        $calcIban = substr($calcIban, 4);
        $onlyBlzAndNumber = $calcIban;

        $lettersToNumbers = '';

        $aInAsciiIs = 65;
        $lettersToNumbersAIs = 10;

        for ($i = 0; $i < strlen($letters); $i++) {
            $x = ord($letters{$i}) - $aInAsciiIs;
            $lettersToNumbers .= $lettersToNumbersAIs + $x;
        }

        $calcIban .= $lettersToNumbers.'00';

        $calcIban2 = $calcIban;

        /*while (strlen($calcIban2) > 0) {
            $block = (int) substr($calcIban2, 0, 9);
            $calcIban2 = substr($calcIban2, 9);

            //$x = floor($block / 97) * 97;
            //$remainder = $block - $x;
            $remainder = $block % 97;

            $remainder = str_pad((string) $remainder, 2, '0');

            if (strlen($calcIban2) > 0) {
                $calcIban2 = $remainder.$calcIban2;
            }
        }*/
        $remainder = \bcmod($calcIban2, '97');

        $verificationNumber = 98 - (int) $remainder;
        $verificationNumber = str_pad((string) $verificationNumber, 2, '0');

        $propperIban = $letters.$verificationNumber.$onlyBlzAndNumber;

        if (0 !== strcmp($propperIban, $iban)) {
            $this->global['verificationsFailed']['counter']++;

            return 'corrupted :: invalid error checking number';
        }

        $this->global['verificationsSuccessful']['counter']++;

        return 'ok';
    }

    /**
     * @param integer $profileId
     * 
     * @throws \Exception
     * 
     * @return array
     */
    public function process($profileId)
    {
        $return = array(
            'filename' => '',
            'output' => array(),
        );

        // Get values from profile
        $exportResult = $this->getExportData($profileId);
        
        // Set filename for output
        $return['filename'] = preg_replace('/[^a-z0-9-_\.]/i', '-', $exportResult->getProfileName()).'_'.date("YmdHis");

        // Ouput column names
        $sortedOutputNames = $exportResult->getSortedColumnNames();

        $sortedOutputNames['original'] = array_map(
            function($value) {
                return '('.utf8_decode($value).')';
            },
            $sortedOutputNames['original']
        );

        $sortedOutputNames['humanized'][] = 'IBAN-Status';
        $sortedOutputNames['original'][] = 'IBAN-Status';

        $return['output'][] = array_map('utf8_decode', $sortedOutputNames['humanized']);
        $return['output'][] = $sortedOutputNames['original'];


        foreach ($exportResult->getData() as $array) {
            try {
                // Test the profile values against the specified Assert's in $requiredColumns
                $this->testValuesAgainstRequiredColumns($exportResult, $array);

                $iban = $array[$exportResult->getValueKey('customers', 'bank_account_iban')];

                // Run verification, add it's result to output
                $array['verificationResult'] = $this->isValid($iban);
            } catch (\InvalidArgumentException $e) {
                // testValuesAgainstRequiredColumns() throws this.

                $this->global['notTestable']['counter']++;
                $array['verificationResult'] = 'not testable';
            }

            $this->global['countAll']['counter']++;

            // Add values from profile to output
            $return['output'][] = array_map('utf8_decode', $array);
        }

        // Adds the collected status information to output
        $return['output'][] = array_fill(0, count($array), '____________');

        $dummyData = array_fill(0, count($array), '');

        foreach ($this->global as $info) {
            $dummyData[1] = utf8_decode($info['message']);
            $dummyData[2] = $info['counter'];

            $return['output'][] = array_map('utf8_encode', $dummyData);
        }

        return $return;
    }
}

?>