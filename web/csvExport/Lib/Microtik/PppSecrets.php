<?php
/**
 * 
 */

namespace csvExport\Lib\Microtik;

require_once __DIR__.'/../Lib.php';

use \csvExport\Lib\Lib;

/**
 * 
 */
class PppSecrets extends Lib
{
    /**
     * Profile must use these columns.
     * Uses Assert-Checks to ensure all columns provide correct values.
     *      
     *      Always end Assert with () !
     * 
     *      'database' => array(
     *          'column' => array(
     *              'notEmpty()',
     *              'eq(value="some string")',
     *              ...
     *          ),
     *          ...
     *      ),
     *      ...
     * 
     * @var array
     * 
     * @see https://github.com/webmozart/assert
     */
    protected $requiredColumns = array(
        'customers' => array(
            'clientid' => array(
                'notEmpty()'
            ),
            'password' => array(
                'notEmpty()',
            ),
            'ip_address' => array(
                'notEmpty()',
            ),
        ),
    );

    /**
     * This value will be shown if the used profile isn't using all columns specified in $requiredColumns
     * 
     * @var string
     */
    protected $messageProfileNotUseingAllRequiredColumns = 'To be able to use your profile must use the following columns:';

    /**
     * @param integer $profileId
     * 
     * @throws \Exception
     * 
     * @return array
     */
    public function process($profileId)
    {
        $return = array(
            'filename' => '',
            'output' => array(),
        );

        /**
         * Get values from profile
         */
        $exportResult = $this->getExportData($profileId);
        
        /**
         * Set filename for output
         */
        $return['filename'] = preg_replace('/[^a-z0-9-_\.]/i', '-', $exportResult->getProfileName()).'_'.date("YmdHis");

        /**
         * Ouput column names
         */
        $sortedOutputNames = $exportResult->getSortedColumnNames();

        $sortedOutputNames['original'] = array_map(
            function($value) {
                return '('.utf8_decode($value).')';
            },
            $sortedOutputNames['original']
        );

        $sortedOutputNames['humanized'][] = 'PPP-Secret';
        $sortedOutputNames['original'][] = 'PPP-Secret';

        $return['output'][] = array_map('utf8_decode', $sortedOutputNames['humanized']);
        $return['output'][] = $sortedOutputNames['original'];


        foreach ($exportResult->getData() as $array) {
            /**
             * Test the profile values against the specified Assert's in $requiredColumns
             */
            $this->testValuesAgainstRequiredColumns($exportResult, $array);

            $ipAddress = $array[$exportResult->getValueKey('customers', 'ip_address')];
            
            $matches = [];
            $clientId = $array[$exportResult->getValueKey('customers', 'clientid')];
            
            preg_match('/; "([^"]+)/', $clientId, $matches);
            
            $clientId = $matches[1];

            $password = $array[$exportResult->getValueKey('customers', 'password')];

            /**
             * Run actual ping add it's result to output
             */
            $array['ppp-secret'] = sprintf('/ppp secret add name=%s service=pppoe password="%s" profile=PPPoE remote-address=%s;',
                $clientId,
                str_replace(['?', '$'], ['\\?', '\\$'], $password),
                $ipAddress
            );

            /**
             * Add values from profile to output
             */
            $return['output'][] = array_map('utf8_decode', $array);
        }

        return $return;
    }
}

?>