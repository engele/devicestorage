<?php
/**
 * 
 */

namespace csvExport\Lib\Sap;

require_once __DIR__.'/../Lib.php';
require __DIR__.'/../IbanVerification.php';

use csvExport\Lib\Lib;
use csvExport\Lib\IbanVerification;

/**
 * 
 */
class DebitorsCreation extends Lib
{
    /**
     * Profile must use these columns.
     * Uses Assert-Checks to ensure all columns provide correct values.
     *      
     *      Always end Assert with () !
     * 
     *      'database' => array(
     *          'column' => array(
     *              'notEmpty()',
     *              'eq(value="some string")',
     *              ...
     *          ),
     *          ...
     *      ),
     *      ...
     * 
     * @var array
     * 
     * @see https://github.com/webmozart/assert
     */
    protected $requiredColumns = array(
        'customers' => array(
            'id' => array(),
            'title' => array(
                'notEmpty()',
            ),
            'firstname' => array(
                'notEmpty()',
            ),
            'lastname' => array(
                'notEmpty()',
            ),
            'street' => array(
                'notEmpty()',
            ),
            'streetno' => array(),
            'city' => array(
                'notEmpty()',
            ),
            'zipcode' => array(
                'notEmpty()',
            ),
            'no_eze' => array(),
            'bank_account_iban' => array(),
            'bank_account_holder_lastname' => array(),
        ),
    );

    /**
     * This value will be shown if the used profile isn't using all columns specified in $requiredColumns
     * 
     * @var string
     */
    protected $messageProfileNotUseingAllRequiredColumns = 'To be able to use SAP-Debitors-Creation-Export your profile must use the following columns:';

    /**
     * Returns filtered title
     * 
     * @param string $title
     * 
     * @return string
     */
    protected function filterTitle($title)
    {
        switch ($title) {
            case 'Frau und Herr':
            case 'Herr und Frau':
                return 'Herr/Frau';

            case 'Herr Dr.':
            case 'Herr Prof. Dr.':
            case 'Herr':
                return 'Herr';

            case 'Frau Dr.':
            case 'Frau':
                return 'Frau';

            case 'Firma':
            case null:
                return $title;

            default:
                throw new \InvalidArgumentException('Invalid value in title');
        }
    }

    /**
     * Returns filtered id
     * 
     * @param string $id
     * 
     * @return string
     */
    protected function filterId($id)
    {
        return preg_replace('/.*; "(\d+)"\)$/', '$1', $id);
    }

    /**
     * @param integer $profileId
     * 
     * @throws \Exception
     * 
     * @return array
     */
    public function process($profileId)
    {
        $return = array(
            'filename' => '',
            'output' => array(),
        );

        // Get values from profile
        $exportResult = $this->getExportData($profileId);
        
        // Set filename for output
        $return['filename'] = preg_replace('/[^a-z0-9-_\.]/i', '-', $exportResult->getProfileName()).'_'.date("YmdHis");

        // Ouput column names
        $sortedOutputNames = $exportResult->getSortedColumnNames();

        $sortedOutputNames['original'] = array_map(
            function($value) {
                return '('.utf8_decode($value).')';
            },
            $sortedOutputNames['original']
        );

        $titleKey = $exportResult->getValueKey('customers', 'title');
        $firstnameKey = $exportResult->getValueKey('customers', 'firstname');
        $lastnameKey = $exportResult->getValueKey('customers', 'lastname');
        $ibanKey = $exportResult->getValueKey('customers', 'bank_account_iban');
        $noEzeKey = $exportResult->getValueKey('customers', 'no_eze');
        $bankLastnameKey = $exportResult->getValueKey('customers', 'bank_account_holder_lastname');
        $streetKey = $exportResult->getValueKey('customers', 'street');
        $streetnoKey = $exportResult->getValueKey('customers', 'streetno');

        $lastnameHeadlineKey = array_search('(lastname)', $sortedOutputNames['original']);

        if (false === $lastnameHeadlineKey) {
            $lastnameHeadlineKey = array_search('(customers.lastname)', $sortedOutputNames['original']);
        }

        $sortedOutputNames['humanized'][$lastnameHeadlineKey] = 'Suchbegriff';

        $bankLastnameHeadlineKey = array_search('(bank_account_holder_lastname)', $sortedOutputNames['original']);

        if (false === $bankLastnameHeadlineKey) {
            $bankLastnameHeadlineKey = array_search('(customers.bank_account_holder_lastname)', $sortedOutputNames['original']);
        }

        $sortedOutputNames['humanized'][$bankLastnameHeadlineKey] = 'Abweichender Kontoinhaber';

        $idHeadlineKey = array_search('(id)', $sortedOutputNames['original']);

        if (false === $idHeadlineKey) {
            $idHeadlineKey = array_search('(customers.id)', $sortedOutputNames['original']);
        }

        $sortedOutputNames['humanized'][$idHeadlineKey] = 'Kontonummer IKV';

        $sortedOutputNames['humanized'][] = 'Name1';
        $sortedOutputNames['original'][] = 'Name1';
        $sortedOutputNames['humanized'][] = 'Name2';
        $sortedOutputNames['original'][] = 'Name2';
        $sortedOutputNames['humanized'][] = 'Zahlweg';
        $sortedOutputNames['original'][] = 'Zahlweg';
        $sortedOutputNames['humanized'][] = 'Strasse / HsNr.';
        $sortedOutputNames['original'][] = 'Strasse / HsNr.';
        /*$sortedOutputNames['humanized'][] = 'IBAN Status';
        $sortedOutputNames['original'][] = 'IBAN Status';*/

        $firstnameHeadlineKey = array_search('(firstname)', $sortedOutputNames['original']);

        if (false === $firstnameHeadlineKey) {
            $firstnameHeadlineKey = array_search('(customers.firstname)', $sortedOutputNames['original']);
        }

        unset($sortedOutputNames['humanized'][$firstnameHeadlineKey], $sortedOutputNames['original'][$firstnameHeadlineKey]);

        $noEzeHeadlineKey = array_search('(no_eze)', $sortedOutputNames['original']);

        if (false === $noEzeHeadlineKey) {
            $noEzeHeadlineKey = array_search('(customers.no_eze)', $sortedOutputNames['original']);
        }

        unset($sortedOutputNames['humanized'][$noEzeHeadlineKey], $sortedOutputNames['original'][$noEzeHeadlineKey]);

        $streetHeadlineKey = array_search('(street)', $sortedOutputNames['original']);

        if (false === $streetHeadlineKey) {
            $streetHeadlineKey = array_search('(customers.street)', $sortedOutputNames['original']);
        }

        unset($sortedOutputNames['humanized'][$streetHeadlineKey], $sortedOutputNames['original'][$streetHeadlineKey]);

        $streetnoHeadlineKey = array_search('(streetno)', $sortedOutputNames['original']);

        if (false === $streetnoHeadlineKey) {
            $streetnoHeadlineKey = array_search('(customers.streetno)', $sortedOutputNames['original']);
        }

        unset($sortedOutputNames['humanized'][$streetnoHeadlineKey], $sortedOutputNames['original'][$streetnoHeadlineKey]);

        $testValuesAgainstRequiredColumnsFailes = 0;
        /*$ibanFailed = 0;

        $ibanVerification = new IbanVerification($this->export);*/

        //$encoding = 'UTF-8';
        $encoding = null;

        $newSortedOutputNames = array(
            'humanized' => array(),
            'original' => array(),
        );

        $newSort = [
            'id',
            'title',
            'Name1',
            'Name2',
            'Suchbegriff',
            'Strasse / HsNr.',
            'zipcode',
            'city',
            'bank_account_holder_lastname',
            'bank_account_iban',
            'Zahlweg',
        ];

        foreach ($newSort as $key => $column) {
            $columnHeadlineKey = array_search('('.$column.')', $sortedOutputNames['original']);

            if (false === $columnHeadlineKey) {
                $columnHeadlineKey = array_search('(customers.'.$column.')', $sortedOutputNames['original']);
            }

            if (false === $columnHeadlineKey) {
                $columnHeadlineKey = array_search($column, $sortedOutputNames['humanized']);
            }

            $newSortedOutputNames['humanized'][$key] = $sortedOutputNames['humanized'][$columnHeadlineKey];
            $newSortedOutputNames['original'][$key] = $sortedOutputNames['original'][$columnHeadlineKey];
        }

        $return['output'][] = array_map('utf8_decode', $newSortedOutputNames['humanized']);
        $return['output'][] = $newSortedOutputNames['original'];

        foreach ($exportResult->getData() as $array) {
            try {
                // Test the profile values against the specified Assert's in $requiredColumns
                $this->testValuesAgainstRequiredColumns($exportResult, $array);

                $array[$titleKey] = $this->filterTitle(
                    $array[$titleKey]
                );

                $array[$exportResult->getValueKey('customers', 'id')] = $this->filterId(
                    $array[$exportResult->getValueKey('customers', 'id')]
                );

                
                $name1 = $array[$firstnameKey].' '.$array[$lastnameKey];
                $name2 = null;

                if (null === $encoding) {
                    $encoding = mb_detect_encoding($name1);
                }

                if ($array[$bankLastnameKey] === $name1) {
                    $array[$bankLastnameKey] = null;
                }

                if (mb_strlen($name1, $encoding) > 35) {
                    $name2 = mb_substr($name1, 35, null, $encoding);
                    $name1 = mb_substr($name1, 0, 35, $encoding);
                }

                $array[] = $name1; // Name1
                $array[] = $name2; // Name2

                unset($array[$firstnameKey]);

                // Zahlweg
                if (!empty($array[$ibanKey]) && 'no' != $array[$noEzeKey]) {
                    $array[] = 'C';
                } elseif (empty($array[$ibanKey]) && 'no' == $array[$noEzeKey]) {
                    $array[] = 'U';
                } elseif (empty($array[$ibanKey]) && 'no' != $array[$noEzeKey]) {
                    $array[] = 'Fehler';
                } elseif (!empty($array[$ibanKey]) && 'no' == $array[$noEzeKey]) {
                    $array[] = 'Fehler';
                } else {
                    $array[] = null;
                }

                unset($array[$noEzeKey]);

                // Strasse / HsNr.
                $array[] = $array[$streetKey].' '.$array[$streetnoKey];
                
                unset($array[$streetKey], $array[$streetnoKey]);

                $array[$lastnameKey] = mb_substr($array[$lastnameKey], 0, 10, $encoding);

                /*$ibanStatus = $ibanVerification->isValid($array[$ibanKey]);

                if ('ok' !== $ibanStatus) {
                    $ibanFailed++;
                }

                // IBAN Status
                $array[] = $ibanStatus;*/
            } catch (\InvalidArgumentException $e) {
                // testValuesAgainstRequiredColumns() throws this.

                $testValuesAgainstRequiredColumnsFailes++;
                $array[] = $e->getMessage();
            }

            $newSortedArray = array();

            foreach ($newSort as $key => $column) {
                try {
                    $arrayKey = $exportResult->getValueKey('customers', $column);
                } catch (\Exception $e) {
                    $arrayKey = array_search($column, $sortedOutputNames['humanized']);
                }

                $newSortedArray[$key] = $array[$arrayKey];
            }

            // Add values from profile to output
            $return['output'][] = array_map('utf8_decode', $newSortedArray);
        }

        // Adds the collected status information to output
        $return['output'][] = array_fill(0, count($array), '____________');

        $dummyData = array_fill(0, count($array), '');
        $dummyData[0] = utf8_decode('Unvollständige Datensätze');
        $dummyData[1] = $testValuesAgainstRequiredColumnsFailes;

        $return['output'][] = array_map('utf8_encode', $dummyData);

        /*$dummyData[0] = utf8_decode('Ungültige IBANs');
        $dummyData[1] = $ibanFailed;

        $return['output'][] = array_map('utf8_encode', $dummyData);*/

        return $return;
    }
}

?>