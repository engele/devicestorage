<?php
/**
 * 
 */

namespace csvExport\Lib;

require_once __DIR__.'/../Exception/ProfileNotUseable.php';
require_once __DIR__.'/../vendor/webmozart/assert/src/Assert.php';

/**
 * It's more or less only a useful function collection.
 */
abstract class Lib
{
    /**
     * @var \csvExport\Export
     */
    protected $export;

    /**
     * Constructor
     * 
     * @param \csvExport\Export $export
     */
    public function __construct(\csvExport\Export $export)
    {
        $this->export = $export;
    }

    /**
     * @param \csvExport\Export $export
     * 
     * @return \csvExport\Ping
     */
    public static function newInstance(\csvExport\Export $export)
    {
        return new static($export);
    }

    /**
     * @param \csvExport\ExportResult $exportResult
     * @param array                   $valuesArray
     * 
     * @return void
     */
    public function testValuesAgainstRequiredColumns(\csvExport\ExportResult $exportResult, array $valuesArray)
    {
        foreach ($this->requiredColumns as $tableName => $columns) {
            foreach ($columns as $columnOriginalName => $assertions) {
                foreach ($assertions as $assertion) {
                    $matches = array();
                    
                    preg_match_all('/([a-z][a-z_0-9]*)\(|([a-z][a-z_0-9]*)=([^,|\)]+)/i', $assertion, $matches, PREG_SET_ORDER);

                    if (!empty($matches)) {
                        $methodName = $matches[0][1];
                        
                        unset($matches[0]);

                        $arguments = array(
                            0 => $valuesArray[$exportResult->getValueKey($tableName, $columnOriginalName)]
                        );

                        foreach ($matches as $match) {
                            if (1 === preg_match('/"|\'/', $match[3])) {
                                $match[3] = preg_replace('/"|\'/', '', $match[3]);
                            } else {
                                $match[3] = (int) $match[3];
                            }

                            $arguments[$match[2]] = $match[3];
                        }

                        try {
                            call_user_func_array(array('\Webmozart\Assert\Assert', $methodName), $arguments);
                        } catch (\InvalidArgumentException $e) {
                            throw new \InvalidArgumentException($e->getMessage().' Column: '.$columnOriginalName);
                        }
                    } else {
                        // Maybe throw an error
                    }
                }
            }
        }
    }

    /**
     * Does this profile uses all required columns?
     * 
     * @param \csvExport\ExportResult $exportResult
     * 
     * @return boolean
     */
    public function usesProfileAllRequiredColumns(\csvExport\ExportResult $exportResult)
    {
        if (!isset($this->requiredColumns) || empty($this->requiredColumns)) {
            return true;
        }

        try {
            foreach ($this->requiredColumns as $tableName => $columns) {
                $valueColumns = $exportResult->getTableColumns($tableName);

                foreach ($columns as $columnOriginalName => $assertions) {
                    if (!isset($valueColumns[$columnOriginalName])) {
                        throw new \Exception($tableName.'.'.$columnOriginalName);
                    }
                }
            }

            return true;
        } catch (\Exception $e) {}

        return false;
    }

    /**
     * @return string
     */
    public function getMessageProfileNotUseable()
    {
        $requiredInfo = array();

        foreach ($this->requiredColumns as $tableName => $columns) {
            foreach ($columns as $columnOriginalName => $assertions) {
                $requiredInfo[] = $tableName.'.'.  $columnOriginalName;
            }
        }

        return implode(', ', $requiredInfo);
    }

    /**
     * @param integer $profileId
     * 
     * @throws \Exception
     * @throws \csvExport\ProfileNotUseable
     * 
     * @return \csvExport\ExportResult
     */
    public function getExportData($profileId)
    {
        $exportResult = $this->export->useProfile($profileId)->getResolvedProfile();

        if (!$this->export->isOutputAllowed()) {
            throw new Exception($this->export->whyIsOutputNotAllowed());
        }
        
        if (!$this->usesProfileAllRequiredColumns($exportResult)) {
            throw new \csvExport\ProfileNotUseable(
                $this->messageProfileNotUseingAllRequiredColumns.' '.$this->getMessageProfileNotUseable()
            );
        }

        return $exportResult;
    }
}
?>