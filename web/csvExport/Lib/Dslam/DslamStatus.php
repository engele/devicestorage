<?php
/**
 * 
 */

namespace csvExport\Lib\Dslam;

require_once __DIR__.'/../Lib.php';

require_once __DIR__.'/../../vendor/multicommand/multicommand/Lib/CommandCollection.php';
require_once __DIR__.'/../../vendor/multicommand/multicommand/Lib/CommandGroup.php';
require_once __DIR__.'/../../vendor/multicommand/multicommand/Lib/Command.php';
require_once __DIR__.'/../../vendor/multicommand/multicommand/Lib/CommandRunner.php';
require_once __DIR__.'/../../vendor/dslamcommands/dslamcommands/Lib/DslamCommandManager.php';
require_once __DIR__.'/../../vendor/dslamcommands/dslamcommands/DslamAccess.php';

use \csvExport\Lib\Lib;
use \MultiCommand\Lib\CommandCollection;
use \MultiCommand\Lib\CommandGroup;
use \MultiCommand\Lib\Command;
use \MultiCommand\Lib\CommandRunner;
use \MultiCommand\Lib\BeforeCommand;
use \DslamCommands\Lib\DslamCommandManager;
use \DslamCommands\DslamAccess;

/**
 * 
 */
class DslamStatus extends Lib
{
    /**
     * Profile must use these columns.
     * Uses Assert-Checks to ensure all columns provide correct values.
     *      
     *      Always end Assert with () !
     * 
     *      'database' => array(
     *          'column' => array(
     *              'notEmpty()',
     *              'eq(value="some string")',
     *              ...
     *          ),
     *          ...
     *      ),
     *      ...
     * 
     * @var array
     * 
     * @see https://github.com/webmozart/assert
     */
    protected $requiredColumns = array(
        'customers' => array(
            'connection_activation_date' => array(
                'notEmpty()',
            ),
            //'connection_inactive_date' => array('isEmpty()'), ???
            'no_ping' => array(
                'integerish',
                'notEq(value=1)',
            ),
            'wisocontract_canceled_date' => array(
                'isEmpty()',
            ),
            'payment_performance' => array(
                'notEq(value="abgeschalten")',
            ),
            'ip_address' => array(
                'notEmpty()',
            ),
            'version' => array(
                'notEq(value=1)',
                'notEq(value=2)',
                'notEq(value=3)',
                'notEq(value=4)',
            ),
            'kvz_addition' => array(
                'notEmpty()',
            ),
            'line_identifier' => array(
                'notEmpty()',
            ),
            'location_id' => array(
                'integerish',
                'notEmpty()',
            ),
        ),
    );

    /**
     * This value will be shown if the used profile isn't using all columns specified in $requiredColumns
     * 
     * @var string
     */
    protected $messageProfileNotUseingAllRequiredColumns = 'To be able to use DSLAM-Status your profile must use the following columns:';

    /**
     * Only used to easily keep track of what happened
     * 
     * @var array
     */
    protected $global = array(
        'commandSuccessfulExecuted' => array(
            'message' => 'Kommando erfolgreich ausgeführt = ', 'counter' => 0
        ),
        'notQueryable' => array(
            'message' => 'Nicht abfragbar = ', 'counter' => 0
        ),
        'countAll' => array(
            'message' => 'Datensätze gesamt = ', 'counter' => 0
        ),
        'connectionFailed' => array(
            'message' => 'Verbindung fehlgeschlagen = ', 'counter' => 0
        ),
        'otherFailures' => array(
            'message' => 'Sonstige Fehler = ', 'counter' => 0
        ),
        'commandExcecutionFailed' => array(
            'message' => 'Kommando-Ausführung fehlgeschlagen = ', 'counter' => 0
        ),
    );

    /**
     * Only used to easily save the output until we actually output it
     * 
     * @var array
     */
    protected $return = array('filename' => '','output' => array());

    /**
     * @var \DslamCommands\Lib\DslamCommandManager
     */
    protected $dslamCommandManager;

    /**
     * Adds filename and the column names to output
     * 
     * @param \csvExport\ExportResult $exportResult
     * 
     * @return void
     */
    public function addHeadlinesToOutput(\csvExport\ExportResult $exportResult)
    {
        $this->return['filename'] = preg_replace('/[^a-z0-9-_\.]/i', '-', $exportResult->getProfileName()).'_'.date("YmdHis");

        $sortedOutputNames = $exportResult->getSortedColumnNames();

        $sortedOutputNames['original'] = array_map(
            function($value) {
                return '('.utf8_decode($value).')';
            },
            $sortedOutputNames['original']
        );

        $sortedOutputNames['humanized'][] = 'Ping-Status';
        $sortedOutputNames['original'][] = 'Ping-Status';

        $this->return['output'][] = array_map('utf8_decode', $sortedOutputNames['humanized']);
        $this->return['output'][] = $sortedOutputNames['original'];
    }

    /**
     * Adds the collected status information to output
     * 
     * @param integer $arrayLength
     * 
     * @return void
     */
    public function addGlobalInfoToOutput($arrayLength)
    {
        $this->return['output'][] = array_fill(0, $arrayLength, '____________');

        $dummyData = array_fill(0, $arrayLength, '');

        foreach ($this->global as $info) {
            $dummyData[1] = utf8_decode($info['message']);
            $dummyData[2] = $info['counter'];

            $this->return['output'][] = $dummyData;
        }
    }

    /**
     * Acutally runs the collected commands.
     * If the runner failed with StopCommandRunnerException its message will be added to output (to the last element!)
     * 
     * @param \MultiCommand\Lib\CommandCollection $commandCollection
     * @param interger                            $arrayLength
     * 
     * @return void
     */
    public function runCollectedCommands(CommandCollection $commandCollection, $arrayLength)
    {
        if (CommandRunner::newInstance()->runGrouped($commandCollection)->hasErrorHappened()) {
            foreach ($this->return['output'] as $key => $output) {
                if (!is_array($output)) {
                    $this->return['output'][$key] = array_fill(0, $arrayLength, '');
                    end($this->return['output'][$key]);
                    $this->return['output'][$key][key($this->return['output'][$key])] = $output;
                }
            }
        }
    }

    /**
     * Get data from profile. Create CommandCollection and collect commands for later execution.
     * 
     * @param integer $profileId
     * 
     * @throws \Exception
     * 
     * @return array
     */
    public function process($profileId)
    {
        $this->dslamCommandManager = new DslamCommandManager;

        /**
         * Get data from profile
         */
        $exportResult = $this->getExportData($profileId);
        
        $this->addHeadlinesToOutput($exportResult);

        $commandCollection = new CommandCollection;

        foreach ($exportResult->getData() as $k => $array) {
            // cards.location_id ?
            $locationId = (int) $array[$exportResult->getValueKey('customers', 'location_id')];

            $kvzAddition = $array[$exportResult->getValueKey('customers', 'kvz_addition')];
            
            try {
                $access = DslamAccess::newInstance()->get($locationId, $array[$exportResult->getValueKey('customers', 'ip_address')]);
            } catch (\InvalidArgumentException $e) {
                $access = array('ipAddress' => 'failed', 'port' => $e->getMessage());
            }

            $groupName = $access['ipAddress'].':'.$access['port'];

            /**
             * Create a CommandGroup.
             * Add Befor and After functions.
             * 
             * This actually means:
             * 
             *      BeforeCommand: will be executed befor any Command in this group.
             *          In this case, we find the correct Commands for this DSLAM-Type & KVZ,
             *          initiate connection and login to DSLAM.
             * 
             *          BeforeCommand->getCommandOutput() will hold whatever data the BeforeCommand returned.
             *              In this case, an instance of \DslamCommands\Commands\xxx\xxx\Commands.
             * 
             *      AfterCommand: will be executed after the last executed Command in this group.
             *          In this case, we logout of DSLAM and close connection.
             * 
             *          AfterCommand->getCommandOutput() will hold whatever data the BeforeCommand returned.
             *              In this case, an instance of \DslamCommands\Commands\xxx\xxx\Commands.
             */
            if (!$commandCollection->hasGroup($groupName)) {
                $commandCollection->setGroup(CommandGroup::newInstance()
                    ->setIdentifier($groupName)
                    /**
                     * Create and add the Command used as BeforeCommand
                     */
                    ->setBeforeCommand(Command::newInstance()
                        ->setCommand(function () use($locationId, $kvzAddition, $access) {
                            return $this->connectAndLoginToDslam(
                                $this->getDslamType($locationId),
                                $this->getKvzType($kvzAddition),
                                $access
                            );
                        })
                    )
                    /**
                     * Create and add the Command used as AfterCommand
                     */
                    ->setAfterCommand(Command::newInstance()
                        ->setCommand(function (BeforeCommand $beforeCommand) {
                            if ($beforeCommand->getCommandOutput() instanceOf \DslamCommands\Lib\DslamCommunication) {
                                $beforeCommand->getCommandOutput()->logout();
                            }
                        })
                    )
                );
            }

            /**
             * Create and add a Command to CommandCollection.
             */
            $commandCollection->addCommand(Command::newInstance()
                ->setGroupIdentifier($groupName)
                ->setCommand(function (BeforeCommand $beforeCommand) use ($array, $exportResult) {
                    $this->executeDslamCommand($beforeCommand, $array, $exportResult);
                })
            );
        }

        $countArray = count($array) + 1;

        $this->runCollectedCommands($commandCollection, $countArray);

        $this->addGlobalInfoToOutput($countArray);

        return $this->return;
    }

    /**
     * @param integer $locationId
     * 
     * @return string
     */
    public function getDslamType($locationId)
    {
        $dslamType = 'alu';

        switch ($locationId) {
            case 26:
                $dslamType = 'keymile';
                break;

            case 55:
            case 57:
                $dslamType = 'iskratel';
                break;
        }

        return $dslamType;
    }

    /**
     * @param string $kvzAddition
     * 
     * @return string|null
     */
    public function getKvzType($kvzAddition)
    {
        switch ($kvzAddition) {
            case 'GPON':
                return 'gpon';

            case 'NELTB':
                return 'neltb';

            case 'ETHERNET':
                return 'ethernet';

            default:
                return null;
        }
    }

    /**
     * BeforeCommand
     * 
     * Pick the correct Commands for this DSLAM-Type & KVZ, initiate connection and login to DSLAM.
     * 
     *      Note: Returning the exception instead of throwing it here gives us the possibility 
     *            to show the occured error on the same place where we wanted to show results.
     *            If you are throwing the StopCommandRunnerException and want to catch it's output 
     *            you need to use setOutputVaiable($someVar) on the Command added in $commandCollection->addCommand().
     *            Then the $someVar will hold the string value of the StopCommandRunnerException.
     * 
     * @param string $dslamType
     * @param string $kvzType
     * @param array  $access
     * 
     * @return \DslamCommands\Commands\xxx\xxx\Commands|\MultiCommand\Exception\StopCommandRunnerException
     */
    public function connectAndLoginToDslam($dslamType, $kvzType, array $access)
    {        
        try {
            if ('failed' === $access['ipAddress']) {
                throw new \Exception('Unable to get DSLAM\'s IP-Address ('.$access['port'].')');
            }

            return $this->dslamCommandManager->getDslam($dslamType, $kvzType)
                ->connect($access['ipAddress'], $access['port'])
                //->connect('185.8.85.3', 50005) // Labor
                //->login($access['username'], 'wisotel@2007.') // password woher? username?
                ->login('isadmin', 'wisotel@2007.')
            ;
        } catch (\DslamCommands\Exception\ConnectionFailedException $e) {
            return new \MultiCommand\Exception\StopCommandRunnerException('Connection to dslam failed. ('.$e->getMessage().')');
        } catch (\DslamCommands\Exception\LoginFailedException $e) {
            return new \MultiCommand\Exception\StopCommandRunnerException('Login to dslam failed. ('.$e->getMessage().')');
        } catch (\Exception $e) {
            return new \MultiCommand\Exception\StopCommandRunnerException('connectAndLoginToDslam failed. ('.$e->getMessage().')');
        }
    }

    /**
     * Command
     * 
     * Test values of profile. Execute commands on DSLAM and save it's results to status-info & output.
     * 
     *      Note: Returning the exception instead of throwing it here gives us the possibility 
     *            to show the occured error on the same place where we wanted to show results.
     *            If you are throwing the StopCommandRunnerException and want to catch it's output 
     *            you need to use setOutputVaiable($someVar) on the Command added in $commandCollection->addCommand().
     *            Then the $someVar will hold the string value of the StopCommandRunnerException.
     * 
     * This method can return a value. The returned value will be bound to the variable provided in setOutputVaiable().
     * 
     * @param \MultiCommand\Lib\BeforeCommand $beforeCommand
     * @param array                           $array
     * @param \csvExport\ExportResult         $exportResult
     * 
     * @return void
     */
    public function executeDslamCommand(BeforeCommand $beforeCommand, array $array, \csvExport\ExportResult $exportResult)
    {
        try {
            /**
             * Test the profile values against the specified Assert's in $requiredColumns
             */
            $this->testValuesAgainstRequiredColumns($exportResult, $array);

            /**
             * Check if BeforCommand->getCommandOutput() is instance of \DslamCommands\Commands\CommandInterface.
             * The \DslamCommands\Commands\xxx\xxx\Commands must implement CommandInterface!
             * 
             *      Note: BeforeCommand->getCommandOutput() might also be an Exception (or anything else)
             *            because we potentially returned it in BeforeCommand.
             *            This could be avoided when throwing the Exception instead of returning it.
             *            But, if thrown, we wouldn't be able to catch it here..
             *            Therefore we return it and check for it here. So we will be able to view the response
             *            wherever we want within the output.
             */
            if (!($beforeCommand->getCommandOutput() instanceOf \DslamCommands\Commands\CommandInterface)) {
                if ($beforeCommand->getCommandOutput() instanceOf \Exception) {
                    throw $beforeCommand->getCommandOutput();
                }

                throw new \Exception('beforeCommand->getCommandOutput() invalid type.');
            }
            
            $lineIdentifier = $array[$exportResult->getValueKey('customers', 'line_identifier')];

            /**
             * Run actual command(s) on DSLAM and add it's returned value to output
             */
            $array['pingResult'] = $beforeCommand->getCommandOutput()->getCustomerStatus($lineIdentifier, 'detail', true, 'ptm');

            $this->global['commandSuccessfulExecuted']['counter']++;

        } catch (\InvalidArgumentException $e) {
            /**
             * testValuesAgainstRequiredColumns() throws this.
             */

            $this->global['notQueryable']['counter']++;
            $array['pingResult'] = 'not able to query';
        } catch (\MultiCommand\Exception\StopCommandRunnerException $e) {
            /**
             * Comming from BeforeCommand
             */

            $this->global['connectionFailed']['counter']++;
            $array['pingResult'] = $e->getMessage();
        } catch (\DslamCommands\Exception\CommandFailedException $e) {
            /**
             * Indicates erros while executing the actual command on the DSLAM
             */

            $this->global['commandExcecutionFailed']['counter']++;
            $array['pingResult'] = $e->getMessage();
        } catch (\Exception $e) {
            /**
             * Anything else
             */

            $this->global['otherFailures']['counter']++;
            $array['pingResult'] = $e->getMessage();
        }

        $this->global['countAll']['counter']++;

        /**
         * Add values from profile to output
         */
        $this->return['output'][] = array_map('utf8_decode', $array);
    }
}

?>