<?php
/**
 * 
 */

namespace csvExport\Lib\Ping;

require_once __DIR__.'/Ping.php';

/**
 * 
 */
class PingOnlyFailed extends Ping
{
    /**
     * @param integer $profileId
     * 
     * @throws \Exception
     * 
     * @return array
     */
    public function process($profileId)
    {
        $return = array(
            'filename' => '',
            'output' => array(),
        );

        $exportResult = $this->getExportData($profileId);
        

        $return['filename'] = preg_replace('/[^a-z0-9-_\.]/i', '-', $exportResult->getProfileName()).'_'.date("YmdHis");

        /**
         * Ouput column names
         */
        $sortedOutputNames = $exportResult->getSortedColumnNames();

        $sortedOutputNames['original'] = array_map(
            function($value) {
                return '('.utf8_decode($value).')';
            },
            $sortedOutputNames['original']
        );

        $sortedOutputNames['humanized'][] = 'Ping-Status';
        $sortedOutputNames['original'][] = 'Ping-Status';

        $return['output'][] = array_map('utf8_decode', $sortedOutputNames['humanized']);
        $return['output'][] = $sortedOutputNames['original'];


        foreach ($exportResult->getData() as $array) {
            $this->global['countAll']['counter']++;

            try {
                $this->testValuesAgainstRequiredColumns($exportResult, $array);

                $ipAddress = $array[$exportResult->getValueKey('customers', 'ip_address')];

                $array['pingResult'] = $this->ping($ipAddress);

                if ('success' === $array['pingResult']) {
                    continue; // Only output when ping failed
                }
            } catch (\InvalidArgumentException $e) {
                $this->global['notPingable']['counter']++;
                $array['pingResult'] = 'not pingable';
            }

            $return['output'][] = array_map('utf8_decode', $array);
        }


        $return['output'][] = array_fill(0, count($array), '____________');

        $dummyData = array_fill(0, count($array), '');

        foreach ($this->global as $info) {
            $dummyData[1] = utf8_decode($info['message']);
            $dummyData[2] = $info['counter'];

            $return['output'][] = $dummyData;
        }

        return $return;
    }
}

?>