<?php
/**
 * 
 */

namespace csvExport\Lib\Ping;

require_once __DIR__.'/../Lib.php';

use \csvExport\Lib\Lib;

/**
 * 
 */
class Ping extends Lib
{
    /**
     * Profile must use these columns.
     * Uses Assert-Checks to ensure all columns provide correct values.
     *      
     *      Always end Assert with () !
     * 
     *      'database' => array(
     *          'column' => array(
     *              'notEmpty()',
     *              'eq(value="some string")',
     *              ...
     *          ),
     *          ...
     *      ),
     *      ...
     * 
     * @var array
     * 
     * @see https://github.com/webmozart/assert
     */
    protected $requiredColumns = array(
        'customers' => array(
            'connection_activation_date' => array(
                'notEmpty()'
            ),
            //'connection_inactive_date' => array('isEmpty()'), ???
            'no_ping' => array(
                'integerish',
                'notEq(value=1)',
            ),
            'wisocontract_canceled_date' => array(
                'isEmpty()',
            ),
            'payment_performance' => array(
                'notEq(value="abgeschalten")',
            ),
            'ip_address' => array(
                'notEmpty()',
            ),
            'version' => array(
                'notEq(value=1)',
                'notEq(value=2)',
                'notEq(value=3)',
                'notEq(value=4)',
            ),
        ),
    );

    /**
     * This value will be shown if the used profile isn't using all columns specified in $requiredColumns
     * 
     * @var string
     */
    protected $messageProfileNotUseingAllRequiredColumns = 'To be able to use Ping-Export your profile must use the following columns:';

    /**
     * Only used to easily keep track of what happened
     * 
     * @var array
     */
    protected $global = array(
        'pingsSuccessful' => array(
            'message' => 'Pings erfolgreich = ', 'counter' => 0
        ),
        'pingsFailed' => array(
            'message' => 'Pings fehlgeschlagen = ', 'counter' => 0
        ),
        'notPingable' => array(
            'message' => 'Nicht pingbar = ', 'counter' => 0
        ),
        'countAll' => array(
            'message' => 'Datensätze gesamt = ', 'counter' => 0
        ),
    );

    /**
     * Actual ping
     * 
     * @param string $ipAddress
     * 
     * @return string
     */
    public function ping($ipAddress)
    {
        $pingOutput = null;
        $pingResult = null;

        exec("ping -c 1 -s 8 -t 64 ".$ipAddress, $pingOutput, $pingResult);

        if (0 == $pingResult) {
            $this->global['pingsSuccessful']['counter']++;

            return 'success';
        }

        $this->global['pingsFailed']['counter']++;

        return 'failed';
    }

    /**
     * @param integer $profileId
     * 
     * @throws \Exception
     * 
     * @return array
     */
    public function process($profileId)
    {
        $return = array(
            'filename' => '',
            'output' => array(),
        );

        /**
         * Get values from profile
         */
        $exportResult = $this->getExportData($profileId);
        
        /**
         * Set filename for output
         */
        $return['filename'] = preg_replace('/[^a-z0-9-_\.]/i', '-', $exportResult->getProfileName()).'_'.date("YmdHis");

        /**
         * Ouput column names
         */
        $sortedOutputNames = $exportResult->getSortedColumnNames();

        $sortedOutputNames['original'] = array_map(
            function($value) {
                return '('.utf8_decode($value).')';
            },
            $sortedOutputNames['original']
        );

        $sortedOutputNames['humanized'][] = 'Ping-Status';
        $sortedOutputNames['original'][] = 'Ping-Status';

        $return['output'][] = array_map('utf8_decode', $sortedOutputNames['humanized']);
        $return['output'][] = $sortedOutputNames['original'];


        foreach ($exportResult->getData() as $array) {
            try {
                /**
                 * Test the profile values against the specified Assert's in $requiredColumns
                 */
                $this->testValuesAgainstRequiredColumns($exportResult, $array);

                $ipAddress = $array[$exportResult->getValueKey('customers', 'ip_address')];

                /**
                 * Run actual ping add it's result to output
                 */
                $array['pingResult'] = $this->ping($ipAddress);
            } catch (\InvalidArgumentException $e) {
                /**
                 * testValuesAgainstRequiredColumns() throws this.
                 */

                $this->global['notPingable']['counter']++;
                $array['pingResult'] = 'not pingable';
            }

            $this->global['countAll']['counter']++;

            /**
             * Add values from profile to output
             */
            $return['output'][] = array_map('utf8_decode', $array);
        }

        /**
         * Adds the collected status information to output
         */
        $return['output'][] = array_fill(0, count($array), '____________');

        $dummyData = array_fill(0, count($array), '');

        foreach ($this->global as $info) {
            $dummyData[1] = utf8_decode($info['message']);
            $dummyData[2] = $info['counter'];

            $return['output'][] = $dummyData;
        }

        return $return;
    }
}

?>