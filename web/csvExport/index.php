<?php

$this->denyAccessUnlessGranted('ROLE_USE_CSV_EXPORT');

/**
 * How-To:
 *  - Extending this feature -
 * (use more, or different database.tables)
 * 
 * 1. Duplicate one of the files inside of ./DatabaseTables/ directory (except for Table.php)
 * 2. Rename it and adapt it's contents to your needs
 * 3. Add the name (and namespace) of your new file to the 
 *    return array within the "usedTables"-method inside of ./Export/ExportTables.php
 */

require_once ($StartPath.'/_conf/database.inc');
require_once ($StartPath.'/'.$SelMenu.'/_conf/database.inc');

/**
 * @return void
 */
function isProfileIdSet()
{
    if (!isset($_GET['profile-id'])) {
        header('Location: ?menu=csvExport');
        exit;
    }

    $_GET['profile-id'] = (int) $_GET['profile-id'];
}


if (isset($_GET['func'])) {
    switch ($_GET['func']) {
        /**
         * Export
         */
        case 'export-csv':
        case 'export-ping-csv':
        case 'export-ping-only-failed-csv':
        case 'export-dslam-status-csv':
        case 'export-dslam-config-batch':
        case 'export-pppee-config-batch':
        case 'export-iban-verification':
        case 'export-microtik-ppp-secrets':
        case 'export-sap-debitors-creation':
            isProfileIdSet();

            require_once $StartPath.'/'.$SelMenu.'/ExportOutput/Controller.php';
            
            new \csvExport\Output\Controller($_GET['func'], $db, $sql);

            break;

        /**
         * Edit profile
         */
        case 'edit':
            isProfileIdSet();

            require_once $StartPath.'/'.$SelMenu.'/profile.php';
            break;

        /**
         * Delete profile
         */
        case 'delete':
            isProfileIdSet();

            $statement = $db->prepare($sql['delete_profile']);
            $statement->bind_param('i', $_GET['profile-id']);

            if ($statement->execute()) {
                header('Location: ?menu=csvExport&success-info=delete');
                exit;
            }

            throw new Exception('Profil konnte nicht gelöscht werden.');

            break;

        /**
         * Create new profile
         */
        case 'create-new':
            require_once $StartPath.'/'.$SelMenu.'/profile.php';
            break;

        /**
         * Clone profile
         */
        case 'clone':
            isProfileIdSet();

            $statement = $db->prepare($sql['select_whole_profile_by_id']);
            $statement->bind_param('i', $_GET['profile-id']);
            $statement->execute();

            $profileResult = $statement->get_result();

            $profile = $profileResult->fetch_assoc();

            $profile['profile_name'] = null;

            if (isset($_GET['profile-name'])) {
                /**
                 * Duplicated entry (profile.php). Remember when changing this.
                 */
                $profile['profile_name'] = preg_replace('/[^a-z0-9-_\s\.]/i', '', urldecode($_GET['profile-name']));
            }

            $statement = $db->prepare($sql['save_profile']);
            $statement->bind_param('ssssi',
                $profile['profile_name'],
                $profile['profile'],
                $profile['profile_sort'],
                $profile['profile_where'],
                $profile['user_id']
            );
            
            if ($statement->execute()) {
                header('Location: ?menu=csvExport&success-info=clone&preselect='.$db->insert_id);
                exit;
            }

            throw new Exception('Profil konnte nicht geclont werden.');

            break;

        /**
         * No reason to be here
         */
        default:
            header('Location: ?menu=csvExport');
            exit;
    }
} else {
    /**
     * default view
     */

    $profiles = array();
    
    /**
     * userId
     * For now, it is always null
     */
    $userId = null;

    $statement = $db->prepare($sql['select_profiles']);
    $statement->bind_param('i', $userId);
    $statement->execute();

    $profilesResult = $statement->get_result();

    while ($profile = $profilesResult->fetch_assoc()) {
        $profiles[] = $profile;
    }

    $profilesResult->free();
    $statement->close();
    ?>

    <h1>CSV-Export</h1>

    <?php
    if (isset($_GET['success-info'])) {
        switch ($_GET['success-info']) {
            case 'create-new':
                echo '<h2 class="color-green">Neues Profil erfolgreich angelegt.</h2>';
                break;
            case 'delete':
                echo '<h2 class="color-green">Profil gelöscht.</h2>';
                break;
            case 'edit':
                echo '<h2 class="color-green">Profil erfolgreich aktualisiert.</h2>';
                break;
            case 'clone':
                echo '<h2 class="color-green">Profil erfolgreich geclont.</h2>';
                break;
        }
    }

    $templatingAssets = $this->get('templating.helper.assets');
    ?>

    <div class="CsvExport box">
        <h3></h3>

        <div>
            <select id="export-profile">
                <option value="">Profil auswählen</option>
                <?php
                foreach ($profiles as $profile) {
                    $selected = '';

                    if (isset($_GET['preselect']) && (int) $_GET['preselect'] === $profile['id']) {
                        $selected = ' selected="selected"';
                    }

                    echo '<option value="'.$profile['id'].'"'.$selected.'>'.$profile['profile_name'].'</option>';
                }
                unset($selected);
                ?>
            </select>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="submit" id="button-export-csv">
                <img alt="" src="<?php echo $templatingAssets->getUrl('_img/Save.png'); ?>" height="12px"> Export (csv)
            </button>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="submit" id="button-edit">
                <img alt="" src="<?php echo $templatingAssets->getUrl('_img/Modify.png'); ?>" height="12px"> Bearbeiten
            </button>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="submit" id="button-delete">
                <img alt="" src="<?php echo $templatingAssets->getUrl('_img/delete.png'); ?>" height="12px"> Löschen
            </button>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="submit" id="button-clone">
                <img alt="" src="<?php echo $templatingAssets->getUrl('_img/Copy.png'); ?>" height="12px"> Profil clonen
            </button>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;
            <button type="button" id="button-new">
                <img alt="" src="<?php echo $templatingAssets->getUrl('_img/New document.png'); ?>" height="12px"> Neues Profil
            </button>
            <br />
            <br />
            <button onclick="backButton()" type="button">
                <img alt="" src="<?php echo $templatingAssets->getUrl('_img/Back.png'); ?>" height="12px"> Zurück
            </button>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="submit" id="button-export-ping-csv">
                <img alt="" src="<?php echo $templatingAssets->getUrl('_img/Save.png'); ?>" height="12px"> Export Ping (csv)
            </button>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <button type="submit" id="button-export-ping-only-failed-csv">
                <img alt="" src="<?php echo $templatingAssets->getUrl('_img/Save.png'); ?>" height="12px"> Export Ping-Only failed (csv)
            </button>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <button type="submit" id="button-export-dslam-status-csv">
                <img alt="" src="<?php echo $templatingAssets->getUrl('_img/Save.png'); ?>" height="12px"> Export DSLAM Status (csv)
            </button>
            <br />
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="submit" id="button-export-dslam-config-batch">
                <img alt="" src="<?php echo $templatingAssets->getUrl('_img/Save.png'); ?>" height="12px"> Export DSLAM Config (bat)
            </button>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <button type="submit" id="button-export-pppee-config-batch">
                <img alt="" src="<?php echo $templatingAssets->getUrl('_img/Save.png'); ?>" height="12px"> Export PPPeE Config (bat)
            </button>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <button type="submit" id="button-export-iban-verification">
                <img alt="" src="<?php echo $templatingAssets->getUrl('_img/Save.png'); ?>" height="12px"> Export IBAN-Prüfung
            </button>
            <br />
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="submit" id="button-export-microtik-ppp-secrets">
                <img alt="" src="<?php echo $templatingAssets->getUrl('_img/Save.png'); ?>" height="12px"> Export Microtik PPP-Secrets
            </button>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <button type="submit" id="button-export-sap-debitors-creation">
                <img alt="" src="<?php echo $templatingAssets->getUrl('_img/Save.png'); ?>" height="12px"> SAP Debitoren-Stamm-Anlagen-Export
            </button>
        </div>

    </div>

    <?php
}

$db->close();
?>
