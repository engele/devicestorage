<?php
/**
 * 
 */

namespace csvExport;

/**
 * 
 */
class WherePlaceholder
{
    /**
     * Centralized place to store placeholders
     * 
     * @var array
     */
    protected $usedPlaceholders = array();

    /**
     * @var array
     */
    protected $placeholderValues = array();

    /**
     * @param array $placeholderValues
     * 
     * @return \csvExport\WherePlaceholder
     */
    public function setPlaceholderValues(array $placeholderValues)
    {
        $this->placeholderValues = $placeholderValues;

        return $this;
    }

    /**
     * Return used placeholders in $value.
     * 
     * Empty array if none are used.
     * Array cointaining all used placeholders otherwise.
     * 
     * @param string $value
     * 
     * @return array
     */
    public function getUsedPlaceholders($value)
    {
        $matches = array();

        preg_match_all("/\[([^\]]+)\]/", $value, $matches, PREG_SET_ORDER);

        return $matches;
    }

    /**
     * Replacements available?
     * 
     * @param string $table
     * @param string $column
     * 
     * @return boolean
     */
    public function hasAvailableReplacements($table, $column)
    {
        return isset($this->placeholderValues[$table][$column]);
    }

    /**
     * @return void
     */
    public function applyReplacements($table, $column, $placeholdes, $value)
    {
        foreach ($placeholdes as $order => $matches) {
            $value = preg_replace(
                "/\[[^\]]+\]/",
                $this->placeholderValues[$table][$column][$order],
                $value,
                1 // this is important
            );
        }

        return $value;
    }

    /**
     * Store placeholders in $usedPlaceholders
     * 
     * @param string $table
     * @param string $column
     * @param array $placeholders
     * 
     * @return void
     */
    public function storePlaceholders($table, $column, $placeholders)
    {
        if (!isset($this->usedPlaceholders[$table])) {
            $this->usedPlaceholders[$table] = array();
        }

        $this->usedPlaceholders[$table][$column] = $placeholders;
    }

    /**
     * Are any placeholders stored?
     * 
     * @return boolean
     */
    public function hasStoredPlaceholders()
    {
        return !empty($this->usedPlaceholders);
    }

    /**
     * Get stored placeholders
     * 
     * @return array
     */
    public function getStoredPlaceholders()
    {
        return $this->usedPlaceholders;
    }
}

?>