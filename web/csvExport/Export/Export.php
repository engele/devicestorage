<?php
/**
 *
 */

namespace csvExport;

require_once __DIR__.'/../Exception/PlaceholderNotProvided.php';

/**
 * Get profile. Build SQL-query. Handle used Placeholdes. Execute SQL-query.
 */
class Export
{
	/**
	 * @var ExportTables
	 */
	protected $exportTables;

	/**
	 * @var WherePlaceholder
	 */
	protected $wherePlaceholder;

    /**
     * @var ExportResult
     */
    protected $exportResult;

	/**
	 * @var mysqli
	 */
	protected $db;

	/**
	 * @var array
	 */
	protected $sql;

	/**
	 * @var boolean
	 */
	protected $allowOutput = true;

    /**
     * Reason why outputting is not allowed
     * 
     * @var string
     */
    protected $dontAllowOutputReason;

    /**
     * @var integer
     */
    protected $profileId;

    /**
     * @var \csvExport\Relations
     */
    protected $relations;

    /**
     * @var boolean
     */
    protected $moreThanOneTableUsed;

	/**
     * Constructor
     *
     * @param ExportTables     $exportTables
     * @param WherePlaceholder $wherePlaceholder
     * @param Relations        $relations
     * @param \mysqli          $db
     * @param array            $sql
     */
    public function __construct(ExportTables $exportTables, Relations $relations, WherePlaceholder $wherePlaceholder, ExportResult $exportResult, \mysqli $db, $sql)
    {
        $this->exportTables = $exportTables;
        $this->relations = $relations;
        $this->wherePlaceholder = $wherePlaceholder;
        $this->exportResult = $exportResult;
        $this->db = $db;
        $this->sql = $sql;
    }

    /**
     * @param integer $profileId
     * 
     * @throws \Exception
     * 
     * @return \csvExport\Export
     */
    public function useProfile($profileId)
    {
        if (!is_int($profileId)) {
            throw new \Exception('Profile-Id is not an integer.');
        }

        if (is_null($this->profileId)) {
            $this->profileId = $profileId;

            return $this;
        }

        return new self($this->exportTables, $this->wherePlaceholder, $this->db, $this->sql);
    }

    /**
     * @return boolean
     */
    public function isOutputAllowed()
    {
        return $this->allowOutput;
    }

    /**
     * @return string
     */
    public function whyIsOutputNotAllowed()
    {
        return $this->dontAllowOutputReason;
    }

    /**
     * @throws \Exception
     * 
     * @return array
     */
    public function getProfile()
    {
        if (!is_int($this->profileId)) {
            throw new \Exception('ProfileId not set.');
        }

    	$return = array();

    	$statement = $this->db->prepare($this->sql['select_profile_by_id']);
        $statement->bind_param('i', $this->profileId);
        $statement->execute();

        $profilesResult = $statement->get_result();

        $profile = $profilesResult->fetch_assoc();

        $return['profileName'] = $profile['profile_name'];
        $return['where'] = unserialize($profile['profile_where']);
        $return['sort'] = unserialize($profile['profile_sort']);
        $return['profile'] = unserialize($profile['profile']);

        $profilesResult->free();
        $statement->close();

    	return $return;
    }

    /**
     * @return array
     */
    public function getResolvedProfile()
    {
        return $this->resolveProfile(
            $this->getProfile()
        );
    }

    /**
     * @param array $profile
     * 
     * @return array
     */
    public function resolveProfile(array $profile)
    {
        $return = array();

        $queryResult = $this->buildSqlQueryFromProfile($profile);

        $sortedColumnNames = array('original' => array(), 'humanized' => array());
        $originalNamesToQueryResults = array();
        $moreThanOneTableUsed = count($queryResult['queryResultsToOriginalNames']) > 1 ? true : false;

        foreach ($queryResult['queryResultsToOriginalNames'] as $tableName => $array) {
            foreach ($array['columns'] as $columnOriginalName => $positionInResult) {
                $originalNamesToQueryResults[$positionInResult] = array(
                    'table' => $tableName, 'column' => $columnOriginalName
                );
            }
        }

        $result = $this->executeQuery(
            $queryResult['query']
        );

        while ($data = $result->fetch_assoc()) {
            if (empty($sortedColumnNames['original'])) {
                foreach ($data as $key => $value) {
                    $tableName = $originalNamesToQueryResults[$key]['table'];
                    $columnOriginalName = $originalNamesToQueryResults[$key]['column'];
                    $currentTable = $queryResult['queryResultsToOriginalNames'][$tableName]['table'];
                    $humanizedColumnName = $currentTable->getColumnHumanizedNameByOriginalName($columnOriginalName);

                    if ($moreThanOneTableUsed) {
                        $sortedColumnNames['original'][] = $tableName.'.'.$columnOriginalName;
                        $sortedColumnNames['humanized'][] = $currentTable->getHumanizedName().'.'.$humanizedColumnName;
                    } else {
                        $sortedColumnNames['original'][] = $columnOriginalName;
                        $sortedColumnNames['humanized'][] = $humanizedColumnName;
                    }
                }
            }

            $return[] = $data;
        }

        $result->close();

        $this->exportResult->setProfileName($profile['profileName']);
        $this->exportResult->setData($return);
        $this->exportResult->setQueryResultsToOriginalNames($queryResult['queryResultsToOriginalNames']);
        $this->exportResult->setSortedColumnNames($sortedColumnNames);

        return $this->exportResult;
    }

    /**
     * Execute query
     * 
     * @param string $query
     * 
     * @throws \Exception
     * 
     * @return mixed - mysqli_result or true
     */
    public function executeQuery($query)
    {
        $result = $this->db->query($query);

        if (false == $result) {
            throw new \Exception('MySQL-Query failed. - '.$this->db->error.' -- '.$query.' --');
        }

        return $result;
    }

    /**
     * This is used as xxx in SELECT xxx, .. WHERE
     * 
     * @param string  $tableName
     * @param string  $columnOrginalName
     * @param string  $useColumnAs
     * @param integer $i
     * 
     * @return string
     * 
     * @todo Find a better name..
     */
    public function buildQueryString($tableName, $columnOrginalName, $useColumnAs, $i)
    {
    	if ($useColumnAs !== $columnOrginalName) {
            return sprintf("%s AS '%d'",
                str_ireplace('{column}', '`'.$tableName.'`.`'.$columnOrginalName.'`', $useColumnAs),
                $i
            );
        } else {
            return sprintf("`%s`.`%s` AS '%d'", $tableName, $columnOrginalName, $i);
        }
    }

    /**
     * This is used as xxx in WHERE xxx
     * 
     * Select appropriate comparison operator.
     * Adjustments to value.
     * columnOriginalName or useInsteadOfName.
     * 
     * @param string $tableName
     * @param string $columnOrginalName
     * @param string $value
     * @param string $useColumnAs
     * 
     * @return string
     * 
     * @todo Find a better name..
     */
    public function buildWhereString($tableName, $columnOrginalName, $value, $useColumnAs)
    {
        $dontWrapValueInSingleQuotes = false;

        $comparisonOperator = $this->exportTables->getComparisonOperator($value, $dontWrapValueInSingleQuotes);
        $value = $this->exportTables->valueReplacements($value, $dontWrapValueInSingleQuotes);

        if ($useColumnAs !== $columnOrginalName) {
            return sprintf("%s %s %s",
                str_ireplace('{column}', '`'.$tableName.'`.`'.$columnOrginalName.'`', $useColumnAs),
                $comparisonOperator,
                $value
            );
        } else {
            // e.g. - `table`.`column` = 'value'
            return sprintf("`%s`.`%s` %s %s", $tableName, $columnOrginalName, $comparisonOperator, $value);
        }
    }

    /**
     * Name, which will be printed for this column later
     * 
     * More than one table used for this export?
     * Prepend table name to $humanizedColumnName.
     * 
     * @param \DatabaseTables\Table $currentTable
     * @param string                $columnOriginalName
     * 
     * @return string
     */
    /*public function getHumanizedColumnName(\DatabaseTables\Table $currentTable, $columnOriginalName)
    {
    	$humanizedColumnName = $currentTable->getColumnHumanizedNameByOriginalName($columnOriginalName);

    	if ($this->moreThanOneTableUsed) {
            $humanizedColumnName = $currentTable->getHumanizedName().'.'.$humanizedColumnName;
        }

        return $humanizedColumnName;
    }*/

    /**
     * Has additional "where" criteria?
     * 
     * Get placeholders used in $value.
     * Any placeholders used in $value? - Apply replacements or store them for later
     * 
     * @param string $value
     * @param string $tableName
     * @param string $columnOriginalName
     * 
     * @return void
     */
    public function resolvePlaceholdersInWhereCriteria(&$value, $tableName, $columnOriginalName)
    {
        $placeholdersUsedInValue = $this->wherePlaceholder->getUsedPlaceholders($value);

        if (!empty($placeholdersUsedInValue)) {
            if ($this->wherePlaceholder->hasAvailableReplacements($tableName, $columnOriginalName)) {
                $value = $this->wherePlaceholder->applyReplacements($tableName, $columnOriginalName, $placeholdersUsedInValue, $value);
            } else {
                $this->wherePlaceholder->storePlaceholders($tableName, $columnOriginalName, $placeholdersUsedInValue);
            }
        }
    }

    /**
     * @param string                $queryString
     * @param array                 $profile
     * @param string                $tableName
     * @param string                $columnOriginalName
     * @param \DatabaseTables\Table $currentTable
     * @param array                 $select
     * 
     * @todo Find a better name..
     */
    public function sortQueryString($queryString, array $profile, $tableName, $columnOriginalName, 
    	\DatabaseTables\Table $currentTable, array &$select)
    {
    	/**
         * Has sort number?
         */
    	if (isset($profile['sort'][$tableName]) && isset($profile['sort'][$tableName][$columnOriginalName])) {
            $number = $profile['sort'][$tableName][$columnOriginalName];

            $select['sorted'][$number][] = $queryString;
        } else {
            $select['unsorted'][] = $queryString;
        }
    }

    /**
     * @param array $profile
     * 
     * @throws \Exception
     * 
     * @return string
     */
    public function buildSqlQueryFromProfile(array $profile)
    {
        $i = 0;
        $this->moreThanOneTableUsed = count($profile['profile']) > 1 ? true : false;

        $select = array('sorted' => array(), 'unsorted' => array());
        $wheresArray = array();

        $from = '';
        $queryResultsToOriginalNames = array();

        foreach ($profile['profile'] as $tableName => $columns) {
            $currentTable = $this->exportTables->findTableByOriginalName($tableName);
            $from .= '`'.$tableName.'`, ';

            foreach ($columns as $column) {
                $queryString = $this->buildQueryString(
                	$tableName,
                	$column,
                	$currentTable->useColumnAs($column, 'select'),
                	$i
                );

                if (!isset($queryResultsToOriginalNames[$tableName])) {
                    $queryResultsToOriginalNames[$tableName] = array(
                        'table' => $currentTable,
                        'columns' => array(),
                    );
                }
                $queryResultsToOriginalNames[$tableName]['columns'][$column] = $i;

                $this->sortQueryString(
                	$queryString,
                	$profile,
                	$tableName,
                	$column,
                    $currentTable,
                	$select
                );

                $i++;
            }

            /**
             * Table has relations?
             */
            $this->relations->analyzeRelation($currentTable, $tableName);

            /**
             * Has additional "where" criteria?
             */
            if (isset($profile['where']['data'][$tableName])) {
            	foreach ($profile['where']['data'][$tableName] as $column => $value) {
		            $this->resolvePlaceholdersInWhereCriteria($value, $tableName, $column);

                    if (false !== stripos($value, 'leerOderNull')) {
                        // Dirty hack to be able to check a field for '' or NULL in one query

                        $tmp = array();

                        foreach (['leer', 'null'] as $alternateValue) {
                            $alternateValue = str_replace('leerOderNull', $alternateValue, $value);

                            $tmp[] = $this->buildWhereString(
                                $tableName,
                                $column,
                                $alternateValue,
                                $currentTable->useColumnAs($column, 'where')
                            );
                        }

                        if (false !== strpos($value, 'nicht leerOderNull')) {
                            $wheresArray = array_merge($wheresArray, $tmp);
                        } else {
                            $wheresArray[] = '('.implode(' OR ', $tmp).')';
                        }
                    } else {
                        $wheresArray[] = $this->buildWhereString(
                            $tableName,
                            $column,
                            $value,
                            $currentTable->useColumnAs($column, 'where')
                        );
                    }
            	}
            }
        }

        if ($this->wherePlaceholder->hasStoredPlaceholders()) {
            $this->allowOutput = false;
            $this->dontAllowOutputReason = 'Placeholder values not provided.';

            throw new PlaceholderNotProvided('Provide placeholder replacements befor calling this method.');
        }

        return array(
            'query' => $this->getQuery($select, $from, $wheresArray, $profile['where']['criteria']),
            'queryResultsToOriginalNames' => $queryResultsToOriginalNames,
        );
    }

    /**
     * @param array  $select
     * @param string $select
     * @param array  $wheresArray
     * @param string $whereCriteria
     */
    public function getQuery($select, $from, array $wheresArray, $whereCriteria)
    {
    	$query = $this->queryGetSelectAndFrom($select, $from);

        /**
         * Found relations?
         * Append them to the query-string
         */
        $query .= $this->queryGetWhere();

        /**
         * Found additional "where" criteria?
         * Append them to the query-string
         */
        $query .= $this->queryGetAdditionalWhere($wheresArray, $whereCriteria);

        return $query;
    }

    /**
     * @param array  $select
     * @param string $from
     * 
     * @return string
     */
    public function queryGetSelectAndFrom(array $select, $from)
    {
    	$selectQuery = '';

        ksort($select['sorted']);

        foreach ($select['sorted'] as $array) {   
            foreach ($array as $string) {
                $selectQuery .= $string.', ';
            }
        }

        $selectQuery .= implode(', ', $select['unsorted']);

        if (', ' === substr($selectQuery, -2)) {
            $selectQuery = substr($selectQuery, 0, -2);
        }

        $from = substr($from, 0, -2);

        return sprintf("SELECT %s FROM %s", $selectQuery, $from);
    }

    /**
     * Found relations?
     * Append them to the query-string
     * 
     * @return string
     */
    public function queryGetWhere()
    {
        $relations = $this->relations->get();

    	if (!empty($relations)) {
            return ' WHERE '.implode(' AND ', $relations);
        }

        return '';
    }

    /**
     * @param array  $wheresArray
     * @param string $whereCriteria
     */
    public function queryGetAdditionalWhere(array $wheresArray, $whereCriteria)
    {
    	if (!empty($wheresArray)) {
            $delimiter = 'or' == $whereCriteria ? ' OR ' : ' AND ';

            if ($this->relations->has()) {
                return ' AND ('.implode($delimiter, $wheresArray).')';
            } else {
                return ' WHERE '.implode($wheresArray, $delimiter);
            }
        }
    }

    /**
     * Ask for placeholder replacements
     */
    public function askForPlaceholderReplacements()
    {
    	echo '<form method="post" action="" id="ask-for-replacements-form" />';

        foreach ($this->wherePlaceholder->getStoredPlaceholders() as $tableName => $columns) {
            $currentTable = $this->exportTables->findTableByOriginalName($tableName);

            echo '<div class="accordion">'
                .'<h3>'.$currentTable->getHumanizedName().'</h3>'
                .'<div><br />'
                .'<table>'
            ;

            foreach ($columns as $column => $placeholdersArray) {
                $humanizedColumnName = $currentTable->getColumnHumanizedNameByOriginalName($column);

                echo '<tr><td colspan="2" class="highlight-profile-entry">'.$humanizedColumnName.'</td></tr>';

                foreach ($placeholdersArray as $order => $placeholder) {
                    echo '<tr>'
                        .'<td class="question-td">'.$placeholder[1].'</td>'
                        .'<td class="replacement-value-td"><input type="text" name="placeholder['.$tableName.']['.$column.']['.$order.']" value="" /></td>'
                        .'</tr>'
                    ;
                }
            }

            echo '</table></div>';
        }

        echo '</div><br /><br />'
            .'<div class="box"><h3></h3>'
            .'<div class="text-center"><button type="submit">'
            .'<img alt="" src="/_img/Yes.png" height="12px"> Export</button>'
            .'</div></div></form>'
        ;
    }
}

?>