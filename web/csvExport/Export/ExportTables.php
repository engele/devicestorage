<?php
/**
 * 
 */

namespace csvExport;

require_once __DIR__.'/DatabaseTables.php';

/**
 * 
 */
class ExportTables extends DatabaseTables
{
    /**
     * This columns will be listed separately.
     * It helpes to keep things a little clearer arranged.
     * 
     *  How to:
     *      'Some name used as headline' => array(
     *          '\Namespace\of\Class:property',
     *          ...
     *      ),
     *      'another group' => array(
     *          ...
     *      ),
     *      ...
     * 
     *      e.g.
     *          'Ueberschrift 1' => array(
     *              '\DatabaseTables\Customers:add_charge_terminal',
     *          )
     * 
     * @var array
     */
    protected $groupColumns = array(
        'Ablauf' => array(
            '\DatabaseTables\Customers:contract_sent_date',
            '\DatabaseTables\Customers:tal_order_date',
            '\DatabaseTables\Customers:tal_ordered_for_date',
            '\DatabaseTables\Customers:contract_version',
            '\DatabaseTables\Customers:tal_order_ack_date',
            '\DatabaseTables\Customers:tal_order_work',
            '\DatabaseTables\Customers:contract_received_date',
            '\DatabaseTables\Customers:customer_connect_info_date',
            '\DatabaseTables\Customers:productname',
            '\DatabaseTables\Customers:tal_letter_to_operator',
            '\DatabaseTables\Customers:tal_letter_to_operator_from',
            '\DatabaseTables\Customers:purtel_product',
            '\DatabaseTables\Customers:purtelproduct_advanced',
            '\DatabaseTables\Customers:fb_vorab',
            '\DatabaseTables\Customers:final_purtelproduct_date',
            '\DatabaseTables\Customers:terminal_ready',
            '\DatabaseTables\Customers:terminal_sended_date',
            '\DatabaseTables\Customers:oldcontract_exists',
            '\DatabaseTables\Customers:patch_date',
            '\DatabaseTables\Customers:pppoe_config_date',
            '\DatabaseTables\Customers:add_phone_nos',
            '\DatabaseTables\Customers:dslam_arranged_date',
            '\DatabaseTables\Customers:purtel_customer_reg_date',
            '\DatabaseTables\Customers:sofortdsl',
            '\DatabaseTables\Customers:soforttel',
            '\DatabaseTables\Customers:ventelo_port_letter_date',
            '\DatabaseTables\Customers:ventelo_confirmation_date',
            '\DatabaseTables\Customers:connection_activation_date',
            '\DatabaseTables\Customers:ventelo_purtel_enter_date',
            '\DatabaseTables\Customers:reached_downstream',
            '\DatabaseTables\Customers:reached_upstream',
            '\DatabaseTables\Customers:stati_port_confirm_date',
            '\DatabaseTables\Customers:purtel_edit_done',
            '\DatabaseTables\Customers:purtel_edit_done_from',
            '\DatabaseTables\Customers:dtagbluckage_sended_date',
            '\DatabaseTables\Customers:dtagbluckage_fullfiled_date',
            '\DatabaseTables\Customers:telefonbuch_eintrag',
        ),
        'Kundendaten' => array(
            '\DatabaseTables\Customers:clientid',
            '\DatabaseTables\Customers:id',
            '\DatabaseTables\Customers:customer_id',
            '\DatabaseTables\Customers:voucher',
            '\DatabaseTables\Customers:phoneareacode',
            '\DatabaseTables\Customers:phonenumber',
            '\DatabaseTables\Customers:fax',
            '\DatabaseTables\Customers:firstname',
            '\DatabaseTables\Customers:lastname',
            '\DatabaseTables\Customers:mobilephone',
            '\DatabaseTables\Customers:company',
            '\DatabaseTables\Customers:emailaddress',
            '\DatabaseTables\Customers:street',
            '\DatabaseTables\Customers:streetno',
            '\DatabaseTables\Customers:birthday',
            '\DatabaseTables\Customers:zipcode',
            '\DatabaseTables\Customers:city',
        ),
        'Anschluss' => array(
            '\DatabaseTables\Customers:connection_street',
            '\DatabaseTables\Customers:connection_streetno',
            '\DatabaseTables\Customers:connection_zipcode',
            '\DatabaseTables\Customers:connection_city',
        ),
        'Kontodaten' => array(
            '\DatabaseTables\Customers:bank_account_holder_lastname',
            '\DatabaseTables\Customers:bank_account_bankname',
            '\DatabaseTables\Customers:bank_account_bic_new',
            '\DatabaseTables\Customers:bank_account_iban',
            '\DatabaseTables\Customers:bank_account_number',
            '\DatabaseTables\Customers:bank_account_bic',
            '\DatabaseTables\Customers:bank_account_emailaddress',
            '\DatabaseTables\Customers:bank_account_street',
            '\DatabaseTables\Customers:bank_account_streetno',
            '\DatabaseTables\Customers:bank_account_zipcode',
            '\DatabaseTables\Customers:bank_account_city',
            '\DatabaseTables\Customers:bank_account_debitor',
            '\DatabaseTables\Customers:bank_account_mandantenid',
        ),
        'Vertragsdaten' => array(
            '\DatabaseTables\Customers:credit_rating_date',
            '\DatabaseTables\Customers:registration_date',
            '\DatabaseTables\Customers:reg_answer_date',
            '\DatabaseTables\Customers:service_level_send',
            '\DatabaseTables\Customers:contract_acknowledge',
            '\DatabaseTables\Customers:add_phone_lines',
            '\DatabaseTables\Customers:house_connection',
            '\DatabaseTables\Customers:recruited_by',
            '\DatabaseTables\Customers:contract_change_to',
            '\DatabaseTables\Customers:tal_cancel_ack_date',
            '\DatabaseTables\Customers:wisocontract_switchoff_finish',
            '\DatabaseTables\Customers:contract_online_date',
            '\DatabaseTables\Customers:special_conditions_from',
            '\DatabaseTables\Customers:special_conditions_till',
            '\DatabaseTables\Customers:phoneentry_done_date',
            '\DatabaseTables\Customers:gutschrift_till',
            '\DatabaseTables\Customers:contract_id',
            '\DatabaseTables\Customers:gf_cabling',
            '\DatabaseTables\Customers:gf_cabling_cost',
            '\DatabaseTables\Customers:firewall_router',
            '\DatabaseTables\Customers:stnz_fb',
            '\DatabaseTables\Customers:add_ip_address',
            '\DatabaseTables\Customers:r_d_n_s_installation',
            '\DatabaseTables\Customers:r_d_n_s_count',
            '\DatabaseTables\Customers:phone_number_block',
            '\DatabaseTables\Customers:add_phone_lines_cost07',
            '\DatabaseTables\Customers:german_network_price07',
            '\DatabaseTables\Customers:german_mobile_price',
            '\DatabaseTables\Customers:flat_german_network_cost',
            '\DatabaseTables\Customers:phone_adapter',
            '\DatabaseTables\Customers:flat_eu_network_cost',
            '\DatabaseTables\Customers:isdn_backup',
            '\DatabaseTables\Customers:isdn_backup2',
            '\DatabaseTables\Customers:new_number_date',
            '\DatabaseTables\Customers:contract_comment',
        ),
        'Altvertrag' => array(
            '\DatabaseTables\Customers:oldcontract_firstname',
            '\DatabaseTables\Customers:oldcontract_lastname',
            '\DatabaseTables\Customers:oldcontract_street',
            '\DatabaseTables\Customers:oldcontract_streetno',
            '\DatabaseTables\Customers:oldcontract_zipcode',
            '\DatabaseTables\Customers:oldcontract_city',
            '\DatabaseTables\Customers:oldcontract_internet_provider',
            '\DatabaseTables\Customers:oldcontract_phone_provider',
            '\DatabaseTables\Customers:oldcontract_internet_clientno',
            '\DatabaseTables\Customers:oldcontract_phone_clientno',
            '\DatabaseTables\Customers:exp_date_int',
            '\DatabaseTables\Customers:exp_date_phone',
            '\DatabaseTables\Customers:notice_period_internet_int',
            '\DatabaseTables\Customers:notice_period_phone_int',
        ),
        'Purtel' => array(
            //'\DatabaseTables\Customers:purtel_login_1',
            //'\DatabaseTables\Customers:purtel_passwort_1',
            //'\DatabaseTables\Customers:purtel_telnr_1',
            //'\DatabaseTables\Customers:purtel_login_2',
            //'\DatabaseTables\Customers:purtel_passwort_2',
            //'\DatabaseTables\Customers:purtel_telnr_2',
            //'\DatabaseTables\Customers:ventelo_port_wish_date',
            //'\DatabaseTables\Customers:purtel_login_3',
            //'\DatabaseTables\Customers:purtel_passwort_3',
            //'\DatabaseTables\Customers:purtel_telnr_3',
            //'\DatabaseTables\Customers:purtel_login',
            //'\DatabaseTables\Customers:purtel_passwort',
            //'\DatabaseTables\Customers:purtel_telnr',
            '\DatabaseTables\Customers:cancel_purtel_for_date',
            '\DatabaseTables\Customers:purtel_delete_date',
            '\DatabaseTables\PurtelAccounts:id',
            '\DatabaseTables\PurtelAccounts:purtel_login',
            '\DatabaseTables\PurtelAccounts:purtel_password',
            '\DatabaseTables\PurtelAccounts:master',
            '\DatabaseTables\PurtelAccounts:porting',
            '\DatabaseTables\PurtelAccounts:nummer',
            '\DatabaseTables\PurtelAccounts:nummernblock',
            '\DatabaseTables\PurtelAccounts:typ',
            '\DatabaseTables\PurtelAccounts:cust_id',
            '\DatabaseTables\PurtelAccounts:phone_book_information',
            '\DatabaseTables\PurtelAccounts:phone_book_digital_media',
            '\DatabaseTables\PurtelAccounts:phone_book_inverse_search',
            '\DatabaseTables\PurtelAccounts:itemized_bill',
            '\DatabaseTables\PurtelAccounts:itemized_bill_anonymized',
            '\DatabaseTables\PurtelAccounts:itemized_bill_shorted',
            '\DatabaseTables\PurtelAccounts:area_code',
        ),
        'TAL' => array(
            '\DatabaseTables\Customers:nb_canceled_date',
            '\DatabaseTables\Customers:nb_canceled_date_phone',
            '\DatabaseTables\Customers:tvs_date',
            '\DatabaseTables\Customers:tvs',
            '\DatabaseTables\Customers:purtel_bluckage_sended_date',
            '\DatabaseTables\Customers:purtel_bluckage_fullfiled_date',
            '\DatabaseTables\Customers:tal_dtag_assignment_no',
            '\DatabaseTables\Customers:dtag_line',
            '\DatabaseTables\Customers:tal_cancel_date',
            '\DatabaseTables\Customers:tal_canceled_for_date',
        ),
        'Techn. Daten' => array(
            '\DatabaseTables\Customers:mac_address',
            '\DatabaseTables\Customers:terminal_type',
            '\DatabaseTables\Customers:terminal_serialno',
            '\DatabaseTables\Customers:firmware_version',
            '\DatabaseTables\Customers:acs_id',
            '\DatabaseTables\Customers:password',
            '\DatabaseTables\Customers:remote_login',
            '\DatabaseTables\Customers:remote_password',
            '\DatabaseTables\Customers:subnetmask',
            '\DatabaseTables\Customers:kvz_addition',
            '\DatabaseTables\Customers:kvz_toggle_no',
            '\DatabaseTables\Customers:line_identifier',
            '\DatabaseTables\Customers:commissioning_wish_date',
            '\DatabaseTables\Customers:commissioning_date',
            '\DatabaseTables\Customers:commissioning_from',
            '\DatabaseTables\Customers:routing_wish_date',
            '\DatabaseTables\Customers:routing_active_date',
            '\DatabaseTables\Customers:routing_active',
            '\DatabaseTables\Customers:routing_deleted',
            '\DatabaseTables\Customers:d_p_b_o',
            '\DatabaseTables\Customers:techdata_comment',
        ),
        'Kündigung' => array(
            '\DatabaseTables\Customers:wisocontract_canceled_date',
            '\DatabaseTables\Customers:wisocontract_cancel_extra',
            '\DatabaseTables\Customers:wisocontract_cancel_input_date',
            '\DatabaseTables\Customers:wisocontract_cancel_ack',
            '\DatabaseTables\Customers:cancel_comment',
        ),
    );

    /**
     * {@inheritdoc}
     */
    protected function usedTables()
    {
        /**
         * Add new tables here!
         * 
         * This "table-objects" will be used
         */
        return array(
            '\DatabaseTables\Customers',
            '\DatabaseTables\LsaKvzPartitions',
            '\DatabaseTables\Locations',
            '\DatabaseTables\Networks',
            '\DatabaseTables\Cards',
            '\DatabaseTables\PurtelAccounts',
            '\DatabaseTables\MainProducts',
            '\DatabaseTables\MainProductCategorys',
            '\DatabaseTables\Optionen',
            '\DatabaseTables\CustomerOptions',
            '\DatabaseTables\CustomerTerminals',
            '\DatabaseTables\IpRanges',
            '\DatabaseTables\CustomerHistory',
            '\DatabaseTables\CustomerHistoryEvents',
        );
    }

    /**
     * Add new comparison operatores here!
     * 
     * Return appropriate comparision operator according to value
     * 
     * @param string $value
     */
    function getComparisonOperator(&$value, &$dontWrapValueInSingleQuotes)
    {
        switch (substr($value, 0, 2)) {
            case '>=':
                $value = preg_replace('/^>=\s+/', '', $value);
                return '>=';

            case '<=':
                $value = preg_replace('/^<=\s+/', '', $value);
                return '<=';
        }

        switch (substr($value, 0, 1)) {
            case '>':
                $value = preg_replace('/^>\s+/', '', $value);
                return '>';

            case '<':
                $value = preg_replace('/^<\s+/', '', $value);
                return '<';
        }

        if (0 === stripos($value, 'nicht null')) {
            $dontWrapValueInSingleQuotes = true;
            $value = 'NULL';
            
            return 'IS NOT';
        }

        if (0 === stripos($value, 'null')) {
            $dontWrapValueInSingleQuotes = true;
            $value = 'NULL';

            return 'IS';
        }

        if (0 === stripos($value, 'wie ')) {
            $value = preg_replace('/^wie\s+/i', '', $value);
            return 'LIKE';
        }

        if (0 === stripos($value, 'nicht wie ')) {
            $value = preg_replace('/^nicht wie\s+/i', '', $value);
            return 'NOT LIKE';
        }

        if (0 === stripos($value, 'nicht leer')) {
            $value = '';
            return '!=';
        }

        if (0 === stripos($value, 'nicht ')) {
            $value = preg_replace('/^nicht\s+/i', '', $value);
            return '!=';
        }

        if (0 === stripos($value, 'leer')) {
            $value = '';
            return '=';
        }

        if (0 === stripos($value, 'zwischen ')) {
            $value = preg_replace('/zwischen\s{1,}(.*)(?<!und\b)\bund\b(.*)/i', '$1AND$2', $value);
            return 'BETWEEN';
        }

        return '=';
    }

    /**
     * Add new value replacements here!
     * 
     * Replaces placeholders within value
     * 
     * @param string  $value
     * @param boolean $dontWrapReturnInSingleQuotes
     * 
     * @return string
     */
    public function valueReplacements($value, $dontWrapReturnInSingleQuotes = false)
    {
        if (false !== stripos($value, 'datum(')) {
            $value = preg_replace('/datum\((\d{2})\.(\d{2})\.(\d{4})\)/i', "'$3-$2-$1'", $value);
            $dontWrapReturnInSingleQuotes = true;
        }

        if (0 === stripos($value, 'jetzt()')) {
            $value = str_ireplace('jetzt()', 'NOW()', $value);
            $dontWrapReturnInSingleQuotes = true;
        }

        if ($dontWrapReturnInSingleQuotes) {
            return $value;
        }

        return "'".$value."'";
    }
}

?>