<?php
/**
 * 
 */

namespace csvExport;

/**
 * After Export execued the SQL-query the result will become an object of this class
 */
class ExportResult
{
	/**
	 * @var string
	 */
	protected $profileName;

	/**
	 * @var array
	 */
	protected $data;

	/**
	 * @var array
	 */
	protected $queryResultsToOriginalNames;

	/**
	 * @var array
	 */
	protected $sortedColumnNames;

	/**
	 * @param string $profileName
	 * 
	 * @return ExportResult
	 */
	public function setProfileName($profileName)
	{
		$this->profileName = $profileName;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getProfileName()
	{
		return $this->profileName;
	}
	
	/**
	 * @param array $data
	 * 
	 * @return ExportResult
	 */
	public function setData(array $data)
	{
		$this->data = $data;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getData()
	{
		return $this->data;
	}
	
	/**
	 * @param array $queryResultsToOriginalNames
	 * 
	 * @return ExportResult
	 */
	public function setQueryResultsToOriginalNames(array $queryResultsToOriginalNames)
	{
		$this->queryResultsToOriginalNames = $queryResultsToOriginalNames;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getQueryResultsToOriginalNames()
	{
		return $this->queryResultsToOriginalNames;
	}

	/**
	 * @param array $sortedColumnNames
	 * 
	 * @return ExportResult
	 */
	public function setSortedColumnNames($sortedColumnNames)
	{
		$this->sortedColumnNames = $sortedColumnNames;

		return $this;
	}

	/**
	 * @param string $which - optional
	 * 
	 * @return array
	 */
	public function getSortedColumnNames($which = null)
	{
		switch ($which) {
			case 'original':
				return $this->sortedColumnNames['original'];

			case 'humanized':
				return $this->sortedColumnNames['humanized'];
		}

		return $this->sortedColumnNames;
	}

	/**
	 * @param string $tableName
	 * 
	 * @return boolean
	 */
	public function hasTable($tableName)
	{
		if (is_array($this->queryResultsToOriginalNames) && isset($this->queryResultsToOriginalNames[$tableName])) {
			return true;
		}

		return false;
	}

	/**
	 * @param string $tableName
	 * @param string $columnOriginalName
	 * 
	 * @return boolean
	 */
	public function hasColumn($tableName, $columnOriginalName)
	{
		if ($this->hasTable($tableName) && isset($this->queryResultsToOriginalNames[$tableName]['columns'][$columnOriginalName])) {
			return true;
		}

		return false;
	}

	/**
	 * @param string $tableName
	 * @param string $columnOriginalName
	 * 
	 * @throws \Exception
	 * 
	 * @return mixed
	 */
	public function getValueKey($tableName, $columnOriginalName)
	{
		if ($this->hasColumn($tableName, $columnOriginalName)) {
			return $this->queryResultsToOriginalNames[$tableName]['columns'][$columnOriginalName];
		}

		throw new \Exception(sprintf('Table (%s) or Column (%s) doesn\'t exists',
            $tableName,
            $columnOriginalName
        ));
	}

	/**
	 * @param string $tableName
	 * 
	 * @throws \Exception
	 * 
	 * @return array
	 */
	public function getTableColumns($tableName)
	{
		if ($this->hasTable($tableName)) {
			return $this->queryResultsToOriginalNames[$tableName]['columns'];
		}

		throw new \Exception('Table doesn\'t exists');
	}
}

?>