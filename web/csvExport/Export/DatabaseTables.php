<?php
/**
 * 
 */

namespace csvExport;

/**
 * 
 */
abstract class DatabaseTables
{
    /**
     * @var array
     */
    protected $loadedTables;

    /**
     * @var array
     */
    protected $groupedColumns = array();

    /**
     * This "tables" will be used
     * 
     * @return array
     */
    abstract protected function usedTables();

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->loadUsedTables();
        $this->loadGroupedColumns();
    }

    /**
     * load grouped columns
     * 
     * @return void
     */
    private function loadGroupedColumns()
    {
        if (!isset($this->groupColumns)) {
            return;
        }

        foreach ($this->groupColumns as $groupHeadline => $values) {
            foreach ($values as $classAndColumn) {
                $classAndColumn_ = explode(':', $classAndColumn);

                if (2 != count($classAndColumn_)) {
                    throw new \Exception('Invalid value in \\csvExport\\ExportTables::$groupColumns (value: '.$classAndColumn.')');
                }

                $className = $classAndColumn_[0];
                $columnOriginalName = $classAndColumn_[1];

                if (!isset($this->loadedTables[$className])) {
                    throw new \Exception('Class ('.$className.') used for \\csvExport\\ExportTables::$groupColumns '
                        .'is not in \\csvExport\\ExportTables::usedTables()');
                }

                if (!isset($this->groupedColumns[$groupHeadline])) {
                    $this->groupedColumns[$groupHeadline] = array();
                }

                /**
                 * Ensure this classAndColumn is used only once!
                 */
                array_map(
                    function($array) use ($className, $columnOriginalName, $classAndColumn) {
                        $found = array_search(
                            array(
                                $this->loadedTables[$className],
                                $columnOriginalName,
                            ),
                            $array,
                            true
                        );

                        if (false !== $found) {
                            throw new \Exception('Invalid value ('.$classAndColumn
                                .') in \\csvExport\\ExportTables::$groupColumns (value was used before)');
                        }
                    },
                    $this->groupedColumns
                );

                $this->groupedColumns[$groupHeadline][] = array(
                    $this->loadedTables[$className],
                    $columnOriginalName,
                );
            }
        }
    }

    /**
     * @return array
     */
    public function getGroupedColumns()
    {
        return $this->groupedColumns;
    }

    /**
     * load table-objects
     * 
     * @throws \Exception
     * 
     * @return void
     */
    protected function loadUsedTables()
    {
        if (is_array($this->loadedTables)) {
            return;
        }

        try {
            foreach ($this->usedTables() as $className) {
                $file = __DIR__.'/../'.str_replace('\\', '/', $className).'.php';

                if (!file_exists($file) || !is_readable($file)) {
                    throw new \Exception($file.' either not found or not readable');
                }

                include $file;

                if (!class_exists($className)) {
                    throw new \Exception($className.' - class or namespace not found in '.$file);
                }

                $this->loadedTables[$className] = new $className;
            }
        } catch(\Exception $e) {
            $this->loadedTables = null;

            throw $e;
        }
    }

    /**
     * @return array
     */
    public function getTables()
    {
        return $this->loadedTables;
    }

    /**
     * @param string $originalName
     * 
     * @return Table|null
     */
    public function findTableByOriginalName($originalName)
    {
        foreach ($this->loadedTables as $table) {
            if ($originalName == $table->getOriginalName()) {
                return $table;
            }
        }

        return null;
    }
}

?>