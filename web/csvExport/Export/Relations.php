<?php
/**
 * 
 */

namespace csvExport;

/**
 * 
 */
class Relations
{
    /**
     * @var array
     */
    protected $recordedTables = array();

    /**
     * @var array
     */
    protected $relations = array();

    /**
     * Table has relations?
     * 
     * @param \DatabaseTables\Table $currentTable
     * @param string                $tableName
     */
    public function analyzeRelation($currentTable, $tableName)
    {
        foreach ($this->recordedTables as $table) {
            if (null !== $relation = $table->getRelationByForeignTable($tableName, true)) {
                $this->relations[] = $relation;
            }
        }

        $this->recordedTables[] = $currentTable;
    }

    /**
     * @return array
     */
    public function get()
    {
        return $this->relations;
    }

    /**
     * @return boolean
     */
    public function has()
    {
        return !empty($this->relations);
    }
}
?>