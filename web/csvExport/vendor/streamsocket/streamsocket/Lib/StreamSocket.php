<?php
/**
 *
 */

namespace StreamSocket\Lib;

require_once __DIR__.'/../Exception/WaitUntilStringIsFoundException.php';

use \StreamSocket\Exception\WaitUntilStringIsFoundException;

/**
 * Provides an OOP-Way to handle Stream_Socket-Connections
 * 
 * Allways use StreamSocket::setTimeout to set the timeout!
 * Do not use stream_set_timeout directly on the stream instance!
 * (it will break the timeout reset in waitUntilStringIsFound())
 */
class StreamSocket
{
    /**
     * Protocol TCP
     * 
     * @var string
     */
    const TCP = 'tcp';

    /**
     * Protocol UDP
     * 
     * @var string
     */
    const UDP = 'udp';

    /**
     * Stream_Socket-Resource
     * 
     * @var resource
     */
    protected $stream;

    /**
     * Default timeout
     * 
     * @var array
     */
    protected $defaultTimeout = array('sec' => 30, 'usec' => 0);

    /**
     * Currently set timeout
     * 
     * @var array
     */
    protected $currentTimeout;

    /**
     * Microseconds
     * (1 mio = 1 sec)
     * 
     * @var integer
     */
    protected $waitUntilStringIsFoundInterval = 100000;

    /**
     * @var string
     */
    protected $lastReadData;

    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Destructor.
     * Auto disconnect
     * 
     * @return void
     */
    public function __destruct()
    {
        $this->disconnect();
    }

    /**
     * Shortcut-Alias for Constructor
     * 
     * @return StreamSocket
     */
    public static function newInstance()
    {
        return new static();
    }

    /**
     * Establish client connection to remote host via stream_socket
     * 
     * @param string  $protocol
     * @param string  $ipAddress
     * @param integer $port
     * @param array   $timeout - optional
     * 
     * @throws \Exception
     * 
     * @return StreamSocket
     */
    public function connect($protocol, $ipAddress, $port, array $timeout = null)
    {
        if (null === $timeout) {
            $timeout = $this->defaultTimeout;
        }

        $this->currentTimeout = $timeout;

        switch ($protocol) {
            case self::TCP:
            case self::UDP:
                break;
            
            default:
                throw new \Exception('Invalid protocol.');
        }

        $this->stream = stream_socket_client($protocol.'://'.$ipAddress.':'.$port, $errno, $errorMessage, $timeout['sec']);

        if (!is_resource($this->stream)) {
            throw new \Exception('Failed to connect to server. '.$errorMessage.' - '.$protocol.'://'.$ipAddress.':'.$port);
        }

        $this->setTimeout($timeout);

        return $this;
    }

    /**
     * Disconnect
     * 
     * @return void
     */
    public function disconnect()
    {
        if (is_resource($this->stream)) {
            fclose($this->stream);

            $this->stream = null;
        }
    }

    /**
     * @param array $timeout
     * 
     * @throws \InvalidArgumentException
     * 
     * @return StreamSocket
     */
    public function setTimeout(array $timeout)
    {
        if (!isset($timeout['sec']) || !isset($timeout['usec'])) {
            throw new \InvalidArgumentException('timeout invalid');
        }

        if (!stream_set_timeout($this->stream, $timeout['sec'], $timeout['usec'])) {
            throw new \InvalidArgumentException('failed to set timeout');
        }

        $this->currentTimeout = $timeout;

        return $this;
    }

    /**
     * Write to stream
     * 
     * @param string $package
     * 
     * @throws \Exception
     * 
     * @return StreamSocket
     */
    public function write($package)
    {
        if (false === fwrite($this->stream, $package)) {
            throw new \Exception('Writing to stream failed.');
        }

        return $this;
    }

    /**
     * Read from stream
     * 
     * @param integer $maxLength - optional
     * @param integer $offset - optional
     * 
     * @throws \Exception
     * 
     * @return string
     */
    public function read($maxLength = -1, $offset = -1)
    {
        $contents = stream_get_contents($this->stream, $maxLength, $offset);

        if (false === $contents) {
            throw new \Exception('Failed to read from stream.');
        }

        return $contents;
    }

    /**
     * Use getLastReadData() to reset the buffer!
     * 
     * @param string  $string
     * @param integer $maxWaitTime - optional
     * @param string  $errorMessage - optional
     * @param boolean $useRegularExpression - optional
     * @param array   $stringsThatAlsoBreak - optional
     * 
     * @throws \InvalidArgumentException|\StreamSocket\Exception\WaitUntilStringIsFoundException
     * 
     * @return SteamSocket
     */
    public function waitUntilStringIsFound($string, $maxWaitTime = null, $errorMessage = null, $useRegularExpression = false, $stringsThatAlsoBreak = null)
    {
        $string = (string) $string;

        if (empty($string)) {
            throw new \InvalidArgumentException('invalid value for string');
        }

        if (null === $errorMessage) {
            $errorMessage = 'unable to find string';
        }

        $currentTimeout = $this->currentTimeout;

        if (!is_int($maxWaitTime)) {
            $maxWaitTime = $currentTimeout['sec'];
        }

        $maxWaitTime = round($maxWaitTime / $this->waitUntilStringIsFoundInterval * 1e6);

        $this->setTimeout(array('sec' => 0, 'usec' => $this->waitUntilStringIsFoundInterval));

        $found = false;

        while (!feof($this->stream)) {
            $data = stream_get_contents($this->stream);

            if (false === $data) {
                break;
            }

            $this->lastReadData .= $data;

            if (0 == $maxWaitTime--) {
                break;
            }

            if (empty($data)) {
                continue;
            }

            if (is_array($stringsThatAlsoBreak)) {
                foreach ($stringsThatAlsoBreak as $str => $error) {
                    if (false !== strpos($data, $str)) {
                        $errorMessage .= ' ('.$error.')';

                        break 2;
                    }
                }
            }

            if ($useRegularExpression) {
                if (1 === preg_match($string, $data)) {
                    $found = true;

                    break;
                }
            } else {
                if (false !== strpos($data, $string)) {
                    $found = true;

                    break;
                }
            }
        }

        $this->setTimeout($currentTimeout);

        if ($found) {
            return $this;
        }

        throw new WaitUntilStringIsFoundException($errorMessage);
    }

    /**
     * Return last read data from waitUntilStringIsFound and resets the buffer
     * 
     * @return string
     */
    public function getLastReadData()
    {
        $data = $this->lastReadData;

        $this->lastReadData = null;

        return $data;
    }
}

?>