<?php
/**
 * Alles was hier drin steht soll irgendwann mal in die Datenbank verschwinden.
 */

namespace DslamCommands;

/**
 * Holds DSLAM informations and handles the "special cases"
 */
class DslamAccess
{
	/**
	 * Key = LocationId
	 * 
	 * @var array
	 */
	private $access = array(
		1 => array(
			'username' => null,
			'name' => 'Lerchenaecker',
			'backup' => 'Lerchenaecker.tar',
			'adminSave' => 'admin save',
			'ipAddress' => null,
			'port' => null,
			'PPPoE' => array(
				'location' => 'Hoefliger',
				'ipAddress' => '0.0.0.0',
				'type' => 'Mikrotik',
			),
			'vlan' => '402',
			'profile' => 'PPPoE',
			'conf' => false,
			'type' => null,
			'vectorLocation' => null,
		),
		2 => array(
			'username' => 'khl://puttytelnet:0.0.0.6:61011',
			'name' => 'AiT',
			'backup' => 'AiT.tar',
			'adminSave' => 'admin save',
			'ipAddress' => '0.0.0.6',
			'port' => '61011',
			'PPPoE' => array(
				'location' => 'AiT',
				'ipAddress' => '195.178.1.2',
				'type' => 'Bartels',
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => true,
			'type' => null,
			'vectorLocation' => null,
		),
		3 => array(
			'username' => 'khl://puttytelnet:0.0.0.6:33333',
			'name' => 'Maubach Wohnen 4',
			'backup' => 'W4Gpon.tar',
			'adminSave' => null,
			'ipAddress' => '0.0.0.6',
			'port' => '33333',
			'PPPoE' => array(
				'location' => null,
				'ipAddress' => null,
				'type' => null,
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => true,
			'type' => null,
			'vectorLocation' => null,
		),
		4 => array(
			'username' => 'khl://puttytelnet:0.0.0.10:62321',
			'name' => 'Allmersbach am Weinberg',
			'backup' => 'AWW.tar',
			'adminSave' => 'admin software-mngt shub database save',
			'ipAddress' => '0.0.0.10',
			'port' => '62321',
			'PPPoE' => array(
				'location' => 'Lerchenäcker',
				'ipAddress' => '0.0.0.10',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'new2015',
			'conf' => true,
			'type' => null,
			'vectorLocation' => true,
		),
		5 => array(
			'username' => 'khl://puttytelnet:0.0.0.10:62322',
			'name' => 'Rietenau',
			'backup' => 'RIE.tar',
			'adminSave' => 'admin software-mngt shub database save',
			'ipAddress' => '0.0.0.10',
			'port' => '62322',
			'PPPoE' => array(
				'location' => 'Lerchenäcker',
				'ipAddress' => '0.0.0.10',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			//'profile' => 'new2015',
			'profile' => 'PPPoE',
			'conf' => true,
			'type' => null,
			'vectorLocation' => true,
		),
		6 => array(
			'username' => 'khl://puttytelnet:0.0.0.5:51004',
			'name' => 'Klein_Aspach',
			'backup' => 'klein_Aspach.tar',
			'adminSave' => null,
			'ipAddress' => '0.0.0.5',
			'port' => '51004',
			'PPPoE' => array(
				'location' => 'Lerchenäcker',
				'ipAddress' => '0.0.0.10',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'new2015',
			'conf' => true,
		),
		8 => array(
			'username' => 'khl://puttytelnet:46.237.221.86:63027',
			'name' => 'Burgrieden',
			'backup' => 'Burgrieden.tar',
			'adminSave' => null,
			'ipAddress' => '46.237.221.86',
			'port' => '63027',
			'PPPoE' => array(
				'location' => 'Burgrieden',
				'ipAddress' => '185.8.87.254',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE_185',
			'conf' => true,
			'type' => null,
			'vectorLocation' => null,
		),
		10 => array(
			'username' => 'khl://puttytelnet:0.0.0.104:60018',
			'name' => 'Hanweiler',
			'backup' => 'Hanweiler.tar',
			'adminSave' => 'admin save',
			'ipAddress' => '0.0.0.104',
			'port' => '60018',
			'PPPoE' => array(
				'location' => 'Zettachring',
				'ipAddress' => '0.0.0.1',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE_Schelm',
			'conf' => true,
			'type' => null,
			'vectorLocation' => null,
		),
		13 => array(
			'username' => 'khl://puttytelnet:0.0.0.104:60028',
			'name' => 'Schelmenholz-Master',
			'backup' => 'SCHELM.tar',
			'adminSave' => 'admin save',
			'ipAddress' => '0.0.0.104',
			'port' => '60028',
			'PPPoE' => array(
				'location' => 'Zettachring',
				'ipAddress' => '0.0.0.1',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE_Schelm',
			'conf' => true,
			'type' => null,
			'vectorLocation' => null,
		),
		14 => array(
			'username' => 'khl://puttytelnet:0.0.0.104:61031',
			'name' => 'Schelmenholz-Slave',
			'backup' => 'SCHELM_sued.tar',
			'adminSave' => 'admin software-mngt shub database save',
			'ipAddress' => '0.0.0.104',
			'port' => '61031',
			'PPPoE' => array(
				'location' => 'Zettachring',
				'ipAddress' => '0.0.0.1',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE_Schelm',
			'conf' => true,
			'type' => null,
			'vectorLocation' => null,
		),
		18 => array(
			'username' => 'khl://puttytelnet:0.0.0.6:10035',
			'name' => 'Nellmersbach_Master',
			'backup' => 'Nellm_mast.tar',
			'adminSave' => null,
			'ipAddress' => '0.0.0.6',
			'port' => '10035',
			'PPPoE' => array(
				'location' => 'Hoefliger',
				'ipAddress' => '0.0.0.0',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => true,
			'type' => null,
			'vectorLocation' => null,
		),
		19 => array(
			'username' => 'khl://puttytelnet:0.0.0.6:10036',
			'name' => 'Nellmersbach_Slave',
			'backup' => 'Nellm_slave.tar',
			'adminSave' => null,
			'ipAddress' => '0.0.0.6',
			'port' => '10036',
			'PPPoE' => array(
				'location' => 'Hoefliger',
				'ipAddress' => '0.0.0.0',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => true,
			'type' => null,
			'vectorLocation' => null,
		),
		26 => array(
			'username' => 'khl://kmile',
			'name' => 'Waldrems Keymile',
			'backup' => 'Waldrems Keymile',
			'adminSave' => null,
			'ipAddress' => '192.168.1.98',
			'port' => '5556',
			'PPPoE' => array(
				'location' => 'Zettachring',
				'ipAddress' => '0.0.0.1',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => false,
			'type' => 'KEYMILE',
			'vectorLocation' => null,
		),
		35 => array(
			'username' => 'khl://puttytelnet:0.0.0.0:62001',
			'name' => 'Weiler_z_St_Master',
			'backup' => 'Weiler_z_St_Master.tar',
			'adminSave' => null,
			'ipAddress' => '0.0.0.0',
			'port' => '62001',
			'PPPoE' => array(
				'location' => 'Hoefliger',
				'ipAddress' => '0.0.0.0',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => true,
			'type' => null,
			'vectorLocation' => null,
		),
		28 => array(
			'username' => 'khl://puttytelnet:0.0.0.0:62002',
			'name' => 'Weiler_z_St_Slave',
			'backup' => 'Weiler_z_St_Slave.tar',
			'adminSave' => null,
			'ipAddress' => '0.0.0.0',
			'port' => '62002',
			'PPPoE' => array(
				'location' => 'Hoefliger',
				'ipAddress' => '0.0.0.0',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => true,
			'type' => null,
			'vectorLocation' => null,
		),
		32 => array(
			'username' => 'khl://puttytelnet:0.0.0.0:62005',
			'name' => 'Hertmannsweiler_Master',
			'backup' => 'Hertmansw_Master.tar',
			'adminSave' => null,
			'ipAddress' => '0.0.0.0',
			'port' => '62005',
			'PPPoE' => array(
				'location' => 'Hoefliger',
				'ipAddress' => '0.0.0.0',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => true,
			'type' => null,
			'vectorLocation' => null,
		),
		40 => array(
			'username' => 'khl://puttytelnet:0.0.0.0:64006',
			'name' => 'Kuchengrund',
			'backup' => 'Kuchengrund.tar',
			'adminSave' => 'admin software-mngt shub database save',
			'ipAddress' => '213.61.107.128',
			'port' => '64006',
			'PPPoE' => array(
				'location' => 'Hoefliger',
				'ipAddress' => '213.61.107.128',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => true,
			'vectorLocation' => true,
		),
    41 => array(
      'username' => 'khl://puttytelnet:0.0.0.0:64006',
      'name' => 'Kuchengrund',
      'backup' => 'Kuchengrund.tar',
      'adminSave' => 'admin software-mngt shub database save',
      'ipAddress' => '213.61.107.128',
      'port' => '64006',
      'PPPoE' => array(
        'location' => 'Hoefliger',
        'ipAddress' => '213.61.107.128',
        'type' => 'Mikrotik',
      ),
      'vlan' => null,
      'profile' => 'PPPoE',
      'conf' => true,
      'vectorLocation' => true,
    ),
		42 => array(
			'username' => 'khl://puttytelnet:0.0.0.0:62003',
			'name' => 'Hertmannsweiler_Slave',
			'backup' => 'Hertmansw_Slave.tar',
			'adminSave' => null,
			'ipAddress' => '0.0.0.0',
			'port' => '62003',
			'PPPoE' => array(
				'location' => 'Hoefliger',
				'ipAddress' => '0.0.0.0',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => true,
			'type' => null,
			'vectorLocation' => null,
		),
		45 => array(
			'username' => 'khl://puttytelnet:0.0.0.5:51008',
			'name' => 'Forstboden',
			'backup' => 'Forstboden.tar',
			'adminSave' => null,
			'ipAddress' => '0.0.0.5',
			'port' => '51008',
			'PPPoE' => array(
				'location' => 'Lerchenäcker',
				'ipAddress' => '0.0.0.10',
				'type' => 'Bartels',
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => true,
			'type' => null,
			'vectorLocation' => null,
		),
		46 => array(
			'username' => 'khl://puttytelnet:185.8.85.3:50005',
			'name' => 'Labor',
			'backup' => 'Labor.tar',
			'adminSave' => null,
			'ipAddress' => '185.8.85.3',
			'port' => '50005',
			'PPPoE' => array(
				'location' => 'Labor',
				'ipAddress' => '185.8.85.3',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => true,
			'type' => null,
			'vectorLocation' => true,
		),
		48 => array(
			'username' => 'khl://puttytelnet:0.0.0.104:55010',
			'name' => 'Bittenfeld_Master1',
			'backup' => 'Bittenfeld_Master1.tar',
			'adminSave' => null,
			'ipAddress' => '0.0.0.104',
			'port' => '55010',
			'PPPoE' => array(
				'location' => 'Zettachring',
				'ipAddress' => '0.0.0.1',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => true,
			'type' => null,
			'vectorLocation' => null,
		),
		49 => array(
			'username' => 'khl://puttytelnet:0.0.0.104:55012',
			'name' => 'Bittenfeld_Master3',
			'backup' => 'Bittenfeld_Master3.tar',
			'adminSave' => null,
			'ipAddress' => '0.0.0.104',
			'port' => '55012',
			'PPPoE' => array(
				'location' => 'Zettachring',
				'ipAddress' => '0.0.0.1',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => true,
			'type' => null,
			'vectorLocation' => null,
		),
		51 => array(
			'username' => 'khl://puttytelnet:0.0.0.104:55011',
			'name' => 'Bittenfeld_Master2',
			'backup' => 'Bittenfeld_Master2.tar',
			'adminSave' => null,
			'ipAddress' => '0.0.0.104',
			'port' => '55011',
			'PPPoE' => array(
				'location' => 'Zettachring',
				'ipAddress' => '0.0.0.1',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => true,
			'type' => null,
			'vectorLocation' => null,
		),
		54 => array(
			'username' => 'khl://puttytelnet:0.0.0.0:63013',
			'name' => 'AiT_B8',
			'backup' => 'AiT_B8.tar',
			'adminSave' => 'admin software-mngt shub database save',
			'ipAddress' => '0.0.0.0',
			'port' => '63013',
			'PPPoE' => array(
				'location' => 'Hoefliger',
				'ipAddress' => '0.0.0.0',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => true,
			'type' => null,
			'vectorLocation' => true,
		),
		55 => array(
			'username' => 'khl://puttytelnet:0.0.0.234:55023',
			'name' => 'PONO Leonberg',
			'backup' => 'AiT_B8.tar',
			'adminSave' => 'admin save',
			'ipAddress' => '0.0.0.234',
			'port' => '55023',
			'PPPoE' => array(
				'location' => 'Leonberg',
				'ipAddress' => '0.0.0.234',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => true,
			'type' => 'ISKRATEL',
			'vectorLocation' => null,
		),
		56 => array(
			'username' => 'khl://puttytelnet:0.0.0.6:53100',
			'name' => 'AiT_Rathaus',
			'backup' => 'AiT_Rathaus.tar',
			'adminSave' => 'admin save',
			'ipAddress' => '0.0.0.6',
			'port' => '53100',
			'PPPoE' => array(
				'location' => 'AiT Schule',
				'ipAddress' => '195.178.1.2',
				'type' => 'Bartels',
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => true,
			'type' => null,
			'vectorLocation' => null,
		),
		57 => array(
			'username' => 'khl://puttytelnet:185.8.85.3:55015',
			'name' => 'Labor PONO',
			'backup' => 'Labor.tar',
			'adminSave' => null,
			'ipAddress' => '185.8.83.4',
			'port' => '55015',
			'PPPoE' => array(
				'location' => 'Labor PONO',
				'ipAddress' => '185.8.83.4',
				'type' => 'Mikrotik',
			),
			'vlan' => null,
			'profile' => 'PPPoE',
			'conf' => true,
			'type' => 'ISKRATEL',
			'vectorLocation' => null,
		),
	);

	/**
	 * @var array
	 */
	private $duplicates = array(
		13 => array(15),
		8 => array(31, 9, 25),
		19 => array(20, 21, 22, 23, 24),
		35 => array(36),
		28 => array(33, 34),
		6 => array(37),
		32 => array(43),
		40 => array(41, 47, 60),
		48 => array(50),
		51 => array(52, 53),
	);

	/**
	 * Constructor
	 */
	public function __construct()
	{
		foreach ($this->duplicates as $locationId => $array) {
			foreach ($array as $newLocationId) {
				$this->access[$newLocationId] = $this->access[$locationId];
			}
		}
	}

	/**
	 * @return DslamAccess
	 */
	public static function newInstance()
	{
		return new self();
	}

	/**
	 * @param integer $locationId
	 * @param string  $ipAs - optional
	 * 
	 * @throws \InvalidArgumentException
	 * 
	 * @return array
	 */
	public function get($locationId, $ipAs = null)
	{
		$locationId = (int) $locationId;

		if (!isset($this->access[$locationId])) {
			throw new \InvalidArgumentException('No data found for this location. ('.$locationId.')');
		}

		$this->checkSpecialCases($locationId, $ipAs);

		return $this->access[$locationId];
	}

	/**
	 * @param integer $locationId
	 * @param string  $ipAs - optional
	 * 
	 * @return void
	 */
	public function checkSpecialCases($locationId, $ipAs = null)
	{
		switch ($locationId) {
			case 3:
				$this->specialCase3($ipAs);
				break;

			case 6:
				$this->specialCase6($locationId, $ipAs);
				break;

			case 37:
				$this->specialCase37($ipAs);
				break;

			case 40:
				$this->specialCase40($locationId, $ipAs);
				break;

			case 41:
				$this->specialCase41($ipAs);
				break;

			case 47:
				$this->specialCase47($ipAs);
				break;

			case 60:
				$this->specialCase60($ipAs);
				break;

			case 45:
				$this->specialCase45($locationId, $ipAs);
				break;
		}
	}

	/**
	 * @param string $ipAs
	 * 
	 * @throws \InvalidArgumentException
	 * 
	 * @return void
	 */
	public function checkIpAs($ipAs)
	{
		if (!is_string($ipAs) || empty($ipAs)) {
			throw new \InvalidArgumentException('IpAs not provided.');
		}
	}

	/**
	 * Special case for locationId 3
	 * 
	 * @param string $ipAs
	 * 
	 * @return void
	 */
	public function specialCase3($ipAs)
	{
		$this->checkIpAs($ipAs);

		$subip = substr($ipAs, 0, 5);

		switch ($subip) {
			case '10.11':
			case '37.99':
				$this->access[3]['PPPoE']['location'] = 'Zettachring';
				$this->access[3]['PPPoE']['ipAddress'] = '0.0.0.1';
				$this->access[3]['PPPoE']['type'] = 'Mikrotik';
				$this->access[3]['vlan'] = '217';

				if (substr($ipAs, 0, 9) <> '37.99.200') {
					$this->access[3]['vlan'] = '199';
				}

				break;

			case '10.10':
			case '185.6':
				$this->access[3]['PPPoE']['location'] = 'Hoefliger';
				$this->access[3]['PPPoE']['ipAddress'] = '0.0.0.0';
				$this->access[3]['PPPoE']['type'] = 'Mikrotik';
				$this->access[3]['vlan'] = '402';

				break;
		}
	}

	/**
	 * Special case for locationId 6
	 * 
	 * @param integer $locationId
	 * @param string  $ipAs
	 * 
	 * @return void
	 */
	public function specialCase6($locationId, $ipAs)
	{
		$this->checkIpAs($ipAs);

		if ('109.75.99' == substr($ipAs, 0, 9)) {
			$this->access[$locationId]['PPPoE']['location'] = 'Zettachring Test PPPoE';
			$this->access[$locationId]['PPPoE']['ipAddress'] = '109.75.99.5';
		}
	}

	/**
	 * Special case for locationId 37
	 * 
	 * @param string $ipAs
	 * 
	 * @return void
	 */
	public function specialCase37($ipAs)
	{
		$this->specialCase6($ipAs);
	}

	/**
	 * Special case for locationId 40
	 * 
	 * @param integer $locationId
	 * @param string  $ipAs
	 * 
	 * @return void
	 */
	public function specialCase40($locationId, $ipAs)
	{
		$this->checkIpAs($ipAs);

		switch (substr($ipAs, 0, 5)) {
			case '10.11':
			case '37.99':
				$this->access[$locationId]['PPPoE']['location'] = 'Zettachring';
				$this->access[$locationId]['PPPoE']['ipAddress'] = '0.0.0.1';
				$this->access[$locationId]['PPPoE']['type'] = 'Mikrotik';
				$this->access[$locationId]['profile'] = 'PPPoE';
				break;
		}
	}

	/**
	 * Special case for locationId 41
	 * 
	 * @param string $ipAs
	 * 
	 * @return void
	 */
	public function specialCase41($ipAs)
	{
		$this->specialCase40($ipAs);
	}

	/**
	 * Special case for locationId 47
	 * 
	 * @param string $ipAs
	 * 
	 * @return void
	 */
	public function specialCase47($ipAs)
	{
		$this->specialCase40($ipAs);
	}

	/**
	 * Special case for locationId 60
	 * 
	 * @param string $ipAs
	 * 
	 * @return void
	 */
	public function specialCase60($ipAs)
	{
		$this->specialCase40($ipAs);
	}

	/**
	 * Special case for locationId 45
	 * 
	 * @param integer $locationId
	 * @param string  $ipAs
	 * 
	 * @return void
	 */
	public function specialCase45($locationId, $ipAs)
	{
		$this->checkIpAs($ipAs);

		switch (substr($ipAs, 0, 5)) {
			case '10.11':
			case '37.99':
			case '109.7':
				$this->access[$locationId]['PPPoE']['location'] = 'Lerchenäcker';
				$this->access[$locationId]['PPPoE']['ipAddress'] = '0.0.0.10';
				$this->access[$locationId]['PPPoE']['type'] = 'Mikrotik';
				$this->access[$locationId]['profile'] = 'new2015';

				if ('109.75.99' == substr($ipAs, 0, 9)) {
					$this->access[$locationId]['PPPoE']['location'] = 'Zettachring Test PPPoE';
					$this->access[$locationId]['PPPoE']['ipAddress'] = '109.75.99.5';
				}

				break;
		}
	}
}
?>