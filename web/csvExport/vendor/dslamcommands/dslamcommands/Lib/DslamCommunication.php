<?php
/**
 * 
 */

namespace DslamCommands\Lib;

require_once __DIR__.'/../../../streamsocket/streamsocket/Lib/StreamSocket.php';
require_once __DIR__.'/../Exception/LoginFailedException.php';
require_once __DIR__.'/../Exception/ConnectionFailedException.php';

use \StreamSocket\Lib\StreamSocket;
use \DslamCommands\Exception\LoginFailedException;
use \DslamCommands\Exception\ConnectionFailedException;

/**
 * @todo
 *      Global Commands ??
 *      -case 'DSLAM Kunden Status maximal':
 *      -case 'DSLAM Kunden Config':
 *      -case 'DSLAM Equipmentslot':
 *      -case 'DSLAM Service':
 *      -case 'DSLAM VLAN':
 *      -case 'ShowSoftware':
 *      -case 'DSLAM Diagnose_SFP':
 *      -case 'DSLAM show Port':
 *      -case 'DSLAM Diagnose_SFP':
 *      -case 'DSLAM Line up':
 *      -case 'DSLAM Line upsum':
 *      -case 'DSLAM Line opup':
 *      -case 'ShowPonOptics':
 *      -case 'ShowOntOptics':
 *      -case 'ShowOntStatus':
 *      -case 'ShowOntupS':
 *      -case 'ShowOntdownS':
 *      -case 'ShowOntup':
 *      -case 'ShowOntdown':
 *      -case 'EQ_planedA':
 *      -case 'EQ_planedA_C':
 *      -case 'EQ_planedC':
 *      -case 'EQ_planedC_C':
 */
class DslamCommunication
{
	/**
	 * @var \StreamSocket\Lib\StreamSocket
	 */
	protected $stream;

	/**
	 * Connect to DSLAM
	 * 
	 * @param string  $ipAddress
	 * @param integer $port
	 * @param array   $timeout - optional
	 * 
	 * @throws \DslamCommands\Exception\ConnectionFailedException
	 * 
	 * @return DslamCommunication
	 */
	public function connect($ipAddress, $port, $timeout = array('sec' => 10, 'usec' => 0))
	{
		try {
			$this->stream = StreamSocket::newInstance()
				->connect(StreamSocket::TCP,
					$ipAddress,
					$port,
					$timeout
				)
			;

			return $this;
		} catch (\Exception $e) {
			throw new ConnectionFailedException($e->getMessage());
		}
	}

	/**
	 * Logout and disconnect from DSLAM
	 * 
	 * @return DslamCommunication
	 */
	public function disconnect()
	{
		$this->logout();

		return $this;
	}

	/**
	 * Login to DSLAM
	 * 
	 * @throws \DslamCommands\Exception\LoginFailedException
	 * 
	 * @return DslamCommunication
	 */
	public function login($username, $password)
	{
		try {
			$stringsThatAlsoBreak = array(
				'incorrect' => 'Login failed (Username or Password incorrect)',
			);

			$this->stream
				->waitUntilStringIsFound('login:', null, 'Did not receive login-prompt', false)
				->write($username."\r\n")
				->waitUntilStringIsFound('password:', null, 'Did not receive password-prompt', false, $stringsThatAlsoBreak)
				->write($password."\r\n")
				->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true, $stringsThatAlsoBreak)
			;

			return $this;
		} catch (\StreamSocket\Exception\WaitUntilStringIsFoundException $e) {
			throw new LoginFailedException($e->getMessage());
		} catch (\Exception $e) {
			throw new LoginFailedException('Uncaught Exception ('.$e->getMessage().')');
		}
		
		throw new LoginFailedException('Login faild with no response.');
	}

	/**
	 * Logout and disconnect from DSLAM
	 * 
	 * @return DslamCommunication
	 * 
	 * @todo
	 * 		get rid of sleep() - respectively waitUntilConnectionClosed ?
	 */
	public function logout()
	{
		if (null !== $this->stream) {
			$this->stream->write("logout\r\n");

			usleep(300000);

			$this->stream->disconnect();
			$this->stream = null;
		}

		return $this;
	}
}

?>