<?php
/**
 * 
 */

namespace DslamCommands\Lib;

require __DIR__.'/../Exception/DslamCommandsNotFoundException.php';
require __DIR__.'/../Exception/DslamCommandNotInstanceOfException.php';

use \DslamCommands\Exception\DslamCommandsNotFoundException;
use \DslamCommands\Exception\DslamCommandNotInstanceOfException;

/**
 * Finds the correct \DslamCommands\Commands\xxx\xxx\Commands
 */
class DslamCommandManager
{
    /**
     * @param string $type
     * @param string $kvzAddition - optional
     * 
     * @throws \InvalidArgumentException
     * 
     * @return \DslamCommands\Commands\xxx\xxx\Commands
     */
    public function getDslam($type, $kvzAddition = null)
    {
        if (is_string($type)) {
            $kvzAddition = is_string($kvzAddition) ? strtolower($kvzAddition) : null;

            switch (strtolower($type)) {
                case 'alu':
                    switch ($kvzAddition) {
                        case 'ethernet':
                            return $this->getCommands('Alu', 'Ethernet');

                        case 'gpon':
                            return $this->getCommands('Alu', 'Gpon');

                        case 'neltb':
                            return $this->getCommands('Alu', 'Neltb');

                        default:
                            return $this->getCommands('Alu', 'DefaultType');
                    }

                    throw new \InvalidArgumentException('Invalid kvzAddition for this type.');

                case 'iskratel':
                    switch ($kvzAddition) {
                        case 'gpon':
                            return $this->getCommands('Iskratel', 'Gpon');

                        case 'pono':
                            return $this->getCommands('Iskratel', 'Pono');
                    }

                    throw new \InvalidArgumentException('Invalid kvzAddition for this type.');
            }
        }

        throw new \InvalidArgumentException('Invalid type.');
    }

    /**
     * Autoload Class
     * 
     * @param string $dslamType
     * @param string $kvz
     * 
     * @throws \DslamCommands\Exception\DslamCommandsNotFoundException
     * 
     * @return \DslamCommands\Commands\xxx\xxx\Commands
     */
    public function getCommands($dslamType, $kvz)
    {
        $className = '\\DslamCommands\\Commands\\'.$dslamType.'\\'.$kvz.'\\Commands';
        
        if (class_exists($className)) {
            return new $className;
        }

        $fileName = __DIR__.'/../DslamType/'.$dslamType.'/'.$kvz.'/Commands.php';

        if (file_exists($fileName)) {
            include $fileName;

            if (class_exists($className)) {
                $instance = new $className;

                if (!($instance instanceOf \DslamCommands\Commands\CommandInterface)) {
                    throw new DslamCommandNotInstanceOfException(
                        'Class ('.$className.') does not implement \\DslamCommands\\Commands\\CommandInterface'
                    );
                }

                if (!($instance instanceOf \DslamCommands\Lib\DslamCommunication)) {
                    throw new DslamCommandNotInstanceOfException(
                        'Class ('.$className.') not an instance of \\DslamCommands\\Lib\\DslamCommunication'
                    );
                }
                
                return $instance;
            }

            throw new DslamCommandsNotFoundException('Expected class ('.$className.') not found in '.$file);
        }

        throw new DslamCommandsNotFoundException('File '.$fileName.' not found.');
    }
}
?>