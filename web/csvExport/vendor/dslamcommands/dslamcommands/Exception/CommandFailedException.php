<?php
/**
 * 
 */

namespace DslamCommands\Exception;

/**
 * 
 */
class CommandFailedException extends \Exception
{
    /**
     * @param string  $message
     * @param integer $number - default null
     */
    public function __construct($message, $number = null)
    {
        parent::__construct($message, $number);
    }
}
?>