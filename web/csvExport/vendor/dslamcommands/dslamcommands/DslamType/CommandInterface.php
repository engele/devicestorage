<?php
/**
 * 
 */

namespace DslamCommands\Commands;

/**
 * 
 */
interface CommandInterface
{
    /**
     * DSLAM Kunden Status
     * 
     * @param string  $lineident
     * @param string  $detail
     * @param boolean $DSLAM_Status
     * @param string  $Transfermode
     * 
     * @throws \DslamCommands\Exception\CommandFailedException
     * 
     * @return string
     */
    public function getCustomerStatus($lineIdentifier, $detail, $DSLAM_Status, $Transfermode);
}

?>