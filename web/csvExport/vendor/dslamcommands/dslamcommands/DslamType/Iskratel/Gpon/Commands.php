<?php
/**
 * 
 */

namespace DslamCommands\Commands\Iskratel\Gpon;

require_once __DIR__.'/../../../Lib/DslamCommunication.php';
require_once __DIR__.'/../../../Exception/CommandFailedException.php';
require_once __DIR__.'/../../CommandInterface.php';
require_once __DIR__.'/../../../../../webmozart/assert/src/Assert.php';

use \DslamCommands\Lib\DslamCommunication;
use \DslamCommands\Exception\CommandFailedException;
use \DslamCommands\Commands\CommandInterface;
use \Webmozart\Assert\Assert;

/**
 * @see https://github.com/webmozart/assert
 * 
 * @todo
 *      // -case 'DSLAM Kunden Status': :: getCustomerStatus()
 *      // -case 'DSLAM Kunden Loeschen': :: deleteCustomer()
 *      // -case 'DSLAM Kunden Loeschen1': :: deleteCustomer1()
 *      // -case 'DSLAM Kunden Einrichten': :: setupCustomer()
 *      -case 'Anzeigen': :: show()
 *      -case 'ONTFirmware':
 *      -case 'ONTSynch':
 *      -case 'ONTReboot':
 *      -case 'Bridge':
 *      -case 'ONTFWneu':
 */
class Commands extends DslamCommunication implements CommandInterface
{
    /**
     * DSLAM Kunden Status
     * 
     * @param string $PON
     * @param string $sublineident
     * 
     * @throws \DslamCommands\Exception\CommandFailedException
     * 
     * @return string
     * 
     * @todo
     *      Is it really supposed to output # ?
     *      Maybe it is supposed to output > ?
     */
    public function getCustomerStatus($PON, $sublineident)
    {
        try {
            Assert::stringNotEmpty($PON);
            Assert::stringNotEmpty($sublineident);

            return $this->stream
                ->write("sysadmin\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write("sysadmin\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write("onu show 1/".$PON."/".$sublineident."\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write("onu status 1/".$PON."/".$sublineident."\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write("bridge show 1-1-".$PON."-7".$sublineident."\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write("exit\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->getLastReadData()
            ;
        } catch (\InvalidArgumentException $e) {
            throw new CommandFailedException('Invalid data provided ('.$e->getMessage().')');
        } catch (\Exception $e) {
            throw new CommandFailedException($e->getMessage());
        }
    }

    /**
     * DSLAM Kunden Loeschen1
     * 
     * @param string $PON
     * @param string $sublineident
     * 
     * @throws \DslamCommands\Exception\CommandFailedException
     * 
     * @return string
     * 
     * @todo
     *      Is it really supposed to output # ?
     *      Maybe it is supposed to output > ?
     */
    public function deleteCustomer1($PON, $sublineident)
    {
        try {
            Assert::stringNotEmpty($PON);
            Assert::stringNotEmpty($sublineident);

            return $this->stream
                ->write("sysadmin\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write("sysadmin\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write("bridge delete 1-1-".$PON."-".$sublineident."/gpononu all\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write("onu clear 1/".$PON."/".$sublineident."\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write("onu delete 1/".$PON."/".$sublineident."\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write("yes\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write("no\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write("yes\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write("exit\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->getLastReadData()
            ;
        } catch (\InvalidArgumentException $e) {
            throw new CommandFailedException('Invalid data provided ('.$e->getMessage().')');
        } catch (\Exception $e) {
            throw new CommandFailedException($e->getMessage());
        }
    }

    /**
     * DSLAM Kunden Einrichten
     * 
     * @param string $PON
     * @param string $sublineident
     * 
     * @throws \DslamCommands\Exception\CommandFailedException
     * 
     * @return string
     * 
     * @todo
     *      Is it really supposed to output # ?
     *      Maybe it is supposed to output > ?
     */
    public function setupCustomer($PON, $sublineident, $Leitung, $FSAN, $Vlan, $purtelUsername, $purtelPasswort, $Service, $clientid, $passwd)
    {
        try {
            Assert::stringNotEmpty($PON);
            Assert::stringNotEmpty($sublineident);
            Assert::stringNotEmpty($Leitung);
            Assert::stringNotEmpty($FSAN);
            Assert::stringNotEmpty($Vlan);
            Assert::stringNotEmpty($purtelUsername);
            Assert::stringNotEmpty($purtelPasswort);
            Assert::stringNotEmpty($Service);
            Assert::stringNotEmpty($clientid);
            Assert::stringNotEmpty($passwd);

            $Einrichten = '';
            $Einrichten1 = '';
            $Einrichten2 = '';
            $Einrichten3 = '';

            switch ($FSAN) {
                case '2426':
                    $Einrichten = "bridge add 1-1-".$PON."-".$sublineident."/gpononu gem 7".$sublineident." gtp ".$Service
                        ." downlink-pppoe vlan ".$Vlan." epktrule ".$Service." tagged sip eth all wlan 1 rg-bpppoe\n";

                    $Einrichten1 = "cpe rg wan modify 1/".$PON."/".$sublineident."-gponport vlan ".$Vlan." pppoe-usr-id ".$clientid
                        ." pppoe-password ".$passwd."\n";

                    $Einrichten2 = "cpe-mgr add local 1-1-".$PON."-".$sublineident."/gpononu gem 5".$sublineident." gtp 1\n";

                    $Einrichten3 = "cpe voip add 1/".$PON."/".$sublineident."/1 dial-number ".$purtelUsername." username ".$purtelUsername
                        ." password ".$purtelPasswort." voip-server-profile WisoTel\n";
                    break;

                case '2301':
                case '2311':
                    $Einrichten = "bridge add 1-1-".$PON."-".$sublineident."/gpononu gem 7".$sublineident." gtp ".$Service
                        ." downlink vlan ".$Vlan." epktrule ".$Service." tagged eth 1\n";

                    $Einrichten1 = "";
                    $Einrichten2 = "";
                    $Einrichten3 = "";

                    break;
            }

            $this->stream
                ->write("sysadmin\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write("sysadmin\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write("onu set 1/".$PON."/".$sublineident." vendorid ZNTS serno fsan ".$Leitung." meprof zhone-".$FSAN."\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write($Einrichten."\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write("bridge stats enable 1-1-".$PON."-7".$sublineident."-gponport-".$Vlan."/bridge\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write($Einrichten1."\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ->write($Einrichten2."\n")
                ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
            ;

            if ($purtelUsername <> '') {
                $this->stream->write($Einrichten3."\n")
                    ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                ;
            }

            return $this->stream->write("exit\n")->getLastReadData();
        } catch (\InvalidArgumentException $e) {
            throw new CommandFailedException('Invalid data provided ('.$e->getMessage().')');
        } catch (\Exception $e) {
            throw new CommandFailedException($e->getMessage());
        }
    }
}

?>