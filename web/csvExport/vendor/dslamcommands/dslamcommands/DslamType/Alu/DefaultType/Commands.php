<?php
/**
 * 
 */

namespace DslamCommands\Commands\Alu\DefaultType;

require_once __DIR__.'/../../../Lib/DslamCommunication.php';
require_once __DIR__.'/../../../Exception/CommandFailedException.php';
require_once __DIR__.'/../../CommandInterface.php';
require_once __DIR__.'/../../../../../webmozart/assert/src/Assert.php';

use \DslamCommands\Lib\DslamCommunication;
use \DslamCommands\Exception\CommandFailedException;
use \DslamCommands\Commands\CommandInterface;
use \Webmozart\Assert\Assert;

/**
 * @todo
 *      -case 'DSLAM Kunden Einrichten': :: setupCustomer()
 *       case 'DSLAM Kunden Bandbreite': :: getCustomerBandwidth()
 *       case 'DSLAM Kunden sperren': :: lockCustomer()
 *       case 'DSLAM Kunden freischalten': :: unlockCustomer()
 *       case 'DSLAM Port Reset': :: resetPort()
 *      // -case 'DSLAM Kunden Status': :: getCustomerStatus()
 *      -case 'Info DSLAM':
 *       case '':
 *       case 'keine':
 *       case 'Experte':
 */
class Commands extends DslamCommunication implements CommandInterface
{
    /**
     * {@inheritdoc}
     */
    public function getCustomerStatus($lineIdentifier, $detail, $DSLAM_Status, $Transfermode)
    {
        try {
            Assert::stringNotEmpty($lineIdentifier);
            Assert::stringNotEmpty($detail);
            Assert::boolean($DSLAM_Status);
            Assert::stringNotEmpty($Transfermode);

            if ($DSLAM_Status) {
                $output = $this->stream
                    ->write("show xdsl operational-data line ".$lineIdentifier." ".$detail."\r\n")
                    ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                    ->getLastReadData()
                ;
            } else {
                $atm = "";
                
                if ($Transfermode == "atm"){
                    $atm = ":1:32";
                }

                $output = $this->stream
                    ->write("info configure xdsl line ".$lineIdentifier." flat\r\n")
                    ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                    ->write("info configure bridge port ".$lineIdentifier.$atm."\r\n")
                    ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                    ->write("show vlan bridge-port-fdb ".$lineIdentifier.$atm."\r\n")
                    ->waitUntilStringIsFound('/#\s*$/', null, 'Did not receive command-prompt', true)
                    ->getLastReadData()
                ;
            }

            if (false !== strpos($output, 'invalid token')) {
                throw new CommandFailedException($output);
            }

            return $output;
        } catch (\InvalidArgumentException $e) {
            throw new CommandFailedException('Invalid data provided ('.$e->getMessage().')');
        } catch (\Exception $e) {
            throw new CommandFailedException($e->getMessage());
        }
    }
}

?>