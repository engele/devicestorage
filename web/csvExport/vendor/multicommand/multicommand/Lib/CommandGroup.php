<?php
/**
 * 
 */

namespace MultiCommand\Lib;

require_once __DIR__.'/CommandCollectionOrGroupInterface.php';

/**
 * 
 */
class CommandGroup implements CommandCollectionOrGroupInterface
{
    /**
     * @var mixed
     */
    protected $identifier;

    /**
     * @var \MultiCommand\Lib\Command
     */
    protected $beforeCommand;

    /**
     * @var \MultiCommand\Lib\Command
     */
    protected $afterCommand;

    /**
     * @var array
     */
    protected $commands = array();

    /**
     * Constructor
     * 
     * @param mixed $groupIdentifier - optional
     */
    public function __construct($groupIdentifier = null)
    {
        $this->setIdentifier($groupIdentifier);
    }

    /**
     * @param mixed $groupIdentifier - optional
     * 
     * @return \MultiCommand\Lib\Command
     */
    public static function newInstance($groupIdentifier = null)
    {
        return new static($groupIdentifier);
    }

    /**
     * @param mixed $identifier
     * 
     * @return \MultiCommand\Lib\CommandGroup
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param \MultiCommand\Lib\Command $beforeCommand
     * 
     * @return \MultiCommand\Lib\CommandGroup
     */
    public function setBeforeCommand(Command $beforeCommand)
    {
        $this->beforeCommand = $beforeCommand;

        return $this;
    }

    /**
     * @return \MultiCommand\Lib\Command
     */
    public function getBeforeCommand()
    {
        return $this->beforeCommand;
    }

    /**
     * @param \MultiCommand\Lib\Command $afterCommand
     * 
     * @return \MultiCommand\Lib\CommandGroup
     */
    public function setAfterCommand(Command $afterCommand)
    {
        $this->afterCommand = $afterCommand;

        return $this;
    }

    /**
     * @return \MultiCommand\Lib\Command
     */
    public function getAfterCommand()
    {
        return $this->afterCommand;
    }

    /**
     * @param integer                   $commandId
     * @param \MultiCommand\Lib\Command $command
     * 
     * @return \MultiCommand\Lib\CommandGroup
     */
    public function addCommand($commandId, Command $command)
    {
        $this->commands[$commandId] = $command;

        return $this;
    }

    /**
     * Use this to overwrite commands
     * 
     * @param array $commands
     * 
     * @return \MultiCommand\Lib\CommandGroup
     */
    public function setCommands(array $commands)
    {
        $this->commands = $commands;

        return $this;
    }

    /**
     * @param integer $commandId
     * 
     * @return \MultiCommand\Lib\CommandGroup
     */
    public function removeCommand($commandId)
    {
        $this->commands[$commandId] = null;

        unset($this->commands[$commandId]);

        return $this;
    }

    /**
     * @param integer $commandId
     * 
     * @throws \InvalidArgumentException
     * 
     * @return \MultiCommand\Lib\Command
     */
    public function getCommand($commandId)
    {
        if (isset($this->commands[$commandId])) {
            return $this->commands[$commandId];
        }

        throw new \InvalidArgumentException('No command with this id belongs to this group');
    }

    /**
     * @return array
     */
    public function getCommands()
    {
        return $this->commands;
    }
}

?>