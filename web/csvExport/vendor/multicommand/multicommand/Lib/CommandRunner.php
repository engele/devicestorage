<?php
/**
 * 
 */

namespace MultiCommand\Lib;

require_once __DIR__.'/BeforeCommand.php';
require_once __DIR__.'/AfterCommand.php';
require_once __DIR__.'/../Exception/StopCommandRunnerException.php';

use \MultiCommand\Exception\StopCommandRunnerException;

/**
 * 
 */
class CommandRunner
{
    /**
     * @var boolean
     */
    protected $errorHappened = false;

    /**
     * @return \MultiCommand\Lib\CommandRunner
     */
    public static function newInstance()
    {
        return new static();
    }

    /**
     * @param \MultiCommand\Lib\CommandGroup $commandGroup
     * 
     * @return \MultiCommand\Lib\Command
     */
    public function runBeforeCommand(CommandGroup $commandGroup)
    {
        $beforeCommand = $commandGroup->getBeforeCommand();

        if (null !== $beforeCommand) {
            
            $beforeCommand = BeforeCommand::newInstance()->setCommand($beforeCommand);

            $var = &$beforeCommand->getCommandOutput();
            // For php 7
            //$var = $beforeCommand->getCommand()->getCommand()($beforeCommand->getCommand()->getDataArray());
            // For php 5.4
            $func = $beforeCommand->getCommand()->getCommand();
            $var = $func($beforeCommand->getCommand()->getDataArray());
        }

        return $beforeCommand;
    }

    /**
     * @param \MultiCommand\Lib\CommandGroup $commandGroup
     * @param \MultiCommand\Lib\Command      $beforeCommand - optional
     * 
     * @return \MultiCommand\Lib\Command
     */
    public function runAfterCommand(CommandGroup $commandGroup, BeforeCommand $beforeCommand = null)
    {
        $afterCommand = $commandGroup->getAfterCommand();

        if (null !== $afterCommand) {
            $afterCommand = AfterCommand::newInstance()->setCommand($afterCommand);

            $var = &$afterCommand->getCommandOutput();
            // For php 7
            //$var = $afterCommand->getCommand()->getCommand()($beforeCommand, $afterCommand->getCommand()->getDataArray());
            // For php 5.4
            $func = $afterCommand->getCommand()->getCommand();
            $var = $func(
                $beforeCommand,
                $afterCommand->getCommand()->getDataArray()
            );
        }

        return $afterCommand;
    }

    /**
     * @param \MultiCommand\Lib\CommandCollectionOrGroupInterface  $commandCollectionOrGroup
     * @param \MultiCommand\Lib\BeforeCommand                      $beforeCommand
     * @param \MultiCommand\Lib\AfterCommand                       $afterCommand - optional
     * 
     * @return void
     */
    public function runCommands(CommandCollectionOrGroupInterface $commandCollectionOrGroup, BeforeCommand $beforeCommand, AfterCommand $afterCommand = null)
    {
        foreach ($commandCollectionOrGroup->getCommands() as $command) {
            $arguments = array();

            $reflectionFunction = new \ReflectionFunction($command->getCommand());

            foreach ($reflectionFunction->getParameters() as $parameter) {
                $class = $parameter->getClass();
                
                if (null !== $class) {
                    switch ($class->getName()) {
                        case 'MultiCommand\\Lib\\BeforeCommand':
                            $arguments[] = $beforeCommand;
                            continue 2;

                        case 'MultiCommand\\Lib\\AfterCommand':
                            $arguments[] = $afterCommand;
                            continue 2;
                    }

                    throw new \Exception(
                        'This parameter ('.$class->getName().') is not available for command'
                    );
                }
            }

            $arguments[] = $command->getDataArray();

            $var = &$command->getOutputVariable();
            $var = call_user_func_array($command->getCommand(), $arguments);
        }
    }

    /**
     * @param \MultiCommand\Lib\CommandCollection $commandCollection
     * 
     * @return \MultiCommand\Lib\CommandRunner
     */
    public function runGrouped(CommandCollection $commandCollection)
    {
        foreach ($commandCollection->getGroups() as $group) {
            try {
                $afterCommand = null;

                $beforeCommand = $this->runBeforeCommand($group);

                $this->runCommands($group, $beforeCommand, $afterCommand);

                $afterCommand = $this->runAfterCommand($group, $beforeCommand);
            } catch (StopCommandRunnerException $e) {
                $this->errorHappened = true;
                
                array_map(function ($command) use ($e) {
                    $var = &$command->getOutputVariable();

                    if (null === $var) {
                        $var = $e->getMessage();
                    }
                }, $group->getCommands());
            }
        }

        return $this;
    }

    /**
     * @return boolean
     */
    public function hasErrorHappened()
    {
        return $this->errorHappened;
    }
}

?>