<?php
/**
 * 
 */

namespace MultiCommand\Lib;

/**
 * 
 */
class BeforeCommand
{
    /**
     * @var \MultiCommand\Lib\Command
     */
    protected $command;

    /**
     * @var mixed
     */
    protected $outputVariable;

    /**
     * @return \MultiCommand\Lib\BeforeCommand
     */
    public static function newInstance()
    {
        return new static();
    }

    /**
     * @param \MultiCommand\Lib\Command $command
     * 
     * @return \MultiCommand\Lib\BeforCommand
     */
    public function setCommand(Command $command)
    {
        $command->setOutputVaiable($this->outputVariable);

        $this->command = $command;

        return $this;
    }

    /**
     * @return \MultiCommand\Lib\Command
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * Same as $this->getCommand()->getOutputVariable()
     * 
     * @return mixed
     */
    public function &getCommandOutput()
    {
        return $this->outputVariable;
    }

    /**
     * Alias for getCommandOutput()
     * 
     * @return mixed
     */
    public function &get()
    {
        return $this->getCommandOutput();
    }
}

?>