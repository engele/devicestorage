<?php
/**
 * 
 */

namespace MultiCommand\Lib;

/**
 * 
 */
class Command
{
    /**
     * @var mixed
     */
    protected $groupIdentifier;

    /**
     * @var mixed
     */
    protected $command;

    /**
     * @var array
     */
    protected $memberOfCollections = array();

    /**
     * @var mixed
     */
    protected $outputVariable;

    /**
     * @var array
     */
    protected $dataArray;

    /**
     * @return \MultiCommand\Lib\Command
     */
    public static function newInstance()
    {
        return new static();
    }

    /**
     * @param mixed $groupIdentifier
     * 
     * @return \MultiCommand\Lib\Command
     */
    public function setGroupIdentifier($groupIdentifier)
    {
        $this->notifyCollectionsOfGroupChange($this->groupIdentifier, $groupIdentifier);

        $this->groupIdentifier = $groupIdentifier;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGroupIdentifier()
    {
        return $this->groupIdentifier;
    }

    /**
     * @param \Closure $command
     * 
     * @return \MultiCommand\Lib\Command
     */
    public function setCommand(\Closure $command)
    {
        $this->command = $command;

        return $this;
    }

    /**
     * @return \Closure
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * @param mixed $variable
     * 
     * @return \MultiCommand\Lib\Command
     */
    public function setOutputVaiable(&$variable)
    {
        $this->outputVariable = &$variable;

        return $this;
    }

    /**
     * @return mixed
     */
    public function &getOutputVariable()
    {
        return $this->outputVariable;
    }

    /**
     * @param \MultiCommand\Lib\CommandCollection $commandCollection
     * @param integer                             $commandId
     * 
     * @return \MultiCommand\Lib\Command
     */
    public function makeMemberOfCollection(CommandCollection $commandCollection, $commandId)
    {
        $this->memberOfCollections[] = array('collection' => $commandCollection, 'id' => $commandId);

        return $this;
    }

    /**
     * @param mixed $oldGroup
     * @param mixed $newGroup
     * 
     * @return \MultiCommand\Lib\Command
     */
    public function notifyCollectionsOfGroupChange($oldGroup, $newGroup)
    {
        foreach ($this->memberOfCollections as $array) {
            $array['collection']->notifyOfGroupChange($array['id'], $oldGroup, $newGroup);
        }

        return $this;
    }

    /**
     * @param array $dataArray
     * 
     * @return \MultiCommand\Lib\Command
     */
    public function setDataArray(array $dataArray)
    {
        $this->dataArray = $dataArray;

        return $this;
    }

    /**
     * @return array
     */
    public function getDataArray()
    {
        return $this->dataArray;
    }
}

?>