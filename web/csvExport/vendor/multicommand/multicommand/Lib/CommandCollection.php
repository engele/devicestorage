<?php
/**
 * 
 */

namespace MultiCommand\Lib;

require_once __DIR__.'/CommandCollectionOrGroupInterface.php';
require_once __DIR__.'/CommandGroup.php';

/**
 * 
 */
class CommandCollection implements CommandCollectionOrGroupInterface
{
    /**
     * @var array
     */
    protected $commands;

    /**
     * @var array
     */
    protected $groups;

    /**
     * @var integer
     */
    protected $lastCommandId = 0;

    /**
     * @return \MultiCommand\Lib\Command
     */
    public static function newInstance()
    {
        return new static();
    }

    /**
     * Use this to create or overwrite a group
     * 
     * @param \MultiCommand\Lib\CommandGroup $commandGroup
     * 
     * @return \MultiCommand\Lib\CommandCollection
     */
    public function setGroup(CommandGroup $commandGroup)
    {
        $groupIdentifier = $commandGroup->getIdentifier();

        if (!isset($this->groups[$groupIdentifier])) {
            $this->groups[$groupIdentifier] = $commandGroup;
        } else {
            // overwrite
            $commandGroup->setCommands($this->groups[$groupIdentifier]->getCommands());
            
            $this->groups[$groupIdentifier] = $commandGroup;
        }

        return $this;
    }

    /**
     * @param mixed $groupIdentifier
     * 
     * @return \MultiCommand\Lib\CommandCollection
     */
    protected function createGroup($groupIdentifier)
    {
        if (!isset($this->groups[$groupIdentifier])) {
            $this->groups[$groupIdentifier] = CommandGroup::newInstance($groupIdentifier);
        }
    }

    /**
     * Add command to collection
     * 
     * Returns the numeric index of the element in the collection
     * 
     * @param \MultiCommand\Lib\Command $command
     * 
     * @return integer
     */
    public function addCommand(Command $command)
    {
        $commandId = $this->lastCommandId++;

        $this->commands[$commandId] = $command;

        $groupIdentifier = $command->getGroupIdentifier();

        if (null != $groupIdentifier) {
            $this->createGroup($groupIdentifier);

            $this->groups[$groupIdentifier]->addCommand($commandId, $command);
        }

        $command->makeMemberOfCollection($this, $commandId);

        return $commandId;
    }

    /**
     * @param integer $commandId
     * 
     * @throws \InvalidArgumentException
     * 
     * @return \MultiCommand\Lib\Command
     */
    public function getCommandById($commandId)
    {
        if (isset($this->commands[$commandId])) {
            return $this->commands[$commandId];
        }

        throw new \InvalidArgumentException('No command with this id found.');
    }

    /**
     * Change group-connection of command
     * 
     * @param integer $commandId
     * @param mixed   $oldGroup
     * @param mixed   $newGroup - optional
     * 
     * @return \MultiCommand\Lib\CommandCollection
     */
    public function notifyOfGroupChange($commandId, $oldGroup, $newGroup = null)
    {
        if (null !== $newGroup) { // change commands group
            $this->groups[$newGroup]->addCommand($commandId,
                $this->groups[$oldGroup]->getCommand($commandId)
            );
        }

        $this->groups[$oldGroup]->removeCommand($commandId);

        $commands = $this->groups[$oldGroup]->getCommands();

        if (empty($commands)) {
            $this->groups[$oldGroup] = null;
            unset($this->groups[$oldGroup]);
        }

        return $this;
    }

    /**
     * @param mixed $groupIdentifier
     * 
     * @return \MultiCommand\Lib\CommandCollection
     */
    public function removeGroup($groupIdentifier)
    {
        if (isset($this->groups[$groupIdentifier])) {
            $this->groups[$groupIdentifier] = null;
            unset($this->groups[$groupIdentifier]);
        }

        return $this;
    }

    /**
     * @param integer $commandId
     * 
     * @return \MultiCommand\Lib\CommandCollection
     */
    public function removeCommand($commandId)
    {
        if (isset($this->commands[$commandId])) {
            foreach ($this->groups as $groupIdentifier => $group) {
                if (isset($group[$commandId])) {
                    $this->notifyOfGroupChange($commandId,
                        $groupIdentifier
                    );
                }
            }

            $this->commands[$commandId] = null;
            unset($this->commands[$commandId]);
        }

        return $this;
    }

    /**
     * @param mixed $groupIdentifier
     * 
     * @throws \InvalidArgumentException
     * 
     * @return \MultiCommand\Lib\CommandGroup
     */
    public function getGroup($groupIdentifier)
    {
        if (isset($this->groups[$groupIdentifier])) {
            return $this->groups[$groupIdentifier];
        }
        
        throw new \InvalidArgumentException('No group found by this identifier');
    }

    /**
     * @return array
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param mixed $groupIdentifier
     * 
     * @return boolean
     */
    public function hasGroup($groupIdentifier)
    {
        return isset($this->groups[$groupIdentifier]);
    }

    /**
     * @param integer $commandId
     * 
     * @throws \InvalidArgumentException
     * 
     * @return \MultiCommand\Lib\Command
     */
    public function getCommand($commandId)
    {
        if (isset($this->command[$commandId])) {
            return $this->command[$commandId];
        }
        
        throw new \InvalidArgumentException('No command found by this id');
    }

    /**
     * @return array
     */
    public function getCommands()
    {
        return $this->commands;
    }
}

?>