$(document).ready(function(){
    var onClickCheckProfileAndRelocate = {
        'button-edit': '?menu=csvExport&func=edit&profile-id={profileId}',
        'button-export-csv': '?menu=csvExport&func=export-csv&profile-id={profileId}',
        'button-export-ping-csv': '?menu=csvExport&func=export-ping-csv&profile-id={profileId}',
        'button-export-ping-only-failed-csv': '?menu=csvExport&func=export-ping-only-failed-csv&profile-id={profileId}',
        'button-export-dslam-status-csv': '?menu=csvExport&func=export-dslam-status-csv&profile-id={profileId}',
        'button-export-dslam-config-batch': '?menu=csvExport&func=export-dslam-config-batch&profile-id={profileId}',
        'button-export-pppee-config-batch': '?menu=csvExport&func=export-pppee-config-batch&profile-id={profileId}',
        'button-export-iban-verification': '?menu=csvExport&func=export-iban-verification&profile-id={profileId}',
        'button-export-microtik-ppp-secrets': '?menu=csvExport&func=export-microtik-ppp-secrets&profile-id={profileId}',
        'button-export-sap-debitors-creation': '?menu=csvExport&func=export-sap-debitors-creation&profile-id={profileId}',
    };

    for (var e in onClickCheckProfileAndRelocate) {
        $('#'+e).on('click', function () {
            if ("" !== $('#export-profile').val()) {
                var location = onClickCheckProfileAndRelocate[$(this).attr('id')];

                window.location = location.replace('{profileId}', $('#export-profile').val());
            } else {
                window.alert('Bitte w\xE4hlen Sie zuerst ein Profil aus');
            }

            return false;
        });
    }


    $('#button-new').on('click', function() {
        window.location = '?menu=csvExport&func=create-new';
    });

    $('#button-delete').on('click', function() {
        if ("" !== $('#export-profile').val()) {
            if (confirm('Wollen Sie dieses Profil wirklich löschen?')) {
                window.location = '?menu=csvExport&func=delete&profile-id='+$('#export-profile').val();
            }
        }

        return false;
    });

    $('#button-clone').on('click', function() {
        if ("" !== $('#export-profile').val()) {
            var profileName = window.prompt('Bitte geben Sie einen Namen f\xFCr das neue Profil ein.');

            if (null !== profileName) {
                window.location = '?menu=csvExport&func=clone&profile-id='+$('#export-profile').val()+'&profile-name='+encodeURI(profileName);
            }
        }

        return false;
    });

    $('#ask-for-replacements-form').on('submit', function(event) {
        var somethingEmpty = false;

        $(this).find('input').each(function () {
            if ("" === $(this).val()) {
                somethingEmpty = true;
            }
        });

        if (somethingEmpty) {
            event.preventDefault();
            window.alert('Bitte f\xFCllen Sie alle Felder aus.');
            return false;
        }

        return true;
    });

    if ($('.highlight-profile-entry').length > 0) {
        $('#list-active').css('display', 'block');
        var table = $('#list-active').find('table');
        var grouped = {};

        $('td.highlight-profile-entry').each(function () {
            var tr = $(this).parent('tr');

            var headline = $(this).closest('div').prev().text();

            if (!grouped[headline]) {
                grouped[headline] = [];
            }

            grouped[headline].push(tr);
        });
        
        for (group in grouped) {
            table.append('<tr><td align="left" colspan="4"><strong>'+group+'</strong></td></tr>');

            for (item in grouped[group]) {
                table.append(grouped[group][item]);
            }
        }
    }
});