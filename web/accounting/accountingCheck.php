<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_ACCOUNTANCY');

// session_start();

//$StartPath = $_SESSION['kv_config']['system']['startPath'];
// $SelPath   = $_SESSION['kv_config']['system']['selPath'];
require_once __DIR__.'/../config.inc';
//require_once $StartPath.'/_conf/function.inc';
//require_once $StartPath.'/_conf/database.inc';
require_once __DIR__.'/../_conf/function.inc';
require_once __DIR__.'/../_conf/database.inc';
require_once __DIR__.'/_conf/database.inc';

function object_to_array($obj) {
    if(is_object($obj)) $obj = (array) $obj;
    if(is_array($obj)) {
        $new = array();
        foreach($obj as $key => $val) {
            $new[$key] = object_to_array($val);
        }
    }
    else $new = $obj;
    return $new;       
}
/*
$accountingTime = strtotime($_GET['accountingDate']);
$day            = date('j', $accountingTime);
$accountingTime = strtotime("-$day days", $accountingTime); 
$toDate         = date ('Ymd', $accountingTime);
$day            = date('t', $accountingTime) - 1;
$accountingTime = strtotime("-$day days", $accountingTime);
$fromDate       = date ('Ymd', $accountingTime);
$periode        = date ('Ym', $accountingTime);
*/
$dateFormatter = new \IntlDateFormatter(
    null, // should be defined in php.ini
    \IntlDateFormatter::FULL,
    \IntlDateFormatter::FULL,
    null, // should be defined in php.ini
    \IntlDateFormatter::GREGORIAN,
    'MMMM yyyy'
);

$period = \DateTime::createFromFormat('U', $dateFormatter->parse($_GET['accountingDate']));
$period->add(new \DateInterval('P1D')); // add plus 1 day to avoid problems with daylight saving

$toDate = $period->format('Ymt');
$fromDate = $period->format('Ym01');
$periode = $period->format('Ym');

$sepaFile       = isset($_GET['cdd']) ? urldecode($_GET['cdd']) : '';
$billFile       = isset($_GET['csv']) ? urldecode($_GET['csv']) : '';
$sapFile        = isset($_GET['sap']) ? urldecode($_GET['sap']) : '';
$billCheck      = array();
$purtelBillnr   = \AppBundle\Util\Configuration::get('purtelBillnr');
$clientIdLength = \AppBundle\Util\Configuration::get('customerClientidLength');
$clientId       = '';

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=check_'.$periode.'.csv');  

if (file_exists($billFile)  and (file_exists($sepaFile) or file_exists($sapFile))) {
    $dbCustomerId   = $db->prepare($sql['customer']);
    $dbCustomerId->bind_param('s', $clientId);

    ini_set("auto_detect_line_endings", true);
    $csv_read = fopen($billFile, 'r');
    $temp = fgetcsv($csv_read, 0, ';', '"' );

    while ($temp = fgetcsv($csv_read, 1000, ";")) {
        $clientId = $temp[4];
        $dbCustomerId->execute();
        $dbCustomerIdResult = $dbCustomerId->get_result();
        $dbCustomerIdRow = $dbCustomerIdResult->fetch_assoc();
        $custId = $dbCustomerIdRow['id'];
        $dbCustomerIdResult->free_result();
        
        if ($custId) {
            $billCheck[$temp[8]]['id'] = '=HYPERLINK("'.$StartURL.'/index.php?menu=customer&func=vertrag&area=SwitchTechData&id='.$custId.'";"'.$custId.'")'; 
        } else {
            $billCheck[$temp[8]]['id'] = $custId;
        }
        $billCheck[$temp[8]]['Rechnungsnr_bill'] = $temp[8];
        $billCheck[$temp[8]]['Rechnungsnr_sepa'] = '';
        $billCheck[$temp[8]]['Rechnungstext_sepa'] = '';
        $billCheck[$temp[8]]['Kundennrnr_bill'] = $temp[4];
        $billCheck[$temp[8]]['Kundennrnr_sepa'] = '';
        $billCheck[$temp[8]]['Name_bill'] = $temp[5];
        $billCheck[$temp[8]]['Name_sepa'] = '';
        $billCheck[$temp[8]]['Betrag_bill'] = $temp[10];
        $billCheck[$temp[8]]['Betrag_sepa'] = '';
        $billCheck[$temp[8]]['Zahlart'] = $temp[13];
        $billCheck[$temp[8]]['IBAN'] = '';
        $billCheck[$temp[8]]['BIC'] = '';
    }
    fclose($csv_read);
    if (file_exists($sepaFile)) {
        $temp = simplexml_load_file($sepaFile);
        $sepa = object_to_array($temp);
        unset ($temp);
        foreach ($sepa['CstmrDrctDbtInitn']['PmtInf'] as $billList) {
            if ($billList['NbOfTxs'] <= 1) {
                $bill = $billList['DrctDbtTxInf'];
                $rechnr = substr($bill['PmtId']['EndToEndId'], $purtelBillnr['preChars'], $purtelBillnr['length']);
                $billCheck[$rechnr]['Rechnungsnr_sepa'] = $rechnr;
                $billCheck[$rechnr]['Rechnungstext_sepa'] = $bill['PmtId']['EndToEndId'];
                $billCheck[$rechnr]['Kundennrnr_sepa'] = $bill['DrctDbtTx']['MndtRltdInf']['MndtId'];
                $billCheck[$rechnr]['Name_sepa'] = $bill['Dbtr']['Nm'];
                $billCheck[$rechnr]['Betrag_sepa'] = str_replace('.', ',', $bill['InstdAmt']);
                $billCheck[$rechnr]['IBAN'] = $bill['DbtrAcct']['Id']['IBAN'];
                $billCheck[$rechnr]['BIC'] = $bill['DbtrAgt']['FinInstnId']['BIC'];
                if (!key_exists('Rechnungsnr_bill', $billCheck[$rechnr])){$billCheck[$rechnr]['Rechnungsnr_bill'] = '';}
                if (!key_exists('Kundennrnr_bill', $billCheck[$rechnr])){$billCheck[$rechnr]['Kundennrnr_bill'] = '';}
                if (!key_exists('Name_bill', $billCheck[$rechnr])){$billCheck[$rechnr]['Name_bill'] = '';}
                if (!key_exists('Betrag_bill', $billCheck[$rechnr])){$billCheck[$rechnr]['Betrag_bill'] = '';}
                if (!key_exists('Zahlart', $billCheck[$rechnr])){$billCheck[$rechnr]['Zahlart'] = '';}
                if (!key_exists('id', $billCheck[$rechnr])) {
                    $clientId = substr($bill['DrctDbtTx']['MndtRltdInf']['MndtId'], 0, $clientIdLength);
                    $dbCustomerId->execute();
                    $dbCustomerIdResult = $dbCustomerId->get_result();
                    $dbCustomerIdRow = $dbCustomerIdResult->fetch_assoc();
                    $custId = $dbCustomerIdRow['id'];
                    $dbCustomerIdResult->free_result();
                    if ($custId) {
                        $billCheck[$rechnr]['id'] = '=HYPERLINK("'.$StartURL.'/index.php?menu=customer&func=vertrag&area=SwitchTechData&id='.$custId.'";"'.$custId.'")'; 
                    } else {
                        $billCheck[$rechnr]['id'] = $custId;
                    }
                }
            } else {
                foreach ($billList['DrctDbtTxInf'] as $bill) {
                    $rechnr = substr($bill['PmtId']['EndToEndId'], $purtelBillnr['preChars'], $purtelBillnr['length']);
                    $billCheck[$rechnr]['Rechnungsnr_sepa'] = $rechnr;
                    $billCheck[$rechnr]['Rechnungstext_sepa'] = $bill['PmtId']['EndToEndId'];
                    $billCheck[$rechnr]['Kundennrnr_sepa'] = $bill['DrctDbtTx']['MndtRltdInf']['MndtId'];
                    $billCheck[$rechnr]['Name_sepa'] = $bill['Dbtr']['Nm'];
                    $billCheck[$rechnr]['Betrag_sepa'] = str_replace('.', ',', $bill['InstdAmt']);
                    $billCheck[$rechnr]['IBAN'] = $bill['DbtrAcct']['Id']['IBAN'];
                    $billCheck[$rechnr]['BIC'] = $bill['DbtrAgt']['FinInstnId']['BIC'];
                    if (!key_exists('Rechnungsnr_bill', $billCheck[$rechnr])){$billCheck[$rechnr]['Rechnungsnr_bill'] = '';}
                    if (!key_exists('Kundennrnr_bill', $billCheck[$rechnr])){$billCheck[$rechnr]['Kundennrnr_bill'] = '';}
                    if (!key_exists('Name_bill', $billCheck[$rechnr])){$billCheck[$rechnr]['Name_bill'] = '';}
                    if (!key_exists('Betrag_bill', $billCheck[$rechnr])){$billCheck[$rechnr]['Betrag_bill'] = '';}
                    if (!key_exists('Zahlart', $billCheck[$rechnr])){$billCheck[$rechnr]['Zahlart'] = '';}
                    if (!key_exists('id', $billCheck[$rechnr])) {
                        $clientId = substr($bill['DrctDbtTx']['MndtRltdInf']['MndtId'], 0, $clientIdLength);
                        $dbCustomerId->execute();
                        $dbCustomerIdResult = $dbCustomerId->get_result();
                        $dbCustomerIdRow = $dbCustomerIdResult->fetch_assoc();
                        $custId = $dbCustomerIdRow['id'];
                        $dbCustomerIdResult->free_result();
                        if ($custId) {
                            $billCheck[$rechnr]['id'] = '=HYPERLINK("'.$StartURL.'/index.php?menu=customer&func=vertrag&area=SwitchTechData&id='.$custId.'";"'.$custId.'")'; 
                        } else {
                            $billCheck[$rechnr]['id'] = $custId;
                        }
                    }
                }
            }
        }
    }
    if (file_exists($sapFile) and !file_exists($sepaFile)) {
        $sap = file($sapFile);
        foreach ($sap as $sap_line) {
            if (preg_match ("/^.{14}(\d{8})\s+.{27}(.\d{13})EUR\s+.{21}(.+)\s+E/", $sap_line, $matches)) {
                $rechnr = (int)$matches[1];
                $betrag = (int)$matches[2] / 100;
                $text = $matches[3];
                $billCheck[$rechnr]['Rechnungsnr_sepa'] = $rechnr;
                $billCheck[$rechnr]['Betrag_sepa'] = str_replace('.', ',', $betrag);
                $billCheck[$rechnr]['Rechnungstext_sepa'] = $text;    
                if (!key_exists('Rechnungsnr_bill', $billCheck[$rechnr])){$billCheck[$rechnr]['Rechnungsnr_bill'] = '';}
                if (!key_exists('Kundennrnr_bill', $billCheck[$rechnr])){$billCheck[$rechnr]['Kundennrnr_bill'] = '';}
                if (!key_exists('Name_bill', $billCheck[$rechnr])){$billCheck[$rechnr]['Name_bill'] = '';}
                if (!key_exists('Betrag_bill', $billCheck[$rechnr])){$billCheck[$rechnr]['Betrag_bill'] = '';}
                if (!key_exists('Zahlart', $billCheck[$rechnr])){$billCheck[$rechnr]['Zahlart'] = '';}
                if (!key_exists('id', $billCheck[$rechnr])) {$billCheck[$rechnr]['id'] = '';}
            }
        }                      
        unset ($sap);
    }

    $csv_handle = fopen('php://output', 'w');

    $headline = array(
        utf8_decode('id'),
        utf8_decode('Rechnungsnr_bill'),
        utf8_decode('Rechnr_sepa_sap'),
        utf8_decode('Rechtext_sepa_sap'),
        utf8_decode('Kundennrnr_bill'),
        utf8_decode('Kundnr_sepa_sap'),
        utf8_decode('Name_bill.'),
        utf8_decode('Name_sepa_sap'),
        utf8_decode('Betrag_bill'),
        utf8_decode('Betrag_sepa_sap'),
        utf8_decode('Zahlart'),
        utf8_decode('IBAN'),
        utf8_decode('BIC'),
    );

    fputcsv($csv_handle, $headline, ';');

    foreach ($billCheck as $temp) {
        $csv = array(
            $temp['id'],
            $temp['Rechnungsnr_bill'],
            $temp['Rechnungsnr_sepa'],
            $temp['Rechnungstext_sepa'],
            $temp['Kundennrnr_bill'],
            $temp['Kundennrnr_sepa'],
            $temp['Name_bill'],
            $temp['Name_sepa'],
            $temp['Betrag_bill'],
            $temp['Betrag_sepa'],
            $temp['Zahlart'],
            $temp['IBAN'],
            $temp['BIC'],
        );

        fputcsv($csv_handle, $csv, ';');
    }
    fclose ($csv_handle);
    $db->close();
} else {
    $csv_handle = fopen('php://output', 'w');
    fputcsv($csv_handle, ["Die notwendigen Dateien sind nicht Vorhanden"], ';');
    fclose ($csv_handle);
}
?>
