function ajaxAccountingBillAnswer(data)
{
    var response  = $.parseJSON(data);
    var csv       = response.csv;
    $('#accountingBill').css('background-color','#EAEAEA');
}

function ajaxAccountingAccountAnswer(data) 
{
    var response  = $.parseJSON(data);
    var csv       = response.csv;
    $('#accountingAccount').css('background-color','#EAEAEA');
}

function accountingFileLoad()
{
    $('#AccountingFiles').uploadifive('upload');
}


//******************************************************************************
$(document).ready(function()
{
    var StartPath       = $("#StartPath").val();
    var SelPath         = $("#SelPath").val();
    var StartURL        = $("#StartURL").val();
    $('#AccountingFiles').uploadifive ({
        'auto':           false,
        'width':          120,
        'height':         20,
        'buttonText':     'Dateiauswahl',
        'queueID':        'AccountingFileQueue',
        'fileType':       false, 
        'uploadScript':    decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+'/ajaxFiles.php',
        'formData': {
            'StartPath':   $("#StartPath").val(),
            'SelPath':     $("#SelPath").val(),
        },
        'onUploadComplete':  function (file, data)
        {
            var response = $.parseJSON(data);
            var ext = response.name.substr(response.name.lastIndexOf('.') +1);
            switch (ext) {
                case 'cdd':
                case 'CDD':
                    $("#AccountingFileQueue").append("<input type='hidden' id='cdd' value='"+response.name+"'>");
                    break;
                case 'csv':
                case 'CSV':
                    $("#AccountingFileQueue").append("<input type='hidden' id='csv' value='"+response.name+"'>");
                    break;
                case 'txt':
                case 'TXT':
                    $("#AccountingFileQueue").append("<input type='hidden' id='sap' value='"+response.name+"'>");
                    break;
            }
            
        }
    });
    $('#accountingBill').click(function(event) {
        var StartPath       = $("#StartPath").val();
        var SelPath         = $("#SelPath").val();
        var StartURL        = $("#StartURL").val();
        var accountingDate  = $('#AccountingDate').val();
        event.preventDefault();
        $(this).css('background-color','#F78C8C');
        document.location.href = decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/accounting-bill.php?accountingDate="+accountingDate;
        $(this).css('background-color','#EAEAEA');
    });

    $('#accountingAccount').click(function(event) {
        var StartPath       = $("#StartPath").val();
        var SelPath         = $("#SelPath").val();
        var StartURL        = $("#StartURL").val();
        var accountingDate  = $('#AccountingDate').val();
        event.preventDefault();
        $(this).css('background-color','#F78C8C');
        document.location.href = decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/accounting-account.php?accountingDate="+accountingDate;
        $(this).css('background-color','#EAEAEA');
    });

    $('#accountingBillSap').click(function(event) {
        var StartPath       = $("#StartPath").val();
        var SelPath         = $("#SelPath").val();
        var StartURL        = $("#StartURL").val();
        var accountingDate  = $('#AccountingDate').val();
        event.preventDefault();
        $(this).css('background-color','#F78C8C');
        document.location.href = decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/accounting-bill-sap.php?accountingDate="+accountingDate;
        $(this).css('background-color','#EAEAEA');
    });

    $('#accountingBillEndica').click(function(event) {
        var StartPath       = $("#StartPath").val();
        var SelPath         = $("#SelPath").val();
        var StartURL        = $("#StartURL").val();
        var accountingDate  = $('#AccountingDate').val();
        event.preventDefault();
        $(this).css('background-color','#F78C8C');
        document.location.href = decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/accounting-bill-endica.php?accountingDate="+accountingDate;
        $(this).css('background-color','#EAEAEA');
    });

    $('#accountingCheckSepa').click(function(event) {
        var StartPath       = $("#StartPath").val();
        var SelPath         = $("#SelPath").val();
        var StartURL        = $("#StartURL").val();
        var accountingDate  = $('#AccountingDate').val();
        event.preventDefault();
        $(this).css('background-color','#F78C8C');
        document.location.href = decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/accounting-check-sepa.php?accountingDate="+accountingDate;
        $(this).css('background-color','#EAEAEA');
    });

    $('#accountingCheck').click(function(event) {
        var StartPath       = $("#StartPath").val();
        var SelPath         = $("#SelPath").val();
        var StartURL        = $("#StartURL").val();
        var accountingDate  = $('#AccountingDate').val();
        var cdd             = $('#cdd').val();
        var csv             = $('#csv').val();
        var sap             = $('#sap').val();
        event.preventDefault();
        $(this).css('background-color','#F78C8C');
        document.location.href = decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/accounting-check.php?accountingDate="+accountingDate+"&cdd="+decodeURIComponent(cdd)+"&csv="+decodeURIComponent(csv)+"&sap="+decodeURIComponent(sap);
        $(this).css('background-color','#EAEAEA');
    });

    $('#accountingHistory').click(function(event) {
        event.preventDefault();

        var StartPath       = $("#StartPath").val();
        var SelPath         = $("#SelPath").val();
        var StartURL        = $("#StartURL").val();
        var accountingDate  = $('#AccountingDate').val();
        var filterHistoryReporting = $('select[name="filter-history-reporting"]').val();

        if (null !== filterHistoryReporting) {
            filterHistoryReporting = JSON.stringify(filterHistoryReporting);
            filterHistoryReporting = encodeURIComponent(filterHistoryReporting);
        } else {
            filterHistoryReporting = '';
        }
        
        $(this).css('background-color','#F78C8C');
        document.location.href = decodeURIComponent(StartURL)+"/"+decodeURIComponent(SelPath)+"/accounting-history.php?accountingDate="+accountingDate+"&filter-history-reporting="+filterHistoryReporting;
        $(this).css('background-color','#EAEAEA');
    });
    
});
