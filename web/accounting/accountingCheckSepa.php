<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_ACCOUNTANCY');

//session_start();
//$StartPath = $_SESSION['kv_config']['system']['startPath'];
//require_once __DIR__.'/../config.inc';
//require_once $StartPath.'/vendor/wisotel/configuration/Configuration.php';

/*
$accountingTime = strtotime($_GET['accountingDate']);
$day            = date('j', $accountingTime);
$accountingTime = strtotime("-$day days", $accountingTime); 
$toDate         = date('Ymd', $accountingTime);
$day            = date('t', $accountingTime) - 1;
$accountingTime = strtotime("-$day days", $accountingTime);
$fromDate       = date ('Ymd', $accountingTime);
$periode        = date ('Ym', $accountingTime);
*/
$dateFormatter = new \IntlDateFormatter(
    null, // should be defined in php.ini
    \IntlDateFormatter::FULL,
    \IntlDateFormatter::FULL,
    null, // should be defined in php.ini
    \IntlDateFormatter::GREGORIAN,
    'MMMM yyyy'
);

$period = \DateTime::createFromFormat('U', $dateFormatter->parse($_GET['accountingDate']));
$period->add(new \DateInterval('P1D')); // add plus 1 day to avoid problems with daylight saving

$toDate = $period->format('Ymt');
$fromDate = $period->format('Ym01');
$periode = $period->format('Ym');

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=check_sepa_'.$periode.'.csv');  

$csv_handle = fopen('php://output', 'w');

$purtel = array();
$purtelUsername = \AppBundle\Util\Configuration::get('purtelContractSuperuserUsername');
$purtelPassword = \AppBundle\Util\Configuration::get('purtelContractSuperuserPassword');
$purtelAction   = 'check_sepa';

$purtelUrl = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
    urlencode($purtelUsername),
    urlencode($purtelPassword),
    urlencode($purtelAction)
);
$curl = curl_init($purtelUrl);

curl_setopt_array($curl, array(
    CURLOPT_URL            => $purtelUrl,
    CURLOPT_HEADER         => false,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_SSL_VERIFYPEER => false,
));

$result = curl_exec($curl);
curl_close($curl);

$lines = explode("\n", $result);
array_pop($lines);
$headline_csv = array_shift ($lines);
$headline     = str_getcsv($headline_csv, ';');

while ($line = array_shift ($lines)) {
    $purt = str_getcsv($line, ';');

    foreach ($headline as $ind => $head) {
        $temp[$head] = $purt[$ind];
    }

    array_push($purtel, $temp);
}

$headline = array(
    utf8_decode('Datenort'),
    utf8_decode('Externe Kundennummer'),
    utf8_decode('Anschluss'),
    utf8_decode('Name'),
    utf8_decode('IBAN'),
    utf8_decode('BIC'),
    utf8_decode('Fehlermeldung'),
);
fputcsv($csv_handle, $headline, ';');

foreach ($purtel as $temp) {
    $csv = array(
        $temp['Datenort'],
        $temp['Externe Kundennummer'],
        $temp['Anschluss'],
        $temp['Name'],
        $temp['IBAN'],
        $temp['BIC'],
        $temp['Fehlermeldung'],
    );
    fputcsv($csv_handle, $csv, ';');
}
fclose($csv_handle);

?>
