<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_ACCOUNTANCY');

//session_start();

require_once __DIR__.'/../config.inc';
//require_once $StartPath.'/vendor/wisotel/configuration/Configuration.php';
/*
$accountingTime = strtotime($_GET['accountingDate']);
$day            = date('j', $accountingTime);
$accountingTime = strtotime("-$day days", $accountingTime); 
$toDate         = date ('Ymd', $accountingTime);
$day            = date('t', $accountingTime) - 1;
$accountingTime = strtotime("-$day days", $accountingTime);
$fromDate       = date ('Ymd', $accountingTime);
$periode        = date ('Ym', $accountingTime);
*/
$dateFormatter = new \IntlDateFormatter(
    null, // should be defined in php.ini
    \IntlDateFormatter::FULL,
    \IntlDateFormatter::FULL,
    null, // should be defined in php.ini
    \IntlDateFormatter::GREGORIAN,
    'MMMM yyyy'
);

$period = \DateTime::createFromFormat('U', $dateFormatter->parse($_GET['accountingDate']));
$period->add(new \DateInterval('P1D')); // add plus 1 day to avoid problems with daylight saving

$toDate = $period->format('Ymt');
$fromDate = $period->format('Ym01');
$periode = $period->format('Ym');
$toEndicaDate = $period->format('t.m.Y');
$fromEndicaDate = $period->format('01.m.Y');

header('Content-Type: text/plain; charset=utf-8');
header('Content-Disposition: attachment; filename=billsEndica_'.$periode.'.txt');  

$txt_handle = fopen('php://output', 'w');

$endica =  \Wisotel\Configuration\Configuration::get('endica');
$purtel = array();
$purtelUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$purtelPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
$purtelAction   = 'getrechnungen';
$purtelParameter = array (
    'von_datum' => $fromDate,
    'bis_datum' => $toDate,
    'erweitert' => 1
);    
$purtelUrl = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
    urlencode($purtelUsername),
    urlencode($purtelPassword),
    urlencode($purtelAction)
);
foreach ($purtelParameter as $key => $value) {
    $purtelUrl .= "&$key=".urlencode(utf8_decode($value));
}

$curl = curl_init($purtelUrl);

curl_setopt_array($curl, array(
    CURLOPT_URL            => $purtelUrl,
    CURLOPT_HEADER         => false,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_SSL_VERIFYPEER => false,
));

$result = curl_exec($curl);

curl_close($curl);

$lines = explode("\n", $result);
array_pop($lines);

$headline_csv = array_shift($lines);
$headline = str_getcsv($headline_csv, ';');

while ($line = array_shift ($lines)) {
    $purt = str_getcsv($line, ';');

    foreach ($headline as $ind => $head) {
        $temp[$head] = $purt[$ind];
    }
    array_push($purtel, $temp);
}

$headline = array(
    utf8_decode('Buchungs-ID'),
    utf8_decode('Datum'),
    utf8_decode('Mandant'),
    utf8_decode('Anschluß'),
    utf8_decode('Kundennr.'),
    utf8_decode('Name'),
    utf8_decode('Straße'),
    utf8_decode('Ort'),
    utf8_decode('Rechnungsnr.'),
    utf8_decode('Betrag'),
    utf8_decode('Brutto'),
    utf8_decode('Netto'),
    utf8_decode('Umsatzsteuer'),
    utf8_decode('Zahlart'),
    utf8_decode('Zahlung'),
    utf8_decode('Debitor'),
    utf8_decode('Rechnungsdatum'),
    utf8_decode('Fälligkeitsdatum'),
    utf8_decode('Geschäfts-Part.-Nr'),
    utf8_decode('Vertrags-Konto-Nr.'),
    utf8_decode('Mandatsreferenz'),
);

/**
* write endica file header line 1
*/

$line = sprintf("% 1.1s% -3.3s% -1.1s% -2.2s% -1.1s\r\n",
    $endica['ReferenzNr']['STYPE'],       # length  1 % 1.1s    
    $endica['ReferenzNr']['MANDT'],       # length  3 % -3.3s
    $endica['ReferenzNr']['APPLK'],       # length  1 % -1.1s
    $endica['ReferenzNr']['HERKF'],       # length  2 % -2.2s
    $endica['ReferenzNr']['XEXTBL']       # length  1 % -1.1s
);

fputs ($txt_handle, $line);

/**
* prepare endica parameter 
*/
$endica['BFKKOP']['OPTXT'] = sprintf ($endica['BFKKOP']['OPTXT'], $fromEndicaDate, $toEndicaDate);

foreach ($purtel as $temp) {
    $purtel_root = array();
    $action = 'stammdatenexport';
    
    $parameter = array(
        'periode' => $periode,
        'anschluss' => $temp['anschluss'],
        'erweitert' => '9',
    );

    $PurtelUrl = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelUsername),
        urlencode($purtelPassword),
        urlencode($action)
    );

    foreach ($parameter as $key => $value) {
        $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
    }

    $curl = curl_init($PurtelUrl);

    curl_setopt_array($curl, array(
        CURLOPT_URL => $PurtelUrl,
        CURLOPT_HEADER => false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
    ));

    $result = curl_exec($curl);

    curl_close($curl);

    $lines = explode("\n", $result);

    array_pop($lines);
    
    $headline_csv = array_shift($lines);
    $headline = str_getcsv($headline_csv, ';');

    while ($line = array_shift ($lines)) {
        $purt = str_getcsv($line, ';');
    
        foreach ($headline as $ind => $head) {
            $temp1[$head] = $purt[$ind];
        }
    }

    $temp['betrag'] = $temp['betrag'] / 100;
    $temp['datum'] = date('d.m.Y', strtotime($temp['datum']));
    $temp['rechnungsdatum'] = date('Ymd', strtotime($temp['rechnungsdatum']));
    $temp['faelligkeitsdatum'] = date('Ymd', strtotime($temp['faelligkeitsdatum']));
    $temp['export_brutto'] = str_replace(',', '.',$temp['export_brutto']);
    $temp['export_netto'] = str_replace(',', '.',$temp['export_netto']);
    $temp['export_steuer'] = str_replace(',', '.',$temp['export_steuer']);
    
/**
* write endica BFKKKO (line 2) 
*/
    $endica['BFKKKO']['BLDAT'] = $temp['rechnungsdatum'];
    $endica['BFKKKO']['BUDAT'] = $temp['rechnungsdatum'];
    $endica['BFKKKO']['XBLNR'] = $temp['renummer'];

    $line = sprintf("% 1.1s% -30.30s% -12.12s% -2.2s% -5.5s% -8.8s% -8.8s% -8.8s% -16.16s\r\n",
        $endica['BFKKKO']['STYPE'],       # length  1 % 1.1s
        $endica['BFKKKO']['TBNAM'],       # length 30 % -30.30s
        $endica['BFKKKO']['OPBEL'],       # length 12 % -12.12s
        $endica['BFKKKO']['BLART'],       # length  2 % -2.2s
        $endica['BFKKKO']['WAERS'],       # length  5 % -5.5s
        $endica['BFKKKO']['BLDAT'],       # length  8 % -8.8s
        $endica['BFKKKO']['BUDAT'],       # length  8 % -8.8s
        $endica['BFKKKO']['WWERT'],       # length  8 % -8.8s
        $endica['BFKKKO']['XBLNR']        # length 16 % -16.16s
    );   

    fputs ($txt_handle, $line);

/**
* write endica BFKKOP (line 3) 
*/
    $endica['BFKKOP']['KOFIZ'] = $endica['BFKKOP']['KOFIZ_tarifkunde']; // should be changed when type of customer is implemented in customer database 

    $endica['BFKKOP']['GPART'] = $temp1['userfield1'];
    $endica['BFKKOP']['VKONT'] = $temp1['userfield2'];
    $endica['BFKKOP']['FAEDN'] = $temp['faelligkeitsdatum'];
    if ('BE' === $endica['BFKKOP']['KOFIZ']) {
        $amount = $temp['export_netto'];
    } else {
        $amount = $temp['export_brutto'];
    }
    $endica['BFKKOP']['BETRW'] = sprintf('%.2f', abs($amount));
    if ($amount < 0) {
        $endica['BFKKOP']['BETRW'] .= '-';
        $endica['BFKKOP']['TVORG'] = $endica['BFKKOP']['TVORG_gutschrift'];
    } else {
        $endica['BFKKOP']['TVORG'] = $endica['BFKKOP']['TVORG_rechnung'];
    }

    $line = sprintf("% 1.1s% -30.30s% -4.4s% -4.4s% -10.10s% -20.20s% -20.20s% -12.12s% -12.12s% 1.1s",
        $endica['BFKKOP']['STYPE'],       # length  1 % 1.1s
        $endica['BFKKOP']['TBNAM'],       # length 30 % -30.30s
        $endica['BFKKOP']['BUKRS'],       # length  4 % -4.4s
        $endica['BFKKOP']['GSBER'],       # length  4 % -4.4s
        $endica['BFKKOP']['GPART'],       # length 10 % -10.10s
        $endica['BFKKOP']['VTREF'],       # length 20 % -20.20s
        $endica['BFKKOP']['VTRE2'],       # length 20 % -20.20s
        $endica['BFKKOP']['VKONT'],       # length 12 % -12.12s
        $endica['BFKKOP']['ABWBL'],       # length 12 % -12.12s
        $endica['BFKKOP']['ABWTP']        # length  1 % 1.1s
    );

    fputs ($txt_handle, $line);
    
    $line = sprintf("% -4.4s% -4.4s% -2.2s% -2.2s% -10.10s% -2.2s% 1.1s% 1.1s% -50.50s%' -8.8s",
        $endica['BFKKOP']['HVORG'],       # length  4 % -4.4s
        $endica['BFKKOP']['TVORG'],       # length  4 % -4.4s
        $endica['BFKKOP']['KOFIZ'],       # length  2 % -2.2s
        $endica['BFKKOP']['SPART'],       # length  2 % -2.2s
        $endica['BFKKOP']['HKONT'],       # length 10 % -10.10s
        $endica['BFKKOP']['MWSKZ'],       # length  2 % -2.2s
        $endica['BFKKOP']['XANZA'],       # length  1 % 1.1s
        $endica['BFKKOP']['STAKZ'],       # length  1 % 1.1s
        $endica['BFKKOP']['OPTXT'],       # length 50 % -50.50s
        $endica['BFKKOP']['FAEDN']        # length  8 % -8.8s
    );

    fputs ($txt_handle, $line);

    $line = sprintf("% -8.8s% 1.1s% -8.8s% -6.6s% 1.1s% -10.10s% 16.16s% 16.16s% 16.16s% 16.16s",
        $endica['BFKKOP']['FAEDS'],       # length  8 % -8.8s
        $endica['BFKKOP']['VERKZ'],       # length  1 % 1.1s
        $endica['BFKKOP']['STUDT'],       # length  8 % -8.8s
        $endica['BFKKOP']['SKTPZ'],       # length  6 % -6.6s
        $endica['BFKKOP']['XMANL'],       # length  1 % 1.1s
        $endica['BFKKOP']['KURSF'],       # length 10 % -10.10s
        $endica['BFKKOP']['BETRH'],       # length 16 % 16.16s
        $endica['BFKKOP']['BETRW'],       # length 16 % 16.16s
        $endica['BFKKOP']['BETR2'],       # length 16 % 16.16s
        $endica['BFKKOP']['BETR3']        # length 16 % 16.16s
    );

    fputs ($txt_handle, $line);

    $line = sprintf("% -16.16s% -16.16s% -16.16s% -16.16s% -10.10s% -10.10s% 1.1s% 1.1s% -8.8s% -3.3s",
        $endica['BFKKOP']['SBETH'],       # length 16 % -16.16s
        $endica['BFKKOP']['SBETW'],       # length 16 % -16.16s
        $endica['BFKKOP']['SBET2'],       # length 16 % -16.16s
        $endica['BFKKOP']['SBET3'],       # length 16 % -16.16s
        $endica['BFKKOP']['MWSKO'],       # length 10 % -10.10s
        $endica['BFKKOP']['MWVKO'],       # length 10 % -10.10s
        $endica['BFKKOP']['SPZAH'],       # length  1 % 1.1s
        $endica['BFKKOP']['PYMET'],       # length  1 % 1.1s
        $endica['BFKKOP']['PERNR'],       # length  8 % -8.8s
        $endica['BFKKOP']['GRKEY']        # length  3 % -3.3s
    );

    fputs ($txt_handle, $line);

    $line = sprintf("% -4.4s% -2.2s% 1.1s% -8.8s% -8.8s% -10.10s% -2.2s% -2.2s% -16.16s% 1.1s",
        $endica['BFKKOP']['PERSL'],       # length  4 % -4.4s
        $endica['BFKKOP']['MAHNV'],       # length  2 % -2.2s
        $endica['BFKKOP']['MANSP'],       # length  1 % 1.1s
        $endica['BFKKOP']['ABRZU'],       # length  8 % -8.8s
        $endica['BFKKOP']['ABRZO'],       # length  8 % -8.8s
        $endica['BFKKOP']['FDGRP'],       # length 10 % -10.10s
        $endica['BFKKOP']['FDLEV'],       # length  2 % -2.2s
        $endica['BFKKOP']['FDZTG'],       # length  2 % -2.2s
        $endica['BFKKOP']['FDWBT'],       # length 16 % -16.16s
        $endica['BFKKOP']['AUGRS']        # length  1 % 1.1s
    );

    fputs ($txt_handle, $line);

    $line = sprintf("% -10.10s% 1.1s% -3.3s% -15.15s% -6.6s% -2.2s% -50.50s% -3.3s% -2.2s% 1.1s",
        $endica['BFKKOP']['PYGRP'],       # length 10 % -10.10s
        $endica['BFKKOP']['SPERZ'],       # length  1 % 1.1s
        $endica['BFKKOP']['INFOZ'],       # length  3 % -3.3s
        $endica['BFKKOP']['TXJCD'],       # length 15 % -15.15s
        $endica['BFKKOP']['VBUND'],       # length  6 % -6.6s
        $endica['BFKKOP']['KONTT'],       # length  2 % -2.2s
        $endica['BFKKOP']['KONTL'],       # length 50 % -50.50s
        $endica['BFKKOP']['OPSTA'],       # length  3 % -3.2s
        $endica['BFKKOP']['QSSKZ'],       # length  2 % -2.2s
        $endica['BFKKOP']['QSPTP']        # length  1 % 1.1s
    );

    fputs ($txt_handle, $line);

    $line = sprintf("% -16.16s% -6.6s% -12.12s\r\n",
        $endica['BFKKOP']['QSSHB'],       # length 16 % -16.16s
        $endica['BFKKOP']['EMCRD'],       # length  6 % -6.6s
        $endica['BFKKOP']['FINRE']        # length 12 % -12.12s
    );

    fputs ($txt_handle, $line);

/**
* write endica BFKKOPK (line 4) netto  
*/
    $endica['BFKKOPK']['BETRW'] = sprintf('%.2f', abs($temp['export_netto']));
    if ($temp['export_netto'] > 0) {
        $endica['BFKKOPK']['BETRW'] .= '-';    
    }
    $endica['BFKKOPK']['SBASW'] = $endica['BFKKOPK']['SBASW_erloes'];

    if ('BT' === $endica['BFKKOP']['KOFIZ']) {
        $endica['BFKKOPK']['HKONT']  = $endica['BFKKOPK']['HKONT_tarifkunde_erloes'];   
        $endica['BFKKOPK']['PRCTR']  = $endica['BFKKOPK']['PRCTR_tarifkunde'];   
        $endica['BFKKOPK']['MWSKZ']  = $endica['BFKKOPK']['MWSKZ_tarifkunde_erloes'];
        $endica['BFKKOPK']['KTOSL']  = $endica['BFKKOPK']['KTOSL_tarifkunde_erloes'];
        $endica['BFKKOPK']['KSCHL']  = $endica['BFKKOPK']['KSCHL_tarifkunde_erloes'];
    } elseif ('BS' === $endica['BFKKOP']['KOFIZ']) {
        $endica['BFKKOPK']['HKONT']  = $endica['BFKKOPK']['HKONT_sonderkunde_erloes'];   
        $endica['BFKKOPK']['PRCTR']  = $endica['BFKKOPK']['PRCTR_sonderkunde'];   
        $endica['BFKKOPK']['MWSKZ']  = $endica['BFKKOPK']['MWSKZ_sonderkunde_erloes'];
        $endica['BFKKOPK']['KTOSL']  = $endica['BFKKOPK']['KTOSL_sonderkunde_erloes'];
        $endica['BFKKOPK']['KSCHL']  = $endica['BFKKOPK']['KSCHL_sonderkunde_erloes'];
    } elseif ('BE' === $endica['BFKKOP']['KOFIZ']) {
        $endica['BFKKOPK']['HKONT']  = $endica['BFKKOPK']['HKONT_eigenverbrauch_erloes'];   
        $endica['BFKKOPK']['PRCTR']  = $endica['BFKKOPK']['PRCTR_eigenverbrauch'];   
        $endica['BFKKOPK']['MWSKZ']  = $endica['BFKKOPK']['MWSKZ_eigenverbrauch_erloes'];
        $endica['BFKKOPK']['KTOSL']  = $endica['BFKKOPK']['KTOSL_eigenverbrauch_erloes'];
        $endica['BFKKOPK']['KSCHL']  = $endica['BFKKOPK']['KSCHL_eigenverbrauch_erloes'];
    }

    $line = sprintf("% 1.1s% -30.30s% -4.4s% -10.10s% -4.4s% -10.10s% -10.10s% -10.10s% -6.6s% -4.4s",
        $endica['BFKKOPK']['STYPE'],      # length  1 % 1.1s
        $endica['BFKKOPK']['TBNAM'],      # length 30 % -30.30s
        $endica['BFKKOPK']['BUKRS'],      # length  4 % -4.4s
        $endica['BFKKOPK']['HKONT'],      # length 10 % -10.10s
        $endica['BFKKOPK']['GSBER'],      # length  4 % -4.4s
        $endica['BFKKOPK']['PRCTR'],      # length 10 % -10.10s
        $endica['BFKKOPK']['KOSTL'],      # length 10 % -10.10s
        $endica['BFKKOPK']['KDAUF'],      # length 10 % -10.10s
        $endica['BFKKOPK']['KDPOS'],      # length  6 % -6.6s
        $endica['BFKKOPK']['KDEIN']       # length  4 % -4.4s
    );

    fputs ($txt_handle, $line);

    $line = sprintf("% -8.8s% -10.10s% -4.4s% -2.2s% -8.8s% -10.10s% 16.16s% 16.16s% 16.16s% 16.16s",
        $endica['BFKKOPK']['PS_PSP_PNR'], # length  8 % -8.8s
        $endica['BFKKOPK']['PAOBJNR'],    # length 10 % -10.10s
        $endica['BFKKOPK']['PASUBNR'],    # length  4 % -4.4s
        $endica['BFKKOPK']['FDLEV'],      # length  2 % -2.2s
        $endica['BFKKOPK']['VALUT'],      # length  8 % -8.8s
        $endica['BFKKOPK']['KURSF'],      # length 10 % -10.10s
        $endica['BFKKOPK']['BETRH'],      # length 16 % 16.16s
        $endica['BFKKOPK']['BETRW'],      # length 16 % 16.16s
        $endica['BFKKOPK']['BETR2'],      # length 16 % 16.16s
        $endica['BFKKOPK']['BETR3']       # length 16 % 16.16s
    );

    fputs ($txt_handle, $line);
    
    $line = sprintf("% -2.2s% -2.2s% -15.15s% -16.16s% 16.16s% -3.3s% -6.6s% -12.12s% -6.6s% -2.2s",
        $endica['BFKKOPK']['STRKZ'],      # length  2 % -2.2s
        $endica['BFKKOPK']['MWSKZ'],      # length  2 % -2.2s
        $endica['BFKKOPK']['TXJCD'],      # length 15 % -15.15s
        $endica['BFKKOPK']['SBASH'],      # length 16 % -16.16s
        $endica['BFKKOPK']['SBASW'],      # length 16 % 16.16s
        $endica['BFKKOPK']['KTOSL'],      # length  3 % -3.3s
        $endica['BFKKOPK']['STPRZ'],      # length  6 % -6.6s
        $endica['BFKKOPK']['AUFNR'],      # length 12 % -12.12s
        $endica['BFKKOPK']['VBUND'],      # length  6 % -6.6s
        $endica['BFKKOPK']['KONTT']       # length  2 % -2.2s
    );

    fputs ($txt_handle, $line);

    $line = sprintf("% -50.50s% -4.4s% -17.17s% -3.3s% -50.50s% -18.18s% 1.1s\r\n",
        $endica['BFKKOPK']['KONTL'],      # length 50 % -50.50s
        $endica['BFKKOPK']['KSCHL'],      # length  4 % -4.4s
        $endica['BFKKOPK']['MENGE'],      # length 17 % -17.17s
        $endica['BFKKOPK']['MEINS'],      # length  3 % -3.3s
        $endica['BFKKOPK']['SGTXT'],      # length 50 % -50.50s
        $endica['BFKKOPK']['HZUON'],      # length 18 % -18.18s
        $endica['BFKKOPK']['XEIPH']       # length  1 % 1.1s
    );

    fputs ($txt_handle, $line);
/**
* write endica BFKKOPK (line 5) netto  
*/
    if ('BE' !== $endica['BFKKOP']['KOFIZ']) {
        $endica['BFKKOPK']['BETRW'] = sprintf('%.2f', abs($temp['export_steuer']));
        if ($temp['export_steuer'] > 0) {
            $endica['BFKKOPK']['BETRW'] .= '-';    
        }
        $endica['BFKKOPK']['SBASW'] = sprintf('%.2f', abs($temp['export_netto']));
        if ($temp['export_netto'] > 0) {
            $endica['BFKKOPK']['SBASW'] .= '-';    
        }
        if ('BT' === $endica['BFKKOP']['KOFIZ']) {
            $endica['BFKKOPK']['HKONT']  = $endica['BFKKOPK']['HKONT_tarifkunde_steuer'];   
            $endica['BFKKOPK']['PRCTR']  = $endica['BFKKOPK']['PRCTR_tarifkunde'];
            $endica['BFKKOPK']['MWSKZ']  = $endica['BFKKOPK']['MWSKZ_tarifkunde_steuer'];
            $endica['BFKKOPK']['KTOSL']  = $endica['BFKKOPK']['KTOSL_tarifkunde_steuer'];
            $endica['BFKKOPK']['KSCHL']  = $endica['BFKKOPK']['KSCHL_tarifkunde_steuer'];
        } elseif ('BS' === $endica['BFKKOP']['KOFIZ']) {
            $endica['BFKKOPK']['HKONT']  = $endica['BFKKOPK']['HKONT_sonderkunde_steuer'];   
            $endica['BFKKOPK']['PRCTR']  = $endica['BFKKOPK']['PRCTR_sonderkunde'];   
            $endica['BFKKOPK']['MWSKZ']  = $endica['BFKKOPK']['MWSKZ_sonderkunde_steuer'];
            $endica['BFKKOPK']['KTOSL']  = $endica['BFKKOPK']['KTOSL_sonderkunde_steuer'];
            $endica['BFKKOPK']['KSCHL']  = $endica['BFKKOPK']['KSCHL_sonderkunde_steuer'];
        }
        
        $line = sprintf("% 1.1s% -30.30s% -4.4s% -10.10s% -4.4s% -10.10s% -10.10s% -10.10s% -6.6s% -4.4s",
            $endica['BFKKOPK']['STYPE'],      # length  1 % 1.1s
            $endica['BFKKOPK']['TBNAM'],      # length 30 % -30.30s
            $endica['BFKKOPK']['BUKRS'],      # length  4 % -4.4s
            $endica['BFKKOPK']['HKONT'],      # length 10 % -10.10s
            $endica['BFKKOPK']['GSBER'],      # length  4 % -4.4s
            $endica['BFKKOPK']['PRCTR'],      # length 10 % -10.10s
            $endica['BFKKOPK']['KOSTL'],      # length 10 % -10.10s
            $endica['BFKKOPK']['KDAUF'],      # length 10 % -10.10s
            $endica['BFKKOPK']['KDPOS'],      # length  6 % -6.6s
            $endica['BFKKOPK']['KDEIN']       # length  4 % -4.4s
        );

        fputs ($txt_handle, $line);

        $line = sprintf("% -8.8s% -10.10s% -4.4s% -2.2s% -8.8s% -10.10s% 16.16s% 16.16s% 16.16s% 16.16s",
            $endica['BFKKOPK']['PS_PSP_PNR'], # length  8 % -8.8s
            $endica['BFKKOPK']['PAOBJNR'],    # length 10 % -10.10s
            $endica['BFKKOPK']['PASUBNR'],    # length  4 % -4.4s
            $endica['BFKKOPK']['FDLEV'],      # length  2 % -2.2s
            $endica['BFKKOPK']['VALUT'],      # length  8 % -8.8s
            $endica['BFKKOPK']['KURSF'],      # length 10 % -10.10s
            $endica['BFKKOPK']['BETRH'],      # length 16 % 16.16s
            $endica['BFKKOPK']['BETRW'],      # length 16 % 16.16s
            $endica['BFKKOPK']['BETR2'],      # length 16 % 16.16s
            $endica['BFKKOPK']['BETR3']       # length 16 % 16.16s
        );

        fputs ($txt_handle, $line);

        $line = sprintf("% -2.2s% -2.2s% -15.15s% -16.16s% 16.16s% -3.3s% -6.6s% -12.12s% -6.6s% -2.2s",
            $endica['BFKKOPK']['STRKZ'],      # length  2 % -2.2s
            $endica['BFKKOPK']['MWSKZ'],      # length  2 % -2.2s
            $endica['BFKKOPK']['TXJCD'],      # length 15 % -15.15s
            $endica['BFKKOPK']['SBASH'],      # length 16 % -16.16s
            $endica['BFKKOPK']['SBASW'],      # length 16 % 16.16s
            $endica['BFKKOPK']['KTOSL'],      # length  3 % -3.3s
            $endica['BFKKOPK']['STPRZ'],      # length  6 % -6.6s
            $endica['BFKKOPK']['AUFNR'],      # length 12 % -12.12s
            $endica['BFKKOPK']['VBUND'],      # length  6 % -6.6s
            $endica['BFKKOPK']['KONTT']       # length  2 % -2.2s
        );

        fputs ($txt_handle, $line);

        $line = sprintf("% -50.50s% -4.4s% -17.17s% -3.3s% -50.50s% -18.18s% 1.1s\r\n",
            $endica['BFKKOPK']['KONTL'],      # length 50 % -50.50s
            $endica['BFKKOPK']['KSCHL'],      # length  4 % -4.4s
            $endica['BFKKOPK']['MENGE'],      # length 17 % -17.17s
            $endica['BFKKOPK']['MEINS'],      # length  3 % -3.3s
            $endica['BFKKOPK']['SGTXT'],      # length 50 % -50.50s
            $endica['BFKKOPK']['HZUON'],      # length 18 % -18.18s
            $endica['BFKKOPK']['XEIPH']       # length  1 % 1.1s
        );

        fputs ($txt_handle, $line);
    }
}

fclose ($txt_handle);

?>
