<?php
$sql = array (
    'networks' => "
        SELECT `id_string` FROM `networks`
    ",
    
    'event' => "
        SELECT `event`, `price_net` FROM `customer_history_events`
    ",
    
    'customer' => "
        SELECT id FROM customers
        WHERE clientid = ?
        AND version IS NULL
    ",
);

$GLOBALS['sql'] = & $sql;
?>
