<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_ACCOUNTANCY');

$dateFormatter = new \IntlDateFormatter(
    null, // should be defined in php.ini
    \IntlDateFormatter::FULL,
    \IntlDateFormatter::FULL,
    null, // should be defined in php.ini
    \IntlDateFormatter::GREGORIAN,
    'MMMM yyyy'
);

$currentDate = $dateFormatter->format(new \DateTime('now -1 month'));

?>

<div class="accounting box">
    <h3>Buchhaltung Purtel</h3>
  
    <div class="accounting form">
    <p>
        <input name="StartPath" id="StartPath" type="hidden" value="<?php echo urlencode($StartPath); ?>" />
        <input name="SelPath" id="SelPath" type="hidden" value="<?php echo urlencode($SelPath); ?>" />
        <input name="StartURL" id="StartURL" type="hidden" value="<?php echo urlencode($StartURL); ?>" />
        <label for="AccountingDate">Buchungsdatum:</label>&nbsp;
        <input id="AccountingDate" type="text" class="only-month-date-picker" value="<?php echo $currentDate; ?>" name="AccountingDate" />
        <!--<input id="AccountingDate" type="text" class="datepicker" value="<?php echo $currentDate; ?>" name="AccountingDate" />-->
    </p>
    <br />
    <p>
        <strong>Verlaufsauswertung filtern:</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a id="reload-page-link" style="display: inline-block; margin-left: 10px;" href="#" title="Vorauswahl wiederherstellen">
            <img src="<?php echo $this->container->get('assets.packages')->getUrl('_img/Erase.png'); ?>" />
        </a>
        <br />
        <select name="filter-history-reporting" multiple="true" style="resize: both;">
            <?php
            foreach ($this->getDoctrine()->getRepository('AppBundle\Entity\User')->findByIsActive(true) as $user) {
                echo sprintf('<option value="%s"%s>%s (%s)</option>',
                    $user->getUsername(),
                    $user->getIncludeInHistoryReporting() ? ' selected="true"' : '',
                    $user->getFullName(),
                    $user->getUsername()
                );
            }
            ?>
        </select>
        <br />
        <br />
    </p>
    <p>
        <a href='#' target='_blank' id='accountingBill' class='button'>Rechnungsliste</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href='#' target='_blank' id='accountingBillEndica' class='button'>Rechnungsliste Endica</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href='#' target='_blank' id='accountingBillSap' class='button'>Rechnungsliste SAP</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href='#' target='_blank' id='accountingCheck' class='button'>Check Sepa und Rechnung</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <br /><br />
        <a href='#' target='_blank' id='accountingAccount' class='button'>Buchungsliste</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href='#' target='_blank' id='accountingCheckSepa' class='button'>Sepa Check</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href='#' target='_blank' id='accountingHistory' class='button'>Verlaufsauswertung</a>
    </p>
    <hr>
    Laden Sie die SEPA-Datei (.CDD) und die zugehörige <b>CSV-Datei</b> bills_YYYYMMDD.csv hoch.<br>
    <b>Nicht die Excel-Datei (.xlsx)</b>
    <div id="AccountingFileQueue"></div>
        <input id="AccountingFiles" name="AccountingFiles" type="file" multiple="true" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button name="AccountingFileload" onclick="accountingFileLoad()" type="button" class='button'><img alt="" src="../_img/Upload.png" height="12px"> Dateien hochladen</button>    
    </div>   
</div>

<script type="text/javascript">
    $(function() {
        $('.only-month-date-picker').datepicker( {
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'MM yy',
            onClose: function(dateText, inst) {
                $(this).datepicker('disable');
                $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                $(this).datepicker('enable');
            }
        });

        $('#reload-page-link').on('click', function () {
            window.location.reload();

            return false;
        });
    });
</script>
<style>
    .ui-datepicker-calendar {
        display: none;
    }
</style>
