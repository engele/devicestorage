<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_ACCOUNTANCY');

//session_start();
//$StartPath = $_SESSION['kv_config']['system']['startPath'];

require_once __DIR__.'/../config.inc';
//require_once $StartPath.'/vendor/wisotel/configuration/Configuration.php';

/*
$accountingTime = strtotime($_GET['accountingDate']);
$day            = date('j', $accountingTime);
$accountingTime = strtotime("-$day days", $accountingTime); 
$toDate         = date('Ymd', $accountingTime);
$day            = date('t', $accountingTime) - 1;
$accountingTime = strtotime("-$day days", $accountingTime);
$fromDate       = date ('Ymd', $accountingTime);
$periode        = date ('Ym', $accountingTime);
*/
$dateFormatter = new \IntlDateFormatter(
    null, // should be defined in php.ini
    \IntlDateFormatter::FULL,
    \IntlDateFormatter::FULL,
    null, // should be defined in php.ini
    \IntlDateFormatter::GREGORIAN,
    'MMMM yyyy'
);

$period = \DateTime::createFromFormat('U', $dateFormatter->parse($_GET['accountingDate']));
$period->add(new \DateInterval('P1D')); // add plus 1 day to avoid problems with daylight saving

$toDate = $period->format('Ymt');
$fromDate = $period->format('Ym01');
$periode = $period->format('Ym');

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=accounts_'.$periode.'.csv');  

$csv_handle = fopen('php://output', 'w');

$purtel = array();
$purtelUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$purtelPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
$purtelAction    = 'getbuchungen';
$purtelParameter = array(
    'von_datum' => $fromDate,
    'bis_datum' => $toDate,
    'erweitert' => 3
);

$purtelUrl = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
    urlencode($purtelUsername),
    urlencode($purtelPassword),
    urlencode($purtelAction)
);

foreach ($purtelParameter as $key => $value) {
    $purtelUrl .= "&$key=".urlencode(utf8_decode($value));
}

$curl = curl_init($purtelUrl);
curl_setopt_array($curl, array(
    CURLOPT_URL             => $purtelUrl,
    CURLOPT_HEADER          => false,
    CURLOPT_RETURNTRANSFER  => true,
    CURLOPT_SSL_VERIFYPEER  => false,
));

$result = curl_exec($curl);
curl_close($curl);

$lines = explode("\n", $result);
array_pop($lines);

$headline_csv = array_shift($lines);
$headline = str_getcsv($headline_csv, ';');

while ($line = array_shift ($lines)) {
    $purt = str_getcsv($line, ';');
    foreach ($headline as $ind => $head) {
        $temp[$head] = $purt[$ind];
    }
    array_push($purtel, $temp);
}

$headline = array(
    utf8_decode('Buchungs-ID'),
    utf8_decode('Rechnungsnr.'),
    utf8_decode('Buchungsdatum'),
    utf8_decode('Mandant'),
    utf8_decode('Anschluß'),
    utf8_decode('Unterkonto'),
    utf8_decode('Kundennr.'),
    utf8_decode('Kundenart'),
    utf8_decode('Verwendungszweck'),
    utf8_decode('Betrag_Soll'),
    utf8_decode('Betrag_Haben'),
    utf8_decode('Mitarbeiter'),
    utf8_decode('Buchungsart'),
    utf8_decode('Erledigt'),
    utf8_decode('Entgeldtyp'),
);

fputcsv($csv_handle, $headline, ';');

foreach ($purtel as $temp) {
    $temp['betrag_soll']  = str_replace ('.', ',', $temp['betrag_soll'] / 100);
    $temp['betrag_haben'] = str_replace ('.', ',', $temp['betrag_haben'] / 100);
    $temp['datum'] = date('d.m.Y h:m',strtotime($temp['datum']));

    $temp['kundenart']  = $temp['kundenart']  == 1 ? 'GK' : 'PK';
    $temp['erledigt']   = $temp['erledigt']   == 1 ? 'ja' : 'nein';

    switch ($temp['entgelttyp']) {
        case 0:
            $temp['entgelttyp'] = 'Sonstige Buchungen';
            break;

        case 1:
            $temp['entgelttyp'] = 'Einmalig Produkt';
            break;

        case 2:
            $temp['entgelttyp'] = 'Einmalig CrossSelling';
            break;

        case 3:
            $temp['entgelttyp'] = 'Laufend Produkt';
            break;

        case 4:
            $temp['entgelttyp'] = 'Laufend CrossSelling';
            break;

        case 5:
            $temp['entgelttyp'] = 'Einmalig Modul';
            break;

        case 6:
            $temp['entgelttyp'] = 'Laufend Modul';
            break;

        case 7:
            $temp['entgelttyp'] = 'Verbindungen';
            break;
    }

    $csv = array(
        $temp['id'],
        $temp['renummer'],
        $temp['datum'],
        $temp['mandant'],
        $temp['anschluss'],
        $temp['unterkonto'],
        $temp['kundennummer_extern'],
        $temp['kundenart'],
        $temp['verwendungszweck'],
        $temp['betrag_soll'],
        $temp['betrag_haben'],
        $temp['mitarbeiter'],
        $temp['art'],
        $temp['erledigt'],
        $temp['entgelttyp'],
    );

    fputcsv($csv_handle, $csv, ';');
}
fclose($csv_handle);

?>
