<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_ACCOUNTANCY');

//session_start();

require_once __DIR__.'/../config.inc';
//require_once $StartPath.'/vendor/wisotel/configuration/Configuration.php';
/*
$accountingTime = strtotime($_GET['accountingDate']);
$day            = date('j', $accountingTime);
$accountingTime = strtotime("-$day days", $accountingTime); 
$toDate         = date ('Ymd', $accountingTime);
$day            = date('t', $accountingTime) - 1;
$accountingTime = strtotime("-$day days", $accountingTime);
$fromDate       = date ('Ymd', $accountingTime);
$periode        = date ('Ym', $accountingTime);
*/
$dateFormatter = new \IntlDateFormatter(
    null, // should be defined in php.ini
    \IntlDateFormatter::FULL,
    \IntlDateFormatter::FULL,
    null, // should be defined in php.ini
    \IntlDateFormatter::GREGORIAN,
    'MMMM yyyy'
);

$period = \DateTime::createFromFormat('U', $dateFormatter->parse($_GET['accountingDate']));
$period->add(new \DateInterval('P1D')); // add plus 1 day to avoid problems with daylight saving

$toDate = $period->format('Ymt');
$fromDate = $period->format('Ym01');
$periode = $period->format('Ym');

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=bills_'.$periode.'.csv');  

$csv_handle = fopen('php://output', 'w');

$purtel = array();
$purtelUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$purtelPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
$purtelAction   = 'getrechnungen';
$purtelParameter = array (
    'von_datum' => $fromDate,
    'bis_datum' => $toDate,
    'erweitert' => 1
);    
$purtelUrl = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
    urlencode($purtelUsername),
    urlencode($purtelPassword),
    urlencode($purtelAction)
);
foreach ($purtelParameter as $key => $value) {
    $purtelUrl .= "&$key=".urlencode(utf8_decode($value));
}

$curl = curl_init($purtelUrl);

curl_setopt_array($curl, array(
    CURLOPT_URL            => $purtelUrl,
    CURLOPT_HEADER         => false,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_SSL_VERIFYPEER => false,
));

$result = curl_exec($curl);

curl_close($curl);

$lines = explode("\n", $result);
array_pop($lines);

$headline_csv = array_shift($lines);
$headline = str_getcsv($headline_csv, ';');

while ($line = array_shift ($lines)) {
    $purt = str_getcsv($line, ';');

    foreach ($headline as $ind => $head) {
        $temp[$head] = $purt[$ind];
    }
    array_push($purtel, $temp);
}

$headline = array(
    utf8_decode('Buchungs-ID'),
    utf8_decode('Datum'),
    utf8_decode('Mandant'),
    utf8_decode('Anschluß'),
    utf8_decode('Kundennr.'),
    utf8_decode('Name'),
    utf8_decode('Straße'),
    utf8_decode('Ort'),
    utf8_decode('Rechnungsnr.'),
    utf8_decode('Betrag'),
    utf8_decode('Brutto'),
    utf8_decode('Netto'),
    utf8_decode('Umsatzsteuer'),
    utf8_decode('Zahlart'),
    utf8_decode('Zahlung'),
    utf8_decode('Debitor'),
    utf8_decode('Rechnungsdatum'),
    utf8_decode('Fälligkeitsdatum'),
    utf8_decode('Geschäfts-Part.-Nr'),
    utf8_decode('Vertrags-Konto-Nr.'),
    utf8_decode('Mandatsreferenz'),
);

fputcsv($csv_handle, $headline, ';');

foreach ($purtel as $temp) {
    $purtel_root = array();
    $action = 'stammdatenexport';
    
    $parameter = array(
        'periode' => $periode,
        'anschluss' => $temp['anschluss'],
        'erweitert' => '9',
    );

    $PurtelUrl = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
        urlencode($purtelUsername),
        urlencode($purtelPassword),
        urlencode($action)
    );

    foreach ($parameter as $key => $value) {
        $PurtelUrl .= "&$key=".urlencode(utf8_decode($value));
    }

    $curl = curl_init($PurtelUrl);

    curl_setopt_array($curl, array(
        CURLOPT_URL => $PurtelUrl,
        CURLOPT_HEADER => false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
    ));

    $result = curl_exec($curl);

    curl_close($curl);

    $lines = explode("\n", $result);

    array_pop($lines);
    
    $headline_csv = array_shift($lines);
    $headline = str_getcsv($headline_csv, ';');

    while ($line = array_shift ($lines)) {
        $purt = str_getcsv($line, ';');
    
        foreach ($headline as $ind => $head) {
            $temp1[$head] = $purt[$ind];
        }
    }

    $temp['betrag'] = str_replace('.', ',', $temp['betrag'] / 100);
    $temp['datum'] = date('d.m.Y', strtotime($temp['datum']));
    $temp['rechnungsdatum'] = date('d.m.Y', strtotime($temp['rechnungsdatum']));
    $temp['faelligkeitsdatum'] = date('d.m.Y', strtotime($temp['faelligkeitsdatum']));
    
    $csv = array(
        $temp['id'],
        $temp['datum'],
        $temp['mandant'],
        $temp['anschluss'],
        $temp['kundennummer_extern'],
        $temp1['Name1'].' '.$temp1['Name2'],
        $temp1['Strasse'],
        $temp1['PLZ'].' '.$temp1['Ort'],
        $temp['renummer'],
        $temp['betrag'],
        $temp['export_brutto'],
        $temp['export_netto'],
        $temp['export_steuer'],
        $temp1['AktuelleZahlart'],
        $temp['export_zahlung'],
        $temp['export_debitor'],
        $temp['rechnungsdatum'],
        $temp['faelligkeitsdatum'],
        $temp1['userfield1'],
        $temp1['userfield2'],
        $temp1['userfield4'],
    );

    fputcsv($csv_handle, $csv, ';');
}

fclose ($csv_handle);

?>
