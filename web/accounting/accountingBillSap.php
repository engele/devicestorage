<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_ACCOUNTANCY');

//session_start();

require_once __DIR__.'/../config.inc';
//require_once $StartPath.'/vendor/wisotel/configuration/Configuration.php';
/*
$accountingTime = strtotime($_GET['accountingDate']);
$day            = date('j', $accountingTime);
$accountingTime = strtotime("-$day days", $accountingTime); 
$toDate         = date ('Ymd', $accountingTime);
$day            = date('t', $accountingTime) - 1;
$accountingTime = strtotime("-$day days", $accountingTime);
$fromDate       = date ('Ymd', $accountingTime);
$periode        = date ('Ym', $accountingTime);
*/
$dateFormatter = new \IntlDateFormatter(
    null, // should be defined in php.ini
    \IntlDateFormatter::FULL,
    \IntlDateFormatter::FULL,
    null, // should be defined in php.ini
    \IntlDateFormatter::GREGORIAN,
    'MMMM yyyy'
);

$period = \DateTime::createFromFormat('U', $dateFormatter->parse($_GET['accountingDate']));
$period->add(new \DateInterval('P1D')); // add plus 1 day to avoid problems with daylight saving

$toDate = $period->format('Ymt');
$fromDate = $period->format('Ym01');
$periode = $period->format('Ym');

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=billsSap_'.$periode.'.txt');  

$txt_handle = fopen('php://output', 'w');

$purtel = array();
$purtelUsername = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserUsername');
$purtelPassword = \Wisotel\Configuration\Configuration::get('purtelContractSuperuserPassword');
$purtelAction   = 'getrechnungen';
$purtelParameter = array (
    'von_datum' => $fromDate,
    'bis_datum' => $toDate,
    'format' => 'SAP'
);    
$purtelUrl = sprintf('https://ipcom.purtel.com/index.php?super_username=%s&super_passwort=%s&action=%s',
    urlencode($purtelUsername),
    urlencode($purtelPassword),
    urlencode($purtelAction)
);
foreach ($purtelParameter as $key => $value) {
    $purtelUrl .= "&$key=".urlencode(utf8_decode($value));
}

$curl = curl_init($purtelUrl);

curl_setopt_array($curl, array(
    CURLOPT_URL            => $purtelUrl,
    CURLOPT_HEADER         => false,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_SSL_VERIFYPEER => false,
));

$result = curl_exec($curl);

curl_close($curl);

$lines = explode("\n", $result);

while ($line = array_shift ($lines)) {
    fputs($txt_handle, $line."\n");
}

fclose ($txt_handle);

?>
