<?php

return [
    'wkvDatabase' => [
        'host'      => 'localhost',
        'login'     => 'bkv',
        'password'  => 'bkv',
        'database'  => 'bkv',
        'encoding'  => 'utf8',
    ],
    'wkvGroupwareDatabase' => [
        'host'      => 'localhost',
        'login'     => 'bkv',
        'password'  => 'bkv',
        'database'  => 'bkv-group',
        'encoding'  => 'utf8',
    ],
    'companyName' => 'Stadtwerke Bühl',
    'headTitle' => 'bkv - Stadtwerke Bühl Kundenverwaltung',
    'secKey' => '', // wird nicht mehr verwendet
    'accountingPurtelUsername' => '746259',
    'accountingPurtelPassword' => '3dc74f224f71',
    'purtelContractSuperuserUsername' => '746259',
    'purtelContractSuperuserPassword' => '3dc74f224f71',
    'purtelReseller' => 'res100312',
    'purtelBillnr' => [
        'preChars' => 0,
        'length' => 6,
    ],
    'endica' => [
        'ReferenzNr' => '0104R',
        'BFKKKO' => [
            'STYPE' => 1,           # length 1
            'TBNAM' => 'BFKKKO',    # length 30
            'OPBEL' => '',          # length 12
            'BLART' => 'EX',        # length 2
            'WAERS' => 'EUR',       # length 5
            'BLDAT' => '',          # length 8
            'BUDAT' => '',          # length 8
            'WWERT' => '',          # length 8
            'XBLNR' => '',          # length 16
            'HBBLA' => '',          # not used
            'PRTID' => '',          # not used
        ],
        'BFKKOP' => [
            'STYPE' => 2,           # length 1
            'TBNAM' => 'BFKKOP',    # length 30
            'BUKRS' => '',          # length 4
            'GSBER' => '',          # length 4
            'GPART' => '',          # length 10
            'SEGMENT' => '',        # not used
            'VTREF' => '',          # length 20
            'VTRE2' => '',          # length 20
            'VKONT' => '',          # length 12 
            'ABWBL' => '',          # length 12
            'ABWTP' => '',          # length 1
            'HVORG' => '',          # length 4
            'TVORG' => '',          # length 4
            'KOFIZ' => '',          # length 2
            'SPART' => '',          # length 2
            'HKONT' => '',          # length 10
            'MWSKZ' => '',          # length 2
            'XANZA' => '',          # length 1
            'STAKZ' => '',          # length 1
            'OPTXT' => 'Rechnung BADEN.NET für den Zeitraum vom ',          # length 50
            'FAEDN' => '',          # length 8 
            'FAEDS' => '',          # length 8
            'VERKZ' => '',          # length 1
            'STUDT' => '',          # length 8
            'SKTPZ' => '',          # length 6
            'XMANL' => '',          # length 1
            'KURSF' => '',          # length 10
            'BETRH' => '',          # length 16 
            'BETRW' => '',          # length 16
            'BETR2' => '',          # length 16
            'BETR3' => '',          # length 16
            'SBETH' => '',          # length 16
            'SBETW' => '',          # length 16
            'SBET2' => '',          # length 16
            'SBET3' => '',          # length 16
            'MWSKO' => '',          # length 10
            'MWVKO' => '',          # length 10
            'SPZAH' => '',          # length 1
            'PYMET' => '',          # length 1
            'PERNR' => '',          # length 8
            'GRKEY' => '',          # length 3
            'PERSL' => '',          # length 4
            'MAHNV' => '',          # length 2
            'MANSP' => '',          # length 1
            'ABRZU' => '',          # length 8
            'ABRZO' => '',          # length 8
            'FDGRP' => '',          # length 10
            'FDLEV' => '',          # length 2
            'FDZTG' => '',          # length 2
            'FDWBT' => '',          # length 16
            'AUGRS' => '',          # length 1
            'PYGRP' => '',          # length 10
            'SPERZ' => '',          # length 1
            'INFOZ' => '',          # length 3
            'TXJCD' => '',          # length 15
            'VBUND' => '',          # length 6
            'KONTT' => '',          # length 2
            'KONTL' => '',          # length 50
            'OPSTA' => '',          # length 3
            'QSSKZ' => '',          # length 2
            'QSPTP' => '',          # length 1
            'QSSHB' => '',          # length 16
            'EMCRD' => '',          # length 6
            'FINRE' => '',          # length 12
            'PYBUK' => '',          # not used
            'PSGRP' => '',          # not used
            'SUBAP' => '',          # not used
            'BUPLA' => '',          # not used
            'INCLUDE' => '',        # not used
            'ORIGFIKRS' => '',      # not used
            'FIPEX' => '',          # not used
            'FISTL' => '',          # not used
            'FONDS' => '',          # not used
            'FKBER' => '',          # not used
            'MEASURE' => '',        # not used
            'GRANT_NBR' => '',      # not used
            'BUDGETYEAR' => '',     # not used
        ],
        'BFKKOPK' => [
            'STYPE' => 2,           # length 1
            'TBNAM' => 'BFKKOPK',   # length 30
            'BUKRS' => '',          # length 4
            'HKONT' => '',          # length 10
            'GSBER' => '',          # length 4
            'PRCTR' => '',          # length 10
            'SEGMENT' => '',        # not used
            'KOSTL' => '',          # length 10
            'KDAUF' => '',          # length 10
            'KDPOS' => '',          # length 6
            'KDEIN' => '',          # length 4
            'PS_PSP_PNR' => '',     # length 8
            'PAOBJNR' => '',        # length 10
            'PASUBNR' => '',        # length 4
            'FDLEV' => '',          # length 2
            'VALUT' => '',          # length 8
            'KURSF' => '',          # length 10
            'BETRH' => '',          # length 16
            'BETRW' => '',          # length 16
            'BETR2' => '',          # length 16
            'BETR3' => '',          # length 16
            'STRKZ' => '',          # length 2
            'MWSKZ' => '',          # length 2
            'TXJCD' => '',          # length 15
            'SBASH' => '',          # length 16
            'SBASW' => '',          # length 16
            'KTOSL' => '',          # length 3
            'STPRZ' => '',          # length 6
            'AUFNR' => '',          # length 12
            'VBUND' => '',          # length 6
            'KONTT' => '',          # length 2
            'KONTL' => '',          # length 50
            'KSCHL' => '',          # length 4
            'MENGE' => '',          # length 17
            'MEINS' => '',          # length 3
            'SGTXT' => '',          # length 50
            'HZUON' => '',          # length 18
            'XEIPH' => '',          # length 1
            'MWSZKZ' => '',         # not used
            'PS_PSP_PNR_TXT' => '', # not used
            'PSGRP' => '',          # not used
            'LDGRP' => '',          # not used
            'INCLUDE' => '',        # not used
            'ORIGFIKRS' => '',      # not used
            'FIPEX' => '',          # not used
            'FISTL' => '',          # not used
            'FONDS' => '',          # not used
            'FKBER' => '',          # not used
            'MEASURE' => '',        # not used
            'GRANT_NBR' => '',      # not used
            'BUDGETYEAR' => '',     # not used
        ],
    ],
    'groupwareIpAddress' => $_SERVER['SERVER_NAME'],
    'groupwareProtocol' => $_SERVER['REQUEST_SCHEME'],
    'groupwarePort' => $_SERVER['SERVER_PORT'],
    'groupwareSubPath' => '/ticket',
    'purtelSuperuser' => [
        'super' => [
            'username' => '746259',
            'password' => '3dc74f224f71',
            'fullname' => 'Super Admin'
        ],
         'jl' => [
            'username' => '753370',
            'password' => '4b33df38f46d',
            'fullname' => 'Jürgen Lehmann'
        ],
         'cno' => [
            'username' => '753368',
            'password' => 'a849015130e2',
            'fullname' => 'Christian Nolepa'
        ],
         'rdlr' => [
            'username' => '753369',
            'password' => '1e79a1026cb4',
            'fullname' => 'Ramon de la Rosa'
        ],
    ],
    'urlToIcinga' => 'http://nowhere.intern',
    'axirosAcs' => [
        'url' => 'http://nowhere.intern:80', // without trailing /
        'username' => 'xxx',
        'password' => 'xxx',
        'registrar' => '',
    ],
    'technicMailAccount' => [
        'username' => '',
        'password' => '',
        'host' => '',
        'port' => 25,
        'encryption' => 'tls',
    ],
    'purtelAccountValues' => [
        'land' => 49,
        'zeitzone' => 1,
        'waehrung' => 'Euro',
        'produkt' => 6736,
        'nolimit' => 1,
        'kredit' => 10000,
        'postversand' => 0,
        'rechnung_per_mail' => 2,
        'evn' => 1,
    ],
    'radiusAccess' => [
        'database' => 'xxx',
        'username' => 'xxx',
        'password' => 'xxx',
        'port' => '3306',
        'host' => 'xxx.xxx.xxx.xxx',
    ],
    'radiusUserGroup' => '',
    'pppoe' => [
        'username' => 'BuehlLapi',
        'password' => 'Buehl@2018.',
        'ipaddress' => '10.175.7.253',
    ],
    'pppoeLabor' => [
        'username' => 'BuehlLapi',
        'password' => 'Buehl@2018.',
        'ipaddress' => '',
        'port' => 50240,
    ],
    'pppoeServer' => [
        ['ipaddress' => '10.175.7.254',],
        ['ipaddress' => '10.175.7.253',],
        ['ipaddress' => '',],
        ['ipaddress' => '',],
        ['ipaddress' => '',],
        ['ipaddress' => '',],
    ],
    'tftpServer' => '10.175.3.2',
    'dnsServer' => [
        '1' => 'ipv4',
    ],
    'sntpServer' => '185.207.104.70',
    'purtelAcsProvEmailadresse' => '',
    'dslam' => [
        'username' => '',
        'password' => '',
    ],
    'dslamLabor' => [
        'ipaddress' => '',
        'port' => 50005,
    ],
    'mailServer' => [
        'smtpHost' => 'localhost',
        'smtpSecure' => null,
        'smtpAuth' => false,
        'smtpUser' => 'USER',
        'smtpPasswd' => 'PASSWORD',
        'smtpPort' => 25,
        'from' => 'kundenservice@baden.net',
        'fromName' => 'Kundenservice BADEN.NET',
        'bcc' => 'kundenservice@baden.net',
        'isHTML' => false,
        'FormIsHTML' => true,
        'CharSet' => 'utf-8',
    ],
];

?>
