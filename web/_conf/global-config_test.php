<?php

return [
    'wkvDatabase' => [
        'host'      => 'localhost',
        'login'     => 'wkv',
        'password'  => 'wkv',
        'database'  => 'automated-test-wkv2',
        'encoding'  => 'utf8',
    ],
    'wkvGroupwareDatabase' => [],
    'companyName' => 'WiSoTEL',
    'headTitle' => 'WKV - WiSoTEL Kundenverwaltung',
    'secKey' => 'WKV - WiSoTEL Kundenverwaltung', // wird nicht mehr verwendet
    'customerClientidLength' => 13,
    'accountingPurtelUsername' => '',
    'accountingPurtelPassword' => '',
    'purtelContractSuperuserUsername' => '',
    'purtelContractSuperuserPassword' => '',
    'purtelReseller' => '',
    'purtelSipRealm' => '',
    'purtelSipRealmOld' => '',
    'purtelRadius' => false,
    'purtelRadiusVia' => 'purtel',
    'purtelRadiusAcs' => '0',
    'purtelBillnr' => [
        'preChars' => 0,
        'length' => 6,  
    ],
    'groupwareIpAddress' => 'groupware.wisotel.intern',
    'groupwarePort' => 80,
    'groupwareProtocol' => 'http',
    'groupwareSubPath' => null,
    'purtelSuperuser' => [
        'asd' => [
            'username' => 'asd',
            'password' => 'asd',
            'fullname' => 'asd'
        ],
    ],
    'urlToIcinga' => 'http://nowhere.intern',
    'axirosAcs' => [
        'url' => '', // without trailing /
        'username' => '',
        'password' => '',
        'registrar' => '',
    ],
    'technicMailAccount' => [
        'username' => '',
        'password' => '',
        'host' => '',
        'port' => 25,
        'encryption' => 'tls',
    ],
    'purtelAccountValues' => [
        'land' => 49,
        'zeitzone' => 1,
        'waehrung' => 'Euro',
        'produkt' => 4480,
        'nolimit' => 1,
        'kredit' => 10000,
        'postversand' => 0,
        'rechnung_per_mail' => 2,
        'evn' => 1,
    ],
    'radiusAccess' => [
        'database' => 'xxx',
        'username' => 'xxx',
        'password' => 'xxx',
        'port' => '3306',
        'host' => '',
    ],
    'radiusUserGroup' => 'ZEAG_DSL',
    'pppoe' => [
        'username' => '',
        'password' => '',
    ],
    'pppoeLabor' => [
        'username' => '',
        'password' => '',
        'ipaddress' => '',
        'port' => 0,
    ],
    'pppoeServer' => [
        ['ipaddress' => '',],
    ],
    'tftpServer' => 'ip.ip.ip.ip',
    'dnsServer' => [
        '1' => 'ipv4',
    ],
    'purtelAcsProvEmailadresse' => '',
    'dslam' => [
        'username' => '',
        'password' => '',
    ],
    'dslamLabor' => [
        'ipaddress' => '',
        'port' => 0,
    ],
    'mailServer' => [
        'smtpHost' => '',
        'smtpAuth' => true,
        'smtpUser' => '',
        'smtpPasswd' => '',
        'smtpPort' => 25,
        'from' => '',
        'fromName' => '',
        'bcc' => '',
        'isHTML' => false,
        'FormIsHTML' => true,
        'CharSet' => 'utf-8',
    ],
    'defaultTaxRate' => 1,
];

?>
