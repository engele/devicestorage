<?php

function generateRemotePassword($clientid, $password)
{
    return $remotePw = substr($clientid, -4, 4) . $password;
}

function setInputValue ($cust_array, $dbfield, $default = '')
{
    if (key_exists($dbfield, $cust_array)){
        echo $cust_array[$dbfield];    
    } else {
        echo $default;    
    }
}

function setInputOption ($cust_array, $dboption, $dbfield, $default = '')
{
    if (key_exists($dbfield, $cust_array)) {
        if (key_exists($cust_array[$dbfield], $dboption)) {
            echo $dboption[$cust_array[$dbfield]];    
        }  else {
            echo '';    
        }
    } else {
        echo $default;
    }
}

function setOption ($option_array, $cust_array = array(), $dbfield = NULL, $ro = '') 
{
    $opt_group = false;

    foreach ($option_array as $key => $value) {
        if ($key === '-n-') {
            $key = '';
        }

        if (preg_match('/^\-{2}\d{2}$/', $key)) {
            if ($opt_group) {
                echo "</optgroup>\n";
            }

            echo "<optgroup label='$value'>\n";
        } else {
            //if (key_exists ($dbfield, $cust_array) and $cust_array[$dbfield] == $key) {
            if (isset($cust_array[$dbfield]) and $cust_array[$dbfield] == $key) {
                echo "<option value='$key' $ro selected>$value</option>\n";
            } else {
                echo "<option value='$key' $ro>$value</option>\n";
            }
        }
    }

    if ($opt_group) {
        echo "</optgroup>\n";
    }
}

/*
function set_sec_field ($field, $key, $iv) 
{
    $crypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $field, MCRYPT_MODE_ECB, $iv);
    return ($crypt);
}

function get_sec_field ($crypt, $key, $iv) 
{
    $field = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $crypt, MCRYPT_MODE_ECB, $iv);
    return ($field);
}

function is_sec_field ($field, $crypt, $key, $iv)
{
    $tfield = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $crypt, MCRYPT_MODE_ECB, $iv));
    $field  = trim($field);
    if ($tfield == $field) {
        return (true);
    } else {
        return (false);
    }
}
*/

/*function set_sec_field ($field, $key, $iv) 
{
    $crypt = openssl_encrypt($field, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);

    return ($crypt);
}

function get_sec_field ($crypt, $key, $iv) 
{
    $field = openssl_decrypt($crypt, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);

    return ($field);
}

function is_sec_field ($field, $crypt, $key, $iv)
{
    $tfield = trim(openssl_decrypt($crypt, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv));
    $field  = trim($field);

    if ($tfield == $field) {
        return (true);
    } else {
        return (false);
    }
}*/

/*
function check_rights ($div_area, $div_class, $div_head, $incpath)
{
    if (key_exists('StartPath',   $GLOBALS)) $StartPath   = $GLOBALS['StartPath'];
    if (key_exists('StartURL',    $GLOBALS)) $StartURL    = $GLOBALS['StartURL'];
    if (key_exists('SelPath',     $GLOBALS)) $SelPath     = $GLOBALS['SelPath'];
    if (!$_SESSION['login']['right'][$div_area][$div_class]) $incpath = 'noRight.php';
    require ($StartPath.'/'.$incpath);
    $IncModul = ob_get_contents();
    ob_clean();
    return ($IncModul);
}
*/

?>
