<?php

require_once __DIR__.'/../vendor/wisotel/configuration/Configuration.php';

// Database kv
$database = \Wisotel\Configuration\Configuration::get('wkvDatabase');

$db_error = '';

$db = new mysqli($database['host'], $database['login'], $database['password'], $database['database'], isset($database['port']) ? $database['port'] : null);

if ($db->connect_errno) {
    $db_error = "<hr>Fehler db: ".$db->connect_errno."<hr>";
} else {
    $db->set_charset($database['encoding']);
    $db->select_db($database['database']);
}

$GLOBALS['db'] = $db;

$dbegw_error = '';
// Database Groupware
$databaseegw = \Wisotel\Configuration\Configuration::get('wkvGroupwareDatabase');

if (!empty($databaseegw)) {
    $dbegw = new mysqli($databaseegw['host'], $databaseegw['login'], $databaseegw['password'], $databaseegw['database']);

    if ($dbegw->connect_errno) {
        $db_error = "<hr>Fehler db: ".$dbegw->connect_errno."<hr>";
    } else {
        $dbegw->set_charset($databaseegw['encoding']);
        $dbegw->select_db($databaseegw['database']);
    }
}

?>
