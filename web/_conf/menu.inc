<?php
$menuicon = array (
    'i' => 'ui-icon-info',
    'w' => 'ui-icon-wrench',
    'e' => 'ui-icon-pencil'
    //'h' => hidden
);
  
$menu_start = 'overview';
// Menütitel:Icon:Funktion
$menu = array (
    'overview'        => 'Übersicht:i:',
    'statistics'      => 'Statistiken:i:',
    'csvExport'       => 'CSV Export:i:',
    'accounting'      => 'Buchhaltung:i:',
    #'partner'         => 'Partner:e:',
    'customerSearch'  => 'Kundensuche:e:',
    'customer:1'      => 'Neuer Interessent:e:interessent',
    'customer:2'      => 'Neuer Vertrag:e:vertrag',
    'customer'        => 'Ändern:h:',          
    'network'         => 'Netzübersicht:w:',
    'iprange'         => 'IP-Block Übersicht:w:',
    'location'        => 'Standorte:h:',
    'card'            => 'DSLAM-Karten:h:',
    'lsaKvzPartition' => 'LSA-KVz Zuordnungen:h:',
    'technics'        => 'Technik Hilfe:w:',
    'administration'  => 'Administration:w:',
    'account'         => 'Anmeldung:e:',
);
?>