<?php

$externalCustomerIdFieldName = 'Externe Kundenkennung';
  
if (null !== $exCustField = \Wisotel\Configuration\Configuration::get('externalCustomerIdFieldName')) {
    $externalCustomerIdFieldName = $exCustField;
}

$option_search = [
    '-n-' => 'Alle Felder',

    '--01' => 'Kunde',
    'lastname' => 'Nachname',
    'company' => 'Firma',
    'zipcode' => 'PLZ',
    'city' => 'Stadt',
    'street' => 'Straße',
    'clientid' => 'Kundennr.',
    'cust.customer_id' => $externalCustomerIdFieldName,
    'cust.emailaddress' => 'E-Mail Adresse',
    'cust.bank_account_debitor' => 'Debitor',
    'cust.bank_account_mandantenid' => 'Mandatsreferenz',

    '--02' => 'Anschluß',
    'connection_zipcode' => 'PLZ',
    'connection_city' => 'Stadt',
    'connection_street' => 'Straße',

    '--03' => 'Purtel',
    'purtel_login' => 'Purtel Account',

    '--04' => 'Leitung',
    'tal_dtag_assignment_no' => 'DTAG-Vertragsnummer',
    'dtag_line' => 'Leitungsbezeichnung (LBZ)',
    
    '--05' => 'Engeräte',
    'terminal_serialno' => 'Seriennummer',
    'mac_address' => 'MAC Adresse',
    'pppoe_pin' => 'PPPoE-PIN',
];

?>
