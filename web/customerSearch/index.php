<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_CUSTOMER_SEARCH');

require_once $StartPath.'/_conf/database.inc';
require_once $StartPath.'/_conf/function.inc';
require_once $StartPath.'/'.$SelMenu.'/_conf/form.inc';

if (key_exists('customer_search', $_POST))       $customer_search        = $_POST['customer_search'];        else $customer_search       = '';
if (key_exists('customer_net', $_POST))          $customer_net           = $_POST['customer_net'];           else $customer_net           = '';
if (key_exists('customer_ip', $_POST))           $customer_ip            = $_POST['customer_ip'];            else $customer_ip           = '';
if (key_exists('customer_kvz', $_POST))          $customer_kvz           = $_POST['customer_kvz'];           else $customer_kvz          = '';
if (key_exists('customer_kvz_addition', $_POST)) $customer_kvz_addition  = $_POST['customer_kvz_addition'];  else $customer_kvz_addition = '';
if (key_exists('customer_line', $_POST))         $customer_line          = $_POST['customer_line'];          else $customer_line         = '';
if (key_exists('customer_version', $_POST) AND ($_POST['customer_version'] == 'on')) $customer_version = 'checked'; else $customer_version = '';
if (key_exists('customer_target', $_POST) AND ($_POST['customer_target']   == 'on')) $customer_target  = 'checked'; else $customer_target  = '';
if (!key_exists('customer_search', $_POST))  $customer_version = 'checked';

// this may come from DefaultController::redirectToCustomerByClientIdAction()
$searchQuery = $request->getSession()->getFlashBag()->get('customerSearch.query');

if (null !== $searchQuery) {
    $customer_search = implode(' ', $searchQuery);
}

$externalCustomerIdFieldName = 'Externe Kundenkennung';
  
if (null !== $exCustField = \Wisotel\Configuration\Configuration::get('externalCustomerIdFieldName')) {
    $externalCustomerIdFieldName = $exCustField;
}
?>

<h1>Kundensuche</h1>
<div id="customerSearch" class="box">
    <h3>Nachname, Firmenname oder Kundennummer suchen:</h3>
    
    <?php

    if (!isset($ro)) {
        $ro = null;
    }
        ?>

        <div class="form">
            <form action="<?php echo $StartURL.'/index.php?menu=customerSearch'; ?>" method="post">
                <button name="search" type="submit" <?php echo $ro; ?>>
                    <img alt="" src="<?php echo $StartURL.'/_img/Search.png'; ?>" height="12px"> Suchen
                </button>&nbsp;
                <label for="customer_searchoption">Suche</label>&nbsp;
                <select id="customer_searchoption" name="customer_searchoption" <?php echo $ro; ?>>
                    <?php setOption($option_search); ?>
                </select>
                <input name="customer_search" type="text" size="40" value="<?php echo $customer_search; ?>" autofocus <?php echo $ro; ?>/>&nbsp;
                <label for="customer_version">Version:</label>&nbsp;
                <input name="customer_version" type="checkbox" <?php echo $customer_version; ?> autofocus <?php echo $ro; ?>/>&nbsp;
                <label for="customer_version"><img alt="" src="<?php echo $StartURL.'/_img/Target.png'; ?>" height="12px"></label>&nbsp;
                <input name="customer_target" type="checkbox" <?php echo $customer_target; ?> autofocus <?php echo $ro; ?>/><br />
                <label for="customer_net">Netz:</label>&nbsp;
                <input name="customer_net" type="text" size="13" value="<?php echo $customer_net; ?>" autofocus <?php echo $ro; ?>/>
                <label for="customer_ip">IP:</label>&nbsp;
                <input name="customer_ip" type="text" size="13" value="<?php echo $customer_ip; ?>" autofocus <?php echo $ro; ?>/>
                <label for="customer_kvz">KVZ:</label>&nbsp;
                <input name="customer_kvz" type="text" size="8" value="<?php echo $customer_kvz; ?>" autofocus <?php echo $ro; ?>/>&nbsp;
                <label for="customer_kvz_addition">KVZ Zusatz:</label>&nbsp;
                <input name="customer_kvz_addition" type="text" size="8" value="<?php echo $customer_kvz_addition; ?>" autofocus <?php echo $ro; ?>/>&nbsp;
                <label for="customer_line">Line:</label>&nbsp;
                <input name="customer_line" type="text" size="8" value="<?php echo $customer_line; ?>" autofocus <?php echo $ro; ?>/>&nbsp;
            </form>
        </div>

        <?php

        if (key_exists('search', $_POST)) {
            if (!$_POST['customer_net'])  $_POST['customer_net']  = "%";
            if (!$_POST['customer_ip'])   $_POST['customer_ip']   = "%";
            if (!$_POST['customer_kvz'])  $_POST['customer_kvz']  = "%";
            if (!$_POST['customer_kvz_addition']) $_POST['customer_kvz_addition']  = "%";
            if (!$_POST['customer_line']) $_POST['customer_line'] = "%";
            
            $count                  = 0;
            $customer_search        = "%".trim(addslashes($_POST['customer_search']))."%";
            $customer_net           = trim(addslashes($_POST['customer_net']));
            $customer_net           = str_replace ('*', '%', $customer_net);
            $customer_ip            = trim(addslashes($_POST['customer_ip']));
            $customer_ip            = str_replace ('*', '%', $customer_ip);
            $customer_kvz           = trim(addslashes(strtoupper($_POST['customer_kvz'])));
            $customer_kvz           = str_replace ('*', '%', $customer_kvz);
            $customer_kvz_addition  = trim(addslashes(strtoupper($_POST['customer_kvz_addition'])));
            $customer_kvz_addition  = str_replace ('*', '%', $customer_kvz_addition);
            $customer_line          = trim(addslashes(strtoupper($_POST['customer_line'])));
            $customer_line          = str_replace ('*', '%', $customer_line);
            
            // Kundensuche
            $sql_sel  = "SELECT cust.`customer_id` as externalCustomerId, `ip_addresses`.`ip_address` AS multi_ip, firstname, lastname, clientid, version, cust.id AS id, company, connection_street, connection_streetno, connection_city, district, pur.purtel_login, cust.ip_address, kvz_standort, kvz_addition, line_identifier, id_string, terminal_serialno, mac_address\n"; 
            $sql_sel  .= "FROM customers AS cust\n";
            $sql_sel  .= "LEFT JOIN networks AS net ON cust.network_id = net.id\n";
            $sql_sel  .= "LEFT JOIN purtel_account AS pur ON cust.id = pur.cust_id\n";
            $sql_sel  .= "LEFT JOIN `ip_addresses` ON `ip_addresses`.`customer_id` = cust.id\n";
            $sql_sel  .= "WHERE IFNULL(id_string, '') LIKE '$customer_net'\n";
            $sql_sel  .= "AND (IFNULL(cust.ip_address, '') LIKE '$customer_ip'\n";
            $sql_sel  .= "OR `ip_addresses`.`ip_address` LIKE '$customer_ip')\n";
            $sql_sel  .= "AND IFNULL(kvz_standort, '') LIKE '$customer_kvz'\n";
            $sql_sel  .= "AND IFNULL(kvz_addition, '') LIKE '$customer_kvz_addition'\n";
            $sql_sel  .= "AND IFNULL(line_identifier, '') LIKE '$customer_line'\n";
            
            if (!key_exists ('customer_version', $_POST)) $sql_sel .= "AND version IS NULL\n";
            
            if (($_POST['customer_searchoption'] == '')) {
                $sql_sel  .= "AND( lastname LIKE '$customer_search'\n";
                $sql_sel  .= " OR company LIKE '$customer_search'\n";
                $sql_sel  .= " OR zipcode LIKE '$customer_search'\n";
                $sql_sel  .= " OR city LIKE '$customer_search'\n";
                $sql_sel  .= " OR street LIKE '$customer_search'\n";
                $sql_sel  .= " OR connection_zipcode LIKE '$customer_search'\n";
                $sql_sel  .= " OR connection_city LIKE '$customer_search'\n";
                $sql_sel  .= " OR connection_street LIKE '$customer_search'\n";
                $sql_sel  .= " OR clientid LIKE '$customer_search'\n";
                $sql_sel  .= " OR pur.purtel_login LIKE '$customer_search'\n";
                $sql_sel  .= " OR tal_dtag_assignment_no LIKE '$customer_search'\n";
                $sql_sel  .= " OR dtag_line LIKE '$customer_search'\n";
                $sql_sel  .= " OR terminal_serialno LIKE '$customer_search'\n";
                $sql_sel  .= " OR wbci_id LIKE '$customer_search'\n";
                $sql_sel  .= " OR mac_address LIKE '$customer_search')\n";
            } else {
                $sql_sel .= "AND ".$_POST['customer_searchoption']." LIKE '$customer_search'\n";
            }
            
            $sql_sel .= "ORDER BY  lastname ASC, firstname ASC, id ASC,company ASC, pur.purtel_login ASC\n";

            $db_result = $db->query($sql_sel);
            $count = $db_result->num_rows;

            echo "<h3>Ergebnis ($count)</h3>\n<div>\n<p><ul>";
            
            $id         = 0;
            $str        = '';
            $purtlogin  = '';
            $purtid     = '';
            
            if ($db_result->num_rows > 0) {
                while ($db_row = $db_result->fetch_assoc()) {
                    if ($purtid != $db_row['purtel_login'] || $id != $db_row['id']) {
                        $purtid =  $db_row['purtel_login']; 

                        if ($id == $db_row['id']) {
                            $purtlogin .= ' '.$db_row['purtel_login'];  
                        } else {
                            $str  = str_replace('###', $purtlogin, $str);
                            
                            echo $str;
                            
                            $id   = $db_row['id'];
                            $purtlogin = $db_row['purtel_login'];
                            $str = "<li><a href='".$StartURL."/index.php?menu=customer&func=vertrag&id=".$db_row['id']."'";

                            if (key_exists ('customer_target', $_POST) AND ($_POST['customer_target'] == 'on')) $str .=  " target='_blank'";

                            $str .=  ">";
                            $str .=  "ID ".$db_row['clientid'];

                            if ($db_row['version']) $str .=  " <span class='text red'>[Version: ".$db_row['version']."]</span> ";
                            
                            if ($db_row['purtel_login'] != '') $str .=  " (###)";

                            $str .=  ": ";

                            if ($db_row['company'] != '') $str .=  $db_row['company']." - ";
                            
                            $str .=  $db_row['lastname'].", ".$db_row['firstname']." | ";
                            $str .=  $db_row['connection_city'].", ".$db_row['district']." | ";
                            
                            if (!empty($db_row['connection_street'])) {
                                $str .= $db_row['connection_street']." ".$db_row['connection_streetno']." | ";
                            }

                            $str .= "<br />".$externalCustomerIdFieldName.": ".$db_row['externalCustomerId'];
                            $str .=  "<br />IP: ".$db_row['ip_address']." | ";
                            
                            $str .=  "KVZ: ".$db_row['kvz_standort']." | ";
                            $str .=  "KVZ Zusatz: ".$db_row['kvz_addition']." | ";
                            $str .=  "Line: ".$db_row['line_identifier']." || ";
                            $str .=  "SN: ".$db_row['terminal_serialno']." |<br>";
                            $str .=  "IP: ".$db_row['multi_ip']." |<br>";
                            $str .=  "MAC: ".$db_row['mac_address'];
                            $str .=  "</a></li>\n";
                        }
                    }
                }

                $db_result->close();
            }

            $str  = str_replace('###', $purtlogin, $str);
            
            echo $str."</ul></p>\n<//div>";

            $db->close();
        }
    
    ?>

</div>
