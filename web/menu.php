<ul id="Menu">
<?php
  foreach ($menu as $menulink => $menuitem) {
    list($mtitle,$micon,$mfunc) = explode(':', $menuitem);
    if ($micon != 'h') { 
      if ($mfunc) {
        echo "<li><a href='".$StartURL."/index.php?menu=".$menulink."&func=".$mfunc."'><span class='ui-icon ".$menuicon[$micon]."'></span>".$mtitle."</a></li>";
      } else {
        echo "<li><a href='".$StartURL."/index.php?menu=".$menulink."'><span class='ui-icon ".$menuicon[$micon]."'></span>".$mtitle."</a></li>";
      }
    }
  }
?>
</ul>
