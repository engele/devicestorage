<?php
  $sql = array (
    // IP-Block
    'iprange' => "
      SELECT *, INET_ATON(start_address) as startint, INET_ATON(end_address) as endint FROM ip_ranges
    ",

    'iprange_single' => "
      SELECT * FROM ip_ranges
      WHERE id = ?
    ",

    'iprange_net' => "
      SELECT net.id, net.id_string, net.name  FROM  ip_ranges AS ipr, ip_ranges_networks AS iprn, networks AS net
      WHERE net.id = iprn.network_id
      AND iprn.ip_range_id = ipr.id
      AND ipr.id = ?
    ",

    // IP-Addresses
    /* counts multiple used IPs multiple times wich results to false count
    'ip_addresses' => "
      SELECT count(*) as used FROM customers 
      WHERE INET_ATON(ip_address) BETWEEN ? AND ?
    ",*/
    'ip_addresses' => "
      SELECT COUNT(DISTINCT `ip_address`) as used FROM customers 
      WHERE INET_ATON(`ip_address`) BETWEEN ? AND ?
    ",
  );
  
$GLOBALS['sql'] = & $sql;
?>
