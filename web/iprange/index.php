<?php

// old code
// new is located in src/AppBundle/Controller/IpRange.php
return;



$this->denyAccessUnlessGranted('ROLE_VIEW_IP_RANGE');

  require_once ($StartPath.'/_conf/database.inc');
  require_once ($StartPath.'/'.$SelMenu.'/_conf/database.inc');

  //############################################################################
  if (key_exists ('func', $_GET) and $_GET['func'] == 'view') {
    if (key_exists ('id', $_GET)) $id = $_GET['id']; else $id = 0;
    $db_iprange = $db->prepare($sql['iprange_single']);
    $db_iprange->bind_param('d', $id);
    $db_iprange->execute();
    $db_iprange_result = $db_iprange->get_result();
    $ip_result = $db_iprange_result->fetch_assoc();
    $db_iprange_result->free();

    $db_iprange_net = $db->prepare($sql['iprange_net']);
    $db_iprange_net->bind_param('d', $id);
    $db_iprange_net->execute();
    $db_iprange_net_result = $db_iprange_net->get_result();
?>
<div class="IpRange box">
  <h3>IP-Range <?php echo $ip_result['name'];?> (<?php echo $ip_result['start_address'].' - '.$ip_result['end_address']; ?>)<br />ist folgenden Netzen zugeordnet</h3>
  <br />
  <table class="IpRange">
    <thead>
    <tr>
      <th>ID</th>
      <th>Netzkennung</th>
      <th>Name</th>
    </tr>
    </thead>
    <tbody>
    <?php
      while ($db_iprange_row = $db_iprange_net_result->fetch_assoc()) {
        echo "<tr>";
        echo "<td class='num'><a href='$StartURL/index.php?menu=network&func=view&id=".$db_iprange_row['id']."'>".$db_iprange_row['id']."</a></td>";
        echo "<td><a href='$StartURL/index.php?menu=network&func=view&id=".$db_iprange_row['id']."'>".$db_iprange_row['id_string']."</a></td>";
        echo "<td><a href='$StartURL/index.php?menu=network&func=view&id=".$db_iprange_row['id']."'>".$db_iprange_row['name']."</a></td>";
        echo "</tr>";
      }
    ?>
    </tbody>
  </table>
</div>

<?php
    $db_iprange_net_result->free();
    $db_iprange_net->close();
    $db_iprange->close();
    $db->close();
  //############################################################################
  } elseif (key_exists ('func', $_GET) and $_GET['func'] == 'edit') {
    $this->denyAccessUnlessGranted('ROLE_EDIT_IP_RANGE');

    if (key_exists ('save', $_POST)) {
      if (!$_POST['IpRangeId']) {
        $sql_save = "INSERT INTO ip_ranges SET ";
        $sql_save .= "start_address='".$_POST['IpRangeStartAddress']."',";
        $sql_save .= "end_address='".$_POST['IpRangeEndAddress']."',";
        $sql_save .= "name='".$_POST['IpRangeName']."',";
        $sql_save .= "subnetmask='".$_POST['IpRangeSubnetmask']."',";
        if (empty($_POST['IpRangeGateway'])) {
            $sql_save .= "gateway=NULL,";
        } else {
            $sql_save .= "gateway='".$_POST['IpRangeGateway']."',";
        }
        $sql_save .= "vlan_ID='".$_POST['IpRangeVlan_ID']."'";
        $db_save_result = $db->query($sql_save);
        if ($db_save_result) {
          $customer_saved = 'wurde hinzugefügt';
        } else {
          $customer_saved = '<span class="text red">konnte nicht hinzugefügt werden!</span>';
        }
        echo "<div class='IpRange box'>";
        echo "<h3>Dieser IP-Block $customer_saved</h3>";
        echo "<div class='IpRange form'><table>";
        echo "<tr><td>Name:</td><td>".$_POST['IpRangeName']."</td></tr>";
        echo "<tr><td>Start-Adresse:</td><td>".$_POST['IpRangeStartAddress']."</td></tr>";
        echo "<tr><td>End-Adresse:</td><td>".$_POST['IpRangeEndAddress']."</td></tr>";
        echo "<tr><td>Subnetzmaske:</td><td>".$_POST['IpRangeSubnetmask']."</td></tr>";
        echo "<tr><td>Gateway:</td><td>".$_POST['IpRangeGateway']."</td></tr>";
        echo "<tr><td>VLAN ID:</td><td>".$_POST['IpRangeVlan_ID']."</td></tr>";
        echo "</table></div></div>";
      } else {
        $sql_save = "UPDATE ip_ranges SET ";
        $sql_save .= "start_address='".$_POST['IpRangeStartAddress']."',";
        $sql_save .= "end_address='".$_POST['IpRangeEndAddress']."',";
        $sql_save .= "name='".$_POST['IpRangeName']."',";
        $sql_save .= "subnetmask='".$_POST['IpRangeSubnetmask']."',";
        if (empty($_POST['IpRangeGateway'])) {
            $sql_save .= "gateway=NULL,";
        } else {
            $sql_save .= "gateway='".$_POST['IpRangeGateway']."',";
        }
        $sql_save .= "vlan_ID='".$_POST['IpRangeVlan_ID']."' ";
        $sql_save .= "WHERE id=".$_POST['IpRangeId'];
        $db_save_result = $db->query($sql_save);
        if ($db_save_result) {
          $customer_saved = 'wurde geändert';
        } else {
          $customer_saved = '<span class="text red">konnte nicht geändert werden!</span>';
        }
        echo "<div class='IpRange box'>";
        echo "<h3>Dieser IP-Block $customer_saved</h3>";
        echo "<div class='IpRange form'><table>";
        echo "<tr><td>Name:</td><td>".$_POST['IpRangeName']."</td></tr>";
        echo "<tr><td>Start-Adresse:</td><td>".$_POST['IpRangeStartAddress']."</td></tr>";
        echo "<tr><td>End-Adresse:</td><td>".$_POST['IpRangeEndAddress']."</td></tr>";
        echo "<tr><td>Subnetzmaske:</td><td>".$_POST['IpRangeSubnetmask']."</td></tr>";
        echo "<tr><td>Gateway:</td><td>".$_POST['IpRangeGateway']."</td></tr>";
        echo "<tr><td>VLAN ID:</td><td>".$_POST['IpRangeVlan_ID']."</td></tr>";
        echo "</table></div></div>";
      }
    } elseif (key_exists ('delete', $_POST)) {
      $sql_delete = "DELETE FROM ip_ranges WHERE id = ".$_POST['IpRangeId'];
      $db_delete_result = $db->query($sql_delete);
      $sql_delete = "DELETE FROM ip_ranges_networks WHERE ip_range_id = ".$_POST['IpRangeId'];
      $db_delete_result = $db->query($sql_delete);
      
      echo "<div class='IpRange box'>";
      echo "<h3>Dieser IP-Block wurde gelöscht</h3>";
      echo "<div class='IpRange form'><table>";
      echo "<tr><td>Name:</td><td>".$_POST['IpRangeName']."</td></tr>";
      echo "<tr><td>Start-Adresse:</td><td>".$_POST['IpRangeStartAddress']."</td></tr>";
      echo "<tr><td>End-Adresse:</td><td>".$_POST['IpRangeEndAddress']."</td></tr>";
      echo "<tr><td>Subnetzmaske:</td><td>".$_POST['IpRangeSubnetmask']."</td></tr>";
      echo "<tr><td>VLAN ID:</td><td>".$_POST['IpRangeVlan_ID']."</td></tr>";
      echo "</table></div></div>";
    } else {
      if (key_exists ('id', $_GET)) $id = $_GET['id']; else $id = 0;
      $db_iprange = $db->prepare($sql['iprange_single']);
      $db_iprange->bind_param('d', $id);
      $db_iprange->execute();
      $db_iprange_result = $db_iprange->get_result();
      $ip_result = $db_iprange_result->fetch_assoc();
      $db_iprange_result->free();
      if (!$ip_result) $ip_result = array();
      if (key_exists ('id', $ip_result))            $ip_db['id']            = $ip_result['id'];             else $ip_db['id']             = 0;
      if (key_exists ('start_address', $ip_result)) $ip_db['start_address'] = $ip_result['start_address'];  else $ip_db['start_address']  = '';
      if (key_exists ('end_address', $ip_result))   $ip_db['end_address']   = $ip_result['end_address'];    else $ip_db['end_address']    = '';
      if (key_exists ('name', $ip_result))          $ip_db['name']          = $ip_result['name'];           else $ip_db['name']           = '';
      if (key_exists ('subnetmask', $ip_result))    $ip_db['subnetmask']    = $ip_result['subnetmask'];     else $ip_db['subnetmask']     = '';
      if (key_exists ('gateway', $ip_result))    $ip_db['gateway']    = $ip_result['gateway'];     else $ip_db['gateway']     = '';
      if (key_exists ('vlan_ID', $ip_result))       $ip_db['vlan_ID']       = $ip_result['vlan_ID'];        else $ip_db['vlan_ID']        = '';
?>
<form id="custForm" action="<?php echo $StartURL.'/index.php?menu=iprange&func=edit'; ?>" method="post">
<div class="IpRange box">
  <h3>IP-Block bearbeiten</h3>
  <div class="IpRange form">
    <input name="IpRangeId" id="IpRangeId" type="hidden" value="<?php echo $ip_db['id']; ?>">
    <p>
      <label for="IpRangeName">Name</label><br>
      <input id="IpRangeName" type="text" size="60" name="IpRangeName" value="<?php echo $ip_db['name']; ?>">
    </p>
    <p>
      <label for="IpRangeStartAddress">Start-Adresse</label><br>
      <input id="IpRangeStartAddress" type="text" size="60" name="IpRangeStartAddress" value="<?php echo $ip_db['start_address']; ?>">
    </p>
    <p>
      <label for="IpRangeEndAddress">End-Adresse</label><br>
      <input id="IpRangeEndAddress" type="text" size="60" name="IpRangeEndAddress" value="<?php echo $ip_db['end_address']; ?>">
    </p>
    <p>
      <label for="IpRangeSubnetmask">Subnetzmaske</label><br>
      <input id="IpRangeSubnetmask" type="text" size="60" name="IpRangeSubnetmask" value="<?php echo $ip_db['subnetmask']; ?>">
    </p>
    <p>
      <label for="IpRangeGateway">Gateway</label><br>
      <input id="IpRangeGateway" type="text" size="60" name="IpRangeGateway" value="<?php echo $ip_db['gateway']; ?>">
    </p>
    <p>
      <label for="IpRangeVlan_ID">VLAN ID</label><br>
      <input id="IpRangeVlan_ID" type="text" size="60" name="IpRangeVlan_ID" value="<?php echo $ip_db['vlan_ID']; ?>">
    </p>
  </div>
  <h3 class="IpSave"></h3>
  <div class="IpSave form">
    <input name='none' id='command' type='hidden' value='yes'>
    <button name="save" onclick="ipSubmit()" type="button"><img alt="" src="<?php echo $StartURL.'/_img/Yes.png'; ?>" height="12px"> Speichern</button>&nbsp;&nbsp;&nbsp;&nbsp;
    <button name="reset" type="reset"><img alt="" src="<?php echo $StartURL.'/_img/Erase.png'; ?>" height="12px"> Zurücksetzten</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <?php if ($id) echo "<button name='delete' onclick='ipDelete()' type='button'><img alt='' src='$StartURL/_img/delete.png' height='12px'> Löschen</button>"; ?>
  </div>
</div>
</form>
<div id="dialog"></div>

<?php
      $db_iprange->close();
      $db->close();
    }
  //############################################################################
  } else {
    $db_iprange = $db->query( $sql['iprange']);
  
    $db_ip_addresses = $db->prepare($sql['ip_addresses']);
    $db_ip_addresses->bind_param('dd', $startint, $endint);
  
?>
<div class="IpRange box">
  <h3>IP-Blöcke</h3>
  <br /><a href='<?php echo $StartURL; ?>/index.php?menu=iprange&func=edit'>Hinzufügen</a><br /><br />
  <table class="IpRange">
    <thead>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>Start-Adresse</th>
      <th>End-Adresse</th>
      <th>Subnetzmaske</th>
      <th>Gateway</th>
      <th>VLAN ID</th>
      <th>Anzahl IPs</th>
      <th>Freie IPs</th>
      <th>Aktionen</th>
    </tr>
    </thead>
    <tbody>
    <?php
      while ($db_iprange_row = $db_iprange->fetch_assoc()) {
        $startint = $db_iprange_row['startint'];
        $endint   = $db_iprange_row['endint'];
        $countip  = $endint - $startint + 1;

        $db_ip_addresses->execute();
        $db_ip_addresses_result = $db_ip_addresses->get_result();
        $ip_result = $db_ip_addresses_result->fetch_assoc();
        $freeip   = $countip - $ip_result['used'];
        $db_ip_addresses_result->free();
        
        echo "<tr>";
        echo "<td class='num'><a href='$StartURL/index.php?menu=iprange&func=view&id=".$db_iprange_row['id']."'>".$db_iprange_row['id']."</a></td>";
        echo "<td><a href='$StartURL/index.php?menu=iprange&func=view&id=".$db_iprange_row['id']."'>".$db_iprange_row['name']."</a></td>";
        echo "<td>".$db_iprange_row['start_address']."</td>";
        echo "<td>".$db_iprange_row['end_address']."</td>";
        echo "<td>".$db_iprange_row['subnetmask']."</td>";
        echo "<td>".$db_iprange_row['gateway']."</td>";
        echo "<td class='Num'>".$db_iprange_row['vlan_ID']."</td>";
        echo "<td class='Num'>".$countip."</td>";
        echo "<td class='Num'>".$freeip."</td>";
        echo "<td>";
        if ($this->get('security.authorization_checker')->isGranted('ROLE_EDIT_IP_RANGE')) {
            echo "<a href='$StartURL/index.php?menu=iprange&func=edit&id=".$db_iprange_row['id']."'>Bearbeiten</a>";
        }
        echo "</td>";
        echo "</tr>";
      }
    ?>
    </tbody>
  </table>
</div>

<?php
    $db_ip_addresses->close();
    $db_iprange->free();
    $db->close();
  }
?>
