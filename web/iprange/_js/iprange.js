function ipSubmit(){
 htmlstr = "Wollen Sie die Änderungen speichern?"
  $("#dialog").html(htmlstr);
  $("#dialog").dialog({
    resizable: true,
    modal: true,
    title: 'IP-Block bearbeiten',
    position: {my: 'top', at: 'top', of: '#Container'},
    height: 300,
    width: 500,
    buttons: {
      'Ja': function () {
        $(this).dialog('close');
        callSubmit(true);
      },
      'Nein': function () {
        $(this).dialog('close');
        callSubmit(false);
      }
    }
  });
}

function callSubmit(save) {
  if (save){
    $("#command").attr('name', 'save');
    $("#custForm").submit();
  }
}

function ipDelete(){
 htmlstr = "Wollen Sie den IP-Block wirklich löschen?"
  $("#dialog").html(htmlstr);
  $("#dialog").dialog({
    resizable: true,
    modal: true,
    title: 'IP-Block löschen',
    position: {my: 'top', at: 'top', of: '#Container'},
    height: 300,
    width: 500,
    buttons: {
      'Ja': function () {
        $(this).dialog('close');
        callDelete(true);
      },
      'Nein': function () {
        $(this).dialog('close');
        callDelete(false);
      }
    }
  });
}

function callDelete(save) {
  if (save){
    $("#command").attr('name', 'delete');
    $("#custForm").submit();
  }
}

$(document).ready(function(){
  $("table.IpRange").tablesorter({
    widgets: ['zebra']
  });
});