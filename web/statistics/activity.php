<?php
  $time                     = time();
  $sum_contracts            = 0;
  $sum_contracts_online     = 0;
  $sum_active               = 0;
  $sum_cancel               = 0;
  $sum_port_send            = 0;
  $sum_port_enter           = 0;
  $sum_tal_order            = 0;
  $sum_connect_info         = 0;
  $sum_tal_cancel           = 0;
  $sum_terminal_ready       = 0;
  $sum_purtel_ticket        = 0;
  $sum_phoneentry_done      = 0;
  $sum_final_purtelproduct  = 0;
  $sum_wbci                 = 0;

  for ($i = 0; $i <= 11; $i++) {
    $mon[$i]  = strftime("%b", $time);
    $mond[$i] = strftime("%m", $time);
    $year[$i] = strftime("%Y", $time);

    // Vertrag manuell erfasst
    $db_contracts  = $db->prepare($sql['contracts']);
    $db_contracts->bind_param('s', $month);
    $month = '%'.$mond[$i].'.'.$year[$i];
    $db_contracts->execute();
    $db_contracts_result = $db_contracts->get_result();
    $db_contracts_row    = $db_contracts_result->fetch_assoc();
    $count_contracts[$i] = $db_contracts_row['count'];
    $db_contracts_result->free_result();
    $sum_contracts += $count_contracts[$i];

    // elektronischen Vertrag prüfen
    $db_contracts_online  = $db->prepare($sql['contracts_online']);
    $db_contracts_online->bind_param('s', $month);
    $month = '%'.$mond[$i].'.'.$year[$i];
    $db_contracts_online->execute();
    $db_contracts_online_result = $db_contracts_online->get_result();
    $db_contracts_online_row    = $db_contracts_online_result->fetch_assoc();
    $count_contracts_online[$i] = $db_contracts_online_row['count'];
    $db_contracts_online_result->free_result();
    $sum_contracts_online += $count_contracts_online[$i];

    $db_active  = $db->prepare($sql['active']);
    $db_active->bind_param('s', $month);
    $month = '%'.$mond[$i].'.'.$year[$i];
    $db_active->execute();
    $db_active_result = $db_active->get_result();
    $db_active_row    = $db_active_result->fetch_assoc();
    $count_active[$i] = $db_active_row['count'];
    $db_active_result->free_result();
    $sum_active += $count_active[$i];

    $db_cancel  = $db->prepare($sql['cancel']);
    $db_cancel->bind_param('s', $month);
    $month = '%'.$mond[$i].'.'.$year[$i];
    $db_cancel->execute();
    $db_cancel_result = $db_cancel->get_result();
    $db_cancel_row    = $db_cancel_result->fetch_assoc();
    $count_cancel[$i] = $db_cancel_row['count'];
    $db_cancel_result->free_result();
    $sum_cancel += $count_cancel[$i];

    $db_port_send  = $db->prepare($sql['port_send']);
    $db_port_send->bind_param('s', $month);
    $month = '%'.$mond[$i].'.'.$year[$i];
    $db_port_send->execute();
    $db_port_send_result = $db_port_send->get_result();
    $db_port_send_row    = $db_port_send_result->fetch_assoc();
    $count_port_send[$i] = $db_port_send_row['count'];
    $db_port_send_result->free_result();
    $sum_port_send += $count_port_send[$i];

    $db_port_enter  = $db->prepare($sql['port_enter']);
    $db_port_enter->bind_param('s', $month);
    $month = '%'.$mond[$i].'.'.$year[$i];
    $db_port_enter->execute();
    $db_port_enter_result = $db_port_enter->get_result();
    $db_port_enter_row    = $db_port_enter_result->fetch_assoc();
    $count_port_enter[$i] = $db_port_enter_row['count'];
    $db_port_enter_result->free_result();
    $sum_port_enter += $count_port_enter[$i];

    $db_tal_order  = $db->prepare($sql['tal_order']);
    $db_tal_order->bind_param('s', $month);
    $month = '%'.$mond[$i].'.'.$year[$i];
    $db_tal_order->execute();
    $db_tal_order_result = $db_tal_order->get_result();
    $db_tal_order_row    = $db_tal_order_result->fetch_assoc();
    $count_tal_order[$i] = $db_tal_order_row['count'];
    $db_tal_order_result->free_result();
    $sum_tal_order += $count_tal_order[$i];

    $db_connect_info  = $db->prepare($sql['connect_info']);
    $db_connect_info->bind_param('s', $month);
    $month = '%'.$mond[$i].'.'.$year[$i];
    $db_connect_info->execute();
    $db_connect_info_result = $db_connect_info->get_result();
    $db_connect_info_row    = $db_connect_info_result->fetch_assoc();
    $count_connect_info[$i] = $db_connect_info_row['count'];
    $db_connect_info_result->free_result();
    $sum_connect_info += $count_connect_info[$i];

    $db_tal_cancel  = $db->prepare($sql['tal_cancel']);
    $db_tal_cancel->bind_param('s', $month);
    $month = '%'.$mond[$i].'.'.$year[$i];
    $db_tal_cancel->execute();
    $db_tal_cancel_result = $db_tal_cancel->get_result();
    $db_tal_cancel_row    = $db_tal_cancel_result->fetch_assoc();
    $count_tal_cancel[$i] = $db_tal_cancel_row['count'];
    $db_tal_cancel_result->free_result();
    $sum_tal_cancel += $count_tal_cancel[$i];

    $db_terminal_ready  = $db->prepare($sql['terminal_ready']);
    $db_terminal_ready->bind_param('s', $month);
    $month = '%'.$mond[$i].'.'.$year[$i];
    $db_terminal_ready->execute();
    $db_terminal_ready_result = $db_terminal_ready->get_result();
    $db_terminal_ready_row    = $db_terminal_ready_result->fetch_assoc();
    $count_terminal_ready[$i] = $db_terminal_ready_row['count'];
    $db_terminal_ready_result->free_result();
    $sum_terminal_ready += $count_terminal_ready[$i];

    $db_purtel_ticket  = $db->prepare($sql['purtel_ticket']);
    $db_purtel_ticket->bind_param('ss', $month, $month);
    $month  = '%'.$mond[$i].'.'.$year[$i];
    $db_purtel_ticket->execute();
    $db_purtel_ticket_result = $db_purtel_ticket->get_result();
    $db_purtel_ticket_row    = $db_purtel_ticket_result->fetch_assoc();
    $count_purtel_ticket[$i] = $db_purtel_ticket_row['count'];
    $db_purtel_ticket_result->free_result();
    $sum_purtel_ticket += $count_purtel_ticket[$i];

    $db_phoneentry_done  = $db->prepare($sql['phoneentry_done']);
    $db_phoneentry_done->bind_param('s', $month);
    $month  = '%'.$mond[$i].'.'.$year[$i];
    $db_phoneentry_done->execute();
    $db_phoneentry_done_result = $db_phoneentry_done->get_result();
    $db_phoneentry_done_row    = $db_phoneentry_done_result->fetch_assoc();
    $count_phoneentry_done[$i] = $db_phoneentry_done_row['count'];
    $db_phoneentry_done_result->free_result();
    $sum_phoneentry_done += $count_phoneentry_done[$i];

    $db_final_purtelproduct  = $db->prepare($sql['final_purtelproduct']);
    $db_final_purtelproduct->bind_param('s', $month);
    $month  = '%'.$mond[$i].'.'.$year[$i];
    $db_final_purtelproduct->execute();
    $db_final_purtelproduct_result = $db_final_purtelproduct->get_result();
    $db_final_purtelproduct_row    = $db_final_purtelproduct_result->fetch_assoc();
    $count_final_purtelproduct[$i] = $db_final_purtelproduct_row['count'];
    $db_final_purtelproduct_result->free_result();
    $sum_final_purtelproduct += $count_final_purtelproduct[$i];

    // WBCI eingestellt
    $db_wbci  = $db->prepare($sql['wbci']);
    $db_wbci->bind_param('s', $month);
    $month = '%'.$mond[$i].'.'.$year[$i];
    $db_wbci->execute();
    $db_wbci_result = $db_wbci->get_result();
    $db_wbci_row    = $db_wbci_result->fetch_assoc();
    $count_wbci[$i] = $db_wbci_row['count'];
    $db_wbci_result->free_result();
    $sum_wbci += $count_wbci[$i];

    $time     = strtotime('-1 month', $time);
  }

  $csv_headline = array ('Leistungen');
  foreach ($mon as $key => $value) {
    $temp = $value.'.'.$year[$key];
    array_push($csv_headline, $temp);
  }
  array_push($csv_headline, 'Gesamt');
  
  $csv_data = array();

  $temp = array();
  $temp[0] = 'Vertrag manuell erfasst';
  foreach ($count_contracts as $value) array_push ($temp, $value);
  array_push ($temp, $sum_contracts);
  array_push ($csv_data, $temp);
  
  $temp = array();
  $temp[0] = 'elektronischen Vertrag prüfen';
  foreach ($count_contracts_online as $value) array_push ($temp, $value);
  array_push ($temp, $sum_contracts_online);
  array_push ($csv_data, $temp);
  
  $temp = array();
  $temp[0] = 'aktivierte Anschaltungen';
  foreach ($count_active as $value) array_push ($temp, $value);
  array_push ($temp, $sum_active);
  array_push ($csv_data, $temp);
  
  $temp = array();
  $temp[0] = 'Vertragskündigungen';
  foreach ($count_cancel as $value) array_push ($temp, $value);
  array_push ($temp, $sum_cancel);
  array_push ($csv_data, $temp);
  
  $temp = array();
  $temp[0] = 'Portierungen an Kd. versenden';
  foreach ($count_port_send as $value) array_push ($temp, $value);
  array_push ($temp, $sum_port_send);
  array_push ($csv_data, $temp);
  
  $temp = array();
  $temp[0] = 'Portierungen hochladen';
  foreach ($count_port_enter as $value) array_push ($temp, $value);
  array_push ($temp, $sum_port_enter);
  array_push ($csv_data, $temp);
  
  $temp = array();
  $temp[0] = 'TAL Bestellungen';
  foreach ($count_tal_order as $value) array_push ($temp, $value);
  array_push ($temp, $sum_tal_order);
  array_push ($csv_data, $temp);
  
  $temp = array();
  $temp[0] = 'TAL Bestätigungen (Kd. informieren)';
  foreach ($count_connect_info as $value) array_push ($temp, $value);
  array_push ($temp, $sum_connect_info);
  array_push ($csv_data, $temp);
  
  $temp = array();
  $temp[0] = 'TAL Kündigungen';
  foreach ($count_tal_cancel as $value) array_push ($temp, $value);
  array_push ($temp, $sum_tal_cancel);
  array_push ($csv_data, $temp);
  
  $temp = array();
  $temp[0] = 'FRITZ!box-Konfiguration';
  foreach ($count_terminal_ready as $value) array_push ($temp, $value);
  array_push ($temp, $sum_terminal_ready);
  array_push ($csv_data, $temp);
  
  $temp = array();
  $temp[0] = 'Störungsticket einstellen';
  foreach ($count_purtel_ticket as $value) array_push ($temp, $value);
  array_push ($temp, $sum_purtel_ticket);
  array_push ($csv_data, $temp);
  
  $temp = array();
  $temp[0] = 'Telefonbucheinträge';
  foreach ($count_phoneentry_done as $value) array_push ($temp, $value);
  array_push ($temp, $sum_phoneentry_done);
  array_push ($csv_data, $temp);
  
  $temp = array();
  $temp[0] = 'Produktwechsel';
  foreach ($count_final_purtelproduct as $value) array_push ($temp, $value);
  array_push ($temp, $sum_final_purtelproduct);
  array_push ($csv_data, $temp);
  
  $temp = array();
  $temp[0] = 'WBCI Auftrag eingestellt';
  foreach ($count_wbci as $value) array_push ($temp, $value);
  array_push ($temp, $sum_wbci);
  array_push ($csv_data, $temp);
  
  $_SESSION['head_activity'] = urlencode(serialize($csv_headline));
  $_SESSION['data_activity'] = urlencode(serialize($csv_data));
  
?>
<div class="StatActivity accordion-single">
  <h3>Leistungen</h3>
  <div>
    <p><a href='<?php echo $StartURL; ?>/statistics/activity_csv.php' target='_blank'>CSV Export</a></p>
    <table class="Stat">
      <tr>
        <th>Leistungen</th>
        <?php
          $y = 0;
          foreach ($mon as $key => $value) {
            echo '<th class="Num">'.$value;
            if ($y != $year[$key]) {
              echo '<br>'.$year[$key];
              $y = $year[$key];
            }
            echo '</th>';
          }
        ?>
        <th class="Num">Gesamt</th>
      </tr>
      <tr>
        <td>Vertrag manuell erfasst</td>
        <?php foreach ($count_contracts as $value) echo '<td class="Num">'.$value.'</td>'; ?>
        <td class="Num"><?php echo $sum_contracts ?></td>  
      </tr>
      <tr>
        <td>elektronischen Vertrag prüfen</td>
        <?php foreach ($count_contracts_online as $value) echo '<td class="Num">'.$value.'</td>'; ?>
        <td class="Num"><?php echo $sum_contracts_online ?></td>  
      </tr>
      <tr>
        <td>aktivierte Anschaltungen</td>
        <?php foreach ($count_active as $value) echo '<td class="Num">'.$value.'</td>'; ?>
        <td class="Num"><?php echo $sum_active ?></td>  
      </tr>
      <tr>
        <td>Vertragskündigungen</td>
        <?php foreach ($count_cancel as $value) echo '<td class="Num">'.$value.'</td>'; ?>
        <td class="Num"><?php echo $sum_cancel ?></td>  
      </tr>
      <tr>
        <td>Portierungen an Kd. versenden</td>
        <?php foreach ($count_port_send as $value) echo '<td class="Num">'.$value.'</td>'; ?>
        <td class="Num"><?php echo $sum_port_send ?></td>  
      </tr>
      <tr>
        <td>Portierungen hochladen</td>
        <?php foreach ($count_port_enter as $value) echo '<td class="Num">'.$value.'</td>'; ?>
        <td class="Num"><?php echo $sum_port_enter ?></td>  
      </tr>
      <tr>
        <td>TAL Bestellungen</td>
        <?php foreach ($count_tal_order as $value) echo '<td class="Num">'.$value.'</td>'; ?>
        <td class="Num"><?php echo $sum_tal_order ?></td>  
      </tr>
      <tr>
        <td>TAL Bestätigungen (Kd. informieren)</td>
        <?php foreach ($count_connect_info as $value) echo '<td class="Num">'.$value.'</td>'; ?>
        <td class="Num"><?php echo $sum_connect_info ?></td>  
      </tr> 
      <tr>
        <td>TAL Kündigungen</td>
        <?php foreach ($count_tal_cancel as $value) echo '<td class="Num">'.$value.'</td>'; ?>
        <td class="Num"><?php echo $sum_tal_cancel ?></td>  
      </tr>
      <tr>
        <td>FRITZ!box-Konfiguration</td>
        <?php foreach ($count_terminal_ready as $value) echo '<td class="Num">'.$value.'</td>'; ?>
        <td class="Num"><?php echo $sum_terminal_ready ?></td>  
      </tr>
      <tr>
        <td>Störungsticket einstellen</td>
        <?php foreach ($count_purtel_ticket as $value) echo '<td class="Num">'.$value.'</td>'; ?>
        <td class="Num"><?php echo $sum_purtel_ticket ?></td>  
      </tr>
      <tr>
        <td>Telefonbucheinträge</td>
        <?php foreach ($count_phoneentry_done as $value) echo '<td class="Num">'.$value.'</td>'; ?>
        <td class="Num"><?php echo $sum_phoneentry_done ?></td>  
      </tr>
      <tr>
        <td>Produktwechsel</td>
        <?php foreach ($count_final_purtelproduct as $value) echo '<td class="Num">'.$value.'</td>'; ?>
        <td class="Num"><?php echo $sum_final_purtelproduct ?></td>  
      </tr>
      <tr>
        <td>WBCI Auftrag eingestellt</td>
        <?php foreach ($count_wbci as $value) echo '<td class="Num">'.$value.'</td>'; ?>
        <td class="Num"><?php echo $sum_wbci ?></td>  
      </tr>
    </table>
  </div>
</div>
