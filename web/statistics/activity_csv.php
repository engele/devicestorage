<?php
  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename=leistungen_'.date ("YmdHis").'.csv');  

  session_start();

  $csv_handle = fopen('php://output', 'w');
  if (isset($_SESSION['head_activity'])) {
    $headline = urldecode ($_SESSION['head_activity']);
    $headline = unserialize ($headline);
    foreach ($headline AS $key => $value) {
      $headline[$key] = utf8_decode(str_replace("<br />", " ", $value));
    }
    fputcsv($csv_handle, $headline, ';');
  }
  if (isset($_SESSION['data_activity'])) {
    $csv = urldecode ($_SESSION['data_activity']);
    $csv = unserialize ($csv);
    foreach ($csv as $index) {
      $data = array();
      foreach ($index as $value) {
        array_push ($data, $value);
      }
      fputcsv($csv_handle, $data, ';');
    }
  }
  fclose ($csv_handle);
?>
