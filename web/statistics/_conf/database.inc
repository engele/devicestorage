<?php
$sql = array (
    'stat_contract' => "
        SELECT COUNT(c.id) as count FROM customers c INNER JOIN networks n ON n.id = c.network_id
        WHERE c.version IS NULL
        AND c.clientid NOT LIKE '0000_.%'
        AND (c.contract_version != '' AND c.contract_version IS NOT NULL)
        AND (c.wisocontract_canceled_date = '' OR c.wisocontract_canceled_date IS NULL)
        AND (n.`no_stat` IS NULL OR n.`no_stat` = 0)
    ",
    
    'stat_active_cust' => "
        SELECT COUNT(c.id) as count FROM customers c INNER JOIN networks n ON n.id = c.network_id
        WHERE c.version IS NULL
        AND connection_activation_date IS NOT NULL
        AND connection_activation_date != ''
        AND (c.wisocontract_canceled_date = '' OR c.wisocontract_canceled_date IS NULL)
        AND (n.`no_stat` IS NULL OR n.`no_stat` = 0)
    ",
    
    'stat_int' => "
        SELECT COUNT(c.id) as count FROM customers c INNER JOIN networks n ON n.id = c.network_id
        WHERE c.version IS NULL
        AND c.clientid LIKE '0000_.%'
        AND c.prospect_supply_status = 'with_supply'
        AND (n.`no_stat` IS NULL OR n.`no_stat` = 0)
    ",

    'stat_abrogated' => "
        SELECT COUNT(c.id) as count FROM customers c INNER JOIN networks n ON n.id = c.network_id
        WHERE c.version IS NULL
        AND c.clientid NOT LIKE '0000_.%'
        AND (c.contract_version != '' AND c.contract_version IS NOT NULL)
        AND c.wisocontract_canceled_date != ''
        AND c.wisocontract_canceled_date IS NOT NULL
        AND (n.`no_stat` IS NULL OR n.`no_stat` = 0)
    ",

    // network
    'stat_network01' => "
        SELECT count(*) as count FROM customers
        WHERE version IS NULL
        AND clientid NOT LIKE '0000_.%'
        AND (contract_version != '' AND contract_version IS NOT NULL)
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND network_id = ?
    ",

    'stat_network02' => "
        SELECT count(*) as count FROM customers
        WHERE version IS NULL
        AND connection_activation_date IS NOT NULL
        AND connection_activation_date != ''
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND network_id = ?
    ",

    'stat_network03' => "
        SELECT count(*) as count FROM customers
        WHERE version IS NULL
        AND clientid LIKE '0000_.%'
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND prospect_supply_status = 'with_supply'
        AND network_id = ?
    ",

    'stat_month_net' => "
        SELECT count(*) as count FROM customers
        WHERE version IS NULL
        AND connection_activation_date LIKE ?
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND prospect_supply_status != 'without_supply'
        AND network_id = ?
    ",
    // district 
    'stat_district01' => "
        SELECT count(*) as count FROM customers
        WHERE version IS NULL
        AND (contract_version != '' AND contract_version IS NOT NULL)
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND district = ?
    ",

    'stat_district02' => "
        SELECT count(*) as count FROM customers
        WHERE version IS NULL
        AND (connection_activation_date != '' AND connection_activation_date IS NOT NULL)
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND district = ?
    ",

    'stat_district03' => "
        SELECT count(*) as count FROM customers
        WHERE version IS NULL
        AND clientid LIKE '0000_.%'
        AND (contract_version = '' OR contract_version IS NULL)
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND district = ?
    ",
     
     'stat_month_district' => "
        SELECT count(*) as count FROM customers
        WHERE version IS NULL
        AND connection_activation_date LIKE ?
        AND (wisocontract_canceled_date = '' OR wisocontract_canceled_date IS NULL)
        AND prospect_supply_status != 'without_supply'
        AND district = ?
    ",
   // city and district and network
    'stat_district' => "
        SELECT * FROM connection_city AS c, connection_district AS d
        WHERE d.city_id = c.id
        ORDER BY d.order ASC
    ",

    'stat_networks' => "
        SELECT * FROM networks
        WHERE no_stat = 0
        OR    no_stat IS NULL
        ORDER BY id_string ASC
    ",

    // Leistungen
    // Vertrag manuell erfasst
    'contracts' => "
        SELECT count(*) as count FROM customers 
        WHERE contract_received_date LIKE ?
        AND (recruited_by != 'semcona' OR recruited_by IS NULL)
    ",
    // elektronischen Vertrag prüfen
    'contracts_online' => "
        SELECT count(*) as count FROM customers 
        WHERE contract_received_date LIKE ?
        AND recruited_by = 'semcona'
    ",
    // Aktivschaltungen
    'active' => "
        SELECT count(*) as count FROM customers 
        WHERE connection_activation_date LIKE ?
    ",
    // Kündigungen
    'cancel' => "
        SELECT count(*) as count FROM customers 
        WHERE wisocontract_canceled_date LIKE ?
    ",
    // Portierungen an Kd. versenden
    'port_send' => "
        SELECT count(*) as count FROM customers 
        WHERE ventelo_port_letter_date LIKE ?
    ",
    // Portierungen hochladen
    'port_enter' => "
        SELECT count(*) as count FROM customers 
        WHERE ventelo_purtel_enter_date LIKE ?
    ",
    // TAL Bestellungen
    'tal_order' => "
        SELECT count(*) as count FROM customers 
        WHERE tal_order_date LIKE ?
    ",
    // TAL Bestätigungen (Kd. informieren)
    'connect_info' => "
        SELECT count(*) as count FROM customers 
        WHERE customer_connect_info_date LIKE ?
    ",
    // TAL Kündigungen
    'tal_cancel' => "
        SELECT count(*) as count FROM customers 
        WHERE tal_cancel_date LIKE ?
    ",
    // FRITZ!box-Konfiguration
    'terminal_ready' => "
        SELECT count(*) as count FROM customers 
        WHERE terminal_ready LIKE ?
    ",
    // Störungsticket einstellen
    'purtel_ticket' => "
        SELECT count(*) as count FROM customers 
        WHERE dtagbluckage_sended_date LIKE ?
        OR purtel_bluckage_sended_date LIKE ?
    ",
    // Telefonbucheinträge
    'phoneentry_done' => "
        SELECT count(*) as count FROM customers 
        WHERE phoneentry_done_date LIKE ?
    ",
    // Produktwechsel
    'final_purtelproduct' => "
        SELECT count(*) as count FROM customers 
        WHERE final_purtelproduct_date LIKE ?
    ",
    // WBCI eingestellt
    'wbci' => "
        SELECT count(*) as count FROM customers 
        WHERE wbci_eingestellt_am LIKE ?
    ",
);
?>
