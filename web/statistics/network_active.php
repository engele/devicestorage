<?php
$count  = array();
$countd = array();

$db_stat_month_net = $db->prepare($sql['stat_month_net']);
$db_stat_month_net->bind_param('si', $month, $network_id);

for ($i = 1; $i <= 12; $i++) {
    $month = sprintf("%%%02d.%4d", $i, $stat_date);
    foreach ($netdb as $net) {
        $network_id = $net['id'];
        $network_name = $net['id_string'].' - '.$net['name'];
        $db_stat_month_net->execute();
        $db_stat_month_net_result = $db_stat_month_net->get_result();
        $db_stat_month_net_row = $db_stat_month_net_result->fetch_assoc();
        $count[$network_name][$i] = $db_stat_month_net_row['count'];
        $count[$network_name][0]  += $db_stat_month_net_row['count'];
        $sum[$i]                  += $db_stat_month_net_row['count'];
        $sum[0]                   += $db_stat_month_net_row['count'];
        $db_stat_month_net_result->free_result();
    }
}
 
// close DB
$db_stat_month_net->close();

// Statistics of connections
$db_stat_month_district = $db->prepare($sql['stat_month_district']);
$db_stat_month_district->bind_param('ss', $month, $district);

$db_district = $db->query($sql['stat_district']);
$districtdb = [];

if (0 === $db->errno) {
    $districtdb  = $db_district->fetch_all(MYSQLI_ASSOC);
    $db_district->close();
}

for ($i = 1; $i <= 12; $i++) {
    $month = sprintf("%%%02d.%4d", $i, $stat_date);
    foreach ($districtdb as $districttemp) {
        $district = $districttemp['district'];
        $city     = $districttemp['city'];

        $db_stat_month_district->execute();
        $db_stat_month_district_result = $db_stat_month_district->get_result();
        $db_stat_month_district_row    = $db_stat_month_district_result->fetch_assoc();
        $countd[$city][$district][$i]  = $db_stat_month_district_row['count'];
        $countd[$city][$district][0]   += $db_stat_month_district_row['count'];
        $countd[$city][$i]             += $db_stat_month_district_row['count'];
        $countd[$city][0]              += $db_stat_month_district_row['count'];
        $sumd[$i]                      += $db_stat_month_district_row['count'];
        $sumd[0]                       += $db_stat_month_district_row['count'];
        $db_stat_month_district_result->free_result();
    }
}
 
// close DB
$db_stat_month_district->close();

?>
<div class="StatActive accordion-single">
    <form id="StatisticsForm" action="<?php echo $StartURL.'/index.php?menu=statistics'; ?>" method="post" enctype="multipart/form-data">
    <h3>Aktivschaltungen <select id="StatisticsYear" name="StatisticsYear">
    <?php 
    for ($i = 0; $i < 10; $i++) {
        $value = $act_date - $i;
        echo ($value == $stat_date)
            ? "<option value='$value' selected='selected'>$value</option>"
            : "<option value='$value'>$value</option>";
    }
    ?>
    </select>
    </h3>
    </form>
<?php

    if (!empty($sum)) {
        ?>  
        <div>
            <table class="Stat">
                <tr>
                    <th></th>
                    <th class="Num">Jan</th>
                    <th class="Num">Feb</th>
                    <th class="Num">Mär</th>
                    <th class="Num">Apr</th>
                    <th class="Num">Mai</th>
                    <th class="Num">Jun</th>
                    <th class="Num">Jul</th>
                    <th class="Num">Aug</th>
                    <th class="Num">Sep</th>
                    <th class="Num">Okt</th>
                    <th class="Num">Nov</th>
                    <th class="Num">Dez</th>
                    <th class="Num"><b>Gesamt</b></th>
                </tr>
         
        <?php
        echo "<tr>\n";
        echo "<td><b>Gesamt Netz</b></td>";
        echo "<td class='Num'><b>".$sum[1]."</b></td>\n";
        echo "<td class='Num'><b>".$sum[2]."</b></td>\n";
        echo "<td class='Num'><b>".$sum[3]."</b></td>\n";
        echo "<td class='Num'><b>".$sum[4]."</b></td>\n";
        echo "<td class='Num'><b>".$sum[5]."</b></td>\n";
        echo "<td class='Num'><b>".$sum[6]."</b></td>\n";
        echo "<td class='Num'><b>".$sum[7]."</b></td>\n";
        echo "<td class='Num'><b>".$sum[8]."</b></td>\n";
        echo "<td class='Num'><b>".$sum[9]."</b></td>\n";
        echo "<td class='Num'><b>".$sum[10]."</b></td>\n";
        echo "<td class='Num'><b>".$sum[11]."</b></td>\n";
        echo "<td class='Num'><b>".$sum[12]."</b></td>\n";
        echo "<td class='Num'><b>".$sum[0]."</b></td>\n";
        echo "</tr>\n";

        foreach ($count as $net => $value) {
            echo "<tr>\n";
            echo "<td>".$net."</td>\n";    
            echo "<td class='Num'>".$count[$net][1]."</td>\n";
            echo "<td class='Num'>".$count[$net][2]."</td>\n";
            echo "<td class='Num'>".$count[$net][3]."</td>\n";
            echo "<td class='Num'>".$count[$net][4]."</td>\n";
            echo "<td class='Num'>".$count[$net][5]."</td>\n";
            echo "<td class='Num'>".$count[$net][6]."</td>\n";
            echo "<td class='Num'>".$count[$net][7]."</td>\n";
            echo "<td class='Num'>".$count[$net][8]."</td>\n";
            echo "<td class='Num'>".$count[$net][9]."</td>\n";
            echo "<td class='Num'>".$count[$net][10]."</td>\n";
            echo "<td class='Num'>".$count[$net][11]."</td>\n";
            echo "<td class='Num'>".$count[$net][12]."</td>\n";
            echo "<td class='Num'><b>".$count[$net][0]."</b></td>\n";
            echo "</tr>\n";
        }          

        echo "<tr>\n";
        echo "<td>&nbsp;</td>";
        echo "<td class='Num'>&nbsp;</td>\n";
        echo "<td class='Num'>&nbsp;</td>\n";
        echo "<td class='Num'>&nbsp;</td>\n";
        echo "<td class='Num'>&nbsp;</td>\n";
        echo "<td class='Num'>&nbsp;</td>\n";
        echo "<td class='Num'>&nbsp;</td>\n";
        echo "<td class='Num'>&nbsp;</td>\n";
        echo "<td class='Num'>&nbsp;</td>\n";
        echo "<td class='Num'>&nbsp;</td>\n";
        echo "<td class='Num'>&nbsp;</td>\n";
        echo "<td class='Num'>&nbsp;</td>\n";
        echo "<td class='Num'>&nbsp;</td>\n";
        echo "<td class='Num'>&nbsp;</td>\n";
        echo "</tr>\n";

        echo "<tr>\n";
        echo "<td><b>Gesamt Ort</b></td>";
        echo "<td class='Num'><b>".$sumd[1]."</b></td>\n";
        echo "<td class='Num'><b>".$sumd[2]."</b></td>\n";
        echo "<td class='Num'><b>".$sumd[3]."</b></td>\n";
        echo "<td class='Num'><b>".$sumd[4]."</b></td>\n";
        echo "<td class='Num'><b>".$sumd[5]."</b></td>\n";
        echo "<td class='Num'><b>".$sumd[6]."</b></td>\n";
        echo "<td class='Num'><b>".$sumd[7]."</b></td>\n";
        echo "<td class='Num'><b>".$sumd[8]."</b></td>\n";
        echo "<td class='Num'><b>".$sumd[9]."</b></td>\n";
        echo "<td class='Num'><b>".$sumd[10]."</b></td>\n";
        echo "<td class='Num'><b>".$sumd[11]."</b></td>\n";
        echo "<td class='Num'><b>".$sumd[12]."</b></td>\n";
        echo "<td class='Num'><b>".$sumd[0]."</b></td>\n";
        echo "</tr>\n";

        foreach ($countd as $city => $dist) {
            echo "<tr>\n";
            echo "<td><b>".$city."</b></td>\n";    
            echo "<td class='Num'><b>".$countd[$city][1]."</b></td>\n";
            echo "<td class='Num'><b>".$countd[$city][2]."</b></td>\n";
            echo "<td class='Num'><b>".$countd[$city][3]."</b></td>\n";
            echo "<td class='Num'><b>".$countd[$city][4]."</b></td>\n";
            echo "<td class='Num'><b>".$countd[$city][5]."</b></td>\n";
            echo "<td class='Num'><b>".$countd[$city][6]."</b></td>\n";
            echo "<td class='Num'><b>".$countd[$city][7]."</b></td>\n";
            echo "<td class='Num'><b>".$countd[$city][8]."</b></td>\n";
            echo "<td class='Num'><b>".$countd[$city][9]."</b></td>\n";
            echo "<td class='Num'><b>".$countd[$city][10]."</b></td>\n";
            echo "<td class='Num'><b>".$countd[$city][11]."</b></td>\n";
            echo "<td class='Num'><b>".$countd[$city][12]."</b></td>\n";
            echo "<td class='Num'><b>".$countd[$city][0]."</b></td>\n";
            echo "</tr>\n";
            foreach ($dist as $key => $value) {
                if (!preg_match('/^\d+/', $key)) {
                    echo "<tr>\n";
                    echo "<td>".$key."</td>\n";    
                    echo "<td class='Num'>".$dist[$key][1]."</td>\n";
                    echo "<td class='Num'>".$dist[$key][2]."</td>\n";
                    echo "<td class='Num'>".$dist[$key][3]."</td>\n";
                    echo "<td class='Num'>".$dist[$key][4]."</td>\n";
                    echo "<td class='Num'>".$dist[$key][5]."</td>\n";
                    echo "<td class='Num'>".$dist[$key][6]."</td>\n";
                    echo "<td class='Num'>".$dist[$key][7]."</td>\n";
                    echo "<td class='Num'>".$dist[$key][8]."</td>\n";
                    echo "<td class='Num'>".$dist[$key][9]."</td>\n";
                    echo "<td class='Num'>".$dist[$key][10]."</td>\n";
                    echo "<td class='Num'>".$dist[$key][11]."</td>\n";
                    echo "<td class='Num'>".$dist[$key][12]."</td>\n";
                    echo "<td class='Num'>".$dist[$key][0]."</td>\n";
                    echo "</tr>\n";
                }
            }
        }          

        ?>
    </table>
  </div>
<?php
}
unset($citydb);
unset($districtdb);
unset($city);
unset($district);
unset($count);
?>
</div>