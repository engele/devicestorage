<?php

require_once ($StartPath.'/_conf/database.inc');
require_once ($StartPath.'/_conf/function.inc');
require_once ($StartPath.'/'.$SelMenu.'/_conf/database.inc');

$act_date = date ('Y');

if (key_exists('StatisticsYear',$_POST)) {
    $stat_date = $_POST['StatisticsYear'];
} else {
    $stat_date = $act_date;
}               
//-------------------------------------------------------------------------  

$statisticsViews = [
    'ROLE_VIEW_STATISTICS_TOTAL' => [
        $StartPath.'/'.$SelMenu.'/list.php',
    ],
    'ROLE_VIEW_STATISTICS_NETWORK' => [
        $StartPath.'/'.$SelMenu.'/network.php',
    ],
    'ROLE_VIEW_STATISTICS_ACTIVATION' => [
        $StartPath.'/'.$SelMenu.'/network_active.php',
    ],
];

?>

<h1 class="StatList">Statistiken</h1>

<?php

$authorizationChecker = $this->get('security.authorization_checker');

foreach ($statisticsViews as $requiredRole => $views) {
    if (!$authorizationChecker->isGranted($requiredRole)) {
        continue;
    }

    foreach ($views as $templateFile) {
        require_once $templateFile;
    }
}

$db->close();

?>
