<?php
$count  = array();
$countd = array();
// network statistic
$db_stat_network01 = $db->prepare($sql['stat_network01']);
$db_stat_network02 = $db->prepare($sql['stat_network02']);
$db_stat_network03 = $db->prepare($sql['stat_network03']);

$db_stat_network01->bind_param('i', $network_id);
$db_stat_network02->bind_param('i', $network_id);
$db_stat_network03->bind_param('i', $network_id);

$db_net = $db->query($sql['stat_networks']);
$netdb  = $db_net->fetch_all(MYSQLI_ASSOC);
$db_net->close();

$sum1 = 0;
$sum2 = 0;
$sum3 = 0;
foreach ($netdb as $net) {
    $network_id   = $net['id'];
    $network_name = $net['id_string'].' - '.$net['name'];

    $db_stat_network01->execute();
    $db_stat_network01_result = $db_stat_network01->get_result();
    $db_stat_network01_row = $db_stat_network01_result->fetch_assoc();
    $count[$network_name][1] = $db_stat_network01_row['count'];
    $db_stat_network01_result->free_result();
    $sum1 += $db_stat_network01_row['count'];

    $db_stat_network02->execute();
    $db_stat_network02_result = $db_stat_network02->get_result();
    $db_stat_network02_row = $db_stat_network02_result->fetch_assoc();
    $count[$network_name][2] = $db_stat_network02_row['count'];
    $db_stat_network02_result->free_result();
    $sum2 += $db_stat_network02_row['count'];

    $db_stat_network03->execute();
    $db_stat_network03_result = $db_stat_network03->get_result();
    $db_stat_network03_row = $db_stat_network03_result->fetch_assoc();
    $count[$network_name][3] = $db_stat_network03_row['count'];
    $db_stat_network03_result->free_result();
    $sum3 += $db_stat_network03_row['count'];
}
// close DB
$db_stat_network01->close();
$db_stat_network02->close();
$db_stat_network03->close();

// distric statistic
$db_stat_district01 = $db->prepare($sql['stat_district01']);
$db_stat_district02 = $db->prepare($sql['stat_district02']);
$db_stat_district03 = $db->prepare($sql['stat_district03']);

$db_stat_district01->bind_param('s', $district);
$db_stat_district02->bind_param('s', $district);
$db_stat_district03->bind_param('s', $district);

$db_district = $db->query($sql['stat_district']);
$districtdb = [];

if (0 === $db->errno) {
    $districtdb = $db_district->fetch_all(MYSQLI_ASSOC);
    $db_district->close();
}

$sumd1 = 0;
$sumd2 = 0;
$sumd3 = 0;

foreach ($districtdb as $district_temp) {
    $district = $district_temp['district'];
    $city     = $district_temp['city'];

    $db_stat_district01->execute();
    $db_stat_district01_result   = $db_stat_district01->get_result();
    $db_stat_district01_row      = $db_stat_district01_result->fetch_assoc();
    $countd[$city][$district][1] = $db_stat_district01_row['count'];
    $db_stat_district01_result->free_result();
    $countd[$city][1] += $db_stat_district01_row['count'];
    $sumd1            += $db_stat_district01_row['count'];

    $db_stat_district02->execute();
    $db_stat_district02_result   = $db_stat_district02->get_result();
    $db_stat_district02_row      = $db_stat_district02_result->fetch_assoc();
    $countd[$city][$district][2] = $db_stat_district02_row['count'];
    $db_stat_district02_result->free_result();
    $countd[$city][2] += $db_stat_district02_row['count'];
    $sumd2            += $db_stat_district02_row['count'];

    $db_stat_district03->execute();
    $db_stat_district03_result   = $db_stat_district03->get_result();
    $db_stat_district03_row      = $db_stat_district03_result->fetch_assoc();
    $countd[$city][$district][3] = $db_stat_district03_row['count'];
    $db_stat_district03_result->free_result();
    $countd[$city][3] += $db_stat_district03_row['count'];
    $sumd3            += $db_stat_district03_row['count'];
}
// close DB
$db_stat_district01->close();
$db_stat_district02->close();
$db_stat_district03->close();


?>

<div class="StatNetwork accordion-single">
<h3>Statistik Netz</h3>

<div>
    <table class="Stat">
        <tr>
            <th>Netz</th>
            <th class="Num">Kunden (mit Vertrag)</th>
            <th class="Num">aktive Kunden</th>
            <th class="Num">Interessenten</th>
            <th class="Num">Anzahl gesamt</th>
      </tr>
<?php

$sumcount = $sum1 + $sum3;
echo "<tr>\n";
echo "<td><b>Gesamt Netz</b></td>\n";
echo "<td class='Num'><b>".$sum1."</b></td>\n";
echo "<td class='Num'><b>".$sum2."</b></td>\n";
echo "<td class='Num'><b>".$sum3."</b></td>\n";
echo "<td class='Num'><b>".$sumcount."</b></td>\n";
echo "</tr>";

foreach ($count as $net => $value) {
    $sumcount = $count[$net][1] + $count[$net][3];
    echo "<tr>\n";
    echo "<td>".$net."</td>\n";
    echo "<td class='Num'>".$count[$net][1]."</td>\n";
    echo "<td class='Num'>".$count[$net][2]."</td>\n";
    echo "<td class='Num'>".$count[$net][3]."</td>\n";
    echo "<td class='Num'>".$sumcount."</td>\n";
    echo "</tr>";
}

echo "<tr>\n";
echo "<td>&nbsp;</td>\n";
echo "<td class='Num'>&nbsp;</td>\n";
echo "<td class='Num'>&nbsp;</td>\n";
echo "<td class='Num'>&nbsp;</td>\n";
echo "<td class='Num'>&nbsp;</td>\n";
echo "</tr>";

$sumdcount = $sumd1 + $sumd3;
echo "<tr>\n";
echo "<td><b>Gesamt Ort</b></td>\n";
echo "<td class='Num'><b>".$sumd1."</b></td>\n";
echo "<td class='Num'><b>".$sumd2."</b></td>\n";
echo "<td class='Num'><b>".$sumd3."</b></td>\n";
echo "<td class='Num'><b>".$sumdcount."</b></td>\n";
echo "</tr>";

foreach ($countd as $city => $dist) {
    $sumdcount = $countd[$city][1] + $countd[$city][3];
    echo "<tr>\n";
    echo "<td><b>".$city."</b></td>\n";
    echo "<td class='Num'><b>".$countd[$city][1]."</b></td>\n";
    echo "<td class='Num'><b>".$countd[$city][2]."</b></td>\n";
    echo "<td class='Num'><b>".$countd[$city][3]."</b></td>\n";
    echo "<td class='Num'><b>".$sumdcount."</b></td>\n";
    echo "</tr>";
    foreach ($dist as $key => $value) {
        if (!preg_match('/^\d+/', $key)) {
            $sumdcount = $dist[$key][1] + $dist[$key][3];
            echo "<tr>\n";
            echo "<td> - ".$key."</td>\n";
            echo "<td class='Num'>".$dist[$key][1]."</td>\n";
            echo "<td class='Num'>".$dist[$key][2]."</td>\n";
            echo "<td class='Num'>".$dist[$key][3]."</td>\n";
            echo "<td class='Num'>".$sumdcount."</td>\n";
            echo "</tr>";
        }
    }
}

?>      
    </table>
</div>
<?php
unset($citydb);
unset($districtdb);
unset($city);
unset($district);
unset($count); 
?>
</div>
