<?php
  // general statistics
  // contract
  $db_stat_contract = $db->query($sql['stat_contract']);
  $stat_contract_count = $db_stat_contract->fetch_assoc();
  $stat_contract_count = $stat_contract_count['count'];
  $db_stat_contract->free();

  // active customers
  $db_stat_active_cust = $db->query($sql['stat_active_cust']);
  $stat_active_cust_count = $db_stat_active_cust->fetch_assoc();
  $stat_active_cust_count = $stat_active_cust_count['count'];
  $db_stat_active_cust->free();

  // interested persons
  $db_stat_int = $db->query($sql['stat_int']);
  $stat_int_count = $db_stat_int->fetch_assoc();
  $stat_int_count = $stat_int_count['count'];
  $db_stat_int->free();

  // contract abrogate
  $db_stat_abrogated = $db->query($sql['stat_abrogated']);
  $stat_abrogated_count = $db_stat_abrogated->fetch_assoc();
  $stat_abrogated_count = $stat_abrogated_count['count'];
  $db_stat_abrogated->free();
  
  // supplieable
  $stat_supply_count = $stat_contract_count + $stat_int_count;
?>

<div class="StatList accordion-single">
  <h3>Statistik gesamt</h3>

  <div>
    <p>
      <span class="fixNum"><?php echo $stat_contract_count; ?></span>Kunden (mit Vertrag)
      <hr>
      <span class="fixNum"><?php echo $stat_active_cust_count; ?></span>davon aktive Kunden
      <hr>
      <span class="fixNum"><?php echo $stat_int_count; ?></span>Interessenten (versorgbar)
      <hr>
      <span class="fixNum"><?php echo '<b>'.$stat_supply_count.'</b>'; ?></span><b>Anzahl Kunden und Interessenten (versorgbar)</b>
      <hr>
      <span class="fixNum"><?php echo $stat_abrogated_count; ?></span>Kunden (Vertrag gekündigt)
      <hr>
    </p>
  </div>
</div>
