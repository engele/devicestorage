<?php
  $sql = array (
    // network
    'network_single' => "
      SELECT id_string AS netid_string, name AS netname FROM networks
      WHERE id = ?
    ",
    // Location
    'location' => "
      SELECT loc.network_id, loc.name AS locname, kvz_prefix, cid_suffix_start, cid_suffix_end, net.id_string AS netid_string, net.name AS netname FROM locations AS loc, networks AS net
      WHERE loc.network_id = net.id
      AND loc.id = ?
    ",
    // KVZ Zuordnung
    'lsaKvzPartition' => "
      SELECT * FROM lsa_kvz_partitions
      WHERE location_id = ?
    ",

    'defective_lsaKvzPartition' => "
      SELECT count(*) AS used FROM defective_lsa_pins
      WHERE lsa_kvz_partition_id = ?
    ",
    // Karten
    'card' => "
      SELECT cards.id, cards.name AS name, cards.port_amount, cards.Line AS Line, typ.name AS type FROM cards, card_types AS typ
      WHERE cards.card_type_id = typ.id
      AND location_id = ?
      ORDER BY cards.name
    ",

    'defective_card' => "
      SELECT count(*) AS used FROM defective_card_ports
      WHERE card_id = ?
    ",
    // Customer
    'used_lsaKvzPartition' => "
      SELECT COUNT(DISTINCT lsa_pin) AS used FROM customers
      WHERE lsa_kvz_partition_id = ?
      AND lsa_pin > 0
    ",

    /*'used_card' => "
      SELECT COUNT(DISTINCT line_identifier) AS used FROM customers
      WHERE card_id = ?
    ",*/
    'used_card' => "
      SELECT COUNT(`dslam_port`) AS used FROM customers
      WHERE card_id = ? AND `dslam_port` IS NOT NULL AND `dslam_port` != ''
    ",

    /**
     * Older "versions" of this customer may also be connected to the same pin.
     * Using "ORDER BY `version` ASC" to ensure we select latest customer account first!
     */
    'used_lsaKvzPartition_details' => "
        SELECT `id`, `lsa_pin`, `connection_activation_date`, `patch_date` FROM `customers`
        WHERE `lsa_kvz_partition_id` = ? 
        ORDER BY `version` ASC
    ",

    'defective_lsaKvzPartition_details' => "
      SELECT `id`, `value` FROM `defective_lsa_pins`
      WHERE `lsa_kvz_partition_id` = ?
    ",

    'defective_card_details' => "
      SELECT `id`, `value` FROM `defective_card_ports`
      WHERE `card_id` = ?
    ",

    /**
     * Older "versions" of this customer may also be connected to the same line_identifier.
     * Using "ORDER BY `version` ASC" to ensure we select latest customer account first!
     */
    'used_card_details' => "
      SELECT `id`, `line_identifier`, `connection_activation_date`, `patch_date` FROM `customers`
      WHERE `card_id` = ? 
      ORDER BY `version` ASC
    ",

    'delete_defective_card_port' => "
      DELETE FROM `defective_card_ports`
      WHERE `card_id` = ? AND `value` = ?
    ",

    'add_defective_card_port' => "
      INSERT INTO `defective_card_ports` (`card_id`, `value`) VALUES (?, ?)
    ",

    'delete_defective_lsaKvzPartition_pin' => "
      DELETE FROM `defective_lsa_pins`
      WHERE `lsa_kvz_partition_id` = ? AND `value` = ?
    ",

    'add_defective_lsaKvzPartition_pin' => "
      INSERT INTO `defective_lsa_pins` (`lsa_kvz_partition_id`, `value`) VALUES (?, ?)
    ",

    // locations
    'insert_locations' => "
        INSERT INTO `locations` (`network_id`, `name`, `kvz_prefix`, `cid_suffix_start`, `cid_suffix_end`, 
            `vlan_pppoe`, `vlan_iptv`, `vlan_acs`, `vectoring`, `logon_port`, `dslam_ip_adress`, `logon_id`, 
            `logon_pw`, `dslam_conf`, `dslam_type`, `vlan_pppoe_local`, `vlan_iptv_local`, `vlan_acs_local`, 
            `acs_type`, `dslam_ip_real`, `dslam_router`, `netz`, `vlan_dcn`, `prozessor`, `dslam_software_release`, 
            `default_ont_firmware_release`, `pppoe_type`
        ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
    ",

    'select_location_by_id' => "
        SELECT * FROM `locations` WHERE `id` = ?
    ",

    'update_location_by_id' => "
        UPDATE `locations` SET `network_id` = ?, `name` = ?, `kvz_prefix` = ?, `cid_suffix_start` = ?, `cid_suffix_end` = ?, 
            `vlan_pppoe` = ?, `vlan_iptv` = ?, `vlan_acs` = ?, `vectoring` = ?, `logon_port` = ?, `dslam_ip_adress` = ?, `logon_id` = ?, 
            `logon_pw` = ?, `dslam_conf` = ?, `dslam_type` = ?, `vlan_pppoe_local` = ?, `vlan_iptv_local` = ?, `vlan_acs_local` = ?, 
            `acs_type` = ?, `dslam_ip_real` = ?, `dslam_router` = ?, `netz` = ?, `vlan_dcn` = ?, `prozessor` = ?, `dslam_software_release` = ?, 
            `default_ont_firmware_release` = ?, `pppoe_type` = ?
        WHERE `id` = ?
    ",
  );

$GLOBALS['sql'] = & $sql;
?>
