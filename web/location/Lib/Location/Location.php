<?php

/**
 *
 */

namespace location\Lib\Location;

require_once __DIR__.'/../../../administration/vendor/propertyreader/propertyreader/PropertyReader.php';

use PropertyReader\PropertyReader;

/**
 * Location entity
 */
class Location
{
    /**
     * Database sql array
     * 
     * @var array
     */
    protected $sql;

    /**
     * Mysqli database connection
     * 
     * @var \mysqli
     */
    protected $db;

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var integer
     */
    protected $networkId;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $kvzPrefix;

    /**
     * @var integer
     */
    protected $cidSuffixStart;

    /**
     * @var integer
     */
    protected $cidSuffixEnd;

    /**
     * @var string
     */
    protected $vlanPppoe;

    /**
     * @var string
     */
    protected $vlanIptv;

    /**
     * @var string
     */
    protected $vlanAcs;

    /**
     * @var boolean
     */
    protected $vectoring;

    /**
     * @var string
     */
    protected $logonPort;

    /**
     * @var string
     */
    protected $dslamIpAddress;

    /**
     * @var string
     */
    protected $logonId;

    /**
     * @var string
     */
    protected $logonPw;

    /**
     * @var boolean
     */
    protected $dslamConf;

    /**
     * @var string
     */
    protected $dslamType;

    /**
     * @var string
     */
    protected $dslamSoftwareRelease;

    /**
     * @var string
     */
    protected $defaultOntFirmwareRelease;

    /**
     * @var string
     */
    protected $vlanPppoeLocal;

    /**
     * @var string
     */
    protected $vlanIptvLocal;

    /**
     * @var string
     */
    protected $vlanAcsLocal;

    /**
     * @var string
     */
    protected $acsType;

    /**
     * @var string
     */
    protected $dslamIpReal;

    /**
     * @var string
     */
    protected $dslamRouter;

    /**
     * @var string
     */
    protected $netz;

    /**
     * @var string
     */
    protected $vlanDcn;

    /**
     * @var string
     */
    protected $prozessor;

    /**
     * @var string
     */
    protected $pppoeType;

    /**
     * Constructor
     * 
     * @param \mysqli $db
     * @param array   $sql
     */
    public function __construct(\mysqli $db, $sql)
    {
        $this->db = $db;
        $this->sql = $sql;
    }

    /**
     * Alias for constructor
     * 
     * @param \mysqli $db
     * @param array   $sql
     * 
     * @return Location
     */
    public static function newInstance(\mysqli $db, $sql)
    {
        return new self($db, $sql);
    }

    /**
     * Fetch location by id
     * 
     * @param integer $id
     * @param \mysqli $db
     * @param array   $sql
     * 
     * @return Location
     */
    public static function fetchById($id, \mysqli $db, $sql)
    {
        $statement = $db->prepare($sql['select_location_by_id']);
        $statement->bind_param('i', $id);
        $statement->execute();

        $result = $statement->get_result();

        $data = $result->fetch_assoc();

        $result->free();

        $location = new self($db, $sql);

        $location
            ->setNetworkId($data['network_id'])
            ->setName($data['name'])
            ->setKvzPrefix($data['kvz_prefix'])
            ->setCidSuffixStart($data['cid_suffix_start'])
            ->setCidSuffixEnd($data['cid_suffix_end'])
            ->setVlanPppoe($data['vlan_pppoe'])
            ->setVlanIptv($data['vlan_iptv'])
            ->setVlanAcs($data['vlan_acs'])
            ->setVectoring($data['vectoring'])
            ->setLogonPort($data['logon_port'])
            ->setDslamIpAddress($data['dslam_ip_adress'])
            ->setLogonId($data['logon_id'])
            ->setLogonPw($data['logon_pw'])
            ->setDslamConf($data['dslam_conf'])
            ->setDslamType($data['dslam_type'])
            ->setVlanPppoeLocal($data['vlan_pppoe_local'])
            ->setVlanIptvLocal($data['vlan_iptv_local'])
            ->setVlanAcsLocal($data['vlan_acs_local'])
            ->setAcsType($data['acs_type'])
            ->setDslamIpReal($data['dslam_ip_real'])
            ->setDslamRouter($data['dslam_router'])
            ->setNetz($data['netz'])
            ->setVlanDcn($data['vlan_dcn'])
            ->setProzessor($data['prozessor'])
            ->setDslamSoftwareRelease($data['dslam_software_release'])
            ->setDefaultOntFirmwareRelease($data['default_ont_firmware_release'])
            ->setPppoeType($data['pppoe_type'])
        ;

        $id = & PropertyReader::newInstance()->read($location, 'id');
        $id = $data['id'];

        return $location;
    }

    /**
     * Persisting data to database
     * 
     * @return void
     */
    public function persist()
    {
        if (null === $this->id) {
            return $this->persistNew();
        }

        return $this->persistUpdate();
    }

    /**
     * Update data in database
     * 
     * @return void
     */
    private function persistUpdate()
    {
        $statement = $this->db->prepare($this->sql['update_location_by_id']);
        
        $statement->bind_param('issiisssissssisssssssssssssi',
            $this->networkId,
            $this->name,
            $this->kvzPrefix,
            $this->cidSuffixStart,
            $this->cidSuffixEnd,
            $this->vlanPppoe,
            $this->vlanIptv,
            $this->vlanAcs,
            $this->vectoring,
            $this->logonPort,
            $this->dslamIpAddress,
            $this->logonId,
            $this->logonPw,
            $this->dslamConf,
            $this->dslamType,
            $this->vlanPppoeLocal,
            $this->vlanIptvLocal,
            $this->vlanAcsLocal,
            $this->acsType,
            $this->dslamIpReal,
            $this->dslamRouter,
            $this->netz,
            $this->vlanDcn,
            $this->prozessor,
            $this->dslamSoftwareRelease,
            $this->defaultOntFirmwareRelease,
            $this->pppoeType,
            $this->id
        );

        if (!$statement->execute()) {
            die(var_dump( $this->db ));
            throw new \Exception($this->db->error);
        }

        $statement->close();
    }

    /**
     * Persist data as new entry
     * 
     * @return void
     */
    private function persistNew()
    {
        $statement = $this->db->prepare($this->sql['insert_locations']);

        $statement->bind_param('issiisssissssisssssssssssss',
            $this->networkId,
            $this->name,
            $this->kvzPrefix,
            $this->cidSuffixStart,
            $this->cidSuffixEnd,
            $this->vlanPppoe,
            $this->vlanIptv,
            $this->vlanAcs,
            $this->vectoring,
            $this->logonPort,
            $this->dslamIpAddress,
            $this->logonId,
            $this->logonPw,
            $this->dslamConf,
            $this->dslamType,
            $this->vlanPppoeLocal,
            $this->vlanIptvLocal,
            $this->vlanAcsLocal,
            $this->acsType,
            $this->dslamIpReal,
            $this->dslamRouter,
            $this->netz,
            $this->vlanDcn,
            $this->prozessor,
            $this->dslamSoftwareRelease,
            $this->defaultOntFirmwareRelease,
            $this->pppoeType
        );

        if (!$statement->execute()) {
            throw new \Exception($statement->error);
        }

        $this->id = $statement->insert_id;

        $statement->close();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return integer
     */
    public function getNetworkId()
    {
        return $this->networkId;
    }

    /**
     * @param integer $networkId
     * 
     * @return Location
     */
    public function setNetworkId($networkId)
    {
        $this->networkId = $networkId;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * 
     * @return Location
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getKvzPrefix()
    {
        return $this->kvzPrefix;
    }

    /**
     * @param string $kvzPrefix
     * 
     * @return Location
     */
    public function setKvzPrefix($kvzPrefix)
    {
        $this->kvzPrefix = $kvzPrefix;

        return $this;
    }

    /**
     * @return integer
     */
    public function getCidSuffixStart()
    {
        return $this->cidSuffixStart;
    }

    /**
     * @param integer $cidSuffixStart
     * 
     * @return Location
     */
    public function setCidSuffixStart($cidSuffixStart)
    {
        $this->cidSuffixStart = $cidSuffixStart;

        return $this;
    }

    /**
     * @return integer
     */
    public function getCidSuffixEnd()
    {
        return $this->cidSuffixEnd;
    }

    /**
     * @param integer $cidSuffixEnd
     * 
     * @return Location
     */
    public function setCidSuffixEnd($cidSuffixEnd)
    {
        $this->cidSuffixEnd = $cidSuffixEnd;

        return $this;
    }

    /**
     * @return string
     */
    public function getVlanPppoe()
    {
        return $this->vlanPppoe;
    }

    /**
     * @param string $vlanPppoe
     * 
     * @return Location
     */
    public function setVlanPppoe($vlanPppoe)
    {
        $this->vlanPppoe = $vlanPppoe;

        return $this;
    }

    /**
     * @return string
     */
    public function getVlanIptv()
    {
        return $this->vlanIptv;
    }

    /**
     * @param string $vlanIptv
     * 
     * @return Location
     */
    public function setVlanIptv($vlanIptv)
    {
        $this->vlanIptv = $vlanIptv;

        return $this;
    }

    /**
     * @return string
     */
    public function getVlanAcs()
    {
        return $this->vlanAcs;
    }

    /**
     * @param string $vlanAcs
     * 
     * @return Location
     */
    public function setVlanAcs($vlanAcs)
    {
        $this->vlanAcs = $vlanAcs;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getVectoring()
    {
        return $this->vectoring;
    }

    /**
     * @param boolean $vectoring
     * 
     * @return Location
     */
    public function setVectoring($vectoring)
    {
        $this->vectoring = $vectoring;

        return $this;
    }

    /**
     * @return string
     */
    public function getLogonPort()
    {
        return $this->logonPort;
    }

    /**
     * @param string $logonPort
     * 
     * @return Location
     */
    public function setLogonPort($logonPort)
    {
        $this->logonPort = $logonPort;

        return $this;
    }

    /**
     * @return string
     */
    public function getDslamIpAddress()
    {
        return $this->dslamIpAddress;
    }

    /**
     * @param string $dslamIpAddress
     * 
     * @return Location
     */
    public function setDslamIpAddress($dslamIpAddress)
    {
        $this->dslamIpAddress = $dslamIpAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getLogonId()
    {
        return $this->logonId;
    }

    /**
     * @param string $logonId
     * 
     * @return Location
     */
    public function setLogonId($logonId)
    {
        $this->logonId = $logonId;

        return $this;
    }

    /**
     * @return string
     */
    public function getLogonPw()
    {
        return $this->logonPw;
    }

    /**
     * @param string $logonPw
     * 
     * @return Location
     */
    public function setLogonPw($logonPw)
    {
        $this->logonPw = $logonPw;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getDslamConf()
    {
        return $this->dslamConf;
    }

    /**
     * @param boolean $dslamConf
     * 
     * @return Location
     */
    public function setDslamConf($dslamConf)
    {
        $this->dslamConf = $dslamConf;

        return $this;
    }

    /**
     * @return string
     */
    public function getDslamType()
    {
        return $this->dslamType;
    }

    /**
     * @param string $dslamType
     * 
     * @return Location
     */
    public function setDslamType($dslamType)
    {
        $this->dslamType = $dslamType;

        return $this;
    }

    /**
     * @return string
     */
    public function getVlanPppoeLocal()
    {
        return $this->vlanPppoeLocal;
    }

    /**
     * @param string $vlanPppoeLocal
     * 
     * @return Location
     */
    public function setVlanPppoeLocal($vlanPppoeLocal)
    {
        $this->vlanPppoeLocal = $vlanPppoeLocal;

        return $this;
    }

    /**
     * @return string
     */
    public function getVlanIptvLocal()
    {
        return $this->vlanIptvLocal;
    }

    /**
     * @param string $vlanIptvLocal
     * 
     * @return Location
     */
    public function setVlanIptvLocal($vlanIptvLocal)
    {
        $this->vlanIptvLocal = $vlanIptvLocal;

        return $this;
    }

    /**
     * @return string
     */
    public function getVlanAcsLocal()
    {
        return $this->vlanAcsLocal;
    }

    /**
     * @param string $vlanAcsLocal
     * 
     * @return Location
     */
    public function setVlanAcsLocal($vlanAcsLocal)
    {
        $this->vlanAcsLocal = $vlanAcsLocal;

        return $this;
    }

    /**
     * @return string
     */
    public function getAcsType()
    {
        return $this->acsType;
    }

    /**
     * @param string $acsType
     * 
     * @return Location
     */
    public function setAcsType($acsType)
    {
        $this->acsType = $acsType;

        return $this;
    }

    /**
     * @return string
     */
    public function getDslamIpReal()
    {
        return $this->dslamIpReal;
    }

    /**
     * @param string $dslamIpReal
     * 
     * @return Location
     */
    public function setDslamIpReal($dslamIpReal)
    {
        $this->dslamIpReal = $dslamIpReal;

        return $this;
    }

    /**
     * @return string
     */
    public function getDslamRouter()
    {
        return $this->dslamRouter;
    }

    /**
     * @param string $dslamRouter
     * 
     * @return Location
     */
    public function setDslamRouter($dslamRouter)
    {
        $this->dslamRouter = $dslamRouter;

        return $this;
    }

    /**
     * @return string
     */
    public function getNetz()
    {
        return $this->netz;
    }

    /**
     * @param string $netz
     * 
     * @return Location
     */
    public function setNetz($netz)
    {
        $this->netz = $netz;

        return $this;
    }

    /**
     * @return string
     */
    public function getVlanDcn()
    {
        return $this->vlanDcn;
    }

    /**
     * @param string $vlanDcn
     * 
     * @return Location
     */
    public function setVlanDcn($vlanDcn)
    {
        $this->vlanDcn = $vlanDcn;

        return $this;
    }

    /**
     * @return string
     */
    public function getProzessor()
    {
        return $this->prozessor;
    }

    /**
     * @param string $prozessor
     * 
     * @return Location
     */
    public function setProzessor($prozessor)
    {
        $this->prozessor = $prozessor;

        return $this;
    }

    /**
     * @return string
     */
    public function getDslamSoftwareRelease()
    {
        return $this->dslamSoftwareRelease;
    }

    /**
     * @param string $dslamSoftwareRelease
     * 
     * @return Location
     */
    public function setDslamSoftwareRelease($dslamSoftwareRelease)
    {
        $this->dslamSoftwareRelease = $dslamSoftwareRelease;

        return $this;
    }

    /**
     * @return string
     */
    public function getDefaultOntFirmwareRelease()
    {
        return $this->defaultOntFirmwareRelease;
    }

    /**
     * @param string $defaultOntFirmwareRelease
     * 
     * @return Location
     */
    public function setDefaultOntFirmwareRelease($defaultOntFirmwareRelease)
    {
        $this->defaultOntFirmwareRelease = $defaultOntFirmwareRelease;

        return $this;
    }

    /**
     * @return string
     */
    public function getPppoeType()
    {
        return $this->pppoeType;
    }

    /**
     * @param string $pppoeType
     * 
     * @return Location
     */
    public function setPppoeType($pppoeType)
    {
        $this->pppoeType = $pppoeType;

        return $this;
    }
}

?>
