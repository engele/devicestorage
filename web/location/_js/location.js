/**
 * Handles click-action on numbers within the graphical view.
 * Select multiple numbers and send them to the server.
 * Or send a single one via Ajax.
 */
var GraphicalViewSelect = {
    /**
     * @member {boolean}
     */
    CTRL_IS_PUSHED: false,
    /**
     * cache-variable
     *
     * @member {object}
     */
    multipleSelected: {},

    /**
     * Constructor
     *
     * @return void
     */
    init: function()
    {
        this.setEventHandler();
    },

    /**
     * @return void
     */
    setEventHandler: function()
    {
        $(document).on('keydown', function(event) {
            if (17 === event.keyCode) {
                GraphicalViewSelect.CTRL_IS_PUSHED = true;
            }
        }).on('keyup', function(event) {
            switch (event.keyCode) {
                case 17:
                    GraphicalViewSelect.CTRL_IS_PUSHED = false;
                    break;

                case 27:
                    GraphicalViewSelect.abortMultiSelect();
                    break;
            }
        });

        $('.graphical-view-table input[name="toggle-defective-status"]').on('click', this.numberClickHandler);
        $('.multi-select-button-container button').on('click', this.multiSelectedSubmit);
    },

    /**
     * Deselect all selected elements and reset cache-variable
     *
     * @return void
     */
    abortMultiSelect: function()
    {
        $('.graphical-view-table-container form .active').removeClass('active');
        GraphicalViewSelect.multipleSelected = {};
        $('.multi-select-button-container').addClass('hide');
    },

    /**
     * @param {object} event
     * @return {boolean} always false
     */
    numberClickHandler: function(event)
    {
        event.preventDefault();

        if (GraphicalViewSelect.CTRL_IS_PUSHED) {
            GraphicalViewSelect.initMultiSelect(this);
        } else {
            GraphicalViewSelect.defaultNumberClick(this);
        }

        return false;
    },

    /**
     * Send's data via ajax (post) to server
     *
     * @param {object} elem
     * @return void
     */
    defaultNumberClick: function(elem)
    {
        var formData = $(elem).closest('form.toggle-defective-form');

        formData = formData.serialize()+'&'+$(elem).attr('name')+'='+$(elem).val()+'&ajax=true';

        $.post('', formData, function(result) {
            result = JSON.parse(result);

            if (result.status === 'success') {
                if (false === $(elem).parent().hasClass('bg-red')) {
                    $(elem).parent().removeClass('bg-grey').addClass('bg-red');
                } else {
                    window.location.reload();
                }

                return;
            }

            window.alert('Failure');
        }).fail(function() {
            window.alert('Request failed');
        });
    },

    /**
     * @param {object} elem
     * @return void
     */
    initMultiSelect: function(elem)
    {
        var formData = $(elem).closest('form.toggle-defective-form');
        /**
         * whatId = Either locationId or cardId
         */
        var whatId = $(formData).find('input[name="toggle-defective-what-id"]').val();

        /**
         * Only allow multiple selections within the same "block"
         */
        if (undefined !== this.multipleSelected.whatId && whatId !== this.multipleSelected.whatId) {
            return false;
        }

        /**
         * Is this the first element?
         * Remember element and also show submit-button
         */
        if (undefined === this.multipleSelected.whatId) {
            this.multipleSelected.whatId = whatId;
            this.multipleSelected.what = $(formData).find('input[name="toggle-defective-what"]').val();
            this.multipleSelected.counter = 0;

            // show submit-button
            $(formData).closest('.graphical-view-table-container')
                .find('.multi-select-button-container')
                .removeClass('hide')
            ;
        }

        /**
         * Select or deselect element?
         */
        if ($(elem).hasClass('active')) {
            $(elem).removeClass('active');

            delete this.multipleSelected[$(elem).val()];
            this.multipleSelected.counter--;
        } else {
            $(elem).addClass('active');

            this.multipleSelected[$(elem).val()] = true;
            this.multipleSelected.counter++;
        }

        /**
         * Last element deselected?
         * Hide submit-button
         */
        if (this.multipleSelected.counter < 1) {
            this.multipleSelected = {}; // reset cache-variable

            $(formData).closest('.graphical-view-table-container')
                .find('.multi-select-button-container')
                .addClass('hide')
            ;
        }
    },

    /**
     * @return boolean always false
     */
    multiSelectedSubmit: function()
    {
        /**
         * nothing to do if cache-variable is empty
         */
        if (undefined === GraphicalViewSelect.multipleSelected.whatId) {
            return false;
        }

        var html = '';

        for (e in GraphicalViewSelect.multipleSelected) {
            if (!isNaN(e)) {
                html += '<input type="hidden" name="toggle-defective-status[]" value="'+e+'" />';
            }
        }

        $(this).parent().append('<form action="'+window.location.href+'&dummy=true" method="post" class="hide" id="multi-select-hidden-form">'
            +'<input type="hidden" name="toggle-defective-what" value="'+GraphicalViewSelect.multipleSelected.what+'" />'
            +'<input type="hidden" name="toggle-defective-what-id" value="'+GraphicalViewSelect.multipleSelected.whatId+'" />'
            +html
            +'</form>'
        );

        $('#multi-select-hidden-form').submit();

        return false;
    }
};


function locationSubmit(){
 htmlstr = "Wollen Sie die Änderungen speichern?"
  $("#dialog").html(htmlstr);
  $("#dialog").dialog({
    resizable: true,
    modal: true,
    title: 'Standort bearbeiten',
    position: {my: 'top', at: 'top', of: '#Container'},
    height: 300,
    width: 500,
    buttons: {
      'Ja': function () {
        $(this).dialog('close');
        callSubmit(true);
      },
      'Nein': function () {
        $(this).dialog('close');
        callSubmit(false);
      }
    }
  });
}

function callSubmit(save) {
  if (save){
    $("#command").attr('name', 'save');
    $("#locationForm").submit();
  }
}

$(document).ready(function(){
    $("table.Location").tablesorter({
        widgets: ['zebra']
    });
    
    GraphicalViewSelect.init();
});