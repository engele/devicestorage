<?php

$this->denyAccessUnlessGranted('ROLE_VIEW_LOCATION');

require_once $StartPath.'/_conf/database.inc';
require_once $StartPath.'/'.$SelMenu.'/_conf/database.inc';
/*require_once $StartPath.'/'.$SelMenu.'/Lib/Location/Location.php';

use location\Lib\Location\Location;*/
use AppBundle\Entity\Location\Location;
use AppBundle\Entity\Location\Card;
use AppBundle\Entity\Location\CardPort;

//############################################################################
if (key_exists ('func', $_GET) and $_GET['func'] == 'view') {
    if (key_exists ('id', $_GET)) $id = $_GET['id']; else $id = 0;

    // Toggle defective-status of card or lsa-kvz-partition pin/port
    if (isset($_POST['toggle-defective-status']) 
        && isset($_POST['toggle-defective-what'])
        && isset($_POST['toggle-defective-what-id'])) {
        
        // what = Either location or card
        $what = $_POST['toggle-defective-what'];
        // whatId = Either locationId or cardId
        $whatId = (int) $_POST['toggle-defective-what-id'];

        $query = array('delete' => null, 'add' => null);

        // Get relevant querys
        switch ($what) {
            case 'card':
                $query['delete'] = $sql['delete_defective_card_port'];
                $query['add'] = $sql['add_defective_card_port'];
                break;

            case 'lsa-kvz-partition':
                $query['delete'] = $sql['delete_defective_lsaKvzPartition_pin'];
                $query['add'] = $sql['add_defective_lsaKvzPartition_pin'];
                break;
        }

        if (!is_array($_POST['toggle-defective-status'])) {
            $_POST['toggle-defective-status'] = array($_POST['toggle-defective-status']);
        }

        // Toggle status
        foreach ($_POST['toggle-defective-status'] as $pinNumber) {
            $pinNumber = (int) $pinNumber;

            // Delete from defective-table.
            $statement = $db->prepare($query['delete']);
            $statement->bind_param('ii', $whatId, $pinNumber);
            $statement->execute();

            // No entry deleted?
            // Than add an entry to defective-table.
            if (0 === $statement->affected_rows) {
                $statement = $db->prepare($query['add']);
                $statement->bind_param('ii', $whatId, $pinNumber);
                $result = $statement->execute();

                if (!(true === $result)) {
                    exit('{"status": "failed"}');
                }

                unset($result);
            }

            $statement->close();
        }

        unset($query, $pinNumber, $what, $whatId, $statement);

        if (isset($_POST['ajax'])) {
            exit('{"status": "success"}');
        } elseif (isset($_GET['dummy'])) {
            header('Location: '.str_replace('&dummy=true', '', $_SERVER['REQUEST_URI']));
            exit;
        }
    }

    $db_location = $db->prepare($sql['location']);
    $db_location->bind_param('d', $id);
    $db_location->execute();
    $db_location_result = $db_location->get_result();
    $loc_result = $db_location_result->fetch_assoc();
    $db_location_result->free();

    $db_lsaKvzPartition = $db->prepare($sql['lsaKvzPartition']);
    $db_lsaKvzPartition->bind_param('d', $id);
    $db_lsaKvzPartition->execute();
    $db_lsaKvzPartition_result = $db_lsaKvzPartition->get_result();

    $db_used_lsaKvzPartition = $db->prepare($sql['used_lsaKvzPartition']);
    $db_used_lsaKvzPartition->bind_param('d', $lid);

    $db_defective_lsaKvzPartition = $db->prepare($sql['defective_lsaKvzPartition']);
    $db_defective_lsaKvzPartition->bind_param('d', $lid);

    $db_card = $db->prepare($sql['card']);
    $db_card->bind_param('d', $id);
    $db_card->execute();
    $db_card_result = $db_card->get_result();

    $db_used_card = $db->prepare($sql['used_card']);
    $db_used_card->bind_param('d', $cid);

    $db_defective_card = $db->prepare($sql['defective_card']);
    $db_defective_card->bind_param('d', $cid);
    /*
    $db_net_localtion = $db->prepare($sql['net_location']);
    $db_net_localtion->bind_param('d', $id);
    $db_net_localtion->execute();
    $db_net_localtion_result = $db_net_localtion->get_result();
    */
    $lsaKvzPartitions = array();
    $db_used_lsaKvzPartition_details = $db->prepare($sql['used_lsaKvzPartition_details']);
    $db_used_lsaKvzPartition_details->bind_param('i', $lid);
    $db_defective_lsaKvzPartition_details = $db->prepare($sql['defective_lsaKvzPartition_details']);
    $db_defective_lsaKvzPartition_details->bind_param('i', $lid);

    $cards = array();
    $db_used_card_details = $db->prepare($sql['used_card_details']);
    $db_used_card_details->bind_param('i', $cid);
    $db_defective_cards_details = $db->prepare($sql['defective_card_details']);
    $db_defective_cards_details->bind_param('i', $cid);
    ?>

<h1 class="Location"><?php echo $loc_result['locname']; ?> (<?php echo $loc_result['netid_string']; ?> - <?php echo $loc_result['netname']; ?>)</h1>
<div class="Location accordion">
  <h3>LSA-KVz Zuordnungen</h3>
  <div>
    <table class="Location">
      <thead>
      <tr>
        <th>ID</th>
        <th>KVz Bezeichnung</th>
        <th>VLAN</th>
        <th>VLAN 1</th>
        <th>VLAN 2</th>
        <th>VLAN 3</th>
        <th>DPBO</th>
        <th>ESEL</th>
        <th>LSA-Kontakte Anzahl</th>
        <th>Freie LSA Kontakte</th>
        <th title="Aktionen">Akt.</th>
      </tr>
      </thead>
      <tbody>
      <?php
        while ($db_lsaKvzPartition_row = $db_lsaKvzPartition_result->fetch_assoc()) {
          $lsaKvzPartitions[] = $db_lsaKvzPartition_row;

          $lid = $db_lsaKvzPartition_row['id'];
   
          $db_used_lsaKvzPartition->execute();
          $db_used_lsaKvzPartition_result = $db_used_lsaKvzPartition->get_result();
          $ulsa_result = $db_used_lsaKvzPartition_result->fetch_assoc();

          $db_defective_lsaKvzPartition->execute();
          $db_defective_lsaKvzPartition_result = $db_defective_lsaKvzPartition->get_result();
          $dlsa_result = $db_defective_lsaKvzPartition_result->fetch_assoc();

          $freelsa = $db_lsaKvzPartition_row['pin_amount'] - $ulsa_result['used'] - $dlsa_result['used'];
          $db_defective_lsaKvzPartition_result->free();
          $db_used_lsaKvzPartition_result->free();
          
          echo "<tr>";
          echo "<td class='num'>".$db_lsaKvzPartition_row['id']."</td>";
          echo "<td>".$db_lsaKvzPartition_row['kvz_name']."</td>";
          echo "<td>".$db_lsaKvzPartition_row['default_VLAN']."</td>";
          echo "<td>".$db_lsaKvzPartition_row['VLAN1']."</td>";
          echo "<td>".$db_lsaKvzPartition_row['VLAN2']."</td>";
          echo "<td>".$db_lsaKvzPartition_row['VLAN3']."</td>";
          echo "<td>".$db_lsaKvzPartition_row['dpbo']."</td>";
          echo "<td>".$db_lsaKvzPartition_row['esel_wert']."</td>";
          echo "<td class='num'>".$db_lsaKvzPartition_row['pin_amount']."</td>";
          echo "<td class='num'>$freelsa</td>";
          echo "<td><a href='$StartURL/index.php?menu=lsaKvzPartition&func=edit&id=".$db_lsaKvzPartition_row['id']."&lid=".$id."'>Ändern</a></td>";
          echo "</tr>";
        }
      ?>
      </tbody>
    </table>
    <br /><a href='<?php echo $StartURL; ?>/index.php?menu=lsaKvzPartition&func=edit&lid=<?php echo $id; ?>'>LSA-KVz Zuordnung hinzufügen</a><br /><br />
    <br /><br /><a href="<?php echo urldecode($this->generateUrl('network-view', ['id' => $loc_result['network_id']])); ?>"><button type="button"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px"> Zurück</button></a>
  </div>

  <h3>DSLAM-Karten</h3>
  <div>

    <?php
    // has this location at least one card with type 'dependent-MDU' and at least one card port with attribute 'olt-connected'
    // and is that port connected to other exsiting port
    $query = $db->prepare("SELECT cp.`id` FROM `card_ports` cp 
        INNER JOIN `card_ports_to_card_port_attributes` cpa ON cpa.`card_port_id` = cp.`id` 
        INNER JOIN `card_port_attributes` cp_a ON cp_a.`id` = cpa.`card_port_attribute_id` 
        INNER JOIN `cards` c ON c.`id` = cp.`card_id` 
        INNER JOIN `card_types` ct ON ct.`id` = c.`card_type_id` 
        WHERE ct.`name` = 'dependent-MDU' 
        AND cp_a.`identifier` = 'olt-connected' 
        AND cp.`card_port_id` IS NOT NULL 
        AND c.`location_id` = ?");

    $query->bind_param('d', $id);
    $query->execute();
    $result = $query->get_result();

    if ($result->num_rows > 0) {
        // $cardPortId = $result->fetch_assoc();

        echo '<br />'
        .sprintf('<a target="_blank" class="fancy-button outline round blue" href="%s">Einrichtung dieser MDU anzeigen</a>',
            $this->generateUrl("location-show-info-about-dependent-mdu", ["id" => $id])
        )
        .'&nbsp;&nbsp;&nbsp;&nbsp;'
        .sprintf('<a target="_blank" class="fancy-button outline round red" href="%s">Karten dieser MDU auf OLT einrichten</a>',
            $this->generateUrl("location-provision-dependent-mdu", ["id" => $id])
        )
        .'&nbsp;&nbsp;&nbsp;&nbsp;'
        .sprintf('<a target="_blank" class="ask-confirmation fancy-button outline round red" href="%s">Diese MDU von OLT löschen</a>',
            $this->generateUrl("location-deprovision-dependent-mdu", ["id" => $id])
        )
        .'<br /><br />';

        ?>
        <script type="text/javascript">
        $(document).ready(function () {
            $('.ask-confirmation').on('click', function(e) {
                if (window.confirm("Wollen Sie diese MDU wirklich vom OLT löschen?")) {
                    return;
                }

                e.preventDefault();
            });
        });
        </script>
        <?php
    }

    $result->free();

    ?>

    <table class="Location">
      <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Typ</th>
        <th>Line</th>
        <th>Port-Anzahl</th>
        <th>Freie Ports</th>
        <th>Aktionen</th>
      </tr>
      </thead>
      <tbody>
      <?php
        while ($db_card_row = $db_card_result->fetch_assoc()) {
          $cards[] = $db_card_row;
          $cid = $db_card_row['id'];

          $db_used_card->execute();
          $db_used_card_result = $db_used_card->get_result();
          $ucard_result = $db_used_card_result->fetch_assoc();

          $db_defective_card->execute();
          $db_defective_card_result = $db_defective_card->get_result();
          $dcard_result = $db_defective_card_result->fetch_assoc();

          $freecard = $db_card_row['port_amount'] - $ucard_result['used'] - $dcard_result['used'];
          $db_defective_card_result->free();
          $db_used_card_result->free();

          echo "<tr>";
          echo "<td class='num'>".$db_card_row['id']."</td>";
          echo "<td>".$db_card_row['name']."</td>";
          echo "<td>".$db_card_row['type']."</td>";
          echo "<td>".$db_card_row['Line']."</td>";
          echo "<td class='num'>".$db_card_row['port_amount']."</td>";
          echo "<td class='num'>$freecard</td>";
          echo "<td><a href=\"".$this->generateUrl('location-card-edit', ['locationId' => $id, 'id' => $db_card_row['id']])."\">Bearbeiten</a>
            &nbsp;&nbsp;
            <a href=\"".$this->generateUrl('location-card-clone', ['id' => $id, 'cloneCardId' => $db_card_row['id']])."\">Clone</a>
            </td>";
          echo "</tr>";
        }
      ?>
      </tbody>
    </table>

    <br /><a href="<?php echo $this->generateUrl('location-card-create-new', ['id' => $id]); ?>">Karte hinzufügen</a>

    <br /><br /><a href="<?php echo urldecode($this->generateUrl('network-view', ['id' => $loc_result['network_id']])); ?>"><button type="button"><img alt="" src="<?php echo $StartURL.'/_img/Back.png'; ?>" height="12px"> Zurück</button></a>
  </div>
  
  <h3>LSA-KVz Zuordnungen Grafisch</h3>
  <div>
    <p class="tipp">Tipp: Mehrfachauswahl mit STRG + linke Maus m&ouml;glich (Abbruch mit ESC)</p>
    <?php
    $itemsPerRow = 10;

    foreach ($lsaKvzPartitions as $lsaKvzPartition) {
        $lid = $lsaKvzPartition['id']; // locationId
        $rows = ceil($lsaKvzPartition['pin_amount'] / $itemsPerRow);
        $pinNumber = 0;

        $db_used_lsaKvzPartition_details->execute();
        $db_used_lsaKvzPartition_details_result = $db_used_lsaKvzPartition_details->get_result();

        $db_defective_lsaKvzPartition_details->execute();
        $db_defective_lsaKvzPartition_details_result = $db_defective_lsaKvzPartition_details->get_result();

        $usedLsaPins = array();

        while ($usedLsaPin = $db_used_lsaKvzPartition_details_result->fetch_assoc()) {
            // first actual pin = 1
            $usedLsaPin['lsa_pin'] = (int) $usedLsaPin['lsa_pin'];

            // if isset($usedLsaPins[$usedLsaPin['lsa_pin']])
            // more than one customer is connected to same port
            if (!empty($usedLsaPin['lsa_pin']) && !isset($usedLsaPins[$usedLsaPin['lsa_pin']])) {
                $usedLsaPins[(int) $usedLsaPin['lsa_pin']] = $usedLsaPin;
            }
        }
        unset($usedLsaPin);

        $defective_lsaKvzPartition = array();

        while ($defective = $db_defective_lsaKvzPartition_details_result->fetch_assoc()) {
            $defective_lsaKvzPartition[(int) $defective['value']] = $defective['id'];
        }
        unset($defective);

        $amountPins = array(
            'free' => 0,
            'defective' => 0,
            'taken' => 0,
            'patched' => 0,
            'active' => 0,
        );

        echo '<p align="center">'.$lsaKvzPartition['kvz_name'].'</p>'
            .'<div class="graphical-view-table-container">'
            .'<form method="post" action="" class="toggle-defective-form">'
            .'<input type="hidden" name="toggle-defective-what" value="lsa-kvz-partition" />'
            .'<input type="hidden" name="toggle-defective-what-id" value="'.$lsaKvzPartition['id'].'" />'
            .'<table class="graphical-view-table" align="center" cellspacing="0">';

        while ($rows > 0) {
            echo '<tr>';

            for ($i = 1; $i <= $itemsPerRow; $i++) {
                if (false == ($pinNumber > ($lsaKvzPartition['pin_amount'] - 1))) {
                    $pinNumber++;
                    $class = 'bg-grey';
                    $tdContent = '<input type="submit" name="toggle-defective-status" value="'.$pinNumber.'" />';

                    if (isset($usedLsaPins[$pinNumber])) {
                        $class = 'bg-orange';
                        $tdContent = '<a href="'.$StartURL.'/index.php?menu=customer&func=vertrag&area=SwitchTechData&id='
                            .$usedLsaPins[$pinNumber]['id'].'" target="_blank">'.$pinNumber.'</a>';

                        if (!empty($usedLsaPins[$pinNumber]['patch_date'])) {
                            $class = 'bg-blue';

                            if (!empty($usedLsaPins[$pinNumber]['connection_activation_date'])) {
                                $amountPins['active']++;
                                $class = 'bg-green';
                            } else {
                              $amountPins['patched']++;
                            }
                        } else {
                          $amountPins['taken']++;
                        }
                    }

                    if (isset($defective_lsaKvzPartition[$pinNumber])) {
                        $amountPins['defective']++;
                        $class = 'bg-red';
                        $tdContent = '<input type="submit" name="toggle-defective-status" value="'.$pinNumber.'" />';
                    }

                    if ('bg-grey' == $class) {
                        $amountPins['free']++;
                    }

                    echo '<td class="'.$class.'">'.$tdContent.'</td>';
                } else {
                    echo '<td></td>';
                }
            }

            echo '</tr>';

            $rows--;
        }

        echo '</table></form>'
            .'<ul class="legend">'
            .'<li class="bg-green">Aktiv ('.$amountPins['active'].')</li>'
            .'<li class="bg-grey">Frei ('.$amountPins['free'].')</li>'
            .'</ul>'
            .'<ul class="legend">'
            .'<li class="bg-red">Defekt ('.$amountPins['defective'].')</li>'
            .'<li class="bg-orange">Belegt ('.$amountPins['taken'].')</li>'
            .'</ul>'
            .'<ul class="legend">'
            .'<li class="bg-blue">Gepatched ('.$amountPins['patched'].')</li>'
            .'</ul>'
            .'<div class="multi-select-button-container hide">'
            .'<button type="button"><img alt="" src="'.$StartURL.'/_img/Yes.png" height="12px"> Update</button>'
            .'</div></div><br />';
    }
    ?>
  </div>

  <h3>DSLAM Karten Grafisch</h3>
  <div>
    <p class="tipp">Tipp: Mehrfachauswahl mit STRG + linke Maus m&ouml;glich (Abbruch mit ESC)</p>
    <?php
    $itemsPerRow = 10;

    //$id // locationId

    //Card
    $doctrine = $this->getDoctrine();
    $location = $doctrine->getRepository(Location::class)->findById($id);
    $cards = $doctrine->getRepository(Card::class)->findByLocation($location);

    foreach ($cards as $card) {
        echo '<p align="center">'.$card->getName().' ('.$card->getCardType()->getName().')</p>'
            .'<div class="graphical-view-table-container">'
            .'<form method="post" action="" class="toggle-defective-form">'
            .'<input type="hidden" name="toggle-defective-what" value="card" />'
            .'<input type="hidden" name="toggle-defective-what-id" value="'.$card->getId().'" />'
            .'<table class="graphical-view-table" align="center" cellspacing="0">';

        $amountPins = [
            'active' => 0,
            'free' => 0,
            'defective' => 0,
            'taken' => 0,
            'patched' => 0,
            'related' => 0,
        ];

        $cardPorts = $doctrine->getRepository(CardPort::class)->findByCard($card, ['number' => 'ASC']);

        if (!empty($cardPorts)) {
            $statement = $doctrine->getConnection()->prepare(
                "SELECT `id`, `dslam_port`, `connection_activation_date`, `patch_date` 
                FROM `customers` 
                WHERE `card_id` = ? AND `dslam_port` IS NOT NULL AND `dslam_port` != ''"
            );
            $statement->bindParam(1, $card->getId(), \PDO::PARAM_INT);
            $statement->execute();

            $usedCardPorts = [];

            foreach ($statement->fetchAll() as $array) {
                $array['dslam_port'] = (int) $array['dslam_port'];

                if (!isset($usedCardPorts[$array['dslam_port']])) {
                    $usedCardPorts[$array['dslam_port']] = [];
                }

                $usedCardPorts[$array['dslam_port']][] = $array;
            }

            $defectiveCards = [];

            $db_used_card_details->execute();
            $db_used_card_details_result = $db_used_card_details->get_result();
            $db_defective_cards_details->execute();
            $db_defective_cards_details_result = $db_defective_cards_details->get_result();

            while ($defective = $db_defective_cards_details_result->fetch_assoc()) {
                $defectiveCards[$defective['value']] = $defective['id'];
            }

            $columnsCounter = $itemsPerRow + 1;

            echo '<tr>';

            foreach ($cardPorts as $cardPort) {
                if (--$columnsCounter < 1) {
                    $columnsCounter = $itemsPerRow;
                    echo '</tr><tr>';
                }

                // is used by customer
                if (isset($usedCardPorts[(int) $cardPort->getNumber()])) {
                    $columnsCounter++;

                    // due to versions a card port can be used with several customers
                    foreach ($usedCardPorts[(int) $cardPort->getNumber()] as $usedCardPort) {
                         if (--$columnsCounter < 1) {
                            $columnsCounter = $itemsPerRow;
                            echo '</tr><tr>';
                        }

                        $class = 'bg-orange';
                        $tdContent = '<a href="'.$StartURL.'/index.php?menu=customer&func=vertrag&area=SwitchTechData&id='
                            .$usedCardPort['id'].'" target="_blank">'.$cardPort->getNumber().'</a>';

                        if (!empty($usedCardPort['patch_date'])) {
                            $class = 'bg-blue';

                            if (!empty($usedCardPort['connection_activation_date'])) {
                                $amountPins['active']++;
                                $class = 'bg-green';
                            } else {
                                $amountPins['patched']++;
                            }
                        } else {
                            $amountPins['taken']++;
                        }

                        echo '<td class="'.$class.'">'.$tdContent.'</td>';
                    }

                    continue;
                }

                $class = 'bg-grey';
                $tdContent = '<input type="submit" name="toggle-defective-status" value="'.$cardPort->getNumber().'" />';
                
                if (null !== $cardPort->getRelatedCardPort()) {
                    // is used by relation?

                    $amountPins['related']++;

                    $class = 'bg-yellow';
                    $tdContent = '<a href="'.$this->generateUrl('location-card-edit', ['locationId' => $cardPort->getRelatedCardPort()->getCard()->getLocation()->getId(), 'id' => $cardPort->getRelatedCardPort()->getCard()->getId()]).'" target="_blank">'.$cardPort->getNumber().'</a>';
                } elseif (isset($defectiveCards[$cardPort->getNumber()])) {
                    // is defective

                    $amountPins['defective']++;

                    $class = 'bg-red';
                    $tdContent = '<input type="submit" name="toggle-defective-status" value="'.$cardPort->getNumber().'" />';
                }

                if ('bg-grey' == $class) {
                    $amountPins['free']++;
                }

                echo '<td class="'.$class.'">'.$tdContent.'</td>';
            }

            for ($columnsCounter > 2; $columnsCounter--;) {
                echo '<td></td>';
            }

            echo '</tr>';
        }

        echo '</table></form>'
            .'<ul class="legend">'
            .'<li class="bg-green">Aktiv ('.$amountPins['active'].')</li>'
            .'<li class="bg-grey">Frei ('.$amountPins['free'].')</li>'
            .'</ul>'
            .'<ul class="legend">'
            .'<li class="bg-red">Defekt ('.$amountPins['defective'].')</li>'
            .'<li class="bg-orange">Belegt ('.$amountPins['taken'].')</li>'
            .'</ul>'
            .'<ul class="legend">'
            .'<li class="bg-blue">Gepatched ('.$amountPins['patched'].')</li>'
            .'<li class="bg-yellow">Verknüpft ('.$amountPins['related'].')</li>'
            .'</ul>'
            .'<div class="multi-select-button-container hide">'
            .'<button type="button"><img alt="" src="'.$StartURL.'/_img/Yes.png" height="12px"> Update</button>'
            .'</div></div><br />';
    }
    ?>
  </div>
  
</div>

<?php
    $db_used_lsaKvzPartition_details->close();
    $db_defective_lsaKvzPartition_details->close();
    $db_defective_cards_details->close();
    $db_used_card_details->close();
#    $db_net_localtion_result->free();
#    $db_net_localtion->close();
    $db_defective_card->close();
    $db_used_card->close();
    $db_card_result->free();
    $db_card->close();
    $db_defective_lsaKvzPartition->close();
    $db_used_lsaKvzPartition->close();
    $db_lsaKvzPartition_result->free();
    $db_lsaKvzPartition->close();
    $db_location->close();
    $db->close();
  //############################################################################
  }
?>
