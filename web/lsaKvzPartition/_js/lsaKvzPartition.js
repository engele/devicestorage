function lsaSubmit(){
 htmlstr = "Wollen Sie die Änderungen speichern?"
  $("#dialog").html(htmlstr);
  $("#dialog").dialog({
    resizable: true,
    modal: true,
    title: 'LSA-KVz Zuordnung bearbeiten',
    position: {my: 'top', at: 'top', of: '#Container'},
    height: 300,
    width: 500,
    buttons: {
      'Ja': function () {
        $(this).dialog('close');
        callSubmit(true);
      },
      'Nein': function () {
        $(this).dialog('close');
        callSubmit(false);
      }
    }
  });
}

function callSubmit(save) {
  if (save){
    $("#command").attr('name', 'save');
    $("#lsaKvzForm").submit();
  }
}

function lsaDelete(){
 htmlstr = "Wollen Sie die LSA-KVz Zuordnung wirklich löschen?"
  $("#dialog").html(htmlstr);
  $("#dialog").dialog({
    resizable: true,
    modal: true,
    title: 'LSA-KVz Zuordnung löschen',
    position: {my: 'top', at: 'top', of: '#Container'},
    height: 300,
    width: 500,
    buttons: {
      'Ja': function () {
        $(this).dialog('close');
        callDelete(true);
      },
      'Nein': function () {
        $(this).dialog('close');
        callDelete(false);
      }
    }
  });
}

function callDelete(save) {
  if (save){
    $("#command").attr('name', 'delete');
    $("#lsaKvzForm").submit();
  }
}

$(document).ready(function(){
  $("table.LsaKvzPartition").tablesorter({
    widgets: ['zebra']
  });
});