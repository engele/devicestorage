<?php
  require_once ($StartPath.'/_conf/database.inc');
  require_once ($StartPath.'/'.$SelMenu.'/_conf/database.inc');

//-------------------------------------------------------------------------  
  if (key_exists ('func', $_GET) and $_GET['func'] == 'edit') {
    if (key_exists ('save', $_POST)) {
      if (!$_POST['LsaKvzPartitionId']) {
        $sql_save = "INSERT INTO lsa_kvz_partitions SET ";
        $sql_save .= "location_id='".$_POST['LsaKvzPartitionLid']."',";
        $sql_save .= "kvz_name='".$_POST['LsaKvzPartitionKvzName']."',";
        $sql_save .= "pin_amount=".$_POST['LsaKvzPartitionPinAmount'].",";
        $sql_save .= "dpbo='".$_POST['LsaKvzPartitionDpbo']."',";
        if (!empty($_POST['LsaKvzPartitionEselWert'])) {
            $sql_save .= "`esel_wert`='".$_POST['LsaKvzPartitionEselWert']."',";
        }
        $sql_save .= "default_VLAN='".$_POST['LsaKvzPartitionDefaultVlan']."'";
        $db_save_result = $db->query($sql_save);
        if ($db_save_result) {
          $customer_saved = 'wurde hinzugefügt';
        } else {
          $customer_saved = '<span class="text red">konnte nicht hinzugefügt werden!</span>';
        }
        echo "<div class='LsaKvzPartition box'>";
        echo "<h3>Diese LSA-KVz Zuordnung $customer_saved</h3>";
        echo "<div class='LsaKvzPartition form'><table>";
        echo "<tr><td>KVz Bezeichnung:</td><td>".$_POST['LsaKvzPartitionKvzName']."</td></tr>";
        echo "<tr><td>Anzahl LSA Kontakte:</td><td>".$_POST['LsaKvzPartitionPinAmount']."</td></tr>";
        echo "<tr><td>DPBO:</td><td>".$_POST['LsaKvzPartitionDpbo']."</td></tr>";
        echo "<tr><td>Esel-Wert:</td><td>".$_POST['LsaKvzPartitionEselWert']."</td></tr>";
        echo "<tr><td>Standard VLAN:</td><td>".$_POST['LsaKvzPartitionDefaultVlan']."</td></tr>";
        echo "</table></div></div>";
      } else {
        $sql_save = "UPDATE lsa_kvz_partitions SET ";
        $sql_save .= "location_id='".$_POST['LsaKvzPartitionLid']."',";
        $sql_save .= "kvz_name='".$_POST['LsaKvzPartitionKvzName']."',";
        $sql_save .= "pin_amount=".$_POST['LsaKvzPartitionPinAmount'].",";
        $sql_save .= "dpbo='".$_POST['LsaKvzPartitionDpbo']."',";
        if (!empty($_POST['LsaKvzPartitionEselWert'])) {
            $sql_save .= "`esel_wert` = '".$_POST['LsaKvzPartitionEselWert']."',";
        } else {
            $sql_save .= "`esel_wert` = NULL,";
        }
        $sql_save .= "default_VLAN='".$_POST['LsaKvzPartitionDefaultVlan']."' ";
        $sql_save .= "WHERE id=".$_POST['LsaKvzPartitionId'];
        $db_save_result = $db->query($sql_save);
        if ($db_save_result) {
          $customer_saved = 'wurde geändert';
        } else {
          $customer_saved = '<span class="text red">konnte nicht geändert werden!</span>';
        }
        echo "<div class='LsaKvzPartition box'>";
        echo "<h3>Diese LSA-KVz Zuordnung $customer_saved</h3>";
        echo "<div class='LsaKvzPartition form'><table>";
        echo "<tr><td>KVz Bezeichnung:</td><td>".$_POST['LsaKvzPartitionKvzName']."</td></tr>";
        echo "<tr><td>Anzahl LSA Kontakte:</td><td>".$_POST['LsaKvzPartitionPinAmount']."</td></tr>";
        echo "<tr><td>DPBO:</td><td>".$_POST['LsaKvzPartitionDpbo']."</td></tr>";
        echo "<tr><td>Esel-Wert:</td><td>".$_POST['LsaKvzPartitionEselWert']."</td></tr>";
        echo "<tr><td>Standard VLAN:</td><td>".$_POST['LsaKvzPartitionDefaultVlan']."</td></tr>";
        echo "</table></div></div>";
      }
    } elseif (key_exists ('delete', $_POST)) {
      $sql_delete = "DELETE FROM lsa_kvz_partitions WHERE id = ".$_POST['LsaKvzPartitionId'];
      $db_delete_result = $db->query($sql_delete);
      
      echo "<div class='LsaKvzPartition box'>";
      echo "<h3>Diese LSA-KVz Zuordnung wurde gelöscht</h3>";
      echo "<div class='LsaKvzPartition form'><table>";
      echo "<div class='LsaKvzPartition form'><table>";
      echo "<tr><td>KVz Bezeichnung:</td><td>".$_POST['LsaKvzPartitionKvzName']."</td></tr>";
      echo "<tr><td>Anzahl LSA Kontakte:</td><td>".$_POST['LsaKvzPartitionPinAmount']."</td></tr>";
      echo "<tr><td>DPBO:</td><td>".$_POST['LsaKvzPartitionDpbo']."</td></tr>";
      echo "<tr><td>Standard VLAN:</td><td>".$_POST['LsaKvzPartitionDefaultVlan']."</td></tr>";
      echo "</table></div></div>";
    } else {
      if (key_exists ('lid', $_GET)) $lid = $_GET['lid']; else $lid = 0;
      $db_location = $db->prepare($sql['location']);
      $db_location->bind_param('d', $lid);
      $db_location->execute();
      $db_location_result = $db_location->get_result();
      $loc_result = $db_location_result->fetch_assoc();
      $db_location_result->free();
      $db_location->close();
      if (key_exists ('netid_string', $loc_result))     $loc_db['netid_string']     = $loc_result['netid_string'];      else $loc_db['netid_string']      = '';
      if (key_exists ('netname', $loc_result))          $loc_db['netname']          = $loc_result['netname'];           else $loc_db['netname']           = '';
      if (key_exists ('locname', $loc_result))          $loc_db['locname']          = $loc_result['locname'];           else $loc_db['locname']           = '';

      if (key_exists ('id', $_GET)) $id = $_GET['id']; else $id = 0;
      $db_kvz_lsa = $db->prepare($sql['lsa_kvz_partitions']);
      $db_kvz_lsa->bind_param('d', $id);
      $db_kvz_lsa->execute();
      $db_kvz_lsa_result = $db_kvz_lsa->get_result();
      $kvz_result = $db_kvz_lsa_result->fetch_assoc();
      $db_kvz_lsa_result->free();
      if (!$kvz_result) $kvz_result = array();
      if (key_exists ('id', $kvz_result))           $kvz_db['id']           = $kvz_result['id'];            else $kvz_db['id']            = 0;
      if (key_exists ('location_id', $kvz_result))  $kvz_db['location_id']  = $kvz_result['location_id'];   else $kvz_db['location_id']   = '';
      if (key_exists ('kvz_name', $kvz_result))     $kvz_db['kvz_name']     = $kvz_result['kvz_name'];      else $kvz_db['kvz_name']      = '';
      if (key_exists ('pin_amount', $kvz_result))   $kvz_db['pin_amount']   = $kvz_result['pin_amount'];    else $kvz_db['pin_amount']    = '';
      if (key_exists ('dpbo', $kvz_result))         $kvz_db['dpbo']         = $kvz_result['dpbo'];          else $kvz_db['dpbo']          = '';
      if (key_exists ('esel_wert', $kvz_result))    $kvz_db['esel_wert']    = $kvz_result['esel_wert'];     else $kvz_db['esel_wert']     = null;
      if (key_exists ('default_VLAN', $kvz_result)) $kvz_db['default_VLAN'] = $kvz_result['default_VLAN'];  else $kvz_db['default_VLAN']  = '';
?>
<form id="lsaKvzForm" action="<?php echo $StartURL.'/index.php?menu=lsaKvzPartition&func=edit'; ?>" method="post">
<div class="LsaKvzPartition box">
  <h3>LSA-KVz Zuordnung <?php if ($id) echo "bearbeiten"; else echo "hinzufügen"; ?> (<?php echo $loc_db['locname']; ?> - <?php echo $loc_db['netid_string']; ?> - <?php echo $loc_db['netname']; ?>)</h3>
  <div class="LsaKvzPartition form">
    <input name="LsaKvzPartitionId" id="LsaKvzPartitionId" type="hidden" value="<?php echo $kvz_db['id']; ?>">
    <input name="LsaKvzPartitionLid" id="LsaKvzPartitionLid" type="hidden" value="<?php echo $lid; ?>">
    <p>
      <label for="LsaKvzPartitionKvzName">KVz Bezeichnung</label><br>
      <input id="LsaKvzPartitionKvzName" type="text" size="60" name="LsaKvzPartitionKvzName" value="<?php echo $kvz_db['kvz_name']; ?>">
    </p>
    <p>
      <label for="LsaKvzPartitionPinAmount">Anzahl LSA Kontakte</label><br>
      <input id="LsaKvzPartitionPinAmount" type="number" size="60" name="LsaKvzPartitionPinAmount" value="<?php echo $kvz_db['pin_amount']; ?>">
    </p>
    <p>
      <label for="LsaKvzPartitionDpbo">DPBO</label><br>
      <input id="LsaKvzPartitionDpbo" type="number" size="60" name="LsaKvzPartitionDpbo" value="<?php echo $kvz_db['dpbo']; ?>">
    </p>
    <p>
      <label for="LsaKvzPartitionEselWert">Esel-Wert</label><br>
      <input id="LsaKvzPartitionEselWert" type="number" size="60" name="LsaKvzPartitionEselWert" value="<?php echo $kvz_db['esel_wert']; ?>">
    </p>
    <p>
      <label for="LsaKvzPartitionDefaultVlan">Standard VLAN</label><br>
      <input id="LsaKvzPartitionDefaultVlan" type="text" size="60" name="LsaKvzPartitionDefaultVlan" value="<?php echo $kvz_db['default_VLAN']; ?>">
    </p>
  </div>
  <h3 class="KvzSave"></h3>
  <div class="KvzSave form">
    <input name='none' id='command' type='hidden' value='yes'>
    <button name="save" onclick="lsaSubmit()" type="button"><img alt="" src="<?php echo $StartURL.'/_img/Yes.png'; ?>" height="12px"> Speichern</button>&nbsp;&nbsp;&nbsp;&nbsp;
    <button name="reset" type="reset"><img alt="" src="<?php echo $StartURL.'/_img/Erase.png'; ?>" height="12px"> Zurücksetzten</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <?php if ($id) echo "<button name='delete' onclick='lsaDelete()' type='button'><img alt='' src='$StartURL/_img/delete.png' height='12px'> Löschen</button>"; ?>
  </div>
</div>
</form>
<div id="dialog"></div>

<?php
      $db_kvz_lsa->close();
      $db->close();
    }
  }
?>
