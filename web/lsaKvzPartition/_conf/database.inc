<?php
  $sql = array (
    // Location
    'location' => "
      SELECT loc.name AS locname, kvz_prefix, cid_suffix_start, cid_suffix_end, net.id_string AS netid_string, net.name AS netname FROM locations AS loc, networks AS net
      WHERE loc.network_id = net.id
      AND loc.id = ?
    ",

    'lsa_kvz_partitions' => "
      SELECT * FROM lsa_kvz_partitions
      WHERE id = ?
    ",

  );

  $GLOBALS['sql'] = & $sql;
?>
