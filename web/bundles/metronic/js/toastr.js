//== Class definition
var ToastrUI = function() {

    //== Private functions
    return {
        // public functions
        init: function(alerts) {

            var i = -1;
            var toastCount = 0;
            var $toastlast;

            if (alerts.success.length > 0) {
                for (var countSuccesses = 0; countSuccesses < alerts.success.length; countSuccesses++) {
                    
                    var shortCutFunction = 'success';
                    var title = 'Erfolgreich!';
                    var msg = alerts.success[countSuccesses];
                    if (!msg) {
                        msg = "this should not happen, please call your admin!";
                    }

                    toastr.options = {
                      "closeButton": true,
                      "debug": false,
                      "newestOnTop": true,
                      "progressBar": false,
                      "positionClass": "toast-top-right",
                      "preventDuplicates": false,
                      "showDuration": "300",
                      "hideDuration": "1000",
                      "timeOut": "5000",
                      "extendedTimeOut": "1000",
                      "showEasing": "swing",
                      "hideEasing": "linear",
                      "showMethod": "fadeIn",
                      "hideMethod": "fadeOut"
                    };

                    $('#toastrOptions').text(
                            'toastr.options = '
                            + JSON.stringify(toastr.options, null, 2)
                            + ';'
                            + '\n\ntoastr.'
                            + shortCutFunction
                            + '("'
                            + msg
                            + (title ? '", "' + title : '')
                            + '");'
                    );
                    var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                }
            }

            if (alerts.error.length > 0) {
                for (var countErrors = 0; countErrors < alerts.error.length; countErrors++) {
                    
                    var shortCutFunction = 'error';
                    var title = 'Fehler!';
                    var msg = alerts.error[countErrors];
                    if (!msg) {
                        msg = "this should not happen, please call your admin!";
                    }

                    toastr.options = {
                      "closeButton": true,
                      "debug": false,
                      "newestOnTop": true,
                      "progressBar": false,
                      "positionClass": "toast-top-right",
                      "preventDuplicates": false,
                      "showDuration": "300",
                      "hideDuration": "1000",
                      "timeOut": "0",
                      "extendedTimeOut": "1000",
                      "showEasing": "swing",
                      "hideEasing": "linear",
                      "showMethod": "fadeIn",
                      "hideMethod": "fadeOut"
                    };

                    $('#toastrOptions').text(
                            'toastr.options = '
                            + JSON.stringify(toastr.options, null, 2)
                            + ';'
                            + '\n\ntoastr.'
                            + shortCutFunction
                            + '("'
                            + msg
                            + (title ? '", "' + title : '')
                            + '");'
                    );
                    var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                }
            }
        }
    };
}();
